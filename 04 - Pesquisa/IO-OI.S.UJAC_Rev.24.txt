 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da UHE Jacuí 
 
 
Código Revisão Item Vigência 
IO-OI.S.UJAC 24 3.7.5.2. 03/09/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Incorporação da MOP/ONS 372 -R/2024 “Mudança da denominação da CEEE -T e do seu centro de 
operação”. 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S BME CPFL-T Coprel RGE Sul 
Hidropan      
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Jacuí IO-OI.S.UJAC 24 3.7.5.2. 03/09/2024 
 
Referência:  2 / 9 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  3 
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL ................................ ....................  3 
4.1. Procedimentos Gerais ................................................................................................................... 3 
4.2. Procedimentos Específicos ............................................................................................................ 4 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  4 
5.1. Procedimentos Gerais ................................................................................................................... 4 
5.2. Procedimentos para Recomposição Fluente ................................................................................. 5 
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................ 5 
5.2.2. Recomposição Fluente da Instalação .......................................................................... 5 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 7 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 7 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 7 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 8 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 8 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 8 
6. MANOBRAS DE UNIDADES GERADORAS ................................ ................................ ........................  8 
6.1. Procedimentos Gerais ................................................................................................................... 8 
6.2. Procedimentos Específicos ............................................................................................................ 8 
6.2.1. Desligamento de Unidades Geradoras ........................................................................ 8 
6.2.2. Sincronismo de Unidades Geradoras .......................................................................... 9 
7. NOTAS IMPORTANTES ................................ ................................ ................................ .................. 9 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Jacuí IO-OI.S.UJAC 24 3.7.5.2. 03/09/2024 
 
Referência:  3 / 9 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da UHE Jacuí , definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da Instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR-S e a Instalação, de propriedade da CEEE-G, é realizada pela 
CPFL-T, agente responsável pela operação da Instalação, por intermédio do COT CPFL-T. 
Nota: A execução da operação na UHE Jacuí é realizada pela CEEE-G. 
2.3. As unidades geradoras e equipamentos desta Instalação fazem parte da Área 230 kV do Rio Grande 
do Sul. 
2.4. O COSR-S controla e supervisiona o despacho de geração desta Usina. 
2.5. Esta Usina: 
• está conectada na Rede de Distribuição; 
• não participa do Controle Automático da Geração – CAG; 
• é de autorrestabelecimento; 
• é fonte para início do processo de recomposição fluente da Área Jacuí; 
• é responsável pelo controle de frequência na Área Jacuí da Região Sul, na fase de recomposição 
fluente. 
2.6. Os dados operacionais desta Usina  estão descritos no Cadastro de Informações Operacionais da 
respectiva Área Elétrica. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. Unidades Geradoras 
As unidades geradoras G-1, G-2, G-3, G-4, G-5 e G-6 23 kV estão conectadas ao barramento de 138 kV pelos 
transformadores TR-1, TR-2, TR-3, TR-4, TR-5 e TR-6 23/138 kV da UHE Jacuí, com todas as seccionadoras e 
disjuntores fechados, exceto uma das seccionadoras seletoras de barra. 
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. A Usina deve manter os valores de geração de acordo com os valores programados, constantes no 
Programa Diário de Operação – PDO. Para atendimento dos valores programados constantes no PDO, 
não é necessária a autorização prévia do COSR-S. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Jacuí IO-OI.S.UJAC 24 3.7.5.2. 03/09/2024 
 
Referência:  4 / 9 
 
Qualquer alteração no valor de geração da Usina em relação ao valor de geração ao constante no PDO 
somente pode ser executada após autorização do COSR-S. 
As reprogramações de geração quando de necessidades sistêmicas serão executadas pela Usina 
quando solicitadas pelo COSR-S. 
Após reprogramação de geração solicitada pelo COSR -S, o Agente somente poderá alterar a geração 
da Usina com autorização do ONS, inclusive para adoção de valores contidos no PDO. 
4.1.2. Os desvios de geração da Usina em relação aos valores previstos no PDO ou em relação às 
reprogramações devem ser controlados observando -se os valores máximos permitidos explicitados 
na Instrução de Operação IO-CG.BR.01 – Controle da Geração em Condição Normal. 
4.1.3. Quando não existir ou não estiver disponível a supervisão da Usina para o ONS, essa deve seguir as 
orientações para envio de dados conforme Rotina Operacional RO-AO.BR.08 – Apuração de Dados de 
Despacho de Geração e de Intercâmbios. 
4.1.4. A Usina deve registrar e informar imediatamente os seguintes dados ao COSR-S: 
• movimentação de unidades geradoras hidráulicas (mudança de estado operativo / disponibilidade);  
• restrições e ocorrências na Usina ou na conexão elétrica que afetem a disponibilidade de geração, 
com o respectivo valor da restrição, contendo o horário de início e término e a descrição do evento; 
• demais informações sobre a operação da Instalação, solicitadas pelo ONS. 
4.1.5. O controle de tensão, por meio da geração ou absorção de potência reativa pelas unidades geradoras 
da Usina, é executado com autonomia pela operação do Agente e deve ser realizado entre o agente 
de geração e o agente de distribuição. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
Não se aplica. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação: caracterizado por meio da verificação de ausência de tensão em 
todos os terminais de suas conexões ou ausência de fluxo de potência ativa nessas conexões. 
• Desligamento parcial da Instalação:  qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, a operação da Instalação deve fornecer ao COSR -S as 
seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Jacuí IO-OI.S.UJAC 24 3.7.5.2. 03/09/2024 
 
Referência:  5 / 9 
 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2., sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia do Agente Operador  da Instalação na 
recomposição, deve ser utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4. 
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE 
No caso de desligamento total, o Agente Operador deve  configurar os disjuntores dos seguintes 
equipamentos, linhas de transmissão e unidades geradoras, conforme condição apresentada a seguir. 
Abrir ou manter abertos os disjuntores: 
• das linhas de transmissão: 
LT 138 kV Cruz Alta 1 / Usina Hidrelétrica Jacuí; 
LT 138 kV Júlio de Castilhos 1 / Usina Hidrelétrica Jacuí; 
LT 138 kV Usina Hidrelétrica Jacuí / Usina Hidrelétrica Passo Real C1; 
LT 138 kV Usina Hidrelétrica Jacuí / Usina Hidrelétrica Passo Real C2; 
LT 138 kV Usina Hidrelétrica Jacuí / Usina Hidrelétrica Rincão do Ivaí. 
• do transformador: 
TR-7 138/23 kV (lado de 138 kV). 
• das unidades geradoras: 
G-1 13,8 kV (de 138 kV); 
G-2 13,8 kV (de 138 kV); 
G-3 13,8 kV (de 138 kV); 
G-4 13,8 kV (de 138 kV); 
G-5 13,8 kV (de 138 kV); 
G-6 13,8 kV (de 138 kV). 
Fechar ou manter fechado o disjuntor: 
• do módulo de interligação de barras de 138 kV. 
Desligar ou manter desligado o modo de comutação automática dos comutadores sob carga da 
transformação 138/23 kV. 
Cabe ao Agente CPFL-T informar ao COSR-S quando a configuração de preparação da Instalação não estiver 
atendida para o início da recomposição, independentemente de o equipamento ser próprio ou de outros 
agentes. Nesse caso, o COSR -S fará contato com os agentes envolvidos  para identificar o motivo do não -
atendimento e , após confirmação do Agente CPFL-T de que os barramentos estão com a configuração 
atendida, o COSR -S coordenar á os procedimentos para recomposição em função da configuração desta 
Instalação. 
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO 
A Instalação faz parte da recomposição da Área Jacuí. O Agente Operador deve  adotar os procedimentos a 
seguir para a recomposição fluente: 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Jacuí IO-OI.S.UJAC 24 3.7.5.2. 03/09/2024 
 
Referência:  6 / 9 
 
Passo Executor Procedimentos Condições ou Limites Associados 
1 
CPFL-T 
Partir uma unidade geradora, 
energizar o barramento 138  kV da 
UHE Jacuí e controlar a frequência em 
60 Hz. 
Manter a tensão terminal em 
13,1 kV. 
1.1 
Energizar a interligação em 23 kV 
entre as usinas hidrelétricas Jacuí e 
Passo Real, como alternativa para 
alimentar os serviços auxiliares da 
UHE Passo Real. 
 
1.2 
Partir a segunda unidade geradora, 
sincronizá-la e controlar a frequência 
em 60 Hz. 
Manter a tensão terminal em 
13,1 kV. 
Possibilitar o restabelecimento de 
carga prioritária na subestação Santa 
Maria 1 e nas subestações atendidas 
pela Santa Maria 1. 
1.2.1 RGE Sul 
Restabelecer carga prioritária na 
subestação Santa Maria 1 e nas 
subestações atendidas pela Santa 
Maria 1. 
Obs.: o recebimento de tensão na  SE 
Santa Maria 1 se dá pela LT 138 kV 
Júlio de Castilhos 1 / Usina 
Hidrelétrica Jacuí e pela LT 138 kV 
Júlio de Castilhos 1 / Santa Maria 1 , 
operadas pela CPFL-T. 
No máximo 30 MW. 
Duas unidades geradoras 
sincronizadas na UHE Jacuí. 
Respeitar o limite mínimo de tensão, 
de 124 kV, no s barramentos de 
138 kV. 
1.3 
CPFL-T 
Energizar a LT 138 kV Usina 
Hidrelétrica Jacuí / Usina Hidrelétrica 
Passo Real C2, enviando tensão para a 
UHE Passo Real. 
Duas unidades geradoras 
sincronizadas na UHE Jacuí. 
1.4 
Partir a terceira unidade geradora, 
sincronizá-la e controlar a frequência 
em 60 Hz. 
Manter a tensão terminal em 
13,1 kV. 
1.5 
Partir a quarta unidade geradora, 
sincronizá-la e controlar a frequência 
em 60 Hz. 
Manter a tensão terminal em 
13,1 kV. 
Possibilitar o restabelecimento de 
carga prioritária na SE Cruz Alta 1 e 
nas subestações atendidas pela SE 
Cruz Alta 1. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Jacuí IO-OI.S.UJAC 24 3.7.5.2. 03/09/2024 
 
Referência:  7 / 9 
 
Passo Executor Procedimentos Condições ou Limites Associados 
1.5.1 RGE Sul 
Restabelecer carga prioritária na SE 
Cruz Alta 1. 
 
No máximo 13 MW. 
Quatro unidades geradoras 
sincronizadas na UHE Jacuí. 
Respeitar o limite mínimo de tensão, 
de 124 kV, nos barramentos de 
138 kV. 
1.5.2 Coprel / Hidropan 
Restabelecer carga prioritária nas 
subestações atendidas pela SE Cruz 
Alta 1. 
 
No máximo 11 MW. 
Quatro unidades geradoras 
sincronizadas na UHE Jacuí. 
Após um intervalo de pelo menos 1 
(um) minuto da tomada de carga na 
SE Cruz Alta 1. 
Respeitar o limite mínimo de tensão, 
de 124 kV, nos barramentos de 
138 kV. 
1.6 CPFL-T 
Partir a quinta unidade geradora, 
sincronizar e controlar a frequência 
em 60 Hz. 
Manter a tensão terminal em 
13,1 kV. 
2 CPFL-T 
Receber tensão da UHE Passo Real, 
pela LT 138 kV Usina Hidrelétrica Jacuí 
/ Usina Hidrelétrica Passo Real C1 e 
ligá-la, em anel. 
∆δ ≤ 10°. 
Cinco unidades geradoras 
sincronizadas na UHE Jacuí. 
3 CPFL-T 
Partir a sexta unidade geradora, caso 
essa estivesse operando antes do 
desligamento, sincronizá -la e 
controlar frequência em 60 Hz. 
Manter a tensão terminal em 
13,1 kV.  
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2 ., enquanto não houver 
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia deste na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras ou na IO-RR.S.JAC.  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Jacuí IO-OI.S.UJAC 24 3.7.5.2. 03/09/2024 
 
Referência:  8 / 9 
 
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, o 
Agente Operador  deve preparar a Instalação conforme Subitem 5.2 .1., sem necessidade de 
autorização do ONS. 
5.4.1.2. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador  
deve recompor a Instalação conforme Subitem 5.2.2., sem necessidade de autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., o Agente Operador  
deve recompor a Instalação conforme Subitem 6.2.2., sem necessidade de autorização do ONS.    
6. MANOBRAS DE UNIDADES GERADORAS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desligamento programado ou de urgência de unidades geradoras só podem 
ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para sincronismo de unidades geradoras, após desligamentos programados, de 
urgência ou de emergência, só podem ser efetuados com controle do COSR-S. 
6.1.3. Os procedimentos para sincronismo de unidades geradoras, após desligamento automático sem 
atuação de proteção que impeça o retorno do equipamento, só podem ser executados com 
autonomia pelo Agente Operador da Instalação quando estiverem explicitados e estiverem atendidas 
as condições do Subitem 6.2.2. desta Instrução de Operação. 
6.1.4. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.5. A partida de unidades geradoras deve seguir critérios próprios do Agente. O sincronismo à Rede de 
Distribuição deve ser realizado após tratativas entre a Usina e o Agente de Distribuição. A tomada de 
carga deve ser realizada com controle do COSR-S. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESLIGAMENTO DE UNIDADES GERADORAS  
O desligamento de unidades geradoras é sempre controlado pelo COSR-S. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Jacuí IO-OI.S.UJAC 24 3.7.5.2. 03/09/2024 
 
Referência:  9 / 9 
 
6.2.2. SINCRONISMO DE UNIDADES GERADORAS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de unidades geradoras. 
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais unidades geradoras em operação, conforme explicitado nas condições de 
energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Unidade Geradora Procedimentos Condições ou Limites Associados 
Unidade Geradora 
G-1, G-2, G-3, G-4, G-5 
ou G-6 13,8 kV 
Partir e sincronizar a unidade geradora. • 13,1 kV ≤ VUJAC-13,8 ≤ 14,5 kV; e 
• elevar a geração da unidade 
geradora, após autorização do 
COSR-S. 
7. NOTAS IMPORTANTES 
Não se aplica. 
