 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da UHE Garibaldi 
 
 
Código Revisão Item Vigência 
IO-OI.S.GAR 15 3.7.5.3. 02/08/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Adequação do Subitem 5.4.  
- Adequação à RT-OI.BR revisão 37, com destaque à inclusão dos Subitens 6.1.8. e 6.1.9.  
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S RPE 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Garibaldi IO-OI.S.GAR 15 3.7.5.3. 02/08/2024 
 
Referência: 2 / 10 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  4 
3.1. Barramento de 230 kV ................................................................................................................... 4 
3.2. Unidades Geradoras ...................................................................................................................... 4 
3.3. Alteração da Configuração dos Barramentos ................................................................................ 4 
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL ................................ ....................  4 
4.1. Procedimentos Gerais ................................................................................................................... 4 
4.2. Procedimentos Específicos ............................................................................................................ 5 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  5 
5.1. Procedimentos Gerais ................................................................................................................... 5 
5.2. Procedimentos para Recomposição com Autonomia ................................................................... 6 
5.2.1. Preparação da Instalação para a Recomposição com Autonomia .............................. 6 
5.2.2. Recomposição com Autonomia após Desligamento Total da Instalação.................... 6 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 7 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 7 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 7 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 7 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 7 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 8 
6. MANOBRAS DE UNIDADES GERADORAS, DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ........ 8 
6.1. Procedimentos Gerais ................................................................................................................... 8 
6.2. Procedimentos Específicos ............................................................................................................ 9 
6.2.1. Desligamento de Unidades Geradoras e Desenergização de Linhas de Transmissão e 
de Equipamentos ......................................................................................................... 9 
6.2.2. Sincronismo de Unidades Geradoras e Energização de Linhas de Transmissão e de 
Equipamentos .............................................................................................................. 9 
7. NOTAS IMPORTANTES ................................ ................................ ................................ ................ 10 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Garibaldi IO-OI.S.GAR 15 3.7.5.3. 02/08/2024 
 
Referência: 3 / 10 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da UHE Garibaldi, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede.  
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pel o Agente Operador da Instalação , devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando -se a complementariedade 
das ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR-S e a Instalação, de propriedade da RCESA, é realizada pela 
RPE, agente responsável pela operação da Instalação, por intermédio do COG CTG Brasil. 
2.3. As unidades geradoras, equipamentos e linhas de transmissão desta Instalação  fazem parte da Área 
230 kV de Santa Catarina. 
2.4. O COSR-S controla e supervisiona o despacho de geração desta Usina. 
2.5. Esta Usina: 
• é despachada centralizadamente; 
• está conectada na Rede de Operação; 
• não participa do Controle Automático da Geração – CAG; 
• não é de autorestabelecimento; 
• não é fonte para início do processo de recomposição fluente de uma área. 
2.6. Os dados operacionais desta Usina estão descritos no Cadastro de Informações Operacionais da 
respectiva Área Elétrica. 
2.7. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.7.1. A definição da quantidade  de tentativas de religamento manual de linha de transmissão ou 
equipamento, bem como o intervalo entre elas é de responsabilidade do Agente e devem ser 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica.  
2.7.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, este deve definir a necessidade de tentativas adicionais e solicitar  ao COSR-S autorização 
para religamento. Nessa oportunidade, o Agente também pode solicitar alteração no sentido normal 
para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em consideração as condições operativas do 
sistema.  
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Garibaldi IO-OI.S.GAR 15 3.7.5.3. 02/08/2024 
 
Referência: 4 / 10 
 
2.8. Quando caracterizado impedimento de linha de transmissão ou equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação em contingência da respectiva área elétrica. 
2.9.  Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.10. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação , quando o Agente tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1.   BARRAMENTO DE 230 KV 
A configuração do barramento de 230 kV é do tipo Principal e Transferência (Barra P e Barra T). Na 
operação normal desse barramento,  todos os disjuntores e seccionadoras devem estar fechados, 
exceto as seccionadoras de transferência das linhas de transmissão ou equipamentos , bem como, a 
barra de transferência deve estar desenergizada com o disjuntor de transferência aberto e as chaves 
seccionadoras do módulo de transferência fechadas. 
3.2.   UNIDADES GERADORAS 
As unidades geradoras  G1, G2 e G3 13,8 kV estão conectadas ao barramento de 230 kV pelos 
transformadores TE-U1, TE-U2 e TE-U3 13,8/230 kV da UHE Garibaldi, com todas as seccionadoras e 
disjuntores fechados, exceto a seccionadora de transferência do barramento 230 kV. 
 
3.3. ALTERAÇÃO DA CONFIGURAÇÃO DOS BARRAMENTOS 
A mudança de configuração do barramento de 230 kV desta Instalação é executada com controle do 
COSR-S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade do Agente 
Operador da Instalação. 
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S.  
As faixas de controle de tensão para esse barramento estão estabelecidas no Cadastro de Informações 
Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob responsabilidade da Instalação. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Garibaldi IO-OI.S.GAR 15 3.7.5.3. 02/08/2024 
 
Referência: 5 / 10 
 
4.1.3. A Usina deve manter os valores de geração de acordo com os valores programados, constantes no 
Programa Diário de Operação – PDO. Para atendimento dos valores programados constantes no PDO, 
não é necessária a autorização prévia do COSR-S. 
Qualquer alteração no valor de geração da usina em relação ao valor de geração constante no PDO 
somente pode ser executada após autorização do COSR-S. 
As reprogramações de geração quando de necessidades sistêmicas serão executadas pela Usina 
quando solicitadas pelo COSR-S. 
Após reprogramação de geração solicitada pelo COSR-S, o Agente somente poderá alterar a geração 
da Usina com autorização do ONS, inclusive para adoção de valores contidos no PDO. 
4.1.4. Os desvios de geração da usina em relação aos valores previstos no PDO ou em relação às 
reprogramações, devem ser controlados observando -se os valores máximos permitidos explicitados 
na Instrução de Operação IO-CG.BR.01 – Controle da Geração em Condição Normal. 
4.1.5. Quando não existir ou não estiver disponível a supervisão da usina para o ONS, essa deve seguir as 
orientações para envio de dados conforme Rotina Operacional RO-AO.BR.08 – Apuração de Dados de 
Despacho de Geração e de Intercâmbios.  A Usina, deve registrar e informar imediatamente os 
seguintes dados ao COSR-S: 
• movimentação de unidades geradoras hidráulicas (mudança de estado operativo). 
• restrições e ocorrências na usina ou na conexão elétrica que afetem a disponibilidade de geração, 
com o respectivo valor da restrição, contendo o horário de início e término e a descrição do evento. 
• demais informações sobre a operação de suas Instalações, solicitadas pelo ONS. 
4.1.6. O controle de tensão por meio da geração ou absorção de potência reativa pelas unidades geradoras 
da Usina, é controlado pelo COSR-S, com comando e execução pelo Agente Operador da Instalação. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
Não se aplica. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de um desligamento da Instalação , a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação: caracterizado por meio da verificação de ausência de tensão em 
todos os terminais de suas conexões ou ausência de fluxo de potência ativa nessas conexões. 
• Desligamento parcial da Instalação:  qualquer outra configuração que não se enquadre como 
desligamento total. 
5.1.2. Quando de um desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao COSR-
S as informações a seguir: 
• horário da ocorrência; 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Garibaldi IO-OI.S.GAR 15 3.7.5.3. 02/08/2024 
 
Referência: 6 / 10 
 
• configuração da instalação logo após a ocorrência; 
• configuração da instalação após ações realizadas com autonomia pela sua operação. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2, sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificand o a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia d o Agente Operador da Instalação  na 
recomposição, deve ser utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4. 
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO COM AUTONOMIA 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO COM AUTONOMIA 
No caso de desligamento total, o Agente Operador da Instalação deve configurar os disjuntores dos 
seguintes equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores:  
• das linhas de transmissão:  
LT 230 kV Abdon Batista / UHE Garibaldi. 
• das unidades geradoras: 
G1 (disjuntor de 230 kV); 
G2 (disjuntor de 230 kV); 
G3 (disjuntor de 230 kV). 
• do módulo de interligação de barras de 230 kV. 
5.2.2. RECOMPOSIÇÃO COM AUTONOMIA APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar com autonomia os seguintes procedimentos para a 
recomposição: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 RPE 
Receber tensão da SE Abdon Batista, pela LT 230 
Abdon Batista / UHE Garibaldi  e energizar o 
barramento de 230 kV. 
 
1.1 RPE 
Partir a primeira unidade geradora, caso essa 
estivesse operando antes do desligamento, e 
sincronizá-la. 
Manter VGAR-13,8 = 13,8 kV. 
Conforme procedimentos internos 
do Agente.  
Gerar o mínimo necessário para 
manter a unidade geradora 
sincronizada. 
Solicitar autorização ao COSR -S 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Garibaldi IO-OI.S.GAR 15 3.7.5.3. 02/08/2024 
 
Referência: 7 / 10 
 
Passo Executor Procedimentos Condições ou Limites Associados 
para elevar a geração. 
1.2 RPE 
Partir a segunda unidade geradora, caso essa 
estivesse operando antes do desligamento, e 
sincronizá-la. 
Manter VGAR-13,8 = 13,8 kV. 
Conforme procedimentos internos 
do Agente.  
Gerar o mínimo necessário para 
manter a unidade geradora 
sincronizada. 
Solicitar autorização ao COSR -S 
para elevar a geração. 
1.3 RPE 
Partir a terceira unidade geradora, caso essa 
estivesse operando antes do desligamento, e 
sincronizá-la. 
Manter VGAR-13,8 = 13,8 kV. 
Conforme procedimentos internos 
do Agente.  
Gerar o mínimo necessário para 
manter a unidade geradora 
sincronizada. 
Solicitar autorização ao COSR -S 
para elevar a geração. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2, enquanto não 
houver intervenção do COSR -S no processo de recomposição ou interrupção da autonomia da 
operação da instalação na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, 
conforme procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
Para os desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Garibaldi IO-OI.S.GAR 15 3.7.5.3. 02/08/2024 
 
Referência: 8 / 10 
 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
O Agente Operador deve recompor a Instalação conforme Subitem 6.2.2, sem necessidade de 
autorização do ONS.  
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
o Agente Operador deve informar ao COSR-S, para que a recomposição da Instalação seja executada 
com controle do COSR-S. 
6. MANOBRAS DE UNIDADES GERADORAS, DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desligamento programado ou de urgência de unidades geradoras, e 
desenergização programada ou de urgência de linhas de transmissão ou de equipamentos, só 
podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, ou sincronismo 
de unidades geradoras, após desligamentos programados, de urgência ou de emergência, só 
podem ser efetuados com controle do COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, ou para sincronismo de unidades geradoras, após desligamento automático sem 
atuação de proteção que impeça o retorno do equipamento, só podem ser executados com 
autonomia pel o Agente Operador da Instalação  quando estiverem explicitados e estiverem 
atendidas as condições do Subitem 6.2.2. desta Instrução de Operação.  
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, o Agente Operador da Instalação  deve verificar se 
existe tensão de retorno e se a condição de fechamento será em anel. O fechamento em anel só 
pode ser executado com autonomia pel o Agente Operador da Instalação  quando estiver 
especificado nesta Instrução de Operação e estiverem atendidas as condições do Subitem 6.2.2. O 
fechamento de paralelo só pode ser efetuado com controle do COSR-S.  
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou, quando permitida, em sentido inverso, pode ser feita com 
autonomia pelo Agente Operador da Instalação , conforme procedimentos para manobras que 
estão definidos nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização 
controlados pelo COSR-S estão definidos na Instrução de Operação de Preparação para Manobras 
da respectiva área elétrica.  
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Garibaldi IO-OI.S.GAR 15 3.7.5.3. 02/08/2024 
 
Referência: 9 / 10 
 
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de 
intervenções, são de responsabilidade do Agente. 
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador 
é energizado, o Agente Operador da Instalação deve fechá-lo em anel desde que tenha autonomia 
para tal, adotando as condições para o fechamento constantes no Subitem 6.2.2.  
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S.  
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com 
controle do COSR-S. 
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.1.11. A partida de unidades geradoras deve seguir critérios próprios do Agente. O sincronismo à Rede de 
Operação só pode ser executado com autonomia pela operação da Usina quando estiver explicitado 
e estiverem atendidas as condições do Subitem 6.2.2. A tomada d e carga deve ser realizada com 
controle do COSR-S. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESLIGAMENTO DE UNIDADES GERADORAS E DESENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE 
EQUIPAMENTOS 
O desligamento de unidades geradoras e a desenergização de linhas de transmissão ou de 
equipamentos, pertencentes à Rede de Operação, é sempre controlado pelo COSR-S. 
6.2.2. SINCRONISMO DE UNIDADES GERADORAS E ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE 
EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de unidades geradoras, de equipamentos ou de linhas de transmissão.  
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais unidades geradoras, linhas de transmissão e equipamentos em operação, 
conforme explicitado nas condições de energização para a manobra. 
 Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Garibaldi IO-OI.S.GAR 15 3.7.5.3. 02/08/2024 
 
Referência: 10 / 10 
 
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
LT 230 kV Abdon Batista / 
UHE Garibaldi 
Sentido Único: UHE Garibaldi recebe tensão da SE Abdon Batista.  
Ligar, energizando o barramento de 230 
kV, a LT 230 kV Abdon Batista  / UHE 
Garibaldi. 
 
Unidade Geradora G1 ou 
G2 ou G3 13,8 kV 
Partir e sincronizar a unidade geradora. 
 
 
12,4 ≤ VGAR-13,8 ≤ 14,5 kV.  
Conforme procedimentos internos 
do Agente.  
Gerar o mínimo necessário para 
manter a unidade geradora 
sincronizada. 
Elevar a geração da unidade geradora. Após autorização do COSR-S. 
7. NOTAS IMPORTANTES 
Não se aplica. 
