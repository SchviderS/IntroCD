 
Manual de Procedimentos da Operação 
 
 
Referência Técnica 
Elaboração de Ajustamentos Operativos 
 
 
Código Revisão Item Vigência 
RT-AO.BR 06 7.10. 20/08/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Adequação do Subitem 3.8., incluindo o subitem 5.2.3. 
- Adequação do subitem 3.16, incluindo observação para usinas associadas. 
- Ajustes de texto ao longo do documento. 
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-SE COSR-NE COSR-NCO COSR-S  
 
Manual de Procedimentos da Operação 
 Referência Técnica Código Revisão Item Vigência 
Elaboração de Ajustamentos Operativos RT-AO.BR 06 7.10. 20/08/2024 
 
Referência:  2 / 14 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. PREMISSAS ................................ ................................ ................................ ................................ ... 3 
3. ESTRUTURA DOS AJUSTAMENTOS OPERATIVOS ................................ ................................ ............ 4 
3.1. FOLHA ROSTO COM O MOTIVO DA REVISÃO, LISTA DE DISTRIBUIÇÃO E ÍNDICE ......................... 4 
3.2. Item 1. OBJETIVO ........................................................................................................................... 4 
3.3. Item 2. CONSIDERAÇÕES GERAIS .................................................................................................. 4 
3.4. Item 3. RELACIONAMENTO OPERACIONAL ................................................................................... 5 
3.5. Item 4. DIAGRAMA UNIFILAR ........................................................................................................ 6 
3.6. Item 5. PROCEDIMENTOS OPERATIVOS ........................................................................................ 6 
3.7. Subitem 5.1. CONFIGURAÇÕES DE OPERAÇÃO ............................................................................. 6 
3.8. Subitem 5.2. CONTROLE DE TENSÃO E CARREGAMENTO ............................................................. 7 
3.9. Subitem 5.3. CONTROLE DE GERAÇÃO .......................................................................................... 7 
3.10. Subitem 5.4. RECOMPOSIÇÃO ....................................................................................................... 8 
3.11. Subitem 5.5. OPERAÇÃO EM CONTINGÊNCIA ............................................................................... 8 
3.12. Subitem 5.6. MANOBRAS DE DESENERGIZAÇÃO E ENERGIZAÇÃO DE EQUIPAMENTOS .............. 9 
3.13. Subitem 5.7. ESQUEMAS ESPECIAIS ............................................................................................ 10 
3.14. Subitem 5.8. SISTEMAS DE SUPERVISÃO ..................................................................................... 10 
3.15. Item 6. INTERVENÇÕES ................................................................................................................ 10 
3.16. Item 7. DADOS E PROCEDIMENTOS ESPECÍFICOS DOS CONJUNTOS .......................................... 11 
3.17. Item 8. NOTAS IMPORTANTES ..................................................................................................... 14 
3.18. Item 9. ANEXO ............................................................................................................................. 14 
  
Manual de Procedimentos da Operação 
 Referência Técnica Código Revisão Item Vigência 
Elaboração de Ajustamentos Operativos RT-AO.BR 06 7.10. 20/08/2024 
 
Referência:  3 / 14 
 
1. OBJETIVO 
Estabelecer a referência para elaboração dos Ajustamentos Operativos – AOs entre o ONS e os Agentes, 
visando sua estruturação e padronização, conforme os Procedimentos de Rede. 
2. PREMISSAS 
2.1. Os AOs são documentos estabelecidos entre o ONS e Agentes, para a definição de ações e requisitos 
específicos aos relacionamentos operacionais e procedimentos para a operação de equipamentos , 
instalações, usinas ou conjuntos de usinas fora da Rede de Operação, porém importantes para as 
atividades de coordenação, supervisão e controle da Rede de Operação. 
2.2. Os AOs são elaborados para atender as necessidades da Rede de Operação, estabelecendo ações em 
equipamentos, instalações, usinas ou conjuntos de usinas fora da Rede de Operação que causam 
influência significativa no processo de coordenação, supervisão e controle da Rede de Operação. 
2.3. Devem constar nos AOs as ações, procedimentos e requisitos específicos de responsabilidade do s 
Agentes para os equipamentos fora da Rede de Operação  e para os quais existe a necessidade de 
coordenação e controle do ONS. 
2.4. Os conceitos importantes para o esclarecimento dos procedimentos operacionais contidos nos AOs, 
devem ser relacionados no item de considerações gerais.  
2.5. Os itens desta Referência Técnica, para os quais não existam procedimentos, não devem constar nos 
AOs. 
2.6. Devem ser elaborados AOs para: 
• usinas fora da Rede de Operação, que exportam energia para os países vizinhos; 
• conjuntos de usinas; 
• usinas Tipo II-B, caso existam procedimentos definidos nos insumos fornecidos pelas gerências da 
engenharia e do planejamento do ONS e o processo de classificação para essa modalidade esteja 
concluído; 
• usinas fora da Rede de Operação com influência significativa na Rede de Operação; 
• equipamentos ou instalações que não pertençam à Rede de Operação, caso sua influência seja 
significativa na Rede de Operação ou existam requisitos ou configurações operativas que 
interfiram nas ações de coordenação e controle da Rede de Operação. 
2.7. As denominações a serem dad as para os AO s devem se restringir ao nome dos equipamento s, da 
instalação, das usinas ou do conjunto de usinas para os quais o AO se aplica. 
Exemplos: 
• Operação da UTE (nome da usina); 
• Operação da UHE (nome da usina); 
• Operação dos Conjuntos Eólicos da (nome da Área Elétrica); 
• Operação dos Conjuntos Fotovoltaicos da (nome da Área Elétrica); 
• Operação dos Conjuntos Termelétricos da (nome da Área Elétrica); 
Manual de Procedimentos da Operação 
 Referência Técnica Código Revisão Item Vigência 
Elaboração de Ajustamentos Operativos RT-AO.BR 06 7.10. 20/08/2024 
 
Referência:  4 / 14 
 
• Operação dos Conjuntos Hidráulicos da (nome da Área Elétrica); 
• Operação dos Conjuntos Híbridos da (nome da Área Elétrica); 
• Operação do (nome do anel, eixo ou trecho); 
• Operação do Banco de Capacitores (nível de tensão) kV da SE (nome da Subestação); 
• Operação do Transformador (nível de tensão) kV da UTE (nome da Usina); 
• Operação de Equipamentos do Agente (este caso envolve vários equipamentos de um mesmo 
Agente). 
2.8. A numeração dos AOs deve estar de acordo com a Rotina Operacional RO-MP.BR.01. 
2.9. Os textos destacados em cor verde, constantes nos itens da estrutura desta Referência Técnica, não 
devem fazer parte do s AOs, pois se referem apenas a orientações para elaboração dos respectivos 
documentos. 
2.10. Os exemplos destacados em itálico, constantes nos itens da estrutura do AO, servem como exemplos 
orientativos para o uso da fraseologia e utilização de textos simples e objetivos. 
2.11. Se necessário, podem ser utilizadas cores ou figuras para facilitar o entendimento dos procedimentos 
e recomendações dos AOs. 
3. ESTRUTURA DOS AJUSTAMENTOS OPERATIVOS 
A estrutura dos AOs está definida a seguir. Essa estrutura deve ser utilizada e serve como ref erência para 
elaboração dos AOs. 
3.1. FOLHA ROSTO COM O MOTIVO DA REVISÃO, LISTA DE DISTRIBUIÇÃO E ÍNDICE 
Deve ser seguido modelo definido pela Referência Técnica RT-MP.BR.02 – Padronização de Redação e Estilo 
dos Documentos do MPO. 
3.2. ITEM 1. OBJETIVO 
O texto deste Item deve ser: 
Estabelecer procedimentos a serem seguidos pelo Operador Nacional do Sistema Elétrico – ONS e  pelos 
Agente(s) para a operação  do(a)(s) (linhas de transmissão ou equipamentos da SE, UHE, UTE, Conjuntos 
Eólicos da Área (nome da área elétrica) ou Conjuntos Fotovoltaicos da da Área (nome da área elétrica)), não 
pertencente(s), porém com reflexos significativos para a Rede de Operação, de acordo com os Procedimentos 
de Rede. 
3.3. ITEM 2. CONSIDERAÇÕES GERAIS 
Neste Item constam assuntos  que podem ser comuns aos Ajustamentos Operativos, podendo ser 
complementados ou suprimidos em função de suas particularidades. 
2.1. Este Ajustamento Operativo deve ser utilizado pelos Agentes ou os procedimentos aqui contidos podem 
ser contemplados em documentos operativos próprios dos Agentes. 
2.2. A influência do(s) ou da(as) (linhas de transmissão ou equipamentos da SE, UHE, UTE ou Conjunto) para 
a Rede de Operação é verificada quanto (explicar onde se dá a influência dos equipamentos objetos do AO). 
Manual de Procedimentos da Operação 
 Referência Técnica Código Revisão Item Vigência 
Elaboração de Ajustamentos Operativos RT-AO.BR 06 7.10. 20/08/2024 
 
Referência:  5 / 14 
 
Quando se tratar de conjunto de Área Elétrica, o seguinte subitem deve ser incluído no AO: 
2.2. As características, usinas, representantes, interlocutores com o ONS e a influência na Rede de Operação 
de cada conjunto da (nome da Área Elétrica) estão listados em item específico deste Ajustamento Operativo. 
Quando se tratar de usina ou de conjunto de usinas, o seguinte subitem deve ser incluído no AO: 
2.3. Este Ajustamento Operativo tem prazo de validade indeterminado, podendo ser revisado nos casos em 
que as condições da Rede de Operação ou da instalação do Agente sejam alteradas. O processo de 
implantação de revisões deste Ajustamento Operativo é realizado por meio eletrônico. 
3.4. ITEM 3. RELACIONAMENTO OPERACIONAL 
Este Item deve descrever os aspectos específicos quanto  ao envolvimento de pessoal responsável para as 
tratativas entre a Gerência de Programação da Operação, o Centro de Operação, a Gerência de 
Procedimentos Operativos e a Gerência de Apuração, Análise e Custos da Operação do ONS e dos Agentes.  
Formato texto: 
3.1. O Agente (nome do agente responsável pela operação  de LT / equipamentos ) é o responsável para 
exercer as atividades  (quando aplicável) de Programação da Operação; Procedimentos Operativos; 
Integração de Obras; Autorrestabelecimento; Tempo Real ; Apuração, Análise e Custos da Operação ; 
Segurança Cibernética; Telecomunicações e Sistema de Supervisão e Controle com o ONS. 
Formato texto, quando os agentes nomearem um agente representante e interlocutor: 
3.1. O agente  (nome do agente responsável pela operação  de LT / equipamentos ) é o representante e 
interlocutor do(a) (linhas de transmissão ou equipamentos da SE, UHE  (CEG GGG.FF.UF.999999-D.VV), UTE 
(CEG GGG.FF.UF.999999-D.VV) ou Conjunto), nomeado pelos demais Agentes para as atividades , (quando 
aplicável) de Programação da Operação; Procedimentos Operativos; Integração de Obras; 
Autorrestabelecimento; Tempo Real ; Apuração, Análise e Custos da Operação ; Segurança Cibernética; 
Telecomunicações e Sistema de Supervisão e Controle com o ONS 
Quando se tratar de conjunto de Área Elétrica, o seguinte subitem deve ser incluído no AO: 
3.1. Os responsáveis para exercer as atividades  (quando aplicável)  de Programação da Operação; 
Procedimentos Operativos; Integração de Obras; Autorrestabelecimento; Tempo Real; Apuração, Análise e 
Custos da Operação ; Segurança Cibernética; Telecomunicações e Sistema de Supervisão e Controle  com o 
ONS estão listados em item específico deste Ajustamento Operativo. 
Formato tabela: 
3.1. O(s) ou A(s) (linhas de transmissão ou equipamentos da SE, UHE  ou UTE) tem(têm) seu relacionamento 
operacional, para as atividades (quando aplicável) de Programação da Operação; Procedimentos Operativos; 
Integração de Obras; Autorrestabelecimento; Operação em Tempo Real ; Apuração, Análise e Custos da 
Operação; Segurança Cibernética; Telecomunicações e Sistema de Supervisão e Controle  com o ONS, 
conforme segue: 
Usina / Instalação Agente de Operação Agente Operador Centro de Operação 
(nome da usina) 
(CEG 
GGG.FF.UF.999999-
D.VV) 
(nome do agente de 
operação) 
(nome do agente 
operador) 
(nome do centro de 
operação) 
3.2. O(s) Agente(s) Operador(es) deverá(ão) dispor de equipes em regime de turno ininterrupto para 
Manual de Procedimentos da Operação 
 Referência Técnica Código Revisão Item Vigência 
Elaboração de Ajustamentos Operativos RT-AO.BR 06 7.10. 20/08/2024 
 
Referência:  6 / 14 
 
comunicação em tempo real com o COSR-X. 
3.3. As tratativas e informações operacionais do ONS para a operação são efetuadas em tempo real, entre o 
COSR-X e o Agente.  
Quando se tratar de usina que possui período de entressafra, o Subitem 3.3 deve ter a seguinte redação: 
3.3. As tratativas e informações  operacionais para comunicação em tempo real devem ser efetuadas em 
regime de turno ininterrupto, podendo, nos períodos de entressafra, o agente formalizar ao COSR-X que não 
disporá desse regime, uma vez que a usina estará fora de operação. Para tal formalização, o agente deverá 
enviar correspondência ao COSR-X, informando as datas do início e do final da interrupção. 
3.4. As tratativas e informações com as áreas (quando aplicável)  de Programação da Operação; 
Procedimentos Operativos; Integração de Obras; Autorrestabelecimento;  Apuração, Análise e Custos da 
Operação; Segurança Cibernética; Telecomunicações e Sistema de Supervisão e Controle  serão efetuados 
durante o horário comercial. 
3.5. O ONS e o Agente devem informar e manter atualizado s os nomes e demais dados referentes ao  
relacionamento operacional, conforme definido na Rotina Operacional RO-RO.BR.02. 
3.5. ITEM 4. DIAGRAMA UNIFILAR 
O(s) diagrama(s) unifilar(es) d o(s) (ou da(s)) (linhas de transmissão ou equipamentos da SE, UHE, UTE ou 
Conjuntos Eólicos da Área (nome da área) ou Conjuntos Fotovoltaicos da Área (nome da área) ) deve(m) ser 
mantido(s) atualizado(s) e ser (em) disponibilizado(s) para o ONS, conforme Rotina Operacional RO-
MP.BR.05. 
3.6. ITEM 5. PROCEDIMENTOS OPERATIVOS 
Neste Item devem ser explicitados os procedimentos operativos e requisitos específicos que interferem na 
coordenação, supervisão e controle da Rede de Operação, conforme Procedimentos de Rede.  
Necessariamente devem constar nos AO s as ações, os procedimentos e os requisitos específicos de 
responsabilidade dos Agentes, para os quais existe a necessidade de coordenação  e controle do ONS, no 
sentido de atender as necessidades de coordenação, supervisão e controle da Rede de Operação. 
Os subitens a serem considerados são os seguintes: 
3.7. SUBITEM 5.1. CONFIGURAÇÕES DE OPERAÇÃO 
Devem constar neste Subitem os aspectos relacionados à configuração de subestações , usinas ou da rede 
que interliga o equipamento , usina ou conjunto de usinas à Rede de Operação, que interferem na 
coordenação, supervisão e controle da Rede de Operação. 
Para usinas devem ser descritos os dados relativos às unidades geradoras e ao ponto de conexão. 
Exemplo 1:  
5.1.1. A Usina (nome da usina) possui capacidade instalada de (montante de geração em MW). 
5.1.2. Em condições normais de operação, a Usina (nome da usina) está conectada à SE (nome da subestação), 
por meio da LT (nome da linha). A SE (nome da subestação) está conectada à SE (nome da subestação), por 
meio da LT (nome da linha). 
5.1.3. A Usina (nome da usina) está conectada à Rede de Operação no barramento (nível de tensão) kV da SE 
(nome da subestação). 
Exemplo 2: 
O (equipamento da SE ou linhas de transmissão) opera em condição .... (informar a condição de operação). 
Manual de Procedimentos da Operação 
 Referência Técnica Código Revisão Item Vigência 
Elaboração de Ajustamentos Operativos RT-AO.BR 06 7.10. 20/08/2024 
 
Referência:  7 / 14 
 
3.8. SUBITEM 5.2. CONTROLE DE TENSÃO E CARREGAMENTO  
Neste Subitem devem ser detalhados os procedimentos a serem executados pela operação do Agente para 
utilização dos recursos de controle de tensão e carregamento que interferem na coordenação, supervisão e 
controle da Rede de Operação. Devem ser destacados os procedimentos que devem ser executados sob 
controle (após autorização) do COSR-X do ONS. 
Exemplos: 
5.2.1. Nas ações de controle de tensão e de carregamento na Rede de Operação, o COSR-X poderá solicitar a 
utilização dos recursos do(s) Conjunto / Usina. 
5.2.2. O controle de tensão, por meio de fornecimento / absorção de potência reativa no Conjunto / Usina, é 
comandado e executado pelo agente operador. 
Incluir o texto abaixo, quando aplicável, para os casos de conjuntos fotovoltaicos.  
5.2.3. Os conjuntos devem zerar a injeção de potência reativa, no ponto de conexão à rede básica, nos 
períodos de geração de potência ativa nula. 
 
3.9. SUBITEM 5.3. CONTROLE DE GERAÇÃO 
Para usinas ou conjuntos de usinas , devem ser detalhados os procedimentos relacionados aos despachos e 
reprogramação de geração a serem executados pela operação do Agente  que interferem na coordenação, 
supervisão e controle da Rede de Operação. Devem ser destacados os procedimentos que devem ser 
executados sob controle (após autorização) do Centro de Operação do Sistema do ONS. 
Exemplos: 
5.3.1. O COSR-X controla e supervisiona a geração do(s) Conjuntos / Usina no ponto de conexão. 
5.3.2. Os Conjuntos Eólicos devem maximizar os valores de geração de acordo com a disponibilidade de vento 
e com a sua capacidade instalada , respeitando alterações solicitadas pelo COSR -X e possíveis restrições de 
geração. 
OU 
5.3.2. Os Conjuntos Fotovoltaicos devem maximizar os valores de geração de acordo com a disponibilidade 
solar e com a sua capacidade instalada, respeitando alterações solicitadas pelo COSR-X e possíveis restrições 
de geração. 
5.3.3. Os Conjuntos Eólicos / Conjuntos Fotovoltaicos  / Usina deve(m) atender, com a maior brevidade 
possível, a solicitação do COSR -X para redespacho de geração, em caso de necessidade de atendimento a 
situações de restrições elétricas da Rede de Operação. 
Para usinas: 
5.3.4. O Agente Operador deve registrar e informar imediatamente os seguintes dados ao COSR-X: 
• restrições e ocorrências na Usina e nos pontos de conexão que afetaram a disponibilidade de geração , 
informando o valor da restrição, os horários de início e de término e a descrição do evento; 
• demais informações sobre a operação de suas instalações, solicitadas pelo COSR-X.  
Para conjuntos: 
5.3.4. O Agente Operador deve registrar e informar imediatamente os seguintes dados ao COSR-X: 
• restrições e ocorrências no Conjunto e nos pontos de conexão que afetaram a disponibilidade de geração, 
que resultarem em indisponibilidade superior a 10% da capacidade instalada total . Deverá ser 
Manual de Procedimentos da Operação 
 Referência Técnica Código Revisão Item Vigência 
Elaboração de Ajustamentos Operativos RT-AO.BR 06 7.10. 20/08/2024 
 
Referência:  8 / 14 
 
informando o valor da restrição, contendo os horários de início e de término e a descrição do evento; 
• demais informações sobre a operação de suas instalações, solicitadas pelo COSR-X. 
Incluir o texto abaixo, quando aplicável, para os casos de conjuntos hidráulicos e térmicos.  
5.3.5. A(s) usina(s) hidrelétrica(s) / usina(s) termelétrica(s) deve(m) manter os valores de geração de acordo 
com os valores programados, constantes no Programa Diário de Operação – PDO. Para atendimento dos 
valores programados constantes no PDO, não é necessária a autorização prévia do COSR-X.  
Qualquer alteração no valor de geração da (s) usina(s) em relação ao constante no PDO somente pode ser 
executada após autorização do COSR-X. 
Após reprogramação de geração solicitada pelo ONS, o(s) Agente(s) somente poderá alterar a geração da(s) 
Usina(s) com autorização do ONS, inclusive para adoção de valores contidos no PDO. 
3.10. SUBITEM 5.4. RECOMPOSIÇÃO 
Neste Subitem devem ser detalhados os procedimentos de recomposição a serem executados pela operação 
do Agente  que interferem na coordenação, supervisão e controle da Rede de Operação. Devem ser 
destacados os procedimentos que devem ser executados sob controle  (ou após autorização) do Centro de 
Operação do ONS. 
5.4.1. No caso de desligamento total , caracterizado pela ausência de tensão em todos os terminais de suas 
linhas de transmissão, o Agente Operador deverá preparar a Instalação para recomposição. 
5.4.2. O Agente deve restabelecer os equipamentos conforme instruções próprias e informar ao COSR-X: 
• logo após a ocorrência: o horário da ocorrência e as condições dos equipamentos; 
• logo após a normalização dos equipamentos: o horário da normalização e as condições dos equipamentos. 
Quando se tratar de usina ou conjunto de usinas, o seguinte subitem deve ser incluído no AO: 
5.4.3. A elevação de geração d o Conjunto / Usina, após desligamentos automáticos de equipamentos que 
impediram ou restringiram sua geração, somente pode ser realizada após autorização do COSR-X. 
3.11. SUBITEM 5.5. OPERAÇÃO EM CONTINGÊNCIA 
Neste subitem devem ser detalhados os procedimentos a serem executados pela operação do Agente para 
a indisponibilidade de equipamentos que interferem na coordenação, supervisão e controle da Rede de 
Operação. Devem ser destacados os procedimentos que devem ser executados sob controle (ou após 
autorização) do Centro de Operação do ONS. 
Exemplo 1: 
5.5.1. – Indisponibilidade da LT (nome da linha) 
Manual de Procedimentos da Operação 
 Referência Técnica Código Revisão Item Vigência 
Elaboração de Ajustamentos Operativos RT-AO.BR 06 7.10. 20/08/2024 
 
Referência:  9 / 14 
 
Passo 
Coordenação 
Controle 
Comando / 
Execução 
Procedimento Objetivo / Item de Controle  
1 
--- 
COSR-X 
--- Limitar a geração em (montante de 
geração, em MW). 
Evitar sobrecarga no circuito remanescente da LT 
(nome da linha de transmissão). 
Exemplo 2: 
5.5.1 – Indisponibilidade do Filtro (nome do filtro) 
Passo 
Coordenação 
Controle 
Comando / 
Execução 
Procedimento Objetivo / Item de Controle  
1 
--- 
COSR-X 
--- Limitar a geração em (montante de 
geração, em MW). 
- Evitar violação limites de de distorções 
harmônicas. 
- Evitar sobrecarga no filtro remanescente. 
3.12. SUBITEM 5.6. MANOBRAS DE DESENERGIZAÇÃO E ENERGIZAÇÃO DE EQUIPAMENTOS 
Neste Subitem devem ser detalhados os procedimentos a serem executados pela operação do Agente para 
manobra de equipamentos que interferem na coordenação, supervisão e controle da Rede de Operação. 
Devem ser destacados os procedimentos que devem ser executados sob controle (ou após autorização) do 
Centro de Operação do ONS. 
Exemplo: 
Para usina: 
5.6.1. A desenergização de equipamentos que impeça ou restrinja a geração d o Conjunto / Usina deve ser 
comunicada em tempo real ao COSR-X. 
Para conjuntos: 
5.6.1. A desenergização de equipamentos que impeça ou restrinja a geração d o Conjunto em valores 
superiores a 10% da sua capacidade instalada total deve ser comunicada em tempo real ao COSR-X. 
5.6.2. Os procedimentos de segurança a serem adotados, quando da ocorrência de desligamentos imprevistos 
de equipamento s que esteja m sendo submetido s a intervenç ões, são de responsabilidade d o agente de 
operação ou responsável pela operação do equipamento. 
5.6.3. A partida e a sincronização de unidades geradoras ou a energização de linhas de transmissão 
associadas ao Conjunto / Usina devem ser realizadas conforme instruções próprias do Agente. 
Manual de Procedimentos da Operação 
 Referência Técnica Código Revisão Item Vigência 
Elaboração de Ajustamentos Operativos RT-AO.BR 06 7.10. 20/08/2024 
 
Referência:  10 / 14 
 
3.13. SUBITEM 5.7. ESQUEMAS ESPECIAIS 
Neste Subitem devem ser detalhados os esquemas especiais  e seus procedimentos associados que 
interferem na coordenação, supervisão e controle da Rede de Operação. 
3.14. SUBITEM 5.8. SISTEMAS DE SUPERVISÃO 
Neste subitem devem ser detalhados os procedimentos a serem adotados pelo Agente em função da 
indisponibilidade para o ONS da supervisão de equipamentos  que interferem na coordenação, supervisão e 
controle da Rede de Operação. 
Exemplo: 
5.8.1. Quando a supervisão do Conjunto / Usina ou do seu ponto de conexão não estiver disponível  para o 
ONS, a operação da instalação deve registrar e informar ao COSR-X, no dia seguinte, a geração média horária 
(em MWh/h) e a disponibilidade verificadas nas 24 horas do dia anterior. 
5.8.2. A supervisão do Conjunto / Usina e do seu ponto de conexão é de responsabilidade do Agente. 
3.15. ITEM 6. INTERVENÇÕES 
Neste Item devem ser detalhados as instalações e equipamentos para os quais exista algum processo 
específico para programação e execução de intervenções. 
6.1. As intervenções nos equipamentos do Conjunto / Usina serão informadas pelo Agente Operador na fase 
de programação, sendo contempladas no Programa Diário de Operação – PDO. 
6.2. As tratativas para a programação de intervenções serão efetuadas entre a Área de Programação do 
Agente e a Área de Programação do ONS até às 15 horas do último dia útil que antecede o dia da intervenção. 
Havendo necessidade de realizar intervenções após às 15 horas do último dia útil que antecede o dia da 
intervenção, as tratativas serão efetuadas entre a Área de Operação em T empo Real do Agente e ao 
Operação em Tempo Real do COSR-X. 
Deve ser incluído o subitem a seguir para o caso de conjunto: 
6.3. As intervenções, seja m em unidades geradoras ou nas instalações de transmissão de uso exclusivo do 
Conjunto, que resultarem em indisponibilidade superiores a 10% da capacidade instalada total, deverão ser 
cadastradas no Sistema de Gestão de Intervenções – SGI.
Manual de Procedimentos da Operação - Módulo 10 
 Referência Técnica Código Revisão Item Vigência 
Elaboração de Ajustamentos Operativos RT-AO.BR 06 7.10. 20/08/2024 
 
Referência:  11 / 14 
 
3.16. ITEM 7. DADOS E PROCEDIMENTOS ESPECÍFICOS DOS CONJUNTOS 
Deve ser incluído este Item para o caso de ajustamento operativo de conjuntos de uma área elétrica. 
Este Item relaciona (quando aplicável) os conjuntos, cada usina que compõe os conjuntos, Agentes de Operação, Agentes Operadores, Centros de Operação, pontos 
de conexão, quantidade de unidades geradoras/inversores, quantidade de grupos, capacidade instalada e modos de controle. 
Deve ser incluído um Subitem para cada conjunto, em ordem alfabética, seguindo um dos exemplos a seguir. As colunas da tabela podem ser excluídas quando não 
forem aplicáveis. 
7.1. Conjunto (nome do conjunto) 
Usinas Agente de Operação Agente 
Operador 
Centro de 
Operação do 
Agente 
Operador 
Ponto de Conexão (1) Modo de controle 
(nome da usina) 
(CEG: GGG.FF.UF.999999-
D.VV) 
(nome do agente de 
operação) 
(nome do 
agente 
operador) 
(nome do 
centro) 
(ponto de conexão) 
 
Ponto de influência na 
rede de operação:  
(ponto de influência) 
(preenchimento de 
acordo com a nota 1) (*) 
Disponíveis: 
 
Operar: 
 
Referência:  
(1) Para conjuntos eólicos: quantidade de aerogeradores / capacidade instalada (em MW); 
para conjuntos fotovoltaicos: quantidade de inversores / capacidade instalada (em MW); 
Incluir o texto do asterisco a seguir, caso as usinas do conjunto sejam associadas às usinas de outro conjunto: 
(*) As usinas do Conjunto (nome do conjunto) são associadas às usinas do Conjunto (nome do conjunto), com um MUST contratado total de XXX MW. 
  
Manual de Procedimentos da Operação - Módulo 10 
 Referência Técnica Código Revisão Item Vigência 
Elaboração de Ajustamentos Operativos RT-AO.BR 06 7.10. 20/08/2024 
 
Referência:  12 / 14 
 
7.1. Conjunto (nome do conjunto) 
Usinas Agente de Operação  
Agente 
Operador 
Centro de 
Operação do 
Agente 
Operador 
Ponto de conexão (1) Modo de controle 
(nome da usina) 
(CEG: GGG.FF.UF.999999-
D.VV) 
(nome do agente de 
operação) 
(nome do 
agente 
operador) (nome do 
centro) 
(ponto de conexão) 
 
Ponto de influência na 
rede de operação:  
(ponto de influência) 
(preenchimento de 
acordo com a nota 1) (*) Disponíveis: 
 
Operar: 
 
Referência:  
(nome da usina) 
(CEG: GGG.FF.UF.999999-
D.VV) 
(nome do agente de 
operação) 
(nome do 
agente 
operador) 
(preenchimento de 
acordo com a nota 1) (*) 
Total do Conjunto XXX: (preenchimento de acordo com a nota 2) 
(1) Para usinas eólicas: quantidade de aerogeradores / capacidade instalada (em MW); 
para usinas fotovoltaicas: quantidade de inversores / capacidade instalada (em MW); 
 
(2) Para conjuntos eólicos: quantidade de aerogeradores / capacidade instalada (em MW); 
para conjuntos fotovoltaicos: quantidade de inversores / capacidade instalada (em MW); 
Incluir o texto do asterisco a seguir, caso as usinas do conjunto sejam associadas às usinas de outro conjunto: 
(*) As usinas do Conjunto (nome do conjunto) são associadas às usinas do Conjunto (nome do conjunto), com um MUST contratado total de XXX MW. 
 
  
Manual de Procedimentos da Operação - Módulo 10 
 Referência Técnica Código Revisão Item Vigência 
Elaboração de Ajustamentos Operativos RT-AO.BR 06 7.10. 20/08/2024 
 
Referência:  13 / 14 
 
 
7.1. Conjunto (nome do conjunto) 
Usinas Agente de Operação  
Agente 
Operador 
Centro de 
Operação do 
Agente 
Operador 
Ponto de conexão (1) 
(nome da usina) (CEG 
GGG.FF.UF.999999-D.VV) (nome do agente de operação) 
(nome do 
agente 
operador) (nome do 
centro) 
(ponto de conexão) 
 
Ponto de influência na 
rede de operação:  
(ponto de influência) 
(preenchimento de acordo com a nota 1) (*) 
(nome da usina) (CEG 
GGG.FF.UF.999999-D.VV) (nome do agente de operação) 
(nome do 
agente 
operador) 
(preenchimento de acordo com a nota 1) (*) 
Total do Conjunto XXX: (preenchimento de acordo com a nota 2) 
(1) Para usinas termelétricas: quantidade de geradoras / capacidade instalada (em MW). 
Para usinas hidrelétricas: quantidade de geradores / capacidade instalada (em MW); 
 
(2) Para conjuntos termelétricos: quantidade de geradores / capacidade instalada (em MW); 
para conjuntos hidrelétricos: quantidade de geradores / capacidade instalada (em MW); 
Incluir o texto do asterisco a seguir, caso as usinas do conjunto sejam associadas às usinas de outro conjunto: 
(*) As usinas do Conjunto (nome do conjunto) são associadas às usinas do Conjunto (nome do conjunto), com um MUST contratado total de XXX MW. 
 
 
 
Manual de Procedimentos da Operação - Módulo 10 
 Referência Técnica Código Revisão Item Vigência 
Elaboração de Ajustamentos Operativos RT-AO.BR 06 7.10. 20/08/2024 
 
Referência:  14 / 14 
 
3.17. ITEM 8. NOTAS IMPORTANTES 
Neste Item devem ser explicitados procedimentos operativos pertinentes a outros assuntos ligados à 
operação do Agente e ao ONS e que não foram contemplados nos itens anteriores. 
Em caso de não haver notas importantes, o Item deve ser suprimido. 
3.18. ITEM 9. ANEXO 
A inclusão de anexos no Ajustamento Operativo deve se restringir a uma necessidade para casos de extrema 
excepcionalidade ou relação de equipamentos abrangidos pelo ajustamento . Quando presente, o título do 
Anexo deve conter a especificação do equipamento e instalação. 
Em caso de não haver Anexo, o Item deve ser suprimido. 
 
