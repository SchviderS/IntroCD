Manual de Procedimentos da Operação
Referência Técnica
Conceitos e entendimento da Região de Segurança para uso em Tempo Real
Código Revisão Item Vigência
RT-CT.BR.01 01 7.2. 22/07/2022
.
MOTIVO DA REVISÃO
Alterados os subitens 2.1, 2.10, 2.11, 3.1, 3.3 em adequação à reestruturação dos Procedimentos de Rede;
Ajuste de texto e formatação ao longo do documento.
LISTA DE DISTRIBUIÇÃO
CNOS COSR-SE COSR-NE COSR-NCO COSR-S
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Conceitos e entendimento da Região de Segurança 
para uso em Tempo Real RT-CT.BR.01 01 7.2. 22/07/2022
Referência: 2/ 8
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. CONCEITOS ......................................................................................................................................3
3. CONSIDERAÇÕES GERAIS .................................................................................................................4
4. DETERMINAÇÃO DA REGIÃO DE SEGURANÇA ..................................................................................5
5. INTERPRETAÇÃO DA REGIÃO DE SEGURANÇA ..................................................................................6
5.1. Representação dos eixos do gráfico..............................................................................................6
5.2. Representações em cruz azul ........................................................................................................6
5.3. Indicação de números nos contornos ...........................................................................................7
5.4. Delimitações em Contorno Verde .................................................................................................7
5.5. Delimitações em Contorno Vermelho ...........................................................................................7
5.6. Área explorada em Região Verde..................................................................................................7
5.7. Área explorada em Região Hachurada ..........................................................................................7
5.8. Área explorada em Região Amarela ..............................................................................................7
5.9. Área explorada em Região Vermelha............................................................................................7
6. BIBLIOGRAFIA ..................................................................................................................................8
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Conceitos e entendimento da Região de Segurança 
para uso em Tempo Real RT-CT.BR.01 01 7.2. 22/07/2022
Referência: 3/ 8
1. OBJETIVO
Apresentar as informações necessárias para a compreensão das regiões de segurança, como ferramenta de 
apoio à decisão da Operação em Tempo Real.
2. CONCEITOS
2.1.Região de Segurança: resultado da exploração de pontos de operação, a partir da condição 
atualizada de operação, onde possam ocorrer violações de limites e critérios operativos, conforme 
definidos nos Cadastros de Informações Operacionais de Limites de equipamentos da Rede de 
Operação, nas Instruções de Operação e nos Procedimentos de Rede. 
2.2.Nomograma: projeção da região de segurança em gráficos bidimensionais. 
2.3.Direções: representação do deslocamento do ponto de operação, indicando a variação dos grupos 
de geração. 
2.4.Grupos de Geração: grupos de usinas em que é efetuada a variação do seu despacho entre valores 
máximos e mínimos do grupo, permitindo a excursão do ponto de operação, em um número pré-
definido de direções.
2.5.Conjunto de Contingências: lista de contingências pré-estabelecidas de linhas de transmissão e 
equipamentos para simulação da região de segurança.
2.6.Limite de geração: limite verificado pela região de segurança onde pelo menos um dos grupos de 
geração atingiu seu valor máximo ou mínimo de excursão do despacho de geração.
2.7.Limite de estabilidade de tensão (curva do nariz): limite verificado pela região de segurança em 
que ocorreu o esgotamento do suporte de potência reativa, explicitando que o sistema atingiu seu 
ponto máximo de carregamento.
2.8.Limite de convergência: limite verificado pela região de segurança onde não foi obtida a 
convergência do fluxo de potência continuado, indicando um ponto de máximo carregamento do 
sistema.
2.9.Limite térmico: limite verificado pela região de segurança onde ocorre a violação de carregamento 
de algum equipamento ou linha de transmissão.
2.10.Limite de tensão: limite verificado pela região de segurança onde ocorreu a violação de tensão, 
conforme estabelecido no Submódulo 2.3 dos Procedimentos de Rede.
2.11.Limite de estabilidade: limite verificado pela região de segurança onde ocorreu a violação dos 
critérios de estudos elétricos para simulações dinâmicas, conforme estabelecido no Submódulo 2.3 
dos Procedimentos de Rede.
2.12.Limite de segurança: limite verificado pela região de segurança onde pelo menos uma contingência 
provoca perda de estabilidade do sistema.
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Conceitos e entendimento da Região de Segurança 
para uso em Tempo Real RT-CT.BR.01 01 7.2. 22/07/2022
Referência: 4/ 8
3. CONSIDERAÇÕES GERAIS
3.1. Conforme Submódulo 5.4, uma das formas de realizar a gestão da segurança operativa é a utilização 
de ferramentas de análise computacional em execução nas Salas de Controle do ONS. O software 
Organon é uma das ferramentas utilizadas pelo ONS para identificar a condição de segurança operativa 
do sistema elétrico, a partir de resultados gráficos de regiões de segurança, tanto para cenários de 
operação presentes, quanto suas possíveis alterações.
3.2. As regiões de segurança são definidas por área de interesse, com o objetivo de determinar pontos de 
operação seguros considerando um conjunto de contingências. São utilizadas para definir ações 
complementares aos limites e procedimentos de controle de tensão e carregamento já estabelecidos 
nas instruções de operação. 
3.3. As regiões de segurança são simuladas conforme os seguintes critérios:
não considera a atuação de equipamentos de controle de tensão que dependem da ação humana 
(chaveamento de capacitores e/ou reatores e alteração de tapes de transformadores com 
comutação sob carga que operem no modo manual) e de carregamento (alteração de tapes de 
transformadores com comutação sob carga que operem no modo automático e alteração de ângulo 
nos transformadores defasadores);
não aumenta o número de unidades geradoras ao longo da variação de geração nos grupos de 
geração. Somente retira unidades geradoras, quando de redução de geração, de acordo com a 
potência mínima de cada UG;
atribui um fator de participação para as cargas de cada barramento, com base nos valores previstos 
nos casos base mensais do planejamento da operação para toda a rede externa não supervisionada 
e representada no Sistema de Supervisão do ONS. Os valores de carga desses barramentos são 
atualizados a cada meia hora, aplicando-se os fatores de participação aos valores de carga previstos 
no Programa Diário de Produção - PDP;
respeita os limites constantes nos Cadastros de Informações Operacionais de Limites de 
Equipamentos e de Faixas para Controle de Tensão das respectivas áreas elétricas, bem como os 
critérios estabelecidos no Submódulo 2.3 dos Procedimentos de Rede para estabilidade 
eletromecânica;
considera a atuação de Sistemas Especiais de Proteção – SEP com influência na área de interesse, 
descritos na Instrução de Operação de Esquemas Especiais.
3.4. As regiões de segurança implementadas nas Salas de Controle do ONS podem ser adequadas, ou até 
mesmo substituídas por novas regiões, de acordo com a necessidade da operação em tempo real.
A atualização dos nomogramas é executada continuadamente em tempo real, com base nos dados 
obtidos a partir do Estimador de Estados do Sistema de Supervisão e Controle do ONS.
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Conceitos e entendimento da Região de Segurança 
para uso em Tempo Real RT-CT.BR.01 01 7.2. 22/07/2022
Referência: 5/ 8
4. DETERMINAÇÃO DA REGIÃO DE SEGURANÇA
4.1. Na definição de uma região de segurança são estabelecidas:
rede elétrica a ser avaliada, obtida a partir do Estimador de Estados;
áreas e/ou barramentos que serão monitorados;
monitoramentos de carregamento e/ou intercâmbios de interesse;
três grupos de geração;
conjunto de contingências;
esquemas especiais.
4.2. O conjunto de contingências é estabelecido em uma lista de eventos, estáticos ou dinâmicos, e é 
aplicado na vizinhança do ponto de operação inicial, utilizando fluxo de potência continuado.
4.3. A definição dos três grupos de geração permite a exploração da vizinhança do ponto de operação, 
a partir da variação do despacho das usinas contidas em cada grupo em um número pré-definido de 
direções. Os dois primeiros grupos de geração são escolhidos para explorar a área que se deseja 
analisar, enquanto o terceiro grupo é responsável por estabelecer o balanço de geração entre os 
demais.
As usinas que compõe um mesmo grupo são determinadas de acordo com critérios e/ou influências 
semelhantes sobre o conjunto de contingências que se deseja avaliar, de modo que seja possível 
adotar procedimentos de redespacho para solução dos problemas identificados. 
Para cada região de segurança são obtidos três nomogramas, com as projeções bidimensionais dos 
grupos de geração: Grupo 1 vs. Grupo 2, Grupo 2 vs. Grupo 3 e Grupo 1 vs. Grupo 3.
Além da definição dos grupos de geração, pode-se definir duas grandezas elétricas adicionais para 
monitoramento, viabilizando uma análise gráfica adicional da região de segurança, formando um 
quarto nomograma, com cada uma dessas grandezas em cada eixo, permitindo visualizar por 
exemplo a relação entre o intercâmbio entre áreas e um determinado carregamento.
4.4. O monitoramento das áreas e barramentos é definido de forma a delimitar a área de interesse que 
se deseja analisar na região de segurança.
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Conceitos e entendimento da Região de Segurança 
para uso em Tempo Real RT-CT.BR.01 01 7.2. 22/07/2022
Referência: 6/ 8
5. INTERPRETAÇÃO DA REGIÃO DE SEGURANÇA
Para exemplificar a interpretação dos resultados de uma região de segurança é apresentado um nomograma 
de exemplo na figura abaixo, onde nele são representados todos os limites operativos expostos no item 2 
desta RT-CT.BR.01, sendo indicados através de seus contornos e áreas, bem como demais indicações 
representativas do resultado da região de segurança:
Dentre os procedimentos a serem realizados a partir da análise da região de segurança, destacam-se os 
seguintes:
redespacho de geração nas usinas que compõem os grupos de geração, permitindo deslocar o ponto de 
operação para qualquer ponto da região de segurança;
controle de tensão nos barramentos afetados, permitindo expandir o contorno que define a sua fronteira 
de violação;
controle de carregamento nos equipamentos afetados, alterando a área de violação apresentada;
adoção de limites de transferência de potência entre sistemas;
 habilitar/desabilitar Esquemas Especiais.
De maneira a auxiliar na interpretação do nomograma, os subitens a seguir descrevem as principais 
representações para análise da região de segurança.
5.1. REPRESENTAÇÃO DOS EIXOS DO GRÁFICO
São as possíveis combinações dos três grupos de geração ou, no caso do quarto nomograma, as duas 
primeiras grandezas elétricas definidas pelo usuário (intercâmbio entre áreas, carregamento em 
equipamentos, entre outras).
5.2. REPRESENTAÇÕES EM CRUZ AZUL
O ponto de operação em que o sistema se encontra é indicado pela cruz azul em tamanho maior.
Também são indicadas em duas cruzes menores os dois últimos pontos de operação de maneira que seja 
possível identificar a trajetória que o ponto de operação está seguindo.

Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Conceitos e entendimento da Região de Segurança 
para uso em Tempo Real RT-CT.BR.01 01 7.2. 22/07/2022
Referência: 7/ 8
5.3. INDICAÇÃO DE NÚMEROS NOS CONTORNOS
A identificação de cada direção de variação de despacho dos grupos de geração é indicada pelos números 
contidos no contorno mais distante em relação ao ponto de operação atual. 
5.4. DELIMITAÇÕES EM CONTORNO VERDE
Este contorno delimita os pontos de operação em que não há violação dos limites de tensão na área de 
interesse monitorada para o conjunto de contingências avaliado. A sua ausência indica que toda a região está 
violada para este critério.
5.5. DELIMITAÇÕES EM CONTORNO VERMELHO
Este contorno delimita os pontos de operação em que não há violação de nenhum dos critérios de simulação 
dinâmica na área de interesse monitorada para o conjunto de contingências avaliado. A sua ausência indica 
que toda a região está violada para este critério.
5.6. ÁREA EXPLORADA EM REGIÃO VERDE
Indica que nesta região não há violação de carregamento das linhas de transmissão ou equipamentos 
monitorados para o conjunto de contingências avaliado. 
5.7. ÁREA EXPLORADA EM REGIÃO HACHURADA
Indica que nesta região há atuação de Esquema Especial para o conjunto de contingências avaliado.
5.8. ÁREA EXPLORADA EM REGIÃO AMARELA
Indica que nesta região há violação de carregamento de uma das linhas de transmissão ou de equipamentos 
monitorados para pelo menos uma das contingências do conjunto.
5.9. ÁREA EXPLORADA EM REGIÃO VERMELHA
Indica que nesta região o sistema monitorado não é seguro pelos seguintes limites:
limite de geração;
limite de estabilidade de tensão (curva do nariz);
limite de convergência;
limite de segurança.
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Conceitos e entendimento da Região de Segurança 
para uso em Tempo Real RT-CT.BR.01 01 7.2. 22/07/2022
Referência: 8/ 8
6. BIBLIOGRAFIA 
[1] Manual do Usuário – Organon – Versão 10.2 – HPPA/2018
[2] Tutorial Básico Sobre região de segurança Dinâmica – ONS/2009
[3] NEVES, Rodrigo Alves das, “Investigação de parâmetros que provocam diferenças entre regiões de 
segurança estática e dinâmica”, Dissertação de Mestrado, UFRJ, 2017.
