 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da UHE Canastra 
 
 
Código Revisão Item Vigência 
IO-OI.S.UCAN 02 3.7.5.2. 03/09/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Incorporação da MOP/ONS 372 -R/2024 “Mudança da denominação da CPFL -T e do seu centro de 
operação”. 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S CPFL-T CEEE-G 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Canastra IO-OI.S.UCAN 02 3.7.5.2. 03/09/2024 
 
Referência:  2/7 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  3 
3.1. Unidades Geradoras ...................................................................................................................... 3 
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL ................................ ....................  3 
4.1. Procedimentos Gerais ................................................................................................................... 3 
4.2. Procedimentos Específicos ............................................................................................................ 4 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  4 
5.1. Procedimentos Gerais ................................................................................................................... 4 
5.2. Procedimentos para Recomposição com Autonomia ................................................................... 5 
5.2.1. Preparação da Instalação para Recomposição com Autonomia ................................. 5 
5.2.2. Recomposição com Autonomia após desligamento total da Instalação .................... 5 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 6 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 6 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 6 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 6 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 6 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 6 
6. MANOBRAS DE UNIDADES GERADORAS ................................ ................................ ........................  6 
6.1. Procedimentos Gerais ................................................................................................................... 6 
6.2. Procedimentos Específicos ............................................................................................................ 7 
6.2.1. Desligamento de Unidades Geradoras ........................................................................ 7 
6.2.2. Sincronismo de Unidades Geradoras .......................................................................... 7 
7. NOTAS IMPORTANTES ................................ ................................ ................................ .................. 7 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Canastra IO-OI.S.UCAN 02 3.7.5.2. 03/09/2024 
 
Referência:  3/7 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da UHE Canastra , definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da Instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR-S e a Instalação, de propriedade da CEEE-G, é realizada pela 
CPFL-T, por intermédio do COT CPFL-T.   
Nota: A execução da operação na UHE Canastra é realizada pela CEEE-G. 
2.3. As unidades geradoras e equipamentos desta Instalação fazem parte da Área 230 kV do Rio Grande 
do Sul. 
2.4. O COSR-S controla e supervisiona o despacho de geração desta Usina. 
2.5. Esta Usina:  
• é despachada centralizadamente; 
• está conectada na Rede de Distribuição; 
• não participa do Controle Automático da Geração – CAG; 
• não é de autorrestabelecimento; 
• não é fonte para início do processo de recomposição fluente de uma área. 
2.6. Os dados operacionais desta Usina estão descritos no Cadastro de Informações Operacionais da 
respectiva Área Elétrica. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. UNIDADES GERADORAS  
As unidades geradoras G1 e G2 13,8 kV estão conectadas ao barramento de 13,8 kV, por meio de seus 
respectivos disjuntores, estando com o barramento de 13,8 kV conectado aos terciários dos transformadores 
TR-1 e TR-2 138/69/13,8 kV da UHE Canastra da CPFL-T. 
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. Os barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão executada 
com responsabilidade pelo Agente Operador da Instalação. 
Esgotados os recursos locais disponíveis, sendo necessário, o Agente deve acionar o COSR -S, que 
verificará a disponibilidade dos recursos sistêmicos. 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Canastra IO-OI.S.UCAN 02 3.7.5.2. 03/09/2024 
 
Referência:  4/7 
 
 
4.1.2. A Usina deve manter os valores de geração de acordo com os valores programados, constantes no 
Programa Diário de Operação – PDO. Para atendimento dos valores programados constantes no PDO, 
não é necessária a autorização prévia do COSR-S. 
Qualquer alteração no valor de geração da Usina em relação ao valor de geração constante no PDO 
somente pode ser executada após autorização do COSR-S. 
As reprogramações de geração quando de necessidades sistêmicas serão executadas pela Usina 
quando solicitadas pelo COSR-S. 
Após reprogramação de geração solicitada pelo ONS, o Agente somente poderá alterar a geração da 
Usina com autorização do ONS, inclusive para adoção de valores contidos no PDO. 
4.1.3. Os desvios de geração da Usina em relação aos valores previstos no PDO ou em relação às 
reprogramações, devem ser controlados observando os valores máximos permitidos explicitados na 
instrução de operação IO-CG.BR.01 – Controle da Geração em Condição Normal. 
4.1.4. Quando não existir ou não estiver disponível a supervisão da Usina para o ONS, essa deve seguir as 
orientações para envio de dados conforme Rotina Operacional RO-AO.BR.08 - Apuração de Dados de 
Geração e de Intercâmbios. 
4.1.5. A Usina, deve registrar e informar imediatamente os seguintes dados ao COSR-S: 
• movimentação de unidades geradoras  hidrelétricas (mudança de estado operativo  / 
disponibilidade); 
• restrições e ocorrências na usina ou na conexão elétrica que afetem a disponibilidade de 
geração, com o respectivo valor da restrição, contendo o horário de início e término e a 
descrição do evento; 
• demais informações sobre a operação de suas instalações, solicitadas pelo ONS. 
4.1.6. O controle de tensão, por meio da geração ou absorção de potência reativa das unidades geradoras 
da Usina é executado com autonomia pela operação do Agente  e deve ser realizado entre o agente 
da geração e o agente da distribuição. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
Não se aplica. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de um desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento Total da Usina:  caracterizado quando há ausência de tensão em todos os 
terminais de suas conexões ou ausência de fluxo de potência ativa nessas conexões. 
• Desligamento Parcial da Usina:  qualquer outra configuração que não se enquadre como 
desligamento total. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Canastra IO-OI.S.UCAN 02 3.7.5.2. 03/09/2024 
 
Referência:  5/7 
 
5.1.2. Quando de um desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao 
COSR-S as informações a seguir: 
• horário da ocorrência; 
• configuração da Usina logo após a ocorrência; 
• configuração da Usina após ações realizadas com autonomia pela sua operação. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição no Subitem 5.2, sem necessidade de autorização prévia por porte do ONS. Caso o ONS 
intervenha no processo de recomposição, identificando a não aplicabilidade da recomposição fluente 
ou interrompendo a autonomia do Agente Operador  da Instalação na recomposição, deve ser 
utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4. 
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO COM AUTONOMIA 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA RECOMPOSIÇÃO COM AUTONOMIA 
No caso de desligamento total, o Agente Operador deve configurar os disjuntores d as seguintes unidades 
geradoras, conforme condição apresentada a seguir: 
• das unidades geradoras: 
G1 13,8 kV; 
G2 13,8 kV. 
5.2.2. RECOMPOSIÇÃO COM AUTONOMIA APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar com autonomia os seguintes procedimentos para a 
recomposição: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 CPFL-T 
Partir a primeira unidade geradora, 
caso essa estivesse operando antes do 
desligamento, e sincronizá-la. 
Manter VUCAN-13,8 = 13,8 kV. 
Conforme procedimentos internos do 
Agente.  
Gerar o mínimo necessário para manter a 
unidade geradora sincronizada. 
Solicitar autorização ao COSR-S para elevar 
a geração. 
1.1 CPFL-T 
Partir a segunda unidade geradora, 
caso essa estivesse operando antes do 
desligamento, e sincronizá-la. 
Manter VUCAN-13,8 = 13,8 kV. 
Conforme procedimentos internos do 
Agente.  
Gerar o mínimo necessário para manter a 
unidade geradora sincronizada. 
Solicitar autorização ao COSR-S para elevar 
a geração. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Canastra IO-OI.S.UCAN 02 3.7.5.2. 03/09/2024 
 
Referência:  6/7 
 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2, enquanto não houver 
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia destes na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da instalação que seja: 
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, o 
Agente Operador  deve preparar a Instalação conforme Subitem 5.2 .1, sem necessidade de 
autorização do ONS. 
5.4.1.2. Para demais desligamentos parciais da Instalação, não há necessidade de preparação de Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador 
da Instalação deve recompor a Instalação conforme Subitem 5.2.2, sem necessidade de autorização 
do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., o Agente Operador 
da Instalação deve recompor a Instalação conforme Subitem 6.2.2, sem necessidade de autorização 
do ONS. 
O sincronismo das unidades geradoras , bem como a elevação de geração  nessas unidades,  é 
realizado conforme as condições definidas no Subitem 6.2.2. 
6. MANOBRAS DE UNIDADES GERADORAS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desligamento programado ou de urgência de unidades geradoras, e 
desenergização programada ou de urgência de equipamentos, só podem ser efetuados com controle 
do COSR-S. 
6.1.2. Os procedimentos para energização de equipamentos, ou sincronismo de unidades geradoras, após 
desligamentos programados, de urgência ou de emergência, só podem ser efetuados com controle 
do COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Canastra IO-OI.S.UCAN 02 3.7.5.2. 03/09/2024 
 
Referência:  7/7 
 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, ou para sincronismo de unidades geradoras, após desligamento automático sem 
atuação de proteção que impeça o retorno do equipamento, só podem ser executados com 
autonomia pelo Agente Operador da Instalação quando estiverem explicitados e estiverem atendidas 
as condições do Subitem 6.2.2. desta Instrução de Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.5. A partida de unidades geradoras deve seguir critérios próprios do Agente. O sincronismo à Rede de 
Distribuição deve ser realizado após tratativas entre a Usina e o Agente de Distribuição. A tomada de 
carga deve ser realizada com controle do COSR-S. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESLIGAMENTO DE UNIDADES GERADORAS  
O desligamento de unidades geradoras é sempre controlado pelo COSR-S. 
6.2.2. SINCRONISMO DE UNIDADES GERADORAS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de unidades geradoras. 
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais unidades geradoras em operação, conforme explicitado nas condições de 
energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Unidade Geradora Procedimentos Condições ou Limites Associados 
Unidade Geradora 
G1 ou G2 13,8 kV 
Partir e sincronizar a unidade geradora. • 12,4 ≤ VUCAN-13,8 ≤ 14,5 kV; e 
• elevar a geração da unidade 
geradora, após autorização do 
COSR-S. 
7. NOTAS IMPORTANTES 
Não se aplica. 
