Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.13
Rotina Operacional
Consistência dos Dados de Perturbações
Código Revisão Item Vigência
RO-AO.BR.03 08 4.3.2. 01/02/2024
.
MOTIVOS DA REVISÃO
•Implantação do envio das informações de interrupção de carga pelos agentes de distribuição e 
consumidores livres conectados diretamente à Rede Básica ou às Demais Instalações de Transmissão - 
DIT através do Sistema Integrado de Perturbações - SIPER.
•Melhorias de texto.
LISTA DE DISTRIBUIÇÃO
CNOS COSR-SE COSR-NE COSR-NCO COSR-S
Escritório Central Agentes de 
Operação
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Consistência dos Dados de Perturbações RO-AO.BR.03 08 4.3.2. 01/02/2024
Referência: E-mail da AOC de 12/01/2024 2 / 11
INDICE
1. OBJETIVO.........................................................................................................................................3
2. REFERÊNCIAS ...................................................................................................................................3
3. CONCEITOS ......................................................................................................................................3
3.1. SIPER – Sistema Integrado de Perturbações .................................................................................3
3.2. Perturbação...................................................................................................................................3
3.3. Desligamento Forçado...................................................................................................................3
3.4. Consistência de dados ...................................................................................................................3
3.5. Síntese Gerencial das Principais Perturbações ocorridas no SIN ..................................................4
3.6. Avaliação de desempenho de componentes, esquemas de religamento automático e de 
sistemas/relés de proteção ...........................................................................................................4
3.7. Indicadores de Segurança Elétrica.................................................................................................4
4. CONSIDERAÇÕES GERAIS .................................................................................................................4
5. DESCRIÇÃO DO PROCESSO ...............................................................................................................5
6. ATRIBUIÇÕES DOS AGENTES E DO ONS ............................................................................................8
6.1. Atribuições do ONS .......................................................................................................................8
6.2. Atribuições dos Agentes de Operação ..........................................................................................8
6.3. Atribuições dos Agentes de distribuição e dos consumidores livres conectados diretamente à 
Rede Básica ou às Demais Instalações de Transmissão - DIT ........................................................9
7. ORIENTAÇÕES TÉCNICAS COMPLEMENTARES.................................................................................10
7.1. Coleta, análise e consistência das informações sobre perturbações ..........................................10
7.2. Classificação estatística dos desligamentos forçados e consistência da classificação ................10
8. ANEXOS .........................................................................................................................................11
ANEXO 1 – INSTRUÇÕES PARA REGISTRO DE INFORMAÇÕES DOS AGENTES DE DISTRIBUIÇÃO E 
CONSUMIDORES LIVRES CONECTADO DIRETAMENTE À REDE BÁSICA OU DIT NO SIPER .................11
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Consistência dos Dados de Perturbações RO-AO.BR.03 08 4.3.2. 01/02/2024
Referência: E-mail da AOC de 12/01/2024 3 / 11
1. OBJETIVO
Definir responsabilidades, premissas e atribuições, visando realizar a coleta e a consistência dos dados de 
perturbações no Sistema Interligado Nacional - SIN com envolvimento da Rede de Operação ou na Rede de 
Distribuição, desde que acarrete interrupção de carga maior ou igual a 100 MW e com duração mínima 10 
minutos.
São objetos desta rotina operacional:
•Elaboração da síntese gerencial das perturbações ocorridas no SIN;
•Avaliação do desempenho de componentes, esquemas de religamento automático e de sistemas/relés de 
proteção da rede de operação;
•Apuração de Indicadores de segurança elétrica no tocante a confiabilidade da Rede Básica. 
2. REFERÊNCIAS
Os seguintes submódulos dos Procedimentos de Rede têm envolvimento direto com esta rotina operacional:
•6.1 – Elaboração dos informes e boletins da Operação;
•6.2 – Análise da operação, ocorrências e perturbações e acompanhamento das providências;
•6.3 – Elaboração do relatório de análise de perturbação;
•6.4 – Análise de falhas em equipamentos e linhas de transmissão;
•7.10 – Implantação e análise do sistema de registro de perturbações;
•9.1 – Indicadores de confiabilidade da Rede Básica;
•9.7 – Indicadores de qualidade de energia elétrica da Rede Básica.
3. CONCEITOS
3.1. SIPER – SISTEMA INTEGRADO DE PERTURBAÇÕES
O SIPER é o sistema computacional utilizado para apurar e armazenar, na Base de Dados Técnica do ONS 
(BDT), os dados de perturbações. Este sistema, desenvolvido em plataforma WEB, permite que os dados 
relativos às perturbações possam ser tratados de forma conjunta entre o ONS e os agentes de operação 
envolvidos no processo de análise de perturbações.
3.2. PERTURBAÇÃO
Conforme o item 305 do submódulo 1.2 - Glossário dos Procedimentos de Rede: 
“Ocorrência no SIN caracterizada pelo desligamento forçado de um ou mais de seus componentes, que 
acarreta quaisquer das seguintes consequências: corte de carga, desligamento de outros componentes 
do sistema, danos em equipamentos ou violação de limites operativos.”
3.3. DESLIGAMENTO FORÇADO 
Conforme o item 139 do submódulo 1.2 - Glossário dos Procedimentos de Rede:
“Desligamento de um componente de serviço, em condições não programadas.”
3.4. CONSISTÊNCIA DE DADOS 
Processo pelo qual se busca a garantia da qualidade dos dados utilizados nos processos do ONS, permitindo, 
sempre que possível, o consenso entre o ONS e o agente de operação no que concerne a informação final. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Consistência dos Dados de Perturbações RO-AO.BR.03 08 4.3.2. 01/02/2024
Referência: E-mail da AOC de 12/01/2024 4 / 11
De forma geral, os dados são classificados como:
•Dados de consistência obrigatória: o dado deve ser obrigatoriamente consistido com os agentes 
de operação antes de ser utilizado pelo ONS.
•Dados de consistência facultativa: os agentes de operação podem ser contatados, a critério do 
ONS, para dirimir eventuais dúvidas ou inconsistências.
3.5. SÍNTESE GERENCIAL DAS PRINCIPAIS PERTURBAÇÕES OCORRIDAS NO SIN
Relatório que apresenta a análise sucinta das principais perturbações que tenham afetado a Rede de 
Operação, relatando as anormalidades identificadas, bem como as providências tomadas, em andamento ou 
recomendações de novas ações.
3.6. AVALIAÇÃO DE DESEMPENHO DE COMPONENTES, ESQUEMAS DE RELIGAMENTO AUTOMÁTICO E 
DE SISTEMAS/RELÉS DE PROTEÇÃO
Análise efetuada por meio de relatórios que apresentam a avaliação do desempenho dos componentes de 
transmissão e geração, com ênfase na origem e causa dos desligamentos forçados apurados, bem como nos 
tipos de atuações e causas de anormalidades de esquemas de religamento automático e de sistemas/relés 
de proteção.
3.7. INDICADORES DE SEGURANÇA ELÉTRICA
Os indicadores de segurança elétrica estão agrupados por afinidade em dois blocos: indicadores de 
confiabilidade da Rede Básica e indicadores de continuidade de serviço dos pontos de controle. Esses 
agrupamentos visam ressaltar o enfoque de cada grupo de indicadores para permitir uma visão geral do 
desempenho da Rede Básica e de suas condições de fronteira.
4. CONSIDERAÇÕES GERAIS
4.1. Os agentes de operação devem informar ao ONS os dados relativos a toda e qualquer perturbação 
que envolva os equipamentos pertencentes à Rede de Operação (Rede Básica, Rede Complementar 
ou Unidade Geradora Despachada Centralizadamente).
Para perturbações sem envolvimento da Rede de Operação, os agentes de operação devem informar 
os dados ao ONS quando ocorrer corte de carga em montante igual ou superior a 100 MW e com um 
tempo de interrupção de carga de no mínimo 10 minutos.
As informações sobre as perturbações são preliminarmente divulgadas e registradas pelas equipes 
de tempo real dos agentes e dos centros de operação do ONS. A partir dos dados preliminares, são 
necessárias interações entre os agentes de operação e o ONS para consolidar os dados informados, 
com o intuito de eliminar dúvidas ou inconsistências.
Devem ser fornecidas inclusive, as informações referentes a desligamentos de caráter acidental 
provocados por ação humana e/ou anormalidades na parte secundária do equipamento (proteção, 
controle, fiação, painel, etc.).
4.2. A consistência dos dados de perturbações é realizada por intermédio do SIPER. Por meio desse 
sistema, o ONS solicita ao(s) agente(s) de operação envolvido(s) as informações necessárias para a 
consolidação e validação dos dados das perturbações.
4.3. A conclusão do processo de consolidação de dados de perturbações ocorridas no SIN, realizada entre 
os agentes de operação e ONS, por meio do SIPER, garante a qualidade das informações 
armazenadas, além de permitir a agilidade e qualidade dos processos que envolvam a análise, 
apuração de indicadores e dados que tenham por base perturbações no SIN.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Consistência dos Dados de Perturbações RO-AO.BR.03 08 4.3.2. 01/02/2024
Referência: E-mail da AOC de 12/01/2024 5 / 11
4.4. Os agentes proprietários de equipamentos e linhas de transmissão são os responsáveis por prestar 
as  informações tratadas por esta Rotina. 
4.5. Os agentes de operação, caso não participem da consistência dos dados conforme os prazos e 
procedimentos estipulados nessa rotina, permitem que o ONS conclua o processo de consistência 
dos dados de perturbações sem a sua contribuição e participação, bem como estarão sujeitos ao 
recebimento de providências a serem cadastradas no Sistema de Gestão de Providências – SGP para 
que o agente forneça informações necessárias sobre a ocorrência, por meio do processo de emissão 
da Síntese Gerencial.
4.6. Relativamente ao controle dos prazos estabelecidos nesta Rotina, são considerados feriados as datas 
definidas como feriados nacionais e também os feriados municipais ou estaduais nas localidades em 
que se encontram instalados os escritórios do ONS (Brasília, Rio de Janeiro, Florianópolis e Recife). 
A prorrogação de prazo em função dos feriados descritos nesse item é considerada para os agentes 
de operação de todo país.
5. DESCRIÇÃO DO PROCESSO
5.1. CONSISTÊNCIA DE DADOS REFERENTES AOS DESLIGAMENTOS FORÇADOS
5.1.1. O ONS solicita aos agentes de operação envolvidos em uma perturbação no SIN, por meio do sistema 
SIPER, no primeiro dia útil após a ocorrência da perturbação, as informações necessárias para a 
consistência obrigatória dos dados. 
5.1.2. Os agentes de operação devem fornecer, no SIPER, as informações referentes ao conjunto de 
perturbações observadas no período de uma semana (segunda-feira a domingo), até o terceiro dia 
útil após o término da semana em análise. 
5.1.3. Os agentes de operação, após prestarem as informações referentes ao item anterior, podem ser 
solicitado pelo ONS a fornecer informações adicionais ou esclarecimentos, com o intuito de dirimir 
dúvidas ou mesmo eliminar possíveis inconsistências. As informações adicionais ou esclarecimentos 
solicitados devem ser respondidos em até 01 (um) dia útil contado a partir do recebimento da 
solicitação no sistema SIPER.
As informações adicionais, que venham a ser solicitadas, são precedidas de aviso de solicitação, por 
meio eletrônico (e-mail) e/ou contato telefônico.
5.1.4. O ONS deve concluir a análise das informações em 04 (quatro) dias úteis após o término do prazo 
estabelecido no item 5.1.2.
5.1.5. Concluídas as etapas descritas nos itens 5.1.1 a 5.1.4, os agentes de operação devem consistir a 
análise realizada pelo ONS até o oitavo dia útil após o término da semana em análise. O ONS, 
portanto, disponibiliza as perturbações para a consistência dos agentes até o sétimo dia útil após o 
término da semana em análise. 
Eventuais disponibilizações efetuadas pelo ONS após o prazo estabelecido são precedidas de aviso 
de solicitação de consistência por meio eletrônico (e-mail) e/ou contato telefônico.
O parecer do agente de operação conclui o processo de consistência das informações de 
perturbações. Caso o agente não consista a análise realizada pelo ONS, a referida análise é 
considerada consistida sem discordância por decurso de prazo.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Consistência dos Dados de Perturbações RO-AO.BR.03 08 4.3.2. 01/02/2024
Referência: E-mail da AOC de 12/01/2024 6 / 11
5.1.6. As informações sobre uma mesma perturbação no SIN que envolva mais de um agente são consistidas 
a partir das informações de todos os agentes envolvidos. O conjunto de informações prestadas é 
consolidado pelo ONS.
5.1.7. O processo de consistência das informações de perturbações deve atender os prazos estabelecidos 
nos itens 5.1.1 a 5.1.5. Eventuais atrasos devem ser justificados.
5.1.8. Concluído o processo de consistência das informações de perturbações entre o ONS e os agentes de 
operação, as informações consistidas são utilizadas na elaboração da Síntese Gerencial e também na 
apuração de indicadores de segurança elétrica, no tocante à confiabilidade da Rede Básica.
Eventuais discordâncias por parte dos agentes de operação quanto à consistência das informações 
de perturbações são registradas no SIPER e contemplados na Síntese Gerencial de perturbações.
5.1.9. Com base nas informações já consistidas, o ONS efetua a classificação estatística dos desligamentos 
forçados que constituem as perturbações do mês até o último dia útil do mês subsequente. 
Os desligamentos forçados associados a perturbações pendentes de parecer no Sistema de Gestão 
de Providências - SGP, cujo atendimento ocorra após o prazo determinado ao ONS para a realização 
da classificação estatística, são tratados como excepcionalidade e devem ser classificados pelo ONS 
em até 3 (três) dias úteis após a data do referido atendimento.
5.1.10. Os agentes devem consistir a classificação estatística realizada pelo ONS em até 15 (quinze) dias úteis 
após o término do prazo estabelecido no item 5.1.9.
A consistência da classificação estatística sem divergência por parte do agente implica no término do 
processo de coleta e consistência de dados de perturbações.
Caso o agente não consista a classificação estatística realizada pelo ONS, a referida classificação é 
considerada consistida sem discordância por decurso de prazo, concluindo assim o processo de 
coleta e consistência de dados de perturbações.
Os desligamentos forçados associados a perturbações pendentes de parecer no Sistema de Gestão 
de Providências - SGP, cujo atendimento tenha sido posterior ao prazo determinado ao ONS para a 
realização da classificação estatística, são tratados como excepcionalidade e devem ser consistidos 
pelo agente em até 10 (dez) dias corridos após a data de disponibilização dos eventos para 
consistência.
5.1.11. Eventuais discordâncias por parte dos agentes de operação quanto à consistência da classificação 
estatística realizada pelo ONS são registradas no SIPER e devem ser tratadas pelo ONS em até 5 (cinco) 
dias úteis após o término do prazo estabelecido no item 5.10.
Quando da ocorrência de divergências, o ONS encaminha e-mail ao agente informando se discordou 
ou concordou, parcial ou totalmente, com o questionamento do agente.
O parecer do ONS com relação à divergência conclui o processo de coleta e consistência de dados de 
perturbações.
5.2. CONSISTÊNCIA DE DADOS REFERENTES ÀS INTERRUPÇÕES DE CARGA (AGENTES DE DISTRIBUIÇÃO 
E CONSUMIDORES LIVRES CONECTADOS À REDE BÀSICA OU DIT)
5.2.1. No dia útil subsequente a uma perturbação com corte de carga, o ONS disponibiliza aos agentes de 
distribuição e aos consumidores livres conectados à Rede Básica ou às Demais Instalações de 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Consistência dos Dados de Perturbações RO-AO.BR.03 08 4.3.2. 01/02/2024
Referência: E-mail da AOC de 12/01/2024 7 / 11
Transmissão - DIT afetados, por meio do SIPER, as informações referentes à interrupção de carga 
para consistência. 
5.2.2. Os agentes devem fornecer as informações referentes à interrupção de carga no SIPER até o primeiro 
dia útil subsequente à perturbação, devendo:
5.2.2.1. Consistir o montante total de carga interrompida, considerando os seguintes critérios:
5.2.2.1.1. São consideradas apenas as interrupções de carga motivadas por desligamentos forçados, 
excetuando-se, portanto, as interrupções de carga em caráter programado ou seletivo em tempo 
real.
Eventual corte de carga manual solicitado em tempo real para controle de carregamento 
decorrente de um desligamento forçado deve ser considerado.
5.2.2.1.2. As cargas interrompidas de consumidores livres e potencialmente livres conectados à rede de 
distribuição devem ser computadas no montante total de carga interrompida do agente de 
distribuição.
Não são consideradas interrupções de carga com origem interna aos consumidores conectados à 
rede de distribuição ou em seus ativos de conexão ou de cessão de outros, sem que haja 
desligamento de equipamentos da rede de distribuição.
5.2.2.1.3. Não são consideradas as perdas de carga resultantes de redução natural (alteração de carga em 
função da redução do módulo da tensão ou de variações na frequência do SIN) ou de 
desligamento de carga pelo próprio consumidor por intermédio de dispositivo de proteção 
interno.
5.2.2.1.4. Não são consideradas interrupções de carga com origem interna aos consumidores livres 
conectados diretamente à Rede Básica ou às Demais Instalações de Transmissão - DIT ou em seus 
ativos de conexão ou de cessão de outros, sem que haja desligamento de equipamentos da Rede 
de Operação ou cargas do restante do SIN.
5.2.2.2. Fornecer os instantes de normalização do fornecimento para cada bloco de carga. 
5.2.2.2.1. Os agentes de distribuição devem considerar o momento em que os respectivos alimentadores 
foram energizados como horário de recomposição de cada parcela de sua carga,  considerando o 
valor da carga recomposta igual ao da carga interrompida.
5.2.2.2.2. Os consumidores conectados diretamente à rede básica ou DIT devem considerar como horário 
de recomposição total de suas cargas interrompidas o horário da disponibilização de tensão no 
ativo de conexão deste consumidor, desde que, a partir desse instante, o consumidor seja 
autorizado a recompor a totalidade de suas cargas interrompidas.
5.2.2.3. No caso de agentes de distribuição, informar a relação de municípios afetados pela interrupção de 
carga, bem como o percentual afetado de cada município.
5.2.3. Os agentes podem ser solicitados pelo ONS a fornecer informações adicionais ou esclarecimentos, 
com o intuito de dirimir dúvidas ou mesmo eliminar possíveis inconsistências. As informações 
adicionais ou esclarecimentos solicitados devem ser respondidos em até 01 (um) dia útil contado a 
partir do recebimento da solicitação.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Consistência dos Dados de Perturbações RO-AO.BR.03 08 4.3.2. 01/02/2024
Referência: E-mail da AOC de 12/01/2024 8 / 11
6. ATRIBUIÇÕES DOS AGENTES E DO ONS
6.1. ATRIBUIÇÕES DO ONS 
6.1.1. Coletar, registrar e disponibilizar, no SIPER, todas as informações a respeito das perturbações 
ocorridas no SIN com envolvimento da Rede de Operação.
6.1.2. Coletar, registrar e disponibilizar, no SIPER, as informações a respeito das perturbações que não 
envolveram a Rede de Operação e que provocaram uma interrupção de carga maior ou igual a 100 
MW e com duração maior ou igual a 10 minutos.
6.1.3. Interagir com os agentes de operação por meio do SIPER e, se necessário, solicitar informações 
complementares por meio eletrônico e/ou contato telefônico.
6.1.4. Consolidar e analisar, a partir das informações prestadas pelos agentes de operação, os dados das 
perturbações, garantindo o armazenamento destes na Base de Dados Técnica do ONS (BDT). 
6.1.5. Tratar eventuais divergências relativas à consistência das informações prestadas pelos agentes.
6.1.6. Realizar a classificação estatística dos desligamentos forçados, com base nas informações prestadas 
pelos agentes.
6.1.7. Tratar eventuais divergências relativas à consistência da classificação estatística.
6.2. ATRIBUIÇÕES DOS AGENTES DE OPERAÇÃO
6.2.1. As equipes de análise de proteção dos agentes de operação devem fornecer ao ONS as informações 
solicitadas sobre as perturbações, que envolvam os equipamentos da Rede de Operação, utilizando 
o SIPER, dentro dos prazos estabelecidos, a fim de permitir a apuração e consistência dos dados, a 
saber:
6.2.1.1. Descrição do(s) desligamento(s) que compõe(m) a perturbação, contendo o seu tipo (automático 
ou manual), origem, causa, tipo de defeito elétrico ou mecânico, fases envolvidas e a localização 
da falta. Caso o desligamento envolva mais de um agente de operação, este item é de 
responsabilidade exclusiva do agente de operação proprietário do equipamento.
6.2.1.2. Proteções / Sistemas Especiais de Proteção - SEP atuados por terminal, inclusive para as tentativas 
de restabelecimento sem sucesso, com o tempo de eliminação do defeito. No caso de proteções 
elétricas, deve ser informado o modelo dos relés atuados. Quando de atuação de relés de 
bloqueio (geradores, barras, transformadores e reatores) deverá(ão) ser explicitada(s) as funções 
de proteção que atuaram sobre o bloqueio. No caso específico de unidades geradoras deverão 
ser explicitadas também as funções de proteção que provocaram paradas parciais ou totais, sem 
acionamento de relés de bloqueio;
6.2.1.3. Causa de atuações acidentais, incorretas e de recusas de atuação de proteção/SEP, inclusive para 
as tentativas de restabelecimento sem sucesso;
6.2.1.4. Desempenho do religamento automático (caso exista), informando a causa de falhas ou 
desempenhos insatisfatórios;
6.2.1.5. Informações sobre anormalidades causadas por falhas humanas e/ou de equipamentos, bem 
como providências tomadas ou em andamento, visando o restabelecimento do equipamento ou 
evitar a reincidência das anormalidades;
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Consistência dos Dados de Perturbações RO-AO.BR.03 08 4.3.2. 01/02/2024
Referência: E-mail da AOC de 12/01/2024 9 / 11
6.2.1.6. Proposta de prazo para conclusão das providências em andamento sob sua responsabilidade; 
6.2.1.7. Dados complementares necessários para o esclarecimento da perturbação (figuras, gráficos, 
relatórios, etc.).
6.2.2. Fornecer ao ONS, quando solicitado, com a utilização do SIPER, informações adicionais que visem 
dirimir dúvidas ou eliminar inconsistências, a fim de permitir a realização da consistência facultativa.
6.2.3. Consistir as informações relativas à análise da perturbação, emitida pelo ONS, conforme o item 5.1.5. 
6.2.4. Consistir a classificação estatística relativa aos desligamentos forçados, realizada pelo ONS, conforme 
o item 5.1.10.
6.2.5. Manter a lista de representantes do agente no SIPER atualizada por intermédio do portal SINtegre 
do ONS.
6.3. ATRIBUIÇÕES DOS AGENTES DE DISTRIBUIÇÃO E DOS CONSUMIDORES LIVRES CONECTADOS 
DIRETAMENTE À REDE BÁSICA OU ÀS DEMAIS INSTALAÇÕES DE TRANSMISSÃO - DIT
6.3.1. Fornecer as informações referentes à interrupção de carga, no SIPER, conforme prazo e critérios 
estabelecidos no item 5.2.
6.3.2. Fornecer ao ONS informações adicionais que visem dirimir dúvidas ou eliminar inconsistências com 
relação à perturbação em até 1 (um) dia útil após a solicitação do ONS.
6.3.3. Manter a lista de representantes do agente no SIPER atualizada por intermédio do portal SINtegre 
do ONS.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Consistência dos Dados de Perturbações RO-AO.BR.03 08 4.3.2. 01/02/2024
Referência: E-mail da AOC de 12/01/2024 10 / 11
7. ORIENTAÇÕES TÉCNICAS COMPLEMENTARES
Prazos relativos às etapas do processo 
Os prazos estipulados nesta rotina estão detalhados nos gráficos a seguir. 
7.1. COLETA, ANÁLISE E CONSISTÊNCIA DAS INFORMAÇÕES SOBRE PERTURBAÇÕES
7.2. CLASSIFICAÇÃO ESTATÍSTICA DOS DESLIGAMENTOS FORÇADOS E CONSISTÊNCIA DA 
CLASSIFICAÇÃO

Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Consistência dos Dados de Perturbações RO-AO.BR.03 08 4.3.2. 01/02/2024
Referência: E-mail da AOC de 12/01/2024 11 / 11
8. ANEXOS
ANEXO 1 – INSTRUÇÕES PARA REGISTRO DE INFORMAÇÕES DOS AGENTES DE DISTRIBUIÇÃO E 
CONSUMIDORES LIVRES CONECTADO DIRETAMENTE À REDE BÁSICA OU DIT NO SIPER
1) Informar o montante total de carga interrompido no campo “Corte Carga (MW)”
2) Para agentes de distribuição, informar os municípios afetados e os respectivos percentuais de carga 
interrompida. Se a carga do município foi inteiramente interrompida, informar 100%.
3) Informar os montantes de cada bloco de carga recomposto e os respectivos horários de recomposição. 
A soma dos blocos de carga recompostos deve ser igual ao montante total interrompido.
Segue abaixo um exemplo de preenchimento dos dados na tela do SIPER.

