REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
8
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 1 de 15
1. ÍNDICE
1.INTRODUÇÃO
2.ESTRUTURA DO REGULAMENTO
3.ABRANGÊNCIA
4.PRINCÍPIOS OPERACIONAIS BÁSICOS
5.TERMINOLOGIA OPERACIONAL
6.RELACIONAMENTO, PROCEDIMENTOS 
DE COMUNICAÇÃO E TROCA DE 
INFORMAÇÕES OPERACIONAIS
7.RECURSOS DE SUPERVISÃO, 
CONTROLE E TELECOMUNICAÇÃO NA 
INTERLIGAÇÃO
8.INTERVENÇÕES, ENSAIOS E TESTES EM 
EQUIPAMENTOS DE INTERLIGAÇÃO
9.COORDENAÇÃO DE MANOBRAS NA 
INTERLIGAÇÃO
10.CONTROLE DOS FLUXOS DE 
INTERCÂMBIO DE ENERGIA
11.REGULAÇÃO DOS NÍVEIS DE TENSÃO
12.ANÁLISE DA OPERAÇÃO E DE 
PERTURBAÇÕES NA INTERLIGAÇÃO
13.SOLUÇÃO DE CONTROVÉRSIAS 
TÉCNICO-OPERACIONAIS
14.DISPOSIÇÕES GERAIS
15.APROVAÇÃO
2. ÍNDICE
1. INTRODUCCIÓN
2. ESTRUCTURA DEL REGLAMENTO
3. ALCANCE
4. PRINCIPIOS OPERACIONALES 
BÁSICOS
5. TERMINOLOGÍA OPERATIVA
6. RELACIÓN, PROCEDIMIENTOS DE 
COMUNICACIÓN E INTERCAMBIO DE 
INFORMACIONES OPERATIVAS
7. RECURSOS DE SUPERVISIÓN, 
CONTROL Y TELECOMUNICACIÓN EN 
LA INTERCONEXIÓN
8. TAREAS DE MANTENIMIENTO, 
ENSAYOS Y PRUEBAS EN 
EQUIPAMIENTOS DE INTERCONEXIÓN
9. COORDINACIÓN DE MANIOBRAS EN LA 
INTERCONEXIÓN
10. CONTROL DE LOS FLUJOS DE 
INTERCAMBIO DE ENERGÍA
11. REGULACIÓN DE LOS NIVELES DE 
TENSIÓN
12. ANÁLISIS DE LA OPERACIÓN Y DE 
PERTURBACIONES EN LA 
INTERCONEXIÓN
13. SOLUCIÓN DE CONTROVERSIAS 
TÉCNICO-OPERATIVAS
14. DISPOSICIONES GENERALES
15. APROBACIÓN
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
8
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 2 de 15

REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
8
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 3 de 15
1.INTRODUÇÃO 1. INTRODUCCIÓN
Este Regulamento, aprovado pelo Operador 
Nacional do Sistema Elétrico – ONS (Brasil) e pela 
Compañia Administradora del Mercado Mayorista 
Elétrico Sociedad Anónima – CAMMESA 
(Argentina), visa definir as regras e procedimentos 
a serem adotados na coordenação da operação da 
Interligação Garabi – Rincón.
Para efeitos deste Regulamento, são válidas 
as seguintes entidades e suas 
correspondentes abrangências específicas:
Agência Nacional de Energia Elétrica – 
ANEEL: autarquia sob regime especial que 
tem a finalidade de regular e fiscalizar a 
produção, transmissão, distribuição e 
comercialização de energia elétrica no Brasil.
Transmissora Aliança de Energia Elétrica 
S.A. – TAESA: é a empresa proprietária e 
responsável pela operação dos 
equipamentos, no Sistema Brasileiro, que 
fazem parte da Interligação Garabi – Rincón.
Companhia de Transmissão do Mercosul 
S.A. (CTMSA) e Transportadora de Energia 
S.A. (TESA): são as empresas proprietárias 
dos equipamentos, no Sistema Argentino, que 
fazem parte da Interligação Garabi – Rincón I 
e II, respectivamente.
Para os equipamentos correspondentes à 
Subestação Rincón (Campos 1, 2, 3 e 4), a 
CTM e a TESA participam como 
Transmissoras Independentes.
Para os equipamentos correspondentes às 
linhas de transmissão de 500 kV, desde a 
Subestação Rincón até a fronteira com o 
Brasil, TESA e CTM participam como 
Transmissoras Internacionais.
YACYLEC: é a Transmissora Independente, 
Este Reglamento, aprobado por el Operador 
Nacional del Sistema Eléctrico – ONS (Brasil) 
y por la Compañía Administradora del 
Mercado Mayorista Eléctrico Sociedad 
Anónima – CAMMESA (Argentina), tiene por 
objeto definir las reglas y procedimientos a 
ser adoptados en la coordinación de la 
operación de la Interconexión Rincón – 
Garabí.
A los efectos del presente Reglamento, las 
entidades que a continuación se detallan 
tienen el alcance que se especifica:
Agencia Nacional de Energía Eléctrica – 
ANEEL: ente autárquico bajo un régimen 
especial cuyo objeto es regular e 
inspeccionar la producción, transmisión, 
distribución y comercialización de energía 
eléctrica en Brasil.
Transmisora Alianza de Energía Eléctrica 
S.A. – TAESA: es la empresa propietaria y 
responsable de operar el equipamiento, del 
Sistema Brasileño, que forma parte de la 
Interconexión Rincón – Garabí.
Compañía de Transmisión del Mercosur 
S.A. (CTMSA) y Transportadora de 
Energía S.A. (TESA): son las empresas 
propietarias del equipamiento, en el Sistema 
Argentino, que forma parte de la 
Interconexión Rincón – Garabí I y II, 
respectivamente.
Para el equipamiento correspondiente a la 
ET Rincón (Campos 1, 2, 3 y 4), CTM y TESA 
participan como Transportistas 
Independientes.
Para el equipamiento correspondiente a las 
líneas de transmisión de 500 kV, desde la ET 
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
8
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 4 de 15
contratada pela TESA e pela CTMSA, como 
Operadora dos equipamentos da TESA e da 
CTMSA na Subestação Rincón de Santa 
Maria. Yacylec opera sob a supervisão da 
TRANSENER.
COC: Centro de Controle de Operações do 
Sistema Argentino de Interconexão (SADI), a 
cargo da CAMMESA.
COS SAIRA: Centro de Operação dos 
equipamentos, no Sistema Brasileiro, que 
fazem parte da Interligação Garabi – Rincón.
COT TRANSENER: Centro de Controle de 
Operações, no Sistema Argentino, dos 
equipamentos que fazem parte da Interligação 
Garabi – Rincón.
COS YACYLEC: Centro de Controle de 
Operações, no Sistema Argentino, dos 
equipamentos de 500 kV na SE Rincón de 
Santa Maria.
Operador Nacional do Sistema Elétrico – 
ONS: o responsável pela operação, definição 
do intercâmbio de energia e de disponibilidade 
dos equipamentos do Sistema Brasileiro.
Centros de Operação do ONS: 
corresponde aos Centros de Operação 
próprios do ONS, designados por CNOS ou 
COSR.
Centro Nacional de Operação do Sistema –
CNOS: Centro de Operação de maior nível 
hierárquico do Operador Nacional do Sistema 
Elétrico – ONS, responsável pela 
coordenação, supervisão e controle da 
operação da Rede de Operação.
Centro de Operação do Sistema Sul – 
COSR-S: é um dos Centros de Operação de 
propriedade do ONS, de nível hierárquico 
imediatamente abaixo do CNOS, e 
responsável pela coordenação, supervisão e 
Rincón hasta la frontera con Brasil, TESA y 
CTM participan como Transportistas 
Internacionales.
YACYLEC: es la Transportista 
Independiente, contratada por TESA y 
CTMSA, como Operador del equipamiento 
de TESA y CTMSA en la ET Rincón de Santa 
María. Yacylec opera bajo la supervisión de 
TRANSENER.
COC: Centro de Control de Operaciones del 
Sistema Argentino de Interconexión (SADI), 
a cargo de CAMMESA.
COS SAIRA: Centro de Operación del 
equipamiento, en el Sistema Brasileño, que 
forma parte de la Interconexión Rincón – 
Garabí.
COT TRANSENER: Centro de Control de 
Operaciones, en el Sistema Argentino, del 
equipamiento que forma parte de la 
Interconexión Rincón – Garabí.
COS YACYLEC: Centro de Control de 
Operaciones, en el Sistema Argentino, del 
equipamiento de 500 kV en la E.T. Rincón de 
Santa María.
Operador Nacional del Sistema Eléctrico – 
ONS: el responsable por la operación, 
definición de intercambios de energía y de 
disponibilidad de equipos del Sistema 
Brasileño.
Centros de Operación del ONS: centros de 
Operación propios del ONS, designados 
CNOS o COSR.
Centro Nacional de Operación del Sistema 
– CNOS: Centro de Operación de mayor 
nivel jerárquico del Operador Nacional del 
Sistema Eléctrico – ONS, responsable por la 
coordinación, supervisión y control de la 
operación de la Red de Operación.
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
8
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 5 de 15
controle da operação na Região Sul do Brasil 
e estado do Mato Grosso do Sul.
Câmara de Comercialização de Energia 
Elétrica – CCEE: organização na qual se 
processam a compra e venda de energia entre 
seus participantes, tanto por meio de 
contratos bilaterais como em regime de curto 
prazo, tendo como limites o Sistema Elétrico 
Interligado do Brasil.
Comercializadora de Energia do Mercosul 
S.A.: agente comercializador do Mercado 
Elétrico Atacadista argentino.
Mercado Elétrico Mayorista (MEM): âmbito 
dentro do qual se executam as transações de 
energia elétrica onde oferta e demanda 
convergem para a compra e venda de 
energia.
Organismo Encarregado do Despacho 
(OED): organismo que tem a seu cargo a 
coordenação da operação técnica e 
administração comercial do Mercado Eléctrico 
Mayorista (MEM). Suas funções foram 
atribuídas à CAMMESA por meio de decreto 
regulamentar da Lei 24.065.
Centro de Operación Sur – COSR-S: es el 
Centro de Operación de propiedad del ONS, 
de nivel jerárquico inmediatamente abajo del 
CNOS, y responsable por la coordinación, 
supervisión y control de la operación en la 
Región Sur del Brasil y estado de Mato 
Grosso do Sul.
Cámara de Comercialización de Energía 
Eléctrica – CCEE: organización en la cual se 
procesan la compra y venta de energía entre 
sus participantes, ya sea por medio de 
contratos bilaterales o en régimen de corto 
plazo, teniendo como límites el Sistema 
Eléctrico Interconectado de Brasil.
Comercializadora de Energía del 
Mercosur S.A.: agente comercializador del 
Mercado Eléctrico Mayorista argentino.
Mercado Eléctrico Mayorista (MEM): 
ámbito dentro del cual se ejecutan las 
transacciones de energía eléctrica donde 
convergen la oferta y la demanda para la 
compraventa de energía.
Organismo Encargado del Despacho 
(OED): organismo que tiene a su cargo la 
coordinación de la operación técnica y la 
administración comercial del Mercado 
Eléctrico Mayorista (MEM). Sus funciones 
fueron asignadas a CAMMESA mediante 
decreto reglamentario de la Ley 24.065.
2.ESTRUTURA DO REGULAMENTO
2.1.O Regulamento é composto pelos 
seguintes módulos:
-Módulo 1 – Normativo Geral;
-Módulo 2 – Terminologia Operacional;
-Módulo 3 – Procedimentos Operativos 
para Relacionamento, Comunicação e 
Troca de Informações;
2. ESTRUCTURA DEL REGLAMENTO
2.1. El Reglamento está compuesto por los 
siguientes módulos:
-Módulo 1 – Normativa General;
-Módulo 2 – Terminología Operativa;
-Módulo 3 – Procedimientos Operativos 
para Relación, Comunicación e 
Intercambio de Informaciones;
-Módulo 4 – Programación de Intercambio 
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
8
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 6 de 15
-Módulo 4 – Programação de Intercâmbio 
de Energia e Procedimentos para 
Solicitação e Execução de Intervenções;
-Módulo 5 – Operação da Interligação 
Internacional Garabi – Rincón.
2.2.O Módulo 1 é de caráter normativo e é 
aprovado e assinado pelo Gerente 
Executivo de Procedimentos e 
Desenvolvimento da Operação do ONS, do 
Brasil, e pelo Gerente de Operações da 
CAMMESA, da Argentina.
2.3.Os demais módulos são de aplicação 
operacional, podendo ser revisados e 
atualizados em qualquer instância e de 
comum acordo pelas Gerências dessas 
organizações.
2.4.Em caso de necessidade, outros módulos 
poderão ser elaborados para contemplar 
procedimentos específicos na operação das 
interligações.
de Energía y Procedimientos para 
Solicitud y Ejecución de Intervenciones;
-Módulo 5 – Operación de la 
Interconexión Internacional Rincón – 
Garabí.
2.2. El Módulo 1 es de carácter normativo y es 
aprobado y suscrito por el Gerente Ejecutivo 
de Procedimientos y Desarrollo de la 
Operación del ONS, de Brasil, y por el Gerente 
de Operaciones de CAMMESA, de Argentina.
2.3. Los demás módulos son de aplicación 
operativa, pudiendo ser revisados y 
actualizados en cualquier instancia de común 
acuerdo por las Gerencias de esas 
organizaciones.
2.4. En caso de necesidad, otros módulos podrán 
ser elaborados para contemplar 
procedimientos específicos en la operación de 
las interconexiones.
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
8
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 7 de 15
3. ABRANGÊNCIA
3.1.Equipamento: todos os equipamentos que 
compõem a interligação Garabi – Rincón: 
•Conversoras Garabi I e II;
•LTs 500 kV Rincón de Santa Maria / 
Garabi I e II.
3.2.Intercâmbio: a coordenação operativa dos 
intercâmbios energéticos entre Brasil e 
Argentina referentes à Interligação Garabi – 
Rincón.
3.3.Coordenação Internacional Operativa: a 
coordenação Internacional é realizada pelo 
ONS, no Brasil, e pela CAMMESA, na 
Argentina.
3.ALCANCE
3.1.Equipamiento: todos los equipamientos 
que afecten a la interconexión Rincón – 
Garabí.
•Conversoras Garabí I y II;
•LAT 500 kV Rincón de Santa Maria / 
Garabí I y II.
3.2.Intercambio: coordinación operativa de los 
intercambios energéticos entre Brasil y 
Argentina referidos a la Interconexión 
Rincón – Garabí.
3.3.Coordinación Internacional Operativa: la 
coordinación internacional es realizada 
por el ONS, de Brasil, y por CAMMESA, 
de Argentina.
4.PRINCÍPIOS OPERACIONAIS BÁSICOS
4.1.ONS e CAMMESA deverão cumprir as 
normas, procedimentos, metodologias, 
regras e critérios aplicáveis dos Módulos 
deste Regulamento, no que se refere ao 
controle de intervenções, operação, 
relacionamento, troca de informações e 
comunicação entre Centros de Operação, 
considerando as regras existentes em cada 
país.
4.2.O ONS e a CAMMESA trocarão entre si 
diagramas unifilares das instalações de 
Garabi e de Rincón, onde constará a 
identificação operacional de equipamentos 
a ser utilizada no relacionamento 
operacional entre ambos, comprometendo-
se a fornecer cópia atualizada destes 
diagramas sempre que ocorrerem 
atualizações.
4.3.O ONS e a CAMMESA deverão informar-se 
4.PRINCIPIOS OPERACIONALES 
BÁSICOS
4.1.ONS y CAMMESA deben cumplir las 
normas, procedimientos, metodologías, 
reglas y criterios aplicables de los Módulos 
de este Reglamento, en lo referente al 
control de intervenciones, la operación, la 
relación, el intercambio de informaciones 
y la comunicación entre sus Centros de 
Operaciones, considerando las reglas 
existentes en cada país.
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
8
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 8 de 15
mutuamente sobre todas as modificações 
nas instalações que possam interferir na 
operação das interligações, tais como 
alterações em esquemas de proteção ou 
nos limites dos equipamentos.
4.4.O ONS e a CAMMESA deverão trocar 
informações sobre as características dos 
equipamentos que permitam modelá-los 
para a realização de estudos.
4.5.Caso se verifiquem diferenças de 
interpretação ou aplicação deste 
Regulamento, ONS e CAMMESA 
procurarão chegar a um acordo o mais 
rápido possível, seguindo as prerrogativas 
do Item 13 deste Módulo.
4.6.ONS e CAMMESA atuarão de maneira que 
a operação das interligações seja realizada 
de forma que não haja prejuízo para 
qualquer dos Sistemas.
4.7.ONS e CAMMESA atuarão durante todo 
processo de intervenção dos equipamentos 
das interligações no sentido de manter a 
segurança e confiabilidade dos Sistemas 
Brasileiro e Argentino.
4.8.A segurança de pessoas e equipamentos 
envolvidos na execução das intervenções 
acima referidas, é de responsabilidade das 
empresas proprietárias das instalações ou 
dos seus agentes operadores designados.
4.2.ONS y CAMMESA intercambiarán 
diagramas unifilares de las instalaciones 
de Garabí y de Rincón, donde constará la 
identificación operativa de los 
equipamientos a ser utilizados en la 
relación operativa entre ambos, 
comprometiéndose a suministrar copia 
actualizada de estos diagramas siempre 
que hubiere actualizaciones.
4.3.ONS y CAMMESA, deberán informarse 
mutuamente todas las modificaciones en 
las instalaciones que puedan interferir en 
la operación de la Interconexión, tales 
como alteraciones en esquemas de 
protección o en los límites de los equipos.
4.4.ONS y CAMMESA, deberán intercambiar 
información sobre las características de 
los equipos que permitan modelarlos para 
la realización de estudios.
4.5.Cuando se verifiquen diferencias de 
interpretación o de aplicación de este 
Reglamento, el ONS y CAMMESA 
procurarán llegar a un acuerdo lo más 
rápido posible, siguiendo las etapas del 
Ítem 13 de este Módulo.
4.6.ONS y CAMMESA actuarán de manera tal 
que la operación de las interconexiones 
sea realizada de forma que no haya 
perjuicio para ninguno de los Sistemas.
4.7.ONS y CAMMESA actuarán, durante toda 
maniobra o ensayo de los equipamientos 
de interconexión, en el sentido de 
mantener la seguridad y confiabilidad de 
los Sistemas Brasileño y Argentino.
4.8.La seguridad de las personas y de los 
equipos involucrados en la ejecución de 
las maniobras y ensayos arriba referidos, 
es responsabilidad de las empresas 
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
8
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 9 de 15
propietarias de las instalaciones o de sus 
agentes operativos designados.
5.TERMINOLOGIA OPERACIONAL
5.1.Os Operadores de Sistema do ONS e da 
CAMMESA devem trocar informações 
relativas a seus Sistemas utilizando os 
termos e definições do Módulo 2 deste 
Regulamento.
5.2.Os Operadores do ONS se expressarão em 
português e os Operadores da CAMMESA, 
em espanhol.
5.TERMINOLOGÍA OPERATIVA
5.1. Los Operadores del ONS y de CAMMESA 
deben intercambiar informaciones relativas a 
sus Sistemas utilizando los términos y 
definiciones del Módulo 2 de este Reglamento.
5.2. Los Operadores del ONS se expresarán en 
portugués y los Operadores de CAMMESA, en 
español.
6.RELACIONAMENTO, PROCEDIMENTOS 
DE COMUNICAÇÃO E TROCA DE 
INFORMAÇÕES OPERACIONAIS
6.1.O relacionamento operativo e a troca de 
informações entre o ONS e a CAMMESA 
deverão ser efetuados conforme 
procedimentos específicos do Módulo 3 
deste Regulamento.
6.RELACIÓN, PROCEDIMIENTOS DE 
COMUNICACIÓN E INTERCAMBIO DE 
INFORMACIONES OPERATIVAS
6.1. La relación operativa y el intercambio de 
información entre el ONS y CAMMESA 
deberán ser efectuados conforme a los 
procedimientos específicos del Módulo 3 de 
este Reglamento.
7.RECURSOS DE SUPERVISÃO, 
CONTROLE E TELECOMUNICAÇÃO NA 
INTERLIGAÇÃO
7.1.Os recursos de supervisão, controle e 
telecomunicação para a aquisição e troca 
de informações entre o ONS e a CAMMESA 
estão descritos no Módulo 3 deste 
Regulamento.
7.RECURSOS DE SUPERVISIÓN, 
CONTROL Y TELECOMUNICACIÓN EN 
LA INTERCONEXIÓN
7.1. Los recursos de supervisión, control y 
telecomunicación para la adquisición e 
intercambio de información entre el ONS y 
CAMMESA se describen en el Módulo 3 de 
este Reglamento.
8.INTERVENÇÕES, ENSAIOS E TESTES EM 
EQUIPAMENTOS DE INTERLIGAÇÃO
8.1.As intervenções, testes e ensaios em 
8.TAREAS DE MANTENIMIENTO, 
ENSAYOS Y PRUEBAS EN 
EQUIPAMIENTOS DE INTERCONEXIÓN
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
8
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 10 de 15
qualquer equipamento componente das 
interligações devem ser solicitadas e 
realizadas conforme procedimentos 
específicos do Módulo 4 deste 
Regulamento.
8.1. Los mantenimientos, pruebas y ensayos en 
cualquier equipamiento componente de las 
Interconexiones deben ser solicitadas y 
realizadas conforme a los procedimientos 
específicos del Módulo 4 de este Reglamento.
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
8
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 11 de 15
9.COORDENAÇÃO DE MANOBRAS NA 
INTERLIGAÇÃO
9.1.As manobras em equipamentos das 
interligações, para serviços de manutenção 
ou durante a recomposição após 
perturbações, deverão ser coordenadas 
com as empresas envolvidas, conforme 
estabelecido no Módulo 3 deste 
Regulamento sobre relacionamento 
operativo.
9.2.Sempre que necessitem ser executadas 
manobras que afetem direta ou 
indiretamente as Interligações, os Centros 
de Operação do ONS e da CAMMESA 
devem realizar contato entre si, de modo 
que cada Centro de Operação esteja ciente 
das manobras que o outro coordenará.
9.3.As manobras que, por motivos de 
segurança de pessoas ou integridade de 
equipamentos, precisem ser realizadas o 
mais rápido possível, não necessitam de 
acordo prévio entre os Centros de 
Operação. O Centro de Operação que 
efetuou a manobra deverá imediatamente 
informar o fato ao outro Centro de 
Operação.
9.4.A coordenação das manobras deve ser 
realizada de forma a que sejam atendidos 
os requisitos de ordem técnica e de 
segurança, objetivando obter o maior grau 
de confiabilidade na operação.
9.5.Os procedimentos para o restabelecimento 
das Interligações, após desligamentos 
voluntários ou involuntários, estão definidos 
em procedimentos específicos descritos no 
9.COORDINACIÓN DE MANIOBRAS EN LA 
INTERCONEXIÓN
9.1.Las maniobras de los equipos de las 
interconexiones, para trabajos de 
mantenimiento o durante el 
restablecimiento luego de perturbaciones, 
deberán ser coordinadas con las 
empresas involucradas, conforme a la 
relación operativa definida en el Módulo 3 
de este Reglamento.
9.2.Siempre que se necesite ejecutar 
maniobras que afecten directa o 
indirectamente a las Interconexiones, los 
Centros de Operaciones de ONS y de 
CAMMESA deberán contactarse, de modo 
que cada Centro de Operaciones esté en 
conocimiento de las maniobras que el otro 
coordinará.
9.3.Las maniobras que, por motivos de 
seguridad de las personas o integridad de 
los equipamientos, deban ser realizadas lo 
más rápido posible, no necesitan de 
acuerdo previo entre los Centros de 
Operaciones. El Centro de Operaciones 
que efectuó la maniobra deberá 
inmediatamente informar el hecho al otro 
Centro de Operaciones.
9.4.La coordinación de las maniobras debe 
ser realizada de forma tal que sean 
satisfechos los requisitos de orden técnico 
y de seguridad, teniendo como objetivo 
obtener el mayor grado de confiabilidad en 
la operación.
9.5.Los procedimientos para el 
restablecimiento de las Interconexiones, 
después de desconexiones voluntarias o 
involuntarias, están definidos en los 
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
8
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 12 de 15
Módulo 5 deste Regulamento. procedimientos específicos descriptos en 
el Módulo 5 de este Reglamento.
10.CONTROLE DOS FLUXOS DE 
INTERCÂMBIO DE ENERGIA
10.1. Para acompanhamento, em tempo real, 
dos fluxos de importação ou exportação de 
energia, devem ser consideradas as 
seguintes referências.
10.1.1. No lado do Brasil:
•na saída da LT 500 kV Conversora 
Garabi I / Rincón de Santa Maria, na 
Conversora Garabi I;
•na saída da LT 500 kV Conversora 
Garabi II / Rincón de Santa Maria, na 
Conversora Garabi II.
10.1.2. No lado da Argentina:
•na saída da LT 500 kV Conversora 
Garabi I / Rincón de Santa Maria, na 
SE Rincón de Santa Maria;
•na saída da LT 500 kV Conversora 
Garabi II / Rincón de Santa Maria, na 
SE Rincón de Santa Maria.
10.2. Qualquer modificação de ponto de 
referência deverá ser acordada entre 
ambas as partes.
10.3. O fluxo de energia nas Interligações 
Garabi – Rincón deve ser controlado pelos 
Centros de Operação do ONS e da 
CAMMESA, de forma a respeitar os 
procedimentos e limites estabelecidos nos 
módulos 3 e 5 deste Regulamento.
10.CONTROL DE LOS FLUJOS DE 
INTERCAMBIO DE ENERGÍA 
10.1. Para monitorear, en tiempo real, los 
flujos de importación o exportación de 
energía, las siguientes referencias deben 
ser consideradas.
10.1.1. En el lado de Brasil:
•en la salida de la LAT 500 kV 
Conversora Garabí I / Rincón de Santa 
María, en Conversora Garabí I;
•en la salida de la LAT 500 kV 
Conversora Garabí II / Rincón de 
Santa María, en Conversora Garabí II.
10.1.2. En el lado de Argentina:
•en la salida de la LAT 500 kV 
Conversora Garabí I / Rincón de Santa 
María, en E.T. Rincón de Santa Maria;
•en la salida de la LAT 500 kV 
Conversora Garabí II / Rincón de 
Santa María, en E.T. Rincón de Santa 
María.
10.2. Cualquier modificación de punto de 
referencia deberá ser acordada entre 
ambas partes.
10.3. El flujo de energía en las 
interconexiones Rincón – Garabí debe ser 
controlado por los Centros de Operaciones 
del ONS y de la CAMMESA, respetando 
los procedimientos y límites establecidos 
en los módulos 3 y 5 de este Reglamento.
11.REGULAÇÃO DOS NÍVEIS DE TENSÃO 11.REGULACIÓN DE LOS NIVELES DE 
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
8
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 13 de 15
11.1. O controle de tensão de cada Interligação 
deverá ser efetuado de maneira a manter os 
níveis de tensão dos Sistemas Brasileiro e 
Argentino dentro de suas respectivas faixas 
permitidas, conforme procedimentos e valores 
estabelecidos no Módulo 5 deste Regulamento.
TENSIÓN
11.1.El control de tensión en cada Interconexión 
deberá ser efectuado de manera de 
mantener los niveles de tensión de los 
Sistemas Brasileño y Argentino dentro de 
sus respectivas bandas permitidas, 
conforme procedimientos y valores 
establecidos en el Módulo 5 de este 
Reglamento.
12.ANÁLISE DA OPERAÇÃO E DE 
PERTURBAÇÕES NA INTERLIGAÇÃO
12.1. A operação de cada Interligação deverá 
ser analisada diariamente pela CAMMESA 
e pelo ONS. As tratativas para análise 
diária, de perturbações e/ou troca de 
informações, deverão ser realizadas 
conforme procedimentos especificados no 
Módulo 3 deste Regulamento.
12.ANÁLISIS DE LA OPERACIÓN Y DE 
PERTURBACIONES EN LA 
INTERCONEXIÓN
12.1.La operación de cada Interconexión 
deberá ser analizada diariamente por 
CAMMESA y por el ONS. Las tratativas 
para el análisis diario, de las 
perturbaciones y/o el intercambio de 
informaciones, deberán ser realizadas 
conforme a los procedimientos 
especificados en el Módulo 3 de este 
Reglamento.
13.SOLUÇÃO DE CONTROVÉRSIAS 
TÉCNICO-OPERACIONAIS
13.1.As partes farão seus melhores esforços 
para resolver todas as divergências de 
caráter técnico-operacional.
13.2.Uma divergência técnico-operacional que 
não puder ser resolvida pelos 
representantes designados será 
considerada uma Controvérsia 
Operacional.
13.3.Constatada uma Controvérsia 
Operacional, as Gerências das áreas 
envolvidas do ONS e da CAMMESA terão 
um prazo de até 10 dias úteis da 
13.SOLUCIÓN DE CONTROVERSIAS 
TÉCNICO-OPERATIVAS
13.1.Las partes harán sus mejores esfuerzos 
para resolver todas las divergencias de 
carácter técnico-operativas.
13.2.Una divergencia técnico-operativa que 
no pueda ser resuelta por los 
representantes designados será 
considerada una Controversia 
Operativa.
13.3.Una vez constatada una Controversia 
Operativa, las Gerencias de las áreas 
asociadas del ONS y de CAMMESA 
tendrán un plazo de hasta diez días 
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
8
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 14 de 15
constatação da Controvérsia para 
solucioná-la. Caso essa não seja 
solucionada pelas Gerências respectivas, 
a controvérsia deverá ser encaminhada 
para ambas as Diretorias, que poderão se 
reunir em Brasília ou em Buenos Aires, ou 
em qualquer outro lugar acordado, para 
solucionar a Controvérsia.
hábiles de la constatación de una 
Controversia para solucionarla. En caso 
de que no sea solucionada por las 
Gerencias, el problema deberá ser 
elevado a ambas Autoridades, que 
podrán reunirse en Brasilia o en Buenos 
Aires, o en cualquier otro lugar acordado, 
para solucionar la Controversia.
14.DISPOSIÇÕES GERAIS
14.1.Toda vez que uma das partes julgar 
necessário proceder a revisão do presente 
Regulamento, poderá convocar reunião.
14.2.Toda modificação do Regulamento será 
feita por meio de substituição desse, que 
se denominará “Revisão Nº _____ do 
Módulo específico do Regulamento 
Internacional de Operação ONS / 
CAMMESA", aprovado pelas partes. A 
revisão do Regulamento poderá ser 
realizada por cada Módulo 
individualmente.
14.DISPOSICIONES GENERALES
14.1.Toda vez que una de las partes juzgue 
necesario proceder a la revisión del 
presente Reglamento, podrá convocar a 
una reunión conjunta.
14.2.Toda modificación del Reglamento será 
realizada por medio de una sustitución, la 
que se denominará “Revisión Nº _____ 
del Módulo específico del Reglamento 
Internacional de Operación CAMMESA / 
ONS", aprobado por las partes. La 
revisión del Reglamento podrá ser 
realizada por cada Módulo 
individualmente.
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
8
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 15 de 15
15.APROVAÇÃO
O Operador Nacional do Sistema Elétrico – ONS e 
a Compañía Administradora del Mercado 
Mayorista Elétrico S.A. – CAMMESA, por 
intermédio de seus representantes autorizados, 
firmam o Normativo Geral, que compreende o 
Módulo 1 do presente Regulamento, redigido em 
Espanhol e em Português, ambos os textos de 
mesmo teor e validade, dentro do Convênio de 
Intercâmbio e Cooperação entre o ONS e a 
CAMMESA, firmados no dia 19 de março de 2024.
Este documento foi assinado 
digitalmente por Arthur da Silva Santa 
Rosa.
Para verificar as assinaturas, vá ao site 
https://portalassinaturas.ons.org.br e 
utilize o código 2472-09D3-6C65-DD60. 
___________________________________
__
Eng. Arthur da Silva Santa Rosa
Gerente Executivo de Procedimentos e 
Desenvolvimento da Operação do ONS
15.APROBACIÓN
El Operador Nacional del Sistema Eléctrico – 
ONS y la Compañía Administradora del 
Mercado Mayorista Eléctrico S.A. – 
CAMMESA, por medio de sus representantes 
autorizados, firman la Normativa General, 
que comprende el Módulo 1 del presente 
Reglamento, redactado en idioma español y 
en idioma portugués, ambos textos del mismo 
tenor y validez, dentro del Convenio de 
Intercambio y Cooperación entre el ONS y 
CAMMESA, firmados en el día 19 de marzo 
de 2024.
__________________________________
__
Ing. Jorge Siryi
Gerente de Operaciones de CAMMESA

