 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.11 
 
Cadastro de Informações Operacionais. 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE TRANSMISSÃO E 
TRANSFORMADORES DA INTERLIGAÇÃO NORTE/ SUDESTE 
 
Código Revisão Item Vigência 
CD-CT.NSE.02 31 2.2.1 10/08/2023 
 
 
MOTIVO DA REVISÃO 
Transferência das seguintes LTs e equipamentos para a Área 230 kV do Tocantins: 
LT 230 kV Dianópolis II / Palmas C1.  
LTs 230 KV Lajeado / Palmas C1 e C2. 
LT 230 kV Dianópolis II / Gurupi. 
Autotransformador AT01 de 500/230/13,8 kV - 450 MVA da SE Gurupi. 
Autotransformadores DDAT6-01 e 02 230/138/13,8 kV - 200 MVA SE Dianópolis II  
Autotransformador PLAT6-01 e 02 230/138/13,8 kV - 200 MVA, SE Palmas. 
Autotransformadores T1 e T2 525/230/13,8 kV - 960 MVA da SE Lajeado  
 
Transferência da LT 230 kV Barreiras II / Dianópolis II para a Área 230 kV Sudoeste da região Nordeste (2SO). 
 
  
 
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-NCO ELETRONORTE ENERGISA SOLUCOES ENERGISA TOCANTINS 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA 
INTERLIGAÇÃO NORTE/ SUDESTE 
CD-CT.NSE.02 31 2.2.1 10/08/2023 
 
Referência:  2 / 13 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
ENERPEIXE FURNAS INTESA INVESTCO TAESA 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA 
INTERLIGAÇÃO NORTE/ SUDESTE 
CD-CT.NSE.02 31 2.2.1 10/08/2023 
 
Referência:  3 / 13 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 4 
2. CONCEITOS ................................ ................................ ................................ ................................ ... 4 
3. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 6 
4. LIMITES DE CARREGAMENTO DE LINHAS DE TRANSMISSÃO ................................ ...........................  7 
4.1. LT 500 kV Gurupi / Miracema C1 ................................................................................................... 7 
4.2. LT 500 kV Gurupi / Miracema C2 ................................................................................................... 7 
4.3. LT 500 kV Gurupi / Miracema C3 ................................................................................................... 8 
4.4. LT 500 kV Gurupi / Peixe 2 ............................................................................................................. 8 
4.5. LT 500 kV Lajeado / Miracema C1 ................................................................................................. 9 
4.6. LT 500 kV Lajeado / Miracema C2 ................................................................................................. 9 
4.7. LT 500 kV Peixe 2 / Serra da Mesa 2 ............................................................................................ 10 
4.8. LT 500 kV Peixe Angical / Peixe 2 ................................................................................................ 10 
4.9. LT 500 kV Serra da Mesa / Gurupi C1 .......................................................................................... 11 
4.10. LT 500 kV Serra da Mesa / Gurupi C2 .......................................................................................... 11 
4.11. LT 500 kV Serra da Mesa / Serra da Mesa 2 ................................................................................ 12 
5. LIMITES DE CARREGAMENTO DE TRANSFORMADORES ................................ ................................  13 
5.1. SE Peixe Angical – Autotransformador AT1 de 525/138/13,8 kV - 525 MVA ............................. 13 
 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA 
INTERLIGAÇÃO NORTE/ SUDESTE 
CD-CT.NSE.02 31 2.2.1 10/08/2023 
 
Referência:  4 / 13 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
1. OBJETIVO 
Apresentar os limites operacionais de linhas de transmissão e de transfo rmadores da Rede de Operação a 
serem controlados pelos operadores dos Centros de Operação do ONS e pela Operação dos Agentes 
envolvidos. 
2. CONCEITOS 
2.1. Limite Operacional: Corresponde ao limite de carregamento do equipamento que deve ser observado 
pelo ONS e pelos Agentes, em condição normal de operação e ou em condição de emergência. 
2.2. Alteração dos Limites Operacionais: É a alteração de limite na qual são estabelecidos novos valores, 
decorrentes de eventos que alteraram a capacidade operativa do equipamento, como por exemplo 
recapacitação, substituição de TC, modificação de característica de proteção, indisponibili dade do 
sistema de refrigeração, flexibilização por parte do Agente, etc. 
2.3. Fator Limitante:  Fator que impede um equipamento de atingir um limite operacional superior ao 
estabelecido de projeto. Um fator limitante ativo provoca aumento no custo de operação ou de 
expansão do SIN. 
2.4. Limites de linhas de transmissão 
2.4.1. Condição Normal de Operação 
Situação de carregamento em que a linha de transmissão conduz continuamente corrente em valor igual ou 
inferior ao seu valor operacional, em sua condição de carregamento nominal, para a qual foi projetada. 
2.4.2. Condição de Emergência de Operação 
Situação de carregamento em que a linha de transmiss ão conduz corrente acima do seu valor operacional, 
em sua condição de carregamento superior ao nominal para a qual foi projetada, por um tempo não superior 
a 4 dias (96 horas) contínuos, ou um somatório máximo de 18 dias (432 horas) intermitentes (se somados 
em base anual todos os períodos contínuos inferiores a 4 dias). 
As linhas de transmissão que se enquadrarem nessa situação terão o campo duração nas tabelas 
apresentadas neste cadastro preenchido com (*). 
2.4.3. Condição Sazonal de Operação 
Situação de carregamento em que a linha de transmissão transporta continuamente corrente em valor igual 
ou inferior ao seu valor operacional, em condição normal de operação e ou em condição de emergência, em 
condições de operação temporárias e específicas para diferentes períodos do ano, tais como verão -dia, 
verão-noite, inverno-dia e inverno-noite ou outro período específico do ano (meses ou estações do ano). 
2.4.4. Limite Diurno  
Limite operacional a ser considerado para o período diurno. O período diurno é o intervalo entre o horário 
do amanhecer e do crepúsculo, conforme disposto nos Procedimentos de Rede. 
Quando o período não for definido pelo agente , deve ser considerado o período entre às 06h00min até às 
17h59, sempre tomando como referência o horário oficial de Brasília. 
2.4.5. Limite Noturno  
Limite operacional a ser considerado para o período noturno. O período noturno é o intervalo de tempo 
entre o horário do crepúsculo e do amanhecer, conforme disposto nos Procedimentos de Rede. 
Quando o período não for definido pelo agente , deve ser considerado o período d as 18h00 até às 05h59, 
sempre tomando como referência o horário oficial de Brasília. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA 
INTERLIGAÇÃO NORTE/ SUDESTE 
CD-CT.NSE.02 31 2.2.1 10/08/2023 
 
Referência:  5 / 13 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
2.5. Limites de Transformadores 
2.5.1. Condição Normal de Operação  
Situação de carregamento em que o transformador conduz continuamente até a  sua corrente nominal. 
Mesmo que em algum momento seja ultrapassada a sua corrente nominal, não deve ser excedida a 
temperatura máxima do óleo e ou do enrolamento, conforme limites estabelecidos na NBR 5356-7, 
respeitadas as condições, valores e prazos admitidos pelo agente. 
2.5.2. Condição de Emergência de Longa Duração  
Situação de carregamento em que o transformador conduz continuamente uma corrente superior à sua 
corrente nominal, com duração não superior a 4 (quatro) horas. Mesmo que em algum momento seja 
ultrapassada a sua corrente de emergência de longa duração, não deve ser excedida a temperatura máxima 
do óleo e ou do enrolamento, conforme limites estabele cidos na NBR 5356-7, respeitadas as condições, 
valores e prazos admitidos pelo agente. 
2.5.3. Condição de Emergência de Curta Duração  
Situação de carregamento em que o transformador conduz continuamente uma corrente superior à sua 
corrente de emergência de longa duração, com duração não superior a 30 (trinta) minutos. Após esse 
intervalo de tempo, deve-se retornar à condição de carregamento de longa duração. 
Mesmo que em algum momento seja ultrapassada a sua corrente de emergência de curta duração, não deve 
ser excedida a temperatura máxima do óleo e ou do enrolamento, conforme limites estabelecidos na NBR 
5356-7, respeitadas as condições, valores e prazos admitidos pelo agente. Esta condição deve ser utilizada 
como último recurso antes de se efetuar corte de carga. 
2.6. Preenchimento das Tabelas de Limites 
2.6.1. Linhas de Transmissão 
O campo “Valor Operacional” deve ser preenchido com o valor limite do equipamento a ser considerado na 
operação em tempo real. 
O campo “Duração (hh:mm)” indica o período admissível para operação da linha de transmissão na condição 
de emergência. 
Nas tabelas em que for detalhado o tempo de operação em determinada condição de operação de 
equipamento, deve-se utilizar tantas linhas quanto forem necessárias para o detalhamento de carregamento 
por período de tempo informado pelo Agente. 
Os campos “Valor Operacional” e “Duração” das tabelas, cujos valores dependem de informação do Agente 
e não estão disponíveis para o ONS, devem ser preenchidas com “NI” (Não Informado pelo Agente). 
O campo “Valor Operacional” das tabelas , referentes a valores de emergência que não são permitidos pelo 
Agente, devem ser preenchidos com “NP” (Não Permitido pelo Agente). Para esse caso, também deve ser 
preenchido o campo “Fator Limitante”. 
O campo “Período do Ano” indica, quando for o caso, a sazonalidade anual definida para determinação dos 
valores operacionais em condição normal e de emergência. 
O campo “Período do Dia” indica, quando for o caso, a sazonalidade diária definida para determinação dos 
valores operacionais em condição normal e de emergência. 
2.6.2. Transformadores 
O campo “Enrolamento (kV)” indica o terminal e a tensão de referência ai quais  está relacionado o limite 
operacional nas condições normal, emergência de longa duração e emergência de curta duração. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA 
INTERLIGAÇÃO NORTE/ SUDESTE 
CD-CT.NSE.02 31 2.2.1 10/08/2023 
 
Referência:  6 / 13 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
O campo “Valor Operacional (A)” indica o valor de corrente limite do transformador para condição normal, 
emergência de longa duração e de curta duração , considerando como tensão nominal a indicada no campo 
“Enrolamento (kV)”. 
O campo “Duração  (hh:mm)” indica o período admissível para operação do transformador na condição de 
emergência de longa e emergência de curta duração. 
Nas tabelas em que for detalhado o tempo de operação em determinada condição de operação de 
equipamento, deve-se utilizar tantas linhas quanto forem necessárias para o detalhamento de carregamento 
por período de tempo informado pelo Agente. 
Os campos “Valor Operacional” e “Duração” das tabelas, cujos valores dependem de informação do Agente 
e não estão disponíveis para o ONS, devem ser preenchidas com “NI” (Não Informado pelo Agente). 
O campo “Valor Operacional” das tabelas referentes a valores de emergência , que não são permitidos pelo 
Agente, devem ser preenchidos com “NP” (Não Permitido pelo Agente). Para esse caso, também deve ser 
preenchido o campo “Fator Limitante”. 
3. CONSIDERAÇÕES GERAIS 
3.1. Este Cadastro de Informações Operacionais apresenta os limites operacionais de carregamento d as 
linhas de transmissão e dos transformadores da Rede de Operação, de acordo com as definições 
estabelecidas entre ANEEL, ONS e agentes, cujo processo de atualização é descrito pela RO-CD.BR.01 
- Controle de Limites de Carregamento de Linhas de Transmissão e Transformadores. 
3.2. Os operadores dos Centros de Operação do ONS, nas ações de coordenação, supervisão e controle 
devem observar os limites operacionais constantes neste Cadastro de Informações Operacionais. 
3.3. A operação dos agentes, em suas ações de comando e execução, deve observar os limites 
operacionais constantes neste Cadastro de Informações Operacionais. 
3.4. Cabe ao agente comunicar de imediato ao Centro de Operação com o qual se relaciona qualquer 
alteração de limite que imponha restrições à operação de linha de transmissão ou de transformador. 
3.5. Toda e qualquer alteração nos valores de limites operacionais constantes neste Cadastro de 
Informações Operacionais deverá ser solicitada formalmente e devidamente justificada e 
caracterizada pelo agente responsável pelo equipamento  ao Centro de Operação do ONS de seu 
relacionamento.  
3.6. Caso a operação em tempo real venha a verificar qualquer possibilidade de violação do limite 
estabelecido para o equipamento, caberá ao Centro de Operação do ONS responsável tomar as 
medidas corretivas cabíveis , para que esse não ultrapasse esses valores, que só podem ser 
ultrapassados com anuência do agente. 
3.7. Os valores de Limites Operacionais dos equipamentos da Rede de Operação, constantes neste 
Cadastro de Informações Operacionais, são os valores informados oficial mente e formalmente pelo 
agente. 
3.8. Os limites de transformadores que constam neste Cadastro de Informações Operacionais consideram 
todos os estágios de refrigeração disponíveis. Os limites considerando os estágios de refrigeração 
indisponíveis constam nos Cadastros de Informações Operacionais de Dados de Equipamentos. 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA 
INTERLIGAÇÃO NORTE/ SUDESTE 
CD-CT.NSE.02 31 2.2.1 10/08/2023 
 
Referência:  7 / 13 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
4. LIMITES DE CARREGAMENTO DE LINHAS DE TRANSMISSÃO 
4.1. LT 500 KV GURUPI / MIRACEMA C1 
Limites de condição normal de operação (Longa duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 2.250 Capacitor série fixo 
01/Jan a 31/Dez Diurno / 
Noturno 2.856 Cabo Condutor, capacitores série fixos baipassados 
 
Limites de condição de emergência (Curta duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 3.150 04:00 Equipamentos na SE Gurupi; capacitores série 
fixos baipassados. 
01/Jan a 31/Dez Diurno / 
Noturno 2.700 (1) 04:00 Capacitor série fixo 
01/Jan a 31/Dez Diurno / 
Noturno 3.037 (2) 00:30 Capacitor série fixo 
Notas: 
(1) - Capacidade de sobrecarga por 4 horas, em um período de 12 horas.. 
(2) - Capacidade de sobrecarga por 30 minutos, em um  período de 06 horas.. 
 
4.2. LT 500 KV GURUPI / MIRACEMA C2 
Limites de condição normal de operação (Longa duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 2.250 
Capacitores série fixos 
Operação no limite, após 02 horas: Alarme 
Operação no limite, após 08 horas: Baipasse (trip) 
01/Jan a 31/Dez Diurno / 
Noturno 3.150 Ajuste de Proteção, capacitores séries fixos baipassados 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA 
INTERLIGAÇÃO NORTE/ SUDESTE 
CD-CT.NSE.02 31 2.2.1 10/08/2023 
 
Referência:  8 / 13 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
Limites de condição de emergência (Curta duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 3.038 00:30 Capacitor série fixo 
01/Jan a 31/Dez Diurno / 
Noturno 3.150 (*) Ajuste de proteção, capacitores série fixos 
baipassados 
 
4.3. LT 500 KV GURUPI / MIRACEMA C3 
Limites de condição normal de operação (Longa duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 2.250 Capacitores série fixos 
01/Jan a 31/Dez Diurno / 
Noturno 3.000 Cabo condutor, capacitores séries fixos baipassados 
 
Limites de condição de emergência (Curta duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 3.000 (1) 00:30 Cabo condutor 
Notas: 
(1) - O limite de emergência para os bancos capacitores série da LT 500 kV Gurupi / Miracema C3 é de 3038 
A. 
 
4.4. LT 500 KV GURUPI / PEIXE 2 
Limites de condição normal de operação (Longa duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 2.250 Capacitor Série Fixo 
01/Jan a 31/Dez Diurno / 
Noturno 3.292 Cabo condutor, capacitor série fixo baipassado 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA 
INTERLIGAÇÃO NORTE/ SUDESTE 
CD-CT.NSE.02 31 2.2.1 10/08/2023 
 
Referência:  9 / 13 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
Limites de condição de emergência (Curta duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 3.038 00:30 Capacitor Série Fixo 
01/Jan a 31/Dez Diurno / 
Noturno 3.292 (*) Cabo condutor, capacitor série fixo baipassado 
 
4.5. LT 500 KV LAJEADO / MIRACEMA C1 
Limites de condição normal de operação (Longa duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 2.000 TC 
 
Limites de condição de emergência (Curta duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno NP - - 
 
4.6. LT 500 KV LAJEADO / MIRACEMA C2 
Limites de condição normal de operação (Longa duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 2.355 - 
 
Limites de condição de emergência (Curta duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 2.945 (*) - 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA 
INTERLIGAÇÃO NORTE/ SUDESTE 
CD-CT.NSE.02 31 2.2.1 10/08/2023 
 
Referência:  10 / 13 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
4.7. LT 500 KV PEIXE 2 / SERRA DA MESA 2 
Limites de condição normal de operação (Longa duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 2.500 Capacitor Série Fixo 
01/Jan a 31/Dez Diurno / 
Noturno 3.000 Cabo condutor, capacitor série fixo baipassado 
 
Limites de condição de emergência (Curta duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 3.375 00:30 Capacitor Série Fixo 
01/Jan a 31/Dez Diurno / 
Noturno 3.000 (*) Cabo condutor, capacitor série fixo baipassado 
 
4.8. LT 500 KV PEIXE ANGICAL / PEIXE 2 
Limites de condição normal de operação (Longa duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 3.292 - 
 
Limites de condição de emergência (Curta duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno NP - - 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA 
INTERLIGAÇÃO NORTE/ SUDESTE 
CD-CT.NSE.02 31 2.2.1 10/08/2023 
 
Referência:  11 / 13 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
4.9. LT 500 KV SERRA DA MESA / GURUPI C1 
Limites de condição normal de operação (Longa duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 2.000 Capacitores série fixo inseridos 
01/Jan a 31/Dez Diurno / 
Noturno 2.856 Bancos de capacitores série fixo baipassados 
 
Limites de condição de emergência (Curta duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 3.000 04:00 TC em Serra da Mesa e bancos de capacitores 
série fixo baipassados 
01/Jan a 31/Dez Diurno / 
Noturno 2.400 (1) 04:00 Banco de capacitores série fixo inseridos 
01/Jan a 31/Dez Diurno / 
Noturno 2.700 (2) 00:30 Banco de capacitores série fixo inseridos 
Notas: 
(1) - Capacidade de sobrecarga por 4 horas, em um período de 12 horas. 
(2) - Capacidade de sobrecarga por 30 minutos, em um período de 06 horas. 
 
4.10. LT 500 KV SERRA DA MESA / GURUPI C2 
Limites de condição normal de operação (Longa duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 2.000 
Capacitor série fixo e/ou variável 
Operação no limite, após 02 horas: Alarme 
Operação no limite, após 08 horas: Baipasse (trip) 
01/Jan a 31/Dez Diurno / 
Noturno 3.150 Ajuste de Proteção, capacitor série fixo e variável 
baipassados 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA 
INTERLIGAÇÃO NORTE/ SUDESTE 
CD-CT.NSE.02 31 2.2.1 10/08/2023 
 
Referência:  12 / 13 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
Limites de condição de emergência (Curta duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 2.700 00:30 Capacitor série fixo e ou variável 
01/Jan a 31/Dez Diurno / 
Noturno 3.150 (*) Ajuste de Proteção, capacitor série fixo e 
variável baipassados 
 
4.11. LT 500 KV SERRA DA MESA / SERRA DA MESA 2 
Limites de condição normal de operação (Longa duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 3.150 - 
 
Limites de condição de emergência (Curta duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 3.150 (*) - 
 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA 
INTERLIGAÇÃO NORTE/ SUDESTE 
CD-CT.NSE.02 31 2.2.1 10/08/2023 
 
Referência:  13 / 13 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
5. LIMITES DE CARREGAMENTO DE TRANSFORMADORES 
5.1. SE PEIXE ANGICAL – AUTOTRANSFORMADOR AT1 DE 525/138/13,8 KV - 525 MVA 
Limites de Condição Normal de Operação 
Enrolamento (kV) Valor 
operacional (A) Fator limitante 
138 2.196 - 
525 577 - 
 
Limites de Condição de Emergência de Longa Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
138 NP - - 
525 NP - - 
 
Limites de Condição de Emergência de Curta Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
138 NP - - 
525 NP - - 
 
