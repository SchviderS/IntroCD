 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Curitiba Leste 
 
 
Código Revisão Item Vigência 
IO-OI.S.CTL 12 3.7.5.1. 25/09/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Alteração das condições de autonomia do Agente para energização, desvinculando os procedimentos do 
Subitem 6.2.2 dos procedimentos de recomposição fluente da Instalação constantes no Subitem 5.2.2, 
passando-se a adotar, no Subitem 6.2.2, condições de sistema completo e/ou sistema N -1 para energização 
das seguintes linhas de transmissão e equipamentos: 
• LT 230 kV Curitiba Leste / Distrito Industrial de São José dos Pinhais; 
• LT 230 kV Curitiba Leste / Posto Fiscal; 
• LT 230 kV Curitiba Leste / Uberaba. 
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S Copel GeT 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Curitiba Leste IO-OI.S.CTL 12 3.7.5.1. 25/09/2024 
 
Referência:  2 / 13 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  5 
3.1. Barramento de 525 kV ................................................................................................................... 5 
3.2. Barramento de 230 kV ................................................................................................................... 5 
3.3. Alteração da Configuração dos Barramentos ................................................................................ 5 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 5 
4.1. Procedimentos Gerais ................................................................................................................... 5 
4.2. Procedimentos Específicos ............................................................................................................ 6 
4.2.1. Operação dos Bancos de Capacitores ......................................................................... 6 
4.2.2. Operação dos Comutadores de Tape Sob Carga (LTC) ................................................ 6 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  6 
5.1. Procedimentos Gerais ................................................................................................................... 6 
5.2. Procedimentos para Recomposição Fluente ................................................................................. 6 
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................ 6 
5.2.2. Recomposição Fluente da Instalação .......................................................................... 7 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 8 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 8 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 8 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 8 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 8 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 9 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 9 
6.1. Procedimentos Gerais ................................................................................................................... 9 
6.2. Procedimentos Específicos .......................................................................................................... 10 
6.2.1. Desenergização de Equipamentos ............................................................................ 10 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ..................................... 11 
7. NOTAS IMPORTANTES ................................ ................................ ................................ ................ 13 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Curitiba Leste IO-OI.S.CTL 12 3.7.5.1. 25/09/2024 
 
Referência:  3 / 13 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE Curitiba Leste, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são somente aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da Instalação , devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR-S e a Instalação, no que se refere aos equipamentos de 
manobra da Instalação, é realizada conforme segue: 
Linha de Transmissão ou 
Equipamento  Agente de Operação Agente Operador Centro de Operação 
do Agente Operador 
Barramento de 525 kV Consórcio Marumbi Copel GeT COGT Copel 
Barramento de 230 kV Consórcio Marumbi Copel GeT COGT Copel 
LT 525 kV Curitiba / Curitiba 
Leste Consórcio Marumbi Copel GeT COGT Copel 
LT 525 kV Blumenau / Curitiba 
Leste Copel GeT Copel GeT COGT Copel 
LT 230 kV Curitiba Leste / 
Distrito Industrial de São José 
dos Pinhais 
Copel GeT Copel GeT 
COGT Copel 
LT 230 kV Curitiba Leste / 
Posto Fiscal Copel GeT Copel GeT COGT Copel 
LT 230 kV Curitiba Leste / Santa 
Mônica Copel GeT Copel GeT COGT Copel 
LT 230 kV Curitiba Leste / 
Uberaba Copel GeT Copel GeT COGT Copel 
Transformador BTF A 
525/230/13,8 kV Consórcio Marumbi Copel GeT COGT Copel 
Banco de Capacitores BC -1 
230 kV Consórcio Marumbi Copel GeT COGT Copel 
Banco de Capacitores BC -2 
230 kV Consórcio Marumbi Copel GeT COGT Copel 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Curitiba Leste IO-OI.S.CTL 12 3.7.5.1. 25/09/2024 
 
Referência:  4 / 13 
 
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte das seguintes áreas elétricas: 
Linha de Transmissão ou Equipamento Área Elétrica 
Banco de Capacitores BC-1 230 kV 
230 kV do Paraná  
Banco de Capacitores BC-2 230 kV 
Barramento de 525 kV 525 kV da Região Sul 
Barramento de 230 kV 230 kV do Paraná 
LT 525 kV Curitiba / Curitiba Leste 
525 kV da Região Sul 
LT 525 kV Blumenau / Curitiba Leste 
LT 230 kV Curitiba Leste / Distrito Industrial de 
São José dos Pinhais 
230 kV do Paraná LT 230 kV Curitiba Leste / Posto Fiscal 
LT 230 kV Curitiba Leste / Santa Mônica 
LT 230 kV Curitiba Leste / Uberaba 
Transformador BTF A 525/230/13,8 kV 525 kV da Região Sul 
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.4.1. A definição da quantidade  de tentativas de religamento manual de linha de transmissão ou de 
equipamento, bem como o intervalo entre elas, é de responsabilidade do Agente e deve m estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica. 
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR -S autorização 
para religamento. Nessa oportunidade, o Agente pode solicitar t ambém a alteração no sentido 
normal para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em consideração as condições operativas do 
sistema. 
2.5. Quando caracterizado impedimento de linha de transmissão  ou de equipamento , deve m ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica caso a manobra seja realizada sob coordenação do COSR-S, ou os procedimentos descritos 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Curitiba Leste IO-OI.S.CTL 12 3.7.5.1. 25/09/2024 
 
Referência:  5 / 13 
 
no Subitem 6.2.2. desta Instrução de Operação quando o Agente  tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 525 KV 
A configuração do barramento de 525 kV (Barra BP -1 e Barra BP -2) é do tipo Disjuntor e Meio. Na 
operação normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados. 
3.2. BARRAMENTO DE 230 KV 
A configuração do barramento de 230 kV é do tipo Barra Dupla (Barra P1 e Barra P2) a Quatro Chaves. 
Na operação normal desse barramento , todos os disjuntores e seccionadoras devem estar fechados, 
exceto as seccionadoras de transferência e uma das seletoras de barra das linhas de transmissão  ou 
equipamentos, operando da seguinte forma: 
Em uma das barras – P1 (ou P2) Na outra barra – P2 (ou P1) 
Banco de Capacitores BC-1 230 kV (ou BC-2); 
LT 230 kV Curitiba Leste / Uberaba (LT 230 kV 
Curitiba Leste / Distrito Industrial de São José 
dos Pinhais); 
LT 230 kV Curitiba Leste / Posto Fiscal (ou LT 
230 kV Curitiba Leste / Santa Mônica); 
Transformador BTF A 525/230/13,8 kV. 
Banco de Capacitores BC-2 230 kV (ou BC-1); 
LT 230 kV Curitiba Leste / Distrito Industrial de São 
José dos Pinhais (ou LT 230 kV Curitiba Leste / 
Uberaba); 
LT 230 kV Curitiba Leste / Santa Mônica (ou LT 
230 kV Curitiba Leste / Posto Fiscal). 
3.3. ALTERAÇÃO DA CONFIGURAÇÃO DOS BARRAMENTOS 
A mudança de configuração do s barramentos de 525 kV e de 230 kV  desta Instalação é executada com 
controle do COSR-S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade do Agente Operador 
da Instalação. 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. Os barramentos de 525 kV e  de 230 kV, pertencente s à Rede de Operação, t êm a sua regulação de 
tensão controlada pelo COSR-S.  
As faixas para controle de tensão para os barramentos de 525 kV e de  230 kV estão estabelecidas no 
Cadastro de Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob a responsabilidade da Instalação. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Curitiba Leste IO-OI.S.CTL 12 3.7.5.1. 25/09/2024 
 
Referência:  6 / 13 
 
4.2. PROCEDIMENTOS ESPECÍFICOS 
4.2.1. OPERAÇÃO DOS BANCOS DE CAPACITORES 
A manobra dos bancos de capacitores BC-1 e BC-2 230 kV – 100 Mvar é executada sob controle do COSR-S. 
4.2.2. OPERAÇÃO DOS COMUTADORES DE TAPE SOB CARGA (LTC) 
Os LTCs do transformador BTF A 525/230/13,8 kV operam em modo manual. 
A movimentação dos comutadores é realizada pela Copel GeT com controle do COSR-S. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação : caracterizado quando não há tensão em todos os terminais de 
suas linhas de transmissão. 
• Desligamento parcial da Instalação : qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao COSR -S 
as seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2., sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia da operação da instalação na recomposição, 
deve ser utilizado o Subitem 5.3.  
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE  
No caso de desligamento total, o Agente Operador da Instalação deve configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Curitiba Leste IO-OI.S.CTL 12 3.7.5.1. 25/09/2024 
 
Referência:  7 / 13 
 
Abrir ou manter abertos os disjuntores:  
• das linhas de transmissão:  
LT 525 kV Blumenau / Curitiba Leste; 
LT 525 kV Curitiba / Curitiba Leste; 
LT 230 kV Curitiba Leste / Santa Mônica; 
LT 230 kV Curitiba Leste / Distrito Industrial de São José dos Pinhais; 
LT 230 kV Curitiba Leste / Posto Fiscal; 
LT 230 kV Curitiba Leste / Uberaba. 
• do transformador: 
BTF A (lados de 525 kV e de 230 kV). 
• da interligação de módulos: 
LT 525 kV Curitiba / Curitiba Leste e transformador BTF A 525/230/13,8 kV; 
LT 525 kV Blumenau / Curitiba Leste e Barra BP-2 525 kV. 
• dos bancos de capacitores: 
BC-1 230 kV; 
BC-2 230 kV. 
Fechar ou manter fechado os disjuntores: 
• dos módulos da LT 525 kV Blumenau / Curitiba Leste; 
• do módulo de interligação de barras de 230 kV, exceto quando esse estiver substituindo o disjuntor 
de um equipamento ou de uma linha de transmissão. 
Desligar ou manter desligado o modo de comutação automática dos comutadores sob carga da 
transformação 525/230/13,8 kV da SE Curitiba Leste. 
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO 
A Instalação faz parte da recomposição da Área Gov. Ney Aminthas de Barros Braga . O Agente 
Operador da Instalação deve adotar os procedimentos a seguir para recomposição fluente: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 
Copel GeT 
Receber tensão da SE Curitiba, pela LT 525 kV 
Curitiba / Curitiba Leste, e ligar, energizando o 
barramento de 525 kV. 
 
1.1 
Energizar, pelo lado de 525 kV, o Transformador 
BTF A 525/230/13,8 kV da SE Curitiba Leste e 
energizar o barramento de 230 kV. 
TAPCTL-525/230 = 11. 
1.2 
Energizar a LT 230 kV Curitiba Leste / Uberaba, 
enviando tensão para SE Uberaba. 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Curitiba Leste IO-OI.S.CTL 12 3.7.5.1. 25/09/2024 
 
Referência:  8 / 13 
 
Passo Executor Procedimentos Condições ou Limites Associados 
1.3 
Energizar a LT 230 kV Curitiba Leste / Posto 
Fiscal, enviando tensão para SE Posto Fiscal. 
VCTL-230 ≤ 240 kV. 
Após fluxo de potência ativa na LT 
230 kV Curitiba Leste / Uberaba. 
1.4 
Energizar a LT 230 kV Curitiba Leste / Distrito 
Industrial de São José dos Pinhais, enviando 
tensão para SE Distrito Industrial de São José dos 
Pinhais. 
Após fluxo de potência ativa: 
• na LT 230 kV Curitiba Leste / 
Uberaba, e; 
• na LT 230 kV Curitiba Leste / 
Posto Fiscal. 
2 
Receber tensão da SE Santa Mônica, pelo 
segundo circuito da LT 230 kV Curitiba Leste / 
Santa Mônica, e ligar, em anel. 
 
Os demais equipamentos desta Instalação são restabelecidos sob controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2., enquanto não houver 
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia da operação da instalação 
na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras.  
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, o 
Agente Operador deve preparar a Instalação conforme Subitem 5.2 .1., sem necessidade de 
autorização do ONS. 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão nas barras de 525 kV, e 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Curitiba Leste IO-OI.S.CTL 12 3.7.5.1. 25/09/2024 
 
Referência:  9 / 13 
 
• ausência de fluxo de potência ativa nas linhas de transmissão de 525 kV, os Agentes Operadores 
devem preparar o setor de 525 kV da Instalação (disjuntores das linhas de transmissão e 
equipamentos) conforme Subitem 5.2.1., sem necessidade de autorização do ONS.  
5.4.1.2. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão nas barras de 230 kV, os Agentes Operadores devem preparar o setor de 230 
kV da Instalação (disjuntores das linhas de transmissão e equipamentos) conforme Subitem 
5.2.1., sem necessidade de autorização do ONS.  
5.4.1.3. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador 
deve recompor a Instalação conforme Subitem 5.2.2., sem necessidade de autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitens 5.4.1.2. e 5.4.1.3., os Agentes 
Operadores devem informar ao COSR-S, para que a recomposição da Instalação seja executada com 
controle do COSR-S. 
5.4.2.3. Caracterizado desligamento parcial da Instalaçã o, conforme Subitem 5.4.1.4., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 6.2.2., sem necessidade de 
autorização do ONS. 
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
os Agentes Operadores devem informar ao COSR -S, para que a recomposição da Instalação seja 
executada com controle do COSR-S. 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, após desligamento 
programado, de urgência ou de emergência, só podem ser efetuados com controle do COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelo Agente Operador da Instalação quando 
explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução de Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir autonomia, 
a energização deve ser executada com controle do COSR -S, conforme Instrução de Operação de 
Preparação para Manobras da respectiva área elétrica. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Curitiba Leste IO-OI.S.CTL 12 3.7.5.1. 25/09/2024 
 
Referência:  10 / 13 
 
6.1.4. Antes do fechamento de qualquer disjuntor, o Agente Operador da Instalação deve verificar se existe 
tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pelo Agente Operador  da Instalação 
quando estiver especificado nesta Instrução de O peração e estiverem atendidas as condições do 
Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou , quando permitida, em sentido  inverso, pode ser feita com 
autonomia pela operação do Agente Operador, conforme procedimentos para manobras que estão 
definidos nesta Instrução de Operação, Subitem 6.2.2 . Os procedimentos para energização 
controlados pelo COSR-S estão definidos na Instrução de Operação de Preparação para Manobras 
da respectiva área elétrica.  
 A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados  na Instalação, durante execução de intervenções,  
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal  / lado pelo qual a linha de transmissão ou transformador é 
energizado, o Agente Operador da Instalação deve fechá-lo em anel desde que tenha autonomia para 
tal, adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S.  
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de linha 
de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle do 
COSR-S. 
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE EQUIPAMENTOS 
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação, é 
sempre controlada pelo COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Curitiba Leste IO-OI.S.CTL 12 3.7.5.1. 25/09/2024 
 
Referência:  11 / 13 
 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de equipamentos ou de linhas de transmissão. 
O Agente Operador  da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado nas 
condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Linha de Transmissão / 
Equipamento  Procedimentos Condições ou Limites Associados 
LT 525 kV Blumenau / 
Curitiba Leste 
Sentido Normal: SE Curitiba Leste recebe tensão da SE Blumenau 
Ligar, em anel, a LT 525 kV Blumenau 
/ Curitiba Leste. 
• VCTL-525 ≤ 541 kV; 
• VCTL-230 ≤ 237 kV; e 
• ∆δ ≤ 22°. 
Sentido Inverso: SE Curitiba Leste envia tensão para a SE Blumenau 
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.5SU. 
LT 525 kV Curitiba / 
Curitiba Leste 
Sentido Normal: SE Curitiba Leste recebe tensão da SE Curitiba 
Ligar, em anel, a LT 525 kV Curitiba / 
Curitiba Leste. 
 
 
Sentido Inverso: SE Curitiba Leste envia tensão para a SE Curitiba 
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.5SU. 
LT 230 kV Curitiba 
Leste / Distrito 
Industrial de São José 
dos Pinhais 
Sentido Normal: SE Curitiba Leste envia tensão para SE Distrito Industrial de 
São José dos Pinhais 
Energizar a LT 230 kV Curitiba Leste / 
Distrito Industrial de São José dos 
Pinhais. 
Sistema completo (de LT e TR) ou N-1 
(de LT ou TR) na SE Curitiba Leste 230 
kV 
• VCTL-230 ≤ 242 kV. 
Sentido Inverso: SE Curitiba Leste recebe tensão da SE Distrito Industrial de 
São José dos Pinhais 
Ligar, em anel, a LT 230  kV Curitiba 
Leste / Distrito Industrial de São José 
dos Pinhais. 
 
LT 230 kV Curitiba Sentido Normal: SE Curitiba Leste envia tensão para a SE Posto Fiscal 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Curitiba Leste IO-OI.S.CTL 12 3.7.5.1. 25/09/2024 
 
Referência:  12 / 13 
 
Linha de Transmissão / 
Equipamento  Procedimentos Condições ou Limites Associados 
Leste / Posto Fiscal Energizar a LT 230  kV Curitiba Leste / 
Posto Fiscal. 
Sistema completo (de LT e TR) ou N-1 
(de LT ou TR) na SE Curitiba Leste 230  
kV 
• VCTL-230 ≤ 240 kV. 
Sentido Inverso: SE Curitiba Leste recebe tensão da SE Posto Fiscal 
Ligar, em anel, a LT 230  kV Curitiba 
Leste / Posto Fiscal. 
• ∆δ ≤ 20°. 
LT 230 kV Curitiba 
Leste / Santa Mônica 
Sentido Normal: SE Curitiba Leste recebe tensão da SE Santa Mônica 
Ligar, em anel, a LT 230 kV Curitiba 
Leste / Santa Mônica. 
• ∆δ ≤ 10°. 
Sentido Inverso: SE Curitiba Leste envia tensão para a SE Curitiba 
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2PR. 
LT 230 kV Curitiba 
Leste / Uberaba 
Sentido Normal: SE Curitiba Leste envia tensão para a SE Uberaba 
Energizar a LT 230  kV Curitiba Leste / 
Uberaba. 
Sistema completo (de LT e TR) ou N-1 
(de LT ou TR) na SE Curitiba Leste 230  
kV 
• VCTL-230 ≤ 242 kV. 
Sentido Inverso: SE Curitiba Leste recebe tensão da SE Uberaba 
Ligar, em anel, a LT 230  kV Curitiba 
Leste / Uberaba. 
 
Transformador BTF A 
525/230/13,8 kV 
Sentido Normal:  partir do lado de 525 kV 
Energizar pelo lado de 525 kV o 
Transformador BTF A 
525/230/13,8 kV. 
• VCTL-525 ≤ 550 kV. 
• A LT 525 kV Curitiba / Curitiba 
Leste deve estar com fluxo de 
potência ativa ou energizando o 
barramento de 525 kV. 
Ligar, em anel, o lado de 230 kV do 
Transformador BTF A 525/230/13,8 
kV. 
• ∆δ ≤ 12°. 
Sentido Inverso: A partir do lado de 230 kV 
Energizar pelo lado de 230 kV o 
Transformador BTF A 
525/230/13,8 kV. 
Sistema completo (de LT) na SE Curitiba 
Leste 230 kV 
• VCTL-230 ≤ 242 kV. 
Ligar, em anel, o lado de 525 kV do 
Transformador BTF A 
525/230/13,8 kV. 
• ∆δ ≤ 12°. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Curitiba Leste IO-OI.S.CTL 12 3.7.5.1. 25/09/2024 
 
Referência:  13 / 13 
 
Linha de Transmissão / 
Equipamento  Procedimentos Condições ou Limites Associados 
Banco de Capacitores 
BC-1 ou BC-2 230 kV A manobra destes Bancos de Capacitores é controlada pelo COSR-S. 
7. NOTAS IMPORTANTES 
7.1. Os disjuntores de interligação de módulos devem ser fechados, em anel, toda vez que forem 
recompostos os dois circuitos do mesmo vão ou quando, num dos lados do vão, for uma barra. 
 
