Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.13
MOTIVO DA REVISÃO 
 Adequação do processo e atribuições para cadastro de unidades geradoras e transformadores elevadores 
de usinas hidráulicas e térmicas, e de compensadores síncronos, alterando e incluindo os seguintes Subitens: 
3.3, 3.8, 4.4, 4.6, 5.2.10, 5.2.11, 5.3.5, 5.3.6, 5.4.5, 6.7.1, 6.7.3 e 6.8.7.
LISTA DE DISTRIBUIÇÃO
CNOS COSR-SE COSR-NE COSR-NCO COSR-S
Rotina Operacional
Manutenção da Base de Dados Técnica
Código Revisão Item Vigência
RO-SC.BR.03 06 4.4.1. 19/07/2024
Manual Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
MANUTENÇÃO DOS DADOS ELÉTRICOS DA BASE DE 
DADOS TÉCNICA RO-SC.BR.03 05 4.4.1 MINUTA
Referência: . 2 / 12
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. REFERÊNCIAS ...................................................................................................................................3
3. CONCEITOS ......................................................................................................................................3
4. CONSIDERAÇÕES GERAIS .................................................................................................................3
5. DESCRIÇÃO DO PROCESSO ...............................................................................................................5
5.1. Cadastro de Agentes, funções de transmissão, pontos de conexão à Rede Básica e 
equipamentos reservas .................................................................................................................5
5.2. Cadastro de Novas Instalações de Geração ..................................................................................6
5.3. Cadastro de Novas Instalações de Transmissão............................................................................7
5.4. Atualização do Cadastro de Instalações de Geração e Transmissão já existentes na BDT............8
5.5. Definição de Siglas das Instalações ...............................................................................................9
6. ATRIBUIÇÕES DO ONS......................................................................................................................9
6.1. Gerência de Arquitetura e Dados - AGD........................................................................................9
6.2. Gerência de Contratos e Contabilização da Transmissão - SAC ..................................................10
6.3. Gerência de Planejamento Elétrico de Médio Prazo - PLM.........................................................10
6.4. Gerência de Planejamento Elétrico de Curto Prazo - PLC e Gerências de Planejamento Elétrico – 
PLS e PLN .....................................................................................................................................10
6.5. Gerência de Estudos Energéticos - PEE .......................................................................................11
6.6. Gerência de Engenharia de Instalações - EGI ..............................................................................11
6.7. Gerência de Estudos Especiais – EGE e Gerências de Engenharia – EGS e EGN..........................11
6.8. Gerência de Relação com Programação, Operação e Apuração - RSO .......................................11
6.9. Gerência de Procedimentos Operativos - PDP ............................................................................12
6.10. Gerência de Configurações de Rede - PDR..................................................................................12
7. ORIENTAÇÕES TÉCNICAS COMPLEMENTARES.................................................................................12
Manual Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
MANUTENÇÃO DOS DADOS ELÉTRICOS DA BASE DE 
DADOS TÉCNICA RO-SC.BR.03 05 4.4.1 MINUTA
Referência: . 3 / 12
1. OBJETIVO
Formalizar a divisão de competência das gerências responsáveis pelo cadastro e manutenção de dados na 
Base de Dados Técnica – BDT, os quais são utilizados por mais de uma gerência no ONS.
2. REFERÊNCIAS
Não se aplica.
3. CONCEITOS
3.1. BDT – Base de Dados Técnica que contém os dados cadastrais dos Agentes, de suas instalações e de 
seus equipamentos. 
3.2. GERCAD – Sistema de Gerenciamento do Cadastro da BDT. 
3.3. GERSINC – Gerador síncrono que está habilitado a operar como compensador síncrono.
3.4. SADEPE – Sistema de Aquisição de Dados Externos para o Planejamento Energético. O sistema é 
responsável pela coleta de informações externas ao ONS, necessárias à elaboração do Planejamento 
da Operação Energética. É utilizado pela Gerência de Estudos Energéticos - PEE.
3.5. SACT – Sistema de Administração dos Contratos de Transmissão. É utilizado pela Gerência de 
Contratos e Contabilização da Transmissão - SAC.
3.6. SGBDIT – Sistema de Gestão da Base de Dados das Instalações de Transmissão. O ONS recebe os 
dados referentes às instalações de transmissão, que subsidiam os processos de fornecimento pelos 
agentes dos dados para a Base de Dados das Instalações de Transmissão – BDIT e de análise e 
aprovação da conformidade das características “como efetivamente implantadas”. É utilizado pela 
Gerência de Engenharia de Instalações – EGI.
3.7. SGIntegração – Sistema de Gestão de Integração de Novas Instalações. O sistema realiza a gestão dos 
processos de modalidade de operação de centrais geradoras e de integração de novas instalações ao 
SIN. É utilizado pela Gerência de Integração e Acesso - SAA. 
3.8. Usinas Convencionais – São usinas hidráulicas e térmicas de tipo I ou II-A.
4. CONSIDERAÇÕES GERAIS
4.1. Todos os dados cadastrados na BDT devem estar respaldados em documentos formais dos órgãos 
reguladores nacionais, dos Agentes ou do ONS.
4.2. Todas as áreas do ONS citadas neste documento usarão o GERCAD como ferramenta de gestão 
(inclusão, alteração ou exclusão) de dados na BDT, exceto:
•PEE, que utiliza o SADEPE;
•SAC, que utiliza o SACT para inserção de dados cadastrais. 
•SAA, que utiliza o SGIntegração para inserção de dados relacionados à modalidade de operação de 
usinas.
Manual Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
MANUTENÇÃO DOS DADOS ELÉTRICOS DA BASE DE 
DADOS TÉCNICA RO-SC.BR.03 05 4.4.1 MINUTA
Referência: . 4 / 12
4.3. Cada gerência é responsável pelo processo de manter atualizado um conjunto de dados na BDT, de 
acordo com os Procedimentos de Rede de sua responsabilidade. Utiliza-se o horizonte temporal de 
entrada em operação comercial do empreendimento como referência. 
4.4. A PLM, a PEE e a SAC, com uma antecedência de 5 (cinco) anos até 6 (seis) meses da data prevista de 
entrada do empreendimento, são responsáveis pelo cadastro inicial, modificações e correções na BDT 
dos dados dos Agentes, das instalações, linhas de transmissão e equipamentos elétricos, que estão 
no horizonte do PAR. 
A PLM, no âmbito dos Submódulos 3.1, 7.1 e 7.3 dos Procedimentos de Rede, trabalha com o 
horizonte do PAR, ou seja, a partir de 5 (cinco) anos até 6 (seis) meses de antecedência da data 
prevista de entrada em operação do empreendimento. 
A RSO, no âmbito do Submódulo 7.9 dos Procedimentos de Rede, com uma antecedência de 6 (seis) 
meses da data prevista de entrada do empreendimento, continua a realização do cadastro dos dados 
na BDT. 
O cadastro dos equipamentos listados abaixo é uma pré-condição para o funcionamento de diversos 
sistemas do ONS:
•Linhas de transmissão de corrente alternada aéreas e subterrâneas;
•Linhas de transmissão de corrente contínua aéreas;
•Conversores, eletrodos, polos e bipolos;
•Transformadores de potência;
•Transformadores elevadores de usinas convencionais e de compensadores síncronos, quando 
solicitado;
•Transformadores de aterramento;
•Reatores série e em derivação;
•Capacitores série e em derivação;
•Disjuntores e chaves seccionadoras (inclusive a chave de aterramento);
•Compensadores estáticos;
•Unidades geradoras das usinas convencionais, quando solicitado;
•Compensadores síncronos;
•Filtros de harmônico passivo.
4.5. Com no mínimo 6 (seis) meses de antecedência da entrada em operação do equipamento, devem ser 
confirmados ou definidos na BDT, os seguintes itens para os quais cada equipamento se enquadra:
•O tipo de rede (complementar, supervisão ou simulação), cuja classificação está sob 
responsabilidade da Gerência de Configurações de Rede – PDR e das gerências de Planejamento 
Elétrico – PLC, PLN e PLS;
•O tipo de malha elétrica (sistêmica ou regional) para os equipamentos da Rede de Operação. A 
gerência responsável para essa classificação é a Gerência de Procedimentos Operativos – PDP, 
em conjunto com a Gerência Executiva da Operação Nacional do Sistema - OS. Será definido 
como malha elétrica local todos os equipamentos que não estão na Rede de Operação;
•A modalidade de operação da usina (Tipo I, Tipo II-A, Tipo II-B, Tipo II-C ou Tipo III), cuja 
Manual Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
MANUTENÇÃO DOS DADOS ELÉTRICOS DA BASE DE 
DADOS TÉCNICA RO-SC.BR.03 05 4.4.1 MINUTA
Referência: . 5 / 12
classificação está sob responsabilidade da Gerência de Integração e Acesso – SAA;
•A área elétrica, conforme definidas na Rotina de Organização e Numeração dos Documentos do 
Manual de Procedimentos da Operação (RO-MP.BR.01). A gerência responsável para essa 
classificação é a Gerência de Procedimentos Operativos – PDP;
•A área de controle (Sul, Sudeste, Norte/Centro-Oeste ou Nordeste), conforme definido no 
Cadastro de Áreas do Controle Automático de Geração do Sistema Interligado Nacional (CD-
CT.BR.03). A gerência responsável para essa classificação é a Gerência de Configurações de Rede 
– PDR, através do Parecer de Acesso emitido pelo ONS;
4.6. No período de 5 (cinco) anos de antecedência até a data prevista de entrada do empreendimento, a 
EGE, a EGN e a EGS são responsáveis por manter atualizados os dados de estudos dinâmicos na BDT 
para geradores convencionais (hidráulica e térmica), compensadores síncronos (GERSINC ou não) e 
seus respectivos transformadores elevadores, nos horizontes do PAR e do curto prazo, para os 
empreendimentos já cadastrados na BDT.
4.7. Os nomes e dados das instalações, dos equipamentos e das linhas de transmissão fornecidos 
oficialmente pelos Agentes à ANEEL são inseridos na BDT conforme homologados por esta Agência. 
Alterações de nomes ou dados que modifiquem os contratos de origem, somente são atualizados na 
BDT após homologação pela ANEEL.
4.8. Equipamentos pertencentes exclusivamente à Rede de Simulação, que precisam ser cadastrados na 
BDT para utilização em algum outro sistema, devem ser cadastrados e mantidos pela área 
responsável por esse dado. 
5. DESCRIÇÃO DO PROCESSO
5.1. CADASTRO DE AGENTES, FUNÇÕES DE TRANSMISSÃO, PONTOS DE CONEXÃO À REDE BÁSICA E 
EQUIPAMENTOS RESERVAS
5.1.1. O cadastro e a manutenção de dados sobre novos Agentes na BDT é realizado pela SAC com a 
utilização do aplicativo SACT. A antecedência para cadastro é de até 5 (cinco) anos em relação à data 
prevista para a entrada em operação, sendo que:
•para os agentes de Transmissão, o cadastro é realizado na BDT após a assinatura dos contratos de 
concessão;
•para os demais agentes, o cadastro ocorre após a emissão do Parecer de Acesso e assinatura do 
CUST ou por solicitação da PLM ou da PEE.
5.1.2. A PDR fará a gestão na BDT do cadastro dos agentes operadores utilizando o SACT, a ser 
disponibilizado pela SAC.
5.1.3. A SAC também cadastra na BDT as funções de transmissão (FT) e os pontos de conexão à Rede Básica. 
Para tal, utiliza como referência os contratos de uso do sistema de transmissão, CUST, e os contratos 
de prestação de serviços de transmissão, CPST.
5.1.4. Toda vez que a SAC tomar conhecimento de que há alteração na razão social, sigla ou identificador 1 
de um Agente ou até mesmo a exclusão do nome dele, essa gerência deve informar à PDP, à PDR, à 
1 No SACT, o termo sigla refere-se ao nome curto do Agente (até 20 caracteres alfanuméricos), usado nos relatórios internos do ONS. 
Na BDT o que se denomina sigla é formado pela sigla do estado da federação + a sigla da instalação. 
Manual Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
MANUTENÇÃO DOS DADOS ELÉTRICOS DA BASE DE 
DADOS TÉCNICA RO-SC.BR.03 05 4.4.1 MINUTA
Referência: . 6 / 12
RSO, à PLM e à EGI para que sejam tomadas providências visando a compatibilização das informações 
da BDT e da BDIT relativas ao Agente.
5.1.5. O cadastro e a manutenção dos dados sobre os equipamentos reservas serão inseridos na BDT pela 
SAC, quando constantes em Resoluções Autorizativas, Contratos de Concessão ou por solicitação do 
agente proprietário da instalação.
5.1.6. Para o cadastro e a manutenção dos dados sobre equipamentos reserva, a SAC deve observar os 
critérios determinados nos Contratos de Prestação de Serviços de Transmissão - CPST e no submódulo 
8.3 dos Procedimentos de Rede.
5.2. CADASTRO DE NOVAS INSTALAÇÕES DE GERAÇÃO
5.2.1. O cadastro e a manutenção de dados das usinas hidráulicas e térmicas simuladas individualmente 
nos modelos energéticos é realizado pela PEE, dentro do aplicativo SADEPE. A antecedência para o 
cadastro é de até 5 (cinco) anos da data prevista para a entrada em operação, sendo que somente 
são cadastradas as que já foram leiloadas e têm avaliação positiva de entrada em operação dada pelo 
Departamento de Monitoramento do Sistema Elétrico - DMSE.
5.2.2. Os dados cadastrados no SADEPE utilizam tabelas restritas e são insumos para o Programa Mensal de 
Operação e para o estudo de Planejamento da Operação Energética. As exportações dos dados destas 
tabelas restritas para as tabelas do GERCAD são executadas pela Gerência de Relação com a 
Programação, Operação e Apuração - RSO.
5.2.2.1. Mensalmente, quando da realização do PMO, a PEE disponibiliza a atualização das datas de previsão 
de entrada em operação comercial bem como potência de unidades geradoras futuras. Os dados 
iniciais das instalações de geração são cadastrados no SGIntegração a partir da emissão do Parecer 
de Acesso e/ou classificação da Modalidade de Operação da Usina.
5.2.3. A PLM cadastra na BDT as instalações de geração do Tipo I e Tipo II-A (exceto as unidades geradoras), 
com antecedência de 3 (três) anos a 6 (seis) meses da data de entrada em operação dessa instalação, 
a partir de informações disponíveis nos leilões de geração e Pareceres de Acesso. Excepcionalmente, 
o cadastro de usinas hidráulicas de grande porte pode ocorrer com até 5 (cinco) anos de 
antecedência, à medida que a PLM vai recebendo informações sobre o empreendimento.
A PLM cadastra, conforme Submódulo 2.2 dos Procedimentos de Rede, os dados mais gerais das 
instalações, como: 
•Agente principal;
•Coordenadas geográficas;
•Sigla preliminar da instalação;
•Data prevista de energização;
•Parâmetros de projeto dos equipamentos, caso já disponíveis;
•Dados das outorgas relacionadas;
•Motivo da implementação da nova instalação etc. 
5.2.4. A continuação do cadastro com informações necessárias para o Sistema de Supervisão e Controle das 
usinas hidráulicas e térmicas do Tipo I e do Tipo II-A é de responsabilidade da RSO. Esse cadastro é 
pré-requisito para que se possa dar início as atividades de atualização da base de dados do Sistema 
Manual Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
MANUTENÇÃO DOS DADOS ELÉTRICOS DA BASE DE 
DADOS TÉCNICA RO-SC.BR.03 05 4.4.1 MINUTA
Referência: . 7 / 12
de Supervisão e Controle, necessário para consolidação da Planilha padrão dos Agentes (Rotina RO-
SC.BR.04), no mínimo 30 dias em relação à entrada em operação dos equipamentos, conforme 
estabelecido no submódulo 7.9. A continuação do cadastro se refere, por exemplo, a todos os 
equipamentos das usinas, que não as próprias unidades geradoras: transformadores elevadores, 
barramentos, linhas de transmissão. Além disso, são cadastradas informações que não se possui no 
momento do cadastro da usina no SADEPE, como por exemplo agente operador, etc. 
5.2.5. O cadastro de usinas do Tipo II-B é realizado através da criação de uma carga que representará a 
injeção de potência destas usinas. O cadastro é feito dessa forma para esse tipo de usina, devido aos 
requisitos de supervisão e controle estabelecidos no submódulo 2.12. A gerência interessada no 
cadastro completo das usinas hidráulicas e térmicas do Tipo II-B e do Tipo III deve ser responsável por 
essa atividade.
5.2.6. O cadastro das usinas classificadas como Tipo II-C é realizado pela RSO, conforme Submódulo 2.12.
5.2.7. As usinas Tipo III são cadastradas pela Gerência de Previsão de Carga (PEC) para os processos de carga 
global.
Para o cadastro destas instalações na BDT são utilizadas as informações do SGIntegração, diagramas 
unifilares, informações dos Agentes e informações da CCEE.
5.2.8. Sempre que houver a definição da modalidade de uma usina ou sua reclassificação, a SAA deve 
disponibilizar no site do ONS a classificação de modalidade de operação das usinas, conforme 
preconiza o Submódulo 7.2 dos Procedimentos de Rede
5.2.9. O modelo elétrico das instalações das usinas Tipo I e Tipo II-A, tais como topologia, são inseridos na 
BDT pela RSO. 
5.2.10. Os dados de estudos dinâmicos das unidades geradoras e os dados dos transformadores elevadores 
de usinas e de compensadores síncronos (GERSINC) são inseridos na BDT pelas Gerências de 
Engenharia - EGE, EGN ou EGS.
5.2.11. Para os equipamentos, cujo cadastro seja de responsabilidade da RSO, a EGE, EGN, EGS, PEE, PLM 
e/ou PEM deverão enviar à RSO os dados de parâmetros elétricos e de características de 
equipamentos (por exemplo tabelas de TAP) necessários para o cadastro na BDT, assim que os 
receberem, seja, no momento anterior à confecção dos relatórios de sua competência ou em 
qualquer momento quando da atualização desses.
5.3. CADASTRO DE NOVAS INSTALAÇÕES DE TRANSMISSÃO
5.3.1. O cadastro das instalações e equipamentos de transmissão da Rede Básica e Demais Instalações da 
Transmissão (DIT), que constam do PAR, dos editais e autorizações da ANEEL ou que já estão 
consolidadas pelo MME ou ainda que possuam CPST, será realizado pela PLM, com o uso do GERCAD. 
A antecedência para realização deste cadastro varia de 5 (cinco) anos a 6 (seis) meses da data prevista 
de entrada em operação da instalação ou equipamento.
5.3.2. O cadastro das instalações e equipamentos de transmissão da Rede de Operação que constam de 
Pareceres de Acesso será realizado pela PLM, com o uso do GERCAD. Esse processo é iniciado a partir 
de emissão mensal e formal, no mês corrente, da lista dos documentos emitidos no decorrer do mês 
anterior pela SAA. Os prazos estão relacionados às datas de entrada em operação comercial 
identificadas nesses Pareceres, podendo ser atualizados pelo acompanhamento de obras do DMSE / 
MME.
Manual Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
MANUTENÇÃO DOS DADOS ELÉTRICOS DA BASE DE 
DADOS TÉCNICA RO-SC.BR.03 05 4.4.1 MINUTA
Referência: . 8 / 12
5.3.3. A PLM cadastra os dados das instalações, conforme submódulo 2.2, tais como:
•agente principal;
•coordenadas geográficas;
•sigla preliminar da instalação;
•data prevista de energização;
•parâmetros de projeto dos equipamentos, caso já estejam disponíveis;
•dados das outorgas relacionadas;
•motivo da implementação da nova instalação etc.
5.3.4. No âmbito do PAR, competem também à PLM o cadastro e a manutenção dos dados sobre linhas de 
transmissão que interligam alguma subestação dentro do território nacional a uma subestação fora 
do país. Por se tratar de instalações da Rede de Operação, o processo, bem como os prazos de 
cadastro, são os mesmos do item 5.3.1.
5.3.5. Após o término de um estudo pré-operacional, a EGE, a EGS ou a EGN deve fornecer os parâmetros 
dos novos equipamentos, exceto os equipamentos mencionados no item 5.3.6, para fins de 
atualização da BDT, realizada pela RSO, forma a manter o modelo da rede elétrica coerente com o 
campo.
5.3.6. Após o término de um estudo pré-operacional envolvendo compensadores síncronos (não GERSINC) 
e seu respectivo transformador de acoplamento, a EGE, a EGS ou a EGN deve atualizar os dados de 
estudos dinâmicos desse equipamento na BDT.
5.3.7. O cadastro do modelo elétrico dos equipamentos de transmissão é feito na BDT pela RSO, assim como 
disponibilizar para o Tempo Real, respeitando o horizonte de até 6 (seis) meses antes da entrada do 
empreendimento.
5.4. ATUALIZAÇÃO DO CADASTRO DE INSTALAÇÕES DE GERAÇÃO E TRANSMISSÃO JÁ EXISTENTES NA 
BDT
5.4.1. A PLM  atualiza o cadastro das instalações e equipamentos de transmissão da Rede de Operação e 
Demais Instalações da Transmissão (DIT) que já constam do PAR, dos editais e autorizações da ANEEL 
ou que já estão consolidadas pelo MME ou ainda que possuem CPST. A antecedência para a realização 
deste cadastro varia de 5 (cinco) anos a 6 (seis) meses da data prevista de entrada em operação da 
instalação ou equipamento. Deve-se considerar que nesse momento as instalações e equipamentos 
ainda não foram disponibilizados para o Tempo real.
5.4.2. A PLC, a PLN ou a PLS a cada finalização do estudo quadrimestral deve informar os parâmetros 
alterados. Posteriormente a RSO efetuará o cadastro desses parâmetros no GERCAD e pode exportar 
as referidas alterações para o REGER.
5.4.3. A PLC, a PLN ou a PLS a cada finalização do estudo mensal deve informar os parâmetros alterados.  
Posteriormente a RSO efetuará o cadastro desses parâmetros no GERCAD e pode exportar as 
referidas alterações para o REGER.
5.4.4. A EGP, a EGN ou a EGS é responsável por manter atualizadas as informações e informar às áreas 
interessadas sobre esquemas de religamento automático de linhas de transmissão da rede de 
operação, quais sejam: data de instalação em cada terminal, data de ativação do esquema, data de 
Manual Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
MANUTENÇÃO DOS DADOS ELÉTRICOS DA BASE DE 
DADOS TÉCNICA RO-SC.BR.03 05 4.4.1 MINUTA
Referência: . 9 / 12
desativação do esquema, tipo de religamento (monopolar, tripolar ou ambos), tempo morto e 
terminal líder.
5.4.5. A EGE, EGN ou EGS deve atualizar alterações dos parâmetros das usinas convencionais, 
compensadores síncronos (GERSINC ou não) e seus respectivos transformadores elevadores 
existentes na BDT.
5.4.6. A PDR, com a participação da PLC, PLN ou PLS, é responsável por manter atualizada a classificação 
dos equipamentos quanto ao tipo de rede (Básica, Complementar, Supervisão e Simulação). A PDR é 
a responsável por atualizar o tipo de rede dos equipamentos na BDT.
5.5. DEFINIÇÃO DE SIGLAS DAS INSTALAÇÕES
5.5.1. Para a definição das siglas das instalações será adotado critério definido pela RO-MP.BR.05 – 
“Controle dos Diagramas Unifilares Operacionais”.
5.5.2. Para a definição de identificadores de equipamentos no REGER será adotado critério definido pela 
RO-SC.BR.01 – “Identificação dos Equipamentos e Medidas do REGER”.
5.5.3. O processo de definição de siglas das instalações pode começar pela Gerência da PLM, no cadastro 
inicial dos novos empreendimentos no horizonte do PAR. No caso de instalações fora da Rede de 
Operação, a sigla deve ser acordada entre a PDR e o Agente.
5.5.4. No caso de o processo começar pela PLM, como não há ainda a sigla do Agente, é formulada uma 
sugestão inicial de acordo com a disponibilidade na BDT. Essa sigla será posteriormente verificada 
pela PDR com o Agente, de modo a torná-la definitiva ou alterá-la.
6. ATRIBUIÇÕES DO ONS
6.1. GERÊNCIA DE ARQUITETURA E DADOS - AGD
6.1.1. Administrar o GERCAD, tendo, entre outras no âmbito da DOP, as seguintes atribuições:
•coordenar e supervisionar as ampliações, manutenções e aperfeiçoamentos do GERCAD;
•aprovar as especificações, os testes e homologação do GERCAD;
•monitorar o desempenho do GERCAD visando garantir sua disponibilidade e qualidade;
•controlar o acesso dos usuários para consulta à BDT através do GERCAD.
6.1.2. Administrar a BDT relativamente à sua integridade física e lógica, gestão de segurança ao acesso aos 
dados (login, senha etc.), disponibilidade ininterrupta, manutenção, da cópia de segurança (backup) 
e restauração de dados.
6.1.3. Analisar e executar as alterações e complementações solicitadas em estruturas das tabelas da BDT.
6.1.4. Comunicar às áreas afetadas as alterações e atualizações de estruturas de dados executadas.
6.1.5. Propor melhorias na BDT visando a preservação dos dados e facilidades ao usuário.
6.1.6. Analisar as solicitações de alterações e complementações na base de dados e realizar a modelagem 
necessária da estrutura de dados e a adequação dos programas computacionais para atendimento a 
BDT.
Manual Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
MANUTENÇÃO DOS DADOS ELÉTRICOS DA BASE DE 
DADOS TÉCNICA RO-SC.BR.03 05 4.4.1 MINUTA
Referência: . 10 / 12
6.2. GERÊNCIA DE CONTRATOS E CONTABILIZAÇÃO DA TRANSMISSÃO - SAC
6.2.1. Cadastrar novos Agentes na BDT considerando o horizonte de até 5 (cinco) anos à frente.
6.2.2. Manter atualizado o cadastro dos Agentes na BDT.
6.2.3. Informar à PDP e à PDR sobre alterações no cadastro de seus respectivos Agentes, tais como criação, 
exclusão e alteração de nome.
6.2.4. Cadastrar funções de transmissão, pontos de conexão e equipamentos reserva.
6.2.5. Definir a sigla do Agente na BDT.
6.2.6. Considerar como referência para o cadastro dos Agentes os contratos de uso do sistema de 
transmissão, CUST, e os contratos de prestação de serviço da transmissão, CPST.
6.3. GERÊNCIA DE PLANEJAMENTO ELÉTRICO DE MÉDIO PRAZO - PLM
6.3.1. Realizar o cadastro inicial na BDT das instalações de geração do Tipo I e Tipo II-A (exceto as unidades 
geradoras), com antecedência de 3 (três) anos a 6 (seis) meses da data de entrada em operação dessa 
instalação, a partir de informações disponíveis nos leilões de geração e Pareceres de Acesso. 
Excepcionalmente, o cadastro de usinas hidráulicas de grande porte pode ocorrer com até 5 (cinco) 
anos de antecedência.
6.3.2. Cadastrar as informações básicas de instalações e equipamentos de transmissão da Rede de 
Operação e das Demais Instalações de Transmissão (DIT) na BDT considerando o horizonte do PAR. 
Para instalações e equipamentos advindos de Pareceres de Acesso, os prazos estão relacionados às 
datas informadas nesses documentos ou atualizadas pelo DMSE / MME.
6.3.3. Definir a sigla prévia das instalações e demais codificações operacionais dos equipamentos.
6.3.4. Considerar como referência para o cadastro de instalações as obras consolidadas pelo Ministério de 
Minas e Energia – MME, os editais/autorizações da ANEEL, os CPSTs, o Plano de Ampliações e 
Reforços (PAR) e Pareceres de Acesso.
6.3.5. Manter atualizado, no que for de sua competência, o cadastro das instalações e equipamentos de 
geração, antes do início da operação desses. 
6.4. GERÊNCIA DE PLANEJAMENTO ELÉTRICO DE CURTO PRAZO - PLC E GERÊNCIAS DE PLANEJAMENTO 
ELÉTRICO – PLS E PLN
6.4.1. Interagir com os Agentes de forma a obter um novo conjunto de casos-base de referência no âmbito 
dos estudos quadrimestrais e listar as diferenças dos parâmetros e topologia entre os casos-base de 
referência dos quadrimestrais vigentes e os novos casos-base.
6.4.2. Enviar lista das diferenças aos representantes designados pelos Centros de Operação do ONS de 
forma que possam ser feitas as avaliações de mudanças de parâmetros do modelo da rede elétrica 
representada na BDT.
Manual Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
MANUTENÇÃO DOS DADOS ELÉTRICOS DA BASE DE 
DADOS TÉCNICA RO-SC.BR.03 05 4.4.1 MINUTA
Referência: . 11 / 12
6.5. GERÊNCIA DE ESTUDOS ENERGÉTICOS - PEE
6.5.1. Considerar como referência para o cadastro no SADEPE as obras consolidadas pelo Ministério de 
Minas e Energia – MME, cronograma de expansão do DMSE e os editais/autorizações da ANEEL.
6.5.2. Cadastrar via SADEPE as informações referentes às usinas térmicas e hidráulicas simuladas 
individualmente nos modelos energéticos, conforme Submódulo 2.4, considerando o horizonte de 
cinco anos à frente.
6.5.3. Manter atualizado, no que for de sua competência ou relativo às informações que dispuser, o 
cadastro das instalações e equipamentos de geração. 
6.6. GERÊNCIA DE ENGENHARIA DE INSTALAÇÕES - EGI
6.6.1. Cadastrar na BDT instalações de transmissão necessárias para que os Agentes possam realizar o 
respectivo envio de dados para a BDIT, caso essas instalações não estejam previamente cadastradas 
na BDT.
6.7. GERÊNCIA DE ESTUDOS ESPECIAIS – EGE E GERÊNCIAS DE ENGENHARIA – EGS E EGN 
6.7.1. Informar os parâmetros dos novos equipamentos, exceto os equipamentos mencionados no item 
5.3.6, a partir da distribuição dos relatórios pré-operacionais, pareceres técnicos e relatórios de 
comissionamento. Deve também realizar as eventuais alterações e/ou atualizações de parâmetros 
elétricos dos equipamentos, unidades geradoras convencionais e linhas de transmissão do SIN, a 
partir desses relatórios. 
6.7.2. Analisar as alterações de parâmetros dos equipamentos e linhas de transmissão identificadas pelos 
Centros de Operação do ONS a serem efetivadas na BDT, informando o seu “de acordo” com a 
atualização ou os motivos de não concordar com as mudanças solicitadas.
6.7.3. Inserir e atualizar na BDT os dados de estudos dinâmicos de unidades geradoras convencionais, 
compensadores síncronos (GERSINC ou não) e seus respectivos transformadores elevadores, a partir 
da distribuição dos relatórios pré-operacionais, pareceres técnicos e relatórios de comissionamento.
6.8. GERÊNCIA DE RELAÇÃO COM PROGRAMAÇÃO, OPERAÇÃO E APURAÇÃO - RSO
6.8.1. Realizar a modelagem topológica das redes elétrica e energética a ser representada na BDT, a fim de 
garantir o desempenho adequando dos Aplicativos Elétricos e Energéticos do REGER. 
6.8.2. Corrigir problemas ou erros de topologia apontados pelas equipes dos Centros de Operação do ONS, 
PDR e PDP.
6.8.3. Realizar o cadastro completo das instalações e equipamentos (instalação, estação e seus respectivos 
equipamentos) da Rede de Operação e Supervisão que não são âmbito do PAR.
6.8.4. Cadastrar instalações de geração Tipo II-C (instalação, estação e seus respectivos equipamentos, 
incluindo as unidades geradoras), que fazem parte de conjuntos de usinas. Continuar o cadastro das 
usinas Tipo I e Tipo II-A.
6.8.5. Continuar o cadastro das novas instalações e equipamentos, inicialmente cadastradas no âmbito do 
PAR, da Rede de Operação na BDT.
Manual Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
MANUTENÇÃO DOS DADOS ELÉTRICOS DA BASE DE 
DADOS TÉCNICA RO-SC.BR.03 05 4.4.1 MINUTA
Referência: . 12 / 12
6.8.6. Realizar cadastro/alterações de informações necessárias para a configuração das usinas pertencentes 
ao CAG. 
6.8.7. Realizar cadastro/atualizações dos parâmetros, no GERCAD, a partir da distribuição dos estudos 
encaminhados pela PLC, PLN e PLS e relatórios pré-operacionais, pareceres técnicos e relatórios de 
comissionamento, encaminhados pela EGE, EGN e EGS, exceto os dados de estudos dinâmicos de 
unidades geradoras convencionais, compensadores síncronos (GERSINC ou não) e seus respectivos 
transformadores elevadores.
6.9. GERÊNCIA DE PROCEDIMENTOS OPERATIVOS - PDP
6.9.1. Definir, em conjunto com a OSN, OSE, OSL, OSD e OSC, os equipamentos a constituírem as Redes de 
Operação Sistêmica e Regional.
6.10. GERÊNCIA DE CONFIGURAÇÕES DE REDE - PDR
6.10.1. Atualizar na BDT a classificação de equipamentos quanto ao tipo de rede, dentre Básica, 
Complementar, Supervisão e Simulação.
6.10.2. Acordar, juntamente com os Agentes, as siglas das instalações e demais codificações operacionais 
dos equipamentos para instalações da Rede de Operação e da Rede de Supervisão. 
6.10.3. Manter atualizados na BDT definição do tipo de malha Sistêmica ou Regional efetuada pela PDP.
6.10.4. Manter atualizados a modalidade de usinas e os dados de conjuntos.
6.10.5. Manter atualizados na BDT no que diz respeito a equipamentos, os dados referentes a data prevista 
para início da operação, data efetiva de início da operação, data de desativação Agente Operador e 
Agente de Operação.
7. ORIENTAÇÕES TÉCNICAS COMPLEMENTARES
Não se aplica.
