 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Gralha Azul 
 
 
Código Revisão Item Vigência 
IO-OI.S.GRL 27 3.7.5.4. 25/09/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Alteração das condições de autonomia do Agente para energização, desvinculando os procedimentos do 
Subitem 6.2.2 dos procedimentos de recomposição fluente da Instalação constantes no Subitem 5.2.2, 
passando-se a adotar, no Subitem 6.2.2, condições de sistema completo e/ou sistema N -1 para energização 
das seguintes linhas de transmissão e equipamentos: 
• LT 230kV Campo Comprido / Gralha Azul;  
• LT 230kV Gralha Azul / Repar; 
• Transformador TF-2 ou TF-3 230/18 kV da UTE Araucária, 
- Alteração das condições de autonomia do Agente para fechamento em anel, no Subitem 6.2.2, das seguintes 
linhas de transmissão e equipamentos: 
• LT 230kV Gralha Azul / Umbará. 
- Complementação do Subitem 5.4.2.2. para casos de desligamentos parciais. 
- Adequação à RT-OI.BR revisão 37, com destaque à inclusão dos Subitens 6.1.8 e 6.1.9. 
- Incorporação da parte correspondente da Mensagem Operativa “MOP/ONS 365-R/2024 - Alteração do 
Agente Operador da UTE Araucária”, acarretando as seguintes alterações: 
• Atualização da tabela do Subitem 2.2., 
• Atualização do Subitem 5.2.1. “Preparação da Instalação para Recomposição Fluente”. 
  
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S Copel GeT 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Gralha Azul IO-OI.S.GRL 27 3.7.5.4. 25/09/2024 
 
Referência:  2 / 9 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  4 
3.1. Barramento de 230 kV ................................................................................................................... 4 
3.2. Alteração da Configuração do Barramento ................................................................................... 4 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 4 
4.1. Procedimentos Gerais ................................................................................................................... 4 
4.2. Procedimentos Específicos ............................................................................................................ 5 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  5 
5.1. Procedimentos Gerais ................................................................................................................... 5 
5.2. Procedimentos para Recomposição Fluente ................................................................................. 5 
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................ 5 
5.2.2. Recomposição Fluente da Instalação .......................................................................... 6 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 6 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 6 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 6 
5.4. Procedimentos Após Desligamento Parcial da Instalação ............................................................ 6 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 6 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 7 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 7 
6.1. Procedimentos Gerais ................................................................................................................... 7 
6.2. Procedimentos Específicos ............................................................................................................ 8 
6.2.1. Desenergização de Linhas de Transmissão e de Equipamentos ................................. 8 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ....................................... 8 
7. NOTAS IMPORTANTES ................................ ................................ ................................ .................. 9 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Gralha Azul IO-OI.S.GRL 27 3.7.5.4. 25/09/2024 
 
Referência:  3 / 9 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE Gralha Azul, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pel o Agente Operador da Instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR -S a Instalação, no que se refere aos equipamentos de 
manobra na Instalação, é realizada conforme segue: 
Linha de Transmissão ou 
Equipamento Agente de Operação Agente Operador Centro de Operação 
do Agente Operador  
Barramento de 230 kV Copel GeT Copel GeT COGT Copel 
LT 230 kV Gralha Azul / 
Umbará Copel GeT Copel GeT COGT Copel 
LT 230 kV Campo Comprido / 
Gralha Azul Copel GeT Copel GeT COGT Copel 
LT 230 kV Gralha Azul / Repar Copel GeT Copel GeT COGT Copel 
LT 230 kV CSN/PR / Gralha 
Azul (*) CSN/PR Copel GeT COGT Copel 
Módulo de 230 kV do 
Transformador TF -1 
230/18 kV da UTE Araucária 
Copel GeT Âmbar Energia UTE Araucária (**) 
Módulo de 230 kV do 
Transformador TF -2 
230/18 kV da UTE Araucária 
Copel GeT Âmbar Energia UTE Araucária (**) 
Módulo de 230 kV do 
Transformador TF -3 
230/18 kV da UTE Araucária 
Copel GeT Âmbar Energia UTE Araucária (**) 
(*) Somente os equipamentos de manobra de 230 kV da LT 230 kV CSN/PR / Gralha Azul, pertence à Rede de 
Operação, conforme a Rotina RO-RD.BR.01. 
(**) A operação é realizada localmente na própria usina. 
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte da Área 230 kV do Paraná. 
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.4.1. A definição d a quantidade de tentativas de religamento manual de linhas de transmissão ou de 
equipamentos, bem como o intervalo entre elas, é de responsabilidade do Agente e devem estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Gralha Azul IO-OI.S.GRL 27 3.7.5.4. 25/09/2024 
 
Referência:  4 / 9 
 
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR -S autorização 
para religamento. Nessa oportunidade, o Agente também pode solicitar alteração no sentido normal 
para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em consideração as condições operativas do 
sistema. 
2.5. Quando caracterizado impedimento de linha de transmissão  ou de equipamento , deve m ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2 . desta Instrução de Operação quando o Agente tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 230 KV 
A configuração do barramento de 230 kV é do tipo Barra Dupla (Barra BP -1 e Barra BP-2) a Quatro Chaves. 
Na operação normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados, exceto 
as seccionadoras de transferência e uma das seletoras de barra das linhas de transmissão ou equipamentos, 
bem como, uma das barras deve estar desenergizada, com o disjuntor de interligação de barras aberto e as 
seccionadoras do módulo de interligação também abertas. 
3.2. ALTERAÇÃO DA CONFIGURAÇÃO DO BARRAMENTO 
A mudança de configuração do barramento de 230 kV desta Instalação é executada com controle do COSR -
S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade do Agente Operador 
da Instalação 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S. 
As faixas de controle de tensão para esse barramento estão estabelecidas no Cadastro de 
Informações Operacionais das Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. Os demais barramentos , não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob responsabilidade da Instalação. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Gralha Azul IO-OI.S.GRL 27 3.7.5.4. 25/09/2024 
 
Referência:  5 / 9 
 
4.2. PROCEDIMENTOS ESPECÍFICOS 
Não se aplica. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação : caracterizado quando não há tensão em todos os terminais 
de suas linhas de transmissão. 
• Desligamento parcial da Instalação : qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao COSR-S 
as seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2., sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia d o Agente Operador  da Instalação na 
recomposição, deve ser utilizado o Subitem 5.3.  
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE 
No caso de desligamento total, o Agente Operador deve configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• das linhas de transmissão: 
LT 230 kV Gralha Azul / Umbará; 
LT 230 kV Campo Comprido / Gralha Azul; 
LT 230 kV CSN/PR – Companhia Siderúrgica Nacional Paraná / Gralha Azul; 
LT 230 kV Gralha Azul / Repar. 
• dos transformadores:  
TF-1 (lado de 230 kV) da UTE Araucária; 
TF-2 (lado de 230 kV) da UTE Araucária; 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Gralha Azul IO-OI.S.GRL 27 3.7.5.4. 25/09/2024 
 
Referência:  6 / 9 
 
TF-3 (lado de 230 kV) da UTE Araucária. 
Cabe ao Agente Copel GeT informar ao COSR -S quando a configuração de preparação da Instalação não 
estiver atendida para o início da recomposição, independentemente de o equipamento ser próprio ou de 
outros agentes. Nesse caso, o COSR -S fará contato com os agentes envolvidos para identificar o motivo do 
não-atendimento e, após confirmação do Agente Copel GeT de que os barramentos estão com a configuração 
atendida, o COSR -S coordenará os procedimentos para recomposição, caso necessário, em função da 
configuração desta Instalação. 
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO 
A Instalação faz parte da recomposição da Área Gov. Ney Aminthas de Barros Braga. O Agente Operador deve 
adotar os procedimentos a seguir para a recomposição fluente: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 Copel GeT 
Receber tensão da SE Umbará, pela LT 230 kV 
Gralha Azul / Umbará , e ligar, energizando o 
barramento de 230 kV. 
O restabelecimento de carga na 
SE Gralha Azul está condicionado 
a autorização do COSR-S. 
1.1 Copel GeT 
Energizar a LT 230 kV Campo Comprido / Gralha 
Azul, enviando tensão para a SE Campo 
Comprido. 
 
1.2 Copel GeT 
Energizar a LT 230 kV Gralha Azul / Repar, 
enviando tensão para a SE Repar.  
Os demais equipamentos desta Instalação são restabelecidos sob controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2 ., enquanto não houver 
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia da operação da instalação 
na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, o 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Gralha Azul IO-OI.S.GRL 27 3.7.5.4. 25/09/2024 
 
Referência:  7 / 9 
 
Agente Operador  deve preparar a Instalação conforme Subitem 5.2.1 ., sem necessidade de 
autorização do ONS. 
5.4.1.2. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador 
deve recompor a Instalação conforme Subitem 5.2.2., sem necessidade de autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 6.2.2., sem necessidade de 
autorização do ONS. 
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
os Agentes Operadores devem informar ao COSR -S, para que a recomposição da Instalação seja 
executada com controle do COSR-S. 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, após 
desligamentos programados, de urgência ou de emergência, só podem ser efetuados com controle 
do COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelo Agente Operador da Instalação quando 
estiverem explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução de 
Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, o Agente Operador da Instalação deve verificar se existe 
tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pelo Agente Operador da Instalação 
quando estiver especificado nesta Instrução de Operação e estiverem atendidas as condições do 
Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou, quando permitida, em sentido inverso, pode ser feita com 
autonomia pela operação do Agente, conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Gralha Azul IO-OI.S.GRL 27 3.7.5.4. 25/09/2024 
 
Referência:  8 / 9 
 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica.  
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o operador da Instalação deve fechá -lo em anel desde que tenha autonomia para tal, 
adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S. 
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S. 
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação é sempre 
controlada pelo COSR-S. 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de unidades geradoras, de equipamentos ou de linhas de transmissão. 
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais unidades geradoras, linhas de transmissão e equipamentos em operação, 
conforme explicitado nas condições de energização para a manobra. 
Para desligamentos parciais, proceder conforme Subitem 5.4. 
Equipamento / Linha 
de Transmissão Procedimentos Condições ou Limites Associados 
LT 230kV Campo Sentido Normal: SE Gralha Azul envia tensão para SE Campo Comprido 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Gralha Azul IO-OI.S.GRL 27 3.7.5.4. 25/09/2024 
 
Referência:  9 / 9 
 
Equipamento / Linha 
de Transmissão Procedimentos Condições ou Limites Associados 
Comprido / Gralha 
Azul 
Energizar a LT 230kV Campo Comprido / 
Gralha Azul. 
Sistema completo (de LT) ou N-1 (de LT) 
na SE Gralha Azul 230 kV 
• VGRL-230 ≤ 242 kV. 
Sentido Inverso: SE Gralha Azul recebe tensão da SE Campo Comprido 
Ligar, em anel, a LT 230 kV Campo 
Comprido / Gralha Azul. 
• Δδ ≤ 10°. 
LT 230kV Gralha 
Azul / Repar 
Sentido Normal: SE Gralha Azul envia tensão para a SE Repar 
Energizar a LT 230 kV Gralha Azul / Repar. Sistema completo (de LT) ou N-1 (de LT) 
na SE Gralha Azul 230 kV 
• VGRL-230 ≤ 242 kV. 
Sentido Inverso: SE Gralha Azul recebe tensão da SE Repar 
Ligar, em anel, a LT 230kV / Gralha Azul / 
Repar. 
• Δδ ≤ 13°. 
LT 230kV Gralha 
Azul / Umbará  
Sentido Normal: SE Gralha Azul recebe tensão da SE Umbará 
Ligar, em anel, a LT 230 kV Gralha Azul / 
Umbará. 
• Δδ ≤ 10°. 
Sentido Inverso: SE Gralha Azul envia tensão para a SE Umbará 
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2PR. 
Transformador  
TF-2 ou TF-3 
230/18 kV da UTE 
Araucária 
Sentido Único: A partir do lado de 230 kV 
Energizar, pelo lado de 230 kV o 
Transformador TF-2 230/18 kV (ou TF -3) 
da UTE Araucária. 
Sistema completo (de LT) ou N-1 (de LT) 
na SE Gralha Azul 230 kV 
Como primeiro ou segundo 
transformador: 
• VARC-230 ≤ 242 kV. 
Antes de energizar cada transformador, 
verificar fluxo de potência ativa nos 
transformadores 230/18 kV que já 
foram energizados. 
7. NOTAS IMPORTANTES 
7.1. As tratativas de operação entre o ONS e o consumidor livre CSN/PR – Companhia Siderúrgica Nacional 
Paraná são feitas pela Copel GeT, interlocutor oficialmente nomeado pela CSN/PR para essas 
tratativas. 
No caso de desligamento total da Instalação, o restabelecimento de carga pela CSN/PR – Companhia 
Siderúrgica Nacional Paraná somente pode ser realizado após autorização do COSR-S. 
