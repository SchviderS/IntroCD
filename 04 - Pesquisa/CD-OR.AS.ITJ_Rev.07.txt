Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.11
Cadastro de Informações Operacionais
Cadastro de Informações Operacionais Hidráulicas da Bacia do Itajaí-Açu
Código Revisão Item Vigência
CD-OR.AS.ITJ 07 2.3. 03/07/2023
.
MOTIVO DA REVISÃO
- Adequação da numeração do índice.
LISTA DE DISTRIBUIÇÃO
CNOS GPD COSR-S NSUL CESAP
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia do Itajaí-Açu CD-OR.AS.ITJ 07 2.3. 03/07/2023
Referência: 2 / 8
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. CONSIDERAÇÕES GERAIS .................................................................................................................3
3. DADOS DO RESERVATÓRIO ..............................................................................................................4
3.1. Dados de Identificação do Aproveitamento..................................................................................4
3.2. A Dados Atemporais do Reservatório – Tabela 1 ..........................................................................4
3.3. Dados Atemporais do Reservatório – Tabela 2 .............................................................................5
3.4. Dados Atemporais do Reservatório – Tabela 3 .............................................................................5
4. RESTRIÇÕES OPERATIVAS HIDRÁULICAS (ROH) E INFORMAÇÕES OPERATIVAS RELEVANTES (IOR) ...6
4.1. UHE Salto Pilão ..............................................................................................................................6
5. REDE HIDROMÉTRICA ......................................................................................................................7
5.1. Localização das Estações Hidrométricas .......................................................................................7
5.2. Características das Estações Hidrométricas ..................................................................................7
6. DIAGRAMA DE OPERAÇÃO...............................................................................................................8
7. VOLUMES DE ESPERA PARA O CONTROLE DE CHEIAS .......................................................................8
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia do Itajaí-Açu CD-OR.AS.ITJ 07 2.3. 03/07/2023
Referência: 3 / 8
1. OBJETIVO
Apresentar os Dados Operacionais Hidráulicos dos Reservatórios da Bacia Hidrográfica do Itajaí-Açu, de 
acordo com os Procedimentos de Rede, a serem observados pelos operadores dos Centros de Operação do 
ONS e pelas Áreas de Operação dos Agentes de Geração envolvidos no controle desses valores.
2. CONSIDERAÇÕES GERAIS
2.1. Os dados constantes neste Cadastro de Informações Operacionais Hidráulicas são informações sobre 
os reservatórios dessa Bacia Hidrográfica, necessárias às ações de coordenação, supervisão e controle 
dos Centros de Operação do ONS para a operação hidráulica desse Sistema de Reservatórios.
2.2. Os operadores dos Centros de Operação do ONS, nas ações de coordenação, supervisão e controle, 
devem observar os dados operacionais constantes neste Cadastro de Informações Operacionais 
Hidráulicas.
2.3. As áreas de operação dos Agentes de Geração, em suas ações de comando e execução, devem 
observar os dados operacionais constantes neste Cadastro de Informações Operacionais Hidráulicas.
2.4. Os campos das tabelas cujos dados operacionais não existam devem ser preenchidos com a sigla “NE” 
– Não Existente.
2.5. Os campos das tabelas cujos dados operacionais não foram informados pelo Agente de Geração 
devem ser preenchidos com a sigla “NI” – Não Informado.
2.6. Os campos das tabelas cujos dados operacionais não se aplicam para um determinado reservatório 
devem ser preenchidos com um hífen “-”.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da Bacia do Itajaí-Açu CD-OR.AS.ITJ 07 2.3. 03/07/2023
Referência: 4 / 8
3. DADOS DO RESERVATÓRIO
3.1. DADOS DE IDENTIFICAÇÃO DO APROVEITAMENTO
Localização em Relação à Barragem (1)
Denominação do 
Reservatório Usina Rio Classificação Regularização
Município Margem Direita Município Margem Esquerda
UHE Salto Pilão UHE Salto Pilão Itajaí-Açu Fio d’água Diária Lontras – SC Ibirama – SC
(1) A casa de força se localiza no município de Apiúna - SC, à margem direita do Rio.
3.2. A DADOS ATEMPORAIS DO RESERVATÓRIO – TABELA 1
Nível (m) Área de Drenagem 
(km²)
Distâncias e Tempos de Viagem 
entre Aproveitamentos
Nome Mínimo
Operativo
Máximo
Operativo
Máximo
Maximorum
Coroa-
mento
Canal de 
Fuga 
(média)
Total até o
Reserva-
tório
Própria do 
Reserva-
tório
Distância 
até a Foz 
(km) Aproveitamentos (km) (h)
Salto Pilão 318,00 319,00 325,00 325,50 112,86 5.440 5.440 148 - - -
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da Bacia do Itajaí-Açu CD-OR.AS.ITJ 07 2.3. 03/07/2023
Referência: 5 / 8
3.3. DADOS ATEMPORAIS DO RESERVATÓRIO – TABELA 2
Volume (hm³) Dados da Casa de Força
Aproveitamento
Útil
No Nível 
Mínimo
Operativo
No Nível 
Máximo
Operativo
No Nível 
Máximo
Maximorum
Quantidade 
de
Unidades 
Geradoras
Potência por 
UG
(MW)
Potência Total
(MW)
Engolimento
por UG
(m³/s)
Produtibilidade 
(MW/m³/s)
Salto Pilão 0,1433 0,1189 0,1884 0,2622 2 95,94 191,89 55,5 1,729 
3.4. DADOS ATEMPORAIS DO RESERVATÓRIO – TABELA 3
Extravasores
Descarregador de Fundo Vertedor com Comporta Tulipa Vertedor com Soleira Livre Válvula Dispersora
Aproveitamento
Quanti-
dade
Cota da 
Soleira
(m)
Vazão  
por 
comporta
(m³/s)
Quanti-
dade 
de Vãos
Cota da
Crista da 
Soleira
(m)
Vazão 
por Vão
(m³/s)
Cota da
Soleira
(m)
Vazão 
Máxima
(m³/s)
Quanti
dade 
de 
Vãos
Cota da 
Crista da
Soleira
(m)
Vazão por 
Vão
(m³/s)
Quanti-
dade
Vazão
por 
Válvula
(m³/s)
Vazão
Total
(m³/s)
Salto Pilão 1 312 374 (1) - - - - - 1 319,10 7.238 (1) (2) - - 7.612
(1) Vazão correspondente ao nível de água na cota 325,00 m.
(2) A vazão de projeto, de 5.300 m³/s, ocorre com nível de água na cota 324,05 m. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia do Itajaí-Açu CD-OR.AS.ITJ 07 2.3. 03/07/2023
Referência: 6 / 8
4. RESTRIÇÕES OPERATIVAS HIDRÁULICAS (ROH) E INFORMAÇÕES OPERATIVAS RELEVANTES (IOR)
4.1. UHE SALTO PILÃO
4.1.1. Restrições Operativas Hidráulicas (ROH)
4.1.1.1. Restrições de Jusante
ROH 1 – Vazão Defluente Mínima
A vazão defluente mínima é de 7,2 m3/s, correspondendo à vazão a ser mantida para atender aos requisitos 
ambientais.
Nota: A casa de força da UHE Salto Pilão situa-se a cerca de 20 km do local da barragem da Usina. A 
manutenção de condições ambientais adequadas nesse trecho à jusante da barragem é obtida pelo 
descarregamento do valor de vazão sanitária acima exposto, que é feito por meio de uma comporta de 
abertura fixa, a ser mantida em quaisquer situações de vazão afluente ou de nível d'água no Reservatório. A 
comporta de descarga de vazão sanitária é constituída por uma estrutura localizada sob a soleira da tomada 
d’água, composta por 8 passagens, permanentemente abertas, que desemboca em uma galeria de 
dimensões de 1,20 m x 2,00 m, que escoa à seção plena, porém controlada por uma comporta e por uma 
válvula de ajuste fino à jusante. Esse dispositivo também promove a limpeza de material transportado por 
arraste de fundo, depositado em caixa de cascalho e areia (gravel trap).
4.1.1.2. Restrições de Montante
Não se aplica.
4.1.1.3. Outras Restrições
Não se aplica.
4.1.2. Informações Operativas Relevantes (IOR)
Não se aplica.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia do Itajaí-Açu CD-OR.AS.ITJ 07 2.3. 03/07/2023
Referência: 7 / 8
5.  REDE HIDROMÉTRICA
5.1. LOCALIZAÇÃO DAS ESTAÇÕES HIDROMÉTRICAS
Coordenadas Geográficas
Estação Código Descrição Latitude 
(S)
Longitude 
(W)
UHE Salto Pilão 
Jusante 83461000
Localizada no Rio Itajaí-Açu, município de 
Apiúna, à jusante da confluência com o Rio 
Hercílio, a aproximadamente 500 m à 
jusante da saída do canal de fuga da UHE 
Salto Pilão, a montante da estação Apiúna.
27°05’57'' 49°27'43''
Barragem 
Boiteux 83349600
Localizada no Rio Hercílio, município de 
José Boiteux, a 2 km à jusante da Barragem 
Norte, na margem esquerda do Rio, a 35 km 
a montante da confluência do Rio Hercílio 
com o Rio Itajaí-Açu.
26°53’55” 49°39’43”
Rio do Oeste 83059000
Localizada no Rio Itajaí do Oeste, município 
de Rio do Oeste, no início da avenida 
central, naquela cidade.
27°11’52” 49°47’08”
Aurora 83270000
Localizada no Rio Itajaí do Sul, município de 
Aurora, em frente ao posto da Polícia 
Rodoviária Estadual de Aurora.
27°17’32” 49°38’58”
UHE Salto Pilão 
Barramento 83458000
Localizada no Rio Itajaí-Açu, município de 
Lontras, no Reservatório da UHE Salto Pilão, 
junto à barragem da Usina.
27°08’14” 49°31’08”
5.2. CARACTERÍSTICAS DAS ESTAÇÕES HIDROMÉTRICAS
Coleta de Dados
Estação Tipo de Dado 
Coletado
Órgão 
Operador
Leitura de 
Dados
Transmissão 
de Dados Órgão Frequência
UHE Salto Pilão 
Jusante LT / PT Innovo Horária Telemétrica Innovo Horária
Barragem Boiteux LT / PT Innovo Horária Telemétrica Innovo Horária
Rio do Oeste LT / PT Innovo Horária Telemétrica Innovo Horária
Aurora LT / PT Innovo Horária Telemétrica Innovo Horária
UHE Salto Pilão 
Barramento LT CESAP Horária Telemétrica CESAP Horária
Notas:
LT: Linimétrica telemedida;
PT: Pluviométrica telemedida.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia do Itajaí-Açu CD-OR.AS.ITJ 07 2.3. 03/07/2023
Referência: 8 / 8
6. DIAGRAMA DE OPERAÇÃO
Não se aplica.
7. VOLUMES DE ESPERA PARA O CONTROLE DE CHEIAS
Não se aplica, devido à inexistência de reservatório com controle de cheias nesta Bacia Hidrográfica.
