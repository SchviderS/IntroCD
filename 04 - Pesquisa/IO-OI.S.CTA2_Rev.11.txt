 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Candiota 2 
 
 
Código Revisão Item Vigência 
IO-OI.S.CTA2 11 3.7.5.1. 10/10/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Início da operação do Compensador Estático CE1 da SE Guaíba 3, com alterações nos subitens 5.2.2. e 6.2.2. 
- Incorporação da parte correspondente do conteúdo da Mensagem Operativa MOP/ONS 372 -R/2024 - 
"Mudança da denominação da CEEE-T e do seu centro de operação". 
 
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S Cymi CPFL-T Pampa Sul 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Candiota 2 IO-OI.S.CTA2 11 3.7.5.1. 10/10/2024 
 
Referência:  2 / 12 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  5 
3.1. Barramento de 525 kV ................................................................................................................... 5 
3.2. Barramento de 230 kV ................................................................................................................... 5 
3.3. Alteração da Configuração dos Barramentos ................................................................................ 5 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 5 
4.1. Procedimentos Gerais ................................................................................................................... 5 
4.2. Procedimentos Específicos ............................................................................................................ 5 
4.2.1. Operação dos Comutadores de Tape Sob Carga (LTC) ................................................ 5 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  6 
5.1. Procedimentos Gerais ................................................................................................................... 6 
5.2. Procedimentos para Recomposição com Autonomia ................................................................... 6 
5.2.1. Preparação da Instalação para a Recomposição com Autonomia .............................. 6 
5.2.2. Recomposição com Autonomia após Desligamento Total da Instalação.................... 7 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 8 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 8 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 8 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 8 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 8 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 9 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 9 
6.1. Procedimentos Gerais ................................................................................................................... 9 
6.2. Procedimentos Específicos .......................................................................................................... 10 
6.2.1. Desenergização de Equipamentos ............................................................................ 10 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ..................................... 11 
7. NOTAS IMPORTANTES ................................ ................................ ................................ ................ 12 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Candiota 2 IO-OI.S.CTA2 11 3.7.5.1. 10/10/2024 
 
Referência:  3 / 12 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE Candiota 2, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta instrução de operação são somente aqueles de interesse sistêmico, 
realizados com autonomia pelos Agentes Operadores da Instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando -se a complementaridade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR -S e a Instalação, no que se refere aos equipamentos de 
manobra da Instalação, é realizada conforme segue: 
Linha de Transmissão ou 
Equipamento  Agente de Operação Agente Operador Centro de Operação 
do Agente Operador 
Barramento de 525 kV Chimarrão Cymi COT Cymi 
Barramento de 230 kV  Chimarrão Cymi COT Cymi 
LT 525 kV Candiota 2 / Guaíba 
3 C1 Chimarrão Cymi COT Cymi 
LT 525 kV Candiota 2 / Guaíba 
3 C2 Chimarrão Cymi COT Cymi 
LT 525 kV Candiota 2 / Usina 
Termelétrica Pampa Sul Pampa Sul Pampa Sul COG Pampa Sul 
LT 230 kV Bagé 2 / Candiota 2 
C1 Vineyards Cymi COT Cymi 
LT 230 kV Bagé 2 / Candiota 2 
C2 (*) CPFL-T Cymi COT Cymi 
LT 230 kV Candiota 2 / 
Presidente Médici (*) CPFL-T Cymi COT Cymi 
Transformador TF2 
525/230/13,8 kV Chimarrão Cymi COT Cymi 
Transformador TF3 
525/230/13,8 kV Chimarrão Cymi COT Cymi 
(*) A LT 230 kV Candiota 2 / Presidente Médici e a LT 230 kV Bagé 2 / Candiota 2 C2, de propriedade da CPFL-
T, possui a CPFL-T como agente responsável pela sua operação, por intermédio do COT CPFL-T. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Candiota 2 IO-OI.S.CTA2 11 3.7.5.1. 10/10/2024 
 
Referência:  4 / 12 
 
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte das seguintes áreas elétricas: 
Linha de Transmissão ou Equipamento Área Elétrica 
LT 525 kV Candiota 2 / Guaíba 3 C1 525 kV da Região Sul 
LT 525 kV Candiota 2 / Guaíba 3 C2 525 kV da Região Sul 
LT 525 kV Candiota 2 / Usina Termelétrica Pampa Sul 525 kV da Região Sul 
LT 230 kV Bagé 2 / Candiota 2 C1 230 kV do Rio Grande do Sul 
LT 230 kV Bagé 2 / Candiota 2 C2 230 kV do Rio Grande do Sul 
LT 230 kV Candiota 2 / Presidente Médici 230 kV do Rio Grande do Sul 
Transformador TF2 525/230/13,8 kV 525 kV da Região Sul 
Transformador TF3 525/230/13,8 kV  525 kV da Região Sul 
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.4.1. A definição da quantidade  de tentativas de religamento manual de linha de transmissão ou de 
equipamento, bem como o intervalo entre elas, é de responsabilidade do Agente e deve m estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica. 
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR -S autorização 
para religamento. Nessa oportunidade, o Agente pode solicitar t ambém a alteração no sentido 
normal para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em consideração as condições operativas do 
sistema. 
2.5. Quando caracterizado impedimento de linha de transmissão  ou de equipamento , deve m ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação quando o Agente  tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Candiota 2 IO-OI.S.CTA2 11 3.7.5.1. 10/10/2024 
 
Referência:  5 / 12 
 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 525 KV 
A configuração do barramento de 525 kV (Barra BP -1 e Barra BP -2) é do tipo Disjuntor e Meio. Na 
operação normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados. 
3.2. BARRAMENTO DE 230 KV 
A configuração do barramento de 230 kV é do tipo Barra Dupla (Barra P1 e Barra P2) a Quatro Chaves. 
Na operação normal desse barramento , todos os disjuntores e seccionadoras devem estar fechados, 
exceto as seccionadoras de transferência e uma das seletoras de barra das linhas de transmissão  ou 
equipamentos, operando da seguinte forma: 
Em uma das barras – P1 (ou P2) 230 kV Na outra barra – P2 (ou P1) 
LT 230 kV Bagé 2 / Candiota 2 C1 (ou C2) LT 230 kV Bagé 2 / Candiota 2 C2 (ou C1) 
LT 230 kV Candiota 2 / Presidente Médici Transformador TF3 525/230/13,8 kV (ou TF 2) 
Transformador TF2 525/230/13,8 kV (ou TF3) - 
3.3. ALTERAÇÃO DA CONFIGURAÇÃO DOS BARRAMENTOS 
A mudança de configuração dos barramentos de 525 kV e 230 kV desta Instalação é executada com controle 
do COSR-S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade dos Agentes 
Operadores da Instalação. 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. Os barramentos de 525 kV e  de 230 kV, pertencente s à Rede de Operação, t êm a sua regulação de 
tensão controlada pelo COSR-S.  
As faixas para controle de tensão para os barramentos de 525 kV e de  230 kV estão estabelecidas no 
Cadastro de Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob a responsabilidade da Instalação. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
4.2.1. OPERAÇÃO DOS COMUTADORES DE TAPE SOB CARGA (LTC) 
Os LTCs dos transformadores TF2 e TF3 525/230/13,8 kV operam em modo manual. 
A movimentação dos comutadores é realizada pela Cymi com controle do COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Candiota 2 IO-OI.S.CTA2 11 3.7.5.1. 10/10/2024 
 
Referência:  6 / 12 
 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação : caracterizado quando não há tensão em todos os terminais de 
suas linhas de transmissão. 
• Desligamento parcial da Instalação : qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, os Agentes Operadores  da Instalação devem fornecer ao 
COSR-S as seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2., sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia da operação da instalação na recomposição, 
deve ser utilizado o Subitem 5.3.  
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO COM AUTONOMIA 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO COM AUTONOMIA  
No caso de desligamento total, os Agentes Operadores  da Instalação devem configurar os disjuntores dos 
seguintes equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• das linhas de transmissão:  
LT 525 kV Candiota 2 / Guaíba 3 C1; 
LT 525 kV Candiota 2 / Guaíba 3 C2; 
LT 525 kV Candiota 2 / Usina Termelétrica Pampa Sul; 
LT 230 kV Bagé 2 / Candiota 2 C1; 
LT 230 kV Bagé 2 / Candiota 2 C2; 
LT 230 kV Candiota 2 / Presidente Médici. 
• dos transformadores: 
TF2 (lados de 525 kV e de 230 kV); 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Candiota 2 IO-OI.S.CTA2 11 3.7.5.1. 10/10/2024 
 
Referência:  7 / 12 
 
TF3 (lados de 525 kV e de 230 kV). 
• das interligações de módulos: 
LT 525 kV Candiota 2 / Guaíba 3 C1 e Transformador TF2 525/230/13,8 kV; 
LT 525 kV Candiota 2 / Guaíba 3 C2 e Transformador TF3 525/230/13,8 kV; 
LT 525 kV Candiota 2 / Usina Termelétrica Pampa Sul e Barra B 525 kV. 
Fechar ou manter fechado o disjuntor:  
• do módulo de interligação de barras de 230 kV, exceto quando esse estiver substituindo o disjuntor 
de um equipamento ou de uma linha de transmissão. 
Desligar ou manter desligado o modo de comutação automática dos comutadores sob carga da 
transformação 525/230/13,8 kV da SE Candiota 2. 
Cabe à Cymi, no que se refere aos barramentos de 525 kV e de 230 kV, informar ao COSR -S quando a 
configuração de preparação da Instalação não estiver atendida para o início da recomposição, 
independentemente de o equipamento ser próprio ou de outros Agentes. Nesse ca so, o COSR-S fará 
contato com os agentes envolvidos, informando-lhes que a preparação da Instalação será coordenada. 
Caso necessário, o COSR -S deverá coordenar os procedimentos para recomposição em função da 
configuração da Instalação. 
5.2.2. RECOMPOSIÇÃO COM AUTONOMIA APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
Os Agentes Operadores da Instalação devem realizar com autonomia os seguintes procedimentos para 
a recomposição: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 Cymi 
Receber tensão da SE Guaíba 3, pela LT 525 kV 
Candiota 2 / Guaíba 3  C1 (ou C2) e ligá-la, 
energizando a Barra A de 525 kV. 
 
1.1 Cymi 
Energizar, pelo lado de 525 kV, o Transformador 
TF2 (ou TF3) 525/230/13,8 kV da SE Candiota 2,  
energizando a Barra B de 525 kV e o barramento 
de 230 kV. 
VCTA2-525 ≤ 550 kV. 
A energização do transformador 
somente pode ser realizada pelo 
DJ 1050 para o TF2 ou pelo DJ 1070 
para o TF3. 
1.1.1 Cymi Energizar a LT 230 kV Bagé 2 / Candiota 2 C2, 
enviando tensão para SE Bagé 2. 
VCTA2-230 ≤ 242 kV. 
2 Cymi 
Receber tensão da SE Guaíba 3, pela LT 525 kV 
Candiota 2 / Guaíba 3 C2 (ou C1) e ligá-la, em 
anel. 
VCTA2-525 ≤ 541 kV, e 
VCTA2-230 ≤ 237 kV. 
∆δ ≤ 16°. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Candiota 2 IO-OI.S.CTA2 11 3.7.5.1. 10/10/2024 
 
Referência:  8 / 12 
 
Passo Executor Procedimentos Condições ou Limites Associados 
2.1 Cymi 
Energizar, pelo lado de 525 kV, o Transformador 
TF3 (ou TF2) 525/230/13,8 kV da SE Candiota 2 
e ligar, interligando esse transformador com 
outro transformador 525/230/13,8 kV da SE 
Candiota 2, o lado de 230 kV. 
VCTA2-525 ≤ 550 kV. 
A energização do transformador 
somente pode ser realizada pelo 
DJ 1050 para o TF2 ou pelo DJ 1070 
para o TF3. 
Após fluxo de potência ativa no 
transformador 525/230/13,8 kV 
que já foi energizado. 
2.1.1 Cymi Energizar a LT 230 kV Bagé 2 / Candiota 2 C 1, 
enviando tensão para SE Bagé 2. 
VCTA2-230 ≤ 242 kV. 
2.1.2 Cymi 
Energizar a LT 230 kV Candiota 2 / Presidente 
Médici, enviando tensão para SE Presidente 
Médici. 
VCTA2-230 ≤ 242 kV. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
Os Agentes Operadores da Instalação devem realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
Os Agentes Operadores  da Instalação devem realizar os procedimentos do Subitem 5.2.2 ., enquanto não 
houver intervenção do COSR-S no processo de recomposição ou interrupção da autonomia da operação da 
instalação na recomposição. 
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras.  
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, os 
Agentes Operadores da Instalação devem preparar a Instalação conforme Subitem 5.2.1., sem 
necessidade de autorização do ONS. 
5.4.1.2. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão nas barras de 525 kV, e 
• ausência de fluxo de potência ativa nas linhas de transmissão de 525 kV, os Agentes Operadores 
devem preparar o setor de 525 kV d a Instalação (disjuntores das linhas de transmissão e 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Candiota 2 IO-OI.S.CTA2 11 3.7.5.1. 10/10/2024 
 
Referência:  9 / 12 
 
equipamentos) conforme Subitem 5.2.1., sem necessidade de autorização do ONS.  
5.4.1.3. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão nas barras de 230 kV , os Agentes Operadores  devem preparar o setor de 
230 kV da Instalação (disjuntores das linhas de transmissão e equipamentos) conforme Subitem 
5.2.1., sem necessidade de autorização do ONS.  
5.4.1.4. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., os Agentes 
Operadores devem  recompor a Instalação conforme Subitem 5.2 .2., sem necessidade de 
autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitens 5.4.1.2. e 5.4.1.3., os Agentes 
Operadores devem informar ao COSR-S, para que a recomposição da Instalação seja executada com 
controle do COSR-S. 
5.4.2.3. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.4., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 6.2.2., sem necessidade de 
autorização do ONS. 
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
os Agentes Operadores devem informar ao COSR -S, para que a recomposição da Instalação seja 
executada com controle do COSR-S.   
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, após desligamento 
programado, de urgência ou de emergência, só podem ser efetuados com controle do COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelos Agentes Operadores  da Instalação 
quando explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução de 
Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir autonomia, 
a energização deve ser executada com controle do COSR -S, conforme Instrução de Operação de 
Preparação para Manobras da respectiva área elétrica. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Candiota 2 IO-OI.S.CTA2 11 3.7.5.1. 10/10/2024 
 
Referência:  10 / 12 
 
6.1.4. Antes do fechamento de qualquer disjuntor, os Agentes Operadores da Instalação devem verificar se 
existe tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pelos Agentes Operadores da Instalação 
quando estiver especificado nesta Instrução de O peração e estiverem atendidas as condições do 
Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou , quando permitida, em sentido  inverso, pode ser feita com 
autonomia pelo Agente Operador, conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica. 
 A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados  na Instalação, durante execução de intervenções,  
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal  / lado pelo qual a linha de transmissão ou transformador é 
energizado, o Agente Operador da Instalação deve fechá-lo em anel desde que tenha autonomia para 
tal, adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S. 
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S. 
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE EQUIPAMENTOS 
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação, é 
sempre controlada pelo COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Candiota 2 IO-OI.S.CTA2 11 3.7.5.1. 10/10/2024 
 
Referência:  11 / 12 
 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelos Agentes Operadores da Instalação, 
após desligamento automático de equipamentos ou de linhas de transmissão. 
Os Agentes Operadores da Instalação devem identificar os desligamentos automáticos observando 
na Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado 
nas condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Linha de Transmissão / 
Equipamento  Procedimentos Condições ou Limites Associados 
LT 525 kV Candiota 2 / 
Guaíba 3 C1 ou C2 
Sentido Normal: SE Candiota 2 recebe tensão da SE Guaíba 3 
Ligar, em anel, a LT 525 kV Candiota 2 
/ Guaíba 3 C1 (ou C2). 
• ∆δ ≤ 16°, 
• VCTA2-525 ≤ 541 kV, e 
• VCTA2-230 ≤ 237 kV. 
Sentido Inverso: SE Candiota 2 envia tensão para SE Guaíba 3 
A energização em sentido inverso é controlada pelo COSR-S, conforme  
IO-PM.S.5SU. 
LT 525 kV Candiota 2 / 
Usina Termelétrica 
Pampa Sul 
Sentido Único: SE Candiota 2 envia tensão para UTE Pampa Sul 
Permitida somente com controle do COSR-S, conforme IO-PM.S.5SU. 
LT 230 kV Bagé 2 / 
Candiota 2 C1 ou C2 
Sentido Normal: SE Candiota 2 envia tensão para SE Bagé 2 
Energizar a LT 230 kV Bagé 2 / 
Candiota 2 C1 (ou C2). 
Sistema completo (de LT e TR) ou N-1 
(de LT ou TR) na SE Candiota 2 230 kV 
• VCTA2-230 ≤ 242 kV. 
Antes de energizar cada linha de 
transmissão, verificar fluxo de potência 
ativa no circuito da LT 230 kV Bagé 2 / 
Candiota 2 que já foi energizado. 
Sentido Inverso: SE Candiota 2 recebe tensão da SE Bagé 2 
Ligar, em anel, a LT 230  kV Bagé 2 / 
Candiota 2 C1 (ou C2). 
• ∆δ ≤ 36°. 
LT 230 kV Candiota 2 / 
Presidente Médici 
Sentido Normal: SE Candiota 2 envia tensão para SE Presidente Médici 
Energizar a LT 230 kV Candiota 2 / 
Presidente Médici. 
Sistema completo (de LT e TR) ou N-1 
(de LT ou TR) na SE Candiota 2 230 kV 
• VCTA2-230 ≤ 242 kV. 
Sentido Inverso: SE Candiota 2 recebe tensão da SE Presidente Médici 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Candiota 2 IO-OI.S.CTA2 11 3.7.5.1. 10/10/2024 
 
Referência:  12 / 12 
 
Linha de Transmissão / 
Equipamento  Procedimentos Condições ou Limites Associados 
Ligar, em anel, a LT 230 kV Candiota 2 
/ Presidente Médici. 
• ∆δ ≤ 16°. 
Transformador TF2 ou 
TF3 525/230/13,8 kV 
Sentido Único: A partir do lado de 525 kV 
Energizar pelo lado de 525 kV o 
transformador TF2 (ou TF3) 
525/230/13,8 kV. 
Sistema completo (de LT) ou N-1 (de LT) 
na SE Candiota 2 525 kV 
Nota: Para sistema completo ou N -1, 
pode ser desconsiderada a LT 525 kV 
Candiota 2 / UTE Pampa Sul. 
Como primeiro ou segundo 
transformador: 
• VCTA2-525 ≤ 550 kV, somente 
pelo DJ 1050 para o TF2 ou pelo 
DJ 1070 para o TF3. 
Antes de energizar cada transformador, 
verificar fluxo de potência ativa nos 
transformadores 525/230/13,8 kV que 
já foram energizados. 
Ligar, interligando esse transformador 
com outro transformador 
525/230/13,8 kV da SE Candiota 2 , o 
lado de 230 kV do transformador TF2 
(ou TF3) 525/230/13,8 kV. 
Sem verificação angular: 
• o outro transformador 
525/230/13,8 kV da SE 
Candiota 2 deve estar com fluxo 
de potência ativa. 
Em caso de abertura apenas do lado de 525 kV, proceder conforme segue: 
Ligar, em anel, o lado de 525 kV do 
transformador TF2 (ou TF3) 
525/230/13,8 kV. 
• ∆δ ≤ 20°. 
7. NOTAS IMPORTANTES 
7.1. Os disjuntores de interligação de módulos deverão ser fechados, em anel, toda vez que forem 
recompostos os dois circuitos do mesmo vão ou quando, num dos lados do vão, for uma barra. 
 
