 
 
 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.13 
 
Rotina Operacional 
Detalhamento de Critérios para a Gestão de Condicionantes Operativos Hidráulicos 
 
 
Código Revisão Item Vigência 
RO-OH.BR.01 00 4.1.13. 11/11/2024 
 
. 
 
MOTIVO DA REVISÃO 
• Emissão inicial. 
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-NCO COSR-NE COSR-S COSR-SE 
Agentes de Geração  
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13 
 Rotina Operacional Código Revisão Item Vigência 
Detalhamento de Critérios para a Gestão de 
Condicionantes Operativos Hidráulicos RO-OH.BR.01 00 4.1.13. 11/11/2024 
 
Referências: 2 / 4 
 
 
INDICE 
 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. REFERÊNCIAS ................................ ................................ ................................ ................................  3 
3. CONCEITOS ................................ ................................ ................................ ................................ ... 3 
3.1. Tipos de Condicionantes Operativos Hidráulicos .......................................................................... 3 
3.2. Temporalidades de Condicionantes Operativos Hidráulicos ........................................................ 3 
4. PARECER TÉCNICO ................................ ................................ ................................ ........................  4 
5. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 4 
6. DESCRIÇÃO DO PROCESSO ................................ ................................ ................................ ............ 4 
7. ATRIBUIÇÕES DOS AGENTES E DO ONS ................................ ................................ ..........................  4 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13 
 Rotina Operacional Código Revisão Item Vigência 
Detalhamento de Critérios para a Gestão de 
Condicionantes Operativos Hidráulicos RO-OH.BR.01 00 4.1.13. 11/11/2024 
 
Referências: 3 / 4 
 
 
1. OBJETIVO 
Estabelecer os tipos e as classificações de temporalidade de condicionantes operativos hidráulicos (COPHIs) 
dos aproveitamentos hidroelétricos do Sistema Interligado Nacional (SIN) e definir os casos em que há 
necessidade de elaboração de parecer técnico.  
2. REFERÊNCIAS 
• Submódulo 4.7 do Módulo 4 dos Procedimentos de Rede. 
3. CONCEITOS 
3.1. TIPOS DE CONDICIONANTES OPERATIVOS HIDRÁULICOS 
Os condicionantes operativos hidráulicos (COPHIs) são quaisquer limitações impostas a variáveis hidráulicas 
de usinas hidroelétricas despachadas de forma centralizada pelo ONS e que podem ser declaradas como 
pertencentes a um dos seguintes tipos: 
• Restrições Hidráulicas (ROH), que se referem a vazões máximas e mínimas em seções e trechos de rio; 
limitações de vazões máximas e mínimas defluentes em aproveitamentos hidroelétricos; limites para os 
níveis máximos e mínimos nos reservatórios; e taxas de variação de defluências; entre outras restrições 
hidráulicas, que são consideradas nos processos relativos ao planejamento, à programação e à operação 
em tempo real dos aproveitamentos hidroelétricos integrantes do SIN; 
• Informações Operativas Relevantes (IOR), que são referências operativas hidráulicas diversas que 
podem ser consideradas, quando possível, na programação diária e operação em tempo real dos 
aproveitamentos hidroelétricos, enquanto não impactarem na otimização da operação. Diferem-se das 
restrições hidráulicas por não serem limites operativos determinantes para o planejamento e 
programação mensal da operação dos aproveitamentos hidroelétricos; 
• Diretrizes Operativas (DZO), que são diretrizes avaliadas e implementadas pelo ONS na operação de 
usinas hidroelétricas do SIN com o objetivo de atender de forma otimizada sob a ótica da operação do 
SIN, resoluções estabelecidas pela ANA e curvas de referência para a operação de reservatórios; e 
• Intervenções (INT), que são condicionantes operativos hidráulicos, temporários, necessários para a 
realização de atividades, serviços e intervenções, que podem ocorrer nas estruturas, dispositivos 
extravasores e instalações dos aproveitamentos ou em suas áreas de influência. 
3.2. TEMPORALIDADES DE CONDICIONANTES OPERATIVOS HIDRÁULICOS 
Atualmente, os condicionantes operativos hidráulicos podem ser classificados, de acordo com sua 
temporalidade, em: 
• Temporária: são os condicionantes operativos hidráulicos declarados que possuem período de vigência 
definido, inferior a 1 ano e que não necessariamente se repetem ao longo dos anos subsequentes. Como 
exemplos, podem ser mencionadas os condicionantes devid o a intervenções em estruturas de usinas 
hidroelétricas; e o atendimento de questões ambientais conjunturais, como aumento da defluência 
mínima para evitar a mortandade de peixes a jusante do canal de fuga, em função de operações 
específicas; 
• Permanente - Sazonal: são os condicionantes operativos declarados que possuem um período de 
vigência bem definido e que se repetem nos anos subsequentes. Exemplo desse tipo de condicionante 
são os relacionados ao aumento da defluência mínima no período da piracema; e 
• Permanente - Atemporal: são os condicionantes operativos que não perecem com a temporalidade, 
muitas vezes relacionados com informações operacionais da usina, como a vazão defluente máxima. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13 
 Rotina Operacional Código Revisão Item Vigência 
Detalhamento de Critérios para a Gestão de 
Condicionantes Operativos Hidráulicos RO-OH.BR.01 00 4.1.13. 11/11/2024 
 
Referências: 4 / 4 
 
 
Ademais, são condicionantes que podem mudar o dado cadastral de uma usina, por questões de 
preservação de ictiofauna, proteção de cidades a jusante, entre outras. 
4. PARECER TÉCNICO 
Há necessidade de elaboração de parecer técnico para COPHIs  do tipo restrição hidráulica e de 
temporalidade permanente, os quais deverão ser disponibilizados à Agência Nacional de Energia Elétrica 
(ANEEL) e/ou à Agência Nacional de Águas e Saneamento Básico (ANA), para fins de avaliação e, para os 
agentes de geração, para fins de conhecimento. 
5. CONSIDERAÇÕES GERAIS 
Os condicionantes operativos hidráulicos possuem os tipos e classificações de temporalidade , conforme 
detalhado no item 3 desta rotina operacional.  
6. DESCRIÇÃO DO PROCESSO 
A gestão das informações sobre condicionantes operativos hidráulicos é realizada pela Gerência de Recursos 
Hídricos e Meteorologia (PRH) a partir de sistema do ONS.  
7. ATRIBUIÇÕES DOS AGENTES E DO ONS 
O documento “Responsabilidades” do Submódulo 4.7 define as responsabilidades dos agentes de geração e 
do ONS na gestão das informações sobre condicionantes operativos hidráulicos.  
 
 
