Página 1 de 2
ANEXO 2 (Módulo 4)
S I
SOLICITUD DE INTERVENCIONES EN EQUIPOS DE 
INTERCONEXIÓN
NÚMERO
____/____
PEDIDO:               (   ) INCLUSIÓN
                               (   ) CANCELACIÓN
(  ) CON DESCONEXIÓN
(  ) PROG.  (  ) NO PROGR.
(  ) SIN DESCONEXIÓN
(  ) PROG.   (  ) NO PROGR.
EQUIPO:_______________________________________________________________________________________
________________________________________________________________________
LUGAR:________________________________________________________________________________
INTERVENCIÓN:
INICIO:      _____ H ____ MIN DEL DIA: ____/____/____
FINAL: _____ H ____ MIN DEL DIA: ___/____/____
EN CASO DE SER NECESARIO, EL TIEMPO DE REPOSICIÓN 
DEL EQUIPO SERÁ DE  _____ H ______ MIN
PERÍODO:
(     )  CONTÍNUO
(     )  DIARIAMENTE
DESCRIPCIÓN SUSCINTA DEL TRABAJO A EJECUTAR: _________________________________________________ 
_______________________________________________________________________________________________________
_____________________________________________________________________________________________________
JUSTIFICACIÓN DE LA URGENCIA: 
_______________________________________________________________________________________________
_____________________________________________________________________________________________
NOMBRE SOLICITANTE: _____________________________EMPRESA: ________________________
SOLICITADO EN EL DÍA: _____ / _____ / _____                      APROBADO POR:
 
OBSERVACIONES:
Página 2 de 2
Llenado de la Solicitud de Intervención:
Campo 1 – Deberá ser llenado con el número de solicitud y con el año en curso.
Campo 2 – Deberá ser indicado si es solicitud de Inclusión o de Cancelación.
Campo 3 – Deberá ser indicado si la solicitud es con desconexión y de tipo programado o no 
programado.
Campo 4 – Deberá ser indicado si la solicitud es sin desconexión y de tipo programado o no 
programado.
Campo 5 – Deberá ser llenado con el tipo de equipo (línea de transmisión 1 o 2, transformador, 
etc.), nivel de tensión del mismo y estación en la que está localizado.
Campo 6 – Deberá ser llenado con el día y el horario previsto para el inicio y fin de la 
desconexión y el tiempo de reposición al servicio del equipamiento en caso de necesidad de la 
operación.
Campo 7 – Deberá ser indicado si el equipamiento permanece intervenido por todo el período 
(contínuo) o si el equipamiento retorna a la operación en cada día (diariamente).
Campo 8 – Deberá ser llenado con una descripción breve del trabajo a ser realizado por los 
equipos de mantenimiento de la empresa ejecutante.
Campo 9 – En caso de que la solicitud sea no programada, deberá ser llenado con la 
justificación para ello. Ejemplo: Riesgo para el equipamiento, riesgo para la operación de la 
interconexión ante la pérdida intempestiva del equipamiento, etc.
Campo 10 – Deberá ser llenado con el nombre y cargo de la persona que solicitó la intervención, 
la empresa y el departamento de trabajo de esa persona, la fecha en que fue efectuada la 
solicitud y el nombre y cargo de la persona que aprobó esa solicitud.
Campo 11 – Deberá ser llenado, en caso de ser necesario, con informaciones complementarias 
para la realización de la intervención.
