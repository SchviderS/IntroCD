Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.13
Rotina Operacional
Operacionalização do Programa de Resposta da Demanda
Código Revisão Item Vigência
RO-GC.BR.04 01 4.1.4. 16/09/2024
.
MOTIVO DA REVISÃO
•Ajuste no item 5.7.2
•Ajuste na sigla do item 5.8.3
•Inclusão do item 5.9.3.
LISTA DE DISTRIBUIÇÃO
CNOS COSR-NCO COSR-NE COSR-S COSR-SE Agentes de 
Transmissão
Agentes de 
Distribuição
Agentes de 
Geração
Consumidores Livres e 
Parcialmente Livres conectados à 
Rede de Supervisão do ONS
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
Instrução de Operação Código Revisão Item Vigência
Operacionalização do Programa de Resposta da 
Demanda RO-GC.BR.04 01 4.1.4. 16/09/2024
Referência: 2 / 8
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. REFERÊNCIAS ...................................................................................................................................3
3. CONCEITOS ......................................................................................................................................3
4. CONSIDERAÇÕES GERAIS .................................................................................................................3
5. DESCRIÇÃO DO PROCESSO ...............................................................................................................4
6. ATRIBUIÇÕES DO ONS, DOS AGENTES E DA CCEE .............................................................................6
7. ORIENTAÇÕES TÉCNICAS COMPLEMENTARES ..................................................................................8
8. ANEXO.............................................................................................................................................8
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
Instrução de Operação Código Revisão Item Vigência
Operacionalização do Programa de Resposta da 
Demanda RO-GC.BR.04 01 4.1.4. 16/09/2024
Referência: 3 / 8
1. OBJETIVO
O objetivo desta Rotina Operacional é apresentar os procedimentos e os processos relativos ao programa de 
Resposta da Demanda - RD, em conformidade com a Resolução Normativa ANEEL n° 1.040 de 30 de agosto 
de 2022, definindo critérios e diretrizes para os procedimentos operativos a serem realizados entre Operador 
Nacional do Sistema Elétrico – ONS e os agentes consumidores e agregadores na operacionalização do 
processo de oferta de redução da demanda e sua consideração na programação da operação.
2. REFERÊNCIAS
[1] Resolução Normativa ANEEL n° 1.040 de 30 de agosto de 2022.
[2] Regra de Comercialização Provisória CCEE – Programa de Resposta da Demanda.
[3] Procedimento de Comercialização Provisório CCEE – Programa de Resposta da Demanda.
3. CONCEITOS
3.1. Resposta da Demanda - RD
Redução do consumo por consumidores previamente habilitados, como recurso adicional para atendimento 
ao Sistema Interligado Nacional – SIN, desde que aceita pelo ONS, de modo a se obter resultados mais 
vantajosos tanto para a confiabilidade do sistema elétrico como para a modicidade tarifária dos 
consumidores finais.
4. CONSIDERAÇÕES GERAIS
4.1. Poderão ser habilitados a participar do programa de RD os seguintes agentes:
4.1.1. Consumidores livres, consumidores parcialmente livres e consumidores cujos contratos de compra 
de energia seguem os preceitos estabelecidos no art. 5º da Lei nº 13.182, de 2015, conectados na 
rede de supervisão do ONS, ou fora da rede de supervisão, onde poderão ser exigidos requisitos para 
o monitoramento do despacho pelo ONS, caso necessário.
4.1.1.1. Os consumidores parcialmente livres poderão participar da oferta de RD até o limite equivalente à 
parcela livre do seu consumo.
4.1.2. Agregadores, sendo agentes da Câmara de Comercialização de Energia Elétrica – CCEE nas categorias 
de consumidores, comercializadores e geradores, responsáveis por agregar e centralizar as cargas 
dos consumidores de que trata o item 4.1.1 e que formalizarem o seu cadastro na CCEE para fins de 
participação no Programa de Resposta da Demanda, conforme Regras e Procedimentos de 
comercialização [2] e [3].
4.1.3. Consumidores de que trata o item 4.1.1 modelados sob agentes varejistas.
4.2. Somente poderão participar das ofertas de RD e suas confirmações diárias, os agentes ofertantes     
(consumidores descritos no item 4.1.1 quando autorrepresentados ou agregadores devidamente 
habilitados) e que estejam adimplentes junto à CCEE e ao ONS, conforme estabelecido na Resolução 
Normativa ANEEL n° 1.040 de 30 de agosto de 2022 e observando as Regras e Procedimentos de 
Comercialização [2] e [3].
4.3. Os dados apurados com base nesta Rotina Operacional Provisória devem estabelecer:
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
Instrução de Operação Código Revisão Item Vigência
Operacionalização do Programa de Resposta da 
Demanda RO-GC.BR.04 01 4.1.4. 16/09/2024
Referência: 4 / 8
4.3.1. Dados necessários para a contabilização na CCEE referente ao aceite e utilização das ofertas de RD 
na Programação Diária da Operação - PDO.
4.4. As ofertas deverão ser realizadas pelos agentes através da plataforma de RD, desenvolvida pelo ONS 
e disponibilizada no ambiente SINtegre.
4.5. As tratativas para efeito do estabelecido nesta Rotina Operacional, entre os agentes ofertantes 
participantes do programa de RD e o ONS, devem ocorrer por meio da plataforma de RD. 
4.6. A referência de hora para os registros de dados desta Rotina Operacional é o horário padrão de 
Brasília.
4.7. O montante relativo à oferta de RD será considerado pelo ONS por período determinado, dentro do 
prazo ofertado, desde que haja confirmação no dia anterior ao despacho (D-1) por parte do agente 
ofertante ao ONS.
4.8. Após a confirmação da disponibilidade do agente ofertante para o produto, o ONS procederá à 
confirmação ou não da alocação da oferta, observadas a otimização do custo total de despacho do 
sistema e a segurança operativa.
5. DESCRIÇÃO DO PROCESSO
5.1. Cadastro das Ofertas de Resposta da Demanda – RD:
5.1.1. As grades horárias com os períodos permitidos para redução de demanda, bem como os períodos 
permitidos para o eventual deslocamento de consumo serão publicadas mensalmente no site do 
ONS.
5.1.2. Os agentes ofertantes deverão encaminhar até quinta-feira às 12h, as ofertas de RD para próxima 
semana operativa, que se inicia aos sábados e termina às sextas-feiras.
5.2. Será previsto o seguinte produto para as ofertas de RD: 
5.2.1. Produto Dia Anterior (D-1) -   Produtos com duração horária de 4 (quatro) até 17 (dezessete) horas, 
conforme necessidade indicada pelo ONS em sua grade horária, lotes com volume mínimo de 5 MW 
por Submercado, com montantes iguais para cada hora de duração da oferta, discretizados no padrão 
de 1 MW, preço em R$/MWh, dia da semana e identificação do Submercado.
5.3. O ONS poderá dispor, mediante autorização específica da ANEEL, de produtos adicionais de Resposta 
da Demanda em ambiente regulatório experimental.
5.4. O Agregador de cargas poderá ofertar reduções de consumo de unidades consumidoras agregadas, 
desde que estas pertençam ao mesmo Submercado e respeitem o limite mínimo agregado de 5 MW.
5.5. Para avaliação da previsão de carga por parte do ONS, principalmente sobre a recomposição da 
demanda, cada oferta de RD deverá ser caracterizada pelo agente ofertante com um dos seguintes 
perfis:
1. Compensação com geração própria atendendo à carga;
2. Deslocamento de demanda;
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
Instrução de Operação Código Revisão Item Vigência
Operacionalização do Programa de Resposta da 
Demanda RO-GC.BR.04 01 4.1.4. 16/09/2024
Referência: 5 / 8
3. Redução temporária sem compensação.
5.5.1. Caso o agente opte por deslocar a sua demanda, este deverá informar no momento do envio de sua 
oferta, o dia e horário no qual o deslocamento irá ocorrer e montante de deslocamento, limitando-
se ao período permitido informado na grade horária.
5.6. Para validação elétrica do ONS, o agente ofertante deverá informar no momento da oferta, o 
barramento da rede de simulação/subestação no qual sua carga encontra-se representada no ONS, 
esteja ele diretamente conectado à Rede Básica ou via conexão da rede de distribuição. No caso de 
agentes agregadores, essas informações também deverão ser disponibilizadas para cada carga 
agregada na oferta.
5.7. Das ofertas realizadas pelos agentes habilitados:
5.7.1. Semanalmente, até às 12 horas de quinta-feira, os agentes deverão utilizar a plataforma 
disponibilizada no SINtegre para realizar suas ofertas de RD para a semana operativa seguinte (de 
sábado a sexta-feira), respeitando a grade horária e as características mencionadas nos itens 5.1 ao 
5.6, contendo as seguintes informações:
•Dia(s) da semana;  
•Submercado;
•Volume da oferta (em MW para cada hora de duração);
•Preço da oferta (em R$/MWh);
•Período de redução da oferta (4 horas até 17 horas – conforme grade horária), não sendo aceitas 
ofertas com horários sobrepostos para o mesmo dia e para uma mesma unidade consumidora;
•Perfil da RD (item 5.5);
•Número do ativo da unidade consumidora (ID Carga CCEE);
•Barramento da Rede de Simulação do ONS no qual a carga do consumidor encontra-se 
diretamente ou indiretamente conectado (item 5.6).
5.7.2. Diariamente, no dia anterior ao despacho (D-1), até às 10h00, os agentes ofertantes devem confirmar 
a disponibilidade das ofertas de seus produtos destinados à RD para o dia seguinte na plataforma de 
ofertas. Caso não exista confirmação, a oferta será considerada indisponível para a análise da 
Programação Diária de Operação.
5.7.3. Ressalta-se que as ofertas e confirmações durante toda sua vigência no programa de RD, devem ser 
efetivadas somente por agentes que estiverem adimplentes junto à CCEE e ao ONS.
5.7.4. No dia anterior ao da redução ofertada, o ONS emitirá até às 23:00h, por meio da plataforma de 
recebimento de ofertas e de correio eletrônico, aviso de aceite das ofertas confirmadas pelos agentes 
ofertantes, ratificando sua utilização na programação diária da operação.
5.7.5. Os agentes ofertantes deverão estar cadastrados no SINtegre para ter acesso à plataforma de ofertas 
de RD, conforme item 6.2.2. Adicionalmente, os agentes agregadores deverão cadastrar previamente 
o seu vínculo com as unidades consumidoras agregadas junto à CCEE que informará os cadastros ao 
ONS semanalmente às quartas-feiras às 17h.
5.8. Programação Diária da Operação:
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
Instrução de Operação Código Revisão Item Vigência
Operacionalização do Programa de Resposta da 
Demanda RO-GC.BR.04 01 4.1.4. 16/09/2024
Referência: 6 / 8
5.8.1. A oferta de RD no produto D-1, cuja confirmação foi realizada pelo agente ofertante conforme item 
5.7.2, será analisada na etapa de programação diária após o processamento do modelo de curtíssimo 
prazo, considerando os critérios de folga de potência para atendimento a segurança operativa do SIN 
e a minimização do custo total da operação.
5.8.2. As ofertas necessárias para manutenção da folga de potência serão aprovadas até às 23h00min do 
dia anterior ao despacho para ser utilizada no tempo real
5.8.3. O relatório executivo da programação diária da operação (REPDOE) conterá as ofertas de RD 
aprovadas para utilização no tempo real.
5.9. Suspensão da participação do programa de RD:
5.9.1. O agente que descumprir por 7 vezes no mês, consecutivas ou não, a redução do consumo 
despachado pelo ONS, será suspenso do programa de RD pelo prazo de 3 meses, tendo suas ofertas 
canceladas e não sendo possível realizar novas ofertas no período de suspensão.
5.9.2. A apuração e indicação do não atendimento ao produto, bem como as penalidades associadas ao 
recebimento da remuneração por parte do agente, serão realizadas conforme Procedimentos e 
Regras de Comercialização da CCEE estabelecidos para o programa de RD [2] e [3].
5.9.3. O agente que estiver inadimplente junto ao ONS e/ou à CCEE não poderá realizar novas ofertas, e 
terá suas ofertas submetidas e ainda não aprovadas pelo ONS suspensas até a confirmação 
automática do pagamento, conforme descrito nos Procedimentos e Regras de Comercialização da 
CCEE e nas Rotinas das Atividades Corporativas - Econômico-Financeiro - Inadimplência Contas a 
Receber - RAC.EF.ICR.01 do ONS.
6. ATRIBUIÇÕES DO ONS, DOS AGENTES E DA CCEE
6.1. Operador Nacional do Sistema Elétrico - ONS
6.1.1. Responsável por definir e disponibilizar a grade horária com os períodos permitidos para a realização 
das ofertas de RD e períodos permitidos para o deslocamento de consumo.
6.1.2. Confirmar para o agente o aceite da oferta referente ao produto D-1, após a ratificação da 
disponibilidade pelo agente ofertante.
6.1.3. Informar para a CCEE mensalmente até o 8º dia útil do mês subsequente ao da operação as ofertas 
que foram consideradas na programação diária da operação, para subsidiar o processo de apuração 
e contabilização do programa de RD.
6.2. Agentes consumidores e/ou agregadores de demanda
6.2.1. Responsável por garantir que a(s) unidade(s) consumidora(s) participante(s) atenda(m) 
mandatoriamente a todos os requisitos exigidos pela Resolução Normativa e pelos documentos que 
a suportam.
6.2.2. Solicitar ao ONS a intenção de participação do programa de Resposta da Demanda, atendendo ao 
seguinte procedimento:
a) Solicitar o cadastro de sua empresa no SINtegre através do endereço eletrônico 
relacionamento.agentes@ons.org.br, fornecendo os seguintes dados:
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
Instrução de Operação Código Revisão Item Vigência
Operacionalização do Programa de Resposta da 
Demanda RO-GC.BR.04 01 4.1.4. 16/09/2024
Referência: 7 / 8
•Assunto de e-mail: Programa Resposta da Demanda
•Razão Social, CNPJ e Nome Fantasia da empresa;
•Domínio de e-mail corporativo; e 
•Nome, telefone e e-mail de um representante para contato.
b) Após recebimento da confirmação do cadastro da empresa pela Central de Relacionamento, 
providenciar o cadastro dos representantes responsáveis pela realização da oferta na 
plataforma disponibilizada pelo ONS no SINtegre, os associando à empresa.
6.2.3. Só poderão acessar a plataforma de ofertas no ambiente SINtegre os consumidores do Ambiente de 
Contratação Livre (ACL) autorrepresentados ou agregadores com o cadastro habilitado na CCEE que 
formaliza sua relação com as unidades consumidoras agregadas, que estejam adimplentes na CCEE e 
no ONS. Responsável por enviar as ofertas de RD através do meio disponibilizado pelo ONS, provendo 
as informações descritas na seção 5.7.1.
6.2.4. Prestar as informações solicitadas pelo ONS necessárias para a validação de dados e informações, 
diretamente ou por meio do seu prestador de serviços de operação.
6.2.5. Dirimir dúvidas e agir para eliminação de inconsistências indicadas pelo ONS, diretamente ou através 
do seu prestador de serviços de operação.
6.2.6. Formalizar, semanalmente até às 12 horas de quinta-feira, as ofertas de RD para a semana operativa 
seguinte na plataforma disponibilizada pelo ONS no SINtegre.
6.2.7. Confirmar, diariamente na plataforma disponibilizada pelo ONS no SINtegre, até às 12h00 do dia 
anterior ao despacho (D-1), a disponibilidade para redução da demanda do produto para o dia 
seguinte (D-1), caso sua oferta esteja aprovada.
6.2.8. Verificar diariamente na plataforma disponibilizada pelo ONS no SINtegre se houve o aceite de sua 
oferta de RD, até às 23h00 para o produto D-1, bem como realizá-la no período definido pelo ONS, 
conforme grade horária disponibilizada.
6.2.9. Buscar junto à distribuidora (quando aplicável) a informação sobre a barra do estudo do ONS à qual 
a unidade consumidora está conectada.
6.2.10. Cumprir com o disposto na Resolução Normativa ANEEL n° 1.040/2022 [1], Rotina Operacional e 
Procedimentos e Regras de Comercialização [2] e [3] de que tratam o mecanismo de RD.
6.3. Agentes de Distribuição
6.3.1. Fornecer ao agente ofertante a informação sobre a barra do estudo do ONS à qual a unidade 
consumidora está conectada.
6.4. Câmara de Comercialização de Energia Elétrica - CCEE
6.4.1. Disponibilizar as seguintes informações ao ONS:
a) Até o 12º dia útil subsequente ao mês de operação, o montante de redução de demanda 
preliminar e até o 21º dia útil subsequente ao mês de operação, o montante de redução de 
demanda verificado em base horária.
b) os valores calculados da linha base, em base horária, por consumidor, bem como os valores da 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
Instrução de Operação Código Revisão Item Vigência
Operacionalização do Programa de Resposta da 
Demanda RO-GC.BR.04 01 4.1.4. 16/09/2024
Referência: 8 / 8
margem superior de tolerância para atendimento da redução de consumo.
c) o resultado do cumprimento do despacho de RD por parte dos agentes consumidores, de forma a 
mostrar quais consumidores atenderam ou não ao produto de RD ofertado, seguindo as diretrizes 
desta Rotina Operacional e Regras e Procedimentos de Comercialização [2] e [3], informando se 
há necessidade de cancelamento e suspensão de suas ofertas no mecanismo.
d) A CCEE informará ao ONS a relação dos agentes participantes inadimplentes que não poderão 
participar do processo de ofertas e das confirmações diárias, bem como a relação dos agentes 
regularizados, conforme o caso.
e) Mensalmente até o 2º dia útil a CCEE informará ao ONS os casos de inclusão, alteração ou exclusão 
de cadastro de agentes e/ou unidades consumidoras aprovados pela CCEE.
f) Mensalmente até o 2º dia útil a CCEE informará ao ONS a relação dos agentes participantes 
desligados da CCEE.
7. ORIENTAÇÕES TÉCNICAS COMPLEMENTARES
Não se aplica.
8. ANEXO
Não se aplica.
