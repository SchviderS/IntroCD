 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da UHE Santa Clara 
 
 
Código Revisão Item Vigência 
IO-OI.S.SCL 11 3.7.5.4. 25/09/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Adequação do Subitem 5.4.  
- Adequação à RT-OI.BR revisão 37, com destaque à inclusão dos Subitens 6.1.5. e 6.1.6.  
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S MCQ 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Santa Clara IO-OI.S.SCL 11 3.7.5.4. 25/09/2024 
 
Referência: 2 / 7 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  3 
3.1. Unidades Geradoras ...................................................................................................................... 3 
4. CONTROLE DE TENSÃO E GERAÇÃO NA OPERAÇÃO NORMAL ................................ .........................  3 
4.1. Procedimentos Gerais ................................................................................................................... 3 
4.2. Procedimentos Específicos ............................................................................................................ 4 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  4 
5.1. Procedimentos Gerais ................................................................................................................... 4 
5.2. Procedimentos para Recomposição com Autonomia ................................................................... 5 
5.2.1. Preparação da Instalação para a Recomposição com Autonomia .............................. 5 
5.2.2. Recomposição com Autonomia Após Desligamento Total da Instalação ................... 5 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 5 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 5 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 5 
5.4. Procedimentos Após Desligamento Parcial da Instalação ............................................................ 6 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 6 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 6 
6. MANOBRAS DE UNIDADES GERADORAS ................................ ................................ ........................  6 
6.1. Procedimentos Gerais ................................................................................................................... 6 
6.2. Procedimentos Específicos ............................................................................................................ 7 
6.2.1. Desligamento de Unidades Geradoras ........................................................................ 7 
6.2.2. Sincronismo de Unidades Geradoras .......................................................................... 7 
7. NOTAS IMPORTANTES ................................ ................................ ................................ .................. 7 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Santa Clara IO-OI.S.SCL 11 3.7.5.4. 25/09/2024 
 
Referência: 3 / 7 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da UHE Santa Clara, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR-S e a Instalação, de propriedade da Elejor, é realizada pela 
MCQ – Eletro Service Ltda, agente responsável pela operação da Instalação, por intermédio do COR 
MCQ. 
2.3. As unidades geradoras e equipamentos desta Instalação fazem parte Área 230 kV do Paraná. 
2.4. O COSR-S controla e supervisiona o despacho de geração desta Usina. 
2.5. Esta Usina: 
• é despachada centralizadamente; 
• está conectada na Rede de Distribuição. 
2.6. Os dados operacionais desta Usina  estão descritos no Cadastro de Informações Operacionais da 
respectiva Área Elétrica. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. UNIDADES GERADORAS 
As unidades geradoras G1 e G2 13,8 kV estão conectadas ao barramento de 138  kV pelos transformadores 
TF-1 e TF-2 138/13,8 kV, da UHE Santa Clara, com todas as seccionadoras e disjuntores de 138 kV fechados, 
exceto a seccionadora de transferência de 138 kV. 
4. CONTROLE DE TENSÃO E GERAÇÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. A Usina deve manter os valores de geração de acordo com os valores programados, constantes no 
Programa Diário de Operação – PDO. Para atendimento dos valores programados constantes no PDO, 
não é necessária autorização prévia do COSR-S. 
Qualquer alteração no valor de geração da Usina em relação ao valor de geração constante no PDO 
somente pode ser executada após autorização do COSR-S. 
As reprogramações de geração quando de necessidades sistêmicas serão executadas pela Usina 
quando solicitadas pelo COSR-S. 
Após reprogramação de geração solicitada pelo COSR-S, o Agente somente poderá alterar a geração 
da Usina com autorização do ONS, inclusive para adoção de valores contidos no PDO. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Santa Clara IO-OI.S.SCL 11 3.7.5.4. 25/09/2024 
 
Referência: 4 / 7 
 
4.1.2. Os desvios de geração da Usina, em relação aos valores previstos no PDO ou em relação às 
reprogramações, devem ser controlados observando-se os valores máximos permitidos explicitados 
na Instrução de Operação IO-CG.BR.01 – Controle da Geração em Condição Normal. 
4.1.3. Quando não existir ou não estiver disponível a supervisão da Usina para o ONS, essa deve seguir as 
orientações para envio de dados conforme Rotina Operacional RO -AO.BR.08 – Apuração de Dados 
de Geração e de Intercâmbios. 
4.1.4. A Usina deve registrar e informar imediatamente os seguintes dados ao COSR-S: 
• movimentação de unidades geradoras (mudança de estado operativo / disponibilidade). 
• restrições e ocorrências na Usina ou na conexão elétrica que afetem a disponibilidade de geração, 
com o respectivo valor da restrição, contendo o horário de início e término e a descrição do 
evento. 
• demais informações sobre a operação da Instalação, solicitadas pelo ONS. 
4.1.5. O controle de tensão, por meio da geração ou absorção de potência reativa pelas unidades geradoras 
da Usina, é executado com autonomia pela operação do Agente e deve ser realizado entre o agente 
de geração e o agente de distribuição. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
Não se aplica. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação: caracterizado por meio da verificação de ausência de tensão em 
todos os terminais de suas conexões ou ausência de fluxo de potência ativa nessas conexões.  
• Desligamento parcial da Instalação: qualquer outra configuração que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao COSR -
S as seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2, sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificand o a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia do Agente Operador  da instalação na 
recomposição, deve ser utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Santa Clara IO-OI.S.SCL 11 3.7.5.4. 25/09/2024 
 
Referência: 5 / 7 
 
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO COM AUTONOMIA 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO COM AUTONOMIA 
O Agente Operador da Instalação deve realizar os seguintes procedimentos para a preparação da Instalação: 
Abrir ou manter abertos os disjuntores: 
• das unidades geradoras: 
G1 13,8 kV (de 138 kV); 
G2 13,8 kV (de 138 kV). 
5.2.2. RECOMPOSIÇÃO COM AUTONOMIA APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador  da Instalação deve realizar com autonomia os seguintes procedimentos para a 
recomposição: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 MCQ Verificar tensão no barramento de 138 kV da 
subestação da Usina. 
 
2 
 
MCQ 
Partir uma das unidades geradoras, caso esta 
estivesse operando antes do desligamento, e 
sincronizá-la. 
 
Devem ser tomadas as medidas 
necessárias para possibilitar o 
fechamento do sincronismo, 
manter a tensão terminal em 13,8 
kV, gerar o mínimo necessário para 
manter a unidade geradora 
sincronizada e solicitar autorização 
ao COSR-S para elevar a geração. 
2.1 MCQ 
Partir a segunda unidade geradora, caso esta 
estivesse operando antes do desligamento, e 
sincronizá-la. 
Devem ser tomadas as medidas 
necessárias para possibilitar o 
fechamento do sincronismo, 
manter a tensão terminal em 13,8 
kV, gerar o mínimo necessário para 
manter a unidade geradora 
sincronizada e solicitar autorização 
ao COSR-S para elevar a geração. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador  da Instalação deve realizar os procedimentos do Subitem 5.2.2, enquanto não houver 
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia deste na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR-S.  
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Santa Clara IO-OI.S.SCL 11 3.7.5.4. 25/09/2024 
 
Referência: 6 / 7 
 
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
Para os desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
O Agente Operador deve recompor a Instalação conforme Subitem 6.2.2, sem necessidade de autorização 
do ONS.  
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, o Agente 
Operador deve informar ao COSR-S, para que a recomposição da Instalação seja executada com controle do 
COSR-S. 
6. MANOBRAS DE UNIDADES GERADORAS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desligamento programado ou de urgência de unidades geradoras só podem 
ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para sincronismo de unidades geradoras, após desligamento programado, após 
desligamentos programados, de urgência ou de emergência, só podem ser efetuados com controle 
do COSR-S. 
6.1.3. Os procedimentos para sincronismo de unidades geradoras, após desligamento automático sem 
atuação de proteção  que impeça o retorno do equipamento , só podem ser executados com 
autonomia pelo Agente Operador  da Instalação quando estiverem explicitado e  estiverem 
atendidas as condições do subitem 6.2.2. desta Instrução de Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Os procedimentos de segurança , a serem adotados na Instalação, durante execução de 
intervenções, são de responsabilidade do Agente. 
6.1.5. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S.  
6.1.6. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com 
controle do COSR-S. 
6.1.7. A partida das unidades geradoras deve seguir critérios próprios do Agente. O sincronismo à Rede 
de Distribuição deve ser realizado após tratativas entre a Usina e o Agente de Distribuição . A 
tomada de carga deve ser realizada com controle do COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Santa Clara IO-OI.S.SCL 11 3.7.5.4. 25/09/2024 
 
Referência: 7 / 7 
 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESLIGAMENTO DE UNIDADES GERADORAS 
O desligamento de unidades geradoras, pertencentes à Rede de Operação, é sempre controlado pelo 
COSR-S. 
6.2.2. SINCRONISMO DE UNIDADES GERADORAS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de unidades geradoras. 
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais unidades geradoras em operação, conforme explicitado nas condições de 
energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Unidade Geradora Procedimentos Condições ou Limites Associados 
Unidade Geradora G1 ou 
G2 13,8 kV 
Partir e sincronizar a unidade geradora. Conforme procedimentos internos 
do Agente. 
Gerar o mínimo necessário para 
manter a unidade geradora 
sincronizada. 
Elevar a geração da unidade geradora. Após autorização do COSR-S. 
7. NOTAS IMPORTANTES 
Não se aplica. 
