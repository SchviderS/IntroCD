 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Salto Caxias 
 
 
Código Revisão Item Vigência 
IO-OI.S.SCX 25 3.7.5.1. 17/10/2024 
 
. 
 
MOTIVO DA REVISÃO 
Adequação das condições de energização da LT 525 kV Cascavel Oeste / Salto Caxias, alterando o Subitem 
6.2.2.  
  
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S Copel GeT 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Salto 
Caxias IO-OI.S.SCX 25 3.7.5.1. 17/10/2024 
 
Referência:  2 / 9 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  3 
3.1. Barramento de 525 kV ................................................................................................................... 3 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 4 
4.1. Procedimentos Gerais ................................................................................................................... 4 
4.2. Procedimentos Específicos ............................................................................................................ 4 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  4 
5.1. Procedimentos Gerais ................................................................................................................... 4 
5.2. Procedimentos para Recomposição Fluente ................................................................................. 4 
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................ 4 
5.2.2. Recomposição Fluente da Instalação .......................................................................... 5 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 6 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 6 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 6 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 6 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 6 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 6 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 6 
6.1. Procedimentos Gerais ................................................................................................................... 6 
6.2. Procedimentos Específicos ............................................................................................................ 7 
6.2.1. Desenergização de Equipamentos .............................................................................. 7 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ....................................... 8 
7. NOTAS IMPORTANTES ................................ ................................ ................................ .................. 8 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Salto 
Caxias IO-OI.S.SCX 25 3.7.5.1. 17/10/2024 
 
Referência:  3 / 9 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE  Salto Caxias, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são somente aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da Instalação , devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando -se a complementaridade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR -S e a Instalação, de propriedade Copel GeT, é realizada pela 
Copel GeT, agente responsável pela operação da Instalação, por intermédio do COGT Copel.  
2.3. As linhas de transmissão desta Instalação fazem parte da Área 525 kV da Região Sul. 
2.4. No que se refere ao religamento manual de linhas de transmissão: 
2.4.1. A definição da quantidade de tentativas de religamento manual de linha de transmissão, bem como o 
intervalo entre elas, é de responsabilidade do Agente e deve ser descrito no Cadastro de Informações 
Operacionais da respectiva Área Elétrica. 
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar autorização ao COSR -S 
autorização para religamento. Nes sa oportunidade, o Agente pode solicitar também a alteração no 
sentido normal para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em consideração as condições operativas do 
sistema.  
2.5. Quando caracterizado impedimento de linha de transmissão, deve ser adotado o procedimento descrito 
na instrução de operação em contingência da respectiva área elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação quando o Agente  tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO  
3.1. BARRAMENTO DE 525 KV 
A configuração do barramento de 525 kV é do tipo Barra Dupla (Barra A e Barra B). Na operação normal 
desse barramento , todos os disjuntores e seccionadoras devem estar fechados, exceto uma das 
seletoras de barra das linhas de transmissão. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Salto 
Caxias IO-OI.S.SCX 25 3.7.5.1. 17/10/2024 
 
Referência:  4 / 9 
 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 525 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S.  
As faixas de controle de tensão para o  barramento 525 kV estão estabelecidas no Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
Nada se aplica. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação : caracterizado quando não há tensão em todos os terminais de 
suas linhas de transmissão. 
• Desligamento parcial da Instalação : qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao COSR-S 
as seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2., sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia da operação da instalação na recomposição, 
deve ser utilizado o Subitem 5.3.  
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE  
No caso de desligamento total, o Agente Operador deve configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Abrir ou manter abertos: 
• os disjuntores das linhas de transmissão:  
LT 525 kV Salto Caxias / Salto Santiago; 
LT 525 kV Cascavel Oeste / Salto Caxias. 
• o disjuntor do módulo de interligação de barras de 525 kV. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Salto 
Caxias IO-OI.S.SCX 25 3.7.5.1. 17/10/2024 
 
Referência:  5 / 9 
 
• a seccionadora 29-39 (89TA), se a SE operando pela barra A antes do desligamento: (*), ou 
• a seccionadora 29-40 (89TB), se a SE operando pela barra B antes do desligamento: (*). 
(*) manobra necessária devido à lógica de acoplamento de gerador em barra morta implementada pela 
Copel GeT, que somente permite energizar o barramento de 525 kV se todas as seccionadoras 
conectadas a esse barramento estiverem abertas, exceto a seccionadora da unidade geradora que 
energizará o barramento. Desse modo, a primeira unidade geradora será conectada ao barramento no 
qual os demais equipamentos não estão conectados. 
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO 
A Instalação faz parte da recomposição da Área Gov. José Richa. O Agente Operador deve adotar os 
procedimentos a seguir para a recomposição fluente: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 Copel GeT 
Verificar que a Barra B (ou Barra A) foi 
energizada por uma das unidades geradoras 
da UHE Gov. José Richa. 
O fechamento do disjuntor será 
efetuado pelo operador da UHE  Gov. 
José Richa. 
1.1 Copel GeT 
Fechar a seccionadora do disjuntor do 
módulo de interligação de barras de 525 kV. 
- Se energizada a Barra B: fechar a 
Seccionadora 29-40 (89TB), ou 
- Se energizada a Barra A: fechar a 
Seccionadora 29-39 (89TA). 
 
1.2 Copel GeT 
Fechar o disjuntor do módulo de interligação 
de barras de 525 kV, energizando a barra A 
(ou barra B). 
 
2 Copel GeT 
Verificar que a  UHE Gov. José Richa  
sincronizou na Barra A (ou na Barra B) mais 
duas unidades geradoras, totalizando três 
unidades geradoras sincronizadas. 
O fechamento dos disjuntores será 
efetuado pelo operador da UHE  Gov. 
José Richa. 
Nota: a quarta unidade geradora da 
UHE Gov. José Richa não deve ser 
utilizada na fase fluente  da 
recomposição. 
2.1 Copel GeT 
Energizar a LT 525 kV Cascavel Oeste / Salto 
Caxias, enviando tensão para a SE Cascavel 
Oeste. 
- 3 UGs sincronizadas na UHE Gov. José 
Richa. 
2.2 Copel GeT 
Energizar a LT 525 kV Salto Caxias / Salto 
Santiago, enviando tensão para a SE Salto 
Santiago. 
 
- VSCX-525 ≤ 494 kV. 
- PGJR-total ≥ 280 MW. 
Nota: na Salto Santiago, a LT 525  kV 
Salto Caxias / Salto Santiago  é ponto 
de fechamento entre as áreas Gov. 
José Richa e Salto Santiago. 
Obs.: Após o término da fase fluente da recomposição, a Copel GeT deve solicitar autorização ao COSR-S para 
retornar o barramento à configuração normal de operação. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Salto 
Caxias IO-OI.S.SCX 25 3.7.5.1. 17/10/2024 
 
Referência:  6 / 9 
 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2., enquanto não houver 
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia da operação da instalação 
na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras.  
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, o 
Agente Operador  deve preparar a Instalação conforme Subitem 5.2 .1., sem necessidade de 
autorização do ONS. 
5.4.1.2. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador  
deve recompor a Instalação conforme Subitem 5.2.2., sem necessidade de autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalaçã o, conforme Subitem 5.4.1. 2., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 6.2.2., sem necessidade de 
autorização do ONS. 
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
os Agentes Operadores devem informar ao COSR -S, para que a recomposição da Instalação seja 
executada com controle do COSR-S. 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, após desligamento 
programado, de urgência ou de emergência, só podem ser efetuados com controle do COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Salto 
Caxias IO-OI.S.SCX 25 3.7.5.1. 17/10/2024 
 
Referência:  7 / 9 
 
equipamento, só podem ser executados com autonomia pelo Agente Operador da Instalação quando 
explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução de Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir autonomia, 
a energização deve ser executada com controle do COSR -S, conforme Instrução de Operação de 
Preparação para Manobras da respectiva área elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, o Agente Operador da Instalação deve verificar se existe 
tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pelo Agente Operador da Instalação 
quando estiver especificado nesta Instrução de O peração e estiverem atendidas as condições do 
Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou , quando permitida, em sentido  inverso, pode ser feita com 
autonomia pelo Agente Operador , conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica. 
 A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o Agente Operador da Instalação deve fechá-lo em anel desde que tenha autonomia para 
tal, adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S.  
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S. 
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE EQUIPAMENTOS 
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação, é 
sempre controlada pelo COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Salto 
Caxias IO-OI.S.SCX 25 3.7.5.1. 17/10/2024 
 
Referência:  8 / 9 
 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de equipamentos ou de linhas de transmissão. 
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado nas 
condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Linha de Transmissão Procedimentos Condições ou Limites Associados 
LT 525 kV Cascavel Oeste / 
Salto Caxias 
Sentido Normal: SE Salto Caxias envia tensão para a SE Cascavel Oeste 
Energizar a LT 525 kV Cascavel Oeste 
/ Salto Caxias. 
Sistema completo (de LT) na SE 
Salto Caxias 525 kV 
• VSCX-525 ≤ 547 kV. 
Sentido Inverso: SE Salto Caxias recebe tensão da SE Cascavel Oeste 
Ligar, em anel , a LT 525 kV Cascavel 
Oeste / Salto Caxias. 
∆δ ≤ 35°. 
LT 525 kV Salto Caxias / 
Salto Santiago 
Sentido Normal: SE Salto Caxias envia tensão para a SE Salto Santiago 
Energizar a LT 525 kV Salto Caxias / 
Salto Santiago. 
Sistema completo (de LT) na SE 
Salto Caxias 525 kV 
• VSCX-525 ≤ 542 kV, e 
• 2 ou mais UGs 
sincronizadas na UHE 
Gov. José Richa. 
Ou 
• VSCX-525 ≤ 535 kV, e 
• nenhuma ou 1 UG 
sincronizada na UHE Gov. 
José Richa. 
Sentido Inverso: SE Salto Caxias recebe tensão da SE Salto Santiago 
Ligar, em anel, a LT 525 kV Salto 
Caxias / Salto Santiago. 
∆δ ≤ 15°. 
7. NOTAS IMPORTANTES 
7.1. A Copel GeT, devido à lógica de acoplamento de gerador em barra morta implantada, deve manter a 
sua equipe de operação capacitada para analisar a situação operacional da instalação e realizar as 
manobras necessárias para o sincronismo da primeira unidade geradora em barra morta .  Entende-
se que por se tratar de uma subestação SF6, problemas associados à s manobras de seccionadoras 
são minimizados ; entretanto, é responsabilidade do Agente eventuais atrasos e risco dessas 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Salto 
Caxias IO-OI.S.SCX 25 3.7.5.1. 17/10/2024 
 
Referência:  9 / 9 
 
manobras, devido à lógica não usual de acoplamento de gerador em barra morta implantada pela 
Copel GeT. 
