Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação
Procedimentos Sistêmicos para a Operação da SE Salto Santiago
Código Revisão Item Vigência
IO-OI.S.SSA 30 3.7.5.1. 03/06/2024
.
MOTIVO DA REVISÃO
- Alteração das condições de autonomia do Agente para energização, desvinculando os procedimentos do 
Subitem 6.2.2 dos procedimentos de recomposição fluente da Instalação constantes no Subitem 5.2.2, 
passando-se a adotar, no Subitem 6.2.2, condições de sistema completo e/ou sistema N-1 para energização 
das seguintes linhas de transmissão e equipamentos:
•LT 525 kV Itá / Santo Santiago C1 ( ou C2) (exclusão de texto sobre desligamento total);
•LT 525 kV Ivaiporã (CGT Eletrosul) / Salto Santiago C1 (ou C2).
- Complementação do Subitem 5.4.2.3. para casos de desligamentos parciais.
- Adequação à RT-OI.BR revisão 36, com destaque à inclusão dos subitens 6.1.8 e 6.1.9. 
LISTA DE DISTRIBUIÇÃO
CNOS COSR-S CGT Eletrosul 
(COT Norte)
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE Salto 
Santiago IO-OI.S.SSA 30 3.7.5.1. 03/06/2024
Referência: 2 / 11
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. CONSIDERAÇÕES GERAIS .................................................................................................................3
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO..............................................................4
3.1. Barramento de 525 kV ..................................................................................................................4
3.2. Alteração da Configuração dos Barramentos................................................................................4
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL .............................................................................5
4.1. Procedimentos Gerais ...................................................................................................................5
4.2. Procedimentos Específicos............................................................................................................5
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................................................5
5.1. Procedimentos Gerais ...................................................................................................................5
5.2. Procedimentos para Recomposição Fluente.................................................................................5
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................5
5.2.2. Recomposição Fluente da Instalação ..........................................................................6
5.3. Procedimentos após Desligamento Total da Instalação................................................................7
5.3.1. Preparação da Instalação após Desligamento Total....................................................7
5.3.2. Recomposição após Desligamento Total da Instalação...............................................7
5.4. Procedimentos após Desligamento Parcial da Instalação .............................................................7
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ..............7
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................7
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS.....................................................8
6.1. Procedimentos Gerais ...................................................................................................................8
6.2. Procedimentos Específicos............................................................................................................9
6.2.1. Desenergização de Linhas de Transmissão e de Equipamentos..................................9
6.2.2. Energização de Linhas de Transmissão e de Equipamentos........................................9
7. NOTAS IMPORTANTES ...................................................................................................................11
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE Salto 
Santiago IO-OI.S.SSA 30 3.7.5.1. 03/06/2024
Referência: 3 / 11
1. OBJETIVO
Estabelecer os procedimentos para a operação da SE Salto Santiago nos aspectos de interesse sistêmico 
e de autonomia por parte da Instalação definidos pelo ONS, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede.
2. CONSIDERAÇÕES GERAIS
2.1. Os procedimentos contidos nesta Instrução de Operação são somente aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS.
2.2. A comunicação operacional entre o COSR-S e a Instalação, no que se refere aos equipamentos de 
manobra da Instalação, é realizada conforme segue:
Linha de Transmissão ou Equipamento Agente de Operação Agente Operador
Centro de 
Operação do 
Agente Operador
Barramento de 525 kV CGT Eletrosul CGT Eletrosul COT Norte
LT 525 kV Itá / Salto Santiago C1 CGT Eletrosul CGT Eletrosul COT Norte
LT 525 kV Itá / Salto Santiago C2 CGT Eletrosul CGT Eletrosul COT Norte
LT 525 kV Ivaiporã (CGT Eletrosul) / Salto 
Santiago C1 CGT Eletrosul CGT Eletrosul
COT Norte
LT 525 kV Ivaiporã (CGT Eletrosul) / 
Santo Santiago C2 CGT Eletrosul CGT Eletrosul
COT Norte
Copel GeT (linha de 
transmissão) CGT Eletrosul
COT Norte
Copel GeT (módulo) CGT Eletrosul COT Norte
LT 525 kV Salto Caxias / Santo Santiago
CGT Eletrosul 
(interligação de 
módulos)
CGT Eletrosul
COT Norte
LT 525 kV Salto Santiago / Segredo CGT Eletrosul CGT Eletrosul COT Norte
Engie 
(transformador) CGT Eletrosul
COT Norte
CGT Eletrosul 
(módulo) CGT Eletrosul
COT NorteTransformador TF 1 525/19/19 kV da 
UHE Salto Santiago
CGT Eletrosul 
(interligação de 
módulos)
CGT Eletrosul COT Norte
Engie 
(transformador) CGT Eletrosul COT NorteTransformador TF 2 525/19/19 kV da 
UHE Salto Santiago CGT Eletrosul 
(módulo) CGT Eletrosul COT Norte
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE Salto 
Santiago IO-OI.S.SSA 30 3.7.5.1. 03/06/2024
Referência: 4 / 11
Linha de Transmissão ou Equipamento Agente de Operação Agente Operador
Centro de 
Operação do 
Agente Operador
CGT Eletrosul 
(interligação de 
módulos)
CGT Eletrosul COT Norte
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte da Área 525 kV da Região Sul.
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos:
2.4.1. A definição da quantidade de tentativas de religamento manual de linha de transmissão ou de 
equipamento, bem como o intervalo entre elas, é de responsabilidade do Agente e devem estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica.
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR-S autorização 
para religamento. Nessa oportunidade, o Agente também pode solicitar a alteração no sentido 
normal para envio de tensão, caso não tenha autonomia para tal.
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR-S levará em consideração as condições operativas do 
sistema. 
2.5. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica.
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica.
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR-S, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação, quando o Agente tiver autonomia para energizar a 
linha de transmissão ou o equipamento.
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO
3.1. BARRAMENTO DE 525 KV
A configuração do barramento de 525 kV (Barra A e Barra B) é do tipo Disjuntor e Meio. Na operação 
normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados.
3.2. ALTERAÇÃO DA CONFIGURAÇÃO DOS BARRAMENTOS
A mudança de configuração do barramento de 525 kV desta Instalação é executada com controle do 
COSR-S.
A mudança de configuração dos demais barramentos é executada sob responsabilidade do Agente 
Operador da Instalação.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE Salto 
Santiago IO-OI.S.SSA 30 3.7.5.1. 03/06/2024
Referência: 5 / 11
4.CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL
4.1. PROCEDIMENTOS GERAIS
4.1.1. O barramento de 525 kV, pertencente à Rede de Operação, têm a sua regulação de tensão controlada 
pelo COSR-S. 
As faixas para controle de tensão para esse barramento estão estabelecidas no Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica.
4.1.2. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob a responsabilidade da Instalação.
4.2. PROCEDIMENTOS ESPECÍFICOS
Não se aplica.
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO
5.1. PROCEDIMENTOS GERAIS
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir:
•Desligamento total da Instalação: caracterizado quando não há tensão em todos os terminais de 
suas linhas de transmissão.
•Desligamento parcial da Instalação: qualquer outra configuração que não se enquadre como 
desligamento total.
5.1.2. Quando de desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao COSR-S 
as seguintes informações:
•horário da ocorrência;
•configuração da Instalação após a ocorrência;
•configuração da Instalação após ações realizadas com autonomia pela operação dessa.
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2, sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia do Agente Operador da instalação na 
recomposição, deve ser utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4. 
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE
No caso de desligamento total, o Agente Operador deve configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir.
Abrir ou manter abertos os disjuntores: 
•das linhas de transmissão:
LT 525 kV Salto Caxias / Salto Santiago;
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE Salto 
Santiago IO-OI.S.SSA 30 3.7.5.1. 03/06/2024
Referência: 6 / 11
LT 525 kV Salto Santiago / Segredo;
LT 525 kV Ivaiporã (CGT Eletrosul) / Salto Santiago C1;
LT 525 kV Ivaiporã (CGT Eletrosul) / Salto Santiago C2; 
LT 525 kV Itá / Salto Santiago C1;
LT 525 kV Itá / Salto Santiago C2.
•das interligações de módulos:
LT 525 kV Itá / Salto Santiago C2 – Barra B;
LT 525 kV Salto Santiago / Segredo – LT 525 kV Itá / Salto Santiago C1;
LT 525 kV Salto Santiago / Usina Hidrelétrica Salto Caxias – Transformador TF 2;
LT 525 kV Ivaiporã (CGT Eletrosul) / Salto Santiago C1 – Transformador TF 1;
LT 525 kV Ivaiporã (CGT Eletrosul) / Salto Santiago C2 – Barra B.
•dos transformadores:
TF 1 525/19/19 kV (da UHE Salto Santiago, lado de 525 kV);
TF 2 525/19/19 kV (da UHE Salto Santiago, lado de 525 kV).
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO
A Instalação faz parte da recomposição da Área Salto Santiago. o Agente Operador deve adotar os 
procedimentos a seguir para a recomposição fluente:
Passo Executor Procedimentos Condições ou Limites Associados
1 CGT Eletrosul 
(COT Norte)
Receber tensão por um dos 
transformadores 525/19/19 kV da UHE 
Salto Santiago, e energizar a barra B de 
525 kV da SE Salto Santiago.
1.1 CGT Eletrosul 
(COT Norte)
Energizar, pelo lado de 525 kV, o outro 
transformador 525/19/19kV da UHE 
Salto Santiago, enviando tensão para a 
UHE Salto Santiago.
1.2 CGT Eletrosul 
(COT Norte)
Energizar a LT 525 kV Ivaiporã (CGT 
Eletrosul) / Salto Santiago C2, enviando 
tensão para a SE Ivaiporã (CGT Eletrosul.
VSSA-525 ≤ 484 kV.
Após estarem sincronizadas três 
unidades geradoras na UHE Salto 
Santiago.
Obs.: A quantidade de unidades 
geradoras é verificada pelos 
módulos de 19 kV fechados, na UHE 
Salto Santiago.
1.2.1 CGT Eletrosul 
(COT Norte)
Energizar a barra A 525 kV. VSSA-525 ≤ 484 kV.
Após estarem sincronizadas três 
unidades geradoras na UHE Salto 
Santiago.
Obs.: A quantidade de unidades 
geradoras é verificada pelos 
módulos de 19 kV fechados, na UHE 
Salto Santiago.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE Salto 
Santiago IO-OI.S.SSA 30 3.7.5.1. 03/06/2024
Referência: 7 / 11
Passo Executor Procedimentos Condições ou Limites Associados
1.3 CGT Eletrosul 
(COT Norte)
Energizar a LT 525kV Ivaiporã (CGT 
Eletrosul) / Salto Santiago C1, enviando 
tensão para a SE Ivaiporã (CGT 
Eletrosul).
Após fluxo de potência ativa superior 
a 300 MW na LT 525 kV Ivaiporã (CGT 
Eletrosul) / Salto Santiago C2.
2 CGT Eletrosul 
(COT Norte)
Receber tensão da SE Segredo pela LT 
525 kV Salto Santiago / Segredo e ligar, 
em anel.
∆δ ≤ 27°.
3 CGT Eletrosul 
(COT Norte)
Receber tensão da SE Salto Caxias pela 
LT 525 kV Salto Caxias / Salto Santiago e 
ligar, em anel.
∆δ ≤ 15°.
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1.
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2, enquanto não 
houver intervenção do COSR-S no processo de recomposição ou interrupção da autonomia deste na 
recomposição. 
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR-S, 
conforme procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja: 
•ausência de tensão em todos os barramentos,
•ausência de fluxo de potência ativa nas linhas de transmissão, e
•existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, O 
Agente Operador da Instalação deve preparar a Instalação conforme Subitem 5.2.1, sem 
necessidade de autorização do ONS.
5.4.1.2. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação.
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., O Agente Operador 
deve recompor a Instalação conforme Subitem 5.2.2, sem necessidade de autorização do ONS.
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.4., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 6.2.2., sem necessidade de 
autorização do ONS.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE Salto 
Santiago IO-OI.S.SSA 30 3.7.5.1. 03/06/2024
Referência: 8 / 11
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
os Agentes Operadores devem informar ao COSR-S, para que a recomposição da Instalação seja 
executada com controle do COSR-S.  
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS
6.1. PROCEDIMENTOS GERAIS
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S.
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, após desligamento 
programado, de urgência ou de emergência, só podem ser efetuados com controle do COSR-S.
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelo Agente Operador da Instalação quando 
estiverem explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução de 
Operação.
Quando as condições ou limites associados não estiverem atendidos ou quando não existir autonomia, 
a energização deve ser executada com controle do COSR-S, conforme Instrução de Operação de 
Preparação para Manobras da respectiva área elétrica.
6.1.4. Antes do fechamento de qualquer disjuntor, o Agente Operador da Instalação deve verificar se existe 
tensão de retorno e se a condição de fechamento será em anel.
O fechamento em anel só pode ser executado com autonomia pelo Agente Operador da Instalação 
quando estiver especificado nesta Instrução de Operação e estiverem atendidas as condições do 
Subitem 6.2.2.
O fechamento de paralelo só pode ser efetuado com controle do COSR-S.
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento:
6.1.5.1. A energização em sentido normal ou, quando permitida, em sentido inverso, pode ser feita com 
autonomia pelo Agente Operador, conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica. 
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente. 
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o Agente Operador da Instalação deve fechá-lo em anel desde que tenha autonomia para 
tal, adotando as condições para o fechamento constantes no Subitem 6.2.2.
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE Salto 
Santiago IO-OI.S.SSA 30 3.7.5.1. 03/06/2024
Referência: 9 / 11
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S.
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra.
6.2. PROCEDIMENTOS ESPECÍFICOS
6.2.1. DESENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação, é 
sempre controlada pelo COSR-S.
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S.
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de equipamentos ou de linhas de transmissão.
o Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado nas 
condições de energização para a manobra.
Para os demais desligamentos parciais, proceder conforme Subitem 5.4.
Linha de Transmissão / 
Equipamento Procedimentos Condições ou Limites Associados
Sentido Normal: SE Salto Santiago envia tensão para a SE Itá 
Energizar a LT 525 kV Itá / Salto 
Santiago C1 (ou C2).
Sistema completo (de LT) na SE Salto 
Santiago 525 kV
- VSSA-525 ≤ 545 kV, e
- 1 ou mais UGs sincronizadas na UHE 
Salto Santiago. 
Antes de energizar cada LT, verificar 
fluxo de potência ativa no circuito da LT 
525 kV Itá / Salto Santiago que já foi 
energizado.
Sentido Inverso: SE Salto Santiago recebe tensão da SE Itá 
LT 525 kV Itá / Salto 
Santiago C1 (ou C2)
Ligar, em anel, LT 525kV Itá / Salto 
Santiago C1 (ou C2).
∆δ ≤ 35°.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE Salto 
Santiago IO-OI.S.SSA 30 3.7.5.1. 03/06/2024
Referência: 10 / 11
Linha de Transmissão / 
Equipamento Procedimentos Condições ou Limites Associados
Sentido Normal: SE Salto Santiago envia tensão para a SE Ivaiporã (CGT 
Eletrosul) 
Energizar a LT 525 kV Ivaiporã (CGT 
Eletrosul) / Salto Santiago C1 (ou 
C2).
Sistema completo (de LT) ou N-1 (de LT) 
na SE Salto Santiago 525 kV
•V SSA-525 ≤ 538 kV, e
•1 ou mais UGs sincronizadas na UHE 
Salto Santiago (gerador ou 
compensador síncrono).
Antes de energizar cada LT, verificar 
fluxo de potência ativa no circuito da LT 
525 kV Ivaiporã (CGT Eletrosul) / Salto 
Santiago que já foi energizado.
Sentido Inverso: SE Salto Santiago recebe tensão da SE Ivaiporã (CGT 
Eletrosul) 
LT 525 kV Ivaiporã (CGT 
Eletrosul) / Salto Santiago 
C1 (ou C2) 
Ligar, em anel, a LT 525kV Ivaiporã 
(CGT Eletrosul) / Salto Santiago C1 
(ou C2).
∆δ ≤ 30°.
Sentido Normal: SE Salto Santiago recebe tensão da SE Salto Caxias 
Ligar, em anel, a LT 525 kV Salto 
Caxias / Salto Santiago.
∆δ ≤ 15°.
Sentido Inverso: SE Salto Santiago envia tensão para a SE Salto Caxias 
LT 525 kV Salto Caxias / 
Salto Santiago 
A energização em sentido inverso é controlada pelo COSR-S, conforme 
IO-PM.S.5SU.
Sentido Normal: SE Salto Santiago recebe tensão da SE Segredo 
Ligar, em anel, a LT 525 kV Salto 
Santiago / Segredo.
∆δ ≤ 27°.
Sentido Inverso: SE Salto Santiago envia tensão para a SE Segredo 
LT 525 kV Salto Santiago / 
Segredo 
A energização em sentido inverso é controlada pelo COSR-S, conforme 
IO-PM.S.5SU.
Sentido Normal: A partir do lado de 525 kV 
Energizar, pelo lado de 525 kV, o 
transformador TF 1 525/19/19 kV 
(ou TF 2) da UHE Salto Santiago.
VSSA-525 ≤ 550 kV.
O outro transformador 525/19/19 kV da 
SE UHE Salto Santiago deve estar com 
fluxo de potência ativa ou, no caso de 
desligamento total, energizando a 
barra B 525 kV.
Transformadores TF 1 ou 
TF 2 525/19/19 kV da UHE 
Salto Santiago
Sentido Inverso: A partir do lado de 19 kV pela UHE Salto Santiago 
(Sentido Inverso):
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE Salto 
Santiago IO-OI.S.SSA 30 3.7.5.1. 03/06/2024
Referência: 11 / 11
Linha de Transmissão / 
Equipamento Procedimentos Condições ou Limites Associados
Ligar, energizando a barra B 525 kV, 
o transformador TF 1 525/19/19 kV 
(ou TF 2) da UHE Salto Santiago.
A barra B 525 kV da SE Salto Santiago 
deve estar desenergizada.
7. NOTAS IMPORTANTES
7.1. Os disjuntores de interligação de módulos devem ser fechados, em anel, toda vez que forem 
recompostos os dois circuitos do mesmo vão ou quando, num dos lados do vão, for uma barra.
