 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.11 
 
Cadastro de Informações Operacionais. 
Cadastro de Informações Operacionais de Faixas para Controle de Tensão da Interligação 
Sul / Sudeste   
 
Código Revisão Item Vigência 
CD-CT.SSE.03 7 2.6.1 26/01/2024 
 
 
MOTIVO DA REVISÃO 
- Exclusão da definição da grandeza VT, sua definição consta na IO-ON.SSE. 
- Substituição, na Lista de Distribuição, do Agente Operador, de CGT Eletrosul, para CGT Eletrosul (COT Norte). 
- Ajuste na faixa de tensão Vt para todas as faixas de carga, alterando o limite inferior de 765 KV para 757 kV, 
modificando a tabela do item 4.2 
 
 
LISTA DE DISTRIBUIÇÃO 
ATE CGT Eletrosul (COT Norte) CNOS COPEL GeT COSR-S 
COSR-SE FURNAS ITAIPU TAESA  
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Interligação Sul / Sudeste   CD-CT.SSE.03 7 2.6.1 26/01/2024 
 
Referência:  2 / 5 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONCEITOS ................................ ................................ ................................ ................................ ... 3 
3. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO ................................ .......................  4 
4.1. Faixas de tensão para Barramentos da Rede de Operação .......................................................... 4 
4.2. Faixas de tensão da Rede de Operação em pontos específicos de controle em linhas de transmissão com 
bancos de capacitores série .......................................................................................................... 4 
4.3. Demais Faixas de tensão para a Rede de Operação ...................................................................... 4 
 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Interligação Sul / Sudeste   CD-CT.SSE.03 7 2.6.1 26/01/2024 
 
Referência:  3 / 5 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
1. OBJETIVO 
Apresentar as faixas de controle de tensão dos barramentos da Rede de Operação e dos secundários das 
transformações da Rede de Operação , cujos barra mentos não pertencem à Rede de Operação , a serem 
controlados pelos operadores dos Centros de Operação do ONS e pela Operação dos agentes envolvidos. 
2. CONCEITOS 
2.1. As faixas de controle de tensão são estabelecidas em função Carga SIN sMMGD, conforme definido na 
IO-ON.SSE. 
2.2. Os períodos de  carga mínima, leve, média e pesada definidos  para o controle de tensão estão 
estabelecidos em função das faixas horárias, conforme tabela abaixo, que tem como referência o horário 
de Brasília. 
Período Segunda Terça a Sábado Domingos e Feriados 
00:00 às 05:00 Mínima Leve Leve 
05:00 às 07:00 Mínima Leve Mínima 
07:00 às 10:00 Média Média Mínima 
10:00 às 17:00 Média Média Leve 
17:00 às 22:00 Pesada Pesada Média 
22:00 às 24:00 Média Média Leve 
3. CONSIDERAÇÕES GERAIS 
3.1. Este Cadastro de Informações Operacionais apresenta os valores das faixas para controle de tensão de 
barramentos da Rede de Operação, de secundários das transformações da Rede de Operação cujas 
barras não pertencem à Rede de Operação  e de pontos específicos de controle em linhas de 
transmssão com bancos de capacitores série. 
3.2. Os Centros de Operação do ONS, nas ações de coordenação, supervisão e controle , devem observar 
os limites operacionais constantes neste Cadastro de Informações Operacionais. 
3.3. A operação dos agentes, em suas ações de comando e execução, deve observar os limites operacionais 
constantes neste Cadastro de Informações Operacionais. 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Interligação Sul / Sudeste   CD-CT.SSE.03 7 2.6.1 26/01/2024 
 
Referência:  4 / 5 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO 
4.1. FAIXAS DE TENSÃO PARA BARRAMENTOS DA REDE DE OPERAÇÃO 
Subestação / Usina Faixas de Tensão  (kV) em função do(a) Carga SIN 
sMMGD 
Nome Tensão 
(kV) 
>=63.000 
(Pesada) 
>=63.000 
(Média) 
<63.000 
(Leve) 
<63.000 
(Mínima) 
SE Tijuco Preto 765 727 - 800 727 - 800 690 - 800 690 - 800 
UHE Itaipu 60 Hz 500 500 - 525 500 - 525 480 - 525 480 - 525 
 
 
4.2. FAIXAS DE TENSÃO DA REDE DE OPERAÇÃO EM PONTOS ESPECÍFICOS DE CONTROLE EM LINHAS DE 
TRANSMISSÃO COM BANCOS DE CAPACITORES SÉRIE 
Barramento Tensão (kV) Faixas de tensão (kV) 
(Todos os períodos de carga) 
VT 765 757 – 800 
Obs.: VT – Conforme definido na IO-ON.SSE. 
4.3. DEMAIS FAIXAS DE TENSÃO PARA A REDE DE OPERAÇÃO 
Os barramentos da Rede de Operação e os secundários das transformações da Rede de Operação, que não 
possuem faixas de tensão definidas nos itens anteriores, devem operar com tensões nas seguintes faixas 
operativas, conforme estabelecido nos Procedimentos de Rede e reproduzido na tabela a seguir: 
Tensão Nominal (kV) Faixa de Tensão (kV) 
765 690 a 800 
525 500 a 550  
500 500 a 550  
440 418 a 460  
345 328 a 362  
230 218 a 242  
138 131 a 145  
88 83,6 a 92,4 
69 65,6 a 72,4 
34,5 32,8 a 36,2 
23 21,8 a 24,2 
13,8 13,1 a 14,5 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Interligação Sul / Sudeste   CD-CT.SSE.03 7 2.6.1 26/01/2024 
 
Referência:  5 / 5 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
 
 
