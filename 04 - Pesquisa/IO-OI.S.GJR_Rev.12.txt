Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação
Procedimentos Sistêmicos para a Operação da UHE Gov. José Richa
Código Revisão Item Vigência
IO-OI.S.GJR 12 3.7.5.1. 14/06/2024
.
MOTIVO DA REVISÃO
- Adequação do Subitem 5.1.1, por se tratar de desligamento total de usina.
- Complementação do Subitem 5.4.2.
- Exclusão do antigo Subitem 6.1.4. por não ser pertinente para usina. 
- Adequação à RT-OI.BR revisão 36, com destaque à inclusão dos Subitens 6.1.5. e 6.1.6. 
LISTA DE DISTRIBUIÇÃO
CNOS COSR-S Copel GeT
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE 
Gov. José Richa IO-OI.S.GJR 12 3.7.5.1. 14/06/2024
Referência: 2 / 8
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. CONSIDERAÇÕES GERAIS .................................................................................................................3
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO..............................................................3
3.1. Unidades Geradoras......................................................................................................................3
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL......................................................3
4.1. Procedimentos Gerais ...................................................................................................................3
4.2. Procedimentos Específicos............................................................................................................4
4.2.1. Operação das Unidades Geradoras como Compensadores Síncronos........................4
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA USINA...........................................................................4
5.1. Procedimentos Gerais ...................................................................................................................4
5.2. Procedimentos para Recomposição Fluente.................................................................................5
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................5
5.2.2. Recomposição Fluente da Instalação ..........................................................................6
5.3. Procedimentos após Desligamento Total da Instalação................................................................6
5.3.1. Preparação da Instalação após Desligamento Total....................................................6
5.3.2. Recomposição após Desligamento Total da Instalação...............................................6
5.4. Procedimentos após Desligamento Parcial da Instalação .............................................................6
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ..............6
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................6
6. MANOBRAS DE DESENERGIZAÇÃO E SINCRONISMO DE UNIDADES GERADORAS..............................7
6.1. Procedimentos Gerais ...................................................................................................................7
6.2. Procedimentos Específicos............................................................................................................7
6.2.1. Desligamento de Unidades Geradoras ........................................................................7
6.2.2. Sincronismo de Unidades Geradoras ..........................................................................7
7. NOTAS IMPORTANTES .....................................................................................................................8
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE 
Gov. José Richa IO-OI.S.GJR 12 3.7.5.1. 14/06/2024
Referência: 3 / 8
1. OBJETIVO
Estabelecer os procedimentos para a operação normal da UHE Gov. José, definidos pelo ONS, responsável 
pela coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos 
de Rede.
2.  CONSIDERAÇÕES GERAIS
2.1. Os procedimentos contidos nesta Instrução de Operação são somente aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da Instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementariedade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS.
2.2. A comunicação operacional entre o COSR-S e a UHE Gov. José Richa, de propriedade da Copel GeT, é 
realizada pela Copel GeT, agente responsável pela operação da Instalação, por intermédio do COGT 
Copel.
2.3. As unidades geradoras e equipamentos desta Instalação fazem parte da Área 525 kV Região Sul.
2.4. O COSR-S controla e supervisiona o despacho de geração desta Usina.
2.5. Esta Usina:
•é despachada centralizadamente;
•está conectada na Rede de Operação;
•participa do Controle Automático da Geração – CAG;
•é de autorrestabelecimento integral;
•é fonte para início do processo de recomposição fluente da Área Gov. José Richa;
•é responsável pelo controle de frequência e tensão da Área Gov. José Richa na fase de 
recomposição fluente, sendo o controle da frequência efetuada pela operação da Usina até 
orientação diferente do COSR-S.
2.6. Os dados operacionais desta Usina estão descritos no Cadastro de Informações Operacionais da 
respectiva Área Elétrica.
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO
3.1. UNIDADES GERADORAS
Na operação normal, as unidades geradoras G1, G2, G3 e G4 16 kV estão conectadas ao barramento de 
525 kV da SE Salto Caxias, por meio de seus respectivos transformadores elevadores, com os disjuntores de 
525 kV fechados e todas as seccionadoras de 525 kV fechadas, exceto uma das seletoras de barras de 525 kV. 
Essas unidades geradoras devem operar na configuração de barras constante na IO-OI.S.SCX.
4.CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL
4.1. PROCEDIMENTOS GERAIS
4.1.1. A Usina deve manter os valores de geração de acordo com os valores programados, constantes no 
Programa Diário de Operação – PDO. Para atendimento dos valores programados constantes no PDO, 
não é necessária a autorização prévia do COSR-S.
Qualquer alteração no valor de geração da Usina em relação ao valor de geração ao constante no 
PDO somente pode ser executada após autorização do COSR-S.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE 
Gov. José Richa IO-OI.S.GJR 12 3.7.5.1. 14/06/2024
Referência: 4 / 8
As reprogramações de geração quando de necessidades sistêmicas serão executadas pela Usina 
quando solicitadas pelo COSR-S.
Após reprogramação de geração solicitada pelo COSR-S, o Agente somente poderá alterar a geração 
da Usina com autorização do ONS, inclusive para adoção de valores contidos no PDO.
4.1.2. Os desvios de geração da Usina em relação aos valores previstos no PDO ou em relação às 
reprogramações devem ser controlados observando-se os valores máximos permitidos explicitados 
na Instrução de Operação IO-CG.BR.01 – Controle da Geração em Condição Normal.
4.1.3. Quando não existir ou não estiver disponível a supervisão da Usina para o ONS, essa deve seguir as 
orientações para envio de dados conforme Rotina Operacional RO-AO.BR.08 – Apuração de Dados 
de Despacho de Geração e de Intercâmbios.
4.1.4. A Usina deve registrar e informar imediatamente os seguintes dados ao COSR-S:
•movimentação de unidades geradoras hidráulicas (mudança de estado operativo / 
disponibilidade); 
•restrições e ocorrências na Usina ou na conexão elétrica que afetem a disponibilidade de geração, 
com o respectivo valor da restrição, contendo o horário de início e término e a descrição do 
evento;
•demais informações sobre a operação da Instalação, solicitadas pelo ONS.
4.1.5. O controle de tensão, por meio da geração ou absorção de potência reativa das unidades geradoras 
da Usina, é controlado pelo COSR-S, com comando e execução pela operação do Agente.
4.2. PROCEDIMENTOS ESPECÍFICOS
4.2.1. OPERAÇÃO DAS UNIDADES GERADORAS COMO COMPENSADORES SÍNCRONOS
A conversão da modalidade de operação de gerador para compensador síncrono, bem como a reversão de 
compensador síncrono para gerador, deve ser executada sob controle do COSR-S.
Devem ser observados os procedimentos relacionados a prestação de Serviços Ancilares de Suporte de 
Reativos, conforme IO-CG.BR.01 – Controle da Geração em Condição Normal.
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA USINA
5.1. PROCEDIMENTOS GERAIS
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir:
•Desligamento total da Instalação: caracterizado por meio da verificação de ausência de tensão em 
todos os terminais de suas conexões ou ausência de fluxo de potência ativa nessas conexões.
•Desligamento parcial da Instalação: qualquer outra configuração que não se enquadre como 
desligamento total.
5.1.2. Quando de desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao COSR-
S as seguintes informações:
•horário da ocorrência;
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE 
Gov. José Richa IO-OI.S.GJR 12 3.7.5.1. 14/06/2024
Referência: 5 / 8
•configuração da Instalação após a ocorrência;
•configuração da Instalação após ações realizadas com autonomia pela operação dessa.
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2., sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia da operação da instalação na recomposição, 
deve ser utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4. 
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE
No caso de desligamento total, o Agente Operador deve configurar os disjuntores das seguintes unidades 
geradoras, conforme condição apresentada a seguir:
Abrir ou manter abertos os disjuntores:
•das unidades geradoras:
G1 16 kV (de 525 kV na SE Salto Caxias);
G2 16 kV (de 525 kV na SE Salto Caxias);
G3 16 kV (de 525 kV na SE Salto Caxias);
G4 16 kV (de 525 kV na SE Salto Caxias).
A Copel GeT deve escolher uma das unidades geradoras para energizar uma das barras de 525 kV da SE Salto 
Caxias:
•Se a SE operando pela Barra A antes do desligamento: 
oSe G1: abrir a seccionadora 29-04 (89G1A) e fechar a seccionadora 29-05 (89G1B) (*), ou
oSe G2: abrir a seccionadora 29-11 (89G2A) e fechar a seccionadora 29-12 (89G2B) (*), ou
oSe G3: abrir a seccionadora 29-18 (89G3A) e fechar a seccionadora 29-19 (89G3B) (*), ou
oSe G4: abrir a seccionadora 29-25 (89G4A) e fechar a seccionadora 29-26 (89G4B) (*).
•Se a SE operando pela Barra B antes do desligamento: 
oSe G1: abrir a seccionadora 29-05 (89G1B) e fechar a seccionadora 29-04 (89G1A) (*), ou
oSe G2: abrir a seccionadora 29-12 (89G2B) e fechar a seccionadora 29-11 (89G2A) (*), ou
oSe G3: abrir a seccionadora 29-19 (89G3B) e fechar a seccionadora 29-18 (89G3A) (*), ou
oSe G4: abrir a seccionadora 29-26 (89G4B) e fechar a seccionadora 29-25 (89G4A) (*).
(*) manobra necessária devido à lógica de acoplamento de gerador em barra morta implementada 
pela Copel GeT, que somente permite energizar o barramento de 525 kV se todas as seccionadoras 
conectadas a esse barramento estiverem abertas, exceto a seccionadora da unidade geradora que 
energizará o barramento. Deste modo, para minimizar manobras de seccionadoras, a primeira unidade 
geradora será conectada ao barramento no qual os demais equipamentos não estão conectados.  
Retirar ou manter fora de operação o Controle Automático de Geração.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE 
Gov. José Richa IO-OI.S.GJR 12 3.7.5.1. 14/06/2024
Referência: 6 / 8
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO 
A Instalação faz parte da recomposição da Área Gov. José Richa. O Agente Operador deve adotar 
procedimentos a seguir para a recomposição fluente:
Passo Executor Procedimentos Condições ou Limites Associados
1 Copel GeT
Partir uma unidade geradora, energizar o 
barramento de 525 kV – Barra B (ou Barra A) da 
SE Salto Caxias e controlar a frequência em 
60 Hz.
Manter VGJR-16 = 14,4 kV.
1.1 Copel GeT
Partir a segunda unidade geradora, sincronizá-la 
na Barra A de 525 kV (ou na Barra B) da SE Salto 
Caxias e controlar a frequência em 60 Hz.
Manter VGJR-16 = 14,4 kV.
Após o disjuntor de interligação de 
módulos estar fechado e a Barra A 
de 525 kV (ou Barra B) energizada.
1.2 Copel GeT
Partir a terceira unidade geradora, sincronizá-la 
na Barra A de 525 kV (ou na Barra B) da SE Salto 
Caxias e controlar a frequência em 60 Hz.
Manter VGJR-16 = 14,4 kV.
Nota: a quarta unidade geradora 
da UHE Gov. José Richa não deve 
ser utilizada na fase fluente da 
recomposição, devido à cavitação.
Obs.: Após o término da fase fluente da recomposição, a Copel GeT deve solicitar autorização ao COSR-S para 
retornar o barramento à configuração normal de operação. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1.
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2., enquanto não 
houver intervenção do COSR-S no processo de recomposição ou interrupção da autonomia da 
operação da instalação na recomposição. 
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR-S. 
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL
Para os desligamentos parciais da Instalação, não há necessidade de preparação da Instalação.
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
O Agente Operador deve recompor a Instalação conforme Subitem 6.2.2., sem necessidade de 
autorização do ONS.
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
os Agentes Operadores devem informar ao COSR-S, para que a recomposição da Instalação seja 
executada com controle do COSR-S.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE 
Gov. José Richa IO-OI.S.GJR 12 3.7.5.1. 14/06/2024
Referência: 7 / 8
6. MANOBRAS DE DESENERGIZAÇÃO E SINCRONISMO DE UNIDADES GERADORAS 
6.1. PROCEDIMENTOS GERAIS
6.1.1. Os procedimentos para desligamento programado ou de urgência de unidades geradoras, só podem 
ser efetuados com controle do COSR-S.
6.1.2. Os procedimentos para sincronismo de unidades geradoras, após desligamentos programados, de 
urgência ou de emergência, só podem ser efetuados com controle do COSR-S.
6.1.3. Os procedimentos para sincronismo de unidades geradoras, após desligamento automático sem 
atuação de proteção que impeça o retorno do equipamento, só podem ser executados com 
autonomia pelo Agente Operador da Instalação quando estiverem explicitados e estiverem atendidas 
as condições do Subitem 6.2.2. desta Instrução de Operação.
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR-S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica.
6.1.4. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente. 
6.1.5. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S. 
6.1.6. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S.
6.1.7. A partida de unidades geradoras deve seguir critérios próprios do Agente. O sincronismo à Rede de 
Operação só pode ser executado com autonomia pela operação da Usina quando estiver explicitado 
e estiverem atendidas as condições do Subitem 6.2.2. A tomada de carga deve ser realizada com 
controle do COSR-S.
6.2. PROCEDIMENTOS ESPECÍFICOS
6.2.1. DESLIGAMENTO DE UNIDADES GERADORAS 
O desligamento de unidades geradoras, é sempre controlado pelo COSR-S.
6.2.2. SINCRONISMO DE UNIDADES GERADORAS
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S.
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de unidades geradoras.
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais unidades geradoras em operação, conforme explicitado nas condições de 
energização para a manobra.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE 
Gov. José Richa IO-OI.S.GJR 12 3.7.5.1. 14/06/2024
Referência: 8 / 8
Para os demais desligamentos parciais, proceder conforme Subitem 5.4.
Unidade Geradora Procedimentos Condições ou Limites Associados
Unidade Geradora
G1, G2, G3 ou G4 16 kV
Partir a unidade geradora e 
sincronizá-la ou energizar o 
barramento de 525 kV (na SE Salto 
Caxias).
14,4 kV ≤ VGJR-16 ≤ 16,8 kV.
Gerar o mínimo necessário para 
manter a unidade geradora 
sincronizada e solicitar autorização 
ao COSR-S para elevar a geração.
7. NOTAS IMPORTANTES
7.1. A Copel GeT, devido à lógica de acoplamento de gerador em barra morta implantada, deve manter a 
sua equipe de operação capacitada para analisar a situação operacional da instalação e realizar as 
manobras necessárias para o sincronismo da primeira unidade geradora em barra morta. Entende-se 
que por se tratar de uma subestação SF6, problemas associados às manobras de seccionadoras são 
minimizados; entretanto, é responsabilidade do Agente eventuais atrasos e risco dessas manobras, 
devido à lógica não usual de acoplamento de gerador em barra morta implantada pela Copel GeT.
