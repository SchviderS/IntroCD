 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Klabin Celulose 
 
Código Revisão Item Vigência 
IO-OI.S.KCL 13 3.7.5.4. 25/09/2024 
 
. 
MOTIVO DA REVISÃO 
- Alteração das condições de autonomia do Agente para energização, desvinculando os procedimentos do 
Subitem 6.2.2 dos procedimentos de recomposição fluente da Instalação constantes no Subitem 5.2.2, 
passando-se a adotar, no Subitem 6.2.2, condições de sis tema completo e/ou sistema N-1 para energização 
das seguintes linhas de transmissão e equipamentos: 
• LT 230 kV Figueira / Klabin Celulose. 
 
- Alteração das condições de autonomia do Agente para fechamento em anel, no Subitem 6.2.2, das seguintes 
linhas de transmissão e equipamentos: 
• LT 230 kV Castro Norte / Klabin Celulose. 
- Inclusão dos Subitens 5.4.1.2. e 5.4.2.2. e complementação do Subitem 5.4.2.3. para casos de desligamentos 
parciais. 
- Adequação à RT-OI.BR revisão 37, com destaque à inclusão dos Subitens 6.1.8 e 6.1.9.  
 
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S Copel GeT Klabin Celulose 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Klabin 
Celulose IO-OI.S.KCL 13 3.7.5.4. 25/09/2024 
 
Referência: 2 / 9 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  4 
3.1. Barramento de 230 kV ................................................................................................................... 4 
3.2. Alteração da Configuração dos Barramentos ................................................................................ 4 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 4 
4.1. Procedimentos Gerais ................................................................................................................... 4 
4.2. Procedimentos Específicos ............................................................................................................ 5 
4.2.1. Operação dos Comutadores de Tapes Sob Carga (LTC) .............................................. 5 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  5 
5.1. Procedimentos Gerais ................................................................................................................... 5 
5.2. Procedimentos Para Recomposição Fluente ................................................................................. 5 
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................ 5 
5.2.2. Recomposição fluente da Instalação ........................................................................... 6 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 6 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 6 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 6 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 7 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 7 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 7 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 7 
6.1. Procedimentos Gerais ................................................................................................................... 7 
6.2. Procedimentos Específicos ............................................................................................................ 8 
6.2.1. Desenergização de Equipamentos .............................................................................. 8 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ....................................... 9 
7. NOTAS IMPORTANTES ................................ ................................ ................................ .................. 9 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Klabin 
Celulose IO-OI.S.KCL 13 3.7.5.4. 25/09/2024 
 
Referência: 3 / 9 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE Ponta Grossa, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelos Agentes Operadores da Instalação, devendo fazer parte do manual 
de operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade 
das ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR -S e a Instalação, no que se refere aos equipamentos de 
manobra na Instalação, é realizada conforme segue. 
Linha de Transmissão ou 
Equipamento Agente de Operação Agente Operador Centro de Operação 
do Agente Operador  
Barramento de 230 kV Copel GeT Copel GeT COGT Copel 
LT 230 kV Castro Norte / 
Klabin Celulose Copel GeT Copel GeT COGT Copel 
LT 230 kV Figueira / Klabin 
Celulose  Copel GeT Copel GeT COGT Copel 
LT 230 kV Klabin Celulose / 
Mauá Copel GeT Copel GeT COGT Copel 
Transformador TF-1 
230/34,5/13,8 kV 
(*) 
Klabin S.A. Klabin Celulose Centro de Operação 
Klabin Celulose 
Transformador TF-2 
230/34,5/13,8 kV 
(*) 
Klabin S.A. Klabin Celulose Centro de Operação 
Klabin Celulose 
LT 230 kV Klabin Celulose / 
Klabin Monte Alegre 
(**) 
Klabin S.A. Copel GeT COGT Copel 
(ver item 7.1) 
(*) Somente os módulos de 230 kV dos Transformadores TF-1 e TF-2 230/34,5/13,8 kV, na SE Klabin Celulose, 
pertencem à Rede de Operação, conforme a Rotina Operacional RO-RD.BR.01. 
(**) Somente o módulo de 230 kV da LT 230 kV Klabin Celulose / Klabin Monte Alegre, na SE Klabin Celulose, 
pertence à Rede de Operação, conforme a Rotina Operacional RO-RD.BR.01. 
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte da Área 230 kV do Paraná. 
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.4.1. A definição da quantidade de tentativas de religamento manual de linhas de transmissão ou de 
equipamentos, bem como o intervalo entre elas, é de responsabilidade do Agente e devem estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica.  
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente , e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar autorização ao COSR-S 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Klabin 
Celulose IO-OI.S.KCL 13 3.7.5.4. 25/09/2024 
 
Referência: 4 / 9 
 
autorização para religamento. Nes sa oportunidade, o Agente pode solicitar também a alteração no 
sentido normal para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em considera ção as condições operativas do 
sistema. 
2.5. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2. desta Instrução de Operação quando o Agente  tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 230 KV 
A configuração do barramento de 230 kV é do tipo Barra Dupla (Barra BP-1 e Barra BP-2) a Quatro Chaves. 
Na operação normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados, exceto 
as seccionadoras de transferência e uma das seletoras de barra das linhas de transmissão ou equipamentos. 
A Instalação deve operar com a seguinte configuração: 
Em uma das Barras – BP-1 (ou BP-2) Na outra Barra – BP-2 (ou BP-1) 
LT 230 kV Figueira / Klabin Celulose 
(ou LT 230 kV Klabin Celulose / Mauá) 
LT 230 kV Klabin Celulose / Mauá 
(ou LT 230 kV Figueira / Klabin Celulose) 
LT 230 kV Castro Norte / Klabin Celulose LT 230 kV Klabin Celulose / Klabin Monte Alegre 
Transformador TF-1 (ou TF-2) 230/34,5/13,8 kV Transformador TF-2 (ou TF-1) 230/34,5/13,8 kV 
3.2. ALTERAÇÃO DA CONFIGURAÇÃO DOS BARRAMENTOS 
A mudança de configuração do barramento de 230 kV desta Instalação é executada com controle do COSR-
S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade dos Agentes 
Operadores da Instalação. 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Klabin 
Celulose IO-OI.S.KCL 13 3.7.5.4. 25/09/2024 
 
Referência: 5 / 9 
 
As faixas de controle de tensão desse barramento estão estabelecidas no Cadastro de Informações 
Operacionais das Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob a responsabilidade da Instalação. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
4.2.1. OPERAÇÃO DOS COMUTADORES DE TAPES SOB CARGA (LTC) 
Os LTCs dos transformadores TF-1 e TF-2 230 / 34,5 / 13,8 kV operam em modo automático.  
A movimentação dos comutadores é executada com autonomia pela operação da Klabin Celulose. 
Alterações no modo de operação desses comutadores são executadas com controle do COSR-S. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação : caracterizado quando não há tensão em todos os terminais 
de suas linhas de transmissão. 
• Desligamento parcial da Instalação : qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, os Agentes Operadores da Instalação devem fornecer ao 
COSR-S as seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2., sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não -aplicabilidade da 
recomposição fluente ou interrompendo a autonomia dos Agentes Operadores  da Instalação na 
recomposição, deve ser utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE 
No caso de desligamento total, os Agentes Operadores devem configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• das linhas de transmissão:  
LT 230 kV Castro Norte / Klabin Celulose; 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Klabin 
Celulose IO-OI.S.KCL 13 3.7.5.4. 25/09/2024 
 
Referência: 6 / 9 
 
LT 230 kV Figueira / Klabin Celulose; 
LT 230 kV Klabin Celulose / Mauá; 
LT 230 kV Klabin Celulose / Klabin Monte Alegre. 
• dos transformadores: 
TF-1 (lado de 230 kV); 
TF-2 (lado de 230 kV). 
Fechar ou manter fechado o disjuntor: 
• do módulo de interligação de barras de 230 kV, exceto quando esse estiver substituindo o disjuntor de 
um equipamento ou de uma linha de transmissão. 
Cabe ao Agente Copel GeT informar ao COSR -S quando a configuração de preparação da Instalação não 
estiver atendida para o início da recomposição, independentemente de o equipamento ser próprio ou de 
outros agentes. Nesse caso, o COSR -S fará contato com os agentes envolvidos, para identificar o motivo do 
não-atendimento e, após confirmação do Agente Copel GeT de que os barramentos estão com a configuração 
atendida, o COSR -S coordenará os procedimentos para recomposição, caso necessário, em função da 
configuração desta Instalação. 
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO 
A Instalação faz parte da recomposição da Área Gov. Ney Aminthas de Barros Braga. O Agentes Operador 
deve adotar os procedimentos a seguir para a recomposição fluente: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 Copel GeT 
Receber tensão da SE Castro Norte , pela LT 
230 kV Castro Norte / Klabin Celulose e ligar, 
energizando o barramento de 230 kV. 
 
1.1 Copel GeT Energizar a LT 230 kV Figueira / Klabin Celulose, 
enviando tensão para a SE Figueira. 
VKCL-230 ≤ 230 kV. 
O restabelecimento de carga na Klabin Monte Alegre e na Klabin Celulose está condicionado à autorização do 
COSR-S. 
Os demais equipamentos desta Instalação são restabelecidos sob controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
Os Agentes Operadores da Instalação devem realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
Os Agentes Operadores da Instalação devem realizar os procedimentos do Subitem 5.2.2 ., enquanto não 
houver intervenção do COSR -S no processo de recomposição ou interrupção da autonomia desses na 
recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras.  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Klabin 
Celulose IO-OI.S.KCL 13 3.7.5.4. 25/09/2024 
 
Referência: 7 / 9 
 
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, os 
Agentes Operadores devem preparar a Instalação conforme Subitem 5.2 .1., sem necessidade de 
autorização do ONS. 
5.4.1.2. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão nas barras de 230 kV, os Agentes Operadores devem preparar o setor de 230 
kV d a Instalação (disjuntores das linhas de transmissão e equipamentos) conforme Subitem 
5.2.1., sem necessidade de autorização do ONS.  
5.4.1.3. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 5.2 .2., sem necessidade de 
autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., os Agentes 
Operadores devem informar ao COSR-S, para que a recomposição da Instalação seja executada com 
controle do COSR-S. 
5.4.2.3. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.3., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 6.2.2., sem necessidade de 
autorização do ONS. 
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
os Agentes Operadores devem informar ao COSR -S, para que a recomposição da Instalação seja 
executada com controle do COSR-S. 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmi ssão ou de equipamentos, após  
desligamento programado, de urgência ou de emergência, só podem ser efetuados com controle do 
COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelos Agentes Operadores  da Instalação 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Klabin 
Celulose IO-OI.S.KCL 13 3.7.5.4. 25/09/2024 
 
Referência: 8 / 9 
 
quando explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução de 
Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, os Agentes Operadores da Instalação devem verificar se 
existe tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pelos Agentes Operadores  da 
Instalação quando estiver especificado nesta Instrução de O peração e estiverem atendidas as 
condições do Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou, quando permitida, em sentido inverso, pode ser feita com 
autonomia pela operação do Agente, conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica. 
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o operador da Instalação deve fechá -lo em anel desde que tenha autonomia  para tal, 
adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S. 
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S. 
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE EQUIPAMENTOS 
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação, é sempre 
controlada pelo COSR-S. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Klabin 
Celulose IO-OI.S.KCL 13 3.7.5.4. 25/09/2024 
 
Referência: 9 / 9 
 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelos Agentes Operadores da Instalação, 
após desligamento automático de equipamentos ou de linhas de transmissão. 
Os Agentes Operadores da Instalação devem identificar os desligamentos automáticos observando 
na Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado 
nas condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
LT 230 kV Castro Norte / 
Klabin Celulose 
Sentido Normal: SE Klabin Celulose recebe tensão da SE Castro Norte 
Ligar, em anel, a LT 230 kV Castro Norte 
/ Klabin Celulose. 
• VKCL-230 ≤ 238 kV. 
Sentido Inverso: SE Klabin Celulose envia tensão para a SE Castro Norte 
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2PR. 
 
LT 230 kV Figueira / Klabin 
Celulose 
Sentido Normal: SE Klabin Celulose envia tensão para a SE Figueira 
Energizar a LT 230  kV Figueira / Klabin 
Celulose. 
Sistema completo (de LT e TR) ou N-
1 (de LT ou TR) na SE Klabin Celulose 
230 kV 
• VKCL-230 ≤ 230 kV. 
Sentido Inverso: SE Klabin Celulose recebe tensão da SE Figueira 
Ligar, em anel, a LT 230  kV Figueira / 
Klabin Celulose. 
• ∆δ ≤ 23°. 
LT 230 kV Klabin Celulose / 
Mauá 
Sentido Normal: SE Klabin Celulose recebe tensão da SE Mauá 
Ligar, em anel, a LT 230  kV Klabin 
Celulose / Mauá. 
• VKCL-230 ≤ 230 kV; e 
• ∆δ ≤ 21°. 
Sentido Inverso: SE Klabin Celulose envia tensão para a SE Mauá 
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2PR. 
7. NOTAS IMPORTANTES 
7.1. As tratativas de operação entre o ONS e o Consumidor Livre Klabin Monte Alegre são feitas pela Copel 
GeT. 
