 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.11 
 
Cadastro de Informações Operacionais. 
Cadastro de Informações Operacionais de Faixas para Controle de Tensão da Área 500 KV 
da Região Sudeste 
 
Código Revisão Item Vigência 
CD-CT.SE.5SE.03 18 2.6.2 20/07/2023 
 
 
MOTIVO DA REVISÃO 
Alteração na denominação de UHE Emborcação 500 kV para SE Emborcação 500 kV. 
 
 
LISTA DE DISTRIBUIÇÃO 
ARARAQUARA CEMIG CNOS COPEL GeT COSR-SE 
COTESA CTEEP EDP BRASIL ETIM FURNAS 
FURNAS.CTRM.O FURNAS.CTRR.O FURNAS.CTRS.O LTT PCTE 
RPTE TAESA ETEO TMT   
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 500 KV da Região Sudeste CD-CT.SE.5SE.03 18 2.6.2 20/07/2023 
 
Referência: RT-CD.BR.03 rev. 05  2 / 5 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONCEITOS ................................ ................................ ................................ ................................ ... 3 
3. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO ................................ .......................  4 
4.1. Faixas de tensão da Rede de Operação ......................................................................................... 4 
4.2. Demais Faixas de tensão da Rede de Operação ............................................................................ 5 
 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 500 KV da Região Sudeste CD-CT.SE.5SE.03 18 2.6.2 20/07/2023 
 
Referência: RT-CD.BR.03 rev. 05  3 / 5 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
1. OBJETIVO 
Apresentar as faixas de controle de  tensão dos barramentos da Rede de Operação e dos secundários das 
transformações da Rede de Operação, cujos barramentos não pertencem à Rede de Operação, a serem 
controlados pelos operadores dos Centros de Operação do ONS e pela Operação dos agentes envolvidos. 
2. CONCEITOS 
2.1. As faixas de controle de tensão são estabelecidas em função de uma grandeza de referência, definidas 
conforme a seguir: 
Nome da 
Grandeza  Sigla  Definição  Sentido Positivo  Medido em  
Carga SIN, RSUL e 
Elos CC ---  
Requisito para definir a faixa de tensão na SE Araraquara  2, com base na 
carga total do Sistema interligado Nacional , condições de RSUL e fluxo nos 
Bipolos. 
Carga Global do 
SIN sem a MMGD 
Carga SIN 
sMMGD Carga SIN sMMGD = Carga SIN – MMGD SIN , conforme IO-ON.SSE 
Carga São Paulo 
sem a MMGD 
Carga São 
Paulo 
sMMGD 
Carga de São Paulo sem a MMGD, conforme IO-ON.SE.3RG. 
  
3. CONSIDERAÇÕES GERAIS 
3.1. Este Cadastro de Informações Operacionais apresenta os valores das faixas para controle de tensão de 
barramentos da Rede de Operação , de secundários das transformações da Rede de Operação cujas 
barras não pertencem à Rede de Operação. 
3.2. Os Centros de Operação do ONS, nas ações de coordenação, supervisão e controle devem observar os 
limites operacionais constantes neste Cadastro de Informações Operacionais. 
3.3. A operação dos agentes, em suas ações de comando e execução, deve observar os limites operacionais 
constantes neste Cadastro de Informações Operacionais. 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 500 KV da Região Sudeste CD-CT.SE.5SE.03 18 2.6.2 20/07/2023 
 
Referência: RT-CD.BR.03 rev. 05  4 / 5 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO 
4.1. FAIXAS DE TENSÃO DA REDE DE OPERAÇÃO 
Subestação / Usina Faixas de Tensão  (kV) em função do(a) Carga SIN 
sMMGD e RSUL e ELOS CC 
Nome Tensão 
(kV) 
Carga SIN 
sMMGD > 
63GW 
(Pesada) 
Carga SIN 
sMMGD > 
63GW 
(Média) 
Carga SIN 
sMMGD < 
63GW 
(Leve) 
Carga SIN 
sMMGD < 
50GW 
(Mínima) 
SE Araraquara 2 500 530 - 550 520 - 550 (1) 520 - 550 500 - 550 (2) 
Notas: 
(1) - Deve ser atendido também a condição: RSUL < 6.500 MW e FBTA < 2.500 MW e Elo Madeira < 4.800 
MW. Quando atendido esta condição e Carga SIN sMMGD > 63GW, deve ser considerada esta faixa. 
(2) - Deve ser atendido também a condição: RSUL < 7.500 MW e Soma Bipolos < 10.000 MW e Elo Madeira < 
2.000 MW. Quando atendido esta condição e Carga SIN sMMGD < 50GW, deve ser considerada esta faixa. 
Soma Bipolos =  (Elo Foz /Ibiúna   Elo Madeira   Elo Xingu/Estreito   Elo Xingu/Terminal Rio), medido nos 
terminas retificadores. 
 
Subestação / Usina Faixas de Tensão  (kV) em função do(a) Carga São 
Paulo sMMGD 
Nome Tensão 
(kV) 
Requisito > 
15500 MW 
(Pesada) 
Requisito > 
15500 MW 
(Média) 
Requisito < 
15500 MW 
(Leve) 
Requisito 
< 15500 
MW 
(Mínima) 
SE Assis 525 515 - 550 515 - 550 515 - 550 515 - 550 
SE Emborcação 500 520 - 550 520 - 550 490 - 550 490 - 550 
SE Marimbondo II 500 525 - 550 525 - 550 500 - 550 500 - 550 
SE São Simão 500 525 - 550 525 - 550 490 - 550 490 - 550 
UHE Água Vermelha 500 525 - 550 525 - 550 500 - 550 500 - 550 
UHE Marimbondo 500 525 - 550 525 - 550 500 - 550 500 - 550 
 
Subestação / Usina Faixas de Tensão  (kV) em função do(a) Carga SIN 
sMMGD 
Nome Tensão 
(kV) 
>=63.000 
(Pesada) 
>=63.000 
(Média) 
<63.000 
(Leve) 
<63.000 
(Mínima) 
SE Estreito 500 530 - 550 530 - 550 520 - 550 520 - 550 
 
 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 500 KV da Região Sudeste CD-CT.SE.5SE.03 18 2.6.2 20/07/2023 
 
Referência: RT-CD.BR.03 rev. 05  5 / 5 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
 
4.2. DEMAIS FAIXAS DE TENSÃO DA REDE DE OPERAÇÃO 
Os barramentos da Rede de Operação e os secundários das transformações da Rede de Operação, que não 
possuem faixas de tensão definidas nos itens anteriores, devem operar com tensões nas seguintes faixas 
operativas, conforme estabelecido nos Procedimentos de Rede e reproduzido na tabela a seguir: 
Tensão Nominal (kV) Faixa de Tensão (kV) 
765 690 a 800 
525 500 a 550  
500 500 a 550  
440 418 a 460  
345 328 a 362  
230 218 a 242  
138 131 a 145  
88 83,6 a 92,4 
69 65,6 a 72,4 
34,5 32,8 a 36,2 
23 21,8 a 24,2 
13,8 13,1 a 14,5 
 
