Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação
Procedimentos Sistêmicos para a Operação da UHE Castro Alves
Código Revisão Item Vigência
IO-OI.S.UHCA 12 3.7.5.2. 06/11/2023
.
MOTIVO DA REVISÃO
- Melhoria textual na tabela do item 6.2.2.2.
LISTA DE DISTRIBUIÇÃO
CNOS COSR-S CERAN
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE 
Castro Alves IO-OI.S.UHCA 12 3.7.5.2. 06/11/2023
Referência: 2 / 9
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. CONSIDERAÇÕES GERAIS .................................................................................................................3
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO..............................................................4
3.1. Unidades Geradoras......................................................................................................................4
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL......................................................4
4.1. Procedimentos Gerais ...................................................................................................................4
4.2. Procedimentos Específicos............................................................................................................5
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................................................5
5.1. Procedimentos Gerais ...................................................................................................................5
5.2. Procedimentos para Recomposição Fluente.................................................................................5
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................5
5.2.2. Recomposição Fluente da Instalação ..........................................................................6
5.3. Procedimentos após Desligamento Total da Instalação................................................................6
5.3.1. Preparação da Instalação após Desligamento Total....................................................6
5.3.2. Recomposição após Desligamento Total da Instalação...............................................6
5.4. Procedimentos após Desligamento Parcial da Instalação .............................................................7
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ..............7
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................7
6. MANOBRAS DE UNIDADES GERADORAS, DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS.........7
6.1. Procedimentos Gerais ...................................................................................................................7
6.2. Procedimentos Específicos............................................................................................................8
6.2.1. Desligamento de Unidades Geradoras e Desenergização de Linhas de Transmissão e 
de Equipamentos.........................................................................................................8
6.2.2. Sincronismo de Unidades Geradoras e Energização de Linhas de Transmissão e de 
Equipamentos..............................................................................................................8
7. NOTAS IMPORTANTES .....................................................................................................................9
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE 
Castro Alves IO-OI.S.UHCA 12 3.7.5.2. 06/11/2023
Referência: 3 / 9
1. OBJETIVO
Estabelecer os procedimentos para a operação da UHE Castro Alves, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede.
2. CONSIDERAÇÕES GERAIS
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da Instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementariedade 
das ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS.
2.2. A comunicação operacional entre o COSR-S e a Instalação, de propriedade da CERAN, é realizada pela 
UHE Monte Claro, agente responsável pela operação da Instalação, por intermédio da operação da 
UHE Monte Claro.
2.3. As unidades geradoras, equipamentos e linha de transmissão desta Instalação fazem parte da Área 
230 kV do Rio Grande do Sul.
2.4. O COSR-S controla e supervisiona o despacho de geração desta Usina.
2.5. Esta Usina:
é despachada centralizadamente;
está conectada na Rede de Operação;
não participa do Controle Automático da Geração – CAG;
é de autorrestabelecimento integral;
participa do processo de recomposição fluente da Área Foz do Chapecó.
2.6. Os dados operacionais desta Usina estão descritos no Cadastro de Informações Operacionais da 
respectiva Área Elétrica.
2.7. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos:
2.7.1. A definição da quantidade de tentativas de religamento manual de linha de transmissão ou 
equipamento, bem como o intervalo entre elas é de responsabilidade do Agente e devem ser 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica. 
2.7.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, este deve definir a necessidade de tentativas adicionais e solicitar ao COSR-S autorização 
para religamento. Nessa oportunidade, o Agente também pode solicitar alteração no sentido normal 
para envio de tensão, caso não tenha autonomia para tal.
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR-S levará em consideração as condições operativas do 
sistema. 
2.8. Quando caracterizado impedimento de linha de transmissão ou equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação em contingência da respectiva área elétrica.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE 
Castro Alves IO-OI.S.UHCA 12 3.7.5.2. 06/11/2023
Referência: 4 / 9
2.9. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica.
2.10. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR-S, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação, quando o Agente tiver autonomia para energizar a 
linha de transmissão ou o equipamento.
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO
3.1. UNIDADES GERADORAS
As unidades geradoras UG 1, UG 2 e UG 3 13,8 kV estão conectadas ao barramento de 230 kV da SE Monte 
Claro, por meio da LT 230 kV Monte Claro / Usina Hidrelétrica Castro Alves e do transformador TE 1 230/13,8 
kV, que se encontra rigidamente conectado à barra de 13,8 kV da UHE Castro Alves, com os disjuntores de 
13,8 kV fechados e todas as seccionadoras de 13,8 kV fechadas.
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL
4.1. PROCEDIMENTOS GERAIS
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S. 
As faixas de controle de tensão para esse barramento estão estabelecidas no Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica.
4.1.2. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob responsabilidade da Instalação.
4.1.3. A Usina deve manter os valores de geração de acordo com os valores programados, constantes no 
Programa Diário de Operação – PDO. Para atendimento dos valores programados constantes no PDO, 
não é necessária a autorização prévia do COSR-S.
Qualquer alteração no valor de geração da usina em relação ao valor de geração constante no PDO 
somente pode ser executada após autorização do COSR-S.
As reprogramações de geração quando de necessidades sistêmicas serão executadas pela Usina 
quando solicitadas pelo COSR-S.
Após reprogramação de geração solicitada pelo COSR-S, o Agente somente poderá alterar a geração 
da Usina com autorização do ONS, inclusive para adoção de valores contidos no PDO.
4.1.4. Os desvios de geração da usina em relação aos valores previstos no PDO ou em relação às 
reprogramações, devem ser controlados observando-se os valores máximos permitidos explicitados 
na Instrução de Operação IO-CG.BR.01 – Controle da Geração em Condição Normal.
4.1.5. Quando não existir ou não estiver disponível a supervisão da usina para o ONS, essa deve seguir as 
orientações para envio de dados conforme Rotina Operacional RO-AO.BR.08 – Apuração de Dados de 
Despacho de Geração e de Intercâmbios. A Usina, deve registrar e informar imediatamente os 
seguintes dados ao COSR-S:
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE 
Castro Alves IO-OI.S.UHCA 12 3.7.5.2. 06/11/2023
Referência: 5 / 9
movimentação de unidades geradoras hidráulicas (mudança de estado operativo).
restrições e ocorrências na usina ou na conexão elétrica que afetem a disponibilidade de 
geração, com o respectivo valor da restrição, contendo o horário de início e término e a 
descrição do evento.
demais informações sobre a operação de suas Instalações, solicitadas pelo ONS.
4.1.6. O controle de tensão por meio da geração ou absorção de potência reativa pelas unidades geradoras 
da Usina, é controlado pelo COSR-S, com comando e execução pelo Agente Operador da Instalação.
4.2. PROCEDIMENTOS ESPECÍFICOS
Não se aplica.
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO
5.1. PROCEDIMENTOS GERAIS
5.1.1. Quando de um desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir:
Desligamento total da Instalação: caracterizado quando não há tensão em todos os terminais 
de suas linhas de transmissão.
Desligamento parcial da Instalação: qualquer outra configuração que não se enquadre como 
desligamento total.
5.1.2. Quando de um desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao 
COSR-S as informações a seguir:
horário da ocorrência;
configuração da instalação logo após a ocorrência;
configuração da instalação após ações realizadas com autonomia pela sua operação.
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2, sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia do Agente Operador da Instalação na 
recomposição, deve ser utilizado o Subitem 5.3.
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE
No caso de desligamento total, o Agente Operador deve configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir:
Abrir ou manter abertos os disjuntores:
das linhas de transmissão: 
LT 230 kV Monte Claro / Usina Hidrelétrica Castro Alves
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE 
Castro Alves IO-OI.S.UHCA 12 3.7.5.2. 06/11/2023
Referência: 6 / 9
das unidades geradoras:
UG 1 13,8 kV (de 13,8 kV);
UG 2 13,8 kV (de 13,8 kV);
UG 3 13,8 kV (de 13,8 kV).
Cabe ao Agente CERAN informar ao COSR-S quando a configuração de preparação da Instalação não estiver 
atendida para o início da recomposição, independentemente de o equipamento ser próprio ou de outros 
agentes. Nesse caso, o COSR-S fará contato com os agentes envolvidos, para identificar o motivo do não-
atendimento e, após confirmação do Agente CERAN de que os barramentos estão com a configuração 
atendida, o COSR-S coordenará os procedimentos para recomposição, caso necessário, em função da 
configuração desta Instalação.
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO
A Instalação faz parte da recomposição da Área Foz do Chapecó. O Agente Operador deve adotar os 
procedimentos a seguir para recomposição fluente: 
Passo Executor Procedimentos Condições ou Limites Associados
1 CERAN
Receber tensão da SE Monte Claro, pela LT 
230 kV Monte Claro / Usina Hidrelétrica 
Castro Alves e pelo transformador TE 1 
230/13,8 kV, energizando a barra de 
13,8 kV.
1.1 CERAN
Partir uma das unidades geradoras, caso 
essa estivesse operando antes do 
desligamento, e sincronizá-la.
Manter VUHCA-13,8 = 13,1 kV.
Elevar a geração da UHE Castro 
Alves para 10 MW.
Conforme procedimentos internos 
do Agente
1.2 CERAN
Partir a segunda unidade geradora, caso 
essa estivesse operando antes do 
desligamento, e sincronizá-la.
Manter VUHCA-13,8 = 13,1 kV.
Conforme procedimentos internos 
do Agente
1.3 CERAN
Partir a terceira unidade geradora, caso 
essa estivesse operando antes do 
desligamento, e sincronizá-la.
Manter VUHCA-13,8 = 13,1 kV.
Conforme procedimentos internos 
do Agente
Os demais equipamentos desta Instalação são restabelecidos sob controle do COSR-S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1.
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2, enquanto não houver 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE 
Castro Alves IO-OI.S.UHCA 12 3.7.5.2. 06/11/2023
Referência: 7 / 9
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia destes na recomposição. 
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR-S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras.
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL
5.4.1.1. Caracterizado desligamento parcial da instalação que seja:
ausência de tensão em todos os barramentos,
ausência de fluxo de potência ativa nas linhas de transmissão, e
existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, o 
Agente Operador deve preparar a Instalação conforme Subitem 5.2.1, sem necessidade de 
autorização do ONS.
5.4.1.2. Para demais desligamentos parciais da Instalação, não há necessidade de preparação de Instalação.
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador 
da Instalação deve recompor a Instalação conforme Subitem 5.2.2, sem necessidade de autorização 
do ONS.
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., o Agente Operador 
da Instalação deve recompor a Instalação conforme Subitem 6.2.2, sem necessidade de autorização 
do ONS.
O sincronismo das unidades geradoras, bem como a elevação de geração nessas unidades, é 
realizado conforme as condições definidas no Subitem 6.2.2.
6. MANOBRAS DE UNIDADES GERADORAS, DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS
6.1. PROCEDIMENTOS GERAIS
6.1.1. Os procedimentos para desligamento programado ou de urgência de unidades geradoras, e 
desenergização programada ou de urgência de linhas de transmissão ou de equipamentos, só podem 
ser efetuados com controle do COSR-S.
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, ou sincronismo 
de unidades geradoras, após desligamentos programados, de urgência ou de emergência, só podem 
ser efetuados com controle do COSR-S.
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, ou para sincronismo de unidades geradoras, após desligamento automático sem 
atuação de proteção que impeça o retorno do equipamento, só podem ser executados com 
autonomia pelo Agente Operador da Instalação quando estiverem explicitados e estiverem atendidas 
as condições do Subitem 6.2.2. desta Instrução de Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR-S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE 
Castro Alves IO-OI.S.UHCA 12 3.7.5.2. 06/11/2023
Referência: 8 / 9
6.1.4. Antes do fechamento de qualquer disjuntor, o Agente Operador da Instalação deve verificar se existe 
tensão de retorno e se a condição de fechamento será em anel. O fechamento em anel só pode ser 
executado com autonomia pelo Agente Operador da Instalação quando estiver especificado nesta 
Instrução de Operação e estiverem atendidas as condições do Subitem 6.2.2. O fechamento de 
paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento:
6.1.5.1. A energização em sentido normal ou, quando permitida, em sentido inverso, pode ser feita com 
autonomia pela operação do Agente, conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica. 
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o Agente Operador da Instalação deve fechá-lo em anel desde que tenha autonomia para 
tal, adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra.
6.1.9. A partida de unidades geradoras deve seguir critérios próprios do Agente. O sincronismo à Rede de 
Operação só pode ser executado com autonomia pela operação da Usina quando estiver explicitado 
e estiverem atendidas as condições do Subitem 6.2.2. A tomada de carga deve ser realizada com 
controle do COSR-S.
6.2. PROCEDIMENTOS ESPECÍFICOS
6.2.1. DESLIGAMENTO DE UNIDADES GERADORAS E DESENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE 
EQUIPAMENTOS
O desligamento de unidades geradoras e a desenergização de linhas de transmissão ou de equipamentos, 
pertencentes à Rede de Operação, é sempre controlado pelo COSR-S.
6.2.2. SINCRONISMO DE UNIDADES GERADORAS E ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE 
EQUIPAMENTOS
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S.
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de unidades geradoras, de equipamentos ou de linhas de transmissão. 
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais unidades geradoras, linhas de transmissão e equipamentos em operação, 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE 
Castro Alves IO-OI.S.UHCA 12 3.7.5.2. 06/11/2023
Referência: 9 / 9
conforme explicitado nas condições de energização para a manobra.
Para os demais desligamentos parciais, proceder conforme Subitem 5.4.
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados
Sentido Único: UHE Castro Alves recebe tensão da SE Monte ClaroLT 230 kV Monte Claro / 
Usina Hidrelétrica 
Castro Alves e 
Transformador TE-1 
230/13,8 kV
Energizando o barramento de 13,8 kV.
Unidade Geradora
UG 1, UG2 ou UG 3
13,8 kV
Partir e sincronizar a unidade 
geradora.
13,11 ≤ V UHCA-13,8 ≤ 14,49 kV; e
elevar a geração da unidade 
geradora, após autorização do 
COSR-S.
7. NOTAS IMPORTANTES
Não se aplica.
