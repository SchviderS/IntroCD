Manual de Procedimentos da Operação
  Referência Técnica
Elaboração de Instruções de Gerenciamento da Carga
Código Revisão Item Vigência
RT-GC.BR 09 7.6. 22/07/2022
.
MOTIVO DA REVISÃO:
Alterados os subitens 2.1 e 3.1 em adequação à reestruturação dos Procedimentos de Rede;
Ajuste de texto e formatação ao longo do documento.
LISTA DE DISTRIBUIÇÃO:
CNOS COSR-SE COSR-NE COSR-NCO COSR-S
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Gerenciamento da 
Carga RT-GC.BR 09 7.6. 22/07/2022
Referência: 2/ 4
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. PREMISSAS ......................................................................................................................................3
3. ESTRUTURA DAS INSTRUÇÕES DE OPERAÇÃO ..................................................................................3
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Gerenciamento da 
Carga RT-GC.BR 09 7.6. 22/07/2022
Referência: 3/ 4
1. OBJETIVO
Estabelecer o documento de referência das Instruções de Operação para o gerenciamento da carga, 
visando a estruturação e padronização das mesmas.
2. PREMISSAS
2.1. Os procedimentos constantes das Instruções de Operação para Gerenciamento de Carga deverão estar 
de acordo com as responsabilidades, premissas, diretrizes e critérios gerais definidos nos submódulos 5.7 
- Gerenciamento da Carga e 5.1 - Operação do sistema e das Instalações da Rede de Operação, dos 
Procedimentos de Rede. 
2.2. O detalhamento de cada Instrução de Operação deve ser feito de forma a assegurar a maior aderência 
possível aos procedimentos operativos vigentes. 
2.3. Instruções de Operação devem ser simples e objetiva, evitando linguagem pesada, textos longos e 
sempre que possível terem o formato tabular.
2.4. A padronização que venha a ser adotada para as Instruções de Operação deve ser aplicável a toda Rede 
de Operação, definindo portanto, o padrão ONS para este processo. 
2.5. A principal prioridade a ser considerada na estruturação que deve ser dada às Instruções de 
Gerenciamento da Carga, é o atendimento das necessidades dos seus usuários. 
3. ESTRUTURA DAS INSTRUÇÕES DE OPERAÇÃO 
3.1. Em função das premissas, diretrizes e critérios estabelecidos no Submódulo 5.7 devem ser elaboradas as 
seguintes Instruções de Operação:
3.1.1. Gerenciamento da carga por atuação do Esquema Regional de Alívio de Carga – ERAC (IO para toda 
Rede de Operação).
3.1.2. Gerenciamento da carga para controle da frequência no Sistema (IO para toda Rede de Operação).
3.1.3. Gerenciamento da carga por corte manual face deficiência de transmissão ou transformação (IO’s 
específicas por área elétrica da Rede de Operação).
3.1.4. Gerenciamento da carga por redução do nível de tensão (IO’s específicas por área elétrica da Rede 
de Operação). 
3.2. Deve ser elaborada uma Rotina Operacional para o plano Manual de corte de carga - PCMC dos agentes, 
conforme submódulo 5.7. A estrutura desse documento obedecerá o documento de referência de 
elaboração de Rotinas Operacionais.
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Gerenciamento da 
Carga RT-GC.BR 09 7.6. 22/07/2022
Referência: 4/ 4
3.3. As Instruções de Operação associadas ao processo Gerenciamento de Carga devem conter basicamente, 
os seguintes itens com a respectiva numeração:
a) Folha rosto com o motivo da revisão, lista de distribuição e índice conforme referência técnica 
RT-MP.BR.02.
b) Item 1 – Objetivo
Descrever o objetivo: Ex.: Estabelecer os procedimentos operacionais para Centros de Operação do 
ONS e Agentes, para o religamento das cargas desligadas pelo ERAC.
c) Item 2 – Conceitos
Neste item devem estar relacionados os conceitos importantes para o esclarecimento dos 
procedimentos operacionais contidos nesta Instrução de Operação, evitando-se a definição de termos 
já contidos no Glossário dos Procedimentos de Rede. Ex.: Conceituar esquema do ERAC, subfrequencia 
sustentada, etc.
d) Item 3 – Considerações gerais
Neste item devem estar contidas as considerações comuns e orientações de caráter geral e importante 
para o operador, usuário da IO. Em princípio, este item deve ser comum a todas as IO’s, podendo ser 
complementado, em função de particularidades do processo.
e) Item 4 - Procedimentos
Neste item relacionar os procedimentos gerais e/ou específicos conforme segue:
Na IO para Gerenciamento da carga por atuação do Esquema Regional de Alívio de Carga – ERAC, 
indicar os procedimentos para o restabelecimanto de circuitos e o detalhamento lógico e 
operacional dos ERAC. 
Na IO para Gerenciamento da carga para controle da frequência no Sistema, indicar o corte de carga 
necessário por agente quando de subfrequencia sustentada, o corte de carga quando a atuação do 
ERAC for insuficiente para restabelecer a frequência e os procedimentos prévios de alerta aos 
agentes em função das condições do sistema.
Nas IO para Gerenciamento da carga por corte manual face deficiência de transmissão ou 
transformação, o gerenciamento de carga quando de previsão de déficit de geração, transmissão 
ou transformação; por área elétrica do sistema
Nas IO para Gerenciamento da carga por redução do nível de tensão, indicar a metodologia aplicada, 
os níveis de tensão mínimos admitidos e os pontos de aplicação dos procedimentos; por área 
elétrica do sistema
NOTA: Recomenda-se a elaboração de quantas IO’s forem necessárias para clareza na execução dos 
procedimentos.
f) Item 5 – Anexos
Incluir neste item tabelas e/ou gráficos necessários para melhor compreensão dos procedimentos.
