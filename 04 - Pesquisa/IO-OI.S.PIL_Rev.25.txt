 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Pilarzinho 
 
 
Código Revisão Item Vigência 
IO-OI.S.PIL 25 3.7.5.4. 25/09/2024 
 
. 
MOTIVO DA REVISÃO 
- Alteração das condições de autonomia do Agente para energização, desvinculando os procedimentos do 
Subitem 6.2.2 dos procedimentos de recomposição fluente da Instalação constantes no Subitem 5.2.2, 
passando-se a adotar, no Subitem 6.2.2, condições de sistema completo e/ou sistema N -1 para energização 
das seguintes linhas de transmissão e equipamentos: 
• Transformador TF-A ou TF-B 230/69/13,8 kV. 
- Alteração das condições de autonomia do Agente para fechamento em anel, no Subitem 6.2.2, das seguintes 
linhas de transmissão e equipamentos: 
• LT 230 kV Pilarzinho / Santa Mônica; 
• Transformador TF-A ou TF-B 230/69/13,8 kV. 
- Inclusão dos Subitens 5.4.1.2. e 5.4.2.2. e complementação do Subitem 5.4.2.3. para casos de desligamentos 
parciais. 
- Adequação à RT-OI.BR revisão 37, com destaque à inclusão dos Subitens 6.1.8 e 6.1.9.  
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S Copel GeT 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Pilarzinho IO-OI.S.PIL 25 3.7.5.4. 25/09/2024 
 
Referência: 2 / 10 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  3 
3.1. Barramento de 230 kV ................................................................................................................... 3 
3.2. Alteração da Configuração do Barramento ................................................................................... 4 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 4 
4.1. Procedimentos Gerais ................................................................................................................... 4 
4.2. Procedimentos Específicos ............................................................................................................ 4 
4.2.1. Operação dos Comutadores de Tape Sob Carga (LTC) ................................................ 4 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  4 
5.1. Procedimentos Gerais ................................................................................................................... 4 
5.2. Procedimentos para Recomposição Fluente ................................................................................. 5 
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................ 5 
5.2.2. Recomposição Fluente da Instalação .......................................................................... 5 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 7 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 7 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 7 
5.4. Procedimentos Após Desligamento Parcial da Instalação ............................................................ 7 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 7 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 7 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 8 
6.1. Procedimentos Gerais ................................................................................................................... 8 
6.2. Procedimentos Específicos ............................................................................................................ 9 
6.2.1. Desenergização de Linhas de Transmissão e de Equipamentos ................................. 9 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ....................................... 9 
7. NOTAS IMPORTANTES ................................ ................................ ................................ ................ 10 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Pilarzinho IO-OI.S.PIL 25 3.7.5.4. 25/09/2024 
 
Referência: 3 / 10 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE Pilarzinho, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, realizados 
com autonomia pelo Agente Operador  da instalação, devendo fazer parte do manual de operação 
próprio elaborado pelo Agente quando existente, observando -se a complementaridade das ações que 
devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR-S e a Instalação, de propriedade da Copel GeT, é realizada pela 
Copel GeT, agente responsável pela operação da Instalação, por intermédio do COGT Copel.  
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte da Área 230 kV do Paraná. 
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.4.1. A definição da quantidade  de tentativas de religamento manual de linhas de transmissão ou de 
equipamentos, bem como o intervalo entre elas, é de responsabilidade do Agente e devem estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica.  
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR-S autorização para 
religamento. Nessa oportunidade, o Agente também pode solicitar alteração no sentido normal para 
envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização , além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em consideração as condições operativas do 
sistema.  
2.5. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação em contingência da respectiva área elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos no 
Subitem 6.2.2. desta Instrução de Operação, quando o Agente tiver autonomia para energizar a linha de 
transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 230 KV 
A configuração do barramento de 230 kV é do tipo P rincipal (seccionada em Barra BP-A e Barra BP-B, 
interligadas por seccionadora) e Transferência (Barra BT). Na operação normal desse barramento, todos 
os disjuntores e seccionadoras devem estar fechados,  exceto as seccionadoras de transferência das 
linhas de transmissão e dos equipamentos , bem como, a barra de transferência deve estar 
desenergizada, com o disjuntor de interligação de barras aberto e as chaves seccionadoras do módulo 
de interligação fechadas. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Pilarzinho IO-OI.S.PIL 25 3.7.5.4. 25/09/2024 
 
Referência: 4 / 10 
 
3.2. ALTERAÇÃO DA CONFIGURAÇÃO DO BARRAMENTO 
A mudança de configuração do barramento de 230 kV desta Instalação  é executada com controle do 
COSR-S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade do Agente 
Operador da Instalação. 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S.  
As faixas de controle de tensão para esse barramento estão estabelecidas no Cadastro de Informações 
Operacionais das Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. O barramento de 69 kV , não pertencente à Rede de Operação, onde se conecta a transformação 
230/69/13,8 kV, tem a sua regulação de tensão executada com autonomia pelo Agent Operador  da 
Instalação por meio da utilização de recursos locais disponíveis de autonomia dessa. 
Esgotados esses recursos o Agente deve acionar o COSR -S que deve verificar a disponibilidade dos 
recursos sistêmicos. 
As faixas para controle de tensão para esse barramento estão estabelecidas no Cadastro de 
Informações Operacionais das Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.3. Os demais barramentos , não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob responsabilidade da Instalação. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
4.2.1. OPERAÇÃO DOS COMUTADORES DE TAPE SOB CARGA (LTC) 
Os LTCs dos transformadores TF-A e TF-B 230/69/13,8 kV operam em modo automático. 
Alterações no modo de operação desses comutadores são executadas com autonomia pelo Agente 
Operador do Agente Copel GeT. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de um desligamento da instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir, para atendimento às necessidades sistêmicas: 
• Desligamento total da Instalação: caracterizado quando não há tensão em todos os terminais de 
suas linhas de transmissão. 
•  Desligamento parcial da Instalação: qualquer outra configuração que não se enquadre como 
desligamento total. 
5.1.2. Quando de um desligamento total ou parcial, o Agente Operador da Instalação  deve fornecer ao 
COSR-S as informações a seguir: 
• horário da ocorrência; 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Pilarzinho IO-OI.S.PIL 25 3.7.5.4. 25/09/2024 
 
Referência: 5 / 10 
 
• configuração da subestação logo após a ocorrência; 
• configuração da instalação após ações realizadas com autonomia pelo Agente Operador dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2., sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia d o Agente Operador da Instalação  na 
recomposição, deve ser utilizado o Subitem 5.3.  
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE 
No caso de desligamento total, o Agente Operador deve c onfigurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• das linhas de transmissão:  
LT 230 kV Pilarzinho / Santa Mônica; 
LT 230 kV Campo Comprido / Pilarzinho; 
LT 230 kV Bateias / Pilarzinho; 
LT 230 kV Curitiba Norte / Pilarzinho. 
• dos transformadores:  
TF-A 230/69/13,8 kV (lados de 230 kV e de 69 kV); 
TF-B 230/69/13,8 kV (lados de 230 kV e de 69 kV). 
• de todas as linhas de transmissão de 69 kV. 
• de todos os bancos de capacitores de 69 e 13,8 kV. 
Desligar ou manter desligado o modo de comutação automática dos comutadores sob carga da 
transformação 230/69/13,8 kV da SE Pilarzinho. 
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO  
A Instalação faz parte da recomposição da Área Gov. Pedro Viriato Parigot de Souza . O Agente 
Operador deve adotar os procedimentos a seguir para recomposição fluente: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 Copel GeT 
Receber tensão da SE Santa Mônica, 
pela LT 230 kV Pilarzinho / Santa 
Mônica, e energiza r o barramento 
de 230 kV. 
 
1.1 Copel GeT 
Energizar, pelo lado de 230 kV,  um 
dos transformadores 
230/69/13,8 kV da SE Pilarzinho e  
ligar, energizando o barramento de 
69 kV. 
TAPPIL-230/69 = 9. 
 
Possibilitar o restabelecimento de 
carga prioritária na SE Pilarzinho  e 
nas subestações atendidas pela SE 
Pilarzinho. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Pilarzinho IO-OI.S.PIL 25 3.7.5.4. 25/09/2024 
 
Referência: 6 / 10 
 
Passo Executor Procedimentos Condições ou Limites Associados 
1.1.1 Copel DIS 
Restabelecer carga prioritária da SE 
Pilarzinho e nas subestações 
atendidas pela SE Pilarzinho. 
No máximo 40 MW. 
 
Respeitar o limite mínimo de 
tensão de 62 kV no barramento de 
69 kV. 
1.1.2 
Caso não seja possível recompor parte ou totalmente a carga de 40 MW na SE Pilarzinho, a 
operação dessa subestação deve solicitar à operação da SE Santa Mônica completar o 
restabelecimento da carga até o limite total de 40 MW considerando as duas subestações. 
1.2 Copel GeT 
Energizar, pelo lado de 230 kV,  o 
segundo transformador 
230/69/13,8 kV da SE Pilarzinho e 
ligar, interligando esse 
transformador com o outro 
transformador 230/69/13,8 kV da SE 
Pilarzinho, pelo lado de 69 kV. 
TAPPIL-230/69 = 9.  
 
Após fluxo de potência ativa  no 
transformador 230/69/13,8 kV da 
SE Pilarzinho energizado. 
 
Possibilitar o restabelecimento de 
carga prioritária na SE Pilarzinho  e 
nas subestações atendidas pela SE 
Pilarzinho. 
1.2.1 Copel DIS 
Restabelecer carga prioritária  da SE 
Pilarzinho e nas subestações 
atendidas pela SE Pilarzinho. 
Após 1 (um) minuto do 
restabelecimento de carga no 
transformador 230/69/13,8 kV da 
SE Pilarzinho energizado, 
restabelecer mais no máximo 40 
MW, totalizando 80 MW. 
 
Respeitar o limite mínimo de 
tensão de 62 kV no barramento de 
69 kV. 
1.2.2 
Caso não seja possível recompor parte ou totalmente a carga de mais 40 MW na SE Pilarzinho, 
a operação dessa subestação deve solicitar à operação da SE Santa Mônica completar o 
restabelecimento da carga até o limite total de 80 MW considerando as duas subestações. 
2 Copel GeT 
Receber tensão da SE Bateias, pela 
LT 230 kV Bateias / Pilarzinho e ligar, 
em anel. 
∆δ ≤ 30°. 
3 Copel GeT 
Receber tensão da SE Campo 
Comprido, pela LT 230  kV Campo 
Comprido / Pilarzinho e ligar, em 
anel. 
 
4 Copel GeT 
Receber tensão da SE Curitiba Norte, 
pela LT 230  kV Curitiba Norte  / 
Pilarzinho e ligar, em anel. 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Pilarzinho IO-OI.S.PIL 25 3.7.5.4. 25/09/2024 
 
Referência: 7 / 10 
 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2 ., enquanto não 
houver intervenção do COSR -S no processo de recomposição ou interrupção da autonomia des se na 
recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR-S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras.  
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, 
o Agente Operador da Instalação deve preparar a Instalação conforme Subitem 5.2.1., sem 
necessidade de autorização do ONS. 
5.4.1.2. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão nas barras de 230 kV, os Agentes Operadores devem preparar o setor de 230 
kV d a Instalação (disjuntores das linhas de transmissão e equipamentos) conforme Subitem 
5.2.1., sem necessidade de autorização do ONS.  
5.4.1.3. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador 
deve recompor a Instalação conforme Subitem 5.2.2., sem necessidade de autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., os Agentes 
Operadores devem informar ao COSR-S, para que a recomposição da Instalação seja executada com 
controle do COSR-S. 
5.4.2.3. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.3., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 6.2.2., sem necessidade de 
autorização do ONS. 
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
os Agentes Operadores devem informar ao COSR -S, para que a recomposição da Instalação seja 
executada com controle do COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Pilarzinho IO-OI.S.PIL 25 3.7.5.4. 25/09/2024 
 
Referência: 8 / 10 
 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, após 
desligamentos programados, de urgência ou de emergência, só podem ser efetuados com controle 
do COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelo Agente Operador da Instalação quando 
estiverem explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução de 
Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir autonomia, 
a energização deve ser executada com controle do COSR -S, conforme Instrução de Operação de 
Preparação para Manobras da respectiva área elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, o Agente Operador da Instalação deve verificar se existe 
tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pelo Agente Operador da Instalação 
quando estiver especificado nesta Instrução de Operação e estiverem atendidas as condições do 
Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou, quando permitida, em sentido inverso, pode ser feita com 
autonomia pelo Agente Operador , conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica.  
 A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, as 
condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o Agente Operador da Instalação deve fechá-lo em anel desde que tenha autonomia para 
tal, adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Pilarzinho IO-OI.S.PIL 25 3.7.5.4. 25/09/2024 
 
Referência: 9 / 10 
 
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S. 
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS  
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação 
é sempre controlada pelo COSR-S. 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático, de equipamentos ou de linhas de transmissão. 
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais, linhas de transmissão e equipamentos em operação, conforme explicitado 
nas condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Equipamento / Linha 
de Transmissão Procedimentos Condições ou Limites Associados 
LT 230 kV Bateias / 
Pilarzinho 
Sentido Normal: SE Pilarzinho recebe tensão da SE Bateias 
Ligar, em anel, a LT 230  kV Bateias / 
Pilarzinho. 
• ∆δ ≤ 30°. 
Sentido Inverso: SE Pilarzinho envia tensão para SE Bateias 
A energização em sentido inverso é controlada pelo COSR-S, conforme  
IO-PM.S.2PR. 
LT 230 kV Campo 
Comprido / 
Pilarzinho 
Sentido Normal: SE Pilarzinho recebe tensão da SE Campo Comprido 
Ligar, em anel, a LT 230  kV Campo 
Comprido / Pilarzinho. 
 
Sentido Inverso: SE Pilarzinho envia tensão para SE Campo Comprido 
A energização em sentido inverso é controlada pelo COSR-S, conforme  
IO-PM.S.2PR. 
LT 230 kV Curitiba 
Norte / Pilarzinho 
Sentido Normal: SE Pilarzinho recebe tensão da SE Curitiba Norte 
Ligar, em anel, a LT 230  kV Curitiba 
Norte / Pilarzinho. 
 
Sentido Inverso: SE Pilarzinho envia tensão para SE Curitiba Norte 
A energização em sentido inverso é controlada pelo COSR-S, conforme  
IO-PM.S.2PR. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Pilarzinho IO-OI.S.PIL 25 3.7.5.4. 25/09/2024 
 
Referência: 10 / 10 
 
Equipamento / Linha 
de Transmissão Procedimentos Condições ou Limites Associados 
LT 230 kV Pilarzinho / 
Santa Mônica 
Sentido Normal: SE Pilarzinho recebe tensão da SE Santa Mônica 
Ligar, em anel, a LT 230 kV Pilarzinho / 
Santa Mônica. 
• ∆δ ≤ 28°. 
Sentido Inverso: SE Pilarzinho envia tensão para SE Santa Mônica 
A energização em sentido inverso é controlada pelo COSR-S, conforme  
IO-PM.S.2PR. 
Transformador 
TF-A ou TF-B 
230/69/13,8 kV 
Sentido Normal: A partir do lado de 230 kV 
Energizar, pelo lado de 230 kV, o 
transformador TF-A 230/69/13,8 kV (ou 
TF-B). 
Sistema completo (de LT) ou N-1 (de LT) 
na SE Pilarzinho 230 kV 
Como primeiro ou segundo 
transformador: 
• VPIL-230 ≤ 242 kV; e 
• 1 ≤ TAPPIL-230/69 ≤ 9. 
Antes de energizar cada transformador, 
verificar fluxo de potência ativa nos 
transformadores 230/69/13,8 kV que já 
foram energizados. 
Ligar, energizando o barramento de 
69 kV, ou interligando esse 
transformador com outro já em 
operação, o lado  de 69  kV do 
transformador TF-A 230/69/13,8 kV (ou 
TF-B). 
Barramento de 69 kV da SE Pilarzinho 
desenergizado ou energizado por outro 
transformador 230/69/13,8 kV. 
Sentido Inverso: A partir do lado de 69 kV 
Energizar, pelo lado de 69 kV, o 
transformador TF-A 230/69/13,8 kV (ou 
TF-B). 
Sistema completo (de LT ) na SE 
Pilarzinho 69 kV 
Como primeiro ou segundo 
transformador: 
• VPIL-69 ≤ 72 kV. 
Antes de energizar cada transformador, 
verificar fluxo de potência ativa nos 
transformadores 230/69/13,8 kV que já 
foram energizados. 
Ligar, interligando esse transformador 
com outro já em operação , o lado de 
230 kV do transformador TF -A 
230/69/13,8 kV (ou TF-B). 
Outro transformador 230/69/13,8 kV  
da SE Pilarzinho com fluxo de potência 
ativa. 
7. NOTAS IMPORTANTES 
Não se aplica. 
