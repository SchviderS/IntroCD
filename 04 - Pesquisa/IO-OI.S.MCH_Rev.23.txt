 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Machadinho 
 
 
Código Revisão Item Vigência 
IO-OI.S.MCH 23 3.7.5.1. 19/08/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Incorporação da MOP/ONS 332-R/2024 – Alteração da denominação do Centro de Operação COT TBE Sul 
para COT TBE. 
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S CGT Eletrosul 
(COT Sul) 
Engie 
(COG) 
Lumitrans 
(COT TBE) 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Machadinho IO-OI.S.MCH 23 3.7.5.1. 19/08/2024 
 
Referência:  2 / 10 
 
ÍNDICE  
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  4 
3.1. Barramento de 525 kV ................................................................................................................... 4 
3.2. Alteração da Configuração dos Barramentos ................................................................................ 4 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 5 
4.1. Procedimentos Gerais ................................................................................................................... 5 
4.2. Procedimentos Específicos ............................................................................................................ 5 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  5 
5.1. Procedimentos Gerais ................................................................................................................... 5 
5.2. Procedimentos para Recomposição Fluente ................................................................................. 5 
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................ 5 
5.2.2. Recomposição Fluente da Instalação .......................................................................... 6 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 6 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 6 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 6 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 7 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 7 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 7 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 7 
6.1. Procedimentos Gerais ................................................................................................................... 7 
6.2. Procedimentos Específicos ............................................................................................................ 9 
6.2.1. Desenergização de Linhas de Transmissão e de Equipamentos ................................. 9 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ....................................... 9 
7. NOTAS IMPORTANTES ................................ ................................ ................................ ................ 10 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Machadinho IO-OI.S.MCH 23 3.7.5.1. 19/08/2024 
 
Referência:  3 / 10 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE  Machadinho, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são somente aqueles de interesse sistêmico, 
realizados com autonomia pelos Agentes Operadores da Instalação, devendo fazer parte do manual 
de operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade 
das ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR -S e a Instalação, no que se refere aos equipamentos de 
manobra da Instalação, é realizada conforme segue: 
Linha de Transmissão ou 
Equipamento  Agente de Operação Agente Operador 
Centro de 
Operação do 
Agente 
Operador 
Barramento 525 kV CGT Eletrosul  CGT Eletrosul  COT Sul 
LT 525 kV Campos Novos / 
Machadinho C1 CGT Eletrosul  CGT Eletrosul  COT Sul 
LT 525 kV Campos Novos / 
Machadinho C2 Lumitrans Lumitrans COT TBE 
LT 525 kV Itá / Machadinho CGT Eletrosul  CGT Eletrosul  COT Sul 
Módulo da Unidade Geradora GH 1 
16 kV da UHE Machadinho Engie Engie COG Engie 
Módulo da Unidade Geradora GH 2 
16 kV da UHE Machadinho Engie Engie COG Engie 
Módulo da Unidade Geradora GH 3 
16 kV da UHE Machadinho Engie Engie COG Engie 
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte da Área 525 kV da Região Sul. 
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.4.1. A definição da quantidade de tentativas de religamento manual de linha de transmissão, bem como 
o intervalo entre elas, é de responsabilidade do Agente e devem estar  descritos no Cadastro de 
Informações Operacionais da respectiva Área Elétrica. 
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar autorização ao COSR -S 
autorização para religamento. Nessa oportunidade, o Agente também pode solicitar a alteração no 
sentido normal para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em consideração as condições operativas do 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Machadinho IO-OI.S.MCH 23 3.7.5.1. 19/08/2024 
 
Referência:  4 / 10 
 
sistema.  
2.5. Quando caracterizado impedimento de linha de transmissão  ou de equipamento , deve m ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação , quando o Agente tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 525 KV 
A configuração do barramento de 525 kV é do tipo Barra Dupla (Barra P e Barra PT) a Quatro Chaves. Na 
operação normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados, exceto 
as seccionadoras de transferência e uma das seletoras de barra das linhas de transmissão ou 
equipamentos.  
A Instalação deve operar em condição normal com a seguinte configuração: 
Barra P (ou PT) 525 kV Barra PT (ou P) 525 kV 
LT 525 kV Campos Novos / Machadinho C1 (ou C2) LT 525 kV Campos Novos / Machadinho C2 (ou C1) 
LT 525 kV Itá / Machadinho Módulo da Unidade Geradora GH 1 (ou GH 2 ou GH 3) 
16 kV da UHE Machadinho 
Módulo da Unidade Geradora GH 2 (ou GH 1) 16 kV 
da UHE Machadinho 
 
 
Módulo da Unidade Geradora GH 3 (ou GH 1) 16 kV 
da UHE Machadinho  
3.2. ALTERAÇÃO DA CONFIGURAÇÃO DOS BARRAMENTOS 
A mudança de configuração do barramento de 525 kV desta Instalação é executada com controle do 
COSR-S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade do s Agentes 
Operadores da Instalação. 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Machadinho IO-OI.S.MCH 23 3.7.5.1. 19/08/2024 
 
Referência:  5 / 10 
 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 525 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S.  
As faixas de controle de tensão para esse barramento estão estabelecidas no Cadastro de Informações 
Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
Não se aplica. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação : caracterizado quando não há tensão em todos os terminais de 
suas linhas de transmissão. 
• Desligamento parcial da Instalação : qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, os Agentes Operadores da Instalação devem  fornecer ao 
COSR-S as seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2, sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificand o a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia dos Agentes Operadores da Instalação  na 
recomposição, deve ser utilizado o Subitem 5.3.  
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE 
No caso de desligamento total, os Agentes Operadores devem configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir. 
Abrir ou manter aberto os disjuntores:  
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Machadinho IO-OI.S.MCH 23 3.7.5.1. 19/08/2024 
 
Referência:  6 / 10 
 
• das linhas de transmissão:  
LT 525 kV Itá / Machadinho; 
LT 525 kV Campos Novos / Machadinho C1; 
LT 525 kV Campos Novos / Machadinho C2. 
 
Fechar ou manter fechado os disjuntores: 
• do módulo de interligação barras de 525 kV, exceto quando o mesmo estiver  substituindo o 
disjuntor de um equipamento ou uma linha de transmissão. 
 
Cabe à CGT Eletrosul informar ao COSR -S quando a configuração de preparação da Instalação não 
estiver atendida para o início da recomposição, independentemente de o equipamento ser próprio ou 
de outros Agentes. Nes se caso, o COSR -S fará contato com os agentes envolvidos  para identificar o 
motivo do não-atendimento e, após confirmação da CGT Eletrosul de que os barramentos estão com 
a configuração atendida, o COSR-S coordenará os procedimentos para recomposição, caso necessário, 
em função da configuração desta Instalação. 
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO 
A Instalação faz parte da recomposição da Área Itá. Os Agentes Operadores da Instalação devem 
adotar os procedimentos a seguir para a recomposição fluente: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 CGT Eletrosul  
(COT Sul) 
Receber tensão da SE Itá, pela LT 525 kV 
Itá / Machadinho e energizar o 
barramento de 525 kV. 
 
2 CGT Eletrosul  
(COT Sul) 
Verificar o recebimento de tensão da 
UHE Machadinho de duas unidades 
geradoras e sincronizá-las. 
 
2.1 CGT Eletrosul  
(COT Sul) 
Energizar a LT 525 kV Campos Novos / 
Machadinho C1, enviando tensão para a 
SE Campos Novos. 
VMCH-525 ≤ 540 kV. 
Após fluxo de potência ativa  na LT 
525 kV Itá / Machadinho. 
2.2 Lumitrans 
(COT TBE) 
Energizar a LT 525 kV Campos Novos / 
Machadinho C2, enviando tensão para a 
SE Campos Novos. 
VMCH-525 ≤ 540 kV. 
Após fluxo de potência ativa  na LT 
525 kV Campos Novos / Machadinho 
C1. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
Os Agentes Operadores da Instalação devem realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
Os Agentes Operadores da Instalação devem realizar os procedimentos do Subitem 5.2.2, enquanto 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Machadinho IO-OI.S.MCH 23 3.7.5.1. 19/08/2024 
 
Referência:  7 / 10 
 
não houver intervenção do COSR -S no processo de recomposição ou interrupção da autonomia 
destes na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, 
conforme procedimentos contidos nas respectivas Instruções de Preparação para Manobras.  
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, os 
Agentes Operadores devem preparar a Instalação conforme Subitem 5.2.1, sem necessidade de 
autorização do ONS. 
5.4.1.2. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 5.2 .2, sem necessidade de 
autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação de até dois equipamentos ou desligamentos 
múltiplos de circuitos e transformadores que operam em paralelo, o s Agentes Operadores devem 
recompor a Instalação observando as condições do Subitem 6.2.2., sem necessidade de autorização 
do ONS.    
Caracterizado desligamento parcial da Instalação de três ou mais equipamentos , que não sejam 
circuitos ou transformadores que operam em paralelo, a recomposição da Instalação é executada 
com controle do COSR-S.   
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização  programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmi ssão ou de equipamentos, após  
desligamentos programados, de urgência ou de emergência, só podem ser efetuados com controle do 
COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Machadinho IO-OI.S.MCH 23 3.7.5.1. 19/08/2024 
 
Referência:  8 / 10 
 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelos Agentes Operadores  da Instalação 
quando estiverem explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução 
de Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir autonomia, 
a energização deve ser executada com controle do COSR -S, conforme Instrução de Operação de 
Preparação para Manobras da respectiva área elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, os Agentes Operadores da Instalação devem  verificar se 
existe tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pelos Agentes Operadores da Instalação 
quando estiver especificado nesta Instrução de O peração e estiverem atendidas as condições do 
Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou , quando permitida, em sentido  inverso, pode ser feita com 
autonomia pelos Agentes Operadores , conforme procedimentos para manobras que estão 
definidos nesta Instrução de Operação, Subitem 6.2.2 . Os procedimentos para energização 
controlados pelo COSR-S estão definidos na Instrução de Operação de Preparação para Manobras 
da respectiva área elétrica.  
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, os Agentes Operadores  da Instalação deve m fechá-lo em anel desde que tenha 
autonomia para tal, adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S. 
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de linha 
de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle do 
COSR-S. 
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Machadinho IO-OI.S.MCH 23 3.7.5.1. 19/08/2024 
 
Referência:  9 / 10 
 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação, é 
sempre controlada pelo COSR-S. 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelos Agentes Operadores da Instalação, 
após desligamento automático de equipamentos ou de linhas de transmissão. 
Os Agentes Operadores da Instalação devem identificar os desligamentos automáticos observando 
na Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado 
nas condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Linha de Transmissão Procedimentos Condições ou Limites Associados 
LT 525 kV Campos Novos / 
Machadinho C1 
Sentido Normal: SE Machadinho envia tensão da SE Campos Novos  
Energizar a  LT 525 kV Campos 
Novos / Machadinho C1. 
Sistema completo (de LT) n a SE 
Machadinho 525 kV  ou N-1 da LT 525 
kV Campos Novos / Machadinho C2 
• VMCH-525 ≤ 545 kV, e  
• 1 ou mais UGs sincronizadas na 
UHE Machadinho. 
 
Antes de energizar cada LT, verificar 
fluxo de potência ativa no circuito da LT 
525 kV Campos Novos / Machadinho 
que já foi energizado. 
 Sentido Inverso: SE Machadinho recebe tensão da SE Campos Novos  
Ligar, em anel, a LT 525 kV Campos 
Novos / Machadinho C1. 
∆δ ≤ 30°. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Machadinho IO-OI.S.MCH 23 3.7.5.1. 19/08/2024 
 
Referência:  10 / 10 
 
Linha de Transmissão Procedimentos Condições ou Limites Associados 
LT 525 kV Campos Novos / 
Machadinho C2 
Sentido Normal: SE Machadinho envia tensão da SE Campos Novos  
Energizar a  LT 525 kV Campos 
Novos / Machadinho C2. 
Sistema completo (de LT) n a SE 
Machadinho 525 kV  ou N-1 da LT 525 
kV Campos Novos / Machadinho C1 
• VMCH-525 ≤ 545 kV, e  
• 1 ou mais UGs sincronizadas na 
UHE Machadinho. 
 
Antes de energizar cada LT, verificar 
fluxo de potência ativa no circuito da LT 
525 kV Campos Novos / Machadinho 
que já foi energizado. 
Sentido Inverso: SE Machadinho recebe tensão da SE Campos Novos  
Ligar, em anel, a LT 525 kV Campos 
Novos / Machadinho C2. 
∆δ ≤ 30°. 
LT 525 kV Itá / Machadinho Sentido Normal: SE Machadinho recebe tensão da SE Itá  
Ligar, em anel, a LT 525 kV Itá  / 
Machadinho.  
∆δ ≤ 30°. 
Sentido Inverso: SE Machadinho envia tensão para SE Itá  
A energização em sentido inverso é controlada pelo COSR-S, conforme  
IO-PM.S.5SU. 
7. NOTAS IMPORTANTES 
7.1. Para substituir o disjuntor da LT 525 kV Campos Novos / Machadinho C2 pelo disjuntor de interligação 
das barras P e PT  525 kV, o COSR -S coordenará essa ação com a Lumitrans e com a CGT Eletrosul. 
Após tal substituição, os procedimentos para abertura do disjuntor ou recomposição das linhas de 
transmissão deverão ser coordenados pelo COSR -S, que orientará a operação da CGT Eletrosul na 
execução dos procedimentos descritos nesta Instrução de Operação, bem como na Instrução de 
Operação de Preparação para Manobras da respectiva Área Elétrica. 
