 
 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.11 
 
Cadastro de Informações Operacionais. 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE TRANSMISSÃO E 
TRANSFORMADORES DA ÁREA 230 KV DO TOCANTINS 
 
Código Revisão Item Vigência 
CD-CT.N.2TO.02 3 2.2.4 21/05/2024 
 
 
MOTIVO DA REVISÃO 
 
Alteração dos limites dos Autotransformadores T1 e T2 500/230/13,8 kV - 960 MVA da SE Lajeado, sendo 
adotada no cálculo a referência de tensão de 500 kV. 
 
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-NCO COSR-NE EDP BRASIL ENERGISA SOLUCOES 
ENERGISA TOCANTINS TAESA    
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA ÁREA 230 KV 
DO TOCANTINS 
CD-CT.N.2TO.02 3 2.2.4 21/05/2024 
 
E-mail SAC de 21/05/2024. 
 
2 / 14 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONCEITOS ................................ ................................ ................................ ................................ ... 3 
3. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 5 
4. LIMITES DE CARREGAMENTO DE LINHAS DE TRANSMISSÃO ................................ ...........................  7 
4.1. LT 230 kV Dianópolis II / Gurupi .................................................................................................... 7 
4.2. LT 230 kV Dianópolis II / Palmas C1 ............................................................................................... 7 
4.3. LT 230 KV Lajeado / Palmas C1 ...................................................................................................... 7 
4.4. LT 230 KV Lajeado / Palmas C2 ...................................................................................................... 8 
5. LIMITES DE CARREGAMENTO DE TRANSFORMADORES ................................ ................................ .. 9 
5.1. SE Dianópolis II – Autotransformador DDAT6-01 230/138/13,8 kV - 200 MVA ........................... 9 
5.2. SE Dianópolis II – Autotransformador DDAT6-02 230/138/13,8 kV - 200 MVA ........................... 9 
5.3. SE Gurupi – Autotransformador AT01 de 500/230/13,8 kV - 450 MVA...................................... 10 
5.4. SE Gurupi – Autotransformador AT04 de 230/138/13,8 kV - 100 MVA...................................... 10 
5.5. SE Gurupi – Autotransformador AT05 de 230/138/13,8 kV - 100 MVA...................................... 11 
5.6. SE Lajeado – Autotransformador T1 500/230/13,8 kV - 960 MVA ............................................. 12 
5.7. SE Lajeado – Autotransformador T2 500/230/13,8 kV - 960 MVA ............................................. 12 
5.8. SE Palmas – Autotransformador PLAT6-01 230/138/13,8 kV - 200 MVA ................................... 13 
5.9. SE Palmas – Autotransformador PLAT6-02 230/138/13,8 kV - 200 MVA ................................... 13 
 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA ÁREA 230 KV 
DO TOCANTINS 
CD-CT.N.2TO.02 3 2.2.4 21/05/2024 
 
E-mail SAC de 21/05/2024. 
 
3 / 14 
 
1. OBJETIVO 
Apresentar os limites operacionais de linhas de transmissão e de transfo rmadores da Rede de Operação a 
serem controlados pelos operadores dos Centros de Operação do ONS e pela Operação dos Agentes 
envolvidos. 
2. CONCEITOS 
2.1. Limite Operacional: Corresponde ao limite de carregamento do equipamento que deve ser observado 
pelo ONS e pelos Agentes, em condição normal de operação e ou em condição de emergência. 
2.2. Alteração dos Limites Operacionais: É a alteração de limite na qual são estabelecidos novos valores, 
decorrentes de eventos que alteraram a capacidade operativa do equipamento, como por exemplo 
recapacitação, substituição de TC, modificação de característica de proteção, indisponibili dade do 
sistema de refrigeração, flexibilização por parte do Agente, etc. 
2.3. Fator Limitante:  Fator que impede um equipamento de atingir um limite operacional superior ao 
estabelecido de projeto. Um fator limitante ativo provoca aumento no custo de operação ou de 
expansão do SIN. 
2.4. Limites de linhas de transmissão 
2.4.1. Condição Normal de Operação 
Situação de carregamento em que a linha de transmissão conduz continuamente corrente em valor igual ou 
inferior ao seu valor operacional, em sua condição de carregamento nominal, para a qual foi projetada. 
2.4.2. Condição de Emergência de Operação 
Situação de carregamento em que a linha de transmiss ão conduz corrente acima do seu valor operacional, 
em sua condição de carregamento superior ao nominal para a qual foi projetada, por um tempo não superior 
a 4 dias (96 horas) contínuos, ou um somatório máximo de 18 dias (432 horas) intermitentes (se somados 
em base anual todos os períodos contínuos inferiores a 4 dias) , conforme disposto no anexo 3 da seção 4.1 
das Regras dos Serviços de Transmissão. 
As linhas de transmissão que se enquadrarem nessa situação terão o campo duração nas tabelas 
apresentadas neste cadastro preenchido com (*). 
2.4.3. Condição Sazonal de Operação 
Situação de carregamento em que a linha de transmissão transporta continuamente corrente em valor igual 
ou inferior ao seu valor operacional, em condição normal de operação e ou em condição de emergência, em 
condições de operação temporárias e específicas para diferentes períodos do ano, tais como verão -dia, 
verão-noite, inverno-dia e inverno -noite ou outro período específico do ano (meses ou estações do ano) , 
conforme disposto no anexo 3 da seção 4.1 das Regras dos Serviços de Transmissão. 
2.4.4. Limite Diurno  
Limite operacional a ser considerado para o período diurno. O período diurno é o intervalo entre o horário 
do amanhecer e do crepúsculo, conforme disposto nos Procedimentos de Rede. 
Quando o período não for definido pelo agente , deve ser considerado o período entre às 6h até às 17h59, 
sempre tomando como referência o horário oficial de Brasília. 
2.4.5. Limite Noturno  
Limite operacional a ser considerado para o período noturno. O período noturno é o intervalo de tempo 
entre o horário do crepúsculo e do amanhecer, conforme disposto nos Procedimentos de Rede. 
Quando o período não for definido pelo agente, deve ser considerado o período das 18h até às 5h59, sempre 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA ÁREA 230 KV 
DO TOCANTINS 
CD-CT.N.2TO.02 3 2.2.4 21/05/2024 
 
E-mail SAC de 21/05/2024. 
 
4 / 14 
 
tomando como referência o horário oficial de Brasília. 
2.5. Limites de Transformadores 
2.5.1. Condição Normal de Operação  
Situação de carregamento em que o transformador conduz continuamente até a  sua corrente nominal. 
Mesmo que em algum momento seja ultrapassada a sua corrente nominal, não deve ser excedida a 
temperatura máxima do óleo e ou do enrolamento, conforme limites estabelecidos na NBR 5356-7, 
respeitadas as condições, valores e prazos admitidos pelo agente. 
2.5.2. Condição de Emergência de Longa Duração  
Situação de carregamento em que o transformador conduz continuamente uma corrente superior à sua 
corrente nominal, com duração não superior a 4 (quatro) horas. Mesmo que em algum momento seja 
ultrapassada a sua corrente de emergência de longa duração, não deve ser excedida a temperatura máxima 
do óleo e ou do enrolamento, conforme limites estabele cidos na NBR 5356-7, respeitadas as condições, 
valores e prazos admitidos pelo agente. 
2.5.3. Condição de Emergência de Curta Duração  
Situação de carregamento em que o transformador conduz continuamente uma corrente superior à sua 
corrente de emergência de longa duração, com duração não superior a 30 (trinta) minutos. Após esse 
intervalo de tempo, deve-se retornar à condição de carregamento de longa duração. 
Mesmo que em algum momento seja ultrapassada a sua corrente de emergência de curta duração, não deve 
ser excedida a temperatura máxima do óleo e ou do enrolamento, conforme limites estabelecidos na NBR 
5356-7, respeitadas as condições, valores e prazos admitidos pelo agente. Esta condição deve ser utilizada 
como último recurso antes de se efetuar corte de carga. 
2.6. Preenchimento das Tabelas de Limites 
2.6.1. Linhas de Transmissão 
O campo “Valor Operacional” deve ser preenchido com o valor limite do equipamento a ser considerado na 
operação em tempo real. 
O campo “Duração (hh:mm)” indica o período admissível para operação da linha de transmissão na condição 
de emergência. 
Nas tabelas em que for detalhado o tempo de operação em determinada condição de operação de 
equipamento, deve-se utilizar tantas linhas quanto forem necessárias para o detalhamento de carregamento 
por período de tempo informado pelo Agente. 
Os campos “Valor Operacional” e “Duração” das tabelas, cujos valores dependem de informação do Agente 
e não estão disponíveis para o ONS, devem ser preenchidas com “NI” (Não Informado pelo Agente). 
O campo “Valor Operacional” das tabelas , referentes a valores de emergência que não são permitidos pelo 
Agente, devem ser preenchidos com “NP” (Não Permitido pelo Agente). Para esse caso, também deve ser 
preenchido o campo “Fator Limitante”. 
O campo “Período do Ano” indica, quando for o caso, a sazonalidade anual definida para determinação dos 
valores operacionais em condição normal e de emergência. 
O campo “Período do Dia” indica, quando for o caso, a sazonalidade diária definida para determinação dos 
valores operacionais em condição normal e de emergência. 
2.6.2. Transformadores 
O campo “Enrolamento (kV)” indica o terminal e a tensão de referência a os quais está relacionado o limite 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA ÁREA 230 KV 
DO TOCANTINS 
CD-CT.N.2TO.02 3 2.2.4 21/05/2024 
 
E-mail SAC de 21/05/2024. 
 
5 / 14 
 
operacional nas condições normal, emergência de longa duração e emergência de curta duração. 
O campo “Valor Operacional (A)” indica o valor de corrente limite do transformador para condição normal, 
emergência de longa duração e de curta duração , considerando como tensão nominal a indicada no campo 
“Enrolamento (kV)”. 
O campo “Duração  (hh:mm)” indica o período admissível para operação do transformador na condição de 
emergência de longa e emergência de curta duração. 
Nas tabelas em que for detalhado o tempo de operação em determinada condição de operação de 
equipamento, deve-se utilizar tantas linhas quanto forem necessárias para o detalhamento de carregamento 
por período de tempo informado pelo Agente. 
Os campos “Valor Operacional” e “Duração” das tabelas, cujos valores dependem de informação do Agente 
e não estão disponíveis para o ONS, devem ser preenchidas com “NI” (Não Informado pelo Agente). 
O campo “Valor Operacional” das tabelas referentes a valores de emergência , que não são permitidos pelo 
Agente, devem ser preenchidos com “NP” (Não Permitido pelo Agente). Para esse caso, também deve ser 
preenchido o campo “Fator Limitante”. 
3. CONSIDERAÇÕES GERAIS 
3.1. Este Cadastro de Informações Operacionais apresenta os limites operacionais de carregamento d as 
linhas de transmissão e dos transformadores da Rede de Operação, de acordo com as definições 
estabelecidas entre ANEEL, ONS e agentes, cujo processo de atualização é descrito pela RO-CD.BR.01 
- Controle de Limites de Carregamento de Linhas de Transmissão e Transformadores. 
3.2. Os operadores dos Centros de Operação do ONS, nas ações de coordenação, supervisão e controle 
devem observar os limites operacionais constantes neste Cadastro de Informações Operacionais. 
3.3. A operação dos agentes, em suas ações de comando e execução, deve observar os limites 
operacionais constantes neste Cadastro de Informações Operacionais. 
3.4. Cabe ao agente comunicar de imediato ao Centro de Operação com o qual se relaciona qualquer 
alteração de limite que imponha restrições à operação de linha de transmissão ou de transformador. 
3.5. Toda e qualquer alteração nos valores de limites operacionais constantes neste Cadastro de 
Informações Operacionais deverá ser solicitada formalmente e devidamente justificada e 
caracterizada pelo agente responsável pelo equipamento  ao Centro de Operação do ONS de seu 
relacionamento.  
3.6. Caso a operação em tempo real venha a verificar qualquer possibilidade de violação do limite 
estabelecido para o equipamento, caberá ao Centro de Operação do ONS responsável tomar as 
medidas corretivas cabíveis , para que esse não ultrapasse esses valores, que só podem ser 
ultrapassados com anuência do agente. 
3.7. Os valores de Limites Operacionais dos equipamentos da Rede de Operação, constantes neste 
Cadastro de Informações Operacionais, são os valores informados oficial mente e formalmente pelo 
agente. 
3.8. Os limites de transformadores que constam neste Cadastro de Informações Operacionais consideram 
todos os estágios de refrigeração disponíveis. Os limites considerando os estágios de refrigeração 
indisponíveis constam nos Cadastros de Informações Operacionais de Dados de Equipamentos. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA ÁREA 230 KV 
DO TOCANTINS 
CD-CT.N.2TO.02 3 2.2.4 21/05/2024 
 
E-mail SAC de 21/05/2024. 
 
6 / 14 
 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA ÁREA 230 KV 
DO TOCANTINS 
CD-CT.N.2TO.02 3 2.2.4 21/05/2024 
 
E-mail SAC de 21/05/2024. 
 
7 / 14 
 
4. LIMITES DE CARREGAMENTO DE LINHAS DE TRANSMISSÃO 
4.1. LT 230 KV DIANÓPOLIS II / GURUPI 
Limites de condição normal de operação (Longa duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 1.450 Capacidade nominal 
 
Limites de condição de emergência (Curta duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 1.798 04:00 - 
 
4.2. LT 230 KV DIANÓPOLIS II / PALMAS C1 
Limites de condição normal de operação (Longa duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 1.412 Capacidade nominal 
 
Limites de condição de emergência (Curta duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 1.768 04:00 - 
 
4.3. LT 230 KV LAJEADO / PALMAS C1 
Limites de condição normal de operação (Longa duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 785 Capacidade nominal 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA ÁREA 230 KV 
DO TOCANTINS 
CD-CT.N.2TO.02 3 2.2.4 21/05/2024 
 
E-mail SAC de 21/05/2024. 
 
8 / 14 
 
Limites de condição de emergência (Curta duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 980 04:00 - 
 
4.4. LT 230 KV LAJEADO / PALMAS C2 
Limites de condição normal de operação (Longa duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 785 Capacidade nominal 
 
Limites de condição de emergência (Curta duração) 
Período do ano Período 
do dia 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
01/Jan a 31/Dez Diurno / 
Noturno 980 (*) - 
 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA ÁREA 230 KV 
DO TOCANTINS 
CD-CT.N.2TO.02 3 2.2.4 21/05/2024 
 
E-mail SAC de 21/05/2024. 
 
9 / 14 
 
5. LIMITES DE CARREGAMENTO DE TRANSFORMADORES 
5.1. SE DIANÓPOLIS II – AUTOTRANSFORMADOR DDAT6-01 230/138/13,8 KV - 200 MVA 
Limites de Condição Normal de Operação 
Enrolamento (kV) Valor 
operacional (A) Fator limitante 
138 837 Capacidade nominal 
230 502 Capacidade nominal 
 
Limites de Condição de Emergência de Longa Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
138 1.004 04:00 - 
230 602 04:00 - 
 
Limites de Condição de Emergência de Curta Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
138 1.171 00:30 - 
230 703 00:30 - 
 
5.2. SE DIANÓPOLIS II – AUTOTRANSFORMADOR DDAT6-02 230/138/13,8 KV - 200 MVA 
Limites de Condição Normal de Operação 
Enrolamento (kV) Valor 
operacional (A) Fator limitante 
138 837 Capacidade nominal 
230 502 Capacidade nominal 
 
Limites de Condição de Emergência de Longa Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
138 1.004 04:00 - 
230 602 04:00 - 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA ÁREA 230 KV 
DO TOCANTINS 
CD-CT.N.2TO.02 3 2.2.4 21/05/2024 
 
E-mail SAC de 21/05/2024. 
 
10 / 14 
 
Limites de Condição de Emergência de Curta Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
138 1.171 00:30 - 
230 703 00:30 - 
 
5.3. SE GURUPI – AUTOTRANSFORMADOR AT01 DE 500/230/13,8 KV - 450 MVA 
Limites de Condição Normal de Operação 
Enrolamento (kV) Valor 
operacional (A) Fator limitante 
230 1.130 Capacidade nominal 
500 520 Capacidade nominal 
 
Limites de Condição de Emergência de Longa Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
230 1.356 04:00 -- 
500 624 04:00 -- 
 
Limites de Condição de Emergência de Curta Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
230 1.582 00:30 -- 
500 728 00:30 -- 
 
5.4. SE GURUPI – AUTOTRANSFORMADOR AT04 DE 230/138/13,8 KV - 100 MVA 
Limites de Condição Normal de Operação 
Enrolamento (kV) Valor 
operacional (A) Fator limitante 
138 418 - 
230 251 - 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA ÁREA 230 KV 
DO TOCANTINS 
CD-CT.N.2TO.02 3 2.2.4 21/05/2024 
 
E-mail SAC de 21/05/2024. 
 
11 / 14 
 
Limites de Condição de Emergência de Longa Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
138 501 04:00 - 
230 301 04:00 - 
 
Limites de Condição de Emergência de Curta Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
138 585 00:30 - 
230 351 00:30 - 
 
5.5. SE GURUPI – AUTOTRANSFORMADOR AT05 DE 230/138/13,8 KV - 100 MVA 
Limites de Condição Normal de Operação 
Enrolamento (kV) Valor 
operacional (A) Fator limitante 
138 418 - 
230 251 - 
 
Limites de Condição de Emergência de Longa Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
138 501 04:00 - 
230 301 04:00 - 
 
Limites de Condição de Emergência de Curta Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
138 585 00:30 - 
230 351 00:30 - 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA ÁREA 230 KV 
DO TOCANTINS 
CD-CT.N.2TO.02 3 2.2.4 21/05/2024 
 
E-mail SAC de 21/05/2024. 
 
12 / 14 
 
5.6. SE LAJEADO – AUTOTRANSFORMADOR T1 500/230/13,8 KV - 960 MVA 
Limites de Condição Normal de Operação 
Enrolamento (kV) Valor 
operacional (A) Fator limitante 
230 2.410 -- 
500 1.109 -- 
 
Limites de Condição de Emergência de Longa Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
230 2.892 04:00 -- 
500 1.330 04:00 -- 
 
Limites de Condição de Emergência de Curta Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
230 3.374 00:30 -- 
500 1.552 00:30 -- 
 
5.7. SE LAJEADO – AUTOTRANSFORMADOR T2 500/230/13,8 KV - 960 MVA 
Limites de Condição Normal de Operação 
Enrolamento (kV) Valor 
operacional (A) Fator limitante 
230 2.410 - 
500 1.109 -- 
 
Limites de Condição de Emergência de Longa Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
230 2.892 04:00 - 
500 1.330 04:00 -- 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA ÁREA 230 KV 
DO TOCANTINS 
CD-CT.N.2TO.02 3 2.2.4 21/05/2024 
 
E-mail SAC de 21/05/2024. 
 
13 / 14 
 
Limites de Condição de Emergência de Curta Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
230 3.374 00:30 - 
500 1.552 00:30 -- 
 
5.8. SE PALMAS – AUTOTRANSFORMADOR PLAT6-01 230/138/13,8 KV - 200 MVA 
Limites de Condição Normal de Operação 
Enrolamento (kV) Valor 
operacional (A) Fator limitante 
138 837 Capacidade nominal 
230 502 Capacidade nominal 
 
Limites de Condição de Emergência de Longa Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
138 1.004 04:00 - 
230 602 04:00 - 
 
Limites de Condição de Emergência de Curta Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
138 1.171 00:30 - 
230 703 00:30 - 
 
5.9. SE PALMAS – AUTOTRANSFORMADOR PLAT6-02 230/138/13,8 KV - 200 MVA 
Limites de Condição Normal de Operação 
Enrolamento (kV) Valor 
operacional (A) Fator limitante 
138 837 Capacidade nominal 
230 502 Capacidade nominal 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
CADASTRO DE LIMITES OPERACIONAIS DE LINHAS DE 
TRANSMISSÃO E TRANSFORMADORES DA ÁREA 230 KV 
DO TOCANTINS 
CD-CT.N.2TO.02 3 2.2.4 21/05/2024 
 
E-mail SAC de 21/05/2024. 
 
14 / 14 
 
Limites de Condição de Emergência de Longa Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
138 1.004 04:00 - 
230 602 04:00 - 
 
Limites de Condição de Emergência de Curta Duração 
Enrolamento 
(kV) 
Valor 
operacional (A) 
Duração 
(Hh:mm) Fator limitante 
138 1.171 00:30 - 
230 703 00:30 - 
 
