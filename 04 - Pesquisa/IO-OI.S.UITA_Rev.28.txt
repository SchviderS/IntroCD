 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da UHE Itaúba 
 
 
Código Revisão Item Vigência 
IO-OI.S.UITA 28 3.7.5.2. 03/09/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Incorporação da MOP/ONS 372 -R/2024 “Mudança da denominação da CPFL-T e do seu centro de 
operação”. 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S CPFL-T 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Itaúba IO-OI.S.UITA 28 3.7.5.2. 03/09/2024 
 
Referência:  2 / 11 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  4 
3.1. Barramento de 230 kV ................................................................................................................... 4 
3.2. Unidades Geradoras ...................................................................................................................... 4 
3.3. Alteração da Configuração dos Barramentos ................................................................................ 4 
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL ................................ ....................  4 
4.1. Procedimentos Gerais ................................................................................................................... 4 
4.2. Procedimentos Específicos ............................................................................................................ 5 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  5 
5.1. Procedimentos Gerais ................................................................................................................... 5 
5.2. Procedimentos para Recomposição Fluente ................................................................................. 6 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 7 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 7 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 8 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 8 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 8 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 8 
6. MANOBRAS DE UNIDADES GERADORAS, DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ........ 8 
6.1. Procedimentos Gerais ................................................................................................................... 8 
6.2. Procedimentos Específicos ............................................................................................................ 9 
6.2.1. Desligamento de Unidades Geradoras e Desenergização de Linhas de Transmissão e de 
Equipamentos .............................................................................................................. 9 
6.2.2. Sincronismo de Unidades Geradoras e Energização de Linhas de Transmissão e de 
Equipamentos .............................................................................................................. 9 
7. NOTAS IMPORTANTES ................................ ................................ ................................ ................ 11 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Itaúba IO-OI.S.UITA 28 3.7.5.2. 03/09/2024 
 
Referência:  3 / 11 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da UHE Itaúba , definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, realizados 
com autonomia pelo Agente Operador  da Instalação, devendo fazer parte do manual de operação 
próprio elaborado pelo Agente quando existente, observando -se a complementaridade das ações que 
devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR-S e a Instalação, de propriedade do Consórcio Itaúba - Energia, 
é realizada pela CPFL-T, agente responsável pela operação da Instalação, por intermédio do COT CPFL-
T. 
Nota: A execução da operação na UHE Itaúba é realizada pela CEEE-G. 
2.3. As unidades geradoras,  equipamentos e linhas de transmissão desta Instalação fazem parte da Área 
230 kV do Rio Grande do Sul. 
2.4. O COSR-S controla e supervisiona o despacho de geração desta Usina. 
2.5. Esta Usina: 
• é despachada centralizadamente; 
• está conectada na Rede de Operação; 
• não participa do Controle Automático de Geração – CAG; 
• é de autorrestabelecimento integral; 
• é fonte para início do processo de recomposição fluente da Área Itaúba; 
• é responsável pelo controle de frequência e tensão da Área Itaúba na fase de recomposição fluente, 
sendo o controle da frequência efetuada pela operação da Usina até orientação diferente do COSR -
S. 
2.6. Os dados operacionais desta Usina  estão descritos no Cadastro de Informações Operacionais da 
respectiva Área Elétrica. 
2.7. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.7.1. A definição da quantidade  de tentativas de religamento manual de linha de transmissão ou de 
equipamento, bem como o intervalo entre elas, é de responsabilidade do Agente e devem estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica. 
2.7.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR-S autorização 
para religamento. Nessa oportunidade, o Agente também pode solicitar alteração no sentido normal 
para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em consideração as condições operativas do 
sistema. 
2.8. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação em contingência da respectiva área elétrica. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Itaúba IO-OI.S.UITA 28 3.7.5.2. 03/09/2024 
 
Referência:  4 / 11 
 
2.9. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica. 
2.10. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos no 
Subitem 6.2.2 desta Instrução de Operação quando o Agente tiver autonomia para energizar a linha de 
transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. Barramento de 230 kV 
A configuração do barramento de 230 kV é do tipo Barra Dupla (Barra A e Barra B) a Cinco Chaves . Na 
operação normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados, exceto as 
seccionadoras de transferência e uma das seletoras de barra dos equipamentos e das linhas de transmissão. 
3.2. Unidades Geradoras 
As unidades geradoras G1, G2, G3 e G4 13,8 kV est ão conectadas ao barramento de 230 kV, pelos 
transformadores TR-1, TR-2, TR3 e TR-4 13,8/230 kV da UHE Itaúba, com todas as seccionadoras e disjuntores 
fechados, exceto a de transferência e uma das seletoras de barras. 
3.3. Alteração da Configuração dos Barramentos 
A mudança de configuração do barramento 230 kV da Instalação é executada com controle do COSR-S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade do Agente Operador 
da Instalação. 
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL 
4.1. Procedimentos Gerais 
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S. 
As faixas de controle de tensão para esse barramento estão estabelecidas no Cadastro de Informações 
Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob a responsabilidade da Instalação. 
4.1.3. A Usina deve manter os valores de geração de acordo com os valores programados, constantes no 
Programa Diário de Operação – PDO. Para atendimento dos valores programados constantes no PDO, 
não é necessária a autorização prévia do COSR-S. 
Qualquer alteração no valor de geração da Usina em relação ao valor de geração ao constante no PDO 
somente pode ser executada após autorização do COSR-S. 
As reprogramações de geração quando de necessidades sistêmicas serão executadas pela Usina 
quando solicitadas pelo COSR-S. 
Após reprogramação de geração solicitada pelo COSR -S, o Agente somente poderá alterar a geração 
da Usina com autorização do ONS, inclusive para adoção de valores contidos no PDO. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Itaúba IO-OI.S.UITA 28 3.7.5.2. 03/09/2024 
 
Referência:  5 / 11 
 
4.1.4. Os desvios de geração da Usina em relação aos valores previstos no PDO ou em relação às 
reprogramações devem ser controlados observando-se os valores máximos permitidos explicitados na 
Instrução de Operação IO-CG.BR.01 – Controle da Geração em Condição Normal. 
4.1.5. Quando não existir ou não estiver disponível a supervisão da Usina para o ONS, essa deve seguir as 
orientações para envio de dados conforme Rotina Operacional RO-AO.BR.08 – Apuração de Dados de 
Despacho de Geração e de Intercâmbios. 
4.1.6. A Usina deve registrar e informar imediatamente os seguintes dados ao COSR-S: 
• movimentação de unidades geradoras hidráulicas (mudança de estado operativo / disponibilidade);  
• restrições e ocorrências na Usina ou na conexão elétrica que afetem a disponibilidade de geração, 
com o respectivo valor da restrição, contendo o horário de início e término e a descrição do evento; 
• demais informações sobre a operação da Instalação, solicitadas pelo ONS. 
4.1.7. O controle de tensão, por meio da geração ou absorção de potência reativa das unidades geradoras 
da Usina, é controlado pelo COSR-S, com comando e execução pela operação do Agente. 
4.2. Procedimentos Específicos 
4.2.1. Operação das Unidades Geradoras como Compensadores Síncronos 
A conversão da modalidade de operação de gerador para compensador síncrono, bem como a 
reversão de compensador síncrono para gerador, deve ser executada sob controle do COSR-S. 
Devem ser observados os procedimentos relacionados a prestação de Serviços Ancilares de Suporte 
de Reativos, conforme Instrução de Operação IO-CG.BR.01 – Controle da Geração em Condição 
Normal. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO  
5.1. Procedimentos Gerais 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação : caracterizado quando não há tensão em todos os terminais de 
suas linhas de transmissão. 
• Desligamento parcial da Instalação : qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, o Agente Operador deve fornecer ao COSR -S as seguintes 
informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2, sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificand o a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia do Agente Operador  da Instalação na 
recomposição, deve ser utilizado o Subitem 5.3.  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Itaúba IO-OI.S.UITA 28 3.7.5.2. 03/09/2024 
 
Referência:  6 / 11 
 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. Procedimentos para Recomposição Fluente 
5.2.1. Preparação da Instalação para a Recomposição Fluente 
No caso de desligamento total, o Agente Operador deve  configurar os disjuntores dos seguintes 
equipamentos, linhas de transmissão e unidades geradoras, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• das linhas de transmissão: 
LT 230 kV Dona Francisca / Usina Hidrelétrica Itaúba C1; 
LT 230 kV Dona Francisca / Usina Hidrelétrica Itaúba C2; 
LT 230 kV Candelária 2/ Usina Hidrelétrica Itaúba; 
LT 230 kV Polo Petroquímico / Usina Hidrelétrica Itaúba; 
LT 230 kV Santa Cruz 1 / Usina Hidrelétrica Itaúba; 
LT 230 kV Usina Hidrelétrica Itaúba / Usina Hidrelétrica Passo Real. 
• das unidades geradoras: 
G1 13,8 kV (de 230 kV); 
G2 13,8 kV (de 230 kV); 
G3 13,8 kV (de 230 kV); 
G4 13,8 kV (de 230 kV). 
Fechar ou manter fechado o disjuntor: 
• do módulo de interligação de barras de 230 kV, exceto quando esse estiver substituindo o disjuntor 
de um equipamento ou de uma linha de transmissão. 
Cabe ao Agente CPFL-T, informar ao COSR-S quando a configuração de preparação da Instalação não estiver 
atendida para o início da recomposição, independentemente de o equipamento ser próprio ou de outros 
agentes. Nesse caso, o COSR -S fará contato com os agentes envolvidos  para identificar o motivo do não -
atendimento e , após confirmação do Agente CPFL-T de que os barramentos estão com a configuração 
atendida, o COSR -S coordenar á os procedimentos para recomposição em função da configuração desta 
Instalação. 
5.2.2. Recomposição Fluente da Instalação 
A Instalação faz parte da recomposição da Área Itaúba. O Agente Operador deve adotar os procedimentos a 
seguir para recomposição fluente: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 
CPFL-T 
Partir uma unidade geradora e fechar , 
energizando o barramento 230 kV, o disjuntor, 
e controlar a frequência em 60 Hz. 
Tensão terminal em 13,1 kV.  
1.1 
Energizar a LT 230 kV Dona Francisca / Usina 
Hidrelétrica Itaúba C1, enviando tensão para a 
SE Dona Francisca. 
 
1.2 Partir a segunda unidade geradora, sincronizá-
la e controlar a frequência em 60 Hz. 
Tensão terminal em 13,1 kV. 
1.2.1 
Energizar a LT 230 kV Polo Petroquímico / 
Usina Hidrelétrica Itaúba, enviando tensão 
para a SE Polo Petroquímico. 
Estar 2 unidades geradoras 
sincronizadas na UHE Itaúba. 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Itaúba IO-OI.S.UITA 28 3.7.5.2. 03/09/2024 
 
Referência:  7 / 11 
 
Passo Executor Procedimentos Condições ou Limites Associados 
1.2 
Elevar a tensão terminal das duas unidades 
geradoras. 
Tensão terminal em 13,3 kV. 
 
Após fluxo de potência ativa superior 
a 75 MW na LT 230 kV Polo 
Petroquímico / Usina Hidrelétrica 
Itaúba. 
1.3 
Partir a terceira unidade geradora, sincronizá-
la e controlar a frequência em 60 Hz. 
Tensão terminal em 13,1 kV. 
Ou 
Tensão terminal em 13,3 kV , se a  
fluxo de potência ativa na LT 230 kV 
Polo Petroquímico / Usina 
Hidrelétrica Itaúba for superior a 
75 MW.  
2 
CPFL-T 
Receber tensão da UHE Dona Francisca, pela 
LT 230 kV Dona Francisca / Usina Hidrelétrica 
Itaúba C2, e fechar, em anel, o disjuntor. 
Após 3 (três) unidades geradoras 
sincronizadas na UHE Itaúba. 
2.1 
Energizar a LT 230 kV Candelária 2 / Usina 
Hidrelétrica Itaúba, enviando tensão para a SE 
Candelária 2. 
Após fluxo de potência ativa superior 
a 50 MW na LT 230 kV Polo 
Petroquímico / Usin a Hidrelétrica 
Itaúba e 3 unidades geradoras 
sincronizadas na UHE Itaúba. 
2.2 
Energizar a LT 230 kV Santa Cruz 1 / Usina 
Hidrelétrica Itaúba, enviando tensão para a SE 
SE Santa Cruz 1. 
Após geração de potência ativa na 
UHE Itaúba superior a 120 MW. 
2.3 
Energizar a LT 230 kV Usina Hidrelétrica Itaúba 
/ Usina Hidrelétrica Passo Real, enviando 
tensão para a UHE Passo Real. 
Após geração de potência ativa na 
UHE Itaúba superior a 170 MW. 
3 CPFL-T 
Partir a quarta unidade geradora, caso es sa 
estivesse operando antes do desligamento, 
sincronizá-la e controlar a frequência em 
60 Hz. 
Tensão terminal em 13,1 kV. 
Ou 
Tensão terminal em 13,3 kV, se a 
fluxo de potência ativa na LT 230 kV  
Polo Petroquímico / Usina 
Hidrelétrica Itaúba for superior a 
75 MW.  
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Itaúba IO-OI.S.UITA 28 3.7.5.2. 03/09/2024 
 
Referência:  8 / 11 
 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador  da Instalação deve realizar os procedimentos do Subitem 5.2.2, enquanto não 
houver intervenção do COSR -S no processo de recomposição ou interrupção da autonomia deste na 
recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR-S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras ou na IO-RR.S.ITB.  
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, o 
Agente Operador  deve preparar a Instalação conforme Subitem 5.2 .1, sem necessidade de 
autorização do ONS. 
5.4.1.2. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador  
deve recompor a Instalação conforme Subitem 5.2.2, sem necessidade de autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., o Agente Operador  
deve recompor a Instalação conforme Subitem 6.2.2, sem necessidade de autorização do ONS.    
6. MANOBRAS DE UNIDADES GERADORAS, DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desligamento programado ou de urgência de unidades geradoras, e para 
desenergização programada ou de urgência, de linhas de transmissão ou de equipamentos, só podem 
ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, ou sincronismo de 
unidades geradoras, após desligamentos programados, de urgência ou de emergência, só podem ser 
efetuados com controle do COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, ou para sincronismo de unidades geradoras,  após desligamento automático sem 
atuação de proteção que impeça o retorno do equipamento, só podem ser executados com autonomia 
pelo Agente Operador da Instalação quando estiverem explicitados e estiverem atendidas as condições 
do Subitem 6.2.2. desta Instrução de Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir autonomia, 
a energização deve ser executada com controle do COSR -S, conforme Instrução de Operação de 
Preparação para Manobras da respectiva área elétrica. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Itaúba IO-OI.S.UITA 28 3.7.5.2. 03/09/2024 
 
Referência:  9 / 11 
 
6.1.4. Antes do fechamento de qualquer disjuntor, o Agente Operador deve verificar se existe tensão de 
retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pel o Agente Operador quando estiver 
especificado nesta Instrução de Operação e estiverem atendidas as condições do Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou, quando permitida, em sentido inverso, pode ser feita com 
autonomia pela operação do Agente, conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica.  
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o operador da Instalação deve fechá -lo em anel desde que tenha autonomia para tal, 
adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.1.9. A partida de unidades geradoras deve seguir critérios próprios do Agente. O sincronismo à Rede de 
Operação só pode ser executado com autonomia pela operação da Usina quando estiver explicitado e 
estiverem atendidas as condições do Subitem 6.2.2. A tomada de carga deve ser realizada com controle 
do COSR-S. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESLIGAMENTO DE UNIDADES GERADORAS E DESENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E 
DE EQUIPAMENTOS 
O desligamento de unidades geradoras e a desenergização de linhas de transmissão ou de 
equipamentos, pertencentes à Rede de Operação, é sempre controlado pelo COSR-S. 
6.2.2. SINCRONISMO DE UNIDADES GERADORAS E ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE 
EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de unidades geradoras, de equipamentos ou de linhas de transmissão. 
O Agente Operador  da Instalação deve identificar os desligamentos automáticos observando na 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Itaúba IO-OI.S.UITA 28 3.7.5.2. 03/09/2024 
 
Referência:  10 / 11 
 
Instalação as demais unidades geradoras, linhas de transmissão  e equipamentos em operação , 
conforme explicitado nas condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Linha de Transmissão / 
Unidade Geradora Procedimentos Condições ou Limites Associados 
LT 230 kV Dona Francisca / 
Usina Hidrelétrica Itaúba 
C1 
Sentido Normal: UHE Itaúba envia tensão para SE Dona Francisca  
Energizar a LT 230 kV Dona Francisca 
/ UHE Itaúba C1.  
Como primeiro ou segundo circuito: 
• VUITA-230 ≤ 242 kV; e 
• pelo menos uma UG (gerador 
ou compensador síncrono) na 
UHE Itaúba. 
Antes de energizar cada LT, verificar 
fluxo de potência ativa nos circuitos da 
LT 230 kV Dona Francisca / UHE Itaúba 
que já foram energizados. 
Sentido Inverso: UHE Itaúba recebe tensão da SE Dona Francisca  
Ligar, em anel, a LT 230  kV Dona 
Francisca / UHE Itaúba C1. 
• ∆δ ≤ 10°. 
LT 230 kV Dona Francisca / 
Usina Hidrelétrica Itaúba 
C2  
Sentido Normal: UHE Itaúba recebe tensão da SE Dona Francisca  
Ligar, em anel, a LT 230  kV Dona 
Francisca / UHE Itaúba C2.  
• ∆δ ≤ 10°. 
Sentido Inverso: UHE Itaúba envia tensão para a SE Dona Francisca  
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2RS. 
LT 230 kV Candelária 2 / 
Usina Hidrelétrica Itaúba 
Sentido Normal: UHE Itaúba envia tensão para a SE Candelária 2  
Energizar a LT 230  kV Candelária 2  / 
UHE Itaúba. 
• VUITA-230 ≤ 242 kV; 
• pelo menos três UGs (gerador 
ou compensador síncrono) na 
UHE Itaúba; 
• PLT 230 kV PPE / UITA ≥ 50 MW; e 
• fluxo de potência ativa nos 
dois circuitos da LT 230 kV 
Dona Francisca / UHE Itaúba. 
Sentido Inverso: UHE Itaúba recebe tensão da SE Candelária 2  
Ligar, em anel, a LT 230  kV 
Candelária 2 / UHE Itaúba. 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Itaúba IO-OI.S.UITA 28 3.7.5.2. 03/09/2024 
 
Referência:  11 / 11 
 
Linha de Transmissão / 
Unidade Geradora Procedimentos Condições ou Limites Associados 
LT 230 kV Polo 
Petroquímico / Usina 
Hidrelétrica Itaúba  
Sentido Normal: UHE Itaúba envia tensão para SE Polo Petroquímico  
Energizar a  LT 230 kV P olo 
Petroquímico / UHE Itaúba. • VUITA-230 ≤ 242 kV; e 
• fluxo de potência ativa nos 
dois circuitos da LT 230 kV 
Dona Francisca / UHE Itaúba; 
ou 
pelo menos duas UGs 
(gerador ou compensador 
síncrono) na UHE Itaúba. 
Sentido Inverso: UHE Itaúba recebe tensão da SE Polo Petroquímico  
Ligar, em anel, a LT 230  kV P olo 
Petroquímico / UHE Itaúba. 
 
LT 230 kV Santa Cruz 1 / 
Usina Hidrelétrica Itaúba 
Sentido Normal: UHE Itaúba envia tensão para a SE Santa Cruz 1  
Energizar a LT 230 kV Santa Cruz 1 / 
UHE Itaúba. 
• VUITA-230 ≤ 242 kV; 
• pelo menos três UGs (gerador 
ou compensador síncrono) na 
UHE Itaúba; e 
• PLT 230 kV PPE / UITA ≥ 120 MW. 
Sentido Inverso: UHE Itaúba recebe tensão da SE Santa Cruz 1  
Ligar, em anel, a LT 230 kV Santa Cruz 
1 / UHE Itaúba. 
 
LT 230 kV Usina 
Hidrelétrica Itaúba / Usina 
Hidrelétrica Passo Real 
Sentido Normal: UHE Itaúba envia tensão para a UHE Passo Real  
Energizar a  LT 230 kV UHE Itaúba / 
UHE Passo Real. 
• VUITA-230 ≤ 242 kV; 
• pelo menos três UGs (gerador 
ou compensador síncrono) na 
UHE Itaúba; e 
• PUITA ≥ 170 MW. 
Sentido Inverso: UHE Itaúba recebe tensão da UHE Passo Real  
Ligar, em anel, a LT 230  kV UHE 
Itaúba / UHE Passo Real. 
 
Unidade Geradora G1, G2, 
G3 ou G4 
13,8 kV 
Partir e sincronizar a unidade 
geradora. 
• 13,1 ≤ VUITA-13,8 ≤ 14,5 kV; e 
• elevar a geração da unidade 
geradora, após autorização do 
COSR-S. 
7. NOTAS IMPORTANTES 
Não se aplica. 
