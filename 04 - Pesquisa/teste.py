#streamlit run teste.py
import os
import pandas as pd
import nltk
from nltk.tokenize import word_tokenize
import unicodedata
import re
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import streamlit as st
from sentence_transformers import SentenceTransformer
from gensim.models import Word2Vec
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
import seaborn as sns

rcParams['font.family'] = 'DejaVu Sans'
#rcParams['font.family'] = 'Liberation Sans'  # Outra alternativa para fontes unicode
rcParams['axes.unicode_minus'] = False  # Resolve o problema com o símbolo de "menos" (-)


labels_map = {
    'comando': ['fechamento', 'abertura', 'religamento manual', 'religamento automatico', 'religamento', 
		   'sentido normal', 'sentido inverso', 'envio tensao', 'recebimento tensao', 'solicitar',
		   'impedimento', 'manobra', 'desenergizacao', 'energizacao', 'ligar', 'desligar', 'energizar', 
		   'desenergizar', 'operacao normal', 'condição normal', 'fechados', 'abertos', 'regulacao tensao',
		   'controle tensao', 'modo manual', 'modo automatico', 'abrir', 'fechar', 'manter aberto', 
		   'manter fechado', 'manter abertos', 'manter fechados', 'receber tensao', 'enviar tensao', 
		   'recebendo tensao', 'enviando tensao', 'intervencao', 'fluxo potencia ativa', 'tensao retorno', 
		   'fecha lo', 'abertura manual', 'abertura automatica', 'anel', 'paralelo', 'aberto', 'fechado', 
		   'desligamento automatico', 'desligamentos automaticos', 'esquema', 'esquema especial', 
		   'recebe tensao', 'envia tensao', 'energizando', 'substituir', 'interligando', 'verificar',
		   'energizado', 'desenergizado', 'sentido unico', 'desligados', 'ligados', 'exceto', 'movimentacao', 
		   'manter desligado', 'manter desligados', 'manter ligado', 'manter ligados', 'possibilitar', 'respeitar', 
		   'interligar', 'manobras', 'elevar', 'reduzir', 'diminuir', 'aumentar', 'interromper', 'cortar', 'remanejar',
           'bloquear', 'desbloquear', 'atuar', 'desatuar', 'ligar anel','sentido inverso', 'controlar',
            ],
    'agente': ['ons',  'cnos', 'cosr s', 'cosr se', 'cosr ne', 'cosr nco', 'copel get'],
    'recomposicao': ['recomposicao', 'recompor', 'desligamento parcial', 'desligamentos parciais', 
				'desligamento total', 'desligamentos totais', 'recomposicao fluente', 'desligamento total ou parcial',
				'restabelecimento', 
            ],
    'equipamento': ['linha transmissao', 'linhas transmissao', 'equipamento', 'equipamentos', 'comutador', 
			   'comutadores', 'ltc', 'ltcs', 'modulo interligacao barras', 'modulo interligador barras', 
			   'rede operacao', 'barramento', 'barra', 'barras', 'transformador', 'transformadores', 'disjuntor', 
               'disjuntores', 'reator', 'reatores', 'seccionadora', 'gerador', 'usina', 'capacitor', 'capacitores', 
               'compensador', 'compensadores', 'geradoras', 'geradores' 'seccionadoras', 'conjunto', 'conjuntos', 
               'unidade geradora', 'unidades geradoras', 'conversores', 'conversoras',
            ]
}

### Bloco para montar o dataset com os TXT
def montar_dataset():    
    manobra = True
    documentos = []
    for file in os.listdir('.'):
        filename = os.fsdecode(file)
        if filename.endswith('.txt'):
            conteudo = open(filename, "r", encoding="utf-8").read()
            manobra = filename.startswith('IO')
            doc = {"MANOBRAS": manobra, "REGIAO":filename.split('.')[1] ,"LOCAL":filename.split('.')[2].split('_')[0], "TIPO":filename.split('.')[0], "NOME": filename[:-4], "CONTEUDO": conteudo}
            documentos.append(doc)
    
    df = pd.DataFrame(documentos)
    
    ### Bloco para normalizar o conteudo dos documentos
    
    stopwords = nltk.corpus.stopwords.words('portuguese')
    
    def remove_acentos(input_str):
        nfkd_form = unicodedata.normalize('NFKD', input_str)
        return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])
    
    stopwords = [remove_acentos(palavra) for palavra in stopwords]
    
    def normaliza_texto(txt):
        return ' '.join([word for word in word_tokenize(str.lower(remove_acentos(txt))) if word not in stopwords and word.isalpha()])
    
    df['CONTEUDO_NORMALIZADO'] = df.apply(lambda linha: normaliza_texto(str(linha['CONTEUDO'])), axis = 1)
    
    ### Bloco da pré anotação do conteúdo que foi normalizado
    
    def annotate_text(text, labels_map):
        annotations = []
    
        # Para cada label e suas palavras associadas
        for label, palavras in labels_map.items():
            for palavra in palavras:
                    start_offset = text.find(palavra)
                    while start_offset != -1:  # Encontrar todas as ocorrências da palavra
                        end_offset = start_offset + len(palavra)
                        annotations.append([start_offset, end_offset, label])
                        start_offset = text.find(palavra, start_offset + 1)  # Procurar por mais ocorrências
        return annotations
        
    lista_labels = []
    
    for doc in df['CONTEUDO_NORMALIZADO']:
        annotations = annotate_text(doc, labels_map)
        result = {
                    'text': doc,
                    'labels': annotations
                }
        lista_labels.append(result)
    print('Anotações geradas com sucesso!')
    
    df['LABELS'] = pd.DataFrame(lista_labels)['labels']
    
    ### Bloco para substituir as labels pelos termos no texto
    
    def replace_terms_with_labels(document, label_to_terms):
        term_to_label = {}
        for label, terms in label_to_terms.items():
            for term in terms:
                term_to_label[term] = label  # Mapeamento direto do termo para label
    
        # Função para trocar os termos pelos labels
        def replace_match(match):
            word = match.group(0)  # Pegar a palavra que correspondeu
            #print(f"Termo sendo trocado: {word}")
            #if word not in term_to_label:
            #    print(f"Termo não encontrado para troca: {word}")
            return term_to_label.get(word, word)
        
        result = re.sub(r'\b\w+\b', replace_match, document)
        return result
    
    documents = [replace_terms_with_labels(doc, labels_map) for doc in df['CONTEUDO_NORMALIZADO'].tolist()]
    print(f'Quantidade de documentos: {len(documents)}')
    df['CONTEUDO_LABELIZADO'] = pd.DataFrame(documents)
    
    return df

### Bloco que faz a busca dos top 10 documentos similares à query

def encode_sbert(doc):
    return st.session_state.bert_model.encode([doc])[0]

def fit_and_transform_tfidf(corpus, query):
    vectorizer = TfidfVectorizer()
    tfidf_matrix = vectorizer.fit_transform(corpus)
    query_vector = vectorizer.transform([query])
    return tfidf_matrix, query_vector, vectorizer

def preprocess_text(text):
    return text.lower().split()

def encode_word2vec(text, model):
    tokens = preprocess_text(text)
    word_vectors = []
    for word in tokens:
        if word in model.wv:
            word_vectors.append(model.wv[word])
    if len(word_vectors) == 0:
        return np.zeros(model.vector_size)
    return np.mean(word_vectors, axis=0)

def jaccard_similarity(doc1, doc2):
    set1 = set(doc1.split())
    set2 = set(doc2.split())
    intersection = set1.intersection(set2)
    union = set1.union(set2)
    return len(intersection) / len(union)
   
def get_top_10_similar_documents(query, selected_df, method, model=None):
    if method == "Word2Vec":
        query_vector = encode_word2vec(query, st.session_state.word2vec_model)
        selected_df['doc_vector'] = selected_df['document'].apply(lambda x: encode_word2vec(x, st.session_state.word2vec_model))
        selected_df['similarity'] = selected_df['doc_vector'].apply(lambda x: cosine_similarity([query_vector], [x])[0][0])
    
    elif method == "TF-IDF":
        tfidf_matrix, query_vector, vectorizer = fit_and_transform_tfidf(selected_df['document'], query)
        cosine_similarities = cosine_similarity(query_vector, tfidf_matrix)
        selected_df['similarity'] = cosine_similarities.flatten()
    
    elif method == "Jaccard":
        selected_df['similarity'] = selected_df['document'].apply(lambda x: jaccard_similarity(x, query))
    
    # Retornando os 10 documentos mais similares
    df_sorted = selected_df.sort_values(by='similarity', ascending=False).head(10)
    return df_sorted

def plot_similarity_bars(dataset_name, word2vec_results, tfidf_results, jaccard_results):
    plt.figure(figsize=(10, 6))
    
    plt.barh(word2vec_results['NOME'], word2vec_results['similarity'], color='skyblue', label='Word2Vec', alpha=0.6)
    
    plt.barh(tfidf_results['NOME'], tfidf_results['similarity'], color='orange', label='TF-IDF', alpha=0.6)
    
    plt.barh(jaccard_results['NOME'], jaccard_results['similarity'], color='red', label='Jaccard', alpha=0.6)
    
    plt.xlabel('Similaridade')
    plt.title(f'Similaridade Comparada: {dataset_name}')
    plt.legend()
    plt.tight_layout()
    #plt.show()
    st.pyplot(plt)
    
# Streamlit Interface
st.title("Sistema Buscador de Manobras")

# Initialize session state if it's not already present
if 'data_loaded' not in st.session_state:
    st.session_state.data_loaded = False
    st.session_state.df = None  # Placeholder for selected dataset
    st.session_state.method = None  # Placeholder for selected method
    st.session_state.query = ""  # Placeholder for query
    st.session_state.similarity = None  # Placeholder for similarity results
    st.session_state.sbert_vectors = None  # Placeholder for Sentence-BERT vectors
    st.session_state.tfidf_vectorizer = None  # Placeholder for TF-IDF vectorizer
    st.session_state.tfidf_matrix = None  # Placeholder for TF-IDF matrix
    st.session_state.bert_model = None # Placeholder for Bertimbau model
    st.session_state.word2vec_model = None
    st.session_state.word2vec_model_df1 = None
    st.session_state.word2vec_model_df2 = None
    st.session_state.word2vec_model_df3 = None
    
# Button to load the data
if st.button("Load Data"):
    st.session_state.df = montar_dataset()  # Store the data in session state
    st.session_state.data_loaded = True  # Mark data as loaded
    
    st.session_state.df1 = st.session_state.df.copy()
    st.session_state.df1.columns = ['document' if x=='CONTEUDO' else x for x in st.session_state.df1.columns]
    # Carregar o modelo word2vec do df1
    processed_documents = st.session_state.df1['document'].apply(preprocess_text)
    st.session_state.word2vec_model_df1 = Word2Vec(sentences=processed_documents, vector_size=100, window=5, min_count=1, workers=4)
    
    
    st.session_state.df2 = st.session_state.df.copy()
    st.session_state.df2.columns = ['document' if x=='CONTEUDO_NORMALIZADO' else x for x in st.session_state.df2.columns]
    # Carregar o modelo word2vec do df2
    processed_documents = st.session_state.df2['document'].apply(preprocess_text)
    st.session_state.word2vec_model_df2 = Word2Vec(sentences=processed_documents, vector_size=100, window=5, min_count=1, workers=4)
    
    st.session_state.df3 = st.session_state.df.copy()
    st.session_state.df3.columns = ['document' if x=='CONTEUDO_LABELIZADO' else x for x in st.session_state.df3.columns]
    # Carregar o modelo word2vec do df3
    processed_documents = st.session_state.df3['document'].apply(preprocess_text)
    st.session_state.word2vec_model_df3 = Word2Vec(sentences=processed_documents, vector_size=100, window=5, min_count=1, workers=4)
    
    #st.session_state.bert_model = SentenceTransformer('neuralmind/bert-large-portuguese-cased')
    
    st.write("Dados carregados com sucesso!")

if st.session_state.data_loaded:
    selected_dataset = st.selectbox("Escolha a base de dados:", ["Original", "Normalizado", "Lematizado"])
    
    selected_method = st.selectbox(
        "Escolha um método de busca:",
        #["TF-IDF", "💀 Sentence-BERT", "💀 Word2Vec", "Jaccard Similarity"]
        ["TF-IDF", "Word2Vec", "Jaccard Similarity"]
    )

    query = st.text_input("Pesquisar sobre:", st.session_state.query)
    st.session_state.method = selected_method
    st.session_state.query = query

    if query:
        if selected_dataset == "Original":
            selected_df = st.session_state.df1
            st.session_state.word2vec_model = st.session_state.word2vec_model_df1
        elif selected_dataset == "Normalizado":
            selected_df = st.session_state.df2
            st.session_state.word2vec_model = st.session_state.word2vec_model_df2
        else:
            selected_df = st.session_state.df3
            st.session_state.word2vec_model = st.session_state.word2vec_model_df3
            
        if selected_method == "TF-IDF":
            tfidf_matrix, query_vector, vectorizer = fit_and_transform_tfidf(selected_df['document'], query)
            cosine_similarities = cosine_similarity(query_vector, tfidf_matrix)
            selected_df['similarity'] = cosine_similarities.flatten()

        elif selected_method == "Sentence-BERT":
            if st.session_state.sbert_vectors is None:
                st.session_state.sbert_vectors = selected_df['document'].apply(encode_sbert).tolist()
            query_vector = encode_sbert(query)
            cosine_similarities = cosine_similarity([query_vector], st.session_state.sbert_vectors)
            selected_df['similarity'] = cosine_similarities.flatten()

        elif selected_method == "Word2Vec":
            query_vector = encode_word2vec(query, st.session_state.word2vec_model)
            selected_df['doc_vector'] = selected_df['document'].apply(lambda x: encode_word2vec(x, st.session_state.word2vec_model))
            selected_df['similarity'] = selected_df['doc_vector'].apply(lambda x: cosine_similarity([query_vector], [x])[0][0])

        elif selected_method == "Jaccard Similarity":
            selected_df['similarity'] = selected_df['document'].apply(lambda x: jaccard_similarity(x, query))

        df_sorted = selected_df.sort_values(by='similarity', ascending=False)
        
        st.subheader("Documentos mais similares:")
        for idx, row in df_sorted.head(10).iterrows():
            st.write(f"**Manobra**: {row['MANOBRAS']} --- **Nome**: {row['NOME']}  --- **Similaridade**: {row['similarity']:.4f}")
            st.write(f"**Região**: {row['REGIAO']} --- **Local**: {row['LOCAL']} --- **Tipo**: {row['TIPO']}")
            st.write("---")
        print(df_sorted.head(10)[['MANOBRAS','REGIAO','LOCAL','TIPO','NOME','similarity']])
        
        # Obtendo os 10 documentos mais similares para cada dataset e método
        top_10_word2vec_dataset1 = get_top_10_similar_documents(query, st.session_state.df1, 'Word2Vec', model=st.session_state.word2vec_model_df1)
        top_10_word2vec_dataset2 = get_top_10_similar_documents(query, st.session_state.df2, 'Word2Vec', model=st.session_state.word2vec_model_df2)
        top_10_word2vec_dataset3 = get_top_10_similar_documents(query, st.session_state.df3, 'Word2Vec', model=st.session_state.word2vec_model_df3)

        top_10_tfidf_dataset1 = get_top_10_similar_documents(query, st.session_state.df1, 'TF-IDF')
        top_10_tfidf_dataset2 = get_top_10_similar_documents(query, st.session_state.df2, 'TF-IDF')
        top_10_tfidf_dataset3 = get_top_10_similar_documents(query, st.session_state.df3, 'TF-IDF')
        
        top_10_jaccard_dataset1 = get_top_10_similar_documents(query, st.session_state.df1, 'Jaccard')
        top_10_jaccard_dataset2 = get_top_10_similar_documents(query, st.session_state.df2, 'Jaccard')
        top_10_jaccard_dataset3 = get_top_10_similar_documents(query, st.session_state.df3, 'Jaccard')

        # Plotando os gráficos para cada dataset
        st.write(f'Aqui estão os resultados da similaridade dos 10 documentos mais relevantes com a query "{query}" encontrados com cada método de busca (Word2Vec, TF-IDF e Jaccard).')

        st.subheader('Documentos Originais')
        plot_similarity_bars('Originais', top_10_word2vec_dataset1, top_10_tfidf_dataset1, top_10_jaccard_dataset1)

        st.subheader('Documentos Normalizados')
        plot_similarity_bars('Normalizados', top_10_word2vec_dataset2, top_10_tfidf_dataset2, top_10_jaccard_dataset2)

        st.subheader('Documentos Lematizados')
        plot_similarity_bars('Lematizados', top_10_word2vec_dataset3, top_10_tfidf_dataset3, top_10_jaccard_dataset3)
