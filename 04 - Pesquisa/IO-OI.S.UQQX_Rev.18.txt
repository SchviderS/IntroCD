 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da UHE Quebra-Queixo 
 
 
Código Revisão Item Vigência 
IO-OI.S.UQQX 18 3.7.5.3. 02/08/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Adequação do Subitem 5.4.  
- Adequação à RT-OI.BR revisão 37, com destaque à inclusão dos Subitens 6.1.5. e 6.1.6.  
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S Energisa Soluções Celesc 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Quebra-Queixo IO-OI.S.UQQX 18 3.7.5.3. 02/08/2024 
 
Referência:  2 / 8 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  3 
3.1. Unidades Geradoras ...................................................................................................................... 3 
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL ................................ ....................  3 
4.1. Procedimentos Gerais ................................................................................................................... 3 
4.2. Procedimentos Específicos ............................................................................................................ 4 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  4 
5.1. Procedimentos Gerais ................................................................................................................... 4 
5.2. Procedimentos para Recomposição com Autonomia ................................................................... 5 
5.2.1. Preparação da Instalação para a Recomposição com Autonomia .............................. 5 
5.2.2. Recomposição com Autonomia após Desligamento Total da Instalação.................... 5 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 6 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 6 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 6 
5.4. Procedimentos Após Desligamento Parcial da Instalação ............................................................ 6 
5.4.1. Preparação da Instalação para a Recomposição Após Desligamento Parcial ............. 6 
5.4.2. Recomposição Após Desligamento Parcial da Instalação ........................................... 6 
6. MANOBRAS DE UNIDADES GERADORAS ................................ ................................ ........................  7 
6.1. Procedimentos Gerais ................................................................................................................... 7 
6.2. Procedimentos Específicos ............................................................................................................ 7 
6.2.1. Desligamento de Unidades Geradoras ........................................................................ 7 
6.2.2. Sincronismo de Unidades Geradoras .......................................................................... 7 
7. NOTAS IMPORTANTES ................................ ................................ ................................ .................. 8 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Quebra-Queixo IO-OI.S.UQQX 18 3.7.5.3. 02/08/2024 
 
Referência:  3 / 8 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da UHE Quebra-Queixo, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido no s Procedimentos de 
Rede.  
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da Instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR-S e a Instalação, de propriedade da Companhia Energética 
Chapecó, é realizada pela Energisa Soluções, agente responsável pela operação da Instalação, por 
intermédio do COT-ESOL. 
2.3. As unidades geradoras, equipamentos e linhas de transmissão desta Instalação fazem parte da Área 
230 kV de Santa Catarina. 
2.4. O COSR-S controla e supervisiona o despacho de geração desta Usina. 
2.5. Esta Usina: 
• está conectada na Rede de Distribuição; 
• é de autorestabelecimento; 
• não participa do Controle Automático da Geração – CAG. 
2.6. Os dados operacionais desta Usina estão descritos no Cadastro de Informações Operacionais da 
respectiva Área Elétrica. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. UNIDADES GERADORAS 
As unidades geradoras G1, G2 e G3 13,8 kV estão conectadas ao barramento de 138 kV  pelos 
transformadores TE -1, TE -2 e TE -3 13,8/138 kV , da UHE Quebra -Queixo, com todas as seccionadoras e 
disjuntores fechados, exceto a seccionadora de transferência de 138 kV. 
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. Os barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão executada 
com responsabilidade pelo Agente Operador da Instalação. 
Esgotados os recursos locais disponíveis, sendo necessário, o Agente deve acionar o COSR -S, que 
verificará a disponibilidade dos recursos sistêmicos. 
4.1.2. A Usina deve manter os valores de geração de acordo com os valores programados, constantes no 
Programa Diário de Operação – PDO. Para atendimento dos valores programados constantes no PDO, 
não é necessária a autorização prévia do COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Quebra-Queixo IO-OI.S.UQQX 18 3.7.5.3. 02/08/2024 
 
Referência:  4 / 8 
 
Qualquer alteração no valor de geração da Usina em relação ao valor de geração constante no PDO 
somente pode ser executada após autorização do COSR-S. 
As reprogramações de geração quando de necessidades sistêmicas serão executadas pela Usina 
quando solicitadas pelo COSR-S. 
Após reprogramação de geração solicitada pelo ONS, o Agente somente poderá alterar a geração da 
Usina com autorização do ONS, inclusive para adoção de valores contidos no PDO. 
4.1.3. Os desvios de geração da Usina em relação aos valores previstos no PDO ou em relação às 
reprogramações, devem ser controlados observando-se os valores máximos permitidos explicitados 
na Instrução de Operação IO-CG.BR.01 – Controle da Geração em Condição Normal. 
4.1.4. Quando não existir ou não estiver disponível a supervisão da Usina para o ONS, essa deve seguir as 
orientações para envio de dados conforme Rotina Operacional RO-AO.BR.08 – Apuração de Dados de 
Geração e de Intercâmbios. 
4.1.5. A Usina deve registrar e informar imediatamente os seguintes dados ao COSR-S: 
• movimentação de unidades geradoras  hidrelétricas (mudança de estado operativo  / 
disponibilidade); 
• restrições e ocorrências na usina ou na conexão elétrica que afetem a disponibilidade de 
geração, com o respectivo valor da restrição, contendo o horário de início e término e a 
descrição do evento; 
• demais informações sobre a operação de suas instalações, solicitadas pelo ONS. 
4.1.6. O controle de tensão, por meio da geração ou absorção de potência reativa das unidades geradoras 
da Usina é executado com autonomia pela operação do Agente  e deve ser realizado entre o Agente 
de Geração e o Agente de Distribuição. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
Não se aplica. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de um desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir, para atendimento às necessidades sistêmicas: 
• Desligamento Total da Usina:  caracterizado quando há ausência de tensão em todos os 
terminais de suas conexões ou ausência de fluxo de potência ativa nessas conexões. 
• Desligamento Parcial da Usina:  qualquer outra configuração que não se enquadre como 
desligamento total. 
5.1.2. Quando de um desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao 
COSR-S as informações a seguir: 
• horário da ocorrência; 
• configuração da subestação logo após a ocorrência; 
• configuração da instalação após ações realizadas com autonomia pela operação dessa. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Quebra-Queixo IO-OI.S.UQQX 18 3.7.5.3. 02/08/2024 
 
Referência:  5 / 8 
 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição no Subitem 5.2, sem necessidade de autorização prévia por porte do ONS. Caso o ONS 
intervenha no processo de recomposição, identificando a não aplicabilidade da recomposição fluente 
ou interromp endo a autonomia do Agente Operador da Instalação na recomposição, deve ser 
utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4. 
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO COM AUTONOMIA 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO COM AUTONOMIA 
No caso de desligamento total, o Agente Operador deve configurar os disjuntores das seguintes unidades 
geradoras, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• das unidades geradoras: 
G1 13,8 kV; 
G2 13,8 kV; 
G3 13,8 kV. 
Cabe ao Agente Energisa Soluções informar ao COSR-S quando a configuração de preparação da Instalação 
não estiver atendida para o início da recomposição, independentemente de o equipamento ser próprio ou 
de outros agentes. Nesse caso, o COSR -S fará contato com os agentes envolvidos para identificar o motivo 
do não-atendimento e, após confirmação do Agente Energisa Soluções de que os barramentos estão com a 
configuração atendida, o COSR -S coordenará os procedimentos para recomposição, caso necessário, em 
função da configuração desta Instalação. 
5.2.2. RECOMPOSIÇÃO COM AUTONOMIA APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar com autonomia os seguintes procedimentos para a 
recomposição: 
Passo Executor Procedimentos Condições ou Limites 
Associados 
1 
Energisa 
Soluções 
Partir uma das unidades geradoras, conforme 
instruções próprias, caso essa estivesse 
operando antes do desligamento, e sincronizá-la. 
Deverão ser tomadas as 
medidas necessárias para 
possibilitar o fechamento do 
sincronismo, gerar o mínimo 
necessário para manter a 
unidade geradora 
sincronizada e solicitar 
autorização ao COSR -S para 
elevar a geração. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Quebra-Queixo IO-OI.S.UQQX 18 3.7.5.3. 02/08/2024 
 
Referência:  6 / 8 
 
Passo Executor Procedimentos Condições ou Limites 
Associados 
1.1 
Energisa 
Soluções 
Partir a segunda unidade geradora, conforme 
instruções próprias, caso essa estivesse 
operando antes do desligamento, e sincronizá-la. 
Deverão ser tomadas as 
medidas necessárias para 
possibilitar o fechamento do 
sincronismo, gerar o mínimo 
necessário para manter a 
unidade geradora 
sincronizada e solicitar 
autorização ao COSR -S para 
elevar a geração. 
1.2 
Energisa 
Soluções 
Partir a terceira unidade geradora, conforme 
instruções próprias, caso essa estivesse 
operando antes do desligamento, e sincronizá-la. 
Deverão ser tomadas as 
medidas necessárias para 
possibilitar o fechamento do 
sincronismo, gerar o mínimo 
necessário para manter a 
unidade geradora 
sincronizada e solicitar 
autorização ao COSR -S para 
elevar a geração. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação  deve realizar os procedimentos do Subitem 5.2.2, enquanto não houver 
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia deste na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR-S. 
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
Para os desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
O Agente Operador deve recompor a Instalação conforme Subitem 6.2.2, sem necessidade de autorização 
do ONS.  
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, o Agente 
Operador deve informar ao COSR-S, para que a recomposição da Instalação seja executada com controle do 
COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Quebra-Queixo IO-OI.S.UQQX 18 3.7.5.3. 02/08/2024 
 
Referência:  7 / 8 
 
6. MANOBRAS DE UNIDADES GERADORAS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desligamento programado ou de urgência de unidades geradoras, e 
desenergização programada ou de urgência de equipamentos, só podem ser efetuados com controle 
do COSR-S. 
6.1.2. Os procedimentos para energização de equipamentos, ou sincronismo de unidades geradoras, após 
desligamentos programados, de urgência ou de emergência, só podem ser efetuados com controle 
do COSR-S. 
6.1.3. Os procedimentos para sincronismo de unidades geradoras, após desligamento automático sem 
atuação de proteção que impeça o retorno do equipamento, só podem ser executados com 
autonomia pelo Agente Operador da Instalação quando estiverem explicitados e estiverem atendidas 
as condições do Subitem 6.2.2. desta Instrução de Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.5. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S.  
6.1.6. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com 
controle do COSR-S. 
6.1.7. A partida de unidades geradoras deve seguir critérios próprios do Agente. O sincronismo à Rede de 
Distribuição deve ser realizado após tratativas entre a Usina e o Agente de Distribuição. A tomada de 
carga deve ser realizada com controle do COSR-S. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESLIGAMENTO DE UNIDADES GERADORAS 
O desligamento de unidades geradoras é sempre controlado pelo COSR-S. 
6.2.2. SINCRONISMO DE UNIDADES GERADORAS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de unidades geradoras ou de equipamentos. 
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais unidades geradoras e equipamentos em operação, conforme explicitado nas 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Quebra-Queixo IO-OI.S.UQQX 18 3.7.5.3. 02/08/2024 
 
Referência:  8 / 8 
 
condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Unidade Geradora Procedimentos Condições ou Limites Associados 
Unidade Geradora 
G1, G2 ou G3 13,8 kV 
Partir e sincronizar a unidade geradora. Conforme procedimentos internos do 
Agente.  
Gerar o mínimo necessário para manter 
a unidade geradora sincronizada. 
Elevar a geração da unidade geradora. Após autorização do COSR-S. 
7. NOTAS IMPORTANTES 
Não se aplica. 
