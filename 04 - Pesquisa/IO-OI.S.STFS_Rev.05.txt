 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para Operação da SE Foz do Iguaçu 60 Hz 
 
 
 
 
. 
 
MOTIVO DA REVISÃO 
Incorporação parcial da MOP/ONS 274-S/2024 - "Alteração dos Centros de Operação de Furnas - CTRR, CTRM 
e CTOS", com alterações ao longo do documento. 
LISTA DE DISTRIBUIÇÃO 
 
CNOS COSR-SE Furnas (CTOR) CTEEP  
 
 
Código Revisão Item Vigência 
IO-OI.S.STFS 05 3.7.1.4. 10/07/2024 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para Operação da SE Foz do 
Iguaçu 60 Hz IO-OI.S.STFS 05 3.7.1.4. 10/07/2024 
 
Referência: RT-ONS DPL 0383/2022, Rev.1 2/9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
ÍNDICE 
 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  4 
3.1. Barramento de 765 kV ................................................................................................................... 4 
3.2. Barramento de 500 kV ................................................................................................................... 4 
3.3. Alteração da Configuração do Barramento ................................................................................... 4 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 4 
4.1. Procedimentos Gerais ................................................................................................................... 4 
4.2. Procedimentos Específicos ............................................................................................................ 4 
4.2.1. Operação dos Reatores ............................................................................................... 4 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  5 
5.1. Procedimentos Gerais ................................................................................................................... 5 
5.2. Procedimentos para a recomposição fluente ............................................................................... 5 
5.3. Procedimentos após desligamento total da Instalação ................................................................ 5 
5.3.1. Preparação da Instalação para recomposição fluente ................................................ 5 
5.3.2. Recomposição Fluente da Instalação .......................................................................... 6 
5.4. Recomposição após desligamento total da Instalação ................................................................. 6 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 7 
6.1. Procedimentos Gerais ................................................................................................................... 7 
6.2. Procedimentos Específicos ............................................................................................................ 8 
6.2.1. Desenergização de linhas de transmissão e de Equipamentos ................................... 8 
6.2.2. Energização de linhas de transmissão e de Equipamentos ......................................... 8 
7. NOTAS IMPORTANTES ................................ ................................ ................................ .................. 9 
 
  
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para Operação da SE Foz do 
Iguaçu 60 Hz IO-OI.S.STFS 05 3.7.1.4. 10/07/2024 
 
Referência: RT-ONS DPL 0383/2022, Rev.1 3/9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE Foz do Iguaçu 60 Hz , definidos pelo ONS, responsável 
pela coordenação, supervisão e controle da Rede de Operação, conforme estabelecido no s Procedimentos 
de Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelos Agentes Operadores da Instalação, devendo fazer parte do manual 
de operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade 
das ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR -SE e a Instalação, no que se refere aos equipamentos de 
manobra na Instalação, é realizada conforme segue. 
Linha de Transmissão ou 
Equipamento Agente de Operação Agente Operador (*) Centro de Operação 
do Agente Operador  
LT 525 kV Cascavel Oeste / Foz 
do Iguaçu (**) COPEL GeT COPEL GeT 
COGT 
(COS da Copel GeT) 
LT 525 kV Foz do Iguaçu / Guaíra 
C1 (**) 
IE Ivaí 
(Disjuntor 9354) 
CTEEP 
(Disjuntor 9354) 
COT CTEEP 
Furnas 
(Disjuntor 9254) 
Furnas 
(Disjuntor 9254) 
CTOR 
LT 525 kV Foz do Iguaçu / Guaíra 
C2 (**) IE Ivaí CTEEP COT CTEEP 
Demais Equipamentos Furnas CTOR 
(*) Nos casos em que a linha de transmissão ou equipamento compartilhem equipamentos de manobra com 
mais de um agente, o agente operador responsável pela LT / equipamento é apresentado em negrito. 
(**) A LT 525 Foz do Iguaçu / Guaíra C1 e C2 e LT 525 kV Cascavel Oeste / Foz do Iguaçu, linha de interligação 
entre as áreas de atuação do COSR-S e do COSR-SE, pertence à área de atuação do COSR-S. 
2.3. Os equipamentos e linhas de transmissão desta Instalação  fazem parte da  Área Interligação 
Sul/Sudeste. 
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.4.1. A definição d a quantidade de tentativas de religamento manual de linha de transmissão ou de 
equipamento, bem como o intervalo entre elas , é de responsabilidade do Agente e devem estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica. 
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, es se deve definir a necessidade de tentativas adicionais e solicitar autorização para 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para Operação da SE Foz do 
Iguaçu 60 Hz IO-OI.S.STFS 05 3.7.1.4. 10/07/2024 
 
Referência: RT-ONS DPL 0383/2022, Rev.1 4/9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
religamento ao COSR-SE. Nessa oportunidade, o Agente também pode solicitar alteração no sentido 
normal para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -SE levará em consideração as condições operativas do 
sistema. 
2.5. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR-SE, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação , quando o Agente  tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 765 KV 
O barramento de 765 kV é do tipo “Disjuntor e Meio” e opera, em condições normais, com todos os 
disjuntores e seccionadoras fechados, exceto as seccionadoras de baipasse e aterramento que 
operam abertas. 
3.2. BARRAMENTO DE 500 KV 
O barramento de 500 kV é do tipo Disjuntor e Meio e opera, em condições normais, com todos os 
disjuntores fechados. 
3.3. ALTERAÇÃO DA CONFIGURAÇÃO DO BARRAMENTO 
A mudança de configuração do s barramentos de 765 kV  e de 500 kV desta Instalação é executada com 
controle COSR-SE. 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS  
Os barramentos de 765 kV e de 500 kV , pertencentes à Rede de Operação, t êm a sua regulação de tensão 
controlada pelo COSR-SE. 
As faixas de controle de tensão para esses barramentos estão estabelecidas nos Cadastros de Informações 
Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
4.2.1. OPERAÇÃO DOS REATORES 
A manobra dos reatores RTFIV1, RTFIV2 e RTFIV3 – 330 Mvar é executada sob controle do COSR-SE. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para Operação da SE Foz do 
Iguaçu 60 Hz IO-OI.S.STFS 05 3.7.1.4. 10/07/2024 
 
Referência: RT-ONS DPL 0383/2022, Rev.1 5/9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
As LT 765 kV Foz do Iguaçu / Ivaiporã C1 ou C2 ou C3 não podem operar sem o reator do terminal da SE Foz 
do Iguaçu, devido às sobretensões.  
As condições de operação das linhas de 765 kV sem reator encontram-se definidas na IO-OC.SSE. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de um desligamento da Instalação, a operação d essa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
 Desligamento total da Instalação: caracterizado quando não há tensão em todos os terminais de 
suas linhas de transmissão. 
 Desligamento parcial da Instalação: qualquer outra configuração que não se enquadre como 
desligamento total. 
5.1.2. Quando de um desligamento total ou parcial, os Agentes Operadores da Instalação devem fornecer ao 
COSR-SE as seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• Configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2, sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificand o a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia d os Agentes Operadores  da Instalação na 
recomposição, deve ser utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4. 
5.2. PROCEDIMENTOS PARA A RECOMPOSIÇÃO FLUENTE 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO  
5.3.1. PREPARAÇÃO DA INSTALAÇÃO PARA RECOMPOSIÇÃO FLUENTE  
No caso de um desligamento total , os Agentes Operadores devem configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Equipamentos Condição 
Disjuntores Abrir ou manter aberto todos os disjuntores da SE. 
 
Cabe ao Agente Furnas informar ao COSR-SE quando a configuração da Instalação não estiver atendida para 
o início da recomposição, independentemente d e o equipamento ser próprio ou de outros Agentes. Nesse 
caso, o COSR -SE fará contato com os agentes envolvidos  para identificar o motivo do não -atendimento e, 
após confirmação do Agente Furnas de que os barramentos estão com a configuração  atendida, o COSR-SE 
coordenará os procedimentos para recomposição, caso necessário,  em função da configuração d esta 
Instalação. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para Operação da SE Foz do 
Iguaçu 60 Hz IO-OI.S.STFS 05 3.7.1.4. 10/07/2024 
 
Referência: RT-ONS DPL 0383/2022, Rev.1 6/9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
5.3.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO  
A SE Foz do Iguaçu 60 Hz faz parte da recomposição fluente do tronco de 765 kV a partir da SE Itaipu até a SE 
Itaberá. Os Agentes Operadores devem adotar os procedimentos a seguir para a recomposição fluente: 
Passo Executor Procedimentos Condições ou Limites 
Associados 
1 Furnas 
(CTOR) 
Após informação do envio de tensão pela LT 13,8 kV 
Itaipu/Foz do Iguaçu 60 Hz, normalizar as cargas do 
serviço auxiliar. 
 
2 Furnas 
(CTOR) 
Recebendo tensão da UHE Itaipu 60 Hz, por um dos 
circuitos da LT 500  kV Itaipu 60 Hz / Foz do Iguaçu, 
energizar o barramento de 500 kV. 
 
Em seguida energizar pelo lado de 500 kV, um dos 
bancos de autotransformadores, preferencialmente o 
banco correspondente ao circuito ligado. 
 
Normalizar o lado de 765 kV energizando a barra A. 
 
Obs: Durante o processo de recomposição os bancos 
AT01 e AT02 não devem alimentar os serviços auxiliares 
da SE. 
Tensão na SE Foz do Iguaçu 
60 Hz igual ou inferior a 550 
kV. 
3 Furnas 
(CTOR) 
Energizar um dos circuitos da LT 765  kV Foz do Iguaçu / 
Ivaiporã, preferencialmente o C3 (para facilitar a 
energização do banco de autotransformadores 
765/345 kV – AT05 ou AT06 da SE Tijuco Preto), enviando 
tensão para a SE Ivaiporã. 
- Reatores da linha ligados na 
SE Ivaiporã e SE Foz do 
Iguaçu, e 
- Tensão na SE Foz 60 Hz igual 
ou inferior a 700 kV, e 
- Após confirmação da SE 
Ivaiporã que o BCS está 
baipassado. 
 
Os demais equipamentos desta Instalação são restabelecidos sob controle do COSR -SE, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras ou na IO-RR.SSE. 
5.4. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
Os Agentes Operadores da Instalação deve m realizar os procedimentos do Subitem 5.2.2, enquanto não 
houver intervenção do COSR -SE no processo de recomposição ou interrupção da autonomia destes na 
recomposição. 
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -SE, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras ou na IO-RR.SSE. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para Operação da SE Foz do 
Iguaçu 60 Hz IO-OI.S.STFS 05 3.7.1.4. 10/07/2024 
 
Referência: RT-ONS DPL 0383/2022, Rev.1 7/9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos só podem ser efetuados com controle do COSR-SE. 
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, após 
desligamentos programados, de urgência ou de emergência, só podem ser efetuados com controle do 
COSR-SE. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou  de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pel os Agentes Operadores da Instalação 
quando estiverem explicitados e estiverem atendidas as condições do Subitem 6.2.2 desta Instrução 
de Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir autonomia, 
a energização deve ser executada com controle do COSR -SE, conforme Instrução de Operação de 
Preparação para Manobras da respectiva Área Elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, os Agentes Operadores da Instalação devem verificar  se 
existe tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pelos Agentes Operadores da Instalação 
quando estiver especificado nesta Instrução de Operação e estiverem atendidas as condições do 
subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-SE. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou , quando permitida, em sentido  inverso, pode ser feita com 
autonomia pela operação do Agente, conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-SE estão definidos na Instrução de Operação de Preparação para Manobras da respectiva 
área elétrica. 
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização. 
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções,  
são de responsabilidade do Agente. 
6.1.7. Em caso de abertura apenas do terminal  / lado pelo qual a linha de transmissão ou transformador é 
energizado, o operador da Instalação deve fechá -lo em anel desde que tenha autonomia  para tal , 
adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para Operação da SE Foz do 
Iguaçu 60 Hz IO-OI.S.STFS 05 3.7.1.4. 10/07/2024 
 
Referência: RT-ONS DPL 0383/2022, Rev.1 8/9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
A desenergização de linhas de transmissão e de equipamentos, pertencentes à Rede de Operação, é sempre 
controlada pelo COSR-SE. 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-SE. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pel os Agentes Operadores da Instalação, 
após desligamento automático simples de equipamentos ou de linhas de transmissão. 
Os Agentes Operadores  da Instalação deve m identificar o desligamento automático simples , 
observando na Instalação todas as demais linhas de transmissão e equipamentos em operação. 
Para desligamentos parciais, proceder conforme Subitem 5.4. 
 
Equipamento / Linha de 
transmissão Procedimentos Condições ou limites associados 
LT 765 kV Foz do 
Iguaçu / Ivaiporã C 1 
ou C2 ou C3 
Sentido Normal: SE Foz do Iguaçu 60 Hz envia tensão para a SE Ivaiporã. 
A energização em sentido normal é controlada pelo COSR-SE, conforme IO -
PM.SSE. 
Sentido Inverso: SE Foz do Iguaçu 60 Hz recebe tensão da SE Ivaiporã. 
A energização em sentido inverso é controlada pelo COSR -SE, conforme IO -
PM.SSE. 
LT 500 kV Itaipu 60 Hz 
/ Foz do Iguaçu C1 ou 
C2 ou C3 ou C4 
Sentido Normal: SE Foz do Iguaçu 60 Hz recebe tensão da UHE Itaipu 60 Hz. 
Receber tensão da UHE Itaipu 60 Hz e 
fechar em anel. 
Defasagem angular menor ou igual a 
30°. 
Sentido Inverso: SE Foz do Iguaçu 60 Hz envia tensão da UHE Itaipu 60 Hz. 
A energização em sentido inverso é controlada pelo COSR-SE, conforme IO -
PM.SSE. 
LT 525 kV Foz do 
Iguaçu / Guaíra C1 ou 
C2 
Sentido Normal: SE Foz do Iguaçu 60 Hz envia tensão para a SE Guaíra. 
A energização em sentido normal é controlada pelo COSR-SE, conforme IO-
PM.SSE. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para Operação da SE Foz do 
Iguaçu 60 Hz IO-OI.S.STFS 05 3.7.1.4. 10/07/2024 
 
Referência: RT-ONS DPL 0383/2022, Rev.1 9/9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
Equipamento / Linha de 
transmissão Procedimentos Condições ou limites associados 
Sentido Inverso: SE Foz do Iguaçu 60 Hz recebe tensão da SE Guaíra. 
A energização em sentido inverso é controlada pelo COSR -SE, conforme IO -
PM.SSE. 
LT 525 kV Cascavel 
Oeste / Foz do Iguaçu 
Sentido Normal: SE Foz do Iguaçu 60 Hz envia tensão para a SE Cascavel Oeste. 
A energização  em sentido normal  é controlada pelo COSR -SE, conforme IO -
PM.SSE. 
Sentido Inverso: SE Foz do Iguaçu 60 Hz recebe tensão da SE Cascavel Oeste. 
A energização em sentido inverso é controlada pelo COSR -S, conforme IO -
PM.SSE. 
Banco de 
Autotransformadores 
AT1, AT2, AT3, AT4 ou 
AT5- 765 / 525 kV de 
1650 MVA 
Sentido Normal: A partir do lado de 500 kV. 
Esta energização é controlada pelo COSR-SE, conforme IO-PM.SSE. 
Sentido Inverso: A partir do lado de 765 kV. 
Esta energização é controlada pelo COSR-SE, conforme IO-PM.SSE. 
 
7. NOTAS IMPORTANTES 
Não se aplica. 
