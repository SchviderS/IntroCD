REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
ANEXO: RELAÇÃO DOS CONTATOS 
OPERACIONAIS DO ONS E DA CAMMESA 
ANEXO:  RELACIÓN DE LOS CONTACTOS 
OPERATIVOS DEL ONS Y DE LA CAMMESA
Módulo / 
Módulo
3
Revisão / 
Revisión
4
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 1 de 6
CAMMESA
INFORMACIÓN GENERAL
Teléfono: (0341) 4958 300
Fax: (0341) 4958 375
Dirección: Ruta 34 (S) Km. 3,5 (S2121GZA) – Pérez – Santa Fe - Argentina Web: 
http://portalweb.cammesa.com/
GERENCIA DE OPERACIONES 
Gerente de Operaciones 
Ing. Jorge Siryi 
Teléfono: (0341) 4958317 
Celular: 3416 659081 
e-mail: jorgesiryi@cammesa.com.ar  
Coordinador Técnico de Operaciones 
Ing. Miguel Gette 
Teléfono: (0341) 4958 332 
Celular 3412 574727 
e-mail: miguelgette@cammesa.com.ar  
Responsable del Centro de Control 
Ing. Marcelo Cogliati 
Teléfono: (0341) 4958 300 int. 458 
Celular: 3416 624075 
e-mail: marcelocogliati@cammesa.com.ar  
Responsable de Procedimientos de la Operación 
Ing. Rubén Albachiaro 
Teléfono: (0341) 4958 300 int. 462 
Celular: 3416 068842 
e-mail: rubenalbachiaro@cammesa.com.ar  
Responsable de Ingeniería de Operación 
Ing. Fabián Ferrer Petit 
Teléfono: (0341) 4958325 
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
ANEXO: RELAÇÃO DOS CONTATOS 
OPERACIONAIS DO ONS E DA CAMMESA 
ANEXO:  RELACIÓN DE LOS CONTACTOS 
OPERATIVOS DEL ONS Y DE LA CAMMESA
Módulo / 
Módulo
3
Revisão / 
Revisión
4
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 2 de 6
Celular: 341 2535595 
e-mail: fabianferrer@cammesa.com.ar   
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
ANEXO: RELAÇÃO DOS CONTATOS 
OPERACIONAIS DO ONS E DA CAMMESA 
ANEXO:  RELACIÓN DE LOS CONTACTOS 
OPERATIVOS DEL ONS Y DE LA CAMMESA
Módulo / 
Módulo
3
Revisão / 
Revisión
4
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 3 de 6
Ingeniería de Operación 
Ing. Rosana Seggiaro 
Teléfono: (0341) 4958 382 
e-mail: rosanaseggiaro@cammesa.com.ar 
 
Ing. Joaquín Beitía 
Teléfono: (0341) 4958300 int. 455 
e-mail: joaquinbeitia@cammesa.com.ar  
Ing. María Belén Cristiani
email: mariacristiani@cammesa.com.ar 
Ing. Carlos Scacheri
e-mail: carlosscacheri@cammesa.com.ar 
Téc. Gustavo Tamburelli Teléfono: (0341) 4958 382 
e-mail: gustavotamburelli@cammesa.com.ar  
Téc. Claudio Calvagna 
Teléfono: (0341) 4958300 int. 434 
e-mail: claudiocalvagna@cammesa.com.ar 
Responsable de Aplicaciones de Tiempo Real
Ing. Pablo Brignone
Celular 341 5760029
e-mail: pablobrignone@cammesa.com.ar
Aplicaciones en Tiempo Real 
Ing. Paula Rosenzvit
e-mail: paularosenzvit@cammesa.com.ar 
Ing. Marco Vitale 
e-mail: marcovitale@cammesa.com.ar  
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
ANEXO: RELAÇÃO DOS CONTATOS 
OPERACIONAIS DO ONS E DA CAMMESA 
ANEXO:  RELACIÓN DE LOS CONTACTOS 
OPERATIVOS DEL ONS Y DE LA CAMMESA
Módulo / 
Módulo
3
Revisão / 
Revisión
4
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 4 de 6
Responsable de la Unidad de Formación de Operadores 
Ing. Ariel Gaspardis 
Celular 3416 627139 
e-mail: arielgaspardis@cammesa.com.ar 
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
ANEXO: RELAÇÃO DOS CONTATOS 
OPERACIONAIS DO ONS E DA CAMMESA 
ANEXO:  RELACIÓN DE LOS CONTACTOS 
OPERATIVOS DEL ONS Y DE LA CAMMESA
Módulo / 
Módulo
3
Revisão / 
Revisión
4
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 5 de 6
CENTRO DE CONTROL 
Teléfonos: 
(54) 0341-495-8600 (COC) 
(54) 0341-495-8313 (COC) 
(54) 0341-525-8110 (COCE – Centro de Operaciones de Emergencia) 
Celulares (54 9 341) 5327843 – 5326083 
Satelital Iridium: 00 8816 3268 0088 
e-mail: turno@cammesa.com.ar  
Jefes de Turno 
Ing. Lucas Costantini 
Ing. Juan Pablo Cortelloni 
Ing. Raúl Santillán 
Ing. Ignacio García Krupa 
Ing. Fernando Caligiuri 
Ing. Juan Pablo Mirable 
Despachantes de Generación 
Ing. Juan Nicolás Gómez 
Téc. Armando Defays 
Ing. Juan Pablo Juaristi 
Téc. Matías Pezzelato 
Ing. Javier Echevarría 
Téc. Lucas Manavella 
Supervisores de Red: 
Ing. Sergio Valdez 
Ing. Ezequiel Kramer 
Ing. Francisco Benetti 
Ing. Manuel Piñol 
Ing. Agustín Maroni 
Ing. Guillermo Torres 
Operadores de Generación Renovable 
Ing. Jesús Flores Arocha 
Téc. Sebastián Cabrera 
Ing. Laura Cagnone 
Téc. Ezequiel Pavón 
Ing. José Ferrari 
Ing. Jeremías Contento
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / CAMMESA
REGLAMENTO INTERNACIONAL DE OPERACIÓN CAMMESA / ONS
ANEXO: RELAÇÃO DOS CONTATOS 
OPERACIONAIS DO ONS E DA CAMMESA 
ANEXO:  RELACIÓN DE LOS CONTACTOS 
OPERATIVOS DEL ONS Y DE LA CAMMESA
Módulo / 
Módulo
3
Revisão / 
Revisión
4
Data de Vigência / 
Fecha de Vigencia
16/04/2024
Referência:
Página 6 de 6
ONS
A relação dos contatos operacionais do ONS pode ser acessada por meio do seguinte link:
Se puede acceder a la lista de contactos operativos del ONS a través del siguiente enlace:
http://www.ons.org.br/AnexosMPO/Anexo%20da%20RO-RO.BR.02.pdf
