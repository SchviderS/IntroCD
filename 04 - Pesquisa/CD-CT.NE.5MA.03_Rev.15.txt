Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.11
Cadastro de Informações Operacionais.
Cadastro de Informações Operacionais de Faixas para Controle de Tensão da Área 500/230 
kV do Maranhão
Código Revisão Item Vigência
CD-CT.NE.5MA.03 15 2.6.3 17/06/2024
MOTIVO DA REVISÃO
Retiradas as definições das Demandas São Luís, Imperatriz e Balsas e a definição dos patamares de carga 
em função dessas grandezas de referência, em função desta área elétrica não possuir faixas de tensão 
específicas a serem observadas.
Adequação da lista de distribuição.
LISTA DE DISTRIBUIÇÃO
ALUMAR ARGO CELEO CHESF CNOS
COSR-NCO COSR-NE CYMIMASA EDP BRASIL ELETRONORTE
ENERGISA TOCANTINS EQUATORIAL_MA GERANORTE Heineken NEXCERA
SIMM EMPREENDIMENTOS UTE PORTO DO ITAQUI
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 500/230 kV do Maranhão
CD-
CT.NE.5MA.03 15 2.6.3 17/06/2024
RT-ONS DPL 0105/2024 - DIRETRIZES PARA OPERAÇÃO ELÉTRICA - VOLUME 19 REVISÃO 01 2 / 4
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. CONCEITOS ......................................................................................................................................3
3. CONSIDERAÇÕES GERAIS .................................................................................................................3
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO .........................................................4
4.1. Faixas de tensão para Barramentos da Rede de Operação...........................................................4
4.2. Demais Faixas de tensão da Rede de Operação............................................................................4
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 500/230 kV do Maranhão
CD-
CT.NE.5MA.03 15 2.6.3 17/06/2024
RT-ONS DPL 0105/2024 - DIRETRIZES PARA OPERAÇÃO ELÉTRICA - VOLUME 19 REVISÃO 01 3 / 4
1. OBJETIVO
Apresentar as faixas de controle de tensão dos barramentos e da Rede de Operação e dos secundários das 
transformações da Rede de Operação, cujos barramentos não pertencem à Rede de Operação, a serem 
controlados pelos operadores dos Centros de Operação do ONS e pela Operação dos agentes envolvidos.
2. CONCEITOS
Não se aplica.
3. CONSIDERAÇÕES GERAIS
3.1. Este Cadastro de Informações Operacionais apresenta os valores das faixas para controle de tensão de 
barramentos da Rede de Operação, de secundários das transformações da Rede de Operação cujas 
barras não pertencem à Rede de Operação.
3.2. Os Centros de Operação do ONS, nas ações de coordenação, supervisão e controle, devem observar 
os limites operacionais constantes neste Cadastro de Informações Operacionais.
3.3. A operação dos agentes, em suas ações de comando e execução, deve observar os limites operacionais 
constantes neste Cadastro de Informações Operacionais.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 500/230 kV do Maranhão
CD-
CT.NE.5MA.03 15 2.6.3 17/06/2024
RT-ONS DPL 0105/2024 - DIRETRIZES PARA OPERAÇÃO ELÉTRICA - VOLUME 19 REVISÃO 01 4 / 4
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO
4.1. FAIXAS DE TENSÃO PARA BARRAMENTOS DA REDE DE OPERAÇÃO
Não se aplica.
4.2. DEMAIS FAIXAS DE TENSÃO DA REDE DE OPERAÇÃO
Os barramentos da Rede de Operação e os secundários das transformações da Rede de Operação, que não 
possuem faixas de tensão definidas nos itens anteriores, devem operar com tensões nas seguintes faixas 
operativas, conforme estabelecido nos Procedimentos de Rede e reproduzido na tabela a seguir:
Tensão Nominal (kV) Faixa de Tensão (kV)
765 690 a 800
525 500 a 550 
500 500 a 550 
440 418 a 460 
345 328 a 362 
230 218 a 242 
138 131 a 145 
88 83,6 a 92,4
69 65,6 a 72,4
34,5 32,8 a 36,2
23 21,8 a 24,2
13,8 13,1 a 14,5
