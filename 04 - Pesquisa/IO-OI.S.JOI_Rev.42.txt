Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação
Procedimentos Sistêmicos para a Operação da SE Joinville
Código Revisão Item Vigência
IO-OI.S.JOI 42 3.7.5.3. 19/08/2024
.
MOTIVO DA REVISÃO
- Alteração das condições para fechamento em anel do Transformador TF 8 230/69/13,8 KV – 150 MVA da 
SE Joinville, alterando o subitem 6.2.2.2.
LISTA DE DISTRIBUIÇÃO
CNOS COSR-S Celesc CGT Eletrosul
(COT Norte) Arcelormittal 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Joinville IO-OI.S.JOI 42 3.7.5.3. 19/08/2024
Referência: 2 / 15
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. CONSIDERAÇÕES GERAIS .................................................................................................................3
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA SE INSTALAÇÃO .........................................................4
3.1. Barramento de 230 kV ..................................................................................................................4
3.2. Alteração da Configuração dos Barramentos................................................................................4
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL .............................................................................5
4.1. Procedimentos Gerais ...................................................................................................................5
4.2. Procedimentos Específicos............................................................................................................5
4.2.1. Operação dos Comutadores de Tape Sob Carga (LTC) ................................................5
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................................................5
5.1. Procedimentos Gerais ...................................................................................................................5
5.2. Procedimentos para Recomposição Fluente.................................................................................6
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................6
5.2.2. Recomposição Fluente da Instalação ..........................................................................7
5.3. Procedimentos após Desligamento Total da Instalação................................................................9
5.3.1. Preparação da Instalação após Desligamento Total....................................................9
5.3.2. Recomposição após Desligamento Total da Instalação...............................................9
5.4. Procedimentos após Desligamento Parcial da Instalação .............................................................9
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ..............9
5.4.2. Recomposição após Desligamento Parcial da Instalação ..........................................10
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS...................................................10
6.1. Procedimentos Gerais .................................................................................................................10
6.2. Procedimentos Específicos..........................................................................................................11
6.2.1. Desenergização de Equipamentos ............................................................................11
6.2.2. Energização de Linhas de Transmissão e de Equipamentos......................................12
7. NOTAS IMPORTANTES ...................................................................................................................15
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Joinville IO-OI.S.JOI 42 3.7.5.3. 19/08/2024
Referência: 3 / 15
1. OBJETIVO
Estabelecer os procedimentos para a operação da SE Joinville, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede.
2. CONSIDERAÇÕES GERAIS
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da Instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade 
das ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do 
ONS.
2.2. A comunicação operacional entre o COSR-S e a Instalação, no que se refere aos equipamentos de 
manobra na Instalação, é realizada conforme segue:
Linha de Transmissão ou 
Equipamento Agente de Operação Agente Operador Centro de Operação 
do Agente Operador
Barramento de 230 kV CGT Eletrosul CGT Eletrosul COT Norte
LT 230 kV Curitiba / Joinville CGT Eletrosul CGT Eletrosul COT Norte
LT 230 kV Joinville / Joinville 
Norte CGT Eletrosul CGT Eletrosul COT Norte
LT 230 kV Blumenau / Joinville CGT Eletrosul CGT Eletrosul COT Norte
LT 230 kV Joinville / Vega do 
Sul C1 (*) 
CGT Eletrosul (módulo 
230 kV e linha de 
transmissão)
CGT Eletrosul
(módulo 230 kV) COT Norte
LT 230 kV Joinville / Vega do 
Sul C2 (*)
CGT Eletrosul (módulo 
230 kV e linha de 
transmissão)
CGT Eletrosul
(módulo 230 kV) COT Norte
Transformador TF 3 
230/138/13,8 kV CGT Eletrosul CGT Eletrosul COT Norte
Transformador TF 4 
230/138/13,8 kV CGT Eletrosul CGT Eletrosul COT Norte
Transformador TF 6 
230/138/13,8 kV CGT Eletrosul CGT Eletrosul COT Norte
Transformador TF 7 
230/138/13,8 kV CGT Eletrosul CGT Eletrosul COT Norte
Transformador TF 8 
230/69/13,8 kV CGT Eletrosul CGT Eletrosul COT Norte
(*) Somente os equipamentos de manobra de 230 kV da LT 230 kV Joinville / Veja do Sul C1 e C2, na SE 
Joinville, pertencem à Rede de Operação, conforme a Rotina Operacional RO-RD.BR.01.
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte da Área 230 kV de Santa 
Catarina.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Joinville IO-OI.S.JOI 42 3.7.5.3. 19/08/2024
Referência: 4 / 15
2.4. No que se refere ao religamento manual de equipamentos ou de linhas de transmissão:
2.4.1. A definição da quantidade de tentativas de religamento manual de equipamento ou de linha de 
transmissão, bem como o intervalo entre elas, é de responsabilidade do Agente e devem ser 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica.
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR-S autorização 
para religamento. Nessa oportunidade, o Agente também pode solicitar alteração no sentido 
normal para envio de tensão, caso não tenha autonomia para tal.
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR-S levará em consideração as condições operativas do 
sistema.
2.5. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica.
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica.
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica caso a manobra seja realizada sob coordenação do COSR-S, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação quando o Agente tiver autonomia para energizar a 
linha de transmissão ou o equipamento.
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA SE INSTALAÇÃO
3.1. BARRAMENTO DE 230 KV
A configuração do barramento de 230 kV é do tipo Principal e Transferência (Barra P e Barra T). Na 
operação normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados, exceto as 
seccionadoras de transferência das linhas de transmissão ou equipamentos.
3.2. ALTERAÇÃO DA CONFIGURAÇÃO DOS BARRAMENTOS
A mudança de configuração do barramento de 230 kV desta Instalação é executada com controle do COSR-
S.
A mudança de configuração dos demais barramentos é executada sob responsabilidade do Agente 
Operador da Instalação
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Joinville IO-OI.S.JOI 42 3.7.5.3. 19/08/2024
Referência: 5 / 15
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL
4.1. PROCEDIMENTOS GERAIS
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão 
controlada pelo COSR-S. 
As faixas de controle de tensão desse barramento estão estabelecidas no Cadastro de Informações 
Operacionais das Faixas para Controle de Tensão da respectiva Área Elétrica.
4.1.2. Os barramentos de 138 kV e 69 kV, não pertencentes à Rede de Operação, onde se conectam as 
transformações 230/138/13,8 kV e 230/69/13,8 kV, têm a sua regulação de tensão executada com 
autonomia pelo Agente Operador da Instalação, por meio da utilização de recursos locais 
disponíveis de autonomia dessa.
Esgotados esses recursos, o Agente deve acionar o COSR-S, que deve verificar a indisponibilidade 
dos recursos sistêmicos.
As faixas de controle de tensão desses barramentos estão estabelecidas no Cadastro de 
Informações Operacionais das Faixas para Controle de Tensão da respectiva Área Elétrica.
4.1.3. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob responsabilidade da Instalação.
4.2. PROCEDIMENTOS ESPECÍFICOS
4.2.1. OPERAÇÃO DOS COMUTADORES DE TAPE SOB CARGA (LTC)
Os LTCs dos transformadores TF 3, TF 4, TF 6 e TF 7 230/138/13,8 kV e TF 8 230/69/13,8 kV operam em 
modo manual.
A movimentação dos comutadores é executada com autonomia pela operação da CGT Eletrosul.
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO
5.1. PROCEDIMENTOS GERAIS
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir:
•Desligamento total da Instalação: caracterizado quando não há tensão em todos os terminais 
de suas linhas de transmissão.
•Desligamento parcial da Instalação: qualquer outra configuração que não se enquadre como 
desligamento total.
5.1.2. Quando de desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao COSR-
S as seguintes informações:
•horário da ocorrência;
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Joinville IO-OI.S.JOI 42 3.7.5.3. 19/08/2024
Referência: 6 / 15
•configuração da Instalação após a ocorrência;
•configuração da Instalação após ações realizadas com autonomia pela operação dessa.
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2, sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia do Agente Operador da Instalação na 
recomposição, deve ser utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4. 
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE 
No caso de desligamento total, o Agente Operador deve configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir:
Abrir ou manter abertos os disjuntores:
•das linhas de transmissão: 
LT 230 kV Curitiba / Joinville;
LT 230 kV Joinville / Joinville Norte;
LT 230 kV Blumenau / Joinville;
LT 230 kV Joinville / Vega do Sul C1;
LT 230 kV Joinville / Vega do Sul C2;
•de todas as linhas de transmissão de 138 e 69 kV;
•dos transformadores: 
TF 3 230/138/13,8 kV (lados de 230 kV e de 138 kV);
TF 4 230/138/13,8 kV (lados de 230 kV e de 138 kV);
TF 6 230/138/13,8 kV (lados de 230 kV e de 138 kV);
TF 7 230/138/13,8 kV (lados de 230 kV e de 138 kV);
TF 8 230/69/13,8 kV (lados de 230 kV e de 69 kV).
Desligar ou manter desligado o modo de comutação automática dos comutadores sob carga das 
transformações 230/138/13,8 kV e de 230/69/13,8 kV da SE Joinville.
Cabe ao Agente CGT Eletrosul para o barramento de 230 kV e ao Agente Celesc para o barramento de 138 
kV informarem ao COSR-S quando a configuração de preparação desses barramentos não estiver atendida 
para o início da recomposição, independentemente de o equipamento ser próprio ou de outros Agentes. 
Nesse caso, o COSR-S fará contato com os agentes envolvidos para identificar o motivo do não-
atendimento e, após confirmação do agente afetado de que o barramento está com a configuração 
atendida, o COSR-S coordenará os procedimentos para recomposição, caso necessário, em função da 
configuração desta Instalação.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Joinville IO-OI.S.JOI 42 3.7.5.3. 19/08/2024
Referência: 7 / 15
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO
A Instalação faz parte da recomposição da Área Gov. Ney Aminthas de Barros Braga. Os Agentes 
Operadores devem adotar os procedimentos a seguir para recomposição fluente:
Passo Executor Procedimentos Condições ou Limites Associados
1 CGT Eletrosul
(COT Norte)
Receber tensão da SE Joinville Norte, pela LT 
230 kV Joinville / Joinville Norte e fechar, em 
energizando o barramento, o disjuntor.
1.1 CGT Eletrosul
(COT Norte)
Energizar, pelo lado 230 kV, o transformador 
TF 3 230/138/13,8 kV da SE Joinville e ligar, 
energizando o barramento de 138 kV, o lado 
de 138 kV.
TAPJOI-230/138 = 0.
Proceder a energização dos 
serviços auxiliares da SE Joinville 
e de uma das linhas transmissão 
de 138 kV.
Possibilitar o restabelecimento 
de carga prioritária nas 
subestações atendidas pela SE 
Joinville.
1.1.1 Celesc
Restabelecer carga prioritária nas subestações 
atendidas pela SE Joinville.
No máximo 50 MW.
Respeitar o limite mínimo de 
tensão de 124 kV no barramento 
de 138 kV.
1.2 CGT Eletrosul
(COT Norte)
Energizar, pelo lado 138 kV, o segundo 
transformador 230/138/13,8 kV da SE Joinville 
e fechar, em anel, o disjuntor do lado de 230 
kV.
TAPJOI-230/138 = 0.
Após fluxo de potência ativa no 
transformador TF 3 
230/138/13,8 kV da SE Joinville.
Proceder à energização da 
segunda linha de transmissão de 
138 kV.
1.2.1 Celesc
Restabelecer carga prioritária nas subestações 
atendidas pela SE Joinville.
No máximo 50 MW, totalizando 
100 MW.
Respeitar o limite mínimo de 
tensão de 124 kV no barramento 
de 138 kV.
2 CGT Eletrosul
(COT Norte)
Receber tensão da SE Curitiba, pela LT 230 kV 
Curitiba / Joinville e fechar, em anel, o 
disjuntor.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Joinville IO-OI.S.JOI 42 3.7.5.3. 19/08/2024
Referência: 8 / 15
Passo Executor Procedimentos Condições ou Limites Associados
2.1 CGT Eletrosul
(COT Norte)
Energizar, pelo lado 230 kV, o transformador 
TF 8 230/69/13,8 kV da SE Joinville e ligar, 
energizando o barramento de 69 kV, o lado de 
69 kV.
TAPJOI-230/138 = 0.
Após fluxo de potência ativa:
•na LT 230 kV Joinville / 
Joinville Norte;
•na LT 230 kV Curitiba / 
Joinville; e
•nos dois transformadores 
230/138 kV da SE Joinville 
que foram energizados.
Proceder à energização de uma 
linha de transmissão de 69 kV.
Possibilitar o restabelecimento da 
carga nas subestações atendidas 
pela SE Joinville.
2.1.1 Celesc
Restabelecer carga prioritária nas subestações 
atendidas pela SE Joinville
No máximo 50 MW.
Respeitar o limite mínimo de 
tensão de 62kV no barramento 
de 69kV.
2.2 CGT Eletrosul
(COT Norte)
Energizar, pelo lado 138 kV, o terceiro 
transformador 230/138/13,8 kV da SE Joinville 
e fechar, em anel, o disjuntor do lado de 
230 kV.
TAPJOI-230/138 = 0.
Após fluxo de potência ativa:
•na LT 230 kV Joinville / 
Joinville Norte;
•na LT 230 kV Curitiba / 
Joinville; e
•nos transformadores 
230/138 kV e 230/69 kV da 
SE Joinville que foram 
energizados.
Proceder à energização da 
terceira linha de transmissão de 
138 kV.
2.2.1 Celesc
Restabelecer carga prioritária nas subestações 
atendidas pela SE Joinville.
No máximo 50 MW, totalizando, 
no máximo, 150 MW na 
transformação 230/138/13,8 kV 
da SE Joinville e 200 MW na SE 
Joinville. 
Respeitar o limite mínimo de 
tensão de 124 kV no barramento 
de 138 kV.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Joinville IO-OI.S.JOI 42 3.7.5.3. 19/08/2024
Referência: 9 / 15
Passo Executor Procedimentos Condições ou Limites Associados
3 CGT Eletrosul
(COT Norte)
Receber tensão da SE Blumenau, pela LT 230 
kV Blumenau / Joinville e fechar, em anel, o 
disjuntor.
3.1
CGT Eletrosul
(COT Norte)
Energizar, pelo lado 138 kV, o quarto 
transformador 230/138/13,8 kV da SE Joinville 
e fechar, em anel, o disjuntor do lado de 
230 kV.
TAPJOI-230/138 = 0.
Após fluxo de potência ativa:
•na LT 230 kV Joinville / 
Joinville Norte;
•na LT 230 kV Curitiba / 
Joinville;
•na LT 230 kV Blumenau / 
Joinville; e 
•nos transformadores 
230/138/13,8 kV e 
230/69/13,8 kV da SE 
Joinville que foram 
energizados.
Os demais equipamentos desta Instalação são restabelecidos sob controle do COSR-S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras.
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1.
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2, enquanto não houver 
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia da operação da 
instalação na recomposição. 
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR-S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras.
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja: 
•ausência de tensão em todos os barramentos,
•ausência de fluxo de potência ativa nas linhas de transmissão, e
•existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, o 
Agente Operador deve preparar a Instalação conforme Subitem 5.2.1, sem necessidade de 
autorização do ONS.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Joinville IO-OI.S.JOI 42 3.7.5.3. 19/08/2024
Referência: 10 / 15
5.4.1.2. Caracterizado desligamento parcial da Instalação que seja: 
•ausência de tensão nas barras de 230 kV, os Agentes Operadores devem preparar o setor de 
230 kV da Instalação (disjuntores das linhas de transmissão e equipamentos) conforme 
Subitem 5.2.1., sem necessidade de autorização do ONS. 
5.4.1.3. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da 
Instalação.
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador 
deve recompor a Instalação conforme Subitem 5.2.2, sem necessidade de autorização do ONS.
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., os Agentes 
Operadores devem informar ao COSR-S, para que a recomposição da Instalação seja executada 
com controle do COSR-S.
5.4.2.3. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.3., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 6.2.2., sem necessidade de 
autorização do ONS.
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem 
atendidos, os Agentes Operadores devem informar ao COSR-S, para que a recomposição da 
Instalação seja executada com controle do COSR-S.
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS
6.1. PROCEDIMENTOS GERAIS
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S.
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, após 
desligamentos programados, de urgência ou de emergência, só podem ser efetuados com controle 
do COSR-S.
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelo Agente Operador Instalação quando 
estiverem explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução de 
Operação.
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR-S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Joinville IO-OI.S.JOI 42 3.7.5.3. 19/08/2024
Referência: 11 / 15
6.1.4. Antes do fechamento de qualquer disjuntor, o Agente Operador da Instalação deve verificar se 
existe tensão de retorno e se a condição de fechamento será em anel.
O fechamento em anel só pode ser executado com autonomia pelo Agente Operador da Instalação 
quando estiver especificado nesta Instrução de Operação e estiverem atendidas as condições do 
Subitem 6.2.2.
O fechamento de paralelo só pode ser efetuado com controle do COSR-S.
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento:
6.1.5.1. A energização em sentido normal ou, quando permitida, em sentido inverso, pode ser feita com 
autonomia pela operação do Agente, conforme procedimentos para manobras que estão 
definidos nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização 
controlados pelo COSR-S estão definidos na Instrução de Operação de Preparação para Manobras 
da respectiva área elétrica.
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de 
intervenções, são de responsabilidade do Agente. 
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o operador da Instalação deve fechá-lo em anel desde que tenha autonomia para tal, 
adotando as condições para o fechamento constantes no Subitem 6.2.2.
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S. 
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com 
controle do COSR-S.
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra.
6.2. PROCEDIMENTOS ESPECÍFICOS
6.2.1. DESENERGIZAÇÃO DE EQUIPAMENTOS
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação é 
sempre controlada pelo COSR-S.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Joinville IO-OI.S.JOI 42 3.7.5.3. 19/08/2024
Referência: 12 / 15
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S.
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de equipamentos ou de linhas de transmissão.
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado 
nas condições de energização para a manobra.
Para os demais desligamentos parciais, proceder conforme Subitem 5.4.
Linha de Transmissão / 
Equipamento Procedimentos Condições ou Limites Associados
Sentido Normal: SE Joinville recebe tensão da SE Blumenau 
Ligar, em anel, a LT 230 kV Blumenau / 
Joinville.
Sentido Inverso: SE Joinville envia tensão para SE Blumenau
LT 230 kV Blumenau / 
Joinville
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2SC.
Sentido Normal: SE Joinville recebe tensão da SE Curitiba
Ligar, em anel, a LT 230 kV Curitiba / 
Joinville.
Sentido Inverso: SE Joinville envia tensão para SE Curitiba
LT 230 kV Curitiba / 
Joinville
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2SC.
Sentido Normal: SE Joinville recebe tensão da SE Joinville Norte
Ligar, em anel, a LT 230 kV Joinville / 
Joinville Norte.
Sentido Inverso: SE Joinville envia tensão para SE Joinville Norte
LT 230 kV Joinville / 
Joinville Norte
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2SC.
Transformador TF 3 ou TF 7 Sentido Normal: A partir do lado de 230 kV 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Joinville IO-OI.S.JOI 42 3.7.5.3. 19/08/2024
Referência: 13 / 15
Linha de Transmissão / 
Equipamento Procedimentos Condições ou Limites Associados
Energizar, pelo lado de 230 kV, o 
transformador TF 3 230/138/13,8 kV 
(ou TF 7).
Sistema completo (de LT) ou N-1 
(de LT) na SE Joinville 230 kV
Como primeiro, segundo, terceiro 
ou quarto transformador:
•V JOI-230 ≤ 242 kV.
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa nos transformadores 
230/138/13,8 kV que já foram 
energizados.
Ligar, energizando o barramento de 
138 kV ou interligando esse 
transformador com outro já em 
operação, o lado de 138 kV do 
transformador TF 3 230/138/13,8 kV 
(ou TF 7).
O barramento de 138 kV deve estar 
desenergizado ou energizado por 
outro transformador 230/138/13,8 
kV da SE Joinville.
Sentido Inverso: A partir do lado de 138 kV
Energizar, pelo lado de 138 kV, o 
transformador TF 3 230/138/13,8 kV 
(ou TF 7).
Sistema completo (de LT) ou N-1 
(de LT) na SE Joinville 138 kV
Como primeiro, segundo, terceiro 
ou quarto transformador:
•V JOI-138 ≤ 145 kV; e
•0 ≤ TAP JOI-230/138 ≤ +9.
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa nos transformadores 
230/138/13,8 kV que já foram 
energizados.
230/138/13,8 kV
Ligar, em anel, o lado de 230 kV do 
transformador TF 3 230/138/13,8 kV 
(ou TF 7).
Transformador TF 4 ou TF 6 Sentido Normal: A partir do lado de 230 kV
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Joinville IO-OI.S.JOI 42 3.7.5.3. 19/08/2024
Referência: 14 / 15
Linha de Transmissão / 
Equipamento Procedimentos Condições ou Limites Associados
Energizar, pelo lado de 230 kV, o 
transformador TF 4 230/138/13,8 kV 
(ou TF 6).
Sistema completo (de LT) ou N-1 
(de LT) na SE Joinville 230 kV
Como primeiro, segundo, terceiro 
ou quarto transformador:
•V JOI-230 ≤ 242 kV.
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa nos transformadores 
230/138/13,8 kV que já foram 
energizados.
Ligar, energizando o barramento de 
138 kV ou interligando esse 
transformador com outro já em 
operação, o lado de 138 kV do 
transformador TF 4 230/138/13,8 kV 
(ou TF 6).
O barramento de 138 kV deve estar 
desenergizado ou energizado por 
outro transformador 230/138/13.8 
kV da SE Joinville.
Sentido Inverso: A partir do lado de 138 kV
Energizar, pelo lado de 138 kV, o 
transformador TF 4 230/138/13,8 kV 
(ou TF 6).
Sistema completo (de LT) na SE 
Joinville 138 kV
Como primeiro, segundo, terceiro 
ou quarto transformador:
•V JOI-138 ≤ 145 kV.
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa nos transformadores 
230/138/13,8 kV que já foram 
energizados.
230/138/13,8 kV
Ligar, em anel, o lado de 230 kV do 
transformador TF 4 230/138/13,8 kV 
(ou TF 6).
Transformador TF 8 Sentido Único: A partir do lado de 230 kV
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Joinville IO-OI.S.JOI 42 3.7.5.3. 19/08/2024
Referência: 15 / 15
Linha de Transmissão / 
Equipamento Procedimentos Condições ou Limites Associados
Energizar, pelo lado de 230 kV, o 
transformador TF 8 230/69/13,8 kV.
Sistema completo (de LT) ou N-1 
(de LT) na SE Joinville 230 kV
Como primeiro, segundo, terceiro, 
quarto ou quinto transformador de 
230 kV:
•V JOI-230 ≤ 242 kV;
•-8 ≤ TAP JOI-230/69 ≤ 0.
Antes de energizar o transformador 
TF 8 230/69/13,8 kV, verificar fluxo 
de potência ativa nos 
transformadores 230/138/13,8 kV 
que já foram energizados.
Ligar, energizando o barramento de 69 
kV ou interligando esse transformador 
com o(s) transformador(es) 
230/138/13,8 kV e 138/69/13,8 kV já 
em operação, o lado de 69 kV do 
transformador TF 8 230/69/13,8 kV. 
Barramento de 69 kV da SE Joinville 
desenergizado ou energizado pelos 
transformadores que interligam o 
barramento de 230 kV, o de 138 kV 
e o de 69 kV da SE Joinville. 
Em caso de abertura apenas do lado de 230 kV, proceder conforme segue:
230/69/13,8 kV
Ligar, em anel, o lado de 230 kV do 
transformador TF 8 230/69/13,8 kV. 
7. NOTAS IMPORTANTES
7.1. As tratativas de operação entre o ONS e o Consumidor Livre Arcelormittal Brasil S.A. são realizadas 
pela própria ArcelorMittal Brasil S.A, por meio do Centro de Operação da Subestação Veja do Sul – SE 
Vega do Sul.
7.2. No caso de desligamento total da SE Joinville o restabelecimento de carga da ArcelorMittal Brasil S.A.  
somente pode ser realizado após autorização do COSR-S.
