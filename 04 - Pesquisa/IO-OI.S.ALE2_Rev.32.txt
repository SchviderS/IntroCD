 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Alegrete 2 
 
Código Revisão Item Vigência 
IO-OI.S.ALE2 32 3.7.5.2. 11/03/2024 
 
. 
MOTIVO DA REVISÃO 
- Padronização da denominação dos Agentes ao longo do Documento. 
- Alteração do agente de operação de Sant’ana para Taesa da LT 230 kV Alegrete 2 / Livramento 3, 
acarretando alteração no item 2.2. 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S CEEE-T Taesa RGE Sul 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Alegrete 2 IO-OI.S.ALE2 32 3.7.5.2. 11/03/2024 
 
Referência:  2 / 12 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  4 
3.1. Barramento de 230 kV ................................................................................................................... 4 
3.2. Alteração na Configuração dos Barramentos ................................................................................ 4 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 4 
4.1. Procedimentos Gerais ................................................................................................................... 4 
4.2. Procedimentos Específicos ............................................................................................................ 5 
4.2.1. Operação dos Comutadores de Tapes Sob Carga (LTC) .............................................. 5 
4.2.2. Operação dos Reatores ............................................................................................... 5 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  5 
5.1. Procedimentos Gerais ................................................................................................................... 5 
5.2. Procedimentos Para Recomposição Fluente ................................................................................. 5 
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................ 5 
5.2.2. Recomposição fluente da Instalação ........................................................................... 6 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 7 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 7 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 7 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 7 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 7 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 8 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 8 
6.1. Procedimentos Gerais ................................................................................................................... 8 
6.2. Procedimentos Específicos ............................................................................................................ 9 
6.2.1. Desenergização de Equipamentos .............................................................................. 9 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ....................................... 9 
7. NOTAS IMPORTANTES ................................ ................................ ................................ ................ 12 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Alegrete 2 IO-OI.S.ALE2 32 3.7.5.2. 11/03/2024 
 
Referência:  3 / 12 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
1. OBJETIVO 
Estabelecer os proc edimentos para a operação da SE Alegrete 2 , definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelos Agentes Operadores da Instalação, devendo fazer parte do manual 
de operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade 
das ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR-S e a Instalação, de propriedade da CEEE-T, no que se refere 
aos equipamentos de manobra da Instalação, é realizada conforme segue: 
Linha de Transmissão ou 
Equipamento  Agente de Operação Agente Operador Centro de Operação 
do Agente Operador 
Barramento 230 kV CEEE-T CEEE-T COT CEEE-T 
LT 230 kV Alegrete 2 / 
Maçambará CEEE-T CEEE-T COT CEEE-T 
LT 230 kV Alegrete 2 / 
Livramento 2 CEEE-T CEEE-T COT CEEE-T 
LT 230 kV Alegrete 2 / 
Livramento 3 Taesa Taesa COS Taesa 
LT 230 kV Alegrete 2 / São 
Vicente do Sul CEEE-T CEEE-T COT CEEE-T 
LT 230 kV Alegrete 2 / Usina 
Termelétrica Uruguaiana CEEE-T CEEE-T COT CEEE-T 
LT 230 kV Alegrete 2 / 
Uruguaiana 5 CEEE-T CEEE-T COT CEEE-T 
TR-1 230/69/13,8 kV CEEE-T CEEE-T COT CEEE-T 
TR-2 230/69/13,8 kV CEEE-T CEEE-T COT CEEE-T 
RB-1 230 kV CEEE-T CEEE-T COT CEEE-T 
RB-2 230 kV CEEE-T CEEE-T COT CEEE-T 
RL-3 230 kV CEEE-T CEEE-T COT CEEE-T 
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte da Área 230 kV do Rio Grande 
do Sul. 
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.4.1. A definição do número de tentativas de religamento manual de linha de transmissão ou 
equipamento, bem como o intervalo entre elas , é de responsabilidade do A gente e devem ser 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Alegrete 2 IO-OI.S.ALE2 32 3.7.5.2. 11/03/2024 
 
Referência:  4 / 12 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente , e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar autorização ao COSR-S 
autorização para religamento. Nessa oportunidade, o Agente pode solicitar também a alteração no 
sentido normal para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em considera ção as condições operativas do 
sistema. 
2.5. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação , quando o Agente  tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 230 KV 
A configuração do barramento de 230 kV é do tipo Principal e Transferência (Barra P e Barra T). Na operação 
normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados, exceto as 
seccionadoras de transferência das linhas de transmissão ou equipamentos.  
Nota: O reator RL-3 somente pode ser utilizado na LT 230 kV Alegrete 2 / Livramento 2 .  Não é permitida a 
utilização do RL-3 na LT 230 kV Alegrete 2 / São Vicente do Sul. 
3.2. ALTERAÇÃO NA CONFIGURAÇÃO DOS BARRAMENTOS 
A mudança de configuração do barramento de  230 kV desta Instalação é executada com controle do  
COSR-S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade da operação da 
Instalação. 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S. 
As faixas de controle de tensão para esse  barramento estão estabelecidas no Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. O barramento de 69 kV, não pertencente à Rede de Operação, onde se conecta a transformação 
230/69/13,8 kV, tem a sua regulação de tensão executada com autonomia pelos Agentes Operadores 
da Instalação, por meio da utilização de recursos locais disponíveis de autonomia dessa. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Alegrete 2 IO-OI.S.ALE2 32 3.7.5.2. 11/03/2024 
 
Referência:  5 / 12 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Esgotados esses recursos, o Agente deve acionar o COSR -S, que deve verificar a disponibilidade dos 
recursos sistêmicos. 
As faixas de controle de tensão para o barramento de 69 kV estão estabelecidas no Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.3. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob a responsabilidade da Instalação. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
4.2.1. OPERAÇÃO DOS COMUTADORES DE TAPES SOB CARGA (LTC) 
Os LTCs dos transformadores TR-1 e TR-2 230 / 69 / 13,8 kV operam em modo automático.  
A movimentação dos comutadores é realizada com autonomia pela operação do Agente CEEE-T. 
4.2.2. OPERAÇÃO DOS REATORES 
A manobra dos reatores RB-1 230 kV 230 kV – 25 Mvar, RB-2 230 kV – 25 Mvar e RL-3 230 kV – 25 Mvar é 
executada com controle do COSR-S. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação : caracterizado quando não há tensão em todos os terminais 
de suas linhas de transmissão. 
• Desligamento parcial da Instalação : qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, os Agentes Operadores da Instalação  devem fornecer ao 
COSR-S as seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2., sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia dos Agentes Operadores da Instalação  na 
recomposição, deve ser utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE 
No caso de desligamento total, os Agentes Operadores devem configurar os disjuntores dos seguintes 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Alegrete 2 IO-OI.S.ALE2 32 3.7.5.2. 11/03/2024 
 
Referência:  6 / 12 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• das linhas de transmissão:  
LT 230 kV Alegrete 2 / Maçambará; 
LT 230 kV Alegrete 2 / Livramento 2; 
LT 230 kV Alegrete 2 / Livramento 3; 
LT 230 kV Alegrete 2 / São Vicente do Sul; 
LT 230 kV Alegrete 2 / Usina Termelétrica Uruguaiana; 
LT 230 kV Alegrete 2 / Uruguaiana 5. 
• de todas as linhas de transmissão de 69 kV 
• dos transformadores:  
TR-1 230/69/13,8 kV (lados de 230 e 69 kV) 
TR-2 230/69/13,8 kV (lados de 230 e 69 kV) 
• dos reatores: 
RB-1 230 kV 
RB-2 230 kV 
RL-3 230 kV 
Fechar ou manter fechados os disjuntores:   
• do módulo de interligação barras de 230 kV, exceto quando esse estiver  substituindo o disjuntor de um 
equipamento ou de uma linha de transmissão. 
Desligar ou manter desligado o modo de comutação automática do comutador sob carga da transformação 
230/69/13,8 kV da SE Alegrete 2. 
Cabe ao Agente CEEE-T informar ao COSR-S quando a configuração de preparação da Instalação não estiver 
atendida para o início da recomposição, independentemente de o equipamento ser próprio ou de outros 
agentes. Nesse caso, o COSR -S fará contato com os agentes envolvidos para identificar o motivo do não -
atendimento e, após confirmação do Agente CEEE-T de que o barramento está com a configuração atendida, 
o COSR -S coordenará os procedimentos para recomposição, caso necessário, em função da configuração 
desta Instalação. 
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO 
A Instalação faz parte da recomposição da Área Itá. Os Agentes Operadores devem adotar os procedimentos 
a seguir para recomposição fluente: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 CEEE-T 
Receber tensão da SE Maçambará , pela LT 230 
kV Alegrete 2 / Maçambará e energizar o 
barramento de 230 kV. 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Alegrete 2 IO-OI.S.ALE2 32 3.7.5.2. 11/03/2024 
 
Referência:  7 / 12 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Passo Executor Procedimentos Condições ou Limites Associados 
1.1 CEEE-T 
Energizar, pelo lado 230 kV, um dos 
transformadores 230/69/13,8 kV da SE Alegrete 
2 e ligar, energizando o barramento de 69 kV, o  
lado de 69 kV. 
TAPALE2-230/69 = 9 
Possibilitar o restabelecimento de 
carga prioritária nas subestações 
atendidas pela SE Alegrete 2. 
1.1.1 RGE Sul 
Restabelecer carga prioritária nas subestações 
atendidas pela SE Alegrete 2. 
No máximo 25 MW de carga. 
 
Respeitar o limite de tensão 
mínima de 62  kV no barramento 
de 69 kV. 
1.2 CEEE-T Ligar o reator RB -1 230 kV (ou RB -2) no 
barramento de 230 kV.  
1.2.1 CEEE-T 
Energizar a LT 230 kV Alegrete 2 / Uruguaiana 5, 
enviando tensão para a SE Uruguaiana 5. 
Após fluxo de potência ativa de no 
mínimo 15 MW na transformação 
230/69/13,8 kV que foi 
energizada.  
1.3 CEEE-T 
Energizar, pelo lado 230 kV, o segundo 
transformador 230/69/13,8 kV da SE Alegrete 2 
e ligar, interligando esse transformador com o 
outro transformador 230/138/13,8 kV da SE 
Alegrete 2, o lado de 69 kV.  
TAPALE2-230/69 = 9 
Após fluxo de potência ativa  no 
transformador 230/69/13,8 kV 
energizado 
2 CEEE-T 
Receber tensão da SE São Vicente do Sul, pela LT 
230 kV Alegrete 2 / São Vicente do Sul e ligar em 
anel. 
Δδ ≤ 30° 
Os demais equipamentos desta Instalação são restabelecidos sob controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
Os Agentes Operadores da Instalação devem realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
Os Agentes Operadores da Instalação devem  realizar os procedimentos do Subitem 5.2.2, enquanto não 
houver intervenção do COSR -S no processo de recomposição ou interrupção da autonomia destes na 
recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras.  
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Alegrete 2 IO-OI.S.ALE2 32 3.7.5.2. 11/03/2024 
 
Referência:  8 / 12 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, os 
Agentes Operadores devem  preparar a Instalação conforme Subitem 5.2 .1, sem necessidade de 
autorização do ONS. 
5.4.1.2. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., os Agentes 
Operadores devem  recompor a Instalação conforme Subitem 5.2 .2, sem necessidade de 
autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., os Agentes 
Operadores devem  recompor a Instalação conforme Subitem 6.2.2, sem necessidade de 
autorização do ONS.    
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmi ssão ou de equipamentos, após  
desligamento programado, de urgência ou de emergência, só podem ser efetuados com controle do 
COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelos Agentes Operadores da Instalação  
quando estiverem explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução 
de Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, os Agentes Operadores da Instalação devem verificar se 
existe tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pelos Agentes Operadores da 
Instalação quando estiver especificado nesta Instrução de O peração e estiverem atendidas as 
condições do Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou , quando permitida, em sentido  inverso, pode ser feita com 
autonomia pela operação do Agente, conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Alegrete 2 IO-OI.S.ALE2 32 3.7.5.2. 11/03/2024 
 
Referência:  9 / 12 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica.  
 A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o operador da Instalação deve fechá -lo em anel desde que tenha autonomia  para tal, 
adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE EQUIPAMENTOS 
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação, é sempre 
controlada pelo COSR-S. 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelos Agentes Operadores da  Instalação, 
após desligamento automático de equipamentos ou de linhas de transmissão. 
Os Agentes Operadores da Instalação devem identificar os desligamentos automáticos observando 
na Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado 
nas condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Alegrete 2 IO-OI.S.ALE2 32 3.7.5.2. 11/03/2024 
 
Referência:  10 / 12 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
LT 230 kV Alegrete 2 / 
Livramento 2 
Sentido Normal: SE Alegrete 2 envia tensão para SE Livramento 2  
Energizar a  LT 230 kV Alegrete 2 / 
Livramento 2. 
Sistema completo ou N -1 (LT ou TR) 
na SE Alegrete 2 230 kV 
• VALE2-230 ≤ 242 kV, 
conectado o RL-3 230 kV da 
SE ALE2; ou 
• VALE2-230 ≤ 238 kV, 
desconectado o RL-3 230 kV 
da SE ALE2. 
(Não é necessária autorização do 
ONS para a conexão do reator RL3 na 
LT 230 kV Alegrete 2 / Livramento 2, 
caso este reator estivesse em 
operação antes do desligamento da 
linha de transmissão). 
No caso de desligamento total da SE 
Alegrete 2 , somente energizar a LT 
230 kV Alegrete 2 / Livramento 2 com 
coordenação do COSR-S. 
Sentido Inverso: SE Alegrete 2 recebe tensão da SE Livramento 2  
Ligar em anel a LT 230 kV Alegrete 2 / 
Livramento 2. 
 
LT 230 kV Alegrete 2 / 
Livramento 3 
Sentido Normal: SE Alegrete 2 envia tensão para a SE Livramento 3  
Energizar a  LT 230kV Alegrete 2 / 
Livramento 3. 
Sistema completo na SE Alegrete 2 
230 kV 
• VALE2-230 ≤ 236 kV. 
No caso de desligamento total da SE 
Alegrete 2 , somente energizar a LT 
230 kV Alegrete 2 / Livramento 3 com 
coordenação do COSR-S. 
Sentido Inverso: SE Alegrete 2 recebe tensão da SE Livramento 3 
Ligar, em anel, a LT 230 kV Alegrete 2 / 
Livramento 3. 
 
LT 230 kV Alegrete 2 / 
Maçambará 
Sentido Normal: SE Alegrete 2 recebe tensão da SE Maçambará  
Ligar, em anel ou energizando o 
barramento de 230 kV, a LT 230 kV 
Alegrete 2 / Maçambará. 
 
Sentido Inverso: SE Alegrete 2 envia tensão para a SE Maçambará  
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2RS. 
LT 230 kV Alegrete 2 / São Sentido Normal: SE Alegrete 2 recebe tensão da SE São Vicente do Sul  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Alegrete 2 IO-OI.S.ALE2 32 3.7.5.2. 11/03/2024 
 
Referência:  11 / 12 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
Vicente do Sul Ligar, em anel, a LT 230 kV Alegrete 2 / 
São Vicente do Sul. 
 
Sentido Inverso: SE Alegrete 2 envia tensão somente para a SE São Vicente 
do Sul  
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2RS. 
LT 230 kV Alegrete 2 / 
Uruguaiana 5 
Sentido Normal: SE Alegrete 2 envia tensão para a SE Uruguaiana 5  
Energizar a  LT 230kV Alegrete 2 / 
Uruguaiana 5. • VALE2-230 ≤ 242 kV; e 
• fluxo de potência ativa na LT 
230 kV Alegrete 2 / 
Maçambará e na 
transformação 230/69/13,8 
kV da SE Alegrete 2. 
Sentido Inverso: SE Alegrete 2 recebe tensão da SE Uruguaiana 5  
Ligar, em anel, a LT 230 kV Alegrete 2 / 
Uruguaiana 5. 
 
LT 230 kV Alegrete 2 / 
Usina Termelétrica 
Uruguaiana 
Sentido Normal: SE Alegrete 2 recebe tensão da UTE Uruguaiana  
A energização em sentido normal é controlada pelo COSR-S, conforme IO-
PM.S.2RS. 
Sentido Inverso: SE Alegrete 2 envia tensão para a UTE Uruguaiana  
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2RS. 
Transformador 
TR-1 ou TR-2 
230/69/13,8kV 
Sentido Único: A partir do lado de 230 kV  
Energizar, pelo lado de 230 kV, o 
transformador TR-1 230/69/13,8 kV (ou 
TR-2). 
Como primeiro ou segundo 
transformador: 
• VALE2-230 ≤ 242 kV; e 
• fluxo de potência ativa na LT 
230 kV Alegrete 2 / 
Maçambará ou esta LT 
energizando o barramento 
de 230 kV. 
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa nos transformadores  
230/69/13,8 kV que já foram 
energizados. 
Ligar, energizando o barramento de 69 
kV ou interligando esse transformador 
com outro já em operação, ou em anel, 
pelo lado de 69 kV, o transformador TR-
1 230/69/13,8 kV (ou TR-2). 
Barramento de 69 kV da SE Alegrete 
2 desenergizado ou energizado por 
outro transformador 230/69/13,8 kV 
da SE Alegrete 2. 
Sentido Inverso: A partir do lado de 69 kV 
A energização pelo terminal de 69 kV não é permitida. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Alegrete 2 IO-OI.S.ALE2 32 3.7.5.2. 11/03/2024 
 
Referência:  12 / 12 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
Em caso de abertura apenas do lado de 230 kV, proceder conforme segue: 
Ligar, interligando esse transformador 
com outro já em operação, ou em anel, 
pelo lado  de 230 kV, o transformador 
TR-1 230/69/13,8 kV (ou TR-2). 
Outro transformador 230/69/13,8 
kV da SE Alegrete 2 com fluxo de 
potência ativa. 
 
Reator RB-1 ou RB-2 230 kV A manobra destes Reatores é controlada pelo COSR-S, conforme IO-PM.S.2RS. 
Reator RL-3 230 kV A manobra deste Reator é controlada pelo COSR-S, conforme IO-PM.S.2RS. 
7. NOTAS IMPORTANTES 
7.1. Para substituir o disjuntor da LT 230 kV Alegrete 2 / Livramento 3  pelo disjuntor de interligação das 
barras P e T 230 kV, o COSR-S coordenará essa ação com a Taesa e com a CEEE-T. Após tal substituição, 
os procedimentos para abertura do disjuntor ou recomposição da linha de transmissão deverão ser 
coordenados pelo COSR -S, que orientará a operação da CEEE-T na execução dos procedimentos 
descritos nesta Instrução de Operação e na IO -PM.S.2RS – Procedimentos para Preparação de 
Manobras na Área 230 kV do Rio Grande do Sul. 
 
