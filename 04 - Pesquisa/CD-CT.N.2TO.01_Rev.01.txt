Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.11
Cadastro de Informações Operacionais
Cadastro de Dados Operacionais de Equipamentos da Área 230 kV do Tocantins
Código Revisão Item Vigência
CD-CT.N.2TO.01 01 2.1.4. 28/04/2024
MOTIVO DA REVISÃO:
•Entrada em operação da Transformação 230/138/13,8 KV - 100 MVA SE Gurupi. Inclui o item 4.3.
LISTA DE DISTRIBUIÇÃO
CNOS COSR-NCO EDP Energisa Soluções
Energisa Tocantins Taesa
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Área 230 kV do Tocantins CD-CT.N.2TO.01 01 2.1.4. 28/04/2024
Referência: RT-ONS-DPL 0101/2024 Estudos pré-operacionais para integração ao SIN do setor de 138 KV da SE Gurupi 2 / 10
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. CONSIDERAÇÕES GERAIS .................................................................................................................3
3. DADOS OPERACIONAIS DE LINHAS DE TRANSMISSÃO......................................................................3
3.1. LT 230 kV Dianópolis II / Gurupi ....................................................................................................4
3.2. LT 230 kV Dianópolis II / Palmas....................................................................................................4
3.3. LT 230 kV Lajeado / Palmas C1 ou C2............................................................................................5
4. DADOS OPERACIONAIS DE TRANSFORMADORES .............................................................................6
4.1. SE Dianópolis II - Autotransformador DDAT6-01 ou 02-230/138/13,8 KV - 200 MVA ..................6
4.2. SE Gurupi - Autotransformador AT01 500/230/13,8 KV - 450 MVA .............................................6
4.3. SE Gurupi - Autotransformador AT04 ou 05 230/138/13,8 KV - 100 MVA ...................................7
4.4. SE Lajeado – Autotransformador LJAT7-01 ou 02 525/230/13,8 kV - 960 MVA ...........................7
4.5. SE Palmas - Autotransformador PLAT6-01 ou 02 230/138/13,8 KV - 200 MVA ............................7
5. DADOS OPERACIONAIS DE UNIDADES GERADORAS .........................................................................8
5.1. UHE Lajeado (CEG: UHE.PH.TO.001304-8.01) ...............................................................................9
5.1.1. Dados das Unidades Geradoras da UHE Lajeado ........................................................9
5.1.2. Curva de Capabilidade das Unidades Geradoras da UHE Lajeado.............................10
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Área 230 kV do Tocantins CD-CT.N.2TO.01 01 2.1.4. 28/04/2024
Referência: RT-ONS-DPL 0101/2024 Estudos pré-operacionais para integração ao SIN do setor de 138 KV da SE Gurupi 3 / 10
1. OBJETIVO
Apresentar os dados operacionais de equipamentos da Área 230 kV do Tocantins, a serem observados pelos 
operadores dos centros de operação do ONS e pela operação dos agentes envolvidos, de acordo com os 
Procedimentos de Rede.
2. CONSIDERAÇÕES GERAIS
2.1. Os dados de linhas de transmissão e equipamentos da Rede de Operação constantes neste Cadastro 
de Informações Operacionais devem ser observados pelos Centros de Operação do ONS para as ações 
de coordenação, supervisão e controle da operação, assim como pelos Agentes para as ações de 
comando e execução.
2.2. As limitações técnicas para tentativas de religamento de equipamentos contidas neste documento 
têm caráter operacional. Poderão ser utilizadas para apuração da qualidade do serviço público de 
transmissão de energia elétrica (apuração de parcela variável) o valor do “Tempo para energização 
após desligamento” de cada equipamento, desde que sejam atendidos os critérios estabelecidos na 
regulação vigente. 
3. DADOS OPERACIONAIS DE LINHAS DE TRANSMISSÃO
Orientações sobre os dados operacionais de linhas de transmissão:
Campo “Religamento Automático”:
•Campo “Tipo”: indica o tipo de religamento utilizado em operação normal: monopolar, tripolar ou 
monopolar / tripolar; caso não exista religamento instalado, o campo é preenchido com “NE” – não 
existe.
•Campo “Situação”: indica se o religamento está ligado ou desligado em operação normal; caso não 
exista religamento instalado, o campo é preenchido com “NE” – não existe.
•Campo “Sentido”: para religamentos tripolares, indica em primeiro lugar a instalação onde se dá o 
fechamento inicial (terminal líder), e após a seta, a instalação de fechamento final (terminal seguidor); 
para religamentos monopolares ou caso não exista religamento instalado, o campo é preenchido com 
hifens (--- ).
•Campo “Ajustes”: indica os ajustes de tempo morto, diferença de tensão, defasagem angular e diferença 
de frequência instalados para a função de verificação de sincronismo para o religamento automático; 
caso não exista religamento automático instalado, o campo é preenchido com hifens (---).
Campo “LT em Mesma Estrutura”: apresenta o nome da LT que compartilha estrutura, e quando disponível, 
a informação da extensão do trecho de compartilhamento e da quantidade de torres compartilhadas; caso 
não exista, o campo é preenchido com “NE” – não existe.
Não deve ser considerado o compartilhamento de estrutura caso esse seja apenas na chegada as subestações 
envolvidas.
Campo “Religamentos Manuais”:
•Campo “Quantidade de Tentativas”: indica quantos religamentos (ou tentativas de religamento) 
manuais são permitidos pelo Agente.
•Campo “Intervalo entre os Religamentos (min)”: é o intervalo de tempo entre os religamentos, em 
minutos, a ser considerado entre a tentativa de fechamento não-bem-sucedida anterior (incluindo 
o religamento automático) e o novo fechamento do disjuntor.
•Campo “Dispositivo de Sincronismo”: indica os terminais onde esses estão instalados e em 
funcionamento. Dispositivo de Sincronismo é o equipamento utilizado para verificação das 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Área 230 kV do Tocantins CD-CT.N.2TO.01 01 2.1.4. 28/04/2024
Referência: RT-ONS-DPL 0101/2024 Estudos pré-operacionais para integração ao SIN do setor de 138 KV da SE Gurupi 4 / 10
condições de fechamento em anel ou paralelo entre dois pontos elétricos; caso não exista, o campo 
é preenchido com “NE” – não existe. Se em algum disjuntor do terminal não houver o dispositivo 
de sincronismo essa informação deve ser destacada.
•Campo “Ajustes”: indica os ajustes de diferença de tensão, defasagem angular e diferença de 
frequência instalados para a função de verificação de sincronismo para o religamento manual de 
cada terminal; caso não exista dispositivo de sincronismo, o campo é preenchido com hifens (---).
Campo “Notas / Restrições”: campo opcional que indica informações relevantes a respeito da linha de 
transmissão.
Caso os dados operacionais não sejam informados pelo Agente, os campos são preenchidos com “NI” – não 
informado.
3.1. LT 230 KV DIANÓPOLIS II / GURUPI
Religamento Automático
Tipo Situação Sentido Ajustes
LT em Mesma Estrutura
Tripolar Ligado Gurupi → 
Dianópolis II
Tempo morto: 5s
V  20%
   30°
f   0,2 Hz
NE
Notas / Restrições: NI
Religamento Manual
Sentido Quantidade de 
Tentativas
Intervalo entre os 
religamentos (minutos)
Dispositivo de 
sincronismo Ajustes
NI 2
Após tentativa de 
religamento automático, 
1 tentativa de 
religamento manual com 
intervalo de 3 minutos 
até a segunda tentativa.
Em ambos os 
terminais NI
3.2. LT 230 KV DIANÓPOLIS II / PALMAS
Religamento Automático
Tipo Situação Sentido Ajustes
LT em Mesma Estrutura
Tripolar Ligado Palmas → 
Dianópolis II
Tempo morto: 5s
V  20%
   30°
f   0,2 Hz
NE
Notas / Restrições: NI
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Área 230 kV do Tocantins CD-CT.N.2TO.01 01 2.1.4. 28/04/2024
Referência: RT-ONS-DPL 0101/2024 Estudos pré-operacionais para integração ao SIN do setor de 138 KV da SE Gurupi 5 / 10
Religamento Manual
Sentido Quantidade de 
Tentativas
Intervalo entre os 
religamentos (minutos)
Dispositivo de 
sincronismo Ajustes
NI 2
Após tentativa de 
religamento automático, 
1 tentativa de 
religamento manual com 
intervalo de 3 minutos 
até a segunda tentativa.
Em ambos os 
terminais NI
3.3. LT 230 KV LAJEADO / PALMAS C1 OU C2
Religamento Automático
Tipo Situação Sentido Ajustes
LT em Mesma Estrutura
Tripolar Ligado Lajeado → 
Palmas
Tempo morto: 5s
V  5%
   10°
f   0,2 Hz
LT 230 kV Lajeado / Palmas 
C1 e C2
Notas / Restrições: NI
Religamento Manual
Sentido Quantidade de 
Tentativas
Intervalo entre os 
religamentos (minutos)
Dispositivo de 
sincronismo Ajustes
NI 2
Após tentativa de 
religamento automático, 
1 tentativa de 
religamento manual com 
intervalo de 3 minutos 
até a segunda tentativa, 
ou 3 tentativas de 
religamento manual, com 
intervalo de 3 minutos 
entre elas.
Em ambos os 
terminais NI
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Área 230 kV do Tocantins CD-CT.N.2TO.01 01 2.1.4. 28/04/2024
Referência: RT-ONS-DPL 0101/2024 Estudos pré-operacionais para integração ao SIN do setor de 138 KV da SE Gurupi 6 / 10
4. DADOS OPERACIONAIS DE TRANSFORMADORES
Orientações sobre os dados operacionais de transformadores:
Campo “Dispositivo de Sincronismo”: indica os terminais onde estão instalados e em funcionamento. 
Dispositivo de Sincronismo é o equipamento utilizado para verificação das condições de fechamento em anel 
ou paralelo entre dois pontos elétricos, o campo é preenchido com “NE” – não existe. Se em algum disjuntor 
do terminal não houver o dispositivo de sincronismo essa informação deve ser destacada.
Campo “Ajustes”: indica os ajustes de diferença de tensão, defasagem angular e diferença de frequência 
instalados para a função de verificação de sincronismo para o religamento manual de cada terminal; caso 
não exista dispositivo de sincronismo, o campo é preenchido com hifens (---).
Campo “Características dos Comutadores”: 
•Campo “Comutador sob carga”: indica o número de posições associando a tensão correspondente à 
identificação das posições mínima, máxima e nominal, bem como a variação de tensão entre cada 
posição, conforme os dados de placa do transformador. Caso os números das posições dos dados de 
placa do transformador sejam diferentes dos números das posições utilizadas no Sistema de Supervisão 
e Controle, incluir, além das posições dos dados de placa, também as posições utilizadas no SSC. Caso 
não exista comutador sob carga no transformador, o campo é preenchido com “NE” – não existe.
•Campo “Comutador em Vazio”: indica as posições do comutador em vazio com os respectivos valores de 
tensão e sua posição em serviço. Caso não exista comutador em vazio no transformador, o campo é 
preenchido com “NE” – não existe.
Campo “Notas / Restrições”: campo opcional que indica informações relevantes a respeito do 
transformador.
Caso os dados operacionais não sejam informados pelo Agente, os campos são preenchidos com “NI” – não 
informado.
4.1.SE DIANÓPOLIS II - AUTOTRANSFORMADOR DDAT6-01 OU 02-230/138/13,8 KV - 200 MVA
Características dos ComutadoresDispositivo de 
Sincronismo Ajustes
Comutador sob Carga Comutador em Vazio
NI NI 1 = 253 kV
21 = 207 kV NE
Notas / Restrições: NI
4.2. SE GURUPI - AUTOTRANSFORMADOR AT01 500/230/13,8 KV - 450 MVA
Características dos Comutadores
Dispositivo de 
Sincronismo Ajustes
Comutador sob Carga Comutador em Vazio
NI NI
1 = 550 kV
21 = 450 kV
NE
Notas / Restrições: NI
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Área 230 kV do Tocantins CD-CT.N.2TO.01 01 2.1.4. 28/04/2024
Referência: RT-ONS-DPL 0101/2024 Estudos pré-operacionais para integração ao SIN do setor de 138 KV da SE Gurupi 7 / 10
4.3. SE GURUPI - AUTOTRANSFORMADOR AT04 OU 05 230/138/13,8 KV - 100 MVA
Características dos ComutadoresDispositivo de 
Sincronismo Ajustes
Comutador sob Carga Comutador em Vazio
NI NI
1 = 253 kV
21 = 207 kV
NE
Notas / Restrições: NI
4.4. SE LAJEADO – AUTOTRANSFORMADOR LJAT7-01 OU 02 525/230/13,8 KV - 960 MVA
Características dos ComutadoresDispositivo de 
Sincronismo Ajustes
Comutador sob Carga Comutador em Vazio
NI NI NI
1 = 551,25
2 = 538,13
3 = 525,00
4 = 511,88
5 = 498,75
Tape em serviço = 4
Notas / Restrições: NI
4.5. SE PALMAS - AUTOTRANSFORMADOR PLAT6-01 OU 02 230/138/13,8 KV - 200 MVA
Características dos Comutadores
Dispositivo de 
Sincronismo Ajustes
Comutador sob Carga Comutador em Vazio
NI NI
230 kV±2,3 kV
1=253 kV
11 = 230 KV (central / nominal)
21=207 kV
NI
Notas / Restrições: NI
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da Área 230 kV do Tocantins CD-CT.N.2TO.01 01 2.1.4. 28/04/2024
Referência: 8 / 10
5. DADOS OPERACIONAIS DE UNIDADES GERADORAS
Orientações sobre os dados operacionais de unidades geradoras de usinas hidrelétricas: 
Código Único de Empreendimentos em Geração – CEG: está apresentado ao lado do nome da usina. O CEG permanece associado ao empreendimento, 
independentemente de alteração de atributos ou de proprietários da usina. O número CEG é obtido no site da ANEEL.
Campo “Unidade Geradora”: identifica cada unidade geradora conforme diagrama unifilar do agente.
Campo “Faixa de Tensão (kV)”:
•Campo “Nom.”: indica a tensão nominal da unidade geradora, em kV.
•Campo “Mín.”: indica a tensão mínima da unidade geradora, em kV.
•Campo “Máx.”: indica a tensão máxima da unidade geradora, em kV.
Campo “Potência Aparente (MVA)”:
•Campo “Nom.”: indica a potência aparente nominal da unidade geradora, em MVA.
•Campo “FP”: indica o fator de potência nominal da unidade geradora.
Campo “Potência Reativa (Mvar)”:
•Campo “Mín.”: indica a potência reativa mínima da unidade geradora, em Mvar, podendo ser referenciada a curva de capabilidade da unidade geradora.
•Campo “Máx.”: indica a potência reativa máxima da unidade geradora, em Mvar, podendo ser referenciada a curva de capabilidade da unidade geradora.
Campo “Faixa de Operação (MW)”:
•Campo “Permitida”: indica a faixa de potência ativa permitida da unidade geradora, em MW, associada a um determinado nível do reservatório, caso necessário.
•Campo “Proibida”: indica a faixa de potência ativa máxima da unidade geradora, em MW, associada a um determinado nível do reservatório, caso necessário.
Campo “Dispositivos de Sincronismo”: indica o disjuntor onde é efetuado o sincronismo da unidade geradora.
Campo “Operação como Compensador Síncrono”:
•Campo “Unidade Geradora”: indica a identificação de cada unidade geradora conforme diagrama unifilar do agente.
•Campo “Conversão / Reversão (min)”: indica o tempo necessário para realizar a conversão de gerador para síncrono e síncrono para gerador, em minutos.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da Área 230 kV do Tocantins CD-CT.N.2TO.01 01 2.1.4. 28/04/2024
Referência: 9 / 10
Campo “Notas / Restrições”: campo opcional que indica informações relevantes a respeito da usina.
Caso os dados operacionais não sejam informados pelo Agente, os campos são preenchidos com “NI” – não informado.
Caso os dados operacionais não se apliquem, os campos são preenchidos com hifens “---”.
5.1. UHE LAJEADO (CEG: UHE.PH.TO.001304-8.01)
5.1.1. DADOS DAS UNIDADES GERADORAS DA UHE LAJEADO
Tensão (kV)
Potência 
Aparente 
(MVA)
Potência 
Reativa 
(Mvar)
Faixa de Operação
(MW)
Operação como 
Compensador Síncrono
Unidade geradora
Nom. Mín. Máx. Nom. FP Mín. Máx. Permitida Proibida
Dispositivo de 
Sincronismo
Unidade 
Geradora
Conversão / 
Reversão
(min)
1, 2 ,3 ,4 e 5 13,8 13,1 14,5 190 0,95 Ver curva 
capabilidade 100 a 180,5 < 100 NI Não ---
Notas / Restrições: Em função de instabilidades cavitacionais nas Unidades Geradoras com geração inferior a 100 MW, resultando em trincas no aro câmara e 
aquecimentos em mancais das turbinas, o limite mínimo de geração por UG é de 100 MW. Esta orientação se sobrepõe à curva de capabilidade. Para potências 
superiores a 100 MW fica valendo a curva de capabilidade e suas faixas proibitivas em função da queda.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da Área 230 kV do Tocantins CD-CT.N.2TO.01 01 2.1.4. 28/04/2024
Referência: 10 / 10
5.1.2. CURVA DE CAPABILIDADE DAS UNIDADES GERADORAS DA UHE LAJEADO

