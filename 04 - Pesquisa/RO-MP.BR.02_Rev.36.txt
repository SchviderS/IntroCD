Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.13
Rotina Operacional
Elaboração, Revisão, Distribuição e Implantação de Documentos Operacionais
Código Revisão Item Vigência
RO-MP.BR.02 36 4.1.5. 29/07/2022
.
MOTIVO DA REVISÃO
- Inclusão das rotinas RO-SC.BR.05 e RO-SC.BR.06 no anexo 1d.
LISTA DE DISTRIBUIÇÃO
CNOS COSR-SE COSR-NE COSR-NCO COSR-S
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Elaboração, Revisão, Distribuição e Implantação de 
Documentos Operacionais RO-MP.BR.02 36 4.1.5. 29/07/2022
Referência: 2 / 14
INDICE
1. OBJETIVO.........................................................................................................................................3
2. REFERÊNCIAS ...................................................................................................................................3
3. CONCEITOS ......................................................................................................................................3
4. CONSIDERAÇÕES GERAIS .................................................................................................................3
5. DESCRIÇÃO DO PROCESSO ...............................................................................................................3
5.1. Descrição Geral .............................................................................................................................3
5.2. Elaboração e Revisão dos Cadastros de Informações Operacionais .............................................4
5.3. Elaboração e Revisão de Instruções de Operação.........................................................................4
5.4. Elaboração e Distribuição de Mensagens Operativas ...................................................................5
5.5. Elaboração e Revisão de Rotinas Operacionais.............................................................................6
5.6. Elaboração e Revisão de Ajustamentos Operativos ......................................................................6
5.7. Elaboração e Revisão de Referências Técnicas .............................................................................6
5.8. Documentação Operacional Vinculada A evento..........................................................................7
5.9. Implantação e Vigenciamento da Documentação Operacional ....................................................7
6. ATRIBUIÇÕES DOS AGENTES E DO ONS ............................................................................................8
7. ORIENTAÇÕES TÉCNICAS COMPLEMENTARES ..................................................................................8
8. ANEXOS ...........................................................................................................................................9
ANEXO 1 - Responsabilidade pela Elaboração, Revisão e Emissão dos Documentos do MPO................9
ANEXO 2 - Prazos para Emissão dos Documentos Operacionais...........................................................14
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Elaboração, Revisão, Distribuição e Implantação de 
Documentos Operacionais RO-MP.BR.02 36 4.1.5. 29/07/2022
Referência: 3 / 14
1. OBJETIVO
Estabelecer os procedimentos para elaboração, revisão, distribuição e implantação de Cadastros de 
Informações Operacionais (CD), Instruções de Operação (IO), Mensagens Operativas (MOP), Rotinas 
Operacionais (RO), Ajustamentos Operativos (AO) e Referências Técnicas (RT).
2. REFERÊNCIAS
Submódulo 1.1 dos Procedimentos de Rede.
3. CONCEITOS
3.1. Instrução de Operação (IO): definição segundo o Submódulo 1.2 - Glossário dos Procedimentos de 
Rede.
3.2. Mensagem Operativa (MOP): definição segundo o Submódulo 1.2 - Glossário dos Procedimentos de 
Rede.
3.3. Rotina Operacional (RO) - definição segundo o Submódulo 1.2 - Glossário dos Procedimentos de Rede.
3.4. Referência Técnica (RT) – definição segundo o Submódulo 1.2 - Glossário dos Procedimentos de Rede.
3.5. Cadastros de Informações Operacionais (CD) – definição segundo o Submódulo 1.2 - Glossário dos 
Procedimentos de Rede.
3.6. Ajustamentos Operativos (AO) – definição segundo o Submódulo 1.2 - Glossário dos Procedimentos 
de Rede.
4. CONSIDERAÇÕES GERAIS
4.1. Esta Rotina Operacional considera que o termo “Documento Operacional” é uma referência genérica 
para qualquer um dos documentos citados nos itens 3.1 a 3.6 desta rotina, para os Regulamentos 
Internacionais e as Referências Técnicas.
4.2. No Anexo 1 constam os responsáveis pela elaboração/revisão de documentos normativos que não 
são de responsabilidade da gerência de Procedimentos Operativos.
5. DESCRIÇÃO DO PROCESSO 
5.1. DESCRIÇÃO GERAL
5.1.1. A gerência de Procedimentos Operativos é responsável pela elaboração e revisão (a menos dos 
documentos citados no Anexo 1), bem como da distribuição e implantação dos documentos 
operativos. 
5.1.2. Quando do início do processo de revisão ou elaboração de um documento operacional, a gerência 
de Procedimentos Operativos deve emitir minuta do documento e avaliar o envolvimento das demais 
áreas do ONS ou de Agentes, devendo esses receberem o documento que foi revisado ou elaborado 
ou serem convocados para participação na sua elaboração ou revisão. Para elaboração das minutas 
devem ser considerados os responsáveis constantes no Anexo 1 e prazos definidos no Anexo 2.  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Elaboração, Revisão, Distribuição e Implantação de 
Documentos Operacionais RO-MP.BR.02 36 4.1.5. 29/07/2022
Referência: 4 / 14
5.1.3. A gerência de Procedimentos Operativos deve verificar possíveis reflexos em outros documentos do 
MPO.
5.1.4. Os Agentes envolvidos devem efetuar a adequação de sua documentação operativa interna de 
acordo com os procedimentos estabelecidos pelo ONS.
5.1.5. As revisões de documentos operacionais, motivadas por correção de paginação do documento, 
palavras incompletas, itemização, incorporações de MOPs sem inclusão de novo conteúdo, 
adequação da lista de distribuição, não necessitam ser encaminhadas em forma de minuta, no 
entanto, deverão, mesmo assim, obedecer aos prazos citados no Anexo 2b. 
5.1.6. Decorridos os prazos relacionados no Anexo 2, a gerência de Procedimentos Operativos emite a 
versão final do documento operacional, incorporando nesse as contribuições enviadas pelos seus 
usuários.
5.1.7. A gerência de Procedimentos Operativos efetua a distribuição da versão final do documento 
operacional de acordo com lista de distribuição específica. 
5.1.8. A vigência de um Documento Operacional se dá quando da ocorrência da data constante no campo 
“Vigência” na capa desse.  Caso o documento esteja vinculado à entrada em operação de uma nova 
obra, sem data de vigência determinada, o campo “Vigência” deve ser preenchido com asterisco (*) 
e o processo segue conforme descrito pelo item 5.5 desta Rotina Operacional.
5.1.9. Os documentos operacionais devem ser disponibilizados para utilização aos seus usuários com a 
antecedência descrita no Anexo 2.
5.1.10. Os prazos de envio de versões finais de documentos operacionais devem ser observados também em 
função da antecedência necessária para a realização de implantação desses com os operadores de 
sistema.
5.1.11. A forma de implantação dos documentos operacionais com os operadores de sistema é função do 
porte das alterações introduzidas nos documentos, cujos prazos estão explicitados no Anexo 2 desta 
Rotina Operacional.
5.1.12. Caso haja alteração no código de um documento já existente, deve ser elaborado um novo 
documento iniciando pela revisão zero. A alteração do assunto ou item não caracteriza a necessidade 
de elaboração de um novo documento.
5.2. ELABORAÇÃO E REVISÃO DOS CADASTROS DE INFORMAÇÕES OPERACIONAIS
5.2.1. Os Cadastros de Informações Operacionais (CD) podem ser implantados de forma imediata, devido 
ao fato de esses documentos não conterem procedimentos operativos e, portanto, dispensam os 
prazos estabelecidos, constantes nas tabelas do Anexo 2.
5.3. ELABORAÇÃO E REVISÃO DE INSTRUÇÕES DE OPERAÇÃO
5.3.1. O processo de elaboração ou de revisão de uma Instrução de Operação é iniciado a partir de um dos 
seguintes eventos:
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Elaboração, Revisão, Distribuição e Implantação de 
Documentos Operacionais RO-MP.BR.02 36 4.1.5. 29/07/2022
Referência: 5 / 14
a) Criação de nova Regra de Operação ou alterações em Regra de Operação já existente, com reflexos 
nos procedimentos operacionais vigentes;
b) Emissão de diretrizes operativas constantes de estudos elétricos, energéticos ou hidrológicos, 
oriundos do ONS;
c) Informações provenientes dos Agentes relativas às disponibilidades, restrições operacionais ou 
alterações de limites, de instalações, de equipamentos ou de linhas de transmissão e mudanças na 
configuração das instalações;
d) Incorporação de Mensagens Operativas contendo procedimentos que alterem Instruções de 
Operação;
e) Informações provenientes da Gerência de Apuração da Operação e da Gerência de Análise de 
Desempenho e de Custos da Operação, em função de ocorrências verificadas na Rede;
f)  Informações recebidas das equipes de tempo real do ONS;
g) Entrada em operação de novos equipamentos.
5.4. ELABORAÇÃO E DISTRIBUIÇÃO DE MENSAGENS OPERATIVAS
5.4.1.1. O processo de elaboração e posterior emissão de uma Mensagem Operativa pode ser iniciado a 
partir de um dos seguintes eventos:
a) Ocorrências imprevistas na Rede de Operação que resultem em alteração de procedimentos 
operacionais, sem que haja tempo hábil para que se cumpram os prazos do anexo 2;
b) Atendimento a condições especiais e temporárias de operação, decorrentes de eventos, 
solenidades, comemorações na sociedade ou de otimização energética;
c) Atendimento a situações que requeiram alteração urgente de procedimentos operativos;
d) Necessidade de alteração de procedimentos cuja respectiva IO esteja programada para entrar 
em vigência em função de outros eventos;
5.4.1.2. Uma MOP somente deve permanecer em vigência até que o seu conteúdo seja incluído em IO ou 
enquanto perdurar a condição especial de operação que motivou sua emissão.
5.4.1.3. Quando a condição que motivou a emissão de uma MOP for caracterizada como permanente ou 
de longa duração, os procedimentos nela contidos devem ser motivo de inclusão em IO pertinente. 
5.4.1.4. O cancelamento de uma MOP só poderá ocorrer pelo fim da condição de operação que motivou 
sua emissão, pela emissão de outra MOP que a altere ou pela inclusão de seus procedimentos em 
instrução de operação. 
5.4.1.5. A itemização da Mensagem Operativa deve seguir o estabelecido na Referência Técnica 
RT.MP.BR.02.
A MOP de abrangência sistêmica deve ser emitida quando envolver equipamentos sistêmicos, CAG, 
operação energética que impacte nas interligações e documentos operacionais referentes a todo o 
SIN. Os demais casos devem ser tratados como MOP de abrangência regional.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Elaboração, Revisão, Distribuição e Implantação de 
Documentos Operacionais RO-MP.BR.02 36 4.1.5. 29/07/2022
Referência: 6 / 14
5.4.1.6. Toda MOP que altera alguma instrução de operação deve ser encaminhada conforme lista de 
distribuição das respectivas instruções. 
5.5. ELABORAÇÃO E REVISÃO DE ROTINAS OPERACIONAIS
O processo de elaboração ou de revisão de uma Rotina Operacional é iniciado a partir de um dos 
seguintes eventos:
5.5.1. Criação ou mudança de procedimentos ou metodologia correlatos às atividades das gerências da 
Diretoria de Operação, descrevendo os procedimentos para o desenvolvimento dessas atividades, 
bem como o conteúdo e a forma de seus produtos.
5.5.2. Criação ou mudança de procedimentos ou metodologia correlatos às atividades das gerências do 
ONS que se relacionam com a operação em tempo real, descrevendo os procedimentos para o 
desenvolvimento dessas atividades, bem como o conteúdo e a forma de seus produtos.
5.5.3. Criação de novos submódulos dos Procedimentos de Rede ou alteração dos existentes, com reflexos 
nos procedimentos operacionais vigentes;
5.5.4. Emissão de diretrizes operativas constantes de estudos elétricos, energéticos ou hidrológicos, 
oriundos do ONS;
5.5.5. Informações provenientes da Gerência de Apuração da Operação e da Gerência de Análise de 
Desempenho e de Custos da Operação, em função de ocorrências verificadas na Rede.
5.6. ELABORAÇÃO E REVISÃO DE AJUSTAMENTOS OPERATIVOS
O processo de elaboração ou de revisão de um ajustamento operativo é iniciado a partir de um dos 
seguintes eventos:
5.6.1. Entrada em operação ou existência de equipamento ou conjunto de equipamentos não pertencentes 
à Rede de Operação cuja operação ou indisponibilidade possa provocar alguma influência relevante 
na rede de operação;
5.6.2. Criação de novos submódulos dos Procedimentos de Rede ou alteração dos existentes, com reflexos 
nos procedimentos operacionais vigentes;
5.6.3. Informações provenientes da Gerência de Apuração da Operação e da Gerência de Análise de 
Desempenho e de Custos da Operação, em função de ocorrências verificadas na Rede.
5.7. ELABORAÇÃO E REVISÃO DE REFERÊNCIAS TÉCNICAS
O processo de elaboração ou de revisão de uma Referência Técnica é iniciado a partir de um dos 
seguintes eventos:
5.7.1. Necessidade de estabelecer em documentação técnica da operação, necessidades evidenciadas no 
dia a dia, onde poderão estar contidos conceitos e critérios a serem utilizados pelos documentos 
operacionais. Ex.: Referência Técnica de Conceitos Básicos para o Controle de Geração - RT-CG.BR.02; 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Elaboração, Revisão, Distribuição e Implantação de 
Documentos Operacionais RO-MP.BR.02 36 4.1.5. 29/07/2022
Referência: 7 / 14
5.7.2. Estabelecimento ou modificação de padrões existentes para as instruções de operação, com a 
definição específica de seu conteúdo e finalidade;
5.7.3. Criação de novos submódulos dos Procedimentos de Rede ou alteração dos existentes, com reflexos 
nos procedimentos operacionais vigentes;
5.7.4. Informações provenientes da Gerência de Apuração da Operação e da Gerência de Análise de 
Desempenho e de Custos da Operação, em função de ocorrências verificadas na Rede.
5.8. DOCUMENTAÇÃO OPERACIONAL VINCULADA A EVENTO
5.8.1. Os documentos devem ser encaminhados com vigência condicionada à data de ocorrência do 
evento.  Desta forma, a IO será encaminhada sem data de vigência definida, com o campo relativo 
à data preenchido apenas com o asterisco, entre parênteses.
5.8.2. Quando da ocorrência do evento, a equipe de tempo real deve confirmar a vigência do documento 
operativo no ECM/MPO conforme sua área de atuação. Após essa confirmação, o ECM/MPO 
atualiza a data no campo Vigência e efetua a distribuição do documento vigente automaticamente.
5.9. IMPLANTAÇÃO E VIGENCIAMENTO DA DOCUMENTAÇÃO OPERACIONAL 
5.9.1. A implantação consiste no contato com outras gerências para adequações no Sistema de Supervisão 
e Controle ou em aplicativos de suporte, e em treinamentos aos operadores considerando o porte 
das modificações dos documentos operativos, conforme RO-MP.BR.03 - Treinamento dos 
Operadores de Sistema dos Centros de Operação do ONS. 
Deve ser realizada após a emissão da versão final dos documentos operativos. 
5.9.2. Devem ser observados os prazos para a definição da data de vigência, conforme o Anexo 2.
5.9.3. As revisões são classificadas quanto ao seu porte e se aplicam a todos os documentos operacionais 
com exceção das Mensagens Operativas e Cadastros Operacionais. 
5.9.3.1. Revisões de Pequeno porte: são as alterações em documentos, motivadas, entre outras, pelas 
seguintes causas:
Alterações de procedimentos para controle de tensão, controle de carregamento, controle de 
geração, operação de reservatórios e de esquemas especiais, sem alteração de filosofia;
Implantação de novos esquemas especiais sem procedimentos associados;
Entrada em operação de unidades geradoras, equipamentos linhas de transmissão e 
seccionamentos que não alteram filosofia de operação.
5.9.3.2. Revisões de Médio porte: são as alterações em documentos, motivadas, entre outras, pelas 
seguintes causas:
Alterações de procedimentos para controle de tensão, controle de carregamento, controle de 
geração e de operação de reservatórios, com alteração de filosofia;
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Elaboração, Revisão, Distribuição e Implantação de 
Documentos Operacionais RO-MP.BR.02 36 4.1.5. 29/07/2022
Referência: 8 / 14
Alterações em Esquemas Especiais de Proteção com mudança de filosofia de esquemas especiais 
já existentes ou instalação de novos esquemas especiais com procedimentos associados;
Entrada em operação de unidades geradoras, equipamentos, linhas de transmissão e 
seccionamentos que alteram filosofia de operação;
Inclusão de novos montantes de carga e de geração ou alterações na sequência de instalações 
restabelecidas na área de recomposição.
5.9.3.3. Revisões de Grande porte: são as alterações em documentos, motivadas, entre outras, pelas 
seguintes causas:
Novas áreas de recomposição;
Novas instalações em corrente contínua.
6.  ATRIBUIÇÕES DOS AGENTES E DO ONS
O ONS deve elaborar, revisar, distribuir e implantar os documentos operacionais do MPO.
Os agentes devem:
apreciar as minutas dos documentos operativos encaminhados pelo ONS e apresentar sugestões e 
comentários, caso existam; 
implantar os procedimentos operativos definidos pelo ONS com os operadores de seus centros ou 
instalações, observando a vigência definida pelo ONS;
manter atualizadas e encaminhar à gerência de Procedimentos Operativos as informações 
necessárias à elaboração e à revisão dos documentos operativos;
informar em tempo hábil as modificações na configuração de suas instalações, troca de 
equipamentos, alteração em limites operacionais em equipamentos ou linhas de transmissão, 
modificações nos processos operativos ou qualquer outro evento com repercussões na Rede de 
Operação para que a gerência de Procedimentos Operativos avalie a necessidade de adequação dos 
documentos operativos.
7. ORIENTAÇÕES TÉCNICAS COMPLEMENTARES
Não se aplica.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Elaboração, Revisão, Distribuição e Implantação de 
Documentos Operacionais RO-MP.BR.02 36 4.1.5. 29/07/2022
Referência: 9 / 14
8. ANEXOS
ANEXO 1 - RESPONSABILIDADE PELA ELABORAÇÃO, REVISÃO E EMISSÃO DOS DOCUMENTOS DO MPO
PR Conteúdo Responsável pela elaboração / revisão / 
emissão
5.11 Cadastros de Informações Operacionais Procedimentos Operativos
5.12 Instruções de Operação Procedimentos Operativos
5.12 Mensagens Operativas Sistêmicas / Regionais Procedimentos Operativos
5.13 Rotinas Operacionais Vide Anexo 1a
5.14 Ajustamentos Operativos Procedimentos Operativos
5.15 Regulamentos Internacionais Procedimentos Operativos / Brasília e 
Florianópolis
--- Referências Técnicas Procedimentos Operativos
Anexo 1a - Rotinas Operacionais Gerais
Item do 
MPO Rotinas Gerais
Responsável pela 
elaboração/revisão
Responsável pela 
emissão
4.1.3.
RO-RD.BR.01 – Definição de Rede a que 
Pertencem os Equipamentos de uma 
Instalação do SIN
Configurações de 
Rede
Procedimentos 
Operativos / Recife
4.1.4.
RO-GC.BR.02 - Operacionalização do 
Programa Piloto de Utilização do Mecanismo 
de Resposta da Demanda
Gerência de Apuração 
da Operação
Procedimentos 
Operativos / Brasília
4.1.5.
RO-MP.BR.05 - Controle dos Diagramas 
Unifilares Operacionais
Configurações de 
Rede
Procedimentos 
Operativos / Recife
RO-RR.BR.01 - Testes Reais de Recomposição 
nas Usinas de Autorrestabelecimento
Configurações de 
Rede
Procedimentos 
Operativos / Recife
4.1.6.
RO-RR.BR.02 - Testes Simulados de 
Recomposição na Rede de Operação
Configurações de 
Rede / 
Procedimentos 
Operativos
Procedimentos 
Operativos / Recife
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Elaboração, Revisão, Distribuição e Implantação de 
Documentos Operacionais RO-MP.BR.02 36 4.1.5. 29/07/2022
Referência: 10 / 14
Item do 
MPO Rotinas Gerais
Responsável pela 
elaboração/revisão
Responsável pela 
emissão
RO-RO.BR.01 - Comunicação Verbal na 
Operação Tempo Real Procedimentos 
Operativos / Recife
4.1.7. RO-RO.BR.02 - Designação de Interlocutores 
para o Relacionamento Operacional entre os 
Centros de Operação do ONS e Agentes
Configurações de 
Rede
Procedimentos 
Operativos / Recife
RO-MP.BR.03 - Treinamento dos Operadores 
de Sistema dos Centros de Operação do ONS 
Tempo real / 
Configurações de 
Rede / 
Procedimentos 
Operativos
Procedimentos 
Operativos / Recife
4.1.10. 
RO-MP.BR.04 - Certificação de 1ª Parte de 
Operadores de Sistema e de Instalações 
Tempo real / 
Configurações de 
Rede / 
Procedimentos 
Operativos
Procedimentos 
Operativos / Recife
4.1.11. 
RO-CB.BR.01 - Controles mínimos de 
segurança cibernética para o Ambiente 
Regulado Cibernético
Governança de TI, 
Escritório de Projetos 
e Segurança 
Cibernética
Procedimentos 
Operativos / 
Florianópolis
Anexo 1b - Rotinas Operacionais da Pré-Operação
Item do 
MPO Rotinas Gerais Responsável pela 
elaboração/revisão
Responsável pela 
emissão
RO-EP.BR.01 - Programação de Intervenções Estudos de 
Intervenções na Rede
Procedimentos 
Operativos / 
Florianópolis
4.2.1.
RO-EP.BR.02 - Elaboração do Programa 
Diário de Operação
Estudos de 
Intervenções na Rede
Procedimentos 
Operativos / 
Florianópolis
RO-OR.BR.01 - Sobreaviso de Hidrologia para 
Controle de Cheias nas Bacias Hidrográficas Tempo Real
Procedimentos 
Operativos / 
Florianópolis
4.2.2. RO-OR.BR.02 - Teleconferência da Operação 
Hidráulica para Controle de Cheias nas 
Bacias Hidrográficas 
Tempo Real
Procedimentos 
Operativos / 
Florianópolis
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Elaboração, Revisão, Distribuição e Implantação de 
Documentos Operacionais RO-MP.BR.02 36 4.1.5. 29/07/2022
Referência: 11 / 14
Anexo 1c - Rotinas Operacionais da Pós Operação
Item do 
MPO Rotinas Gerais Responsável pela 
elaboração/revisão
Responsável pela 
emissão
RO-AN.BR.01 - Elaboração do Relatório de 
Análise de Ocorrência - RO
Análise de 
Desempenho e de 
Custos da Operação
Procedimentos 
Operativos / Rio de 
Janeiro
RO-AN.BR.02 - Elaboração do Relatório de 
Análise da Operação - RAO
Análise de 
Desempenho e de 
Custos da Operação
Procedimentos 
Operativos / Rio de 
Janeiro
RO-AN.BR.04 - Informações e Dados sobre 
Perturbações
Análise de 
Desempenho e de 
Custos da Operação / 
Tempo Real
Procedimentos 
Operativos / Rio de 
Janeiro 
4.3.1.
RO-EA.BR - Gestão de Recomendações e 
Providências em andamento dos Relatórios 
de Análise
Análise de 
Desempenho e de 
Custos da Operação
Procedimentos 
Operativos / Rio de 
Janeiro
RO-AO.BR.02 - Apuração dos Dados 
Hidrológicos e Hidráulicos
Gerência de Apuração 
da Operação
Procedimentos 
Operativos / Brasília
RO-AO.BR.03 - Consistência dos Dados de 
Perturbações  
Gerência de Apuração 
da Operação
Procedimentos 
Operativos / Brasília
RO-AO.BR.04 - Apuração das Mudanças de 
Estados Operativos de Unidades Geradoras, 
Usinas e Interligações Internacionais
Gerência de Apuração 
da Operação
Procedimentos 
Operativos / Brasília
RO-AO.BR.05 - Apuração de Eventos em 
Instalações do Sistema de Transmissão  
Gerência de Apuração 
da Operação
Procedimentos 
Operativos / Brasília
RO-AO.BR.06 - Apuração das Interrupções do 
Serviço da Rede Básica nos Pontos de 
Controle
Gerência de Apuração 
da Operação
Procedimentos 
Operativos / Brasília
RO-AO.BR.07 - Apuração dos Dados para 
Composição da Carga Global do Sistema 
Interligado Nacional
Gerência de Apuração 
da Operação
Procedimentos 
Operativos / Brasília
4.3.2.
RO-AO.BR.08 - Apuração dos Dados de 
Despacho de Geração e do Intercâmbio
Gerência de Apuração 
da Operação
Procedimentos 
Operativos / Brasília
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Elaboração, Revisão, Distribuição e Implantação de 
Documentos Operacionais RO-MP.BR.02 36 4.1.5. 29/07/2022
Referência: 12 / 14
Item do 
MPO Rotinas Gerais Responsável pela 
elaboração/revisão
Responsável pela 
emissão
RO-AO.BR.11 - Apuração das Sobrecargas 
em Transformadores da Rede Básica  
Gerência de Apuração 
da Operação
Procedimentos 
Operativos / Brasília
RO-AO.BR.12 - Apuração do Uso do Sistema 
de Transmissão  
Gerência de Apuração 
da Operação
Procedimentos 
Operativos / Brasília
Anexo 1d - Rotinas Operacionais da Supervisão & Controle e Telecomunicações
Item do 
MPO Rotinas Gerais Responsável pela 
elaboração/revisão
Responsável pela 
emissão
RO-SC.BR.01 - Identificação dos 
Equipamentos e Medidas do REGER
Relação com 
Programação, 
Operação e Apuração
Procedimentos 
Operativos / 
Florianópolis 
RO-SC.BR.02 - Protocolos de Comunicação 
com o Sistema de Supervisão e Controle do 
ONS  
Relação com 
Programação, 
Operação e Apuração
Procedimentos 
Operativos / 
Florianópolis 
RO-SC.BR.03 - Manutenção dos Dados 
Elétricos da Base de Dados Técnica  
Relação com 
Programação, 
Operação e Apuração
Procedimentos 
Operativos / 
Florianópolis 
RO-SC.BR.04 – Padrão para Encaminhamento 
das Informações para a Base de Dados do 
Sistema de Supervisão e Controle do ONS
Relação com 
Programação, 
Operação e Apuração
Procedimentos 
Operativos / 
Florianópolis
RO-SC.BR.05 - Identificação de PMU, de 
Fasores e de Fluxos Sincrofasoriais Enviados 
ao SMSF do ONS
Relação com 
Programação, 
Operação e Apuração
Procedimentos 
Operativos / 
Florianópolis
4.4.1.
RO-SC.BR.06 - Testes para Integração dos 
Dados Fasoriais dos Agentes no SMSF do ONS
Relação com 
Programação, 
Operação e Apuração
Procedimentos 
Operativos / 
Florianópolis
4.4.2.
RO-TC.BR.01 – Instalação dos Equipamentos 
dos Agentes e Provedores nas Instalações do 
ONS
Relação com 
Programação, 
Operação e Apuração
Procedimentos 
Operativos / 
Florianópolis 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Elaboração, Revisão, Distribuição e Implantação de 
Documentos Operacionais RO-MP.BR.02 36 4.1.5. 29/07/2022
Referência: 13 / 14
Anexo 1e - Rotinas específicas
Item do 
MPO Rotinas Gerais Responsável pela 
elaboração/revisão
Responsável pela 
emissão
4.5.
RO-EP.BR.IT - Programação de 
Intervenções Envolvendo Equipamentos 
que Influenciam na Operação da Itaipu 
Binacional
Estudos de Intervenções 
na Rede
Procedimentos 
Operativos / Brasília
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Elaboração, Revisão, Distribuição e Implantação de 
Documentos Operacionais RO-MP.BR.02 36 4.1.5. 29/07/2022
Referência: 14 / 14
ANEXO 2 - PRAZOS PARA EMISSÃO DOS DOCUMENTOS OPERACIONAIS
2.a) Prazos considerados na emissão das minutas dos Documentos Operacionais
Prazo mínimo (dias)
Providência Revisões de 
pequeno porte
Revisões de 
médio porte
Revisões de 
grande porte
Antecedência mínima, em relação ao prazo final para 
comentários, para envio da minuta às áreas do ONS e 
Agentes envolvidos.
2 3 5
Observações:
Os dias a serem considerados são dias úteis.
Observar os exemplos de alterações de pequeno, médio e grande porte no item 5.9 deste documento.
2.b) Prazos considerados na emissão das versões finais dos Documentos Operacionais
Prazo mínimo (dias)
Providência Revisões de 
pequeno porte
Revisões de 
médio porte
Revisões de 
grande porte
Antecedência mínima, em relação à data de vigência, 
para emissão da versão final do documento consolidado. 2 5 15
Observações:
Os dias a serem considerados são dias úteis.
Observar os exemplos de alterações de pequeno, médio e grande porte no item 5.9 deste documento.
Os treinamentos para implantação das instruções de operação e MOP deve ser efetuado conforme 
RO-MP.BR.03.
