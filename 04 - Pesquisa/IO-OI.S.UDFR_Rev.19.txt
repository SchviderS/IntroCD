 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da UHE Dona Francisca 
 
 
Código Revisão Item Vigência 
IO-OI.S.UDFR 19 3.7.5.2. 10/10/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Correção do Subitem 2.2. 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S CPFL-T Cotesa 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Dona Francisca IO-OI.S.UDFR 19 3.7.5.2. 10/10/2024 
 
Referência:  2/8 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  3 
3.1. Unidades Geradoras ...................................................................................................................... 3 
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL ................................ ....................  3 
4.1. Procedimentos Gerais ................................................................................................................... 3 
4.2. Procedimentos Específicos ............................................................................................................ 4 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  4 
5.1. Procedimentos Gerais ................................................................................................................... 4 
5.2. Procedimentos para Recomposição Fluente ................................................................................. 5 
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................ 5 
5.2.2. Recomposição Fluente da Instalação .......................................................................... 5 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 6 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 6 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 6 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 6 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 6 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 6 
6. MANOBRAS DE UNIDADES GERADORAS ................................ ................................ ........................  7 
6.1. Procedimentos Gerais ................................................................................................................... 7 
6.2. Procedimentos Específicos ............................................................................................................ 7 
6.2.1. Desligamento de Unidades Geradoras ........................................................................ 7 
6.2.2. Sincronismo de Unidades Geradoras .......................................................................... 7 
7. NOTAS IMPORTANTES ................................ ................................ ................................ .................. 8 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Dona Francisca IO-OI.S.UDFR 19 3.7.5.2. 10/10/2024 
 
Referência:  3/8 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da UHE Dona Francisca, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da Instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando -se a complementariedade 
das ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR-S e a Instalação de propriedade da DFESA, é realizada pela 
Cotesa, agente responsável pela operação da Usina, por intermédio da UHE Dona Francisca. 
2.3. As unidades geradoras, equipamentos e linhas de transmissão desta Instalação fazem parte da Área 
230 kV Rio Grande do Sul. 
2.4. O COSR-S controla e supervisiona o despacho de geração desta Usina. 
2.5. Esta Usina: 
• é despachada centralizadamente; 
• está conectada na Rede de Operação; 
• não participa do Controle Automático da Geração – CAG; 
• é de autorrestabelecimento integral; 
• participa do processo de recomposição fluente da Área Itaúba. 
2.6. Os dados operacionais desta Usina  estão descritos no Cadastro de Informações Operacionais da 
respectiva Área Elétrica. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. UNIDADES GERADORAS 
As unidades geradoras G1 e G2 13,8 kV estão conectadas ao barramento de 230 kV da SE Dona Francisca, por 
meio dos transformadores elevadores TR-1 e TR -2 13,8/230 kV  e respectivos módulos nessa subestação, 
constante na Instrução de Operação IO-OI.S.DFR. 
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S.  
As faixas de controle de tensão para esse barramento estão estabelecidas no Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob responsabilidade da Instalação. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Dona Francisca IO-OI.S.UDFR 19 3.7.5.2. 10/10/2024 
 
Referência:  4/8 
 
4.1.3. A Usina deve manter os valores de geração de acordo com os valores programados, constantes no 
Programa Diário de Operação – PDO. Para atendimento dos valores programados constantes no PDO, 
não é necessária a autorização prévia do COSR-S. 
Qualquer alteração no valor de geração da usina em relação ao valor de geração constante no PDO 
somente pode ser executada após autorização do COSR-S. 
As reprogramações de geração quando de necessidades sistêmicas serão executadas pela Usina 
quando solicitadas pelo COSR-S. 
Após reprogramação de geração solicitada pelo COSR-S, o Agente somente poderá alterar a geração 
da Usina com autorização do ONS, inclusive para adoção de valores contidos no PDO. 
4.1.4. Os desvios de geração da usina em relação aos valores previstos no PDO ou em relação às 
reprogramações, devem ser controlados observando-se os valores máximos permitidos explicitados 
na Instrução de Operação IO-CG.BR.01 – Controle da Geração em Condição Normal. 
4.1.5. Quando não existir ou não estiver disponível a supervisão da usina para o ONS, essa deve seguir as 
orientações para envio de dados conforme Rotina Operacional RO-AO.BR.08 – Apuração de Dados de 
Despacho de Geração e de Intercâmbios. A Usina, deve registrar e informar imediatamente os 
seguintes dados ao COSR-S: 
• movimentação de unidades geradoras hidráulicas (mudança de estado operativo). 
• restrições e ocorrências na usina ou na conexão elétrica que afetem a disponibilidade de 
geração, com o respectivo valor da restrição, contendo o horário de início e término e a 
descrição do evento. 
• demais informações sobre a operação de suas Instalações, solicitadas pelo ONS. 
4.1.6. O controle de tensão por meio da geração ou absorção de potência reativa pelas unidades geradoras 
da Usina, é controlado pelo COSR-S, com comando e execução pelo Agente Operador da Instalação. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
Não se aplica. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de um desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação: caracterizado quando não há tensão em todos os terminais 
de suas linhas de transmissão. 
•  Desligamento parcial da Instalação:  qualquer outra configuração que não se enquadre como 
desligamento total. 
5.1.2. Quando de um desligamento total ou parcial, o Agente Operador da Instalação  deve fornecer ao 
COSR-S as informações a seguir: 
• horário da ocorrência; 
• configuração da instalação logo após a ocorrência; 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Dona Francisca IO-OI.S.UDFR 19 3.7.5.2. 10/10/2024 
 
Referência:  5/8 
 
• configuração da instalação após ações realizadas com autonomia pela sua operação. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2, sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia do Agente Operador da Instalação na 
recomposição, deve ser utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4. 
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE 
No caso de desligamento total, o Agente Operador deve configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores:  
• das unidades geradoras: 
G1 13,8 kV (de 230 kV na SE Dona Francisca); 
G2 13,8 kV (de 230 kV na SE Dona Francisca). 
Cabe ao Agente Cotesa informar ao COSR-S quando a configuração de preparação da Instalação não estiver 
atendida para o início da recomposição, independentemente de o equipamento ser próprio ou de outros 
agentes. Nesse caso, o COSR -S fará contato com os agentes envolvidos, para  identificar o motivo do não -
atendimento e, após confirmação do Agente Cotesa de que os barramentos estão com a configuração 
atendida, o COSR -S coordenará os procedimentos para recomposição, caso necessário, em função da 
configuração desta Instalação. 
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO 
A Instalação faz parte da recomposição da Área Itaúba. O Agente Operador deve adotar os procedimentos a 
seguir para recomposição fluente:  
Passo Executor Procedimentos Condições ou Limites Associados 
1 Cotesa A Verificar tensão no barramento de 
230 kV da SE Dona Francisca. 
 
2 Cotesa 
Partir uma unidade geradora, 
sincronizá-la e controlar a geração. 
Manter VUDFR-13,8 = 13,1 kV. 
Conforme procedimentos internos do 
Agente.  
2.1 Cotesa 
Elevar e manter a geração na UHE Dona 
Francisca num  valor igual ao fluxo de 
potência ativa na LT 230 kV Dona 
Francisca / Santa Maria 3 C1. 
Limitando em no máximo 50 MW. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Dona Francisca IO-OI.S.UDFR 19 3.7.5.2. 10/10/2024 
 
Referência:  6/8 
 
Passo Executor Procedimentos Condições ou Limites Associados 
2.2 Cotesa 
Partir a segunda unidade geradora, caso 
essa estivesse operando antes do 
desligamento, sincronizá-la e controlar 
a geração. 
Manter VUDFR-13,8 = 13,1 kV. 
Conforme procedimentos internos do 
Agente.  
Os demais equipamentos desta Instalação são restabelecidos sob controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2, enquanto não houver 
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia destes na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da instalação que seja: 
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, o 
Agente Operador  deve preparar a Instalação conforme Subitem 5.2 .1, sem necessidade de 
autorização do ONS. 
5.4.1.2. Para demais desligamentos parciais da Instalação, não há necessidade de preparação de Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador 
da Instalação deve recompor a Instalação conforme Subitem 5.2.2, sem necessidade de autorização 
do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., o Agente Operador 
da Instalação deve recompor a Instalação conforme Subitem 6.2.2, sem necessidade de autorização 
do ONS. 
O sincronismo das unidades geradoras , bem como a elevação de geração  nessas unidades,  é 
realizado conforme as condições definidas no Subitem 6.2.2. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Dona Francisca IO-OI.S.UDFR 19 3.7.5.2. 10/10/2024 
 
Referência:  7/8 
 
6. MANOBRAS DE UNIDADES GERADORAS  
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desligamento programado ou de urgência de unidades geradoras, e 
desenergização programada ou de urgência de equipamentos, só podem ser efetuados com controle 
do COSR-S. 
6.1.2. Os procedimentos para energização de equipamentos, ou sincronismo de unidades geradoras, após 
desligamentos programados, de urgência ou de emergência, só podem ser efetuados com controle 
do COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, ou para sincronismo de unidades geradoras, após desligamento automático sem 
atuação de proteção que impeça o retorno do equipamento, só podem ser executados com 
autonomia pelo Agente Operador da Instalação quando estiverem explicitados e estiverem atendidas 
as condições do Subitem 6.2.2. desta Instrução de Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.5. A partida de unidades geradoras deve seguir critérios próprios do Agente. O sincronismo à Rede de 
Operação só pode ser executado com autonomia pela operação da Usina quando estiver explicitado 
e estiverem atendidas as condições do Subitem 6.2.2. A tomada de carga deve ser realizada com 
controle do COSR-S. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESLIGAMENTO DE UNIDADES GERADORAS  
O desligamento de unidades geradoras é sempre controlado pelo COSR-S. 
6.2.2. SINCRONISMO DE UNIDADES GERADORAS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de unidades geradoras. 
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais unidades geradoras em operação, conforme explicitado nas condições de 
energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UHE 
Dona Francisca IO-OI.S.UDFR 19 3.7.5.2. 10/10/2024 
 
Referência:  8/8 
 
Unidade Geradora Procedimentos Condições ou Limites Associados 
Unidade Geradora 
G1 ou G2 13,8 kV 
Partir e sincronizar a unidade geradora. • 13,8 ≤ VUDFR-13,8 ≤ 14,5 kV; e 
• elevar a geração da unidade 
geradora, após autorização do 
COSR-S. 
7. NOTAS IMPORTANTES 
7.1. O operador deverá, quando a Usina não estiver gerando nem vertendo, abrir a válvula existente, 
destinada a manutenção da vazão sanitária; fechando -a quando a Usina voltar e gerar ou houver 
vertimento. O COSR-S deverá ser informado quando da abertura ou fechamento dessa válvula. 
