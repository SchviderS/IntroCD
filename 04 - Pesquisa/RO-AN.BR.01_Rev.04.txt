Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.13
Rotina Operacional
 Elaboração do Relatório de Análise de Ocorrência - RO
Código Revisão Item Vigência
RO-AN.BR.01 04 4.3.1. 27/07/2022
.
MOTIVO DA REVISÃO: 
Inclusão de referência para lista de interlocutores dos agentes envolvidos no processo de emissão deste 
tipo de relatório de análise
Adequação devido à alteração de nomenclatura do Sistema de Gestão de Recomendações, SGR, para 
Sistema de Gestão de Providências, SGP
LISTA DE DISTRIBUIÇÃO:
AOC ITT RSO Agentes de Operação
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
 Elaboração do Relatório de Análise de Ocorrência - RO RO-AN.BR.01 04 4.3.1. 27/07/2022
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. REFERÊNCIAS ...................................................................................................................................3
3. CONCEITOS ......................................................................................................................................3
4. CONSIDERAÇÕES GERAIS .................................................................................................................3
5. DESCRIÇÃO DO PROCESSO ...............................................................................................................3
5.1. Seleção das ocorrências para elaboração do RO...........................................................................3
5.2. Análise e Tratamento das Ocorrências..........................................................................................4
5.3. Elaboração e encaminhamento da minuta do RO:........................................................................4
5.4. Colaboração dos órgãos do ONS e agentes de operação envolvidos............................................5
5.5. Elaboração e encaminhamento da versão final do RO..................................................................6
6. ATRIBUIÇÕES DOS AGENTES E DO ONS. ...........................................................................................6
7. ORIENTAÇÕES TÉCNICAS COMPLEMENTARES ..................................................................................7
8. ANEXOS ...........................................................................................................................................7
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
 Elaboração do Relatório de Análise de Ocorrência - RO RO-AN.BR.01 04 4.3.1. 27/07/2022
1. OBJETIVO
Estabelecer os procedimentos para a elaboração do Relatório de Análise de Ocorrência - RO, de modo 
que, dispondo da rápida identificação das causas das anormalidades e dificuldades operativas 
verificadas nas ocorrências, seja agilizada a tomada de providências corretivas ou preventivas, tendo em 
vista a segurança e o aprimoramento da operação do Sistema Interligado Nacional - SIN.
2. REFERÊNCIAS
Submódulo 2.12 - Requisitos mínimos de supervisão e controle para a operação.
Submódulo 2.15 - Requisitos mínimos de telecomunicações
Submódulo 6.2 - Análise da operação, ocorrências e perturbações e acompanhamento das 
providências;
Rotina Operacional RO-EA.BR - Gestão de Providências dos Relatórios de Análise.
3. CONCEITOS
Não se aplica
4. CONSIDERAÇÕES GERAIS
4.1. Os critérios e procedimentos gerais para elaboração do RO estão estabelecidos no submódulo 6.2 
dos Procedimentos de Rede.
4.2. A abrangência das ocorrências para elaboração do RO é a Rede de Operação.
4.3. O RO é elaborado pelas equipes da Gerência de Análise de Desempenho e de Custos da Operação – 
AOC, da Gerência de Relação com Programação, Operação e Apuração – RSO, da Gerência de 
Telecomunicações – ITT, contando com a participação de outros órgãos do ONS, sempre que 
necessário, e dos agentes de operação envolvidos na ocorrência.
5. DESCRIÇÃO DO PROCESSO
O processo de elaboração do RO é composto das seguintes etapas:
5.1.  SELEÇÃO DAS OCORRÊNCIAS PARA ELABORAÇÃO DO RO
5.1.1. A equipe da AOC, na execução das atividades diárias de triagem das ocorrências e perturbações 
descritas no submódulo 6.2, caso identifique anormalidades ou dificuldades operativas que causaram 
dificuldades no controle de tensão, de frequência, de carregamento ou dos limites sistêmicos e que 
se enquadrem nos critérios estabelecidos no submódulo 6.2, iniciam o processo de elaboração do 
RO.
5.1.2. A atividade de triagem diária de ocorrências e perturbações no SIN consiste na pré-análise da 
operação do sistema com o objetivo de identificar ocorrências e perturbações caracterizadas por 
anormalidades, dificuldades, eventos indesejáveis ou desempenhos insatisfatórios que exigem 
tratamento por parte do ONS ou dos agentes, com o intuito de evitar reincidência do evento.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
 Elaboração do Relatório de Análise de Ocorrência - RO RO-AN.BR.01 04 4.3.1. 27/07/2022
5.1.3. A equipe da ITT, durante a análise do atendimento aos requisitos estabelecidos no submódulo 2.15, 
caso identifique ocorrências, eventos ou desempenhos indesejáveis, que se enquadrem nos critérios 
estabelecidos no submódulo 6.2, inicia o processo de elaboração do RO.
5.1.4. A equipe da RSO, durante a análise do atendimento aos requisitos estabelecidos no submódulo 2.12, 
caso identifique ocorrências, eventos ou desempenhos indesejáveis, que se enquadrem nos critérios 
estabelecidos no submódulo 6.2, iniciam o processo de elaboração do RO.
5.1.5. O processo de análise de ocorrência também pode ser iniciado por solicitação de um agente de 
operação desde que seja caracterizado um dos critérios para seleção de ocorrências que devem dar 
origem a um RO estabelecidos no submódulo 6.2.
5.2. ANÁLISE E TRATAMENTO DAS OCORRÊNCIAS
5.2.1. A equipe da AOC deve, a partir da apuração dos dados e informações sobre as ocorrências e 
perturbações envolvendo a Rede de Operação, identificar aquelas que devem ser objeto da 
elaboração de Relatórios descritos no submódulo 6.2. Caso haja indicação para a elaboração de 
Relatório de Análise de Perturbação – RAP, a elaboração é realizada com a interação entre as equipes 
das diretorias de operação e de planejamento da operação do ONS.
5.2.2. Dentre as demais ocorrências selecionadas, a equipe da AOC deve identificar aquelas que serão 
objeto de complementação de informações que não foram repassadas em tempo real e encaminhar 
solicitação de esclarecimentos aos agentes de operação, conforme submódulo 6.2.
5.2.2.1. Os agentes devem relatar as informações solicitadas pelo ONS, as ações tomadas para tratamento 
imediato do evento, além das medidas tomadas de forma a evitar a reincidência das 
anormalidades verificadas.
5.2.2.2. O agente tem um prazo de três (3) dias úteis para responder à solicitação de complementação de 
informações feita pelo ONS.
5.2.2.3. As ocorrências que, após complementação de informação, não se enquadram nos critérios 
estabelecidos pelo submódulo 6.2, serão classificadas e armazenados em banco de dados 
específico, para análise de desempenho do sistema.
5.3. ELABORAÇÃO E ENCAMINHAMENTO DA MINUTA DO RO:
5.3.1. Assim que identificada a necessidade de elaboração de um RO, a equipe responsável deve 
encaminhar aviso sobre o início de elaboração do RO aos órgãos do ONS e agentes de operação 
envolvidos, por meio de mensagem eletrônica.
5.3.2. A lista de interlocutores dos agentes de operação para recebimento das mensagens eletrônicas 
no processo de elaboração de um RO será composta pela versão mais atualizada dos 
representantes cadastrados no Sistema de Gestão de Providências – SGP, com perfil 
“Responsável Atendimento – Operação” para os relatórios emitidos pela AOC ou “Responsável 
Atendimento – Infraestrutura” para os relatórios emitidos pela ITT e pela RSO. Para tanto, os 
agentes envolvidos deverão realizar e manter atualizado seu cadastro no referido sistema 
seguindo as orientações do Manual do Usuário disponível neste link.  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
 Elaboração do Relatório de Análise de Ocorrência - RO RO-AN.BR.01 04 4.3.1. 27/07/2022
5.3.3. O RO deve ser elaborado de acordo com o modelo apresentado no Anexo 1 deste documento.
5.3.4. Para possibilitar a execução da análise, o responsável pela elaboração deve coletar todos os 
dados e informações referentes à ocorrência disponíveis no RDO - Relatório Diário de 
Operação, no histórico das telemedições e telessinalizações do SSC - Sistema de Supervisão e 
Controle, na BDT - Base de Dados Técnica, no PDO - Programa Diário de Operação, nos 
documentos normativos da operação, nas gravações das comunicações verbais da operação e 
demais fontes de informação disponíveis no ONS.
5.3.5. Quando necessário, deve-se solicitar informações complementares às equipes de tempo real 
do ONS envolvidas e aos agentes de operação envolvidos.
5.3.6. A ocorrência deve ser descrita de forma sucinta, com clareza e objetividade, identificando as 
anormalidades e dificuldades operativas em item específico.
5.3.7. Devem ser relacionadas as providências tomadas e as providências em andamento necessárias 
para corrigir as anormalidades ou dificuldades operativas ou para identificar suas causas 
através de análises complementares.
5.3.8. As providências em andamento emitidas devem seguir as seguintes orientações:
5.3.8.1. Ser fundamentadas nos resultados das análises efetuadas e, por conseguinte, ser 
intrinsecamente aderentes ao assunto originário do RO;
5.3.8.2. Explicitar o responsável pela conclusão da providência em andamento, identificando O 
QUE deve ser feito e o OBJETIVO da ação proposta. O responsável pelo atendimento das 
providências deve ser o agente proprietário dos equipamentos envolvidos no processo 
de análise. Para as providências relacionadas a Conjuntos de Usinas, o responsável pelo 
atendimento será o agente operador da instalação envolvida. No caso de providências 
internas ao ONS, o responsável pelo atendimento será a gerência envolvida;
5.3.8.3. Identificar uma única ação e um único responsável pela conclusão da providência em 
andamento, de forma a possibilitar a confirmação de sua implementação;
5.3.8.4. Propor prazos para conclusão das providências em andamento, a serem consolidados 
com os responsáveis pela ação proposta, exceto nos casos das providências que devem 
ser tomadas de imediato;
Quando necessário, deve-se anexar gráficos, diagramas unifilares e tabelas para ilustrar 
a análise, bem como documentos dos agentes de operação e relatórios técnicos 
complementares.
5.3.8.5. A minuta do RO deve ser encaminhada aos órgãos do ONS e agentes de operação 
envolvidos conforme prazos especificados no submódulo 6.2.
5.4. COLABORAÇÃO DOS ÓRGÃOS DO ONS E AGENTES DE OPERAÇÃO ENVOLVIDOS
5.4.1. Os órgãos do ONS e agentes de operação envolvidos devem apreciar e comentar a minuta do 
RO, informando providências já tomadas, bem como validar/alterar os prazos para conclusão 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
 Elaboração do Relatório de Análise de Ocorrência - RO RO-AN.BR.01 04 4.3.1. 27/07/2022
das providências em andamento sob sua responsabilidade, encaminhando ao responsável pela 
elaboração.
5.4.2. Quando a ocorrência envolver mais de um agente de operação, as alterações propostas por 
cada um dos agentes de operação envolvidos devem ser consolidadas pelo ONS, conciliando 
cada proposta com a respectiva área de responsabilidade de cada agente, antes da emissão da 
versão final.
5.4.3. Se os comentários dos órgãos do ONS e dos agentes de operação envolvidos não forem 
recebidos nos prazos previstos no submódulo 6.2, o relatório será considerado aprovado e 
deverá ser emitido, podendo manter a redação da minuta em sua versão final.
5.5. ELABORAÇÃO E ENCAMINHAMENTO DA VERSÃO FINAL DO RO
5.5.1. Depois de consolidar os comentários recebidos dos órgãos do ONS e dos agentes de operação 
envolvidos, relativos à minuta do RO, a equipe responsável deve elaborar a versão final e 
encaminhá-la para os envolvidos, nos prazos previstos no submódulo 6.2.
5.5.2. Quando houver divergências entre os comentários recebidos, ou a não concordância com eles, 
a equipe responsável deverá entrar em contato com os agentes de operação envolvidos para a 
obtenção do consenso. As divergências que eventualmente persistirem entre os agentes de 
operação envolvidos e o ONS devem ser apresentadas no item de anexos do RO, com a 
explicitação dos diferentes pontos de vista. A existência de divergências deve ser citada na 
descrição das anormalidades e dificuldades observadas.
5.5.3. Caso seja constatada a necessidade de aprofundamento das análises, o RO deverá ser 
cancelado e deverá ser iniciada a elaboração de um Relatório de Análise da Operação - RAO, 
cujos critérios estão estabelecidos no submódulo 6.2.
5.5.4. Caso sobrevenha a necessidade de se elaborar um Relatório de Análise de Perturbação - RAP, 
cujos critérios estão estabelecidos no submódulo 6.3, o RO deverá ser igualmente cancelado.
5.5.5. Caso seja constatado que o evento que motivou a análise não se enquadra nos critérios para 
sua elaboração, o relatório deverá ser cancelado.
5.5.6. As providências tomadas e as providências em andamento devem ser cadastradas no Sistema 
de Gestão de Providências - SGP, pela equipe responsável pela elaboração do relatório, no 
prazo de três dias úteis após a emissão e o encaminhamento da versão final do RO, com link do 
respectivo relatório devidamente cadastrado no SINtegre no macroprocesso adequado, para 
fins de acompanhamento e avaliações estatísticas.
5.5.7. As providências em andamento devem ser acompanhadas conforme estabelecido no 
submódulo 6.2 e na Rotina Operacional RO-EA.BR.
5.5.8. Os prazos envolvidos na elaboração do RO estão estabelecidos no submódulo 6.2.
6. ATRIBUIÇÕES DOS AGENTES E DO ONS.
As atribuições dos agentes e do ONS no processo de elaboração do RO estão estabelecidas no submódulo 6.2.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
 Elaboração do Relatório de Análise de Ocorrência - RO RO-AN.BR.01 04 4.3.1. 27/07/2022
7. ORIENTAÇÕES TÉCNICAS COMPLEMENTARES
Não se aplica.
8. ANEXOS
Relatório de Análise de Ocorrência – RO
 (Submódulo 6.2)
N.º: 
DOP-AOC ###/aaaa
ou
DTA-ITT ###/aaaa
ou
DTA-RSO ###/aaaa
Aviso: 
dd/mm/aaaa
Minuta: 
dd/mm/aaaa
Versão final: 
dd/mm/aaaa
Distribuição: ONS, Agentes de Operação.
Título (principal anormalidade ou dificuldade observada com a respectiva data e hora da ocorrência e 
instalação afetada)
1. Descrição da ocorrência
Texto livre.
2. Anormalidades e dificuldades observadas
 Texto livre.
3. Providências tomadas
Texto livre.
4. Providências em andamento
Texto livre.
Prazo: dia/mês/ano (AGENTE confirmar/adequar prazo)
5.Anexos

Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
 Elaboração do Relatório de Análise de Ocorrência - RO RO-AN.BR.01 04 4.3.1. 27/07/2022
8.1.1.
