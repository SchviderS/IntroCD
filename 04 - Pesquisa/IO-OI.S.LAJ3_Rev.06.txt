 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Lajeado 3 
 
 
Código Revisão Item Vigência 
IO-OI.S.LAJ3 06 3.7.5.2. 06/11/2023 
 
. 
 
MOTIVO DA REVISÃO 
- Alteração da apresentação das condições de energização e fechamento em anel no item 6.2.2, conforme 
IO-PM.S.2RS. 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S CEEE-T CERTEL CYMI 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Lajeado 3 IO-OI.S.LAJ3 06 3.7.5.2. 06/11/2023 
 
Referência:  2 / 9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  4 
3.1. Barramento de 230 kV ................................................................................................................... 4 
3.2. Alteração na Configuração dos Barramentos ................................................................................ 4 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 4 
4.1. Procedimentos Gerais ................................................................................................................... 4 
4.2. Procedimentos Específicos ............................................................................................................ 4 
4.2.1. Operação dos Comutadores de Tapes Sob Carga (LTC) .............................................. 4 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  5 
5.1. Procedimentos Gerais ................................................................................................................... 5 
5.2. Procedimentos Para Recomposição com Autonomia ................................................................... 5 
5.2.1. Preparação da Instalação para a Recomposição com Autonomia .............................. 5 
5.2.2. Recomposição com Autonomia após Desligamento Total da Instalação.................... 6 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 6 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 6 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 6 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 6 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 6 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 7 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 7 
6.1. Procedimentos Gerais ................................................................................................................... 7 
6.2. Procedimentos Específicos ............................................................................................................ 8 
6.2.1. Desenergização de Equipamentos .............................................................................. 8 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ....................................... 8 
7. NOTAS IMPORTANTES ................................ ................................ ................................ .................. 9 
 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Lajeado 3 IO-OI.S.LAJ3 06 3.7.5.2. 06/11/2023 
 
Referência:  3 / 9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
1. OBJETIVO 
Estabelecer os proc edimentos para a operação da SE Lajeado 3, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da Instalação , devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR-S e a Instalação, de propriedade da Vineyards, é realizada 
pela CYMI, agente responsável pela operação da instalação, por intermédio do Centro de Operação 
COT CYMI.  
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte da Área 230 kV do Rio Grande 
do Sul. 
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.4.1. A definição do número de tentativas de religamento manual de linha de transmissão ou 
equipamento, bem como o intervalo entre elas , é de responsabilidade do A gente e devem ser 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica. 
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente , e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR-S autorização 
para religamento. Nes sa oportunidade , o Agente pode solicitar também a alteração no sentido 
normal para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em considera ção as condições operativas do 
sistema. 
2.5. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação , quando o Agente  tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Lajeado 3 IO-OI.S.LAJ3 06 3.7.5.2. 06/11/2023 
 
Referência:  4 / 9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 230 KV 
A configuração do barramento de 230 kV é do tipo Barra Dupla (Barra I e Barra II) a Quatro Chaves. Na 
operação normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados, exceto as 
seccionadoras de transferência e uma das seletoras de barra das linhas de transmissão ou equipamentos.  
A Instalação deve operar em condição normal com a seguinte configuração: 
Em uma das barras - I (ou II)  Na outra barra - II (ou I) 
LT 230 kV Lajeado 2 / Lajeado 3 (ou LT 230 kV 
Garibaldi 1 / Lajeado 3)  
LT 230 kV Garibaldi 1 / Lajeado 3  (ou LT 230 kV 
Lajeado 2 / Lajeado 3)  
Transformador TR-1 230/69/13,8 kV (ou TR-2)  Transformador TR-2 230/69/13,8 kV (ou TR-1)  
3.2. ALTERAÇÃO NA CONFIGURAÇÃO DOS BARRAMENTOS 
A mudança de configuração do barramento de 230 kV desta Instalação é executada com controle do  
COSR-S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade dos Agentes 
Operadores da Instalação. 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S. 
As faixas de controle de tensão para esse barramento estão estabelecidas no Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. O barramento de 69 kV, não pertencente à Rede de Operação, onde se conecta a transformação 
230/69/13,8 kV, tem a sua regulação de tensão executada com autonomia pelo Agente Operador da 
Instalação, por meio da utilização de recursos locais disponíveis de autonomia dessa. 
Esgotados esses recursos, o Agente deve acionar o COSR -S, que deve verificar a disponibilidade dos 
recursos sistêmicos. 
As faixas de controle de tensão para o barramento de 69 kV estão estabelecidas no Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.3. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob a responsabilidade da Instalação. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
4.2.1. OPERAÇÃO DOS COMUTADORES DE TAPES SOB CARGA (LTC) 
Os LTCs dos transformadores TR-1 e TR-2 230 / 69 /13,8 kV operam em modo manual.  
A movimentação dos comutadores é realizada com autonomia pela operação do Agente CYMI. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Lajeado 3 IO-OI.S.LAJ3 06 3.7.5.2. 06/11/2023 
 
Referência:  5 / 9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação : caracterizado quando não há tensão em todos os terminais 
de suas linhas de transmissão. 
• Desligamento parcial da Instalação : qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao COSR-S 
as seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2., sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia da operação do Agente Operador da Instalação 
na recomposição, deve ser utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO COM AUTONOMIA 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO COM AUTONOMIA 
No caso de desligamento total, o Agente Operador deve configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• das linhas de transmissão:  
LT 230 kV Garibaldi 1 / Lajeado 3; 
LT 230 kV Lajeado 2 / Lajeado 3; 
• de todas as linhas de transmissão de 69 kV 
• dos transformadores: 
TR-1 230/69/13,8 kV (lados de 230 kV e de 69 kV); 
TR-2 230/69/13,8 kV (lados de 230 kV e de 69 kV); 
Desligar ou manter desligado o modo de comutação automática dos comutadores sob carga da 
transformação 230/69/13.8 kV da SE Lajeado 3. 
Cabe ao Agente CYMI informar ao COSR-S quando a configuração de preparação da Instalação não estiver 
atendida para o início da recomposição, independentemente de o equipamento ser próprio ou de outros 
agentes. Nesse caso, o COSR -S fará contato com os agentes envolvidos para identificar o motivo do não -
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Lajeado 3 IO-OI.S.LAJ3 06 3.7.5.2. 06/11/2023 
 
Referência:  6 / 9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
atendimento e, após confirmação do Agente CYMI de que o barramento está com a configuração atendida, 
o COSR -S coordenará os procedimentos para recomposição, caso necessário, em função da configuração 
desta Instalação. 
5.2.2. RECOMPOSIÇÃO COM AUTONOMIA APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar com autonomia os seguintes procedimentos para a 
recomposição: 
Passo Executor Procedimentos Objetivo / Item de Controle 
1 CYMI 
Receber tensão da SE Garibaldi 1, pela LT 230 
kV Garibaldi 1 / Lajeado 3  e energizar o 
barramento de 230 kV. 
 
1.1 CYMI Energizar a LT 230 kV Lajeado 2 / Lajeado 3, 
enviando tensão para a SE Lajeado 2. 
 
1.2 CYMI 
Energizar, pelo lado 230 kV, o transformador 
TR-1 230/69/13,8 kV (ou TR-2) da SE Lajeado 3 
e ligar, energizando o barramento de 69 kV, 
pelo lado de 69 kV. 
TAPLAJ3-230/69 = 9 
 
O restabelecimento d e carga pela 
SE Lajeado 3 somente pode ser 
realizada após autorização do 
COSR-S. 
1.3 CYMI 
Energizar, pelo lado 230 kV, o segundo 
transformador 230/69/13,8 kV da SE Lajeado 
3 e ligar, interligando esse transformador com 
o outro transformador 230/69/13,8 kV da SE 
Lajeado 3, pelo lado de 69 kV.  
TAPLAJ3-230/69 = 9 
Após fluxo de potência ativa  no 
primeiro transformador 
230/69/13,8 kV da SE Lajeado 3 
que foi energizado. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2, enquanto não houver 
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia deste na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras.  
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, o 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Lajeado 3 IO-OI.S.LAJ3 06 3.7.5.2. 06/11/2023 
 
Referência:  7 / 9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Agente Operador da Instalação deve preparar a Instalação conforme Subitem 5.2.1, sem necessidade 
de autorização do ONS. 
5.4.1.2. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador 
da Instalação deve recompor a Instalação conforme Subitem 5.2.2, sem necessidade de autorização 
do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., o Agente Operador 
da Instalação deve recompor a Instalação conforme Subitem 6.2.2, sem necessidade de autorização 
do ONS.    
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmi ssão ou de equipamentos, após  
desligamento programado, de urgência ou de emergência, só podem ser efetuados com controle do 
COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelo Agente Operador da Instalação quando 
estiverem explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução de 
Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, o Agente Operador da Instalação deve verificar se existe 
tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pela operação da Instalação quando 
estiver especificado nesta Instrução de O peração e estiverem atendidas as condições do Subitem 
6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou , quando permitida, em sentido  inverso, pode ser feita com 
autonomia pela operação do Agente, conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica.  
 A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Lajeado 3 IO-OI.S.LAJ3 06 3.7.5.2. 06/11/2023 
 
Referência:  8 / 9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o operador da Instalação deve fechá -lo em anel desde que tenha autonomia  para tal, 
adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE EQUIPAMENTOS 
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação, é sempre 
controlada pelo COSR-S. 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de equipamentos ou de linhas de transmissão. 
O Agente Operador da Instalação  deve identificar os desligamentos automáticos observando na 
Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado nas 
condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
LT 230 kV Garibaldi 1 / 
Lajeado 3  
Sentido Normal: SE Lajeado 3 recebe tensão da SE Garibaldi 1  
Ligar, em anel  ou energizando o 
barramento de 230 kV , a LT 230  kV 
Garibaldi 1 / Lajeado 3. 
 
Sentido Inverso: SE Lajeado 3 envia tensão para a SE Garibaldi 1  
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2RS. 
LT 230 kV Lajeado 2 / 
Lajeado 3 
Sentido Normal: SE Lajeado 3 envia tensão para a SE Lajeado 2  
Energizar a LT 230 kV Lajeado 2 / 
Lajeado 3. 
Sistema completo ou N-1 de TR na SE 
Lajeado 3 230 kV 
• VLAJ3-230 ≤ 242 kV. 
Sentido Inverso: SE Lajeado 3 recebe tensão da SE Lajeado 2  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Lajeado 3 IO-OI.S.LAJ3 06 3.7.5.2. 06/11/2023 
 
Referência:  9 / 9 
 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
Ligar, em anel, a LT 230 kV Lajeado 2 / 
Lajeado 3. 
 
Transformador TR-1 ou TR-2  
230/69/13,8 kV 
Sentido Único: A partir do lado de 230 kV 
Energizar, pelo lado de 230 kV, o 
transformador TR-1 230/69/13,8 kV (ou 
TR-2). 
Como primeiro ou segundo 
transformador: 
• VLAJ3-230 ≤ 242 kV; e 
• fluxo de potência ativa na LT 
230 kV Garibaldi 1 / Lajeado 
3 e na LT 230 kV Lajeado 2 / 
Lajeado 3. 
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa nos transformadores  
230/69/13,8 kV que já foram 
energizados. 
Ligar, energizando o barramento de 69 
kV ou interligando esse transformador 
com outro já em operação, ou em anel, 
pelo lado de 69 kV, o transformador TR-
1 230/69/13,8 kV (ou TR-2). 
Barramento de 69 kV da SE Lajeado 3 
desenergizado ou energizado por 
outro transformador 230/69/13,8 kV 
da SE Lajeado 3. 
Sentido Normal: A partir do lado de 69 kV 
A energização pelo terminal de 69 kV não é permitida. 
Em caso de abertura apenas do lado de 230 kV, proceder conforme segue: 
Ligar, interligando esse transformador 
com outro já em operação, ou em anel, 
pelo lado de 230 kV, o transformador 
TR-1 230/69/13,8 kV (ou TR-2).  
Outro transformador 230/69/13,8 
kV da SE Lajeado 3 com fluxo de 
potência ativa. 
7. NOTAS IMPORTANTES 
7.1. As tratativas de operação entre o ONS e a Cooperativa de Distribuição de Energia Teutônia – CERTEL 
Energia são feitas pela CYMI, interlocutora nomeado pela CERTEL para estas tratativas. 
7.2. No caso de desligamento total da SE Lajeado 3, o restabelecimento de carga da CERTEL somente pode 
ser realizado após autorização do COSR-S. 
 
