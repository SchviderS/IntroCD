Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.11
Cadastro de Informações Operacionais
Cadastro de Informações Operacionais de Faixas para Controle de Tensão da Área 230 kV 
do Mato Grosso do Sul
Código Revisão Item Vigência
CD-CT.CO.2MS.03 31 2.6.6 29/04/2023
MOTIVO DA REVISÃO
Exclusão das restrições em faixas de tensão, passando todos os barramentos a operarem na faixa de 95% a 
105% das respectivas tensões nominais, com exclusão da grandeza de referência associada à carga (antigo 
Subitem 2.1 no Item "Conceitos").
LISTA DE DISTRIBUIÇÃO 
BTE BTE II CEL ENGENHARIA CELEO CNOS
COSR-S ELETROSUL EMS ITATIM LTC
Manual de Procedimentos da Operação - Módulo 10 - Submódulo 10.18
 Referência técnica Código Revisão Item Vigência
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 230 kV do Mato Grosso do 
Sul
CD-
CT.CO.2MS.03 31 2.6.6 29/04/2023
Referência: 2 / 5
NEOENERGIA PPTE RIO PARANA ENERGIA TER
Manual de Procedimentos da Operação - Módulo 10 - Submódulo 10.18
 Referência técnica Código Revisão Item Vigência
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 230 kV do Mato Grosso do 
Sul
CD-
CT.CO.2MS.03 31 2.6.6 29/04/2023
Referência: 3 / 5
ÍNDICE
1. OBJETIVO.........................................................................................................................................4
2. CONCEITOS ......................................................................................................................................4
3. CONSIDERAÇÕES GERAIS .................................................................................................................4
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO .........................................................4
4.1. Faixas de tensão para Barramentos da Rede de Operação...........................................................4
4.2. Demais Faixas de tensão para a Rede de Operação......................................................................5
Manual de Procedimentos da Operação - Módulo 10 - Submódulo 10.18
 Referência técnica Código Revisão Item Vigência
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 230 kV do Mato Grosso do 
Sul
CD-
CT.CO.2MS.03 31 2.6.6 29/04/2023
Referência: 4 / 5
1. OBJETIVO
Apresentar as faixas de controle de tensão nos barramentos da Rede de Operação e dos secundários das 
transformações da Rede de Operação, cujos barramentos não pertencem à Rede de operação, a serem 
controlados pelos operadores dos Centros de Operação do ONS e pela Operação dos Agentes envolvidos.
2. CONCEITOS
Não se aplica.
3. CONSIDERAÇÕES GERAIS
3.1. Este Cadastro de Informações Operacionais apresenta os valores das faixas para controle de tensão de 
barramentos da Rede de Operação e de secundários das transformações da Rede de Operação cujas 
barras não pertencem à Rede de Operação.
3.2. Os Centros de Operação do ONS, nas ações de coordenação, supervisão e controle, devem observar os 
limites operacionais constantes neste Cadastro de Informações Operacionais.
3.3. A operação dos agentes, em suas ações de comando e execução, deve observar os limites operacionais 
constantes neste Cadastro de Informações Operacionais.
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO
4.1. FAIXAS DE TENSÃO PARA BARRAMENTOS DA REDE DE OPERAÇÃO
Para a Área 230 kV do Mato Grosso do Sul, todos os barramentos da Rede de Operação e secundários das 
transformações da Rede de Operação operam com faixas de tensão conforme apresentado no Subitem 4.2.
Manual de Procedimentos da Operação - Módulo 10 - Submódulo 10.18
 Referência técnica Código Revisão Item Vigência
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 230 kV do Mato Grosso do 
Sul
CD-
CT.CO.2MS.03 31 2.6.6 29/04/2023
Referência: 5 / 5
4.2. DEMAIS FAIXAS DE TENSÃO PARA A REDE DE OPERAÇÃO
Os barramentos da Rede de Operação e os enrolamentos das transformações da Rede de Operação, que não 
possuem faixas de tensão definidas nos itens anteriores, devem operar com tensões nas seguintes faixas 
operativas, conforme estabelecido nos Procedimentos de Rede e reproduzido na tabela a seguir:
Tensão Nominal (kV) Faixa de Tensão (kV)
765 690 a 800
525 500 a 550 
500 500 a 550 
440 418 a 460 
345 328 a 362 
230 218 a 242 
138 131 a 145 
88 83,6 a 92,4
69 65,6 a 72,4
34,5 32,8 a 36,2
23 21,8 a 24,2
13,8 13,1 a 14,5
