 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.11 
 
Cadastro de Informações Operacionais. 
Cadastro de Limites Operacionais de Linhas de Transmissão e Transformadores da 
Interligação em Corrente Contínua de Xingu 
 
Código Revisão Item Vigência 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
 
MOTIVO DA REVISÃO 
Ajustes na formatação do documento sem alteração do seu conteúdo. 
 
 
LISTA DE DISTRIBUIÇÃO 
BMTE CNOS COSR-NCO COSR-SE FURNAS 
FURNAS.CTRM.O LTT LXTE PCTE UHE BELO MONTE 
XRTE     
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  2 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
ÍNDICE 
   
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONCEITOS ................................ ................................ ................................ ................................ ... 3 
3. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 5 
4. LIMITES DE CARREGAMENTO DE LINHAS DE TRANSMISSÃO ................................ ...........................  7 
5. LIMITES DE CARREGAMENTO DE TRANSFORMADORES ................................ ................................ .. 7 
6. LIMITES DE CARREGAMENTO DE SISTEMAS HVDC................................ ................................ .......... 7 
6.1. Bipolo Xingu / Estreito ................................................................................................................... 7 
6.1.1. SE Xingu – Conversor 1 e 2 em Tensão Nominal (800 kV) ........................................... 7 
6.1.2. SE Xingu – Conversor 1 e 2 em Tensão Reduzida (680 kV).......................................... 7 
6.1.3. SE Xingu – Conversor 1 e 2 em Tensão Reduzida (560 kV).......................................... 8 
6.1.4. SE Estreito – Conversor 1 e 2 em Tensão Nominal (800 kV) ....................................... 9 
6.1.5. SE Estreito – Conversor 1 e 2 em Tensão Reduzida (680 kV) ...................................... 9 
6.1.6. SE Estreito – Conversor 1 e 2 em Tensão Reduzida (560 kV) .................................... 10 
6.1.7. LT 800 kV Xingu / Estreito C1 e C2 ............................................................................. 10 
6.1.8. Linha de Eletrodo da SE Xingu C1 e C2 ...................................................................... 11 
6.1.9. Linha de Eletrodo da SE Estreito C1 e C2................................................................... 11 
6.1.10. Eletrodo da SE Xingu .................................................................................................. 11 
6.1.11. Eletrodo da SE Estreito .............................................................................................. 12 
6.2. Bipolo Xingu / Terminal Rio ......................................................................................................... 12 
6.2.1. SE Xingu – Conversor 1 e 2 em Tensão Nominal (800 kV) ......................................... 12 
6.2.2. SE Xingu – Conversor 1 e 2 em Tensão Reduzida (560 kV)........................................ 13 
6.2.3. SE Terminal Rio – Conversor 1 e 2 em Tensão Nominal (800 kV) ............................. 14 
6.2.4. SE Terminal Rio – Conversor 1 e 2 em Tensão Reduzida (560 kV) ............................ 14 
6.2.5. LT 800 kV Xingu/ Terminal Rio C1 e C2 ...................................................................... 15 
6.2.6. Linha de Eletrodo da SE Xingu C1 e C2 ...................................................................... 15 
6.2.7. Linha de Eletrodo da SE Terminal Rio C1 e C2 ........................................................... 16 
6.2.8. Eletrodo da SE Xingu .................................................................................................. 16 
6.2.9. Eletrodo da SE Terminal Rio ...................................................................................... 17 
 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  3 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
1. OBJETIVO 
Apresentar os limites operacionais de linhas de transmissão e de transfo rmadores da Rede de Operação a 
serem controlados pelos operadores dos Centros de Operação do ONS e pela Operação dos Agentes 
envolvidos. 
2. CONCEITOS 
2.1. Limite Operacional: Corresponde ao limite de carregamento do equipamento que deve ser observado 
pelo ONS e pelos Agentes, em condição normal de operação e ou em condição de emergência. 
2.2. Alteração dos Limites Operacionais: É a alteração de limite na qual são estabelecidos novos valores, 
decorrentes de eventos que alteraram a capacidade operativa do equipamento, como por exemplo 
recapacitação, substituições de TC, modificação de característica de proteção, indisponibili dade do 
sistema de refrigeração, flexibilização por parte do Agente, etc. 
2.3. Fator Limitante:  Fator que impede um equipamento de atingir um limite operacional superior ao 
estabelecido de projeto. Um fator limitante ativo provoca aumento no custo de operação ou de 
expansão do SIN. 
2.4. Limites de linhas de transmissão 
2.4.1. Condição Normal de Operação 
Situação de carregamento em que a linha de transmissão conduz continuamente corrente em valor igual ou 
inferior ao seu valor operacional, em sua condição de carregamento nominal, para a qual foi projetada. 
2.4.2. Condição de Emergência de Operação 
Situação de carregamento em que a linha de transmiss ão conduz corrente acima do seu valor operacional, 
em sua condição de carregamento superior ao nominal para a qual foi projetada, por um tempo não superior 
a 4 dias (96 horas) contínuos, ou um somatório máximo de 18 dias (432 horas) intermitentes (se somados 
em base anual todos os períodos contínuos inferiores a 4 dias ), conforme disposto no anexo 3 da seção 4.1 
das Regras dos Serviços de Transmissão. 
As linhas de transmissão que se enquadrarem nessa situação terão o campo duração nas tabelas 
apresentadas neste cadastro preenchido com (*). 
2.4.3. Condição Sazonal de Operação 
Situação de carregamento em que a linha de transmissão transporta continuamente corrente em valor igual 
ou inferior ao seu valor operacional, em condição normal de operação e ou em condição de emergência, em 
condições de operação temporárias e específicas para diferentes períodos do ano, tais como verão -dia, 
verão-noite, inverno-dia e inverno -noite ou outro período específico do ano (meses ou estações do ano) , 
conforme disposto no anexo 3 da seção 4.1 das Regras dos Serviços de Transmissão. 
2.4.4. Limite Diurno  
Limite operacional a ser considerado para o período diurno. O período diurno é o intervalo entre o horário 
do amanhecer e do crepúsculo, conforme disposto nos Procedimentos de Rede. 
Quando o período não for definido pelo agente , deve ser considerado o período entre 06h00min até 
17h59min, sempre tomando como referência o horário oficial de Brasília. 
2.4.5. Limite Noturno  
Limite operacional a ser considerado para o período noturno. O período noturno é o intervalo de tempo 
entre o horário do crepúsculo e do amanhecer, conforme disposto nos Procedimentos de Rede. 
Quando o período não for definido pelo agente, deve ser considerado o período de 18h00min até 05h59min, 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  4 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
sempre tomando como referência o horário oficial de Brasília. 
2.5. Limites de Transformadores 
2.5.1. Condição Normal de Operação  
Situação de carregamento em que o transformador conduz continuamente até a  sua corrente nominal. 
Mesmo que em algum momento seja ultrapassada a sua corrente nominal, não deve ser excedida a 
temperatura máxima do óleo ou do enrolamento, conforme limites estabelecidos na NBR 5356-7, respeitadas 
as condições, valores e prazos admitidos pelo agente. 
2.5.2. Condição de Emergência de Longa Duração  
Situação de carregamento em que o transformador conduz continuamente uma corrente superior a sua 
corrente nominal, com duração não superior a 4 (quatro) horas. Mesmo que em algum momento seja 
ultrapassada a sua corrente de emergência de longa duração, não deve ser excedida a temperatura máxima 
do óleo e ou do enrolamento, conf orme limites estabelecidos na NBR 5356-7, respeitadas as condições, 
valores e prazos admitidos pelo agente. 
2.5.3. Condição de Emergência de Curta Duração  
Situação de carregamento em que o transformador conduz continuamente uma corrente superior a sua 
corrente de emergência de longa duração, com duração não superior a 30 (trinta) minutos. Após esse 
intervalo de tempo, deve-se retornar à condição de carregamento de longa duração. 
Mesmo que em algum momento seja ultrapassada a sua corrente de emergência de curta duração, não deve 
ser excedida a temperatura máxima do óleo e ou do enrolamento, conforme limites estabelecidos na NBR 
5356-7, respeitadas as condições, valores e prazos admitidos pelo agente. Esta condição deve ser utilizada 
como último recurso antes do corte de carga. 
2.6. Preenchimento das Tabelas de Limites 
2.6.1. Linhas de Transmissão 
O campo “Valor Operacional” deve ser preenchido com o valor limite do equipamento a ser considerado na 
operação em tempo real. 
O campo “Duração (hh:mm)” indica o período admissível para operação da linha de transmissão na condição 
de emergência.  As linhas de transmissão que possuem duração conforme item 2.4.2 terão esse campo 
preenchido com (*). 
Nas tabelas em que for detalhado o tempo de operação em determinada condição de operação de 
equipamento, deve-se utilizar tantas linhas quanto forem necessárias para o detalhamento de carregamento 
por período de tempo informado pelo Agente. 
Os campos “Valor Operacional” e “Duração” das tabelas cujos valores dependem de informação do Agente 
e não estão disponíveis para o ONS, devem ser preenchidas com “NI” (Não Informado pelo Agente). 
O campo “Valor Operacional” das tabelas referentes a valores de emergência que não são permitidos pelo 
Agente, devem ser preenchidos com “NP” (Não Permitido pelo Agente). Para esse caso, também deve ser 
preenchido o campo “Fator Limitante”. 
O campo “Período do Ano” indica, quando for o caso, a sazonalidade anual definida para determinação dos 
valores operacionais em condição normal e de emergência. 
O campo “Período do Dia” indica, quando for o caso, a sazonalidade diária definida para determinação dos 
valores operacionais em condição normal e de emergência. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  5 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
2.6.2. Transformadores 
O campo “Enrolamento (kV)” indica o terminal e a tensão de referência aos quais  está relacionado o limite 
operacional nas condições normal, emergência de longa duração e emergência de curta duração. 
O campo “Valor Operacional (A)” indica o valor de corrente limite do transformador para condição normal, 
emergência de longa duração e de curta duração , considerando como tensão nominal a indicada no campo 
“Enrolamento (kV)”. 
O campo “Duração  (hh:mm)” indica o período admissível para operação do transformador na condição de 
emergência de longa e curta duração. 
Nas tabelas em que for detalhado o tempo de operação em determinada condição de operação de 
equipamento, deve-se utilizar tantas linhas quanto forem necessárias para o detalhamento de carregamento 
por período de tempo informado pelo Agente. 
Os campos “Valor Operacional” e “Duração” das tabelas cujos valores dependem de informação do Agente 
e não estão disponíveis para o ONS, devem ser preenchidas com “NI” (Não Informado pelo Agente). 
O campo “Valor Operacional” das tabelas referentes a valores de emergência que não são permitidos pelo 
Agente, devem ser preenchidos com “NP” (Não Permitido pelo Agente). Para esse caso, também deve ser 
preenchido o campo “Fator Limitante”. 
3. CONSIDERAÇÕES GERAIS 
3.1. Este Cadastro apresenta os limites operacionais de carregamento d e linhas de transmissão e d e 
transformadores, de acordo com as definições estabelecidas entre ANEEL, ONS e Agentes, cujo 
processo de atualização está descrito na RO-CD.BR.01 - Controle de Limites de Carregamento de 
Linhas de Transmissão e Transformadores. 
Devem constar neste Cadastro os limites operacionais de linhas de transmissão e de transformadores 
da Rede de Operação, exceto os limites de transformadores de uso exclusivo da geração de usinas 
despachadas centralizadamente ou de Conjuntos de Usinas, de t ransformadores de função de 
transmissão de controle de reativos e de atendimento a Consumidor Livre, quando classificados como 
Rede de Operação. 
3.2. Os operadores dos Centros de Operação do ONS, nas ações de coordenação, supervisão e controle 
devem observar os limites operacionais constantes neste Cadastro de Informações Operacionais. 
3.3. A operação dos agentes, em suas ações de comando e execução, deve observar os limites 
operacionais constantes neste Cadastro de Informações Operacionais. 
3.4. Cabe ao agente comunicar de imediato ao Centro de Operação com o qual se relaciona qualquer 
alteração de limite que imponha restrições à operação de linha de transmissão ou de transformador. 
3.5. Toda e qualquer alteração nos valores de limites operacionais constantes neste Cadastro de 
Informações Operacionais deverá ser solicitada formalmente e devidamente justificada e 
caracterizada pelo agente responsável pelo equipamento  ao Centro de Operação do ONS de seu 
relacionamento.  
3.6. Caso a operação em tempo real venha a verificar qualquer possibilidade de violação do limite 
estabelecido para o equipamento, caberá ao Centro de Operação do ONS responsável tomar as 
medidas corretivas cabíveis , para que esse não ultrapasse esses valores, que só podem ser 
ultrapassados com anuência do agente. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  6 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
3.7. Os valores de Limites Operacionais dos equipamentos da Rede de Operação, constantes neste 
Cadastro de Informações Operacionais, são os valores informados oficia lmente e formalmente pelo 
agente. 
3.8. Os limites de transformadores que constam neste Cadastro de Informações Operacionais consideram 
todos os estágios de refrigeração disponíveis. Os limites considerando os estágios de refrigeração 
indisponíveis constam nos Cadastros de Informações Operacionais de Dados de Equipamentos. 
3.9. Os limites de transformadores apresentados têm como ref erência a tensão indicada no nome do 
respectivo equipamento. 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  7 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
4. LIMITES DE CARREGAMENTO DE LINHAS DE TRANSMISSÃO 
Não se aplica 
5. LIMITES DE CARREGAMENTO DE TRANSFORMADORES 
Não se aplica 
6. LIMITES DE CARREGAMENTO DE SISTEMAS HVDC 
6.1. BIPOLO XINGU / ESTREITO 
6.1.1. SE XINGU – CONVERSOR 1 E 2 EM TENSÃO NOMINAL (800 KV) 
Limites de Condição Normal de Operação 
Modo operação Valor Operacional (MW) Fator Limitante 
Monopolar 2.000 
--- 
Bipolar 4.000 
 
Limites de Condição de Emergência 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 2.660 
00:30:00 
Em 5s Runback para 100% 
Intervalo entre sobrecargas: 24h 
Máximo de 20 ocorrências por ano. Bipolar 5.320 
Monopolar 3.000 
00:00:05 Em 100ms Runback para 133% 
Intervalo entre sobrecargas: 05min Bipolar 6.000 
 
Limites Mínimos 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 200 
--- --- 
Bipolar 400 
6.1.2. SE XINGU – CONVERSOR 1 E 2 EM TENSÃO REDUZIDA (680 KV) 
Limites de Condição Normal de Operação 
Modo operação Valor Operacional (MW) Fator Limitante 
Monopolar 1.700 
--- 
Bipolar 3.400 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  8 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
Limites de Condição de Emergência 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 2.261 
00:30:00 
Em 5s Runback para 100% 
Intervalo entre sobrecargas: 24h 
Máximo de 20 ocorrências por ano. Bipolar 4.522 
Monopolar 2.550 
00:00:05 Em 100ms Runback para 133% 
Intervalo entre sobrecargas: 05min Bipolar 5.100 
 
Limites Mínimos 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 200 
--- --- 
Bipolar 400 
6.1.3. SE XINGU – CONVERSOR 1 E 2 EM TENSÃO REDUZIDA (560 KV) 
Limites de Condição Normal de Operação 
Modo operação Valor Operacional (MW) Fator Limitante 
Monopolar 1.400 
--- 
Bipolar 2.800 
 
Limites de Condição de Emergência 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 1.862 
00:30:00 
Em 5s Runback para 100% 
Intervalo entre sobrecargas: 24h 
Máximo de 20 ocorrências por ano. Bipolar 3.724 
Monopolar 2.100 
00:00:05 Em 100ms Runback para 133% 
Intervalo entre sobrecargas: 05min Bipolar 4.200 
 
Limites Mínimos 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 200 
--- --- 
Bipolar 400 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  9 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
6.1.4. SE ESTREITO – CONVERSOR 1 E 2 EM TENSÃO NOMINAL (800 KV) 
Limites de Condição Normal de Operação 
Modo operação Valor Operacional (MW) Fator Limitante 
Monopolar 1.925 
--- 
Bipolar 3.850 
 
Limites de Condição de Emergência 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 2.560 
00:30:00 
Em 5s Runback para 100% 
Intervalo entre sobrecargas: 24h 
Máximo de 20 ocorrências por ano. Bipolar 5.120 
Monopolar 2.887 
00:00:05 Em 100ms Runback para 133% 
Intervalo entre sobrecargas: 05min Bipolar 5.775 
 
Limites Mínimos 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 200 
--- --- 
Bipolar 400 
6.1.5. SE ESTREITO – CONVERSOR 1 E 2 EM TENSÃO REDUZIDA (680 KV) 
Limites de Condição Normal de Operação 
Modo operação Valor Operacional (MW) Fator Limitante 
Monopolar 1.636 
--- 
Bipolar 3.272 
 
Limites de Condição de Emergência 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 2.176 
00:30:00 
Em 5s Runback para 100% 
Intervalo entre sobrecargas: 24h 
Máximo de 20 ocorrências por ano. Bipolar 4.352 
Monopolar 2.454 
00:00:05 Em 100ms run back para 133% 
Intervalo entre sobrecargas: 05min Bipolar 4.908 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  10 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
Limites Mínimos 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 200 
--- --- 
Bipolar 400 
6.1.6. SE ESTREITO – CONVERSOR 1 E 2 EM TENSÃO REDUZIDA (560 KV) 
Limites de Condição Normal de Operação 
Modo operação Valor Operacional (MW) Fator Limitante 
Monopolar 1.347 
--- 
Bipolar 2.695 
 
Limites de Condição de Emergência 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 1.792 
00:30:00 
Em 5s Runback para 100% 
Intervalo entre sobrecargas: 24h 
Máximo de 20 ocorrências por ano. Bipolar 3.584 
Monopolar 2.021 
00:00:05 Em 100ms run back para 133% 
Intervalo entre sobrecargas: 05min Bipolar 4.042 
 
Limites Mínimos 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 200 
--- --- 
Bipolar 400 
6.1.7. LT 800 KV XINGU / ESTREITO C1 E C2 
Limites de Condição Normal de Operação ( Longa Duração ) 
Período do ano Período do dia Valor Operacional (A) Fator Limitante 
Jan a Dez Diurno e Noturno 2.625  
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  11 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
Limites de Condição de Emergência ( Curta Duração ) 
Período do dia Valor Operacional (A) Duração 
(Hh:mm:ss) Fator Limitante 
Diurno e Noturno 3.500 ---  
6.1.8. LINHA DE ELETRODO DA SE XINGU C1 E C2 
Limites de Condição Normal de Operação ( Longa Duração ) 
Período do ano Período do dia Valor Operacional (A) Fator Limitante 
Jan a Dez Diurno e Noturno 1.375 Limite de tempo definido pelo eletrodo 
 
Limites de Condição de Emergência ( Curta Duração ) 
Período do dia Valor Operacional (A) Duração 
(Hh:mm:ss) Fator Limitante 
Diurno e Noturno 1.662 00:30:00  
6.1.9. LINHA DE ELETRODO DA SE ESTREITO C1 E C2 
Limites de Condição Normal de Operação ( Longa Duração ) 
Período do ano Período do dia Valor Operacional (A) Fator Limitante 
Jan a Dez Diurno e Noturno 1.375 Limite de tempo definido pelo eletrodo 
 
Limites de Condição de Emergência ( Curta Duração ) 
Período do dia Valor Operacional (A) Duração 
(Hh:mm:ss) Fator Limitante 
Diurno e Noturno 1.662 00:30:00  
6.1.10. ELETRODO DA SE XINGU 
Limites de Condição Normal de Operação ( Longa Duração ) 
Período do ano Período do dia Valor Operacional (A) Fator Limitante 
Jan a Dez Diurno e Noturno 40  
 
Limites de Condição de Emergência ( Curta Duração ) 
Período do dia Valor Operacional (A) Duração 
(Hh:mm:ss) Fator Limitante 
Diurno e Noturno 2.500 220 horas / ano Monopolar retorno pela terra 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  12 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
Limites de Condição de Emergência ( Curta Duração ) 
Período do dia Valor Operacional (A) Duração 
(Hh:mm:ss) Fator Limitante 
3.325 00:30:00 Monopolar retorno pela terra 
 
Limites para compartilhamento de eletrodo 
Período do dia Valor Operacional (A) Duração (Hh:mm) Fator Limitante 
Diurno e Noturno 
80 2 meses / ano Bipolar 
2.540 30 horas / ano Monopolar retorno pela terra 
3.325 05 horas / ano Monopolar retorno pela terra 
6.1.11. ELETRODO DA SE ESTREITO 
Limites de Condição Normal de Operação ( Longa Duração ) 
Período do ano Período do dia Valor Operacional (A) Fator Limitante 
Jan a Dez Diurno e Noturno 40  
 
Limites de Condição de Emergência ( Curta Duração ) 
Período do dia Valor Operacional (A) Duração 
(Hh:mm:ss) Fator Limitante 
Diurno e Noturno 
2.500 250 horas / ano Monopolar retorno pela terra 
3.325 00:30:00 Monopolar retorno pela terra 
6.2. BIPOLO XINGU / TERMINAL RIO 
6.2.1. SE XINGU – CONVERSOR 1 E 2 EM TENSÃO NOMINAL (800 KV) 
Limites de Condição Normal de Operação 
Modo operação Valor Operacional (MW) Fator Limitante 
Monopolar 2.000 
--- 
Bipolar 4.000 
 
Limites de Condição de Emergência 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 2.761 00:30:00 Após 30 min, em 300ms Runback para 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  13 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
Limites de Condição de Emergência 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Bipolar 5.523 
100%. 
Intervalo entre sobrecargas: 09 horas. 
Alarme acima de 20 ocorrências por ano. 
Monopolar 3.182 
00:00:05 
Após 05 seg, em 300ms Runback para 
133%. 
Intervalo entre sobrecargas: 02 horas. 
Alarme acima de 20 ocorrências por ano. 
Bipolar 6.364 
 
Limites Mínimos 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 200 
--- --- 
Bipolar 400 
6.2.2. SE XINGU – CONVERSOR 1 E 2 EM TENSÃO REDUZIDA (560 KV) 
Limites de Condição Normal de Operação 
Modo operação Valor Operacional (MW) Fator Limitante 
Monopolar 1.400 
--- 
Bipolar 2.800 
 
Limites de Condição de Emergência 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 1.933 
00:30:00 
Após 30 min, em 300ms Runback para 
100%. 
Intervalo entre sobrecargas: 09 horas. 
Alarme acima de 20 ocorrências por ano. 
Bipolar 3.866 
Monopolar 2.227 
00:00:05 
Após 05 seg, em 300ms Runback para 
133%. 
Intervalo entre sobrecargas: 02 horas. 
Alarme acima de 20 ocorrências por ano. 
Bipolar 4.455 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  14 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
Limites Mínimos 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 200 
--- --- 
Bipolar 400 
6.2.3. SE TERMINAL RIO – CONVERSOR 1 E 2 EM TENSÃO NOMINAL (800 KV) 
Limites de Condição Normal de Operação 
Modo operação Valor Operacional (MW) Fator Limitante 
Monopolar 1.635 
--- 
Bipolar 3.270 
 
Limites de Condição de Emergência 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 2.238 
00:30:00 
Após 30 min, em 300ms Runback para 
100%. 
Intervalo entre sobrecargas: 09 horas. 
Alarme acima de 20 ocorrências por ano. 
Bipolar 4.478 
Monopolar 2.566 
00:00:05 
Após 05 seg, em 300ms Runback para 
133%. 
Intervalo entre sobrecargas: 02 horas. 
Alarme acima de 20 ocorrências por ano. 
Bipolar 5.132 
 
Limites Mínimos 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 200 
--- --- 
Bipolar 400 
6.2.4. SE TERMINAL RIO – CONVERSOR 1 E 2 EM TENSÃO REDUZIDA (560 KV) 
Limites de Condição Normal de Operação 
Modo operação Valor Operacional (MW) Fator Limitante 
Monopolar 1.145 
--- 
Bipolar 2.289 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  15 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
Limites de Condição de Emergência 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 1.566 
00:30:00 
Após 30 min, em 300ms Runback para 
100%. 
Intervalo entre sobrecargas: 09 horas. 
Alarme acima de 20 ocorrências por ano. 
Bipolar 3.133 
Monopolar 1.796 
00:00:05 
Após 05 seg, em 300ms Runback para 
133%. 
Intervalo entre sobrecargas: 02 horas. 
Alarme acima de 20 ocorrências por ano. 
Bipolar 3.592 
 
Limites Mínimos 
Modo operação Valor Operacional (MW) Duração 
(Hh:mm:ss) Fator Limitante 
Monopolar 200 
--- --- 
Bipolar 400 
6.2.5. LT 800 KV XINGU/ TERMINAL RIO C1 E C2 
Limites de Condição Normal de Operação ( Longa Duração ) 
Período do ano Período do dia Valor Operacional (A) Fator Limitante 
Jan a Dez Diurno e Noturno 2.625  
 
Limites de Condição de Emergência ( Curta Duração ) 
Período do dia Valor Operacional (A) Duração 
(Hh:mm:ss) Fator Limitante 
Diurno e Noturno 3.500 ---  
6.2.6. LINHA DE ELETRODO DA SE XINGU C1 E C2 
Limites de Condição Normal de Operação ( Longa Duração ) 
Período do ano Período do dia Valor Operacional (A) Fator Limitante 
Jan a Dez Diurno e Noturno 1.270 Limite de tempo definido pelo eletrodo 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  16 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
Limites de Condição de Emergência ( Curta Duração ) 
Período do dia Valor Operacional (A) Duração 
(Hh:mm:ss) Fator Limitante 
Diurno e Noturno 1.722 00:30:00  
6.2.7. LINHA DE ELETRODO DA SE TERMINAL RIO C1 E C2 
Limites de Condição Normal de Operação ( Longa Duração ) 
Período do ano Período do dia Valor Operacional (A) Fator Limitante 
Jan a Dez Diurno e Noturno 1.250 Limite de tempo definido pelo eletrodo 
 
Limites de Condição de Emergência ( Curta Duração ) 
Período do dia Valor Operacional (A) Duração 
(Hh:mm:ss) Fator Limitante 
Diurno e Noturno 1.682 00:30:00  
6.2.8. ELETRODO DA SE XINGU  
Limites de Condição Normal de Operação ( Longa Duração ) 
Período do ano Período do dia Valor Operacional (A) Fator Limitante 
Jan a Dez Diurno e Noturno 40  
 
Limites de Condição de Emergência ( Curta Duração ) 
Período do dia Valor Operacional (A) Duração 
(Hh:mm:ss) Fator Limitante 
Diurno e Noturno 
2.500 220 horas / ano  
3.325 00:30:00  
 
Limites para compartilhamento de eletrodo 
Período do dia Valor Operacional (A) Duração (Hh:mm) Fator Limitante 
Diurno e Noturno 
80 2 meses / ano Bipolar 
2.540 30 horas / ano Monopolar retorno pela terra 
3.325 05 horas / ano Monopolar retorno pela terra 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Limites Operacionais de Linhas de 
Transmissão e Transformadores da Interligação em 
Corrente Contínua de Xingu 
CD-CT.8XG.02 6 2.2.1. 02/08/2024 
 
Referência:  17 / 17 
 
Alterado pela(s) MOP(s): 
MOP/ONS 463-S/2024;  
6.2.9. ELETRODO DA SE TERMINAL RIO 
Limites de Condição Normal de Operação ( Longa Duração ) 
Período do ano Período do dia Valor Operacional (A) Fator Limitante 
Jan a Dez Diurno e Noturno 40  
 
Limites de Condição de Emergência ( Curta Duração ) 
Período do dia Valor Operacional (A) Duração 
(Hh:mm:ss) Fator Limitante 
Diurno e Noturno 
2.500 250 horas / ano  
3.325 00:30:00  
 
