 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.11 
 
Cadastro de Informações Operacionais. 
Cadastro de Informações Operacionais de Faixas para Controle de Tensão da Área 345 kV 
Rio Grande   
 
Código Revisão Item Vigência 
CD-CT.SE.3RG.03 7 2.6.2 23/05/2024 
 
 
MOTIVO DA REVISÃO 
Inserido o barramento de 138 kV da UHE Mascarenhas de Moraes 
 
 
LISTA DE DISTRIBUIÇÃO 
CEMIG CNOS COSR-SE CPFL CTEEP 
FURNAS FURNAS.CTRM.O LTT   
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 345 kV Rio Grande   
CD-
CT.SE.3RG.03 7 2.6.2 23/05/2024 
 
RT-ONS DPL 0105/2024, volume 13, rev. 2 
 
2 / 4 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONCEITOS ................................ ................................ ................................ ................................ ... 3 
3. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO ................................ .......................  4 
4.1. Faixas de tensão para Barramentos da Rede de Operação .......................................................... 4 
4.2. Demais Faixas de tensão para a Rede de Operação ...................................................................... 4 
 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 345 kV Rio Grande   
CD-
CT.SE.3RG.03 7 2.6.2 23/05/2024 
 
RT-ONS DPL 0105/2024, volume 13, rev. 2 
 
3 / 4 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
1. OBJETIVO 
Apresentar as faixas de controle de tensão dos barramentos da Rede de Operação e dos secundários das 
transformações da Rede de Operação, cujos barramentos não pertencem à Rede de Operação, a serem 
controlados pelos operadores dos Centros de Operação do ONS e pela Operação dos agentes envolvidos. 
2. CONCEITOS 
2.1. As faixas de controle de tensão são estabelecidas em função de uma grandeza de referência, definida 
conforme a seguir: 
Sigla  
Nome da Grandeza  
Definição / Equipamento Sentido Positivo Ponto de 
Medição 
Carga Minas Gerais sMMGD Carga de Minas Gerais sem a MMGD, conforme CD-CT.SE.5MG.03 
 
3. CONSIDERAÇÕES GERAIS 
3.1. Este Cadastro de Informações Operacionais apresenta os valores das faixas para controle de tensão de 
barramentos da Rede de Operação e  de secundários das transformações da Rede de Operação cujas 
barras não pertencem à Rede de Operação. 
3.2. Os Centros de Operação do ONS, nas ações de coordenação, supervisão e controle , devem observar 
os limites operacionais constantes neste Cadastro de Informações Operacionais. 
3.3. A operação dos agentes, em suas ações de comando e execução, deve observar os limites operacionais 
constantes neste Cadastro de Informações Operacionais. 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 345 kV Rio Grande   
CD-
CT.SE.3RG.03 7 2.6.2 23/05/2024 
 
RT-ONS DPL 0105/2024, volume 13, rev. 2 
 
4 / 4 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO 
4.1. FAIXAS DE TENSÃO PARA BARRAMENTOS DA REDE DE OPERAÇÃO 
Subestação / Usina Faixas de Tensão  (kV) em função do(a) Carga Minas 
Gerais sMMGD 
Nome Tensão 
(kV) 
> 7500 MW 
(Pesada) 
> 7500 MW 
(Média) 
<= 7500 MW 
(Leve) 
<= 7500 MW 
(Mínima) 
UHE Mascarenhas De Moraes 138 142 - 145 142 - 145 142 - 145 142 - 145 
 
 
 
4.2. DEMAIS FAIXAS DE TENSÃO PARA A REDE DE OPERAÇÃO 
Os barramentos da Rede de Operação e os secundários das transformações da Rede de Operação, que não 
possuem faixas de tensão definidas nos itens anteriores, devem operar com tensões nas seguintes faixas 
operativas, conforme estabelecido nos Procedimentos de Rede e reproduzido na tabela a seguir: 
Tensão Nominal (kV) Faixa de Tensão (kV) 
765 690 a 800 
525 500 a 550  
500 500 a 550  
440 418 a 460  
345 328 a 362  
230 218 a 242  
138 131 a 145  
88 83,6 a 92,4 
69 65,6 a 72,4 
34,5 32,8 a 36,2 
23 21,8 a 24,2 
13,8 13,1 a 14,5 
 
