Manual de Procedimentos da Operação
Módulo 10 - Submódulo 10.18
Cadastro de Informações Operacionais
Cadastro de Informações Operacionais de Faixas para Controle de Tensão da Interligação 
em Corrente Contínua do Madeira
Código Revisão Item Vigência
CD-CT.6MD.03 7 2.6.1 02/07/2020
MOTIVO DA REVISÃO
Atualizada a tabela do item 4.2.
LISTA DE DISTRIBUIÇÃO 
ARARAQUARA CNOS COSR-NCO COSR-SE ELETRONORTE
ELETROSUL ESBR-EnergiaSustentável(UHE JIRAU) IEM SAESA
Manual de Procedimentos da Operação - Módulo 10 - Submódulo 10.18
 Referência técnica Código Revisão Item Vigência
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Interligação em Corrente 
Contínua do Madeira
CD-CT.6MD.03 7 2.6.1 02/07/2020
Referência: RT-CD.BR.03 rev. 01. 2 / 5
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. CONCEITOS ......................................................................................................................................3
3. CONSIDERAÇÕES GERAIS .................................................................................................................3
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO .........................................................4
4.1. Faixas de tensão da rede de operação ..........................................................................................4
4.2. Demais Faixas de tensão da rede de operação .............................................................................4
Manual de Procedimentos da Operação - Módulo 10 - Submódulo 10.18
 Referência técnica Código Revisão Item Vigência
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Interligação em Corrente 
Contínua do Madeira
CD-CT.6MD.03 7 2.6.1 02/07/2020
Referência: RT-CD.BR.03 rev. 01. 3 / 5
1. OBJETIVO
Apresentar as faixas de controle de tensão nos barramentos e transformadores da Rede de Operação a 
serem controlados pelos operadores dos Centros de Operação do ONS e pela Operação dos agentes 
envolvidos.
2. CONCEITOS
2.1. Os períodos de  carga mínima, leve, média e pesada definidos  para o controle de tensão estão 
estabelecidos em função das Faixas Horárias, conforme tabela abaixo, que tem como referência o 
horário de Brasília.
Período Segunda Terça a Sábado Domingos e Feriados
00:00 às 05:00 Mínima Leve Leve
05:00 às 07:00 Mínima Leve Mínima
07:00 às 10:00 Média Média Mínima
10:00 às 17:00 Média Média Leve
17:00 às 22:00 Pesada Pesada Média
22:00 às 24:00 Média Média Leve
2.2. Quando da vigência do horário de verão, é emitida uma Mensagem Operativa – MOP, pelo ONS, para 
estabelecer novos horários de períodos de cargas, tendo como referência o horário de Brasília.   
3. CONSIDERAÇÕES GERAIS
3.1. Este Cadastro de Informações Operacionais apresenta os valores das faixas para controle de tensão de 
barramentos da Rede de Operação e de secundários das transformações da Rede de Operação cujas 
barras não pertencem à Rede de Operação.
3.2. Os Centros de Operação do ONS, nas ações de coordenação, supervisão e controle devem observar os 
limites operacionais constantes deste Cadastro de Informações Operacionais.
3.3. A operação dos agentes, em suas ações de comando e execução, deve observar os limites operacionais 
constantes deste Cadastro de Informações Operacionais.
Manual de Procedimentos da Operação - Módulo 10 - Submódulo 10.18
 Referência técnica Código Revisão Item Vigência
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Interligação em Corrente 
Contínua do Madeira
CD-CT.6MD.03 7 2.6.1 02/07/2020
Referência: RT-CD.BR.03 rev. 01. 4 / 5
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO
4.1. FAIXAS DE TENSÃO DA REDE DE OPERAÇÃO
Subestação / Usina Faixas de Tensão  (kV)
Nome Tensão
(kV) Pesada Média Leve Mínima
Coletora Porto Velho (1) 500 500 - 550 500 - 550 500 - 550 500 - 550
UHE Jirau Margem Direita (1) 500 500 - 550 500 - 550 500 - 550 500 - 550
UHE Jirau Margem Esquerda (1) 500 500 - 550 500 - 550 500 - 550 500 - 550
Notas:
(1) - Estando o TF13 em operação, o limite superior da faixa de tensão é de 535 kV.
4.2. DEMAIS FAIXAS DE TENSÃO DA REDE DE OPERAÇÃO
Os barramentos da Rede de Operação e os enrolamentos das transformações da Rede de Operação que 
não possuem faixas de tensão definidas nos itens anteriores devem operar com tensões nas seguintes 
faixas operativas, conforme estabelecido no submódulo 23.3 dos Procedimentos de Rede e reproduzido 
pela tabela abaixo:
Tensão Nominal Faixas de Tensão (kV)
765 kV 690 a 800
525 kV 500 a 550 
500 kV 500 a 550 
440 kV 418 a 460 
345 kV 328 a 362 
230 kV 218 a 242 
138 kV 131 a 145 
69 kV 65,5 a 72,4
34,5 kV 32,7 a 36,2
23 kV 21,8 a 24,1
13,8 kV 13,1 a 14,5
Manual de Procedimentos da Operação - Módulo 10 - Submódulo 10.18
 Referência técnica Código Revisão Item Vigência
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Interligação em Corrente 
Contínua do Madeira
CD-CT.6MD.03 7 2.6.1 02/07/2020
Referência: RT-CD.BR.03 rev. 01. 5 / 5

