 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da UTE Jorge Lacerda A 
 
 
Código Revisão Item Vigência 
IO-OI.S.UTLA 16 3.7.5.3. 02/08/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Adequação do Subitem 5.4.  
- Adequação à RT-OI.BR revisão 37, com destaque à inclusão dos Subitens 6.1.5. e 6.1.6.  
 
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S CGT Eletrosul 
(COT Norte) 
Diamante 
Geração 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UTE 
Jorge Lacerda A IO-OI.S.UTLA 16 3.7.5.3. 02/08/2024 
 
Referência:  2 / 10 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  3 
3.1. Unidades Geradoras ...................................................................................................................... 3 
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL ................................ ....................  3 
4.1. Procedimentos Gerais ................................................................................................................... 3 
4.2. Procedimentos Específicos ............................................................................................................ 4 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  4 
5.1. Procedimentos Gerais ................................................................................................................... 4 
5.2. Procedimentos para Recomposição com Autonomia ................................................................... 5 
5.2.1. Preparação da Instalação para a Recomposição com Autonomia .............................. 5 
5.2.2. Recomposição com Autonomia após Desligamento Total da Instalação.................... 5 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 8 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 8 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 8 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 9 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 9 
5.4.2. Recomposição Após Desligamento Parcial da Instalação ........................................... 9 
6. MANOBRAS DE UNIDADES GERADORAS, LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ............. 9 
6.1. Procedimentos Gerais ................................................................................................................... 9 
6.2. Procedimentos Específicos .......................................................................................................... 10 
6.2.1. Desligamento de Unidades Geradoras e Desenergização de Equipamentos ........... 10 
6.2.2. Sincronismo de Unidades Geradoras e Energização de Equipamentos .................... 10 
7. NOTAS IMPORTANTES ................................ ................................ ................................ ................ 10 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UTE 
Jorge Lacerda A IO-OI.S.UTLA 16 3.7.5.3. 02/08/2024 
 
Referência:  3 / 10 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da UTE Jorge Lacerda A, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido no s Procedimentos de 
Rede.  
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da Instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR -S e a Instalação, de propriedade da Diamante Geração de 
Energia, é realizada pela Diamante Geração de Energia , agente responsável pela operação da 
Instalação, por intermédio da UTE Jorge Lacerda A. 
2.3. As unidades geradoras e equipamentos desta Instalação fazem parte da Área 230 kV de Santa 
Catarina. 
2.4. O COSR-S controla e supervisiona o despacho de geração desta Usina. 
2.5. Esta Usina: 
• é despachada centralizadamente; 
• está conectada na Rede de Operação; 
• não participa do Controle Automático da Geração – CAG. 
2.6. Os dados operacionais desta Usina  estão descritos no Cadastro de Informações Operacionais da 
respectiva Área Elétrica. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. UNIDADES GERADORAS 
As unidades geradoras GT 1 e GT 2 13,8 kV estão conectadas ao barramento de 138 kV pelos transformadores 
TF1 e TF2 13,8/138 kV e as unidades geradoras GT 3 e GT 4 estão conectadas ao barramento de 230 kV pelos 
transformadores TF4 e TF6 13,8/230 kV,  da SE Jorge Lacerda A , com todas as seccionadoras e disjuntores 
fechados, exceto a seccionadora de transferência e uma das seletoras de barras de 230 kV. 
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. Os barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão executada 
com responsabilidade pelo Agente Operador da Instalação. 
Esgotados os recursos locais disponíveis, sendo necessário, o Agente deve acionar o COSR -S, que 
verificará a disponibilidade dos recursos sistêmicos. 
4.1.2. A Usina deve manter os valores de geração de acordo com os valores programados, constantes no 
Programa Diário de Operação – PDO. Para atendimento dos valores programados constantes no PDO, 
não é necessária a autorização prévia do COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UTE 
Jorge Lacerda A IO-OI.S.UTLA 16 3.7.5.3. 02/08/2024 
 
Referência:  4 / 10 
 
Qualquer alteração no valor de geração da Usina em relação ao valor de geração constante no PDO 
somente pode ser executada após autorização do COSR-S. 
As reprogramações de geração quando de necessidades sistêmicas serão executadas pela Usina 
quando solicitadas pelo COSR-S. 
Após reprogramação de geração solicitada pelo ONS, o Agente somente poderá alterar a geração da 
Usina com autorização do ONS, inclusive para adoção de valores contidos no PDO. 
4.1.3. Os desvios de geração da Usina em relação aos valores previstos no PDO ou em relação às 
reprogramações, devem ser controlados observando-se os valores máximos permitidos explicitados 
na Instrução de Operação IO-CG.BR.01 – Controle da Geração em Condição Normal. 
4.1.4. Quando não existir ou não estiver disponível a supervisão da Usina para o ONS, essa deve seguir as 
orientações para envio de dados conforme Rotina Operacional RO-AO.BR.08 – Apuração de Dados de 
Geração e de Intercâmbios. 
4.1.5. A Usina deve registrar e informar imediatamente os seguintes dados ao COSR-S: 
• movimentação de unidades geradoras (mudança de estado operativo / disponibilidade); 
• restrições e ocorrências na usina ou na conexão elétrica que afetem a disponibilidade de 
geração, com o respectivo valor da restrição, contendo o horário de início e término e a 
descrição do evento; 
• demais informações sobre a operação de suas instalações, solicitadas pelo ONS. 
4.1.6. O controle de tensão, por meio da geração ou absorção de potência reativa pelas unidades geradoras 
da Usina, é controlado pelo COSR-S, com comando e execução pela operação do Agente. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
Não se aplica. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de um desligamento da instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir, para atendimento às necessidades sistêmicas: 
• Desligamento total da Instalação: caracterizado por meio da verificação de ausência de tensão 
em todos os terminais de suas conexões ou ausência de fluxo de potência ativa nessas conexões. 
•  Desligamento parcial da Instalação:  qualquer outra configuração que não se enquadre como 
desligamento total. 
5.1.2. Quando de um desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao 
COSR-S as informações a seguir: 
• horário da ocorrência; 
• configuração da subestação logo após a ocorrência; 
• configuração da instalação após ações realizadas com autonomia pela operação dessa. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UTE 
Jorge Lacerda A IO-OI.S.UTLA 16 3.7.5.3. 02/08/2024 
 
Referência:  5 / 10 
 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição no Subitem 5.2, sem necessidade de autorização prévia por porte do ONS. Caso o ONS 
intervenha no processo de recomposição, identificando a não aplicabilidade da recomposição fluente 
ou interrompendo a autonomia do Agente Operador da Instalação na recomposição, deve ser 
utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4. 
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO COM AUTONOMIA 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO COM AUTONOMIA 
No caso de desligamento total, o  Agente Operador  deve configurar os disjuntores dos seguintes 
equipamentos e unidades geradoras, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• das unidades geradoras: 
GT 1 13,8 kV (disjuntor de 138 kV na SE Jorge Lacerda A); 
GT 2 13,8 kV (disjuntor de 138 kV na SE Jorge Lacerda A); 
GT 3 13,8 kV (disjuntor de 230 kV na SE Jorge Lacerda A); 
GT 4 13,8 kV (disjuntor de 230 kV na SE Jorge Lacerda A). 
• do transformador: 
TF 5 (lado de 6,6 kV). 
Cabe ao Agente Diamante Geração informar ao COSR-S quando a configuração de preparação da Instalação 
não estiver atendida para o início da recomposição, independentemente de o equipamento ser próprio ou 
de outros agentes. Nesse caso, o COSR -S fará contato com os agentes envolvidos para i dentificar o motivo 
do não-atendimento e, após confirmação do Agente Diamante Geração de que os barramentos estão com a 
configuração atendida, o COSR -S coordenará os procedimentos para recomposição, caso necessário, em 
função da configuração desta Instalação. 
5.2.2. RECOMPOSIÇÃO COM AUTONOMIA APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.2.2.1. POR MEIO DA UNIDADE GERADORA GT 3 OU GT 4 13,8 KV 
O Agente Operador da Instalação deve realizar com autonomia os seguintes procedimentos para a 
recomposição: 
Passo Executor Procedimentos Condições ou Limites 
Associados 
1 
Diamante 
Geração 
Receber tensão da SE Jorge Lacerda A, pelo 
transformador TF 5 230/139,6/6,6 kV e fechar o 
disjuntor do lado de 6,6 kV, energizando os serviços 
auxiliares. 
 
1.1 
Diamante 
Geração 
Partir a unidade geradora GT 3 13,8 kV (ou GT 4) 
conforme instruções próprias, caso essa estivesse 
operando antes do desligamento, e sincronizá-la. 
Deverão ser tomadas as 
medidas necessárias para 
possibilitar o fechamento do 
sincronismo, gerar o mínimo 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UTE 
Jorge Lacerda A IO-OI.S.UTLA 16 3.7.5.3. 02/08/2024 
 
Referência:  6 / 10 
 
Passo Executor Procedimentos Condições ou Limites 
Associados 
necessário para manter as 
unidades geradoras 
sincronizadas e solicitar 
autorização ao COSR -S para 
elevar a geração. 
O sincronismo será efetuado 
através do fechamento do 
disjuntor da unidade geradora 
na SE Jorge Lacerda A. 
1.2 
Diamante 
Geração 
Partir a unidade geradora GT 4 13,8 kV (ou GT 3) 
conforme instruções próprias, caso essa estivesse 
operando antes do desligamento, e sincronizá-la. 
Deverão ser tomadas as 
medidas necessárias para 
possibilitar o fechamento do 
sincronismo, gerar o mínimo 
necessário para manter as 
unidades geradoras 
sincronizadas e solicitar 
autorização ao COSR -S para 
elevar a geração. 
O sincronismo será efetuado 
através do fechamento do 
disjuntor da unidade geradora 
na SE Jorge Lacerda A. 
2 
Diamante 
Geração 
Receber tensão da SE Jorge Lacerda A , energizado 
os serviços auxiliares das unidades geradoras GT 1 
e GT 2 13,8 kV. 
 
2.1 
Diamante 
Geração 
Partir a unidade geradora GT 1 13,8 kV (ou GT 2) 
conforme instruções próprias, caso essa estivesse 
operando antes do desligamento, e sincronizá-la. 
Deverão ser tomadas as 
medidas necessárias para 
possibilitar o fechamento do 
sincronismo, gerar o mínimo 
necessário para manter as 
unidades geradoras 
sincronizadas e solicitar 
autorização ao COSR -S para 
elevar a geração. 
O sincronismo será efetuado 
por meio  do fechamento do 
disjuntor da unidade geradora 
na SE Jorge Lacerda A. 
2.2 
Diamante 
Geração 
Partir a unidade geradora GT 2 13,8 kV (ou GT 1) 
conforme instruções próprias, caso essa estivesse 
operando antes do desligamento, e sincronizá-la. 
Deverão ser tomadas as 
medidas necessárias para 
possibilitar o fechamento do 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UTE 
Jorge Lacerda A IO-OI.S.UTLA 16 3.7.5.3. 02/08/2024 
 
Referência:  7 / 10 
 
Passo Executor Procedimentos Condições ou Limites 
Associados 
sincronismo, gerar o mínimo 
necessário para manter as 
unidades geradoras 
sincronizadas e solicitar 
autorização ao COSR -S para 
elevar a geração. 
O sincronismo será efetuado 
por meio do fechamento do 
disjuntor da unidade geradora 
na SE Jorge Lacerda A. 
5.2.2.2. POR MEIO DA UNIDADE GERADORA GT 1 OU GT 2 13,8 KV 
O Agente Operador da Instalação deve realizar com autonomia os seguintes procedimentos para a 
recomposição: 
Passo Executor Procedimentos Condições ou Limites 
Associados 
1 
Diamante 
Geração 
Receber tensão da SE Jorge Lacerda A , 
energizado os serviços auxiliares das unidades 
geradoras GT 1 e GT 2 13,8 kV 
 
1.1 
Diamante 
Geração 
Partir a unidade geradora GT 1 13,8 kV (ou GT 
2) conforme instruções próprias, caso essa 
estivesse operando antes do desligamento, e 
sincronizá-la. 
Deverão ser tomadas as 
medidas necessárias para 
possibilitar o fechamento do 
sincronismo, gerar o mínimo 
necessário para manter as 
unidades geradoras 
sincronizadas e solicitar 
autorização ao COSR -S para 
elevar a geração. 
O sincronismo será efetuado 
por meio  do fechamento do 
disjuntor da unidade geradora 
na SE Jorge Lacerda A. 
1.2 
Diamante 
Geração 
Partir a unidade geradora GT 2 13,8 kV (ou GT 
1) conforme instruções próprias, caso essa 
estivesse operando antes do desligamento, e 
sincronizá-la. 
Deverão ser tomadas as 
medidas necessárias para 
possibilitar o fechamento do 
sincronismo, gerar o mínimo 
necessário para manter as 
unidades geradoras 
sincronizadas e solicitar 
autorização ao COSR -S para 
elevar a geração. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UTE 
Jorge Lacerda A IO-OI.S.UTLA 16 3.7.5.3. 02/08/2024 
 
Referência:  8 / 10 
 
Passo Executor Procedimentos Condições ou Limites 
Associados 
O sincronismo será efetuado 
por meio do fechamento do 
disjuntor da unidade geradora 
na SE Jorge Lacerda A. 
2 
Diamante 
Geração 
Receber tensão da SE Jorge Lacerda A, pelo 
transformador TF 5 230/139,6/6,6 kV e fechar o 
disjuntor do lado de 6,6  kV energizando os 
serviços auxiliares. 
 
2.1 
Diamante 
Geração 
Partir a unidade geradora GT 3 13,8 kV (ou GT 
4) conforme instruções próprias, caso essa 
estivesse operando antes do desligamento, e 
sincronizá-la. 
Deverão ser tomadas as 
medidas necessárias para 
possibilitar o fechamento do 
sincronismo, gerar o mínimo 
necessário para manter as 
unidades geradoras 
sincronizadas e solicitar 
autorização ao COSR -S para 
elevar a geração.  
O sincronismo será efetuado 
por meio  do fechamento do 
disjuntor da unidade geradora 
na SE Jorge Lacerda A. 
2.2 
Diamante 
Geração 
Partir a unidade geradora GT 4 13,8 kV (ou GT 
3) conforme instruções próprias, caso essa 
estivesse operando antes do desligamento, e 
sincronizá-la. 
Deverão ser tomadas as 
medidas necessárias para 
possibilitar o fechamento do 
sincronismo, gerar o mínimo 
necessário para manter as 
unidades geradoras 
sincronizadas e solicitar 
autorização ao COSR -S para 
elevar a geração.  
O sincronismo será efetuado 
por meio do fechamento do 
disjuntor da unidade geradora 
na SE Jorge Lacerda A. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2, enquanto não houver 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UTE 
Jorge Lacerda A IO-OI.S.UTLA 16 3.7.5.3. 02/08/2024 
 
Referência:  9 / 10 
 
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia deste na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR-S. 
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
Para os desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
O Agente Operador deve recompor a Instalação conforme Subitem 6.2.2, sem necessidade de autorização 
do ONS.  
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, o Agente 
Operador deve informar ao COSR-S, para que a recomposição da Instalação seja executada com controle do 
COSR-S. 
6. MANOBRAS DE UNIDADES GERADORAS, LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desligamento programado ou de urgência de unidades geradoras, e 
desenergização programada ou de urgência de equipamentos, só podem ser efetuados com controle 
do COSR-S. 
6.1.2. Os procedimentos para energização de equipamentos, ou sincronismo de unidades geradoras, após 
desligamentos programados, de urgência ou de emergência, só podem ser efetuados com controle 
do COSR-S. 
6.1.3. Os procedimentos para sincronismo de unidades geradoras, após desligamento automático sem 
atuação de proteção que impeça o retorno do equipamento, só podem ser executados com 
autonomia pelo Agente Operador da Instalação quando estiverem explicitados e estiverem atendidas 
as condições do Subitem 6.2.2. desta Instrução de Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.5. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S.  
6.1.6. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S. 
6.1.7. A partida de unidades geradoras deve seguir critérios próprios do Agente. O sincronismo à Rede de 
Distribuição deve ser realizado após tratativas entre a Usina e o Agente de Distribuição. A tomada de 
carga deve ser realizada com controle do COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da UTE 
Jorge Lacerda A IO-OI.S.UTLA 16 3.7.5.3. 02/08/2024 
 
Referência:  10 / 10 
 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESLIGAMENTO DE UNIDADES GERADORAS E DESENERGIZAÇÃO DE EQUIPAMENTOS 
A des ligamento de unidades geradoras e a desenergização de equipamentos, pertencentes à Rede de 
Operação é sempre controlada pelo COSR-S. 
6.2.2. SINCRONISMO DE UNIDADES GERADORAS E ENERGIZAÇÃO DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de unidades geradoras ou de equipamentos. 
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais unidades geradoras e equipamentos em operação, conforme explicitado nas 
condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Equipamento / 
Unidade Geradora Procedimentos Condições ou Limites Associados 
Transformador TF 5 
230/139,6/6,6 kV 
Sentido Único: A partir do lado de 230 kV 
A energização, pelo lado de 230 kV, será efetuada pela operação da SE Jorge 
Lacerda A, conforme estabelecido na IO-OI.S.JLA 
Ligar, energizando os serviços auxiliares 
de 6,6 kV, o lado de 6,6 kV do TF 5 
230/139,6/6,6 kV. 
A área de 6,6 kV deve estar 
desenergizada. 
Unidade Geradora 
GT 1, GT 2, GT 3 ou 
GT 4 13,8 kV 
Partir e sincronizar a unidade geradora. Conforme procedimentos internos do 
Agente.  
Gerar o mínimo necessário para manter 
a unidade geradora sincronizada. 
Elevar a geração da unidade geradora. Após autorização do COSR-S. 
7. NOTAS IMPORTANTES 
Não se aplica. 
