 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Ponta Grossa Norte 
 
 
Código Revisão Item Vigência 
IO-OI.S.PGN 25 3.7.5.4. 25/09/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Alteração das condições de autonomia do Agente para energização, desvinculando os procedimentos do 
Subitem 6.2.2 dos procedimentos de recomposição fluente da Instalação constantes no Subitem 5.2.2, 
passando-se a adotar, no Subitem 6.2.2, condições de sistema completo e/ou sistema N -1 para energização 
das seguintes linhas de transmissão e equipamentos: 
• LT 230 kV Ponta Grossa Norte / Ponta Grossa Sul; 
• Transformador ATF-A ou ATF-B 230/138/13,8 kV; 
• Transformador TF-1 ou TF-2 230/34,5/13,8 kV. 
- Alteração das condições de autonomia do Agente para fechamento em anel, no Subitem 6.2.2, das seguintes 
linhas de transmissão e equipamentos: 
• LT 230 kV Ponta Grossa / Ponta Grossa Norte C2; 
• Transformador ATF-A ou ATF-B 230/138/13,8 kV; 
• Transformador TF-1 ou TF-2 230/34,5/13,8 kV. 
- Inclusão dos Subitens 5.4.1.2. e 5.4.2.2. e complementação do Subitem 5.4.2.3. para casos de desligamentos 
parciais. 
- Adequação à RT-OI.BR revisão 37, com destaque à inclusão dos Subitens 6.1.8 e 6.1.9.  
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S Copel DIS Copel GeT 
CGT 
Eletrosul 
(COT Norte) 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Ponta Grossa Norte IO-OI.S.PGN 25 3.7.5.4. 25/09/2024 
 
Referência:  2 / 12 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  4 
3.1. Barramento de 230 kV ................................................................................................................... 4 
3.2. Alteração da Configuração do Barramento ................................................................................... 4 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 4 
4.1. Procedimentos Gerais ................................................................................................................... 4 
4.2. Procedimentos Específicos ............................................................................................................ 5 
4.2.1. Operação dos Comutadores de Tape sob Carga (LTC) ................................................ 5 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  5 
5.1. Procedimentos Gerais ................................................................................................................... 5 
5.2. Procedimento para Recomposição Fluente .................................................................................. 6 
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................ 6 
5.2.2. Recomposição Fluente da Instalação .......................................................................... 6 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 8 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 8 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 8 
5.4. Procedimentos Após Desligamento Parcial da Instalação ............................................................ 8 
5.4.1. Preaparação da Instalação para a Recomposição Após Desligamento Parcial ........... 8 
5.4.2. Recomposição Após Desligamento Parcial da Instalação ........................................... 8 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 9 
6.1. Procedimentos Gerais ................................................................................................................... 9 
6.2. Procedimentos Específicos .......................................................................................................... 10 
6.2.1. Desenergização de Linhas de Transmissão e de Equipamentos ............................... 10 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ..................................... 10 
7. NOTAS IMPORTANTES ................................ ................................ ................................ ................ 12 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Ponta Grossa Norte IO-OI.S.PGN 25 3.7.5.4. 25/09/2024 
 
Referência:  3 / 12 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE Ponta Grossa Norte, definidos pelo ONS, responsável 
pela coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos 
de Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR -S e a Instalação, no que se refere aos equipamentos de 
manobra na Instalação, é realizada conforme segue. 
Linha de Transmissão ou 
Equipamento Agente de Operação Agente Operador Centro de Operação 
do Agente Operador 
LT 230 kV Ponta Grossa / 
Ponta Grossa Norte C1 (*) 
CGT Eletrosul 
(linha de Transmissão) Copel GeT COGT Copel Copel GeT 
(módulo na SE PGN) 
LT 230 kV Ponta Grossa / 
Ponta Grossa Norte C2 Copel GeT Copel GeT COGT Copel 
LT 230 kV Ponta Grossa Norte 
/ Ponta Grossa Sul Copel GeT Copel GeT COGT Copel 
Barramento de 230 kV Copel GeT Copel GeT COGT Copel 
Transformador ATF-A 
230/138/13,8 kV Copel GeT Copel GeT COGT Copel 
Transformador ATF-B 
230/138/13,8 kV Copel GeT Copel GeT COGT Copel 
Transformador TF-1 
230/34,5/13,8 kV Copel GeT Copel GeT COGT Copel 
Transformador TF-2 
230/34,5/13,8 kV Copel GeT Copel GeT COGT Copel 
(*) A CGT Eletrosul é o agente responsável pela operação da LT 230 kV Ponta Grossa / Ponta Grossa Norte  
C1, por intermédio do COT Norte. 
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte da Área 230 kV do Paraná. 
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.4.1. A definição da quantidade  de tentativas de religamento manual de linhas de transmissão ou de 
equipamentos, bem como o intervalo entre elas, é de responsabilidade do Agente e devem estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Ponta Grossa Norte IO-OI.S.PGN 25 3.7.5.4. 25/09/2024 
 
Referência:  4 / 12 
 
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR -S autorização 
para religamento. Nessa oportunidade, o Agente também pode solicitar alteração no sentido normal 
para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em consideração as condições operativas do 
sistema. 
2.5. Quando caracterizado impedimento de linha de transmissão  ou de equipamento , deve m ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação , quando o Agente tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 230 KV 
A configuração do barramento de 230 kV é do tipo Principal e Transferência (Barra BP  eBarra BT). Na 
operação normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados, exceto as 
seccionadoras de transferência das linhas de transmissão ou equipamentos, bem com o, a barra de 
transferência deve estar desenergizada , com o disjuntor de transferência aberto e as seccionadoras do 
módulo de transferência fechadas. 
3.2. ALTERAÇÃO DA CONFIGURAÇÃO DO BARRAMENTO 
A mudança de configuração do barramento de 230 kV desta Instalação é executada com controle do COSR-
S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade do Agente Operador 
da Instalação. 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S. 
As f aixas para controle de tensão para ess e barramento estão estabelecidas no Cadastro de 
Informações Operacionais das Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. Os barramentos de 138 kV , 34,5 kV e 13,8 kV,  não pertencente à Rede de Operação, onde se 
conectam as transformações 230/138/13,8 kV e de 230/34,5/13,8 kV, têm a sua regulação de tensão 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Ponta Grossa Norte IO-OI.S.PGN 25 3.7.5.4. 25/09/2024 
 
Referência:  5 / 12 
 
executada com autonomia pelo Agente Operador da Instalação, por meio da utilização de recursos 
locais disponíveis de autonomia dessa. 
Esgotados esses recursos, o Agente deve acionar o COSR -S, que deve verificar a disponibilidade dos 
recursos sistêmicos. 
As faixas para controle de tensão para esses barramentos estão estabelecidas no Cadastro de 
Informações Operacionais das Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.3. Os demais barramentos , não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob responsabilidade da Instalação. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
4.2.1. OPERAÇÃO DOS COMUTADORES DE TAPE SOB CARGA (LTC) 
Os LTCs dos transformadores ATF -A e ATF -B 230/138/13,8 kV e TF -1 e TF -2 230/34,5/13,8 kV operam em 
modo automático. 
Alterações no modo de operação desses comutadores são executadas com autonomia pela operação d o 
Agente Copel GeT. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de um desligamento da instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir, para atendimento às necessidades sistêmicas: 
• Desligamento total da Instalação: caracterizado quando não há tensão em todos os terminais de 
suas linhas de transmissão. 
•  Desligamento parcial da Instalação: qualquer outra configuração que não se enquadre como 
desligamento total. 
5.1.2. Quando de um desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao 
COSR-S as informações a seguir: 
• horário da ocorrência; 
• configuração da subestação logo após a ocorrência; 
• configuração da instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2, sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificand o a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia do Agente Oeprador da Instalação  na 
recomposição, deve ser utilizado o Subitem 5.3.  
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Ponta Grossa Norte IO-OI.S.PGN 25 3.7.5.4. 25/09/2024 
 
Referência:  6 / 12 
 
5.2. PROCEDIMENTO PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE 
No caso de desligamento total, o Agente Operador deve configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• das linhas de transmissão:  
LT 230 kV Ponta Grossa / Ponta Grossa Norte C1; 
LT 230 kV Ponta Grossa / Ponta Grossa Norte C2; 
LT 230 kV Ponta Grossa Norte / Ponta Grossa Sul; 
• de todas as linhas de transmissão de 138 kV e de 34,5 kV. 
• dos transformadores: 
ATF-A (lados de 230 kV e de 138 kV); 
ATF-B (lados de 230 kV e de 138 kV); 
TF-1 (lados de 230 kV, de 34,5 kV e de 13,8 kV); 
TF-2 (lados de 230 kV, de 34,5 kV e de 13,8 kV). 
Desligar ou manter desligado o modo de comutação automática dos comutadores sob carga da 
transformação 230/138/13,8 kV e da transformação 230/34,5/13,8 kV da SE Ponta Grossa Norte. 
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO 
A Instalação faz parte da recomposição da Área Gov. Ney Aminthas de Barros Braga. o Agente Operador deve 
adotar os procedimentos a seguir para recomposição fluente: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 Copel GeT 
Receber tensão da SE Ponta Grossa , pela LT 
230 kV Ponta Grossa / Ponta Grossa Norte C2, e 
energizar o barramento de 230 kV. 
 
1.1 Copel GeT 
Energizar, pelo lado de 230 kV , um dos 
transformadores 230/138/13,8 kV da SE Ponta 
Grossa Norte, e energizar o barramento de 138 
kV pelo lado de 138 kV. 
TAPPGN-230/138 = 9. 
Possibilitar o restabelecimento de 
carga prioritária na SE Ponta 
Grossa Norte e nas subestações 
atendidas pela SE Ponta Grossa 
Norte. 
1.1.1 Copel DIS 
Restabelecer carga prioritária  na SE Ponta 
Grossa Norte e nas subestações atendidas pela 
SE Ponta Grossa Norte. 
No máximo de 70 MW. 
Respeitar o limite mínimo de 
tensão, de 124 kV, no barramento 
de 138 kV. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Ponta Grossa Norte IO-OI.S.PGN 25 3.7.5.4. 25/09/2024 
 
Referência:  7 / 12 
 
Passo Executor Procedimentos Condições ou Limites Associados 
1.2 Copel GeT 
Energizar, pelo lado de 230 kV , o  segundo 
transformador 230/138/13,8 kV da SE Ponta 
Grossa Norte, e ligar, em anel, pelo lado de 138 
kV. 
TAPPGN-230/138 = 9. 
Após fluxo de potência ativa  no 
primeiro transformador 
230/138/13,8 kV que foi 
energizado. 
1.3 Copel GeT 
Energizar a LT 230 kV Ponta Grossa Norte / 
Ponta Grossa Sul, enviando tensão para a SE 
Ponta Grossa Sul. 
 
1.4 Copel GeT 
Energizar, pelo lado de 230 kV, um dos 
transformadores 230/34,5/13,8 kV da SE Ponta 
Grossa Norte e ligar, energizando o barramento 
de 34,5 kV, o lado de 34,5 kV. 
TAPPGN-230/34,5 =11. 
Após fluxo de potência ativa  no 
primeiro transformador 
230/138/13,8 kV que foi 
energizado. 
Possibilitar o restabelecimento de 
carga prioritária na SE Ponta 
Grossa Norte e nas subestações 
atendidas pela SE Ponta Grossa 
Norte. 
1.4.1 Copel DIS 
Restabelecer carga prioritária  na SE Ponta 
Grossa Norte e nas subestações atendidas pela 
SE Ponta Grossa Norte. 
No máximo 30 MW. 
Respeitar o limite mínimo de 
tensão, de 31 kV, no barramento 
de 34,5 kV. 
1.5 Copel GeT 
Energizar, pelo lado de 230 kV , o segundo 
transformador 230/34,5/13,8 kV da SE Ponta 
Grossa Norte, e energizar o barramento de 13,8 
kV, pelo lado de 13,8 kV. 
TAPPGN-230/34,5 =11. 
Após fluxo de potência ativa  no 
transformador 230/34/13,8  kV da 
SE Ponta Grossa Norte que foi 
energizado. 
1.5.1 Copel DIS 
Restabelecer carga prioritária  na SE Ponta 
Grossa Norte e nas subestações atendidas pela 
SE Ponta Grossa Norte. 
No máximo 10 MW, totalizando, 
no máximo , 40 MW  na 
transformação 230/34,5/13,8 kV  
da SE Ponta Grossa Norte e 110 
MW na SE Ponta Grossa Norte. 
Respeitar o limite mínimo de 
tensão, de 12,4 kV, no barramento 
de 13,8 kV. 
Os demais equipamentos desta Instalação são restabelecidos sob controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Ponta Grossa Norte IO-OI.S.PGN 25 3.7.5.4. 25/09/2024 
 
Referência:  8 / 12 
 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2, enquanto não houver 
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia deste na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras.  
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREAPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, o 
Agente Operador da Instalação deve preparar a Instalação conforme Subitem 5.2.1, sem 
necessidade de autorização do ONS. 
5.4.1.2. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão nas barras de 230 kV, os Agentes Operadores devem preparar o setor de 230 
kV d a Instalação (disjuntores das linhas de transmissão e equipamentos) conforme Subitem 
5.2.1., sem necessidade de autorização do ONS.  
5.4.1.3. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador  
deve recompor a Instalação conforme Subitem 5.2.2, sem necessidade de autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., os Agentes 
Operadores devem informar ao COSR-S, para que a recomposição da Instalação seja executada com 
controle do COSR-S. 
5.4.2.3. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.3., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 6.2.2., sem necessidade de 
autorização do ONS. 
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
os Agentes Operadores devem informar ao COSR -S, para que a recomposição da Instalação seja 
executada com controle do COSR-S. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Ponta Grossa Norte IO-OI.S.PGN 25 3.7.5.4. 25/09/2024 
 
Referência:  9 / 12 
 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, após 
desligamentos programados, de urgência ou de emergência, só podem ser efetuados com controle 
do COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de  proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelo Agente Operador da Instalação quando 
estiverem explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução de 
Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, o Agente Operador da Instalação deve verificar se existe 
tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pelo Agente Operador da Instalação 
quando estiver especificado nesta Instrução de Operação e estiverem atendidas as condições do 
Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou, quando permitida, em sentido inverso, pode ser feita com 
autonomia pelo Agente Operador , conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica.  
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o Agente Operador da Instalação deve fechá-lo em anel desde que tenha autonomia para 
tal, adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Ponta Grossa Norte IO-OI.S.PGN 25 3.7.5.4. 25/09/2024 
 
Referência:  10 / 12 
 
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S. 
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
A desenergização de linhas de transmissão equipamentos, pertencentes à Rede de Operação é sempre 
controlada pelo COSR-S. 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de equipamentos ou de linhas de transmissão. 
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado nas 
condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
LT 230 kV Ponta Grossa / 
Ponta Grossa Norte C1 ou 
C2 
Sentido Normal: SE Ponta Grossa Norte recebe tensão da SE Ponta Grossa. 
Ligar, em anel, a LT 230 kV Ponta Grossa 
/ Ponta Grossa Norte C1 (ou C2). 
 
Sentido Inverso: SE Ponta Grossa Norte envia tensão para SE Ponta Grossa. 
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2PR. 
LT 230 kV Ponta Grossa 
Norte / Ponta Grossa Sul  
Sentido Normal: SE Ponta Grossa Norte envia tensão para SE Ponta Grossa 
Sul. 
Energizar a LT 230 kV Ponta Grossa Sul / 
Ponta Grossa Norte. 
Sistema completo (de LT e TR) ou N-
1 (de LT ou TR) na SE Ponta Grossa 
Norte 230 kV 
• VPGN-230≤ 242 kV. 
Sentido Inverso: SE Ponta Grossa Norte recebe tensão da SE Ponta Grossa Sul. 
Ligar, em anel, a LT 230 kV Ponta Grossa 
Norte / Ponta Grossa Sul. 
 
Transformador ATF-A ou Sentido Único: A partir do lado de 230 kV. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Ponta Grossa Norte IO-OI.S.PGN 25 3.7.5.4. 25/09/2024 
 
Referência:  11 / 12 
 
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
ATF-B 230/138/13,8 kV Energizar, pelo lado de 230 kV, o 
transformador ATF- A 230/138/13,8 kV 
(ou ATF-B). 
Sistema completo  (de LT  e TR 
230/34,5) ou N -1 ( de LT ou TR 
230/34,5) na SE Ponta Grossa Norte 
230 kV 
Como primeiro ou segundo 
transformador: 
• VPGN-230 ≤ 242 kV. 
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa nos transformadores 
230/138/13,8 kV que já foram 
energizados. 
Ligar, energizando o barramento de 138 
kV, ou em anel,  o lado  de 138 kV  do 
transformador ATF -A 230/138/13,8 kV 
(ou ATF-B). 
 
Em caso de abertura apenas do lado de 230 kV, proceder conforme segue: 
Ligar, interligando esse transformador 
com outro já em operação, o lado de  
230 kV do transformador ATF -A 
230/138/13,8 kV (ou ATF-B). 
Outro transformador 230/138/13,8 
kV da SE Ponta Grossa Norte com 
fluxo de potência ativa. 
 
Transformador TF-1 ou TF-2 
230/34,5/13,8 kV 
Sentido Único: A partir do lado de 230 kV. 
Energizar, pelo lado de 230 kV, o 
transformador TF- 1 230/34,50/13,8 kV 
(ou TF-2). 
Sistema completo (de LT e TR 
230/138) ou N -1 (de LT ou TR 
230/138) na SE Ponta Grossa Norte 
230 kV 
Como primeiro ou segundo 
transformador:     
• VPGN-230 ≤ 242 kV; e 
• 1 ≤ TAPPGN-230/34,5 ≤ 11. 
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa nos transformadores 
230/34,5/13,8 kV que já foram 
energizados. 
Ligar, energizando a seção do 
barramento de 34,5 kV correspondente, 
o lado de 34,5 kV do transformador TF-
1 230/ 34,5/13,8 kV (ou TF-2). 
Barramento de 34,5 kV da SE Ponta 
Grossa Norte desenergizado. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Ponta Grossa Norte IO-OI.S.PGN 25 3.7.5.4. 25/09/2024 
 
Referência:  12 / 12 
 
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
Ligar, energizando a seção do 
barramento de 13,8 kV correspondente, 
o lado de 13,8 kV do transformador TF-
1 230/34,5/13,8 kV (ou TF-2). 
Barramento de 13,8 kV da SE Ponta 
Grossa Norte desenergizado. 
7. NOTAS IMPORTANTES 
Não se aplica. 
