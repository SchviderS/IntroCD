Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.11
Cadastro de Informações Operacionais
Cadastro de Dados Operacionais de Equipamentos da Interligação Norte / Nordeste
Código Revisão Item Vigência
CD-CT.NNE.01 70 2.1.1. 28/04/2023
.
MOTIVO DA REVISÃO
Adequações de textos conforme RT-CD.BR.01 Revisão 09.
Adequações de informações quanto aos dados operacionais de bancos de capacitores série no item 4.
Inclusão do banco de capacitores da SE Ribeiro Gonçalves na LT 500 kV Colinas / Ribeiro Gonçalves C2 
no item 4.2.
Inclusão do banco de capacitores da SE Ribeiro Gonçalves na LT 500 kV Ribeiro Gonçalves / São João do 
Piauí C2 no item 4.4.
Adequação do nome do banco de capacitores da SE São João do Piauí da LT 500 kV São João do Piauí / 
Boa Esperança no item 4.5.
LISTA DE DISTRIBUIÇÃO
CNOS COSR-NCO COSR-NE ARGO CHESF
CTEEP ELETRONORTE IENNE NEOENERGIA TAESA
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Interligação Norte / Nordeste CD-CT.NNE.01 70 2.1.1. 28/04/2023
Referência: 2 / 15
ÍNDICE
1. OBJETIVO.........................................................................................................................................4
2. CONSIDERAÇÕES GERAIS .................................................................................................................4
3. DADOS OPERACIONAIS DE LINHAS DE TRANSMISSÃO......................................................................4
3.1. LT 500 kV Bacabeira / Parnaiba III C1 ou C2..................................................................................5
3.2. LT 500 kV Colinas / Ribeiro Gonçalves C1 ou C2 ...........................................................................6
3.3. LT 500 KV Gilbués / Miracema C3 .................................................................................................6
3.4. LT 500 kV Miranda II / Bacabeira C1 ou C2 ...................................................................................7
3.5. LT 500 kV Presidente Dutra / Boa Esperança ................................................................................7
3.6. LT 500 kV Presidente Dutra / Teresina II C1 ( C8 ) ou C2 (C9) .......................................................8
3.7. LT 500 kV Ribeiro Gonçalves / São João do Piauí C1 ou C2 ...........................................................9
3.8. LT 500 kV São João do Piauí / Boa Esperança................................................................................9
4. DADOS OPERACIONAIS DE BANCOS DE CAPACITORES SÉRIE ..........................................................10
4.1. SE Ribeiro Gonçalves – Banco de Capacitores Série BCS1 500 kV / 462 Mvar da LT 500 kV 
Colinas / Ribeiro Gonçalves C1....................................................................................................10
4.1.1. SE Ribeiro Gonçalves – Banco de Capacitores Série BCS1 500 kV / 462 Mvar da LT 
500 kV Colinas / Ribeiro Gonçalves C1 - Curva Característica Tensão x Corrente.....11
4.1.2. SE Ribeiro Gonçalves – Banco de Capacitores Série BCS1 500 kV / 462 Mvar da LT 
500 kV Colinas / Ribeiro Gonçalves C1 - Curva Resfriamento do MOV Temperatura x 
Tempo........................................................................................................................11
4.2. SE Ribeiro Gonçalves – Banco de Capacitores Série BCS3 500 kV / 442 Mvar da LT 500 kV 
Colinas / Ribeiro Gonçalves C2....................................................................................................12
4.2.1. SE Ribeiro Gonçalves – Banco de Capacitores Série BCS3 500 kV / 442 Mvar da LT 
500 kV Colinas / Ribeiro Gonçalves C2 - Curva Característica Tensão X Corrente.....12
4.2.2. SE Ribeiro Gonçalves – Banco de Capacitores Série BCS3 500 kV / 442 Mvar da LT 
500 kV Colinas / Ribeiro Gonçalves C2 - Curva Resfriamento do MOV Temperatura X 
Tempo........................................................................................................................12
4.3. SE Ribeiro Gonçalves – Banco de Capacitores Série BCS2 500 kV / 425 Mvar da LT 500 kV 
Ribeiro Gonçalves / São João do Piauí C1....................................................................................13
4.3.1. SE Ribeiro Gonçalves – Banco de Capacitores Série BCS2 500 kV / 425 Mvar da LT 
500 kV Ribeiro Gonçalves / São João do Piauí C1 - Curva Característica Tensão X 
Corrente ....................................................................................................................13
4.3.2. SE Ribeiro Gonçalves – Banco de Capacitores Série BCS2 500 kV / 425 Mvar da LT 
500 kV Ribeiro Gonçalves / São João do Piauí C1 - Curva Resfriamento do MOV 
Temperatura x Tempo ...............................................................................................14
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Interligação Norte / Nordeste CD-CT.NNE.01 70 2.1.1. 28/04/2023
Referência: 3 / 15
4.4. SE Ribeiro Gonçalves – Banco de Capacitores Série BCS4 500 kV / 425 Mvar da LT 500 kV 
Ribeiro Gonçalves / São João do Piauí C2....................................................................................14
4.4.1. SE Ribeiro Gonçalves – Banco de Capacitores Série BCS4 500 kV / 425 Mvar da LT 
500 kV Ribeiro Gonçalves / São João do Piauí C2 - Curva Característica Tensão X 
Corrente ....................................................................................................................14
4.4.2. SE Ribeiro Gonçalves – Banco de Capacitores Série BCS4 500 kV / 425 Mvar da LT 
500 kV Ribeiro Gonçalves / São João do Piauí C2 - Curva Resfriamento do MOV 
Temperatura x Tempo ...............................................................................................15
4.5. SE São João do Piauí – Banco de capacitores série 05H2 500 Kv / 484 Mvar da LT 500 kV São 
João do Piauí / Boa Esperança.....................................................................................................15
4.5.1. SE São João do Piauí – Banco de capacitores série 05H2 500 Kv / 484 Mvar da LT 500 
kV São João do Piauí / Boa Esperança - Curva Característica Tensão X Corrente......15
4.5.2. SE São João do Piauí – Banco de capacitores série 05H2 500 Kv / 484 Mvar da LT 500 
kV São João do Piauí / Boa Esperança - Curva Resfriamento do MOV Temperatura x 
Tempo........................................................................................................................15
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Interligação Norte / Nordeste CD-CT.NNE.01 70 2.1.1. 28/04/2023
Referência: 4 / 15
1. OBJETIVO
Apresentar os Dados Operacionais de Equipamentos da interligação Norte / Nordeste, a serem observados 
pelos operadores dos Centros de Operação do ONS e pela Operação dos Agentes envolvidos, de acordo com 
os Procedimentos de Rede.
2. CONSIDERAÇÕES GERAIS
2.1. Os dados de linhas de transmissão e equipamentos da Rede de Operação constantes neste Cadastro 
de Informações Operacionais devem ser observados pelos Centros de Operação do ONS para as 
ações de coordenação, supervisão e controle da operação, assim como pelos Agentes para as ações 
de comando e execução.
2.2. As limitações técnicas para tentativas de religamento de equipamentos contidas neste documento 
têm caráter operacional. Poderão ser utilizadas para apuração da qualidade do serviço público de 
transmissão de energia elétrica (apuração de parcela variável) o valor do “Tempo para energização 
após desligamento” de cada equipamento, desde que sejam atendidos os critérios estabelecidos na 
regulação vigente.
3. DADOS OPERACIONAIS DE LINHAS DE TRANSMISSÃO
Orientações sobre os dados operacionais de linhas de transmissão:
Campo “Religamento Automático”:
Campo “Tipo”: indica o tipo de religamento utilizado em operação normal: monopolar, tripolar ou 
monopolar / tripolar; caso não exista religamento instalado, o campo é preenchido com “NE” – não 
existe.
Campo “Situação”: indica se o religamento está ligado ou desligado em operação normal; caso não 
exista religamento instalado, o campo é preenchido com “NE” – não existe.
Campo “Sentido”: para religamentos tripolares, indica em primeiro lugar a instalação onde se dá o 
fechamento inicial (terminal líder), e após a seta, a instalação de fechamento final (terminal 
seguidor); para religamentos monopolares ou caso não exista religamento instalado, o campo é 
preenchido com hifens (---).
Campo “Ajustes”: indica os ajustes de tempo morto, diferença de tensão, defasagem angular e 
diferença de frequência instalados para a função de verificação de sincronismo para o religamento 
automático; caso não exista religamento automático instalado, o campo é preenchido com hifens 
(---).
Campo “LT em Mesma Estrutura”: apresenta o nome da LT que compartilha estrutura, e quando disponível, 
a informação da extensão do trecho de compartilhamento e da quantidade de torres compartilhadas; caso 
não exista, o campo é preenchido com “NE” – não existe.
Não deve ser considerado o compartilhamento de estrutura caso esse seja apenas na chegada as subestações 
envolvidas.
Campo “Religamentos Manuais”:
Campo “Quantidade de Tentativas”: indica quantos religamentos (ou tentativas de religamento) 
manuais são permitidos pelo Agente.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Interligação Norte / Nordeste CD-CT.NNE.01 70 2.1.1. 28/04/2023
Referência: 5 / 15
Campo “Intervalo entre os Religamentos (min)”: é o intervalo de tempo entre os religamentos, em 
minutos, a ser considerado entre a tentativa de fechamento não-bem-sucedida anterior (incluindo 
o religamento automático) e o novo fechamento do disjuntor.
Campo “Dispositivo de Sincronismo”: indica os terminais onde esses estão instalados e em 
funcionamento. Dispositivo de Sincronismo é o equipamento utilizado para verificação das 
condições de fechamento em anel ou paralelo entre dois pontos elétricos; caso não exista, o campo 
é preenchido com “NE” – não existe. Se em algum disjuntor do terminal não houver o dispositivo 
de sincronismo essa informação deve ser destacada.
Campo “Ajustes”: indica os ajustes de diferença de tensão, defasagem angular e diferença de 
frequência instalados para a função de verificação de sincronismo para o religamento manual de 
cada terminal; caso não exista dispositivo de sincronismo, o campo é preenchido com hifens (---).
Campo “Notas / Restrições”: campo opcional que indica informações relevantes a respeito da linha de 
transmissão.
Caso os dados operacionais não sejam informados pelo Agente, os campos são preenchidos com “NI” – não 
informado.
3.1. LT 500 KV BACABEIRA / PARNAIBA III C1 OU C2
Religamento Automático
Tipo Situação Sentido Ajustes
LT em Mesma Estrutura
Tripolar Ligado Bacabeira  
Paranaíba III
V  100 kV
   30°
f   0,2 Hz
Tempo morto: 5s
NE
Notas / Restrições: NI
Religamento Manual
Sentido Quantidade de 
Tentativas
Intervalo entre os 
religamentos (minutos)
Dispositivo de 
sincronismo Ajustes
--- 2 NI. Em ambos os 
terminais NI
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Interligação Norte / Nordeste CD-CT.NNE.01 70 2.1.1. 28/04/2023
Referência: 6 / 15
3.2. LT 500 KV COLINAS / RIBEIRO GONÇALVES C1 OU C2
Religamento Automático
Tipo Situação Sentido Ajustes
LT em Mesma Estrutura
Tripolar Ligado
Colinas  
Ribeiro 
Gonçalves
V  100 kV
   40°
f   0,2 Hz
Tempo morto: 5s
NE
Notas / Restrições: O primeiro terminal a religar (terminal líder) é o da SE Colinas, pelo disjuntor 500 kV 
lado da barra. O segundo terminal a religar (terminal seguidor) é o terminal da SE Ribeiro Gonçalves, pelo 
disjuntor 500 kV lado da barra, 2 segundos após o fechamento do terminal líder. Para o C2, após o 
religamento, o BCS da LT é inserido automaticamente. Os disjuntores centrais em Colinas e Ribeiro 
Gonçalves, após religamento, serão fechados manualmente.
Religamento Manual
Sentido Quantidade de 
Tentativas
Intervalo entre os 
religamentos (minutos)
Dispositivo de 
sincronismo Ajustes
--- 2 3 Em ambos os 
terminais NI
3.3. LT 500 KV GILBUÉS / MIRACEMA C3
Religamento Automático
Tipo Situação Sentido Ajustes
LT em Mesma Estrutura
Tripolar Ligado Miracema  
Gilbués
V  100 kV
   45°
f   0,2 Hz
Tempo morto: 5s
NE
Notas / Restrições: NI
Religamento Manual
Sentido Quantidade de 
Tentativas
Intervalo entre os 
religamentos (minutos)
Dispositivo de 
sincronismo Ajustes
--- NI NI. Em ambos os 
terminais NI
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Interligação Norte / Nordeste CD-CT.NNE.01 70 2.1.1. 28/04/2023
Referência: 7 / 15
3.4. LT 500 KV MIRANDA II / BACABEIRA C1 OU C2
Religamento Automático
Tipo Situação Sentido Ajustes
LT em Mesma Estrutura
Tripolar Ligado Miranda II  
Bacabeira
V  100 kV
   15°
f   0,2 Hz
Tempo morto: 5s
NE
Notas / Restrições: NI
Religamento Manual
Sentido Quantidade de 
Tentativas
Intervalo entre os 
religamentos (minutos)
Dispositivo de 
sincronismo Ajustes
--- 2
Primeira tentativa, em 
sentido normal pelo DJ do 
lado da barra, após 3 
(três) minutos do 
religamento automático.
A segunda tentativa, em 
sentido normal pelo DJ 
central, após 3 (três) 
minutos da primeira 
tentativa.
Em ambos os 
terminais NI
3.5. LT 500 KV PRESIDENTE DUTRA / BOA ESPERANÇA
Religamento Automático
Tipo Situação Sentido Ajustes
LT em Mesma Estrutura
Tripolar Ligado
Boa 
Esperança  
P. Dutra
V  50 kV
   30°
f   0,2 Hz
Tempo morto: 5s
NE
Notas / Restrições: O religamento automático da LT deve estar desligado na indisponibilidade de reator de 
linha.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Interligação Norte / Nordeste CD-CT.NNE.01 70 2.1.1. 28/04/2023
Referência: 8 / 15
Religamento Manual
Sentido Quantidade de 
Tentativas
Intervalo entre os 
religamentos (minutos)
Dispositivo de 
sincronismo Ajustes
--- 2
Primeira tentativa, em 
sentido normal pelo DJ do 
lado da barra, após 3 
(três) minutos do 
religamento automático.
A segunda tentativa, em 
sentido normal pelo DJ 
central, após 3 (três) 
minutos da primeira 
tentativa.
Em ambos os 
terminais NI
3.6. LT 500 KV PRESIDENTE DUTRA / TERESINA II C1 ( C8 ) OU C2 (C9)
Religamento Automático
Tipo Situação Sentido Ajustes
LT em Mesma Estrutura
Tripolar Ligado
Presidente 
Dutra  
Teresina II
V  50 kV
   30°
f   0,2 Hz
Tempo morto:
C1 (C8): 6 s.
C2 (C9): 5 s.
NE
Notas / Restrições: O primeiro terminal a religar (terminal líder) é o da SE Presidente Dutra, com a 
configuração barra viva, linha morta. O segundo terminal a religar (terminal seguidor) é o terminal da SE 
Teresina II, com a configuração linha viva, barra viva.
Religamento Manual
Sentido Quantidade de 
Tentativas
Intervalo entre os 
religamentos (minutos)
Dispositivo de 
sincronismo Ajustes
--- NI NI Em ambos os 
terminais NI
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Interligação Norte / Nordeste CD-CT.NNE.01 70 2.1.1. 28/04/2023
Referência: 9 / 15
3.7. LT 500 KV RIBEIRO GONÇALVES / SÃO JOÃO DO PIAUÍ C1 OU C2
Religamento Automático
Tipo Situação Sentido Ajustes
LT em Mesma Estrutura
Tripolar Ligado
Ribeiro 
Gonçalves  
São João do 
Piauí
V  100 kV
   40°
f   0,2 Hz
Tempo morto: 5 s
NE
Notas / Restrições: O primeiro terminal a religar (terminal líder) é o da SE Ribeiro Gonçalves, pelo disjuntor 
500 kV lado da barra. O segundo terminal a religar (terminal seguidor) é o terminal da SE São João do 
Piauí, pelo disjuntor 500 kV lado da barra, 2 segundos após o fechamento do terminal líder. Para o C2, 
após o religamento, o BCS da LT é inserido automaticamente. Os disjuntores centrais em Ribeiro 
Gonçalves e São João do Piauí, após religamento, serão fechados manualmente.
Religamento Manual
Sentido Quantidade de 
Tentativas
Intervalo entre os 
religamentos (minutos)
Dispositivo de 
sincronismo Ajustes
--- 2 3 Em ambos os 
terminais NI
3.8. LT 500 KV SÃO JOÃO DO PIAUÍ / BOA ESPERANÇA
Religamento Automático
Tipo Situação Sentido Ajustes
LT em Mesma Estrutura
Tripolar Ligado
São João do 
Piauí  Boa 
Esperança
V  50 kV
   25°
f   0,2 Hz
Tempo morto: 5 s
NE
Notas / Restrições: NI
Religamento Manual
Sentido Quantidade de 
Tentativas
Intervalo entre os 
religamentos (minutos)
Dispositivo de 
sincronismo Ajustes
--- NI NI Em ambos os 
terminais NI
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Interligação Norte / Nordeste CD-CT.NNE.01 70 2.1.1. 28/04/2023
Referência: 10 / 15
4. DADOS OPERACIONAIS DE BANCOS DE CAPACITORES SÉRIE
Orientações sobre os dados operacionais de bancos de capacitores:
Campo “Identificação Operacional”: identificação do banco de capacitores, conforme diagrama 
unifilar do agente.
Campo “Tensão Nominal (kV)”: indica a tensão nominal dos bancos de capacitores.
Campo “Potência Nominal (Mvar)”: indica a potência nominal dos bancos de capacitores.
Campo “Tempo para energização após desligamento”: é o tempo entre abertura dos disjuntores 
desconectando esse equipamento do sistema e o fechamento dos disjuntores reconectando o 
equipamento no sistema. Pode ser determinado pelas curvas tensão x corrente e temperatura x 
tempo.
Campo “Notas / Restrições”: campo opcional que indica informações relevantes a respeito dos 
bancos de capacitores. 
Caso os dados operacionais não sejam informados pelo Agente, os campos são preenchidos com “NI” – não 
informado.
Caso os dados operacionais não se apliquem, os campos são preenchidos com hifens “---”.
4.1. SE RIBEIRO GONÇALVES – BANCO DE CAPACITORES SÉRIE BCS1 500 KV / 462 MVAR DA LT 500 KV 
COLINAS / RIBEIRO GONÇALVES C1
Identificação 
Operacional Tensão Nominal (kV) Potência Nominal 
(Mvar)
Tempo para Energização 
após desligamento
BCS1 500 462 Ver itens a seguir
Notas / Restrições:
A função de supervisão do resistor de amortecimento bloqueia a reinserção após atuação da proteção do 
Banco de Capacitor Série conforme a seguir:
Banco de Capacitor Série descarregado 2 (duas) vezes dentro de 10 (dez) segundos, ocasiona 
bloqueio de 10 minutos;
Banco de Capacitor Série descarregado 3 (três) vezes dentro de um período de 8 (oito) horas, 
ocasiona bloqueio de 8 horas.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Interligação Norte / Nordeste CD-CT.NNE.01 70 2.1.1. 28/04/2023
Referência: 11 / 15
4.1.1. SE RIBEIRO GONÇALVES – BANCO DE CAPACITORES SÉRIE BCS1 500 KV / 462 MVAR DA LT 
500 KV COLINAS / RIBEIRO GONÇALVES C1 - CURVA CARACTERÍSTICA TENSÃO X CORRENTE
4.1.2. SE RIBEIRO GONÇALVES – BANCO DE CAPACITORES SÉRIE BCS1 500 KV / 462 MVAR DA LT 
500 KV COLINAS / RIBEIRO GONÇALVES C1 - CURVA RESFRIAMENTO DO MOV 
TEMPERATURA X TEMPO

Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Interligação Norte / Nordeste CD-CT.NNE.01 70 2.1.1. 28/04/2023
Referência: 12 / 15
4.2. SE RIBEIRO GONÇALVES – BANCO DE CAPACITORES SÉRIE BCS3 500 KV / 442 MVAR DA LT 500 KV 
COLINAS / RIBEIRO GONÇALVES C2
Identificação 
Operacional Tensão Nominal (kV) Potência Nominal 
(Mvar)
Tempo para Energização 
após desligamento
BCS3 500 442 NI
Notas / Restrições: NI
4.2.1. SE RIBEIRO GONÇALVES – BANCO DE CAPACITORES SÉRIE BCS3 500 KV / 442 MVAR DA LT 
500 KV COLINAS / RIBEIRO GONÇALVES C2 - CURVA CARACTERÍSTICA TENSÃO X CORRENTE
NI
4.2.2. SE RIBEIRO GONÇALVES – BANCO DE CAPACITORES SÉRIE BCS3 500 KV / 442 MVAR DA LT 
500 KV COLINAS / RIBEIRO GONÇALVES C2 - CURVA RESFRIAMENTO DO MOV 
TEMPERATURA X TEMPO
NI
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Interligação Norte / Nordeste CD-CT.NNE.01 70 2.1.1. 28/04/2023
Referência: 13 / 15
4.3. SE RIBEIRO GONÇALVES – BANCO DE CAPACITORES SÉRIE BCS2 500 KV / 425 MVAR DA LT 500 KV 
RIBEIRO GONÇALVES / SÃO JOÃO DO PIAUÍ C1
Identificação 
Operacional Tensão Nominal (kV) Potência Nominal 
(Mvar)
Tempo para Energização 
após desligamento
BCS2 500 425 Ver itens a seguir
Notas / Restrições:
A função de supervisão do resistor de amortecimento bloqueia a reinserção após atuação da proteção do 
Banco de Capacitor Série conforme a seguir:
Banco de Capacitor Série descarregado 2 (duas) vezes dentro de 10 (dez) segundos, ocasiona 
bloqueio de 10 minutos;
Banco de Capacitor Série descarregado 3 (três) vezes dentro de um período de 8 (oito) horas, 
ocasiona bloqueio de 8 horas.
4.3.1. SE RIBEIRO GONÇALVES – BANCO DE CAPACITORES SÉRIE BCS2 500 KV / 425 MVAR DA LT 
500 KV RIBEIRO GONÇALVES / SÃO JOÃO DO PIAUÍ C1 - CURVA CARACTERÍSTICA TENSÃO X 
CORRENTE

Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Interligação Norte / Nordeste CD-CT.NNE.01 70 2.1.1. 28/04/2023
Referência: 14 / 15
4.3.2. SE RIBEIRO GONÇALVES – BANCO DE CAPACITORES SÉRIE BCS2 500 KV / 425 MVAR DA LT 
500 KV RIBEIRO GONÇALVES / SÃO JOÃO DO PIAUÍ C1 - CURVA RESFRIAMENTO DO MOV 
TEMPERATURA X TEMPO
4.4. SE RIBEIRO GONÇALVES – BANCO DE CAPACITORES SÉRIE BCS4 500 KV / 425 MVAR DA LT 500 KV 
RIBEIRO GONÇALVES / SÃO JOÃO DO PIAUÍ C2
Identificação 
Operacional Tensão Nominal (kV) Potência Nominal 
(Mvar)
Tempo para Energização 
após desligamento
BCS4 500 425 Ver itens a seguir
Notas / Restrições:
A função de supervisão do resistor de amortecimento bloqueia a reinserção após atuação da proteção do 
Banco de Capacitor Série conforme a seguir:
Banco de Capacitor Série descarregado 2 (duas) vezes dentro de 10 (dez) segundos, ocasiona 
bloqueio de 10 minutos;
Banco de Capacitor Série descarregado 3 (três) vezes dentro de um período de 8 (oito) horas, 
ocasiona bloqueio de 8 horas.
4.4.1. SE RIBEIRO GONÇALVES – BANCO DE CAPACITORES SÉRIE BCS4 500 KV / 425 MVAR DA LT 
500 KV RIBEIRO GONÇALVES / SÃO JOÃO DO PIAUÍ C2 - CURVA CARACTERÍSTICA TENSÃO X 
CORRENTE
NI

Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Dados Operacionais de Equipamentos da 
Interligação Norte / Nordeste CD-CT.NNE.01 70 2.1.1. 28/04/2023
Referência: 15 / 15
4.4.2. SE RIBEIRO GONÇALVES – BANCO DE CAPACITORES SÉRIE BCS4 500 KV / 425 MVAR DA LT 
500 KV RIBEIRO GONÇALVES / SÃO JOÃO DO PIAUÍ C2 - CURVA RESFRIAMENTO DO MOV 
TEMPERATURA X TEMPO
NI
4.5. SE SÃO JOÃO DO PIAUÍ – BANCO DE CAPACITORES SÉRIE 05H2 500 KV / 484 MVAR DA LT 500 KV 
SÃO JOÃO DO PIAUÍ / BOA ESPERANÇA
Identificação 
Operacional Tensão Nominal (kV) Potência Nominal 
(Mvar)
Tempo para Energização 
após desligamento
05H2 500 484 10 minutos (*)
Notas / Restrições:
(**) No caso de atuação de proteção de alta temperatura do MOV, deve ser considerado um tempo 
mínimo entre 10 minutos e 3 horas para energização do Banco de Capacitores Série.
4.5.1. SE SÃO JOÃO DO PIAUÍ – BANCO DE CAPACITORES SÉRIE 05H2 500 KV / 484 MVAR DA LT 
500 KV SÃO JOÃO DO PIAUÍ / BOA ESPERANÇA - CURVA CARACTERÍSTICA TENSÃO X 
CORRENTE
NI.
4.5.2. SE SÃO JOÃO DO PIAUÍ – BANCO DE CAPACITORES SÉRIE 05H2 500 KV / 484 Mvar DA LT 500 
KV SÃO JOÃO DO PIAUÍ / BOA ESPERANÇA - CURVA RESFRIAMENTO DO MOV 
TEMPERATURA X TEMPO
NI.
