 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.11 
 
Cadastro de Informações Operacionais 
Cadastro de Informações Operacionais de Faixas para Controle de Tensão da Área 345/230 
kV São Paulo 
 
Código Revisão Item Vigência 
CD-CT.SE.3SP.03 22 2.6.2 29/05/2024 
 
 
MOTIVO DA REVISÃO 
-  Exclusão das restrições nas faixas de tensão das SE Itatiba 138 kV, Campinas (Furnas) 138 kV e UHE Henry 
Borden Subterrânea 230 kV; 
- Alteração da faixa de tensão da SE Henry Borden 88 kV; 
- Inclusão das restrições nas faixas de tensão das SE Aparecida 88 kV e Santa Cabeça 88 kV. 
 
LISTA DE DISTRIBUIÇÃO  
AES-TIETE ATE BANDEIRANTE CEMIG CESP 
COSR-SE CPFL CTEEP ELETRONUCLEAR EMAE 
ENEL DIST SP FURNAS FURNAS.CTRS.O GV DO BRASIL LIGHT 
NEOENERGIA ELEKTRO PETROBRAS RIO PARANAPANEMA ENERGIA   
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Referência técnica Código Revisão Item Vigência 
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 345/230 kV São Paulo CD-CT.SE.3SP.03 22 2.6.2 29/05/2024 
 
Referência:  2 / 5 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONCEITOS ................................ ................................ ................................ ................................ ... 3 
3. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO ................................ .......................  4 
4.1. Faixas de tensão da rede de operação .......................................................................................... 4 
4.2. Demais Faixas de tensão da rede de operação ............................................................................. 5 
 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Referência técnica Código Revisão Item Vigência 
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 345/230 kV São Paulo CD-CT.SE.3SP.03 22 2.6.2 29/05/2024 
 
Referência:  3 / 5 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
1. OBJETIVO 
Apresentar as faixas de controle de  tensão dos barramentos da Rede de Operação e dos secundários das 
transformações da Rede de Operação, cujos barramentos não pertencem à Rede de Operação, a serem 
controlados pelos operadores dos Centros de Operação do ONS e pela Operação dos agentes envolvidos. 
2. CONCEITOS 
2.1. Os períodos de carga s mínima, leve, média e pesada , definidos para o controle de tensão , estão 
estabelecidos em função das faixas horárias, conforme tabela abaixo, que tem como referência o horário 
de Brasília. 
Período Segunda Terça a Sábado Domingos e Feriados 
00:00 às 05:00 Mínima Leve Leve 
05:00 às 07:00 Mínima Leve Mínima 
07:00 às 10:00 Média Média Mínima 
10:00 às 17:00 Média Média Leve 
17:00 às 22:00 Pesada Pesada Média 
22:00 às 24:00 Média Média Leve 
 
2.2. As faixas de controle de tensão são estabelecidas em função de uma grandeza de referência, definida 
conforme a seguir. 
Nome da Grandeza Sigla Definição 
Carga São Paulo sem a 
MMGD 
Carga 
São 
Paulo 
sMMGD 
Carga São Paulo sem a MMGD,  conforme IO-ON.SE.3RG. 
 
3. CONSIDERAÇÕES GERAIS 
3.1. Este Cadastro de Informações Operacionais apresenta os valores das faixas para controle de tensão de 
barramentos da Rede de Operação e  de secundários das transformações da Rede de Operação cujas 
barras não pertencem à Rede de Operação. 
3.2. Os Centros de Operação do ONS, nas ações de coordenação, supervisão e controle devem observar os 
limites operacionais constantes neste Cadastro de Informações Operacionais. 
3.3. A operação dos agentes, em suas ações de comando e execução, deve observar os limites operacionais 
constantes neste Cadastro de Informações Operacionais. 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Referência técnica Código Revisão Item Vigência 
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 345/230 kV São Paulo CD-CT.SE.3SP.03 22 2.6.2 29/05/2024 
 
Referência:  4 / 5 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO 
4.1. FAIXAS DE TENSÃO DA REDE DE OPERAÇÃO 
Subestação / Usina Faixas de Tensão  (kV) em função do(a) Carga São Paulo 
sMMGD 
Nome Tensão 
(kV) 
Requisito > 
15500 MW 
(Pesada) 
Requisito > 
15500 MW 
(Média) 
Requisito < 
15500 MW 
(Leve) 
Requisito < 
15500 MW 
(Mínima) 
SE Ibiúna 345 354 - 362 354 - 362 335 - 362 335 - 362 
SE Tijuco Preto 345 354 - 362 354 - 362 335 - 362 335 - 362 
SE Mogi Das Cruzes 230 230 - 241 230 - 241 226 - 241 226 - 241 
SE Piratininga 230 219 - 238 219 - 238 226 - 236 226 - 236 
SE São José dos Campos 230 226 - 241 226 - 241 226 - 241 226 - 241 
SE Henry Borden 88 88 - 92 88 - 92 85 - 92 85 - 92 
SE Piratininga 88 87 - 89 87 - 89 87 - 89 87 - 89 
 
Subestação / Usina Faixas de Tensão  (kV) 
Nome Tensão 
(kV) Pesada Média Leve Mínima 
SE Aparecida * 88 86 - 92 86 - 92 84 - 92 84 - 92 
SE Santa Cabeça * 88 86 - 92 86 - 92 84 - 92 84 - 92 
* - Faixas de tensão do secundário do transformador 
 
 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Referência técnica Código Revisão Item Vigência 
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Área 345/230 kV São Paulo CD-CT.SE.3SP.03 22 2.6.2 29/05/2024 
 
Referência:  5 / 5 
 
Alterado pela(s) MOP(s): 
MOP/ONS 462-S/2024;  
4.2. DEMAIS FAIXAS DE TENSÃO DA REDE DE OPERAÇÃO 
Os barramentos da Rede de Operação e os secundários das transformações da Rede de Operação , que não 
possuem faixas de tensão definidas nos itens anteriores , devem operar com tensões nas seguintes faixas 
operativas, conforme estabelecido nos Procedimentos de Rede e reproduzido pela tabela abaixo: 
Tensão Nominal (kV) Faixa de Tensão (kV) 
765 690 a 800 
525 500 a 550  
500 500 a 550  
440 418 a 460  
345 328 a 362  
230 218 a 242  
138 131 a 145  
88 83,6 a 92,4 
69 65,6 a 72,4 
34,5 32,8 a 36,2 
23 21,8 a 24,2 
13,8 13,1 a 14,5 
 
