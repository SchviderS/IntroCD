Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação
Procedimentos Sistêmicos para a Operação da UHE Itá
Código Revisão Item Vigência
IO-OI.S.UHIT 21 3.7.5.1. 03/06/2024
.
MOTIVO DA REVISÃO
- Alteração das condições de autonomia do Agente para energização, desvinculando os procedimentos do 
Subitem 6.2.2 dos procedimentos de recomposição fluente da Instalação constantes no Subitem 5.2.2, 
alterando a energização da: 
•LT 525 kV kV Itá / Usina Hidrelétrica Itá C1 ou C2.
- Adequação à RT-OI.BR revisão 36, com destaque à inclusão dos subitens 6.1.8 e 6.1.9. 
- Adequação do Subitem 5.4. para desligamentos parciais.
LISTA DE DISTRIBUIÇÃO
CNOS COSR-S Engie
(COG)
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE Itá IO-OI.S.UHIT 21 3.7.5.1. 03/06/2024
Referência: 2 / 10
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. CONSIDERAÇÕES GERAIS .................................................................................................................3
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO..............................................................4
3.1. Barramento de 525 kV ..................................................................................................................4
3.2. Alteração da Configuração dos Barramentos................................................................................4
3.3. Unidades Geradoras......................................................................................................................4
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL......................................................4
4.1. Procedimentos Gerais ...................................................................................................................4
4.2. Procedimentos Específicos............................................................................................................5
4.2.1. Operação das Unidades Geradoras como Compensadores Síncronos........................5
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................................................5
5.1. Procedimentos Gerais ...................................................................................................................5
5.2. Procedimentos para Recomposição Fluente.................................................................................6
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................6
5.2.2. Recomposição Fluente da Instalação ..........................................................................6
5.3. Procedimentos após Desligamento Total da Instalação................................................................7
5.3.1. Preparação da Instalação após Desligamento Total....................................................7
5.3.2. Recomposição após Desligamento Total da Instalação...............................................7
5.4. Procedimentos após Desligamento Parcial da Instalação .............................................................7
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ..............7
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................7
6. MANOBRAS DE UNIDADES GERADORAS E DE LINHAS DE TRANSMISSÃO .........................................8
6.1. Procedimentos Gerais ...................................................................................................................8
6.2. Procedimentos Específicos............................................................................................................9
6.2.1. Desligamento de Unidades Geradoras e Desenergização de Linhas de Transmissão .9
6.2.2. Sincronismo de Unidades Geradoras e Energização de Linhas de Transmissão..........9
7. NOTAS IMPORTANTES ...................................................................................................................10
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE Itá IO-OI.S.UHIT 21 3.7.5.1. 03/06/2024
Referência: 3 / 10
1. OBJETIVO
Estabelecer os procedimentos para a operação da UHE Itá, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede.
2. CONSIDERAÇÕES GERAIS
2.1. Os procedimentos contidos nesta Instrução de Operação são somente aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da Instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS.
2.2. A comunicação operacional entre o COSR-S e a Instalação, de propriedade da Engie, é realizada pela 
Engie, agente responsável pela operação da Instalação, por intermédio do COG Engie.
2.3. As unidades geradoras, equipamentos e linhas de transmissão desta Instalação fazem parte da Área 
525 kV da Região Sul.
2.4. O COSR-S controla e supervisiona o despacho de geração desta Usina.
2.5. Esta Usina:
•é despachada centralizadamente;
•está conectada na Rede de Operação;
•participa do Controle Automático da Geração – CAG;
•é de autorrestabelecimento integral;
•é fonte para início do processo de recomposição fluente da Área Itá;
•é responsável pelo controle de frequência e tensão da Área Itá na fase de recomposição fluente, 
sendo o controle da frequência efetuada pela operação da Usina até orientação diferente do 
COSR-S.
2.6. Os dados operacionais desta Usina estão descritos no Cadastro de Informações Operacionais da 
respectiva Área Elétrica.
2.7. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos:
2.7.1. A definição da quantidade de tentativas de religamento manual de linha de transmissão ou de 
equipamento, bem como o intervalo entre elas, é de responsabilidade do Agente e devem estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica.
2.7.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar autorização ao COSR-S 
autorização para religamento. Nessa oportunidade, o Agente também pode solicitar a alteração no 
sentido normal para envio de tensão, caso não tenha autonomia para tal.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE Itá IO-OI.S.UHIT 21 3.7.5.1. 03/06/2024
Referência: 4 / 10
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR-S levará em consideração as condições operativas do 
sistema.
2.8. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica.
2.9. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica.
2.10. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR-S, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação, quando o Agente tiver autonomia para energizar a 
linha de transmissão ou o equipamento.
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO
3.1. BARRAMENTO DE 525 KV
A configuração do barramento de 525 kV é do tipo Barra Simples, seccionada em (Barra 01 525 kV) e 
(Barra 02 525 kV), interligadas por seccionadora. Na operação normal desse barramento, todos os 
disjuntores e seccionadoras devem estar fechados.
3.2. ALTERAÇÃO DA CONFIGURAÇÃO DOS BARRAMENTOS
A mudança de configuração do barramento de 525 kV desta Instalação é executada com controle do 
COSR-S.
A mudança de configuração dos demais barramentos é executada sob responsabilidade dos Agentes 
Operadores da Instalação.
3.3. UNIDADES GERADORAS
As unidades geradoras GH 1, GH 2, GH 3, GH 4 e GH 5 16 kV estão conectadas ao barramento de 525 kV, 
por meio de seus respectivos transformadores elevadores, com os disjuntores de 525 kV fechados e as 
seccionadoras de 525 kV fechadas.
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL
4.1. PROCEDIMENTOS GERAIS
4.1.1. A Usina deve manter os valores de geração de acordo com os valores programados, constantes no 
Programa Diário de Operação – PDO. Para atendimento dos valores programados constantes no PDO, 
não é necessária a autorização prévia do COSR-S.
Qualquer alteração no valor de geração da Usina em relação ao valor de geração ao constante no 
PDO somente pode ser executada após autorização do COSR-S.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE Itá IO-OI.S.UHIT 21 3.7.5.1. 03/06/2024
Referência: 5 / 10
As reprogramações de geração quando de necessidades sistêmicas serão executadas pela Usina 
quando solicitadas pelo COSR-S.
Após reprogramação de geração solicitada pelo COSR-S, o Agente somente poderá alterar a geração 
da Usina com autorização do ONS, inclusive para adoção de valores contidos no PDO.
4.1.2. Os desvios de geração da Usina em relação aos valores previstos no PDO ou em relação às 
reprogramações devem ser controlados observando-se os valores máximos permitidos explicitados 
na Instrução de Operação IO-CG.BR.01 – Controle da Geração em Condição Normal.
4.1.3. Quando não existir ou não estiver disponível a supervisão da Usina para o ONS, essa deve seguir as 
orientações para envio de dados conforme Rotina Operacional RO-AO.BR.08 – Apuração de Dados de 
Despacho de Geração e de Intercâmbios.
4.1.4. A Usina deve registrar e informar imediatamente os seguintes dados ao COSR-S:
•movimentação de unidades geradoras hidráulicas (mudança de estado operativo / 
disponibilidade); 
•restrições e ocorrências na Usina ou na conexão elétrica que afetem a disponibilidade de geração, 
com o respectivo valor da restrição, contendo o horário de início e término e a descrição do 
evento;
•demais informações sobre a operação da Instalação, solicitadas pelo ONS.
4.1.5. O controle de tensão, por meio da geração ou absorção de potência reativa das unidades geradoras 
da Usina, é controlado pelo COSR-S, com comando e execução pela operação do Agente.
4.2. PROCEDIMENTOS ESPECÍFICOS
4.2.1. OPERAÇÃO DAS UNIDADES GERADORAS COMO COMPENSADORES SÍNCRONOS
A conversão da modalidade de operação de gerador para compensador síncrono, bem como a 
reversão de compensador síncrono para gerador, deve ser executada sob controle do COSR-S.
Devem ser observados os procedimentos relacionados a prestação de Serviços Ancilares de Suporte 
de Reativos, conforme Instrução de Operação IO-CG.BR.01 – Controle da Geração em Condição 
Normal.
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO
5.1. PROCEDIMENTOS GERAIS
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir:
•Desligamento total da Instalação: caracterizado por meio da verificação de ausência de tensão 
em todos os terminais de suas conexões ou ausência de fluxo de potência ativa nessas conexões.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE Itá IO-OI.S.UHIT 21 3.7.5.1. 03/06/2024
Referência: 6 / 10
•Desligamento parcial da Instalação: qualquer outra configuração que não se enquadre como 
desligamento total.
5.1.2. Quando de desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao COSR-S 
as seguintes informações:
•horário da ocorrência;
•configuração da Instalação após a ocorrência;
•configuração da Instalação após ações realizadas com autonomia pela operação dessa.
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2, sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia o Agente Operador da instalação na 
recomposição, deve ser utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4. 
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE
No caso de desligamento total, o Agente Operador deve configurar os disjuntores das seguintes linhas 
de transmissão e unidades geradoras, conforme condição apresentada a seguir.
Abrir ou manter abertos os disjuntores:
•das linhas de transmissão:
LT 525kV Itá / Usina Hidrelétrica Itá C1;
LT 525kV Itá / Usina Hidrelétrica Itá C2.
•das unidades geradoras:
GH 1 16 kV (de 525 kV);
GH 2 16 kV (de 525 kV);
GH 3 16 kV (de 525 kV);
GH 4 16 kV (de 525 kV);
GH 5 16 kV (de 525 kV).
Retirar ou manter fora de operação o Controle Automático de Geração.
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO
A Instalação parte da recomposição da Área Itá. o Agente Operador deve adotar procedimentos a 
seguir para a recomposição fluente:
Passo Executor Procedimentos Condições ou Limites Associados
1 Engie 
(COG)
Partir uma das unidades geradoras, 
energizar o barramento de 525 kV e 
controlar a frequência em 60 Hz.
Manter VUHIT-16 = 15,2 kV. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE Itá IO-OI.S.UHIT 21 3.7.5.1. 03/06/2024
Referência: 7 / 10
Passo Executor Procedimentos Condições ou Limites Associados
1.1
Engie 
(COG)
Partir a segunda unidade geradora, 
sincronizá-la e controlar a frequência em 
60 Hz.
Manter VUHIT-16 = 15,2 kV.
1.2
Engie 
(COG)
Partir a terceira unidade geradora, 
sincronizá-la e controlar a frequência em 
60 Hz.
Manter VUHIT-16 = 15,2 kV.
1.3
Engie 
(COG)
Energizar um dos circuitos da LT 525 kV 
Itá / Usina Hidrelétrica Itá, enviando 
tensão para a SE Itá.
Pelo menos 3 (três) unidades 
geradoras sincronizadas na UHE Itá.
1.4
Engie 
(COG)
Partir a quarta unidade geradora, 
sincronizá-la e controlar a frequência em 
60 Hz.
Manter VUHIT-16 = 15,2 kV.
1.5
Engie 
(COG)
Energizar o segundo circuito da LT 525 kV 
Itá / Usina Hidrelétrica Itá, enviando 
tensão para a SE Itá.
Após carga ativa superior a 600 MW 
no circuito da LT 525 kV Itá / Usina 
Hidrelétrica Itá que foi energizado.
1.6
Engie 
(COG)
Partir a quinta unidade geradora, caso 
essa estivesse operando antes do 
desligamento, sincronizá-la e controlar a 
frequência em 60 Hz.
Manter VUHIT-16 = 15,2 kV.
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1.
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2, enquanto não 
houver intervenção do COSR-S no processo de recomposição ou interrupção da autonomia deste na 
recomposição. 
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR-S. 
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL
Para os desligamentos parciais da Instalação, não há necessidade de preparação da Instalação.
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
O Agente Operador deve recompor a Instalação conforme Subitem 6.2.2, sem necessidade de 
autorização do ONS.  
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE Itá IO-OI.S.UHIT 21 3.7.5.1. 03/06/2024
Referência: 8 / 10
o Agente Operador deve informar ao COSR-S, para que a recomposição da Instalação seja executada 
com controle do COSR-S.
6. MANOBRAS DE UNIDADES GERADORAS E DE LINHAS DE TRANSMISSÃO
6.1. PROCEDIMENTOS GERAIS
6.1.1. Os procedimentos para desligamento programado ou de urgência de unidades geradoras, e para 
desenergização programada ou de urgência, de linhas de transmissão ou de equipamentos, só 
podem ser efetuados com controle do COSR-S.
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, ou sincronismo 
de unidades geradoras, após desligamentos programados, de urgência ou de emergência, só 
podem ser efetuados com controle do COSR-S.
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, ou para sincronismo de unidades geradoras, após desligamento automático sem 
atuação de proteção que impeça o retorno do equipamento, só podem ser executados com 
autonomia pelo Agente Operador da Instalação quando estiverem explicitados e estiverem 
atendidas as condições do Subitem 6.2.2. desta Instrução de Operação.
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR-S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica.
6.1.4. Antes do fechamento de qualquer disjuntor, o Agente Operador da Instalação deve verificar se 
existe tensão de retorno e se a condição de fechamento será em anel.
O fechamento em anel só pode ser executado com autonomia pelo Agente Operador da Instalação 
quando estiver especificado nesta Instrução de Operação e estiverem atendidas as condições do 
Subitem 6.2.2.
O fechamento de paralelo só pode ser efetuado com controle do COSR-S.
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento:
6.1.5.1. A energização em sentido normal ou, quando permitida, em sentido inverso, pode ser feita com 
autonomia pelo Agente Operador, conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica. 
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de 
intervenções, são de responsabilidade do Agente. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE Itá IO-OI.S.UHIT 21 3.7.5.1. 03/06/2024
Referência: 9 / 10
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador 
é energizado, o Agente Operador da Instalação deve fechá-lo em anel desde que tenha autonomia 
para tal, adotando as condições para o fechamento constantes no Subitem 6.2.2.
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S. 
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S.
6.1.10. A partida de unidades geradoras deve seguir critérios próprios do Agente. O sincronismo à Rede de 
Operação só pode ser executado com autonomia pela operação da Usina quando estiver explicitado 
e estiverem atendidas as condições do Subitem 6.2.2. A tomada de carga deve ser realizada com 
controle do COSR-S.
6.2. PROCEDIMENTOS ESPECÍFICOS
6.2.1. DESLIGAMENTO DE UNIDADES GERADORAS E DESENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO
O desligamento de unidades geradoras e a desenergização de linhas de transmissão, pertencentes à 
Rede de Operação, é sempre controlado pelo COSR-S.
6.2.2. SINCRONISMO DE UNIDADES GERADORAS E ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S.
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de unidades geradoras ou de linhas de transmissão.
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais unidades geradoras e linhas de transmissão em operação, conforme 
explicitado nas condições de energização para a manobra.
Para os demais desligamentos parciais, proceder conforme Subitem 5.4.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE Itá IO-OI.S.UHIT 21 3.7.5.1. 03/06/2024
Referência: 10 / 10
Linha de Transmissão / 
Unidade Geradora Procedimentos Condições ou Limites Associados
Sentido Normal: UHE Itá envia tensão para a SE Itá 
Ligar a LT 525 kV Itá / Usina Hidrelétrica 
Itá C1 (ou C2).
Como primeiro ou segundo circuito: 
•V UHIT-525 ≤ 550 kV.
Antes de energizar cada LT, verificar 
fluxo de potência ativa no circuito da LT 
525 kV Itá / Usina Hidrelétrica Itá que já 
foi energizado.
Sentido Inverso: UHE Itá recebe tensão da SE Itá 
LT 525 kV Itá / Usina 
Hidrelétrica Itá
C1 ou C2
Ligar, energizando o barramento de 525 
kV, a LT 525 kV Itá / Usina Hidrelétrica 
Itá C1 (ou C2).
O barramento de 525 kV deve estar 
desenergizado.
Partir e sincronizar a unidade geradora. Conforme procedimentos internos do 
Agente.
Gerar o mínimo necessário para manter 
a unidade geradora sincronizada.
Unidade Geradora
GH 1, GH 2, GH 3, 
GH 4 ou GH 5 16 kV
Elevar a geração da unidade geradora. Após autorização do COSR-S.
7. NOTAS IMPORTANTES
7.1. Devido as LT 525 kV Itá / Usina Hidrelétrica Itá C1 e C2 não possuírem transformadores de potencial 
e nem dispositivo de sincronização na UHE Itá, o fechamento do disjuntor da LT 525 kV Ita / Usina 
Hidrelétrica Itá C1 ou C2, nesta usina hidrelétrica, só será permitido na condição de Barra Morta / 
Linha Viva ou Barra Viva / Linha Morta.
7.2. A informação da tensão na LT 525 kV Itá / Usina Hidrelétrica Itá C1 ou C2 na UHE Itá é oriunda do 
transformador de potencial da referida linha de transmissão na SE Itá.
7.3. Caso uma das seções do barramento de 525 kV (barra 1 ou 2) da UHE Itá ficar sem tensão por seis 
segundos, será aberto o disjuntor do circuito da LT 525 kV Itá / Usina Hidrelétrica Itá conectado nessa 
seção.
