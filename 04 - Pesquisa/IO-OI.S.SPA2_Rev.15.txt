 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Santa Vitória do Palmar 2 
 
 
Código Revisão Item Vigência 
IO-OI.S.SPA2 15 3.7.5.1. 03/06/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Alteração das condições de autonomia do Agente para fechamento em anel, no Subitem 6.2.2, das seguintes 
linhas de transmissão e equipamentos: 
• LT 525 kV Marmeleiro 2 / Santa Vitória do Palmar 2 C1. 
- Inclusão dos Subitens 5.4.1.2. e 5.4.2.2. e complementação do Subitem 5.4.2.3. para casos de desligamentos 
parciais. 
- Adequação à RT-OI.BR revisão 36, com destaque à inclusão dos subitens 6.1.8 e 6.1.9.  
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S CGT Eletrosul Cymi CEEE-T CEEE-D 
Iqony      
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Santa 
Vitória do Palmar 2 IO-OI.S.SPA2 15 3.7.5.1. 03/06/2024 
 
Referência:  2 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  4 
3.1. Barramento de 525 kV ................................................................................................................... 4 
3.2. Alteração da Configuração dos Barramentos ................................................................................ 4 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 4 
4.1. Procedimentos Gerais ................................................................................................................... 4 
4.2. Procedimentos Específicos ............................................................................................................ 5 
4.2.1. Operação dos Comutadores de Tape Sob Carga (LTC) ................................................ 5 
4.2.2. Operação dos Reatores ............................................................................................... 5 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  5 
5.1. Procedimentos Gerais ................................................................................................................... 5 
5.2. Procedimentos para Recomposição com Autonomia ................................................................... 5 
5.2.1. Preparação da Instalação para a Recomposição com Autonomia .............................. 5 
5.2.2. Recomposição com Autonomia Após Desligamento Total da Instalação ................... 6 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 7 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 7 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 7 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 7 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 7 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 7 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 8 
6.1. Procedimentos Gerais ................................................................................................................... 8 
6.2. Procedimentos Específicos ............................................................................................................ 9 
6.2.1. Desenergização de Linhas de Transmissão e de Equipamentos ................................. 9 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ....................................... 9 
7. NOTAS IMPORTANTES ................................ ................................ ................................ ................ 11 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Santa 
Vitória do Palmar 2 IO-OI.S.SPA2 15 3.7.5.1. 03/06/2024 
 
Referência:  3 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE Santa Vitória do Palmar  2, definidos pelo ONS, 
responsável pela coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos 
Procedimentos de Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são somente aqueles de interesse sistêmico, 
realizados com autonomia pelos Agentes Operadores da Instalação, devendo fazer parte do manual 
de operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade 
das ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR-S e a Instalação, no que se refere aos equipamentos de 
manobra da Instalação, é realizada conforme segue: 
Linha de Transmissão ou 
Equipamento  
Agente de 
Operação Agente Operador Centro de Operação do 
Agente Operador  
Barramento de 525 kV CGT Eletrosul CGT Eletrosul COT Sul 
LT 525 kV Marmeleiro 2 / Santa 
Vitória do Palmar 2 C1 e Reator RE 1 
525 kV (manobrável de linha) 
 CGT Eletrosul CGT Eletrosul COT Sul 
LT 525 kV Marmeleiro 2 / Santa 
Vitória do Palmar 2 C2 e Reator RE 2 
525 kV (manobrável de linha) 
Chimarrão Cymi COT Cymi 
Transformador TF 1 525/138/13,8 kV CGT Eletrosul CGT Eletrosul COT Sul 
Transformador TF 2 525/138/13,8 kV 
CGT Eletrosul 
(transformador e 
módulo) 
CGT Eletrosul COT Sul 
Chimarrão 
(interligação de 
módulos) 
Cymi COT Cymi 
Transformador TF 3 525/138/13,8 kV 
(*) 
Santa Vitória do 
Palmar Holding Iqony COS Iqony 
Transformador TF 4 525/138/13,8 kV 
(*) 
Hermenegildo I 
S.A., 
Hermenegildo II 
S.A. e 
Hermenegildo III 
S.A. 
Iqony COS Iqony 
(*) Somente os equipamentos de manobra de 525 kV do Transformador TF 3 e do Transformador TF 4 
525/230/13,8 kV pertencem à Rede de Operação, conforme a Rotina Operacional RO-RD.BR.01. 
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte Área 525 kV da Região Sul. 
2.4. No que se refere ao religamento manual de linhas de transmissão ou equipamentos: 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Santa 
Vitória do Palmar 2 IO-OI.S.SPA2 15 3.7.5.1. 03/06/2024 
 
Referência:  4 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
2.4.1. A definição da quantidade  de tentativas de religamento manual de linha de transmissão  ou de 
equipamento, bem como o intervalo entre elas, é de responsabilidade do Agente e deve m estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica. 
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR -S autorização 
para religamento. Nessa oportunidade, o Agente também pode solicitar alteração no sentido normal 
para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em consideração as condições operativas do 
sistema.  
2.5. Quando caracterizado impedimento de linha de transmissão  ou de equipamento , deve m ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação , quando o Agente  tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 525 kV 
A configuração do barramento de 525 kV (Barra A e Barra B) é do tipo Disjuntor e Meio. Na operação 
normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados. 
3.2. ALTERAÇÃO DA CONFIGURAÇÃO DOS BARRAMENTOS 
A mudança de configuração do barramento de 525 kV desta Instalação  é executada com controle do 
COSR-S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade dos Agentes 
Operadores da Instalação. 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 525 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S.  
As faixas de controle de tensão para esse barramento estão estabelecidas no Cadastro de Informações 
Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob a responsabilidade da Instalação. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Santa 
Vitória do Palmar 2 IO-OI.S.SPA2 15 3.7.5.1. 03/06/2024 
 
Referência:  5 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
4.2. PROCEDIMENTOS ESPECÍFICOS 
4.2.1. OPERAÇÃO DOS COMUTADORES DE TAPE SOB CARGA (LTC) 
Os LTCs do Transformador TF 1 e TF 2 525/230/13,8 kV operam em modo manual. 
A movimentação dos comutadores é realizada com autonomia pela operação do Agente CGT Eletrosul. 
4.2.2. OPERAÇÃO DOS REATORES 
A manobra do Reator RE 1 525 kV – 50 Mvar ou do Reator  RE 2 525 kV – 50 Mvar é executada com 
controle do COSR-S. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação : caracterizado quando não há tensão em todos os terminais de 
suas linhas de transmissão. 
• Desligamento parcial da Instalação : qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, os Agentes Operadores  da Instalação devem fornecer ao 
COSR-S as seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2, sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificand o a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia dos Agentes Operadores  da Instalação na 
recomposição, deve ser utilizado o Subitem 5.3.  
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO COM AUTONOMIA 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO COM AUTONOMIA 
No caso de desligamento total, os Agentes Operadores  devem configurar os disjuntores dos 
seguintes equipamentos e linhas de transmissão, conforme condição apresentada a seguir. 
Abrir ou manter abertos os disjuntores: 
• das linhas de transmissão: 
LT 525 kV Marmeleiro 2 / Santa Vitória do Palmar 2 C1; 
LT 525 kV Marmeleiro 2 / Santa Vitória do Palmar C2; 
de todas as linhas de transmissão de 138 kV. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Santa 
Vitória do Palmar 2 IO-OI.S.SPA2 15 3.7.5.1. 03/06/2024 
 
Referência:  6 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
• dos transformadores: 
TF 1 525/138/13,8 kV (lados de 525 kV e de 138 kV); 
TF 2 525/138/13,8 kV (lados de 525 kV e de 138 kV); 
demais transformadores 525/138/13,8 kV. 
• das interligações de módulos: 
LT 525 kV Marmeleiro 2 / Santa Vitória do Palmar 2 C1 – Transformador TF 1 525/138/13,8 kV; 
LT 525 kV Marmeleiro 2 / Santa Vitória do Palmar 2 C2 – Transformador TF 2 525/138/13,8 kV ; 
Barra A – Transformador TF 3 525/138/13,8 kV; 
Barra A – Transformador TF 4 525/138/13,8 kV. 
• do reator: 
RE 1 525 kV; 
RE 2 525 kV. 
Desligar ou manter desligado o modo de comutação automática dos comutadores sob carga da 
transformação 525/138/13,8 kV da SE Santa Vitória do Palmar 2. 
Cabe à CGT Eletrosul informar ao COSR-S quando a configuração de preparação da Instalação não 
estiver atendida para o início da recomposição, independentemente de o equipamento ser próprio 
ou de outros Agentes. Nesse caso, o COSR-S fará contato com os agentes envolvidos para identificar 
o motivo do não-atendimento e, após confirmação da CGT Eletrosul de que os barramentos estão 
com a configuração atendida, o COSR -S coordenar á os procedimentos para recomposição , caso 
necessário, em função da configuração desta Instalação. 
5.2.2. RECOMPOSIÇÃO COM AUTONOMIA APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
Os Agentes Operadores da Instalação devem  realizar com autonomia os seguintes procedimentos 
para a recomposição: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 
CGT Eletrosul 
(COT Sul) 
Receber tensão da SE Marmeleiro 2, pela 
LT 525 kV Marmeleiro 2 / Santa Vitória do 
Palmar 2  C1, e energizar a Barra A de 
525 kV. 
 
1.1 
CGT Eletrosul 
(COT Sul) 
Energizar o  primeiro Transformador TF 1 
525/138/13,8 kV (ou TF 2) e a Barra B de 
525 kV da SE Santa Vitória do Palmar 2, 
pelo lado de 525  kV e, após, energizar o 
barramento de 138 kV. 
VSPA2-525 ≤ 550 kV. 
Restabelecer carga somente após 
autorização do COSR-S. 
1.2 Cymi 
Receber tensão da SE Marmeleiro 2, pela 
LT 525 kV Marmeleiro 2 / Santa Vitória do 
Palmar 2 C2, e ligar em anel. 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Santa 
Vitória do Palmar 2 IO-OI.S.SPA2 15 3.7.5.1. 03/06/2024 
 
Referência:  7 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Passo Executor Procedimentos Condições ou Limites Associados 
1.3 
CGT Eletrosul 
(COT Sul) 
Energizar o segundo Transformador TF 2 
525/138/13,8 kV (ou TF 1) e a Barra B de 
525 kV da SE Santa Vitória do Palmar 2, 
pelo lado de 525  kV e, após, energizar o 
barramento de 138 kV. 
VSPA2-525 ≤ 550 kV, e 
Após fluxo de potência ativa no 
transformador TF 1 (ou TF 2) 
525/138/13,8 kV que já foi 
energizado. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
Os Agentes Operadores da Instalação devem realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
Os Agentes Operadores da Instalação devem realizar os procedimentos do Subitem 5.2.2, enquanto 
não houver intervenção do COSR -S no processo de recomposição ou interrupção da autonomia 
destes na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, 
conforme procedimentos contidos nas respectivas Instruções de Preparação para Manobras.  
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, Os 
Agentes Operadores devem preparar a Instalação conforme Subitem 5.2.1, sem necessidade de 
autorização do ONS. 
5.4.1.2. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão nas barras de 525 kV, e 
• ausência de fluxo de potência ativa nas linhas de transmissão de 525 kV, os Agentes Operadores 
devem preparar o setor de 525 kV d a Instalação (disjuntores das linhas de transmissão e 
equipamentos) conforme Subitem 5.2.1., sem necessidade de autorização do ONS.  
5.4.1.3. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 5.2 .2, sem necessidade de 
autorização do ONS. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Santa 
Vitória do Palmar 2 IO-OI.S.SPA2 15 3.7.5.1. 03/06/2024 
 
Referência:  8 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., os Agentes 
Operadores devem informar ao COSR-S, para que a recomposição da Instalação seja executada com 
controle do COSR-S. 
5.4.2.3. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.3., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 6.2.2., sem necessidade de 
autorização do ONS. 
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
os Agentes Operadores devem informar ao COSR -S, para que a recomposição da Instalação seja 
executada com controle do COSR-S.     
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, após desligamento 
programado, de urgência ou de emergência, só podem ser efetuados com controle do COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelos Agentes Operadores da  Instalação 
quando estiverem explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução 
de Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir autonomia, 
a energização deve ser executada com controle do COSR -S, conforme Instrução de Operação de 
Preparação para Manobras da respectiva área elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, os Agentes Operadores da Instalação devem verificar se 
existe tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pelos Agentes Operadores da Instalação 
quando estiver especificado nesta Instrução de O peração e estiverem atendidas as condições do 
Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou , quando permitida, em sentido  inverso, pode ser feita com 
autonomia pelos Agentes Operadores , conforme procedimentos para manobras que estão 
definidos nesta Instrução de Operação, Subitem 6.2.2 . Os procedimentos para energização 
controlados pelo COSR-S estão definidos na Instrução de Operação de Preparação para Manobras 
da respectiva área elétrica.  
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Santa 
Vitória do Palmar 2 IO-OI.S.SPA2 15 3.7.5.1. 03/06/2024 
 
Referência:  9 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, os Agentes Operadores  da Instalação deve m fechá-lo em anel desde que tenha 
autonomia para tal, adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S.  
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S. 
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação, 
é sempre controlada pelo COSR-S. 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelos Agentes Operadores da Instalação, 
após desligamento automático de equipamentos ou de linhas de transmissão. 
Os Agentes Operadores da Instalação devem identificar os desligamentos automáticos observando 
na Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado 
nas condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Linha de Transmissão / 
Equipamento Procedimentos Condições ou Limites Associados 
LT 525 kV Marmeleiro 2 / 
Santa Vitória do Palmar 2 
C1 ou C2 
Sentido Normal: SE Santa Vitória do Palmar 2 recebe tensão da SE 
Marmeleiro 2  
Ligar, em anel, a LT 525 kV Marmeleiro 
2 / Santa Vitória do Palmar 2  C1 (ou 
C2). 
 
Sentido Inverso: SE Santa Vitória do Palmar 2 envia tensão para a SE 
Marmeleiro 2  
A energização em sentido inverso é controlada pelo COSR-S, conforme  
IO-PM.S.5SU. 
  
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Santa 
Vitória do Palmar 2 IO-OI.S.SPA2 15 3.7.5.1. 03/06/2024 
 
Referência:  10 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Linha de Transmissão / 
Equipamento Procedimentos Condições ou Limites Associados 
  
 
 
Transformador TF 1 (ou TF 
2) 525/138/13,8 kV 
Sentido Normal: A partir do lado de 525 kV 
Energizar, pelo lado de 525 kV, o 
Transformador TF  1 525/138/13,8 kV  
(ou TF 2). 
Sistema completo (de LT) ou N-1 (de 
LT) na SE Santa Vitória do Palmar 2 
525 kV  
Como primeiro ou segundo 
transformador: 
• VSPA2-525 ≤ 550 kV, e 
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa no transformador 
525/138/13,8 kV que já foi 
energizado. 
Ligar, em anel ou energizando o 
barramento de 138  kV, o lado de 138 
kV do Transformador TF 1 
525/138/13,8 kV (ou TF 2). 
 
Sentido Inverso: A partir do lado de 138 kV 
Energizar, pelo lado de 138 kV, o 
Transformador TF  1 525/138/13,8 kV 
(ou TF 2). 
Sistema completo (de LT) na SE 
Santa Vitória do Palmar 2 138 kV  
 
Como primeiro transformador: 
• Não é permitido. 
Como segundo transformador: 
• VSPA2-138 ≤ 145 kV, e 
• 1 ≤ TAPSPA2-525/138 ≤ 10. 
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa no transformador 
525/138/13,8 kV que já foi 
energizado. 
Ligar, em anel, o lado de 525 kV do 
Transformador TF 1 525/138/13,8 kV 
(ou TF 2). 
 
Reator RE 1 525 kV A manobra deste Reator é controlada pelo COSR-S.   
Reator RE 2 525 kV A manobra deste Reator é controlada pelo COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE Santa 
Vitória do Palmar 2 IO-OI.S.SPA2 15 3.7.5.1. 03/06/2024 
 
Referência:  11 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
7. NOTAS IMPORTANTES 
7.1. Os disjuntores de interligação de módulos deve m ser fechados, em anel, toda vez que forem 
recompostos os dois circuitos do mesmo vão ou quando, num dos lados do vão, for uma barra. 
 
