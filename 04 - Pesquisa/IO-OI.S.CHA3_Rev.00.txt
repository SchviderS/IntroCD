 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Charqueadas 3 
 
 
Código Revisão Item Vigência 
IO-OI.S.CAC3 00 3.7.5.2. 23/06/2024 
 
. 
 
MOTIVO DA REVISÃO 
Emissão inicial do Documento, devida ao início da operação da SE Charqueadas 3. 
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S CEEE-T MEZ 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas 3 IO-OI.S.CAC3 00 3.7.5.2. 23/06/2024 
 
Referência:  2 / 9 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  4 
3.1. Barramento de 230 kV ................................................................................................................... 4 
3.2. Alteração na Configuração dos Barramentos ................................................................................ 4 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 4 
4.1. Procedimentos Gerais ................................................................................................................... 4 
4.2. Procedimentos Específicos ............................................................................................................ 5 
4.2.1. Operação dos Comutadores de Tapes Sob Carga (LTC) .............................................. 5 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  5 
5.1. Procedimentos Gerais ................................................................................................................... 5 
5.2. Procedimentos Para Recomposição Fluente ................................................................................. 5 
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................ 5 
5.2.2. Recomposição com Autonomia após Desligamento Total da Instalação.................... 6 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 6 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 6 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 6 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 7 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 7 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 7 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 7 
6.1. Procedimentos Gerais ................................................................................................................... 7 
6.2. Procedimentos Específicos ............................................................................................................ 8 
6.2.1. Desenergização de Equipamentos .............................................................................. 8 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ....................................... 9 
7. NOTAS IMPORTANTES ................................ ................................ ................................ .................. 9 
 
  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas 3 IO-OI.S.CAC3 00 3.7.5.2. 23/06/2024 
 
Referência:  3 / 9 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE Charqueadas 3, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelos Agentes Operadores da Instalação, devendo fazer parte do manual 
de operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade 
das ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR -S e a Instalação, no que se refere aos equipamentos de 
manobra na Instalação, é realizada conforme segue. 
Linha de Transmissão ou 
Equipamento Agente de Operação Agente Operador Centro de Operação 
do Agente Operador  
Barramento de 230 kV MEZ 5 MEZ COZ MEZ Energia 
LT 230 kV Charqueadas 3 / 
Guaíba 3 MEZ 5 MEZ COZ MEZ Energia 
Transformador TR-01 
230/69/13,8 kV MEZ 5 MEZ COZ MEZ Energia 
Transformador TR-01 
230/69/13,8 kV MEZ 5 MEZ COZ MEZ Energia 
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte da Área 230 kV do Rio Grande 
do Sul. 
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.4.1. A definição da quantidade  de tentativas de religamento manual de linha de transmissão ou 
equipamento, bem como o intervalo entre elas,  é de responsabilidade do Agente e devem ser 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica. 
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR-S autorização 
para religamento. Nes sa oportunidade , o Agente pode solicitar também a alteração no sentido 
normal para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em considera ção as condições operativas do 
sistema. 
2.5. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas 3 IO-OI.S.CAC3 00 3.7.5.2. 23/06/2024 
 
Referência:  4 / 9 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2. desta Instrução de Operação , quando o Agente  tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 230 KV 
A configuração do barramento de 230 kV é do tipo Barra Dupla (Barra  1 e Barra 2) a Quatro Chaves. Na 
operação normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados, exceto as 
seccionadoras de transferência e uma das seletoras de barra das linhas de transmissão ou equipamentos. 
A Instalação deve operar em condição normal com a seguinte configuração: 
Em uma das barras – Barra 1 (ou Barra 2)  Na outra barra – Barra 2 (ou Barra 1) 
LT 230 kV Charqueadas 3 / Guaíba 3 - 
Transformador TR-01 230/69/13,8 kV (ou TR-02) Transformador TR-02 230/69/13,8 kV (ou TR-01) 
3.2. ALTERAÇÃO NA CONFIGURAÇÃO DOS BARRAMENTOS 
A mudança de configuração do barramento de  230 kV desta Instalação é executada com controle do  
COSR-S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade d os Agentes 
Operadores da Instalação.  
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S. 
As faixas de controle de tensão para esse  barramento estão estabelecidas no Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. O barramento de 69 kV, não pertencente à Rede de Operação, onde se conecta a transformação 
230/69/13,8 kV, tem a sua regulação de tensão executada com autonomia pelo Agente Operador da 
Instalação, por meio da utilização de recursos locais disponíveis de autonomia dessa. 
Esgotados esses recursos, o Agente deve acionar o COSR -S, que deve verificar a disponibilidade dos 
recursos sistêmicos. 
As faixas de controle de tensão para o barramento de 69 kV estão estabelecidas no Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.3. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob a responsabilidade da Instalação. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas 3 IO-OI.S.CAC3 00 3.7.5.2. 23/06/2024 
 
Referência:  5 / 9 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
4.2. PROCEDIMENTOS ESPECÍFICOS 
4.2.1. OPERAÇÃO DOS COMUTADORES DE TAPES SOB CARGA (LTC) 
Os LTCs dos transformadores TR-01 ou TR-02 230/69/13,8 kV operam em modo manual. 
Alterações no modo de operação desses comutadores são executadas com autonomia pela operação do 
Agente MEZ. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação: caracterizado quando não há tensão em todos os terminais de 
suas linhas de transmissão. 
• Desligamento parcial da Instalação : qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao COSR-S 
as seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2., sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia dos Agentes Operadores da Instalação  na 
recomposição, deve ser utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE 
No caso de desligamento total, o Agente Operador da Instalação deve configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• da linha de transmissão:  
LT 230 kV Charqueadas 3 / Guaíba 3. 
• de todas as linhas de transmissão de 69 kV. 
• dos transformadores:  
TR-01 230/69/13,8 kV (lados de 230 kV e de 69 kV); 
TR-02 230/69/13,8 kV (lados de 230 kV e de 69 kV). 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas 3 IO-OI.S.CAC3 00 3.7.5.2. 23/06/2024 
 
Referência:  6 / 9 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Fechar ou manter fechado o disjuntor: 
• do módulo de interligação de barras de 230 kV, exceto quando esse estiver substituindo o disjuntor de 
um equipamento ou de uma linha de transmissão. 
Desligar ou manter desligado o modo de comutação automática dos comutadores sob carga da 
transformação 230/69/13,8 kV da SE Charqueadas 3. 
Cabe ao Agente MEZ informar ao COSR -S quando a configuração de preparação da Instalação não estiver 
atendida para o início da recomposição, independentemente de o equipamento ser próprio ou de outros 
agentes. Nesse caso, o COSR -S fará contato com os agentes envolvidos para i dentificar o motivo do não -
atendimento e, após confirmação do Agente MEZ de que o barramento está com a configuração atendida, o 
COSR-S coordenará os procedimentos para recomposição, caso necessário, em função da configuração desta 
Instalação. 
5.2.2. RECOMPOSIÇÃO COM AUTONOMIA APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar com autonomia os seguintes procedimentos para a 
recomposição: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 MEZ 
Receber tensão da SE Guaíba 3  pela LT 230 kV 
Charqueadas 3 / Guaíba 3  e energiza r o 
barramento de 230 kV. 
 
1.1 MEZ 
Energizar, pelo lado de 230 kV, um dos 
transformadores 230/69/13,8  kV e ligar, 
energizando o barramento de 69 kV, o lado de 
69 kV. 
1 ≤ TAPCHA3-230/69 ≤ 16 
Restabelecer carga somente após 
autorização do COSR-S. 
1.2 MEZ 
Energizar, pelo lado de 230 kV, o outro 
transformador 230/69/13,8  kV e ligar, 
interligando esse com o outro transformador 
230/69/13,8, o lado de 69 kV. 
1 ≤ TAPCHA3-230/69 ≤ 16 
Após fluxo de potência ativa no 
outro transformador 
230/69/13,8 kV. 
Os demais equipamentos desta Instalação são restabelecidos sob controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
Os Agentes Operadores da Instalação devem realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
Os Agentes Operadores da Instalação devem realizar os procedimentos do Subitem 5.2.2 ., enquanto não 
houver intervenção do COSR-S no processo de recomposição ou interrupção da autonomia da operação da 
instalação na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras.  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas 3 IO-OI.S.CAC3 00 3.7.5.2. 23/06/2024 
 
Referência:  7 / 9 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, a 
operação do Agente deve preparar a Instalação conforme Subitem 5.2.1., sem necessidade de 
autorização do ONS. 
5.4.1.2. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão nas barras de 230 kV , o Agente Operador deve preparar o setor de 230  kV 
da Instalação (disjuntores das linhas de transmissão e equipamentos) conforme Subitem 5.2.1., 
sem necessidade de autorização do ONS.  
5.4.1.3. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador 
da Instalação deve recompor a Instalação conforme Subitem 5.2 .2., sem necessidade de 
autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitens 5.4.1.2., o Agente Operador 
deve informar ao COSR-S, para que a recomposição da Instalação seja executada com controle do 
COSR-S. 
5.4.2.3. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.3., o Agente Operador 
deve recompor a Instalação conforme Subitem 6.2.2., sem necessidade de autorização do ONS. 
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
o Agente Operador deve informar ao COSR -S, para que a recomposição da Instalação seja 
executada com controle do COSR-S. 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmi ssão ou de equipamentos, após  
desligamento programado, de urgência ou de emergência, só podem ser efetuados com controle do 
COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelos Agentes Operadores  da Instalação 
quando estiverem explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução 
de Operação. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas 3 IO-OI.S.CAC3 00 3.7.5.2. 23/06/2024 
 
Referência:  8 / 9 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, os Agentes Operadores da Instalação devem verificar se 
existe tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pel os os Agentes Operadores da 
Instalação quando estiver especificado nesta Instrução de O peração e estiverem atendidas as 
condições do Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou , quando permitida, em sentido  inverso, pode ser feita com 
autonomia pela operação do Agente, conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica.  
 A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o operador da Instalação deve fechá -lo em anel desde que tenha autonomia  para tal, 
adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S. 
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S. 
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE EQUIPAMENTOS 
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação, é sempre 
controlada pelo COSR-S. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas 3 IO-OI.S.CAC3 00 3.7.5.2. 23/06/2024 
 
Referência:  9 / 9 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelos Agentes Operadores da Instalação, 
após desligamento automático de equipamentos ou de linhas de transmissão. 
Os Agentes Operadores da Instalação devem identificar os desligamentos automáticos observando 
na Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado 
nas condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
LT 230 kV Charqueadas 3 / 
Guaíba 3 
Sentido Único: SE Charqueadas 3 recebe tensão da SE Guaíba 3 
Ligar, energizando o barramento de 
230 kV, a LT 230 kV Charqueadas 3 / 
Guaíba 3. 
 
Transformador TR-01 ou 
TR-02 230/69/13,8 kV  
Sentido Único: A partir do lado de 230 kV 
Energizar, pelo lado de 230  kV, o 
Transformador TR-01 230/69/13,8 kV 
(ou TR-02). 
Como primeiro  ou segundo 
transformador: 
• VCHA3-230 ≤ 242 kV; e 
• 1 ≤ TAPCHA3-230/69 ≤ 16. 
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa no transformador 
230/69/13,8 kV que já foi energizado. 
Ligar, energizando o barramento de 
69 kV ou interligando esse 
transformador com outro já em 
operação, o lado de 69 kV do 
Transformador TR-01 230/69/13,8 kV 
(ou TR-02). 
Barramento de 69 kV da SE 
Charqueadas 3 desenergizado ou 
energizado por outro transformador 
230/69/13,8 kV. 
7. NOTAS IMPORTANTES 
Nada consta. 
