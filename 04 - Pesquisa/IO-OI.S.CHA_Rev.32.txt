 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Charqueadas 
 
 
Código Revisão Item Vigência 
IO-OI.S.CHA 32 3.7.5.2. 02/01/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Padronização dos textos de restabelecimento de carga, referenciando os montantes de carga prioritária às 
instalações da Rede de Operação, deste modo, excluindo, quando possível, citações às instalações fora da 
Rede de Operação, acarretando alterações no item 5.2.2. 
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S CEEE-T CGT Eletrosul 
(COT Sul) 
Gerdau 
RGE Sul  
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas IO-OI.S.CHA 32 3.7.5.2. 02/01/2024 
 
Referência:  2 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  4 
3.1. Barramento de 230 kV ................................................................................................................... 4 
3.2. Alteração na Configuração dos Barramentos ................................................................................ 4 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 5 
4.1. Procedimentos Gerais ................................................................................................................... 5 
4.2. Procedimentos Específicos ............................................................................................................ 5 
4.2.1. Operação dos Comutadores de Tapes Sob Carga (LTC) .............................................. 5 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  5 
5.1. Procedimentos Gerais ................................................................................................................... 5 
5.2. Procedimentos Para Recomposição Fluente ................................................................................. 6 
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................ 6 
5.2.2. Recomposição fluente da Instalação ........................................................................... 6 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 7 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 7 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 7 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 8 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 8 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 8 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 8 
6.1. Procedimentos Gerais ................................................................................................................... 8 
6.2. Procedimentos Específicos ............................................................................................................ 9 
6.2.1. Desenergização de Equipamentos .............................................................................. 9 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ....................................... 9 
7. NOTAS IMPORTANTES ................................ ................................ ................................ ................ 11 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas IO-OI.S.CHA 32 3.7.5.2. 02/01/2024 
 
Referência:  3 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE Charqueadas, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelos Agentes Operadores da Instalação, devendo fazer parte do manual 
de operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade 
das ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR -S e a Instalação,  no que se refere aos equipamen tos de 
manobra na Instalação, é realizada conforme segue. 
Linha de Transmissão ou 
Equipamento Agente de Operação Agente 
Operador 
Centro de Operação 
do Agente Operador  
Barramento de 230 kV CGT Eletrosul CGT Eletrosul COT Sul 
LT 230 kV Charqueadas / 
Cidade Industrial (*) 
CEEE-T 
(linha de transmissão) CGT Eletrosul  COT Sul  CGT Eletrosul 
(módulo) 
LT 230 kV Charqueadas / 
Santa Cruz 1 (**)  
CEEE-T 
(linha de transmissão) CGT Eletrosul COT Sul CGT Eletrosul 
(módulo) 
LT 230 kV Charqueadas / 
Scharlau 2 (***) 
CEEE-T 
(linha de transmissão) CGT Eletrosul COT Sul CGT Eletrosul 
(módulo) 
Transformador  
TF 5 230/69/13,8 kV CGT Eletrosul CGT Eletrosul COT Sul 
Transformador  
TF 6 230/69/13,8 kV 
(Ver subitem 7.1) 
Gerdau CGT Eletrosul COT Sul 
Transformador  
TF 9 230/34,5 kV 
(Ver subitem 7.1) 
Gerdau CGT Eletrosul COT Sul 
(*) O CEEE-T é o agente de operação da LT 230 kV Charqueadas / Cidade Industrial. 
(**) O CEEE-T é o agente de operação da LT 230 kV Charqueadas / Santa Cruz 1. 
(***) O CEEE-T é o agente de operação da LT 230 kV Charqueadas / Scharlau 2. 
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte da Área 230 kV do Rio Grande 
do Sul. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas IO-OI.S.CHA 32 3.7.5.2. 02/01/2024 
 
Referência:  4 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.4.1. A definição do número de tentativas de religamento manual de linha de transmissão ou de 
equipamento, bem como o intervalo entre elas, é de responsabilidade do Agente e devem estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica. 
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR -S autorização 
para religamento. Nessa oportunidade, o Agente também pode solicitar alteração no sentido normal 
para envio de tensão, caso não tenha autonomia para tal.   
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em consideração as condições operativas do 
sistema. 
2.5. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica.  
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação , quando o Agente tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 230 KV 
A configuração do barramento de 230 kV é do tipo Barra Dupla (Barra A e Barra B) a Cinco Chaves. Na 
operação normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados, exceto as 
seccionadoras de transferência e uma das seletoras de barra das linhas de transmissão ou equipamentos. 
A Instalação deve operar em condição normal com a seguinte configuração: 
Em uma das barras – A (ou B) Na outra barra – B (ou A) 
LT 230 kV Charqueadas / Cidade Industrial (ou LT 230 
kV Charqueadas / Scharlau 2) 
LT 230 kV Charqueadas / Scharlau 2 (ou LT 230 kV 
Charqueadas / Cidade Industrial) 
LT 230 kV Charqueadas / Santa Cruz 1 Transformador TF 6 230/69/13,8 kV (ou TF 5) 
Transformador TF 5 230/69/13,8 kV (ou TF 6 ou TF 9) Transformador TF 9 230/34,5 kV (ou TF 5) 
3.2. ALTERAÇÃO NA CONFIGURAÇÃO DOS BARRAMENTOS 
A mudança de configuração do barramento de 230 kV desta Instalação é executada com controle do  
COSR-S. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas IO-OI.S.CHA 32 3.7.5.2. 02/01/2024 
 
Referência:  5 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
A mudança de configuração dos demais barramentos é executada sob responsabilidade do Agente Operador 
da Instalação. 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S. 
As faixas de controle de tensão para esse barramento estão estabelecidas no Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. O barramento de 69 kV, não pertencente à Rede de Operação, onde se conecta a transformação 
230/69/13,8 kV, tem a sua regulação de tensão executada com autonomia pelo Agente Operador da 
Instalação, por meio da utilização de recursos locais disponíveis de autonomia dessa. 
Esgotados esses recursos, o Agente deve acionar o COSR -S, que deve verificar a disponibilidade dos 
recursos sistêmicos. 
As faixas de controle de tensão para o barramento de 69 kV estão estabelecidas no Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.3. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob a responsabilidade da Instalação. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
4.2.1. OPERAÇÃO DOS COMUTADORES DE TAPES SOB CARGA (LTC) 
Os LTCs dos transformadores TF 5 230 / 69 / 13,8 kV operam em modo manual.  
A movimentação dos comutadores é realizada com autonomia pela operação do Agente CGT Eletrosul. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação:  caracterizado quando não há tensão em todos os terminais 
de suas linhas de transmissão. 
• Desligamento parcial da Instalação:  qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao COSR-S 
as seguintes informações: 
• horário da ocorrência; 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas IO-OI.S.CHA 32 3.7.5.2. 02/01/2024 
 
Referência:  6 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2., sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identifican do a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia do Agente Operador da Instalação  na 
recomposição, deve ser utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE 
No caso de desligamento total, configurar os disjuntores dos seguintes equipamentos e linhas de 
transmissão, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• das linhas de transmissão:  
LT 230 kV Charqueadas / Santa Cruz 1; 
LT 230 kV Charqueadas / Scharlau 2; 
LT 230 kV Charqueadas / Cidade Industrial. 
• de todas as linhas de transmissão de 69 kV. 
• dos transformadores: 
TF 5 230/69/13,8 kV (lados de 230 kV e de 69 kV); 
TF 6 230/69/13,8 kV (lados de 230 kV e de 69 kV); 
TF 9 230/34,5 kV (lado de 230 kV). 
Fechar ou manter fechado o disjuntor: 
• do módulo de interligação barras de 230 kV, exceto quando esse estiver substituindo o disjuntor de um 
equipamento ou de uma linha de transmissão. 
Desligar ou manter desligado o modo de comutação automática dos comutadores sob carga da 
transformação 230/69/13,8 kV da SE Charqueadas. 
Cabe ao Agente CGT Eletrosul informar ao COSR-S quando a configuração de preparação da Instalação não 
estiver atendida para o início da recomposição, independentemente de o equipamento ser próprio ou de 
outros agentes. Nesse caso, o COSR -S fará contato com os agentes envolvidos para identificar o motivo do 
não-atendimento e, após confirmação do Agente CGT Eletrosul de que o barramento está com a configuração 
atendida, o COSR -S coordenará os procedimentos para recomposição, caso necessário, em função da 
configuração desta Instalação. 
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO 
A Instalação faz parte da recomposição da Área Itaúba. O Agente Operador deve adotar os procedimentos a 
seguir para recomposição fluente: 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas IO-OI.S.CHA 32 3.7.5.2. 02/01/2024 
 
Referência:  7 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Passo Executor Procedimentos Condições ou Limites Associados 
1 
CGT Eletrosul 
(COT Sul) 
Receber tensão da SE Santa Cruz 1, pela LT 230 
kV Charqueadas / Santa Cruz 1 e energiz ar o 
barramento de 230 kV. 
 
1.1 
CGT Eletrosul 
(COT Sul) 
Energizar, pelo lado 230 kV, o transformador TF 
5 230/69/13,8 kV da SE Charqueadas e ligar, 
energizando o barramento de 69 kV, o lado de 
69 kV. 
TAPCHA-230/69 = 9 
Possibilitar o restabe lecimento de 
carga prioritária nas subestações 
atendidas pela SE Charqueadas. 
1.1.1 CEEE-D 
Restabelecer carga prioritária nas subestações 
atendidas pela SE Charqueadas. 
No máximo 25 MW de carga. 
Respeitar o limite mínimo de 
tensão de 62 kV no barramento 69 
kV. 
1.2 
CGT Eletrosul 
(COT Sul) 
Energizar a LT 230 kV Charqueadas / Cidade 
Industrial, enviando tensão para a SE Cidade 
Industrial. 
Após fluxo de potência ativa  no 
transformador TF 5 230/69/13,8 
kV da SE Charqueadas. 
Possibilitar o restabelecimento de 
carga prioritária nas subestações 
atendidas pela SE Charqueadas. 
1.2.1 RGE Sul 
Restabelecer carga prioritária nas subestações 
atendidas pela SE Charqueadas. 
No máximo 25 MW de carga. 
Respeitar o limite mínimo de 
tensão de 62 kV no barramento 69 
kV. 
2 
CGT Eletrosul 
(COT Sul) 
Receber tensão da SE Scharlau 2 pela LT 230 kV 
Charqueadas / Scharlau 2 e ligar em anel. 
Δδ ≤ 40° 
Os demais equipamentos desta Instalação são restabelecidos sob controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador  da Instalação deve realizar os procedimentos do Subitem 5.2.2, enquanto não houver 
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia deste na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras.  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas IO-OI.S.CHA 32 3.7.5.2. 02/01/2024 
 
Referência:  8 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, o 
Agente Operador  deve preparar a Instalação conforme Subitem 5.2.1, sem necessidade de 
autorização do ONS. 
5.4.1.2. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador  
deve recompor a Instalação conforme Subitem 5.2.2, sem necessidade de autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., o Agente Operador 
deve recompor a Instalação conforme Subitem 6.2.2, sem necessidade de autorização do ONS.    
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, após 
desligamento programado, de urgência ou de emergência, só podem ser efetuados com controle do 
COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelo Agente Operador da Instalação quando 
estiverem explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução de 
Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, o Agente Operador da Instalação deve verificar se existe 
tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pelo Agente Operador da Instalação 
quando estiver especificado nesta Instrução de Operação e estiverem atendidas as condições do 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas IO-OI.S.CHA 32 3.7.5.2. 02/01/2024 
 
Referência:  9 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou, quando permitida, em sentido inverso, pode ser feita com 
autonomia pela operação do Agente, conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica.  
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o operador da Instalação deve fechá -lo em anel desde que tenha autonomia para tal, 
adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE EQUIPAMENTOS 
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação, é sempre 
controlada pelo COSR-S. 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de equipamentos ou de linhas de transmissão. 
O Agente Operador  da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado nas 
condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas IO-OI.S.CHA 32 3.7.5.2. 02/01/2024 
 
Referência:  10 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
LT 230 kV Charqueadas / 
Cidade Industrial 
Sentido Normal: SE Charqueadas envia tensão para SE Cidade Industrial 
Energizar LT 230 kV Charqueadas / 
Cidade Industrial. 
• VCHA-230 ≤ 242 kV. 
• fluxo de potência na LT 230 
kV Charqueadas / Santa Cruz 
1 e no transformador TR 5 
230/69/13,8 kV da SE 
Charqueadas. 
Sentido Inverso: SE Charqueadas recebe tensão da SE Cidade Industrial  
Ligar, em anel, a LT 230 kV Charqueadas 
/ Cidade Industrial. 
 
LT 230 kV Charqueadas / 
Santa Cruz 1 
Sentido Normal: SE Charqueadas recebe tensão da SE Santa Cruz 1  
Ligar, em anel ou energizando o 
barramento de 230 kV,  a LT 230 kV 
Charqueadas / Santa Cruz 1. 
 
Sentido Inverso: SE Charqueadas envia tensão para SE Santa Cruz 1  
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2RS. 
LT 230 kV Charqueadas / 
Scharlau 2 
Sentido Normal: SE Charqueadas recebe tensão da SE Scharlau 2  
Ligar, em anel, a LT 230 kV Charqueadas 
/ Scharlau 2. 
• Δδ ≤ 40°. 
Sentido Inverso: SE Charqueadas envia tensão para SE Scharlau 2 
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2RS. 
Transformador TF 5  
230/69/13,8 kV 
Sentido Normal: A partir do lado de 230 kV 
Energizar, pelo lado de 230 kV, o 
transformador TF 5 230/69/13,8 kV. 
• VCHA-230 ≤ 242 kV; e 
• fluxo de potência ativa na LT 
230 kV Charqueadas / Santa 
Cruz 1 ou esta LT 
energizando o barramento 
de 230 kV. 
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa nos transformadores 
230/69/13,8 kV que já foram 
energizados. 
Ligar, energizando o barramento de 69 
kV ou interligando esse transformador 
com outro já em operação, ou em anel, 
pelo lado de 69 kV, o transformador TF 
5 230/69/13,8 kV. 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Charqueadas IO-OI.S.CHA 32 3.7.5.2. 02/01/2024 
 
Referência:  11 / 11 
 
Alterado pela(s) MOP(s): 
MOP/ONS 508-R/2024;  
7. NOTAS IMPORTANTES 
7.1. Somente o módulo de 230 kV do Transformador TF 6 230/69/13,8 kV e do TF 9 230/34,5 kV da SE 
Charqueadas, pertencem à Rede do Operação, conforme Rotina Operacional RO-RD.BR.01. 
7.2. As tratativas de operação entre o ONS e o consumidor livre Gerdau, ligado ao TF 9 230/34,5 kV, são 
feitas pela CGT Eletrosul , interlocutor oficialmente nomeado pela Gerdaru para essas tratativas, 
conforme carta da Gerdaru SGE-089/1509 de 28/10/2015.  
7.3. No caso de desligamento total da SE Charqueadas, o restabelecimento de carga pelo consumidor livre 
Gerdaru somente pode ser realizado após autorização do COSR-S. 
