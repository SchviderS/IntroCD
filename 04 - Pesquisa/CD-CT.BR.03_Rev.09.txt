 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.11 
 
Cadastro de Informações Operacionais 
Cadastro de Áreas do Controle Automático de Geração do Sistema Interligado Nacional 
 
 
Código Revisão Item Vigência 
CD-CT.BR.03 09 2.4. 12/09/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Seccionamento da LT 138 kV Nova Andradina / Porto Primavera na SE Laguna, alterando o subitem 3.5. 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-NCO COSR-NE COSR-S COSR-SE 
 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Áreas do Controle Automático de Geração 
do Sistema Interligado Nacional CD-CT.BR.03 09 2.4. 12/09/2024 
 
Referência:  2 / 12 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONCEITOS ................................ ................................ ................................ ................................ ... 3 
3. PONTOS DE INTERCÂMBIO DAS ÁREAS DE CONTROLE DE CAG DOS CENTROS DE OPERAÇÃO .......... 4 
3.1. COSR-NCO com o COSR-NE............................................................................................................ 4 
3.2. COSR-NCO com o COSR-SE ............................................................................................................ 4 
3.3. COSR-NCO com o COSR-S .............................................................................................................. 5 
3.4. COSR-NE com o COSR-SE ............................................................................................................... 5 
3.5. COSR-SE com o COSR-S .................................................................................................................. 5 
3.6. COSR-S com as Interligações Internacionais ................................................................................. 6 
4. USINAS SOB CONTROLE E FORMA DE ATUAÇÃO DO CAG ................................ ...............................  6 
4.1. COSR-NCO ...................................................................................................................................... 6 
4.2. COSR-NE ......................................................................................................................................... 6 
4.3. COSR-SE ......................................................................................................................................... 6 
4.4. COSR-S ........................................................................................................................................... 7 
5. CARACTERÍSTICAS DO CAG DO COSR-NCO ................................ ................................ .....................  8 
6. CARACTERÍSTICAS DO CAG DO COSR-NE ................................ ................................ ........................  9 
7. CARACTERÍSTICAS DO CAG DO COSR-SE ................................ ................................ ......................  10 
8. CARACTERÍSTICAS DO CAG DO COSR-S ................................ ................................ ........................  11 
9. PARÂMETROS DE BANDA DE ATUAÇÃO DO CAG ................................ ................................ .......... 11 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Áreas do Controle Automático de Geração 
do Sistema Interligado Nacional CD-CT.BR.03 09 2.4. 12/09/2024 
 
Referência:  3 / 12 
 
1. OBJETIVO 
Apresentar a organização das áreas de controle da operação em CAG - Controle automático de Geração, 
considerando os seguintes aspectos: 
• Descrição das áreas de controle. 
• Pontos de intercâmbio das áreas de controle. 
• Características principais dos CAGs dos Centros do ONS. 
• Parâmetros de banda de atuação do CAG. 
2. CONCEITOS 
2.1. Área de Controle: Conforme submódulo 1.2 dos Procedimentos de Rede, Rev. 2020.12, “Área do SIN 
sobre a qual um centro de operação tem a responsabilidade pelo controle de frequência e do 
intercâmbio de energia elétrica”. 
2.2. Características de uma área de controle: 
• atuação subordinada a somente um Centro do ONS. 
• capacidade de geração que permite um equilíbrio entre carga e geração em qualquer período de 
carga. 
• existência de pontos de intercâmbio, com telemedição entre outras áreas de controle. 
• geração de usinas sob controle ligadas ao CAG do Centro de controle da área. 
2.3. As áreas de controle são constituídas de: 
• usinas controladas automaticamente. 
• usinas não controladas automaticamente. 
• carga na área de controle. 
• transmissão na área de controle. 
2.4. Unidade de despacho: pode ser uma usina, quando essa é operada por controle conjunto, ou uma 
unidade geradora quando a usina é operada por controle individual. 
2.5. Fator de Participação: indica a proporção de atuação da unidade de despacho em relação às demais 
unidades que estão sob controle do CAG, sendo seu ajuste normalmente automático, porém 
podendo ser alterado manualmente pelo operador de forma a obter o melhor desempenho no 
controle das unidades de despacho. 
2.6. Ponto Base: indica o valor de geração desejado para a unidade de despacho, podendo ser calculado 
pelo CAG ou inserido manualmente pelo operador.  
2.7. Os conceitos necessários à compreensão do Controle de Geração estão expostos na RT-CG.BR.02. 
2.8. O diagrama DU-CG.BR.01 - Áreas de Controle do CAG identifica os pontos de medição de intercâmbio 
utilizados pelos equipamentos de CAG dos Centros do ONS, em cada equipamento de fronteira. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Áreas do Controle Automático de Geração 
do Sistema Interligado Nacional CD-CT.BR.03 09 2.4. 12/09/2024 
 
Referência:  4 / 12 
 
3. PONTOS DE INTERCÂMBIO DAS ÁREAS DE CONTROLE DE CAG DOS CENTROS DE OPERAÇÃO 
3.1. COSR-NCO COM O COSR-NE 
Centro de Operação Interligações com o COSR-NE 
COSR-NCO 
• LT 500 kV Luziânia (*) / Rio das Éguas  
• LT 500 kV Colinas (*) / Ribeiro Gonçalves C1 e C2 
• LT 500 kV Serra da Mesa 2 (*) / Rio das Éguas 
• LT 500 kV Boa Esperança (*) / Presidente Dutra 
• LT 500 kV Presidente Dutra (*) / Teresina II C1 e C2 
• LT 500 kV Bacabeira (*) / Parnaíba III C1 e C2 
• LT 500 kV Gilbués II / Miracema (*) C3 
• LT 230 kV Teresina (*) / Coelho Neto 
• LT 230 kV Ribeiro Gonçalves (*) / Balsas 
• LT 230 kV Dianópolis II (*) / Barreiras II 
(*) ponto de intercâmbio/medição. 
3.2. COSR-NCO COM O COSR-SE 
Centro de Operação Interligações com o COSR-SE 
COSR-NCO 
• TF 1T01 e 1T02 de 500 (*) / 329,5 kV da SE Estreito 
• TF 2T01 e 2T02 de 500 (*) / 329,5 kV da SE Estreito 
• TF 1T01 e 1T02 de 500 (*) / 330,9 kV da SE Terminal Rio 
• TF 2T01 e 2T02 de 500 (*) / 330,9 kV da SE Terminal Rio 
• TF1D e TF1Y de 500 (*) / 236 kV da SE Araraquara 2 
• TF2D e TF2Y de 500 (*) / 236 kV da SE Araraquara 2 
• TF3D e TF3Y de 500 (*) / 236 kV da SE Araraquara 2 
• TF4D e TF4Y de 500 (*) / 236 kV da SE Araraquara 2 
• LT 500 kV Nova Ponte / Itumbiara (*) 
• LT 500 kV Luziânia / Paracatu 4 (*) 
• LT 500 kV Samambaia/ Emborcação (*) 
• LT 500 kV Itumbiara (*) / São Simão 
• LT 500 kV São Simão (*) / Itaguaçu 
• LT 500 kV Emborcação/ Itumbiara (*) 
• LT 500 kV Itumbiara (*) / Marimbondo; 
• LT 500 kV Rio Verde Norte / Marimbondo II C1 e C2 (*) 
• LT 500 kV Luziânia (*) / Pirapora 2 
• LT 345 kV Itumbiara (*) / Porto Colômbia 
• LT 138 kV Avatinguara / Cachoeira Dourada C1 e C2 (*) 
• LT 138 kV DIMIC / Emborcação (*) 
• LT 138 kV Catalão / Emborcação (*) 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Áreas do Controle Automático de Geração 
do Sistema Interligado Nacional CD-CT.BR.03 09 2.4. 12/09/2024 
 
Referência:  5 / 12 
 
(*) ponto de intercâmbio/medição. 
3.3. COSR-NCO COM O COSR-S 
Centro de Operação Interligações com o COSR-S 
COSR-NCO • LT 230 kV Chapadão (*) / Jataí C1 e C2 
(*) ponto de intercâmbio/medição. 
3.4. COSR-NE COM O COSR-SE 
Centro de Operação Interligações com o COSR-SE 
COSR-NE 
• LT 500 kV Arinos 2 (*) / Rio das Éguas C1 (N4) 
• LT 500 kV Padre Paraíso 2 (*) / Poções III C1 e C2 
• LT 500 kV Bom Jesus da Lapa II / Janaúba 3 (*) (L4) 
• LT 500 kV Igaporã III / Janaúba 3 (*) C1 e C2  
(*) ponto de intercâmbio/medição. 
3.5. COSR-SE COM O COSR-S 
Centro de Operação Interligações com o COSR-S 
COSR-SE 
• LT 525 kV Cascavel Oeste / Foz do Iguaçu 60 Hz (*) 
• LT 525 kV Ivaiporã (CGT ELETROSUL) / Ivaiporã (FURNAS) (*) C1, C2 e C3 
• LT 525 kV Foz do Iguaçu 60Hz (*) / Guaíra C1 e C2 
• LT 525 KV Londrina (CGT ELETROSUL) / Assis (*) C1 e C2 
• LT 500 kV Ibiúna / Bateias (*) C1 e C2 
• LT 500 kV Itatiba / Bateias (*) 
• LT 440 kV Ilha Solteira (*) / Ilha Solteira 2 C1 e C2 
• LT 440 kV Nova Porto Primavera / Porto Primavera C1 e C2 (*) 
• LT 230 kV Assis (*) / Londrina (COPEL GeT) C1 e C2 
• LT 230 kV Andirá Leste (*) / Assis 
• LT 230 kV Andirá Leste (*) / Salto Grande 
• LT 230 kV Chavantes (*) / Figueira 
• LT 230 kV Jaguariaíva (*) / Itararé II 
• LT 230 kV Nova Porto Primavera (*) / Rosana C1 e C2 
• LT 138 kV Água Clara / Jupiá (*) 
• LT 138 kV Jupiá (*) / Mimoso C2, C3 e C4 
• LT 138 kV Laguna / Porto Primavera (*) 
• LT 88 kV Salto Grande (*) / Andirá C1 e C2 
(*) ponto de intercâmbio/medição. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Áreas do Controle Automático de Geração 
do Sistema Interligado Nacional CD-CT.BR.03 09 2.4. 12/09/2024 
 
Referência:  6 / 12 
 
3.6. COSR-S COM AS INTERLIGAÇÕES INTERNACIONAIS 
Centro de Operação Interligações internacionais 
COSR-S 
• Conversora de Frequência Garabi 1: LT 525 kV Conversora Garabi I / Santo 
Ângelo (*). 
• Conversora de Frequência Garabi 2: LT 525 kV Conversora Garabi II / Santo 
Ângelo (*). 
• Conversora de Frequência Rivera: LT 230 kV Livramento 2 (*) / Rivera. 
• Conversora de Frequência de Melo: 
• Exportação: LT 525 kV Candiota (*) / Conversora Melo; 
• Importação: Conversora Melo – 60 Hz / 50 Hz (*). 
4. USINAS SOB CONTROLE E FORMA DE ATUAÇÃO DO CAG 
4.1. COSR-NCO 
 O controle conjunto das usinas divide a geração determinada pelo CAG entre as unidades geradoras 
sob controle (unidades de despacho). 
Usina Modo de controle Envio de sinal 
Tucuruí 
Conjunto 
Set-point 
Lajeado 
Estreito 
Peixe Angical 
Itumbiara Pulso 
Sinop Individual Set-point 
4.2. COSR-NE 
Envio de pulso para as unidades geradoras (unidades de despacho) selecionadas para operação sob 
CAG. 
Usina Modo de controle Envio de sinal 
Luiz Gonzaga 
Individual Pulso 
Paulo Afonso IV 
4.3. COSR-SE 
Envio de pulso para as usinas sob CAG, cada uma configurando uma unidade de despacho. O controle 
conjunto das usinas divide o requisito de geração determinado pelo CAG entre as unidades geradoras 
sob controle. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Áreas do Controle Automático de Geração 
do Sistema Interligado Nacional CD-CT.BR.03 09 2.4. 12/09/2024 
 
Referência:  7 / 12 
 
Usina Modo de controle Envio de sinal 
Furnas 
Conjunto Pulso 
Itaipu 60 Hz 
L. C. Barreto 
Marimbondo 
Água Vermelha 
Individual 
Pulso 
Capivara 
Theodomiro Carneiro 
Santiago 
Ilha Solteira 
Nova Ponte Set point 
4.4. COSR-S 
Usina Modo de controle Envio de sinal 
Gov. Bento Munhoz 
Individual 
Pulso 
Gov. Ney Braga 
Barra Grande 
Set point 
Passo Fundo 
Campos Novos 
Foz do Chapecó 
Gov. José Richa 
Itá 
Machadinho 
Salto Osório 
Salto Santiago 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Áreas do Controle Automático de Geração 
do Sistema Interligado Nacional CD-CT.BR.03 09 2.4. 12/09/2024 
 
Referência:  8 / 12 
 
5. CARACTERÍSTICAS DO CAG DO COSR-NCO 
Modalidade de Operação Pontos de Aquisição 
do Sinal de 
Frequência 
Estimador 
de Estado BIAS 
Possível Normal 
TLB, FF, FTL 
TLB ou FF, 
conforme 
condições 
estabelecidas 
na IO-
CG.BR.02 
1. Medição Local 
(COSR-NCO – 
Brasília) 
2. Barra de 500 kV 
da SE Tucuruí 
3. Barra de 230 kV 
da SE Samuel 
4. Barra de 138 kV 
da SE Santana 
(em ordem de 
prioridade aquisição) 
SIM 
Calculado automaticamente, em função 
da carga da área, das unidades 
geradoras sincronizadas na área e da 
folga de geração em relação à máxima 
geração das unidades geradoras 
sincronizadas. 
Existe possibilidade de entrada manual 
de valor constante. 
 
Usina sob CAG Unidades da(s) Usina(s) sob CAG Número mínimo de máquinas 
Tucuruí Primeira etapa: Todas 
Segunda etapa: Nenhuma 2 
Itumbiara Todas (6) - 
Lajeado Todas (5) 3 
Tucuruí e Lajeado - 2 em Tucuruí e 2 em Lajeado 
Estreito 8 2 
Peixe Angical 3 2 
Sinop 2 2 
5.1. A segunda etapa de Tucuruí não opera no CAG. 
5.2. A seleção do sinal de frequência para o funcionamento do CAG do COSR -NCO é efetuada de forma 
automática, nos pontos apresentados na tabela acima, com aquisição na sequência apresentada. 
5.3. O número mínimo de máquinas nas usinas é estabelecido apenas como parâmetro para a ativação 
do controle conjunto das unidades. No caso da operação apenas de Lajeado no CAG, o número 
mínimo de máquinas são três , pois duas unidades não são capazes de alocar a reserva de potência 
necessária nesta usina. 
5.4. O CAG será desligado pelos seguintes eventos: 
• desvio de Frequência igual ou superior a +/- 0,5 Hz (parâmetro FREQMX); 
• desvio de intercâmbio (parâmetro NIDEVM); 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Áreas do Controle Automático de Geração 
do Sistema Interligado Nacional CD-CT.BR.03 09 2.4. 12/09/2024 
 
Referência:  9 / 12 
 
• perda de todas as telemedições de frequência; 
• perda das telemedições (principal e backup) de alguma linha de intercâmbio, quando operando em 
TLB. 
6. CARACTERÍSTICAS DO CAG DO COSR-NE 
Modalidade de Operação Pontos de Aquisição do Sinal de 
Frequência Usinas sob CAG 
Unidades da 
Usina sob 
CAG Possível Normal 
TLB, FF, FTL 
TLB ou FF, 
conforme 
condições 
estabelecidas 
na IO-CG.BR.02 
1. Medição Local (COSR-NE – Recife) 
2. Barra de 230 kV da SE Penedo 
3. Barra de 230 kV da UTE Guarani 
4. Barra de 230 kV da SE Bom Jesus da 
Lapa II 
5. Barra de 230 kV da SE Maceió 
6. Barra de 230 kV da SE Arapiraca III 
7. Barra de 500 kV da SE Xingó 
(em ordem de prioridade aquisição) 
Paulo Afonso IV Todas 
Luiz Gonzaga Todas 
 
Estimador de 
Estado Será desligado pelos seguintes eventos BIAS 
Não disponível 
• Desvio de Frequência igual ou superior a +/ - 0,5 Hz 
(parâmetro FREQMX); 
• Desvio de intercâmbio (parâmetro NIDEVM); 
• Perda de todas as telemedições de frequência; 
• Perda das telemedições (principal e backup) de 
alguma linha de intercâmbio, quando operando em 
TLB. 
Calculado automaticamente, em 
função da carga da área, das 
unidades geradoras 
sincronizadas na área e da folga 
de geração em relação à máxima 
geração das unidades geradoras 
sincronizadas. 
Existe possibilidade de entrada 
manual de valor constante. 
6.1. As unidades geradoras das Usinas de Paulo Afonso IV e Luiz Gonzaga sob o controle do CAG só podem 
operar em Regulador de Potência. 
6.2. Haverá desligamento automático do CAG e comutação automática da modalidade  de regulação de 
potência para regulação de velocidade das unidades geradoras da usina de Paulo Afonso IV ou Luiz 
Gonzaga, operando sob CAG , quando ocorrer variação de frequência superior a 0,6 Hz na usina de 
Paulo Afonso IV ou 1,2 Hz para a Usina de Luiz Gonzaga. 
• O sinal de frequência proveniente da barra 04BP da SE Cauípe ou do COS é proveniente de fontes 
independentes, comutadas de forma automática quando da perda de uma delas. A comutação 
para o sinal proveniente da barra 230 kV de Paulo Afonso é feita de forma manual. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Áreas do Controle Automático de Geração 
do Sistema Interligado Nacional CD-CT.BR.03 09 2.4. 12/09/2024 
 
Referência:  10 / 12 
 
7. CARACTERÍSTICAS DO CAG DO COSR-SE 
Modalidade de Operação 
Pontos de Aquisição do Sinal 
de Frequência Usinas sob CAG Unidades da(s) 
Usina(s) sob CAG 
Possível Normal 
TLB, FF ou 
FTL 
TLB ou FF, 
conforme 
condições 
estabelecidas 
na IO-
CG.BR.02 
1. Medição Local (COSR -SE – 
Rio de Janeiro) 
2. Barra de 440 kV da SE 
Cabreúva 
3. Barra de 440 kV da SE Bauru 
4. Barra de 440 kV da SE Bom 
Jardim 
5. Barra de 345 kV da UHE 
Furnas 
6. Barra de 345 kV da SE 
Barbacena 
7. Barra de 500 kV da SE Bom 
Despacho III 
(em ordem de prioridade 
aquisição) 
• Furnas 
• Luiz Carlos 
Barreto 
• Marimbondo 
• Itaipu-60 HZ (1) 
• Ilha Solteira 
• Água Vermelha 
• Capivara  
• Theodomiro 
Carneiro Santiago 
• Nova Ponte 
Todas 
Nota (1): A UHE Itaipu 60 Hz está temporariamente fora do CAG. 
Estimador de 
Estado Será desligado pelos seguintes eventos BIAS 
Não 
disponível 
• Desvio de Frequência igual ou superior a 
+/- 0,5 Hz (parâmetro FREQMX); 
• Desvio de intercâmbio (parâmetro 
NIDEVM); 
• Perda de todas as telemedições de 
frequência; 
• Perda das telemedições  (principal e 
backup) de alguma linha de intercâmbio, 
quando operando em TLB; 
Calculado automaticamente, em função da 
carga da área, das unidades geradoras 
sincronizadas na área e da folga de geração 
em relação à máxima geração das unidades 
geradoras sincronizadas. 
Existe possibilidade de entrada manual de 
valor constante. 
7.1. Em caso de falha, a comutação dos sinais de frequência que podem ser usados é automática. Existem 
sete fontes de frequência disponíveis, sendo a fonte preferencial a da rede de alimentação do COSR-
SE. 
7.2. A distribuição de geração entre as unidades sob controle é feita de forma proporcional à geração 
programada. A largura dos pulsos emitidos pelo CAG é variável entre um valor mínimo e um valor 
máximo. 
7.3. A carga da área de controle é calculada por meio da diferença entre geração total (medida e não 
medida) e o intercâmbio líquido verificado. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Áreas do Controle Automático de Geração 
do Sistema Interligado Nacional CD-CT.BR.03 09 2.4. 12/09/2024 
 
Referência:  11 / 12 
 
8. CARACTERÍSTICAS DO CAG DO COSR-S 
Modalidade de Operação 
Pontos de Aquisição do 
Sinal de Frequência Usinas sob CAG Unidades sob 
CAG Possível Normal 
TLB, FF, 
FTL 
TLB ou FF, 
conforme 
condições 
estabelecidas 
na IO-CG.BR.02 
1. Medição local (COSR-S- 
Florianópolis) 
2. Barra de 230 kV da SE 
Farroupilha 
3. Barra de 230 kV da SE 
Salto Osório 
4. Barra de 525 kV da SE 
Curitiba 
(em ordem de prioridade 
aquisição) 
• Salto Osório 
• Salto Santiago 
• Machadinho 
• Passo Fundo 
• Itá 
• Barra Grande 
• Campos Novos 
• Gov. Bento Munhoz da 
Rocha Netto 
• Gov. Ney Aminthas de 
Barros Braga 
• UHE Gov. José Richa 
• Foz do Chapecó 
Todas 
 
Estimador de 
Estado Será desligado pelos seguintes eventos BIAS 
Disponível 
• Desvio de Frequência igual ou superior a +/ - 0,5 Hz 
(parâmetro FREQMX); 
• Desvio de intercâmbio (parâmetro NIDEVM); 
• Perda de todas as telemedições de frequência; 
• Perda das telemedições (principal e backup) de 
alguma linha de intercâmbio, quando operando 
em TLB. 
Calculado automaticamente, em 
função da carga da área, das 
unidades geradoras sincronizadas 
na área e da folga de geração em 
relação à máxima geração das 
unidades geradoras sincronizadas. 
Existe possibilidade de entrada 
manual de valor constante. 
8.1. A seleção do sinal de frequência para o funcionamento do CAG do COSR -S é efetuada de forma 
automática, nos pontos apresentados na tabela acima, com aquisição na sequência apresentada. 
9. PARÂMETROS DE BANDA DE ATUAÇÃO DO CAG 
Parâmetro/Centro 
Parâmetros MW (*) 
NCO SE NE S 
ACEAST (valor de assistência - ECA) 360 513,52 196 205 
ACEWRN (valor de advertência - ECA) 250 366,80 140 287 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11 
 Cadastro de Informações Operacionais Código Revisão Item Vigência 
Cadastro de Áreas do Controle Automático de Geração 
do Sistema Interligado Nacional CD-CT.BR.03 09 2.4. 12/09/2024 
 
Referência:  12 / 12 
 
Parâmetro/Centro 
Parâmetros MW (*) 
NCO SE NE S 
ACEMAX (máximo valor para correção - ECA) 720 733,60 280 410 
NIDEVM (máximo desvio de intercâmbio) 650 720,26 252 369 
 
