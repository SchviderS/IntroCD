Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.11
Cadastro de Informações Operacionais.
Cadastro de Informações Operacionais de Faixas para Controle de Tensão da Interligação 
Sudeste/ Nordeste
Código Revisão Item Vigência
CD-CT.SENE.03 25 2.6.1 05/02/2024
MOTIVO DA REVISÃO
- Adequação da grandeza de referência para as faixas de tensão da SE Padre Paraíso 2.
LISTA DE DISTRIBUIÇÃO
CHESF CNOS COSR-NCO COSR-NE COSR-SE
EQUATORIAL INTESA PTE STATE GRID TAESA TSN
TCC TME TPE
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Interligação Sudeste/ Nordeste CD-CT.SENE.03 25 2.6.1 05/02/2024
- Diretrizes para operação elétrica com horizonte quadrimestral Janeiro-Abril 2024 - Volume 1. 2 / 4
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. CONCEITOS ......................................................................................................................................3
3. CONSIDERAÇÕES GERAIS .................................................................................................................3
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO .........................................................4
4.1. Faixas de tensão para Barramentos da Rede de Operação...........................................................4
4.2. Demais Faixas de tensão para a Rede de Operação......................................................................4
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Interligação Sudeste/ Nordeste CD-CT.SENE.03 25 2.6.1 05/02/2024
- Diretrizes para operação elétrica com horizonte quadrimestral Janeiro-Abril 2024 - Volume 1. 3 / 4
1. OBJETIVO
Apresentar as faixas de controle de tensão nos barramentos e transformadores da Rede de Operação a serem 
controlados pelos operadores dos Centros de Operação do ONS e pela Operação dos agentes envolvidos.
2. CONCEITOS
2.1. As faixas de controle de tensão são estabelecidas em função de uma grandeza de referência, definida 
conforme a seguir:
Nome da 
Grandeza Sigla Definição / Equipamento Sentido Positivo Ponto de 
Medição
É o somatório dos fluxos (MW) abaixo descritos:
LT 500 kV Presidente Dutra / Boa 
Esperança
Presidente Dutra → Boa 
Esperança
SE 
Presidente 
Dutra
LTs 500 kV Presidente Dutra / 
Teresina II C1 e C2
Presidente Dutra → 
Teresina II
SE 
Presidente 
Dutra
LTs 500 kV Colinas / Ribeiro 
Gonçalves C1 e C2
Colinas → Ribeiro 
Gonçalves SE Colinas
LT 500 kV Bacabeira / Parnaíba III C1 
e C2 Bacabeira → Parnaíba III SE Bacabeira
LT 500 KV Gilbués II / Miracema C3 Miracema → Gilbués II SE Miracema
Fluxo 
Norte / 
Nordeste
FNNE
LT 230 kV Coelho Neto / Teresina Coelho Neto → Teresina SE Coelho 
Neto
3. CONSIDERAÇÕES GERAIS
3.1. Este Cadastro de Informações Operacionais apresenta os valores das faixas para controle de tensão de 
barramentos da Rede de Operação e de secundários das transformações da Rede de Operação cujas 
barras não pertencem à Rede de Operação.
3.2. Os Centros de Operação do ONS, nas ações de coordenação, supervisão e controle, devem observar 
os limites operacionais constantes neste Cadastro de Informações Operacionais.
3.3. A operação dos agentes, em suas ações de comando e execução, deve observar os limites operacionais 
constantes neste Cadastro de Informações Operacionais.
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais de Faixas para 
Controle de Tensão da Interligação Sudeste/ Nordeste CD-CT.SENE.03 25 2.6.1 05/02/2024
- Diretrizes para operação elétrica com horizonte quadrimestral Janeiro-Abril 2024 - Volume 1. 4 / 4
4. FAIXAS PARA CONTROLE DE TENSÃO DA REDE DE OPERAÇÃO
4.1. FAIXAS DE TENSÃO PARA BARRAMENTOS DA REDE DE OPERAÇÃO
Subestação / Usina Faixas de Tensão  (kV) em função do(a) FNNE
Nome Tensão
(kV)
FNNE > 5500 
MW
(Pesada)
4000 MW < 
FNNE <= 
5500 MW
(Média)
FNNE <= 
4000 MW
(Leve)
FNNE <  4000 
MW
(Mínima)
SE Padre Paraíso 2 500 520 - 550 520 - 550 500 - 550 500 - 550
4.2. DEMAIS FAIXAS DE TENSÃO PARA A REDE DE OPERAÇÃO
Os barramentos da Rede de Operação e os enrolamentos das transformações da Rede de Operação, que não 
possuem faixas de tensão definidas nos itens anteriores, devem operar com tensões nas seguintes faixas 
operativas, conforme estabelecido no submódulo 23.3 dos Procedimentos de Rede e reproduzido na tabela 
a seguir:
Tensão Nominal (kV) Faixa de Tensão (kV)
765 690 a 800
525 500 a 550 
500 500 a 550 
440 418 a 460 
345 328 a 362 
230 218 a 242 
138 131 a 145 
88 83,6 a 92,4
69 65,6 a 72,4
34,5 32,8 a 36,2
23 21,8 a 24,2
13,8 13,1 a 14,5
