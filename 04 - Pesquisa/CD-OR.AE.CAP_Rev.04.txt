Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.11
Cadastro de Informações Operacionais
Cadastro de Informações Operacionais Hidráulicas da Bacia dos Rios Capivari e 
Cachoeira
Código Revisão Item Vigência
CD-OR.AE.CAP 04 2.3. 04/09/2023
.
MOTIVO DA REVISÃO
Inclusão do Item 7., conforme padronização do Documento.
LISTA DE DISTRIBUIÇÃO
CNOS COSR-S GPD NSUL COPEL GeT
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia dos Rios Capivari e Cachoeira CD-OR.AE.CAP 04 2.3. 04/09/2023
Referência: 2 / 15
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. CONSIDERAÇÕES GERAIS .................................................................................................................3
3. DADOS DO RESERVATÓRIO ..............................................................................................................4
3.1. Dados de Identificação do Aproveitamento..................................................................................4
3.2. Dados Atemporais do Reservatório – Tabela 1 .............................................................................4
3.3. Dados Atemporais do Reservatório – Tabela 2 .............................................................................5
3.4. Dados Atemporais do Reservatório – Tabela 3 .............................................................................5
4. RESTRIÇÕES OPERATIVAS HIDRÁULICAS (ROH) E INFORMAÇÕES OPERATIVAS RELEVANTES (IOR) ...6
4.1. UHE Gov. Pedro Viriato Parigot de Souza......................................................................................6
5. REDE HIDROMÉTRICA ......................................................................................................................7
5.1. Estações Hidrométricas – COPEL GeT ...........................................................................................7
5.2. Mapa Ilustrativo da Localização das Estações Hidrométricas .......................................................7
5.3. Características das Estações Hidrométricas ..................................................................................8
6. DIAGRAMAS DE OPERAÇÃO .............................................................................................................9
6.1. Reservatório da UHE Gov. Pedro Viriato Parigot de Souza ...........................................................9
7. VOLUMES DE ESPERA PARA O CONTROLE DE CHEIAS .....................................................................15
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia dos Rios Capivari e Cachoeira CD-OR.AE.CAP 04 2.3. 04/09/2023
Referência: 3 / 15
1. OBJETIVO
Apresentar os Dados Operacionais Hidráulicos dos Reservatórios da Bacia Hidrográfica dos Rios Capivari e 
Cachoeira, de acordo com os Procedimentos de Rede, a serem observados pelos operadores dos Centros de 
Operação do ONS e pelas Áreas de Operação dos Agentes de Geração envolvidos no controle desses valores.
2. CONSIDERAÇÕES GERAIS
2.1. Os dados constantes neste Cadastro de Informações Operacionais Hidráulicas são informações sobre 
os reservatórios dessa Bacia Hidrográfica, necessárias às ações de coordenação, supervisão e controle 
dos Centros de Operação do ONS para a operação hidráulica desse Sistema de Reservatórios.
2.2. Os operadores dos Centros de Operação do ONS, nas ações de coordenação, de supervisão e de 
controle, devem observar os dados operacionais constantes neste Cadastro de Informações 
Operacionais Hidráulicas.
2.3. As áreas de operação dos Agentes de Geração, em suas ações de comando e execução, devem 
observar os dados operacionais constantes neste Cadastro de Informações Operacionais Hidráulicas.
2.4. Os campos das tabelas, cujos dados operacionais não existam, devem ser preenchidos com a sigla 
“NE” – Não Existente.
2.5. Os campos das tabelas cujos dados operacionais não foram informados pelo Agente de Geração 
devem ser preenchidos com a sigla “NI” – Não Informado.
2.6. Os campos das tabelas cujos dados operacionais não se aplicam para o Reservatório devem ser 
preenchidos com um hífen “-”.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da Bacia dos Rios Capivari e Cachoeira CD-OR.AE.CAP 04 2.3. 04/09/2023
Referência: 4 / 15
3. DADOS DO RESERVATÓRIO
3.1. Dados de Identificação do Aproveitamento
Localização em Relação à Barragem
Denominação do 
Reservatório Usina Rio Classificação Regularização Município Margem 
Direita
Município Margem 
Esquerda
Gov. Pedro Viriato 
Parigot de Souza
UHE Gov. Pedro 
Viriato Parigot de 
Souza
Capivari Acumulação Mensal Campina Grande do Sul - 
PR Bocaiúva do Sul - PR
3.2. Dados Atemporais do Reservatório – Tabela 1
Nível (m) Área de Drenagem (km²) Distâncias e Tempos de Viagens 
entre Aproveitamentos
Nome
Mínimo 
Operativo
Máximo 
Operativo
Máximo 
Maximorum Coroamento Canal de Fuga 
(média)
Total até o 
Reservatório
Própria do 
Reservatório Aproveitamentos (km) (h)
Gov. Pedro Viriato 
Parigot de Souza 822,00 845,00 845,50 849,00 90,70 945 945 - - -
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da Bacia dos Rios Capivari e Cachoeira CD-OR.AE.CAP 04 2.3. 04/09/2023
Referência: 5 / 15
3.3. Dados Atemporais do Reservatório – Tabela 2
Volume (hm³) Dados da Casa de ForçaAproveitamento
Útil
No Nível 
Mínimo 
Operativo
No Nível 
Máximo 
Operativo
No Nível 
Máximo 
Maximorum
Quantidade de
Unidades 
Geradoras
Potência 
por UG
(MW)
Potência Total
(MW)
Engolimento 
por UG
(m³/s)
Produtibilidade     
a 65% VU
(MW/m³/s)
Gov. Pedro Viriato 
Parigot de Souza 156,00 23,00 179,00 186,00 4 65 260 9,575 6,50
3.4. Dados Atemporais do Reservatório – Tabela 3
Extravasores
Descarregador de Fundo  Vertedor com Comporta Tulipa Vertedor com Soleira Livre Válvula 
Dispersora
Aproveitamento
Quanti
dade
Cota da 
Soleira
(m)
Vazão por 
Comporta
 (m³/s)
Quantid
ade de 
Vãos
Cota da 
Crista da 
Soleira
(m)
Vazão por 
Vão
 (m³/s)
Cota da 
Soleira
(m)
Vazão 
Máxima
(m³/s)
Quantid
ade de 
Vãos
Cota da 
Crista da 
Soleira
(m)
Vazão 
por Vão
(m³/s)
Quanti
dade
Vazão 
por 
Válvula
(m³/s)
Vazão 
Total
(m³/s)
Gov. Pedro Viriato 
Parigot de Souza 2 800,50 125 2 838,00 382 - - - - - - - 1014
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia dos Rios Capivari e Cachoeira CD-OR.AE.CAP 04 2.3. 04/09/2023
Referência:  6 / 15
4. RESTRIÇÕES OPERATIVAS HIDRÁULICAS (ROH) E INFORMAÇÕES OPERATIVAS RELEVANTES (IOR)
4.1. UHE Gov. Pedro Viriato Parigot de Souza
4.1.1.Restrições Operativas Hidráulicas (ROH)
4.1.1.1.Restrições de Jusante 
a) Rio Capivari
ROH 1 – Vazão Vertida Máxima
A vazão vertida deve ser no máximo de 140 m³/s, de maneira a se evitar danos em ponte sobre 
esse mesmo Rio, localizada 700 m à jusante do vertedor, construída pelo DER/PR, em estrada 
secundária que liga os municípios de Bocaiúva do Sul e Campina Grande do Sul, no estado do 
Paraná.
ROH 2 – Vazão Turbinada Mínima
A vazão turbinada deve ser de, no mínimo, 2,4 m³/s, que equivale a geração de 15 MW na UHE Gov. 
Pedro Viriato Parigot de Souza. Isso se deve à necessidade de resfriamento dos equipamentos da 
UHE Gov. Pedro Viriato Parigot de Souza sempre que houver unidade geradora disponível ao 
sistema. Em caso de parada total da Usina para manutenção, estando todas as unidades geradoras 
desligadas, não é necessário se manter essa vazão para resfriamento.
b) Rio Cachoeira
Não há.
4.1.1.2.Restrições de Montante
Não há.
4.1.1.3.Outras Restrições
Nada consta.
4.1.2.Informações Operativas Relevantes (IOR)
IOR 1 – Taxas de Variação Máxima de Vertimento
Os valores de taxas de variação máxima da vazão vertida, que deverão ser consideradas tanto nas 
situações de elevação como de redução de vertimento, são apresentadas a seguir.
Vazão Vertida de Partida
(m³/s)
Taxa de Variação Máxima de Vertimento
(m³/s/h)
de 0 até 10 10
acima de 10 até 45 de 15 até 30, linearmente
acima de 45 até 1014 30
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia dos Rios Capivari e Cachoeira CD-OR.AE.CAP 04 2.3. 04/09/2023
Referência:  7 / 15
5. REDE HIDROMÉTRICA
5.1. Estações Hidrométricas – COPEL GeT
Localização
Estação Código ANA
Latitude (S) Longitude (W) Rio
Capivari Montante 81299001 / 02548082 25°13’00'' 48°57'00'' Capivari
UHE Capivari - Cachoeira – 
Barramento 81301001 / 02548080 25°08’18” 48°52’28” Capivari
Capivari Jusante 81303001 / 02548083 25°05’17” 48°49’33” Capivari
Vila Nova 82121002 / 02548083 25°19’10” 48°42’29” Cachoeira
5.2. Mapa Ilustrativo da Localização das Estações Hidrométricas

Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia dos Rios Capivari e Cachoeira CD-OR.AE.CAP 04 2.3. 04/09/2023
Referência:  8 / 15
5.3. Características das Estações Hidrométricas
Coleta de Dados
Estação
Tipo de 
Dado 
Coletado
Órgão 
Operador
Leitura 
de 
Dados
Transmissão 
Órgão Frequência
FdT / PT SIMEPAR Horária Telemétrica SIMEPAR Horária
Capivari Montante
Fd VLF 
Serviços 7h e 17h Boletim VLF Serviços Diária
LT / PT SIMEPAR Horária Telemétrica SIMEPAR HoráriaUHE Capivari - 
Cachoeira – Barramento L COPEL GeT Eventual Boletim COPEL GeT Eventual
FdT / PT SIMEPAR Horária Telemétrica SIMEPAR Horária
Capivari Jusante
Fd VLF 
Serviços 7h e 17h Boletim VLF Serviços Diária
Vila Nova Fd / P VLF 
Serviços 7h e 17h Boletim VLF Serviços Diária
Notas:
FdT: Fluviométrica telemedida;
Fd: Fluviométrica convencional;
PT: Pluviométrica telemedida;
P: Pluviométrica convencional;
LT: Linimétrica telemedida;
L: Linimétrica convencional.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia dos Rios Capivari e Cachoeira CD-OR.AE.CAP 04 2.3. 04/09/2023
Referência:  9 / 15
6. DIAGRAMAS DE OPERAÇÃO
6.1. Reservatório da UHE Gov. Pedro Viriato Parigot de Souza
6.1.1. Diagrama de Operação Normal
Afluência
[m³/s] 20 40 60 80 100 150 200 300 400 500 600 700 800 900 1000
NAR [m] Defluência Indicada (Vazão Turbinada + Vazão Vertida) [m3/s]
839,00 0 0 0 0 0 0 0 0 0 0 0 0 7 33 64
839,10 0 0 0 0 0 0 0 0 0 0 0 0 10 36 68
839,20 0 0 0 0 0 0 0 0 0 0 0 0 12 39 72
839,30 0 0 0 0 0 0 0 0 0 0 0 0 14 42 76
839,40 0 0 0 0 0 0 0 0 0 0 0 0 17 46 80
839,50 0 0 0 0 0 0 0 0 0 0 0 0 20 49 84
839,60 0 0 0 0 0 0 0 0 0 0 0 1 23 53 89
839,70 0 0 0 0 0 0 0 0 0 0 0 2 26 57 93
839,80 0 0 0 0 0 0 0 0 0 0 0 4 29 61 98
839,90 0 0 0 0 0 0 0 0 0 0 0 6 32 65 102
840,00 0 0 0 0 0 0 0 0 0 0 0 9 36 69 107
840,10 0 0 0 0 0 0 0 0 0 0 0 11 39 73 112
840,20 0 0 0 0 0 0 0 0 0 0 0 14 43 78 117
840,30 0 0 0 0 0 0 0 0 0 0 0 16 46 82 122
840,40 0 0 0 0 0 0 0 0 0 0 0 19 50 86 127
840,50 0 0 0 0 0 0 0 0 0 0 0 22 54 91 132
840,60 0 0 0 0 0 0 0 0 0 0 1 26 58 96 138
840,70 0 0 0 0 0 0 0 0 0 0 4 29 63 101 144
840,80 0 0 0 0 0 0 0 0 0 0 6 33 67 106 149
840,90 0 0 0 0 0 0 0 0 0 0 8 37 72 112 155
841,00 0 0 0 0 0 0 0 0 0 0 11 40 77 117 162
841,10 0 0 0 0 0 0 0 0 0 0 14 45 82 123 168
841,20 0 0 0 0 0 0 0 0 0 0 17 49 87 129 175
841,30 0 0 0 0 0 0 0 0 0 0 21 53 92 135 181
841,40 0 0 0 0 0 0 0 0 0 0 24 58 97 141 188
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia dos Rios Capivari e Cachoeira CD-OR.AE.CAP 04 2.3. 04/09/2023
Referência:  10 / 15
Afluência
[m³/s] 20 40 60 80 100 150 200 300 400 500 600 700 800 900 1000
NAR [m] Defluência Indicada (Vazão Turbinada + Vazão Vertida) [m3/s]
841,50 0 0 0 0 0 0 0 0 0 2 28 63 103 147 195
841,60 0 0 0 0 0 0 0 0 0 4 32 68 109 154 203
841,70 0 0 0 0 0 0 0 0 0 7 36 73 115 161 210
841,80 0 0 0 0 0 0 0 0 0 10 41 78 121 168 218
841,90 0 0 0 0 0 0 0 0 0 14 45 84 128 175 226
842,00 0 0 0 0 0 0 0 0 0 17 50 90 134 183 234
842,10 0 0 0 0 0 0 0 0 0 21 55 96 141 191 243
842,20 0 0 0 0 0 0 0 0 0 25 60 102 149 199 252
842,30 0 0 0 0 0 0 0 0 2 29 66 109 156 207 261
842,40 0 0 0 0 0 0 0 0 5 34 72 116 164 216 270
842,50 0 0 0 0 0 0 0 0 7 38 78 123 172 225 280
842,60 0 0 0 0 0 0 0 0 11 43 84 131 181 234 290
842,70 0 0 0 0 0 0 0 0 15 49 91 138 190 244 301
842,80 0 0 0 0 0 0 0 0 19 55 98 147 199 254 312
842,90 0 0 0 0 0 0 0 0 23 61 105 155 208 265 323
843,00 0 0 0 0 0 0 0 0 28 67 113 164 218 275 335
843,10 0 0 0 0 0 0 0 3 33 74 121 173 229 287 348
843,20 0 0 0 0 0 0 0 6 39 81 130 184 240 300 361
843,30 0 0 0 0 0 0 0 10 45 89 140 194 252 313 375
843,40 0 0 0 0 0 0 0 14 51 98 149 205 264 326 390
843,50 0 0 0 0 0 0 0 19 58 106 160 217 277 340 405
843,60 0 0 0 0 0 0 0 25 66 116 171 230 291 355 421
843,70 0 0 0 0 0 0 0 30 75 126 183 243 306 371 439
843,80 0 0 0 0 0 0 3 37 84 137 195 257 322 388 457
843,81 0 0 0 0 0 0 3 38 85 138 197 259 323 390 459
843,82 0 0 0 0 0 0 3 39 86 140 198 260 325 392 460
843,83 0 0 0 0 0 0 4 39 87 141 199 261 326 393 462
843,84 0 0 0 0 0 0 4 40 87 142 201 263 328 395 464
843,85 0 0 0 0 0 0 5 41 89 143 202 264 330 397 466
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia dos Rios Capivari e Cachoeira CD-OR.AE.CAP 04 2.3. 04/09/2023
Referência:  11 / 15
Afluência
[m³/s] 20 40 60 80 100 150 200 300 400 500 600 700 800 900 1000
NAR [m] Defluência Indicada (Vazão Turbinada + Vazão Vertida) [m3/s]
843,86 0 0 0 0 0 0 5 42 89 144 203 266 331 399 468
843,87 0 0 0 0 0 0 5 42 90 145 205 268 333 401 470
843,88 0 0 0 0 0 0 6 43 91 146 206 269 335 403 472
843,89 0 0 0 0 0 0 6 44 92 148 207 271 336 404 474
843,90 0 0 0 0 0 0 6 44 93 149 209 272 338 406 476
843,91 0 0 0 0 0 0 7 45 94 150 210 274 340 408 478
843,92 0 0 0 0 0 0 8 46 96 151 212 275 342 410 480
843,93 0 0 0 0 0 0 8 47 96 153 213 277 343 411 482
843,94 0 0 0 0 0 0 8 48 98 154 214 278 345 413 484
843,95 0 0 0 0 0 0 9 48 99 155 216 280 346 415 486
843,96 0 0 0 0 0 0 9 49 99 156 217 282 348 417 488
843,97 0 0 0 0 0 0 10 50 101 157 219 283 350 419 490
843,98 0 0 0 0 0 0 10 51 102 159 220 285 352 421 492
843,99 0 0 0 0 0 0 11 51 103 160 222 286 353 423 494
844,00 0 0 0 0 0 0 11 52 104 161 223 288 355 425 496
844,01 0 0 0 0 0 0 12 53 105 163 225 290 357 427 498
844,02 0 0 0 0 0 0 12 54 106 164 226 291 359 429 500
844,03 0 0 0 0 0 0 13 55 107 165 228 293 361 431 503
844,04 0 0 0 0 0 0 14 56 108 167 229 295 363 433 505
844,05 0 0 0 0 0 0 14 57 110 168 231 297 365 435 507
844,06 0 0 0 0 0 0 15 58 111 170 233 299 367 437 509
844,07 0 0 0 0 0 1 15 59 112 171 234 300 369 439 512
844,08 0 0 0 0 0 1 16 60 113 173 236 302 371 442 514
844,09 0 0 0 0 0 1 17 61 114 174 238 304 373 444 516
844,10 0 0 0 0 0 2 17 62 115 175 239 306 375 446 519
844,11 0 0 0 0 0 2 18 63 117 177 241 308 377 448 521
844,12 0 0 0 0 0 2 18 63 118 178 242 310 379 450 523
844,13 0 0 0 0 0 3 19 65 119 180 244 311 381 452 525
844,14 0 0 0 0 0 3 20 66 121 181 246 313 383 455 528
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia dos Rios Capivari e Cachoeira CD-OR.AE.CAP 04 2.3. 04/09/2023
Referência:  12 / 15
Afluência
[m³/s] 20 40 60 80 100 150 200 300 400 500 600 700 800 900 1000
NAR [m] Defluência Indicada (Vazão Turbinada + Vazão Vertida) [m3/s]
844,15 0 0 0 0 0 3 20 66 122 183 247 315 385 457 530
844,16 0 0 0 0 0 4 21 68 123 184 249 317 387 459 533
844,17 0 0 0 0 0 4 22 68 124 186 251 319 389 462 535
844,18 0 0 0 0 0 5 23 70 126 187 253 321 392 464 538
844,19 0 0 0 0 0 5 23 71 127 189 254 323 394 466 540
844,20 0 0 0 0 0 6 24 72 128 191 256 325 396 468 543
844,21 0 0 0 0 0 6 24 73 130 192 258 327 398 471 545
844,22 0 0 0 0 0 7 25 74 131 194 260 329 400 473 548
844,23 0 0 0 0 0 7 26 75 133 195 262 331 403 476 550
844,24 0 0 0 0 0 8 27 76 134 197 264 333 405 478 553
844,25 0 0 0 0 0 8 28 77 135 199 266 335 407 480 555
844,26 0 0 0 0 0 9 28 78 137 200 267 337 409 483 558
844,27 0 0 0 0 0 9 29 80 138 202 269 339 412 485 561
844,28 0 0 0 0 0 10 30 81 140 204 271 341 414 488 563
844,29 0 0 0 0 0 10 31 82 141 205 273 344 416 490 566
844,30 0 0 0 0 0 11 31 83 142 207 275 346 419 493 568
844,31 0 0 0 0 0 12 32 84 144 209 277 348 421 495 571
844,32 0 0 0 0 0 12 33 85 146 211 279 350 423 498 574
844,33 0 0 0 0 0 13 34 87 147 212 281 352 426 501 577
844,34 0 0 0 0 0 13 35 88 149 214 283 355 428 503 580
844,35 0 0 0 0 0 14 36 89 150 216 285 357 430 506 582
844,36 0 0 0 0 0 15 37 90 152 218 287 359 433 508 585
844,37 0 0 0 0 0 15 38 92 153 220 289 362 435 511 588
844,38 0 0 0 0 0 16 39 93 155 222 291 364 438 514 591
844,39 0 0 0 0 1 17 39 94 156 223 293 366 440 517 594
844,40 0 0 0 0 1 17 40 96 158 225 296 368 443 519 597
844,41 0 0 0 0 1 18 41 97 160 227 298 371 446 522 600
844,42 0 0 0 0 1 19 42 98 162 229 300 373 448 525 603
844,43 0 0 0 0 2 20 43 100 163 231 302 376 451 528 606
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia dos Rios Capivari e Cachoeira CD-OR.AE.CAP 04 2.3. 04/09/2023
Referência:  13 / 15
Afluência
[m³/s] 20 40 60 80 100 150 200 300 400 500 600 700 800 900 1000
NAR [m] Defluência Indicada (Vazão Turbinada + Vazão Vertida) [m3/s]
844,44 0 0 0 0 2 20 44 101 165 233 305 378 454 531 609
844,45 0 0 0 0 3 21 45 102 167 235 307 381 456 534 612
844,46 0 0 0 0 3 22 46 104 168 237 309 383 459 537 615
844,47 0 0 0 0 4 23 48 105 170 239 311 386 462 539 618
844,48 0 0 0 0 4 24 48 107 172 241 314 388 465 543 621
844,49 0 0 0 0 5 25 49 108 174 243 316 391 467 546 625
844,50 0 0 0 0 5 25 51 110 176 246 318 394 470 549 628
844,51 0 0 0 0 6 26 52 112 178 248 321 396 473 552 632
844,52 0 0 0 1 6 27 53 113 180 250 324 399 476 555 635
844,53 0 0 0 1 7 28 54 115 181 252 326 402 480 558 639
844,54 0 0 0 1 7 29 55 117 183 255 329 405 483 562 642
844,55 0 0 0 2 8 30 57 118 186 257 332 408 486 565 646
844,56 0 0 0 2 9 31 58 120 188 260 334 411 489 569 649
844,57 0 0 0 3 9 32 59 121 190 262 337 414 492 572 653
844,58 0 0 0 3 10 33 61 123 192 265 340 417 496 576 657
844,59 0 0 0 4 11 34 62 125 194 267 342 420 499 579 661
844,60 0 0 0 4 12 35 64 127 196 270 345 423 503 583 665
844,61 0 0 0 5 12 36 65 129 199 272 348 426 506 587 668
844,62 0 0 0 5 13 37 66 131 201 275 351 430 509 591 673
844,63 0 0 0 6 14 39 68 133 203 278 354 433 513 594 677
844,64 0 0 0 6 15 40 69 135 206 280 357 436 517 598 681
844,65 0 0 1 7 15 41 71 137 208 283 361 440 520 602 685
844,66 0 0 1 8 16 42 72 139 211 286 363 443 524 606 689
844,67 0 0 2 8 17 44 74 141 213 289 367 447 528 610 693
844,68 0 0 2 9 18 45 75 143 216 292 370 450 531 614 698
844,69 0 0 3 10 19 46 77 145 218 295 373 454 536 618 702
844,70 0 0 3 11 20 48 79 147 221 298 377 458 540 623 707
844,71 0 0 4 11 21 49 80 150 224 301 380 461 544 627 711
844,72 0 0 4 12 22 50 82 152 226 304 384 465 548 632 716
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia dos Rios Capivari e Cachoeira CD-OR.AE.CAP 04 2.3. 04/09/2023
Referência:  14 / 15
Afluência
[m³/s] 20 40 60 80 100 150 200 300 400 500 600 700 800 900 1000
NAR [m] Defluência Indicada (Vazão Turbinada + Vazão Vertida) [m3/s]
844,73 0 0 5 13 23 52 84 154 229 307 388 469 552 636 721
844,74 0 0 6 14 24 54 86 157 232 311 391 473 556 641 726
844,75 0 0 6 15 25 55 88 159 235 314 395 477 561 646 731
844,76 0 0 7 16 26 56 90 162 238 318 399 481 565 651 736
844,77 0 1 8 17 28 58 92 165 241 321 403 486 570 655 742
844,78 0 1 8 18 29 60 94 167 244 325 407 490 575 661 747
844,79 0 1 9 19 30 62 96 170 248 329 411 495 580 666 752
844,80 0 2 10 20 32 63 98 173 251 332 415 500 585 671 758
844,81 0 3 11 21 33 66 101 176 255 336 420 504 590 677 764
844,82 0 3 12 23 35 67 103 179 258 340 424 509 596 682 770
844,83 0 4 13 24 36 69 105 182 262 345 429 514 601 688 776
844,84 0 5 14 26 38 71 108 185 266 349 434 520 606 694 783
844,85 0 6 15 27 39 74 110 189 270 353 439 525 612 700 789
844,86 0 6 16 28 41 76 113 192 274 358 444 531 619 707 796
844,87 0 7 18 30 43 78 116 196 278 363 449 537 625 714 803
844,88 0 8 19 31 45 81 119 199 283 368 455 543 631 721 811
844,89 1 9 21 33 47 84 122 204 287 373 461 549 639 728 819
844,90 1 10 22 35 49 86 126 208 292 379 467 556 646 736 827
844,91 2 11 24 37 51 89 129 212 298 385 474 563 653 744 835
844,92 2 13 25 39 54 92 133 217 303 391 481 571 662 753 844
844,93 3 14 27 42 56 96 137 222 309 398 488 579 670 762 854
844,94 4 16 29 44 60 100 141 228 316 405 496 587 680 772 865
844,95 5 18 32 47 63 104 146 233 323 414 505 597 690 783 876
844,96 6 20 35 50 67 109 152 240 331 422 515 608 701 795 889
844,97 8 22 38 54 71 114 158 248 340 433 526 620 714 809 904
844,98 10 25 42 59 76 120 165 257 351 445 539 634 730 825 921
844,99 13 29 47 65 83 129 175 270 365 461 557 653 750 847 941
845,00 (máx.) 20 40 60 80 100 150 200 300 400 500 600 700 800 900 1000
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.11
 Cadastro de Informações Operacionais Código Revisão Item Vigência
Cadastro de Informações Operacionais Hidráulicas da 
Bacia dos Rios Capivari e Cachoeira CD-OR.AE.CAP 04 2.3. 04/09/2023
Referência:  15 / 15
6.1.2.Operação Considerando Sobrecarga Induzida até a Cota 846,80 m (Representação Gráfica)
Legenda – Valores de Vazão Afluente (m3/s):
7. VOLUMES DE ESPERA PARA O CONTROLE DE CHEIAS
Não se aplica, devido à inexistência de reservatório com controle de cheias nesta Bacia Hidrográfica.

