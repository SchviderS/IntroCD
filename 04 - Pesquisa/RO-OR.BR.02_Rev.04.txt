Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.13
Rotina Operacional
Teleconferência da Operação Hidráulica para Controle de Cheias nas Bacias Hidrográficas 
do SIN
Código Revisão Item Vigência
RO-OR.BR.02 04 4.2.2. 22/06/2022
.
MOTIVO DA REVISÃO
Atualização e correção do documento, conforme atualização dos Procedimentos de Rede.
- LISTA DE DISTRIBUIÇÃO:
CNOS COSR-NCO COSR-NE COSR-S COSR-SE
Agentes de 
Operação 
Hidráulica
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Teleconferência da Operação Hidráulica para Controle 
de Cheias nas Bacias Hidrográficas do SIN RO-OR.BR.02 04 4.2.2. 22/06/2022
Referência: 2 / 9
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. REFERÊNCIAS ...................................................................................................................................3
3. CONCEITOS ......................................................................................................................................3
4. CONSIDERAÇÕES GERAIS .................................................................................................................3
5. DESCRIÇÃO DO PROCESSO ...............................................................................................................4
6. ATRIBUIÇÕES DOS AGENTES E DO ONS ............................................................................................5
7. ANEXO 1 - MODELO PARA O CADASTRO DE CONTATOS DAS ÁREAS ENVOLVIDAS NA OPERAÇÃO 
HIDRÁULICA DAS BACIAS HIDROGRÁFICAS ......................................................................................6
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Teleconferência da Operação Hidráulica para Controle 
de Cheias nas Bacias Hidrográficas do SIN RO-OR.BR.02 04 4.2.2. 22/06/2022
Referência: 3 / 9
1. OBJETIVO
Estabelecer os procedimentos para realização de teleconferência com participação da Gerência de Recursos 
Hídricos e Meteorologia do ONS (PRH) e dos Agentes de Geração envolvidos visando à troca de informações 
sobre a operação integrada dos sistemas de reservatórios para controle de cheias das bacias hidrográficas 
constantes no Plano Anual de Prevenção de Cheias - PAPC.
2. REFERÊNCIAS
Submódulo 2.5 dos Procedimentos de Rede – Critérios para Operação;
Submódulo 5.5 dos Procedimentos de Rede – Operação hidráulica de Reservatórios.
3. CONCEITOS
Não se aplica.
4. CONSIDERAÇÕES GERAIS
4.1. Durante o período de controle de cheias da bacia, sempre que houver necessidade de nivelamento 
de informações para o controle de cheias será realizada teleconferência entre a Gerência de Recursos 
Hídricos e Meteorologia do ONS (PRH) e Agentes envolvidos, para realizar a troca de informações e 
análise conjunta das condições hidráulicas e hidrológicas e da política de operação a ser adotada.
4.2. Os procedimentos para teleconferência da operação hidráulica, descritos nesta Rotina Operacional, 
se aplicam às bacias com sistemas de reservatórios para controle de cheias incluídos do PAPC, nos 
seus respectivos períodos de controle de cheias, conforme quadro abaixo:
Bacias incluídas no PAPC
Período de 
Controle de 
Cheias
Centro responsável 
pela coordenação da 
operação da bacia
Centro responsável 
pela operação da bacia
Bacia do rio Paraná até Porto 
São José
Novembro / 
Abril CNOS CNOS / COSR-SE / 
COSR-NCO
Bacia do rio Paranapanema Maio / Outubro COSR-SE CNOS / COSR-SE
Bacia do rio Paraíba do Sul Novembro / 
Abril COSR-SE CNOS / COSR-SE
Bacia do rio São Francisco Outubro / Maio CNOS CNOS / COSR-SE / 
COSR-NE (*)
Bacia do rio Parnaíba Outubro / Maio COSR-NE CNOS / COSR-NE
Bacia do rio Iguaçu Maio / Outubro COSR-S CNOS / COSR-S
Bacia do rio Jacuí Novembro / 
Outubro COSR-S CNOS / COSR-S
Bacia do rio Jequitinhonha Outubro / Maio COSR-SE CNOS / COSR-SE
(*) A coordenação da operação em tempo real dos reservatórios do sistema denominado Três Marias no alto 
São Francisco e Queimado no rio Preto é de responsabilidade do COSR-SE. A coordenação da operação em 
tempo real dos reservatórios do sistema denominado Sobradinho e Itaparica, no submédio São Francisco, é 
de responsabilidade do COSR-NE.
Para as bacias onde não está estabelecido o PAPC, pode existir a necessidade de teleconferência em função 
de condições de operação especiais.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Teleconferência da Operação Hidráulica para Controle 
de Cheias nas Bacias Hidrográficas do SIN RO-OR.BR.02 04 4.2.2. 22/06/2022
Referência: 4 / 9
4.3. Os procedimentos para teleconferência da operação hidráulica, descritos nesta Rotina Operacional, 
poderão ser aplicados fora do período de controle de cheias para as bacias relacionadas no item 2.2, 
bem como às demais bacias hidrográficas durante o ano todo, quando as condições de operação 
hidráulica da bacia assim o exigirem.
4.4. Durante a fase de programação a coordenação para controle de cheias é executada pela Gerência de 
Recursos Hídricos e Meteorologia do ONS (PRH), cabe ao Centro de Operação do ONS, citado acima, 
a coordenação da operação do controle de cheias em tempo real das respectivas bacias.
4.5. A teleconferência poderá ser solicitada pela área do ONS responsável pela coordenação do controle 
de cheias da bacia em tempo real ou durante a fase de programação, pelos agentes de geração ou 
por algum outro órgão envolvido na operação hidráulica da bacia, sempre que as condições de 
operação hidráulica da bacia indicarem a necessidade. 
4.6. Na fase de programação, a teleconferência será coordenada pela Gerência de Recursos Hídricos e 
Meteorologia do ONS (PRH). Durante a operação em tempo real a teleconferência será coordenada 
pelo Centro de Operação do ONS responsável pela coordenação da bacia.
4.7. O modelo para o cadastro de contatos, contendo nomes e telefones dos participantes da 
teleconferência, é apresentado no anexo 1. A Gerência de Recursos Hídricos e Meteorologia do ONS 
(PRH) é responsável por manter o cadastro atualizado, com base nas informações fornecidas pelas 
demais áreas integrantes da teleconferência.
4.8. As áreas envolvidas na operação da bacia devem informar à Gerência de Recursos Hídricos e 
Meteorologia do ONS (PRH) qualquer alteração nos contatos, inclusão ou exclusão de participantes.
4.9. Sempre que houver alteração no cadastro de contatos, a Gerência de Recursos Hídricos e 
Meteorologia do ONS (PRH) deverá encaminhar a nova lista para todas as áreas envolvidas no 
controle de cheias da bacia.
4.10. Pelo menos um representante de cada área deverá participar da teleconferência.
5. DESCRIÇÃO DO PROCESSO
5.1. A teleconferência deverá contemplar o seguinte conteúdo:
5.1.1. Explanação, pela Gerência de Recursos Hídricos e Meteorologia do ONS (PRH) e da área de 
meteorologia dos Agentes, sobre as condições de tempo e precipitação previstas para a bacia;
5.1.2. Explanação, pela Gerência de Recursos Hídricos e Meteorologia do ONS (PRH), sobre a operação 
hidráulica realizada no dia anterior, bem como sobre a existência de eventuais restrições temporárias 
que devam ser consideradas na elaboração do PDF do(s) dia(s) seguinte(s); 
5.1.3. Explanação, pelos Agentes, sobre a operação hidráulica realizada nos seus reservatórios com enfoque 
local, bem como sobre a existência de novas restrições que devam ser consideradas na elaboração 
do PDF do(s) dia(s) seguinte(s);
5.2. Análise conjunta do quadro hidrometeorológico vigente, visando identificar:
5.2.1. A situação de operação verificada (Energética / Normal para Controle de Cheias / em Emergência 
para Controle de Cheias);
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Teleconferência da Operação Hidráulica para Controle 
de Cheias nas Bacias Hidrográficas do SIN RO-OR.BR.02 04 4.2.2. 22/06/2022
Referência: 5 / 9
5.2.2. As políticas de operação hidráulica dos sistemas de reservatórios a serem adotadas na 
reprogramação da operação para as horas seguintes, em função do estado de armazenamento dos 
reservatórios e das previsões de afluências; 
5.2.3. A necessidade de reprogramação do PDFc vigente; 
5.2.4. A necessidade de ajustes em procedimentos de programação ou operação, e consequentemente 
revisão na documentação operativa;
5.2.5. A necessidade de flexibilização do volume de espera para o dia corrente e para o(s) dia(s) seguinte(s);
5.2.6. A política de operação hidráulica a ser adotada na elaboração do PDF, do(s) dia(s) seguinte(s) em 
função do estado de armazenamento dos reservatórios e das previsões de afluência para o(s) dia(s) 
seguinte(s).
5.3. A teleconferência deverá iniciar às 09h30min dos dias úteis (horário oficial de Brasília) e em caso de 
necessidade, a teleconferência poderá ser realizada a qualquer hora através de acerto das áreas 
envolvidas.
6. ATRIBUIÇÕES DOS AGENTES E DO ONS
6.1. O enlace de comunicação para o estabelecimento da teleconferência entre o Centro de Operação do 
ONS, demais áreas do ONS envolvidas e Agentes de Geração, deve ser gerada a partir da Gerência de 
Recursos Hídricos e Meteorologia do ONS (PRH), quando se tratar de coordenação da programação 
da operação hidráulica da bacia.
6.2. O enlace de comunicação para o estabelecimento da teleconferência entre o Centro de Operação do 
ONS, demais áreas do ONS envolvidas e Agentes de Geração, deve ser gerada a partir do Centro de 
Operação responsável pela coordenação da operação da bacia, quando se tratar da operação 
hidráulica em tempo real.
6.3. Áreas participantes da teleconferência
ÁREAS ENVOLVIDAS NO CONTROLE DE CHEIAS DA BACIA
Centro de Operação do ONS Tempo Real
PRH Área de Recursos Hídricos e Meteorologia.
AGENTE 1 Área de Hidrologia
AGENTE 2 Área de Hidrologia
AGENTE 3 Área de Hidrologia
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Teleconferência da Operação Hidráulica para Controle 
de Cheias nas Bacias Hidrográficas do SIN RO-OR.BR.02 04 4.2.2. 22/06/2022
Referência: 6 / 9
7. ANEXO 1 - MODELO PARA O CADASTRO DE CONTATOS DAS ÁREAS ENVOLVIDAS NA OPERAÇÃO 
HIDRÁULICA DAS BACIAS HIDROGRÁFICAS 
CADASTRO DE CONTATOS DAS ÁREAS ENVOLVIDAS NA OPERAÇÃO HIDRÁULICA DA BACIA DO RIO XXX
ÁREA NOME TELEFONES E-MAIL
Comercial (xx) xxxx-xxxx
Rede SINSC
Fax
Celular
Residencial
Comercial
Rede SINSC
Fax
Celular
APOIO TÉCNICO
CNOS 
Residencial
Comercial
Rede SINSC
Fax
Celular
Residencial
Comercial
Rede SINSC
Fax
Celular
APOIO TÉCNICO
COSR-XX
Residencial
APO
PRH Comercial
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Teleconferência da Operação Hidráulica para Controle 
de Cheias nas Bacias Hidrográficas do SIN RO-OR.BR.02 04 4.2.2. 22/06/2022
Referência: 7 / 9
CADASTRO DE CONTATOS DAS ÁREAS ENVOLVIDAS NA OPERAÇÃO HIDRÁULICA DA BACIA DO RIO XXX
ÁREA NOME TELEFONES E-MAIL
Rede SINSC
Fax
Celular
Residencial
Comercial
Rede SINSC
Fax
Celular
IO 
TÉC
NIC
O Residencial
Comercial
Rede SINSC
Fax
Celular
Residencial
Comercial
Rede SINSC
Fax
Celular
APOIO TÉCNICO
AGENTE 1
Residencial
Comercial
Rede SINSC
APOIO TÉCNICO
AGENTE 2
Fax
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Teleconferência da Operação Hidráulica para Controle 
de Cheias nas Bacias Hidrográficas do SIN RO-OR.BR.02 04 4.2.2. 22/06/2022
Referência: 8 / 9
CADASTRO DE CONTATOS DAS ÁREAS ENVOLVIDAS NA OPERAÇÃO HIDRÁULICA DA BACIA DO RIO XXX
ÁREA NOME TELEFONES E-MAIL
Celular
Residencial
Comercial
Rede SINSC
Fax
Celular
Residencial
Comercial
Rede SINSC
Fax
Celular
Residencial
Comercial
Rede SINSC
Fax
Celular
APOIO TÉCNICO
AGENTE 3
Residencial
Comercial
Rede SINSC
Fax
Celular
TEMPO REAL
CNOS
ENGENHEIROS E
OPERADORES
Residencial
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Teleconferência da Operação Hidráulica para Controle 
de Cheias nas Bacias Hidrográficas do SIN RO-OR.BR.02 04 4.2.2. 22/06/2022
Referência: 9 / 9
CADASTRO DE CONTATOS DAS ÁREAS ENVOLVIDAS NA OPERAÇÃO HIDRÁULICA DA BACIA DO RIO XXX
ÁREA NOME TELEFONES E-MAIL
Comercial
Rede SINSC
Fax
Celular
TEMPO REAL
COSR-XX OPERADORES
Residencial
Comercial
Rede SINSC
Fax
Celular
TEMPO REAL
AGENTE 1 OPERADORES
Residencial
Comercial
Rede SINSC
Fax
Celular
TEMPO REAL
AGENTE 2 OPERADORES
Residencial
Comercial
Rede SINSC
Fax
Celular
TEMPO REAL
AGENTE 3 OPERADORES
Residencial
