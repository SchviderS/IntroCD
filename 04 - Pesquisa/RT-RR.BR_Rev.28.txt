Manual de Procedimentos da Operação
 Referência Técnica
Elaboração de Instruções de Operação para a Recomposição do Sistema após Perturbação
Código Revisão Item Vigência
RT-RR.BR 28 7.5. 06/07/2023
.
MOTIVO DA REVISÃO:
- Exclusão de frase em função de repetição, na segunda tabela do subitem 2.1, página 14.
- Atualizado o item 3.2; 3.3 e 4 (da IO de recomposição) melhorando a indicação e responsabilidades no 
fechamento de paralelo
- Inclusão dos subitens 3.2.9, 3.2.10 e 3.3.8, complementação do subitem 3.3.4 e inclusão do passo 4 do 
exemplo do subitem 4.1 da IO de Recomposição Fluente e Coordenada da Área. 
- Inclusão dos subitens 3.2.8, 3.2.9 e 3.3.4, complementação do subitem 3.3.3 e inclusão da orientação da 
obrigatoriedade de constar as condições para fechamento de paralelo nos procedimentos de fechamento 
de paralelo nos passos 1 e 2 do exemplo do subitem 4 da IO de Recomposição de Interligação entre Áreas e 
de uma Região ou de Interligação entre Regiões.
- Melhoria no texto dos itens 2, 3 e 4 da RT
LISTA DE DISTRIBUIÇÃO:
CNOS COSR-NCO COSR-NE COSR-S COSR-SE
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 2/ 22
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. PREMISSAS ......................................................................................................................................3
3. ESTRUTURA DAS INSTRUÇÕES DE OPERAÇÃO ..................................................................................4
4. MODELOS DAS INSTRUÇÕES ............................................................................................................4
4.1. Modelo de Instrução de Recomposição Fluente e Coordenada da Área (nome da usina de 
autorrestabelecimento do corredor de recomposição) ................................................................4
4.2. Modelo de Instrução de Recomposição de Interligações entre Áreas de uma Região ou de 
Interligações entre Regiões .........................................................................................................13
5. ANEXOS DE DIAGRAMAS PARA UTILIZAÇÃO NOS MODELOS DE INSTRUÇÃO DE RECOMPOSIÇÃO ..21
ANEXO – Modelo de Diagrama Unifilar Simplificado / Fluxograma ......................................................21
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 3/ 22
1. OBJETIVO
Estabelecer o documento de referência para elaboração das Instruções de Operação para 
recomposição do sistema, nas fases fluente e coordenada, visando a estruturação e padronização 
dessas instruções.
2. PREMISSAS
2.1. Os procedimentos constantes nas Instruções de Operação para Recomposição devem estar de acordo 
com as responsabilidades, premissas, diretrizes e critérios gerais definidos nos Procedimentos de 
Rede.
2.2. O detalhamento de cada Instrução de Operação deve ser feito de forma a assegurar sua aderência 
aos procedimentos operativos vigentes.
2.3. Instruções de Operação devem ser simples e objetivas, evitando linguagem pesada e textos longos 
procurando manter seus procedimentos descritos em formato tabular.
2.4. A padronização adotada para as Instruções de Operação deve ser aplicável a toda Rede de Operação, 
definindo o padrão ONS para este processo.
2.5. Os itens das instruções de Operação para os quais não existem procedimentos devem ter seus títulos 
mantidos e com a mesma numeração ditada pela referência técnica, com o texto “Não se aplica” na 
linha seguinte.
2.6. Os textos destacados em cor verde, constantes nos itens da estrutura da instrução de operação não 
devem fazer parte dessas instruções, pois se referem apenas a orientações para elaboração dos 
respectivos documentos.
2.7. Os exemplos desta Referência Técnica destacados em itálico, constantes nos itens da estrutura da 
Instrução de Operação para Recomposição, servem como exemplos orientativos para o uso da 
fraseologia e utilização de textos simples e objetivos.
2.8. A fraseologia adotada deve ser adequada com aquela estabelecida na Rotina Operacional RO-
RO.BR.01 – Comunicação Verbal na Operação. 
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 4/ 22
3. ESTRUTURA DAS INSTRUÇÕES DE OPERAÇÃO
3.1. Podem ser elaboradas Instruções de Operação de Recomposição para as seguintes situações:
instrução de operação de recomposição fluente da Área (nome da usina de autorrestabelecimento 
do corredor de recomposição) (Item 5.1);
instrução de operação de recomposição de interligação das Áreas da Região (nome da região 
geoelétrica) ou entre Regiões (Item 5.2).
3.2. O preenchimento das tabelas na instrução de operação deve seguir as orientações definidas na 
Referência Técnica RT-MP.BR.02 - Padronização de Redação e Estilo dos Documentos do MPO.
3.3. Os procedimentos da fase fluente da área de recomposição devem ser apresentados em sequência 
temporal mais provável ou, caso seja conveniente, por blocos de procedimentos por instalação. 
De modo a destacar procedimentos importantes, devem ser utilizadas cores, conforme constante 
em cada modelo desta Referência Técnica. 
3.4. Deverá ser incluído no corpo da instrução de operação um diagrama unifilar simplificado, conforme 
modelos anexos, de forma a proporcionar ao usuário uma visão geral da Área de Recomposição 
(Modelo 1 desta Referência Técnica). 
Opcionalmente poderá ser incluído um fluxograma apresentando uma sequência lógica da 
recomposição ou diagrama unifilar simplificado, conforme modelo em anexo desta Referência 
Técnica. 
As cores dos equipamentos, representados nos diagramas e fluxogramas, devem obedecer as cores 
definidas para cada nível de tensão, conforme Referência Técnica RT-MP.BR.03 - Elaboração de 
Diagramas Unifilares Operacionais do ONS. Podem ser utilizados conjuntos de equipamentos com 
fundo em cores distintas para caracterizar, no diagrama e no fluxograma, uma sequência de 
procedimentos específicos. 
Essas cores também podem ser utilizadas nos respectivos passos na tabela de procedimento de 
recomposição.
4. MODELOS DAS INSTRUÇÕES
Existem os seguintes modelos para as instruções de recomposição:
1 – Modelo de Instrução de Recomposição Fluente e Coordenada da Área (nome da usina de 
autorrestabelecimento do corredor de recomposição);
2 – Modelo de Instrução de Recomposição de Interligação entre Áreas de uma Região (nome da região 
geoelétrica) ou de Interligação entre Regiões.
4.1. MODELO DE INSTRUÇÃO DE RECOMPOSIÇÃO FLUENTE E COORDENADA DA ÁREA (NOME DA USINA 
DE AUTORRESTABELECIMENTO DO CORREDOR DE RECOMPOSIÇÃO)
A denominação do corredor de recomposição fluente deve ser preferencialmente o nome da usina de 
autorrestabelecimento responsável pelo início do processo de recomposição.
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 5/ 22
Exemplo: Recomposição da Área Itaipu.
Item 1. OBJETIVO
Estabelecer os procedimentos operativos a serem seguidos pelos Operadores de Sistema do CNOS, 
pelos Operadores de Sistema do COSR-X e pelos Centros de Operação dos Agentes envolvidos, para 
recomposição da Área (nome da usina de autorrestabelecimento do corredor de recomposição), de 
acordo com os Procedimentos de Rede.
Item 2. CONSIDERAÇÕES GERAIS
2.1. A Recomposição da Área (nome da usina de autorrestabelecimento do corredor de recomposição) 
abrange as instalações a seguir:
SE A SE B SE C SE D SE E SE F
SE G SE H SE I SE J SE K SE L
UHE A UHE B UHE C UHE D UTE A UTE B
Obs.: Células cujas instalações estão com fundo na cor azul claro fazem parte da fase fluente da 
recomposição e as instalações com fundo na cor laranja clara fazem parte da fase coordenada. 
Todas as instalações devem constar em ordem alfabética, iniciando pelas subestações e seguidas pelas 
usinas.
2.2 O COSR-X deve caracterizar para o Agente que a recomposição das instalações será efetuada sob 
coordenação desse Centro de Operação nas seguintes situações:
a) não for possível seguir a recomposição de forma fluente, conforme os procedimentos constantes 
nesta Instrução de Operação, quer seja pela configuração resultante, quer seja pela indisponibilidade 
de equipamentos; 
ou
b) não existir procedimentos, a serem executados com autonomia pelos agentes, para recomposição 
das instalações afetadas.
Incluir o item 2.3 a seguir nos casos em que nos procedimentos não esteja caracterizada autorização para os 
Agentes restabelecerem os equipamentos fora da rede de operação:
2.3 Quando da existência de equipamentos fora da rede de operação conectados a barramentos da rede 
de operação, o COSR-X deve autorizar ao Agente o restabelecimento desses equipamentos, conforme 
procedimentos próprios dos agentes, tão logo as condições o permitam.
2.4 Os textos em destaque nesta Instrução de Operação objetivam:
texto em negrito na cor vermelha: montante de carga, fechamento de paralelo e fechamento de 
anel;
texto em negrito na cor preta: condições do tipo tensão de energização, posição de comutadores 
sob carga e fluxo de potência ativa;
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 6/ 22
texto na cor azul: passos com procedimentos alternativos;
fundo na cor verde: na fase coordenada, quando houver delegação de autonomia para o Agente, 
nos passos em que o ONS assume a coordenação e o controle do procedimento, as colunas “Passo”, 
“Coordenação”, “Controle” e “Comando / Execução”, opcionalmente, podem constar com fundo 
nessa cor;
fundo colorido de tom claro: para dar destaque, caso seja conveniente, a algum procedimento ou 
conjunto de procedimentos.
Incluir apenas os itens acima aplicáveis.
Incluir demais considerações gerais específicas deste Corredor, caso sejam pertinentes.
Item 3. PROCEDIMENTOS GERAIS
Item 3.1 Para os Operadores de Sistema do CNOS
3.1.1. Supervisionar as ações de recomposição na fase fluente, monitorando as ações e a evolução das 
principais grandezas, intervindo no processo e alterando-o quando julgar necessário.
3.1.2. Supervisionar as ações de recomposição da Área na fase coordenada, intervindo no processo e 
alterando-o quando julgar necessário ou quando solicitado.
3.1.3. Coordenar a operação do Controle Automático de Geração – CAG durante toda a recomposição da 
Rede de Operação sob responsabilidade do Operador Nacional do Sistema Elétrico – ONS.
Item 3.2. Para os Operadores de Sistema do COSR-X
3.2.1. Supervisionar as ações de recomposição na fase fluente, monitorando as ações e a evolução das 
principais grandezas, intervindo no processo e alterando-o quando julgar necessário ou quando 
solicitado pelos centros de operação dos agentes de operação. 
3.2.2. Controlar as faixas de tensão e de frequência constantes nas tabelas a seguir:
Tensão Nominal do 
Barramento (kV) Faixa de Tensão (kV)
765 690 a 800
525 472 a 550 
500 450 a 550 
440 396 a 484 
345 311 a 380
230 207 a 242
138 124 a 145
88 79 a 92
69 62 a 72
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 7/ 22
Incluir na tabela apenas os níveis de tensão existentes na Área.
Faixa de Frequência (Hz)
Fase Fluente Fase Coordenada
58 a 62 59 a 61
3.2.3. Coordenar, supervisionar e controlar, na fase coordenada, as ações para recomposição das áreas nas 
suas respectivas áreas de atuação.
3.2.4. Desligar ou manter desligado o Controle Automático de Geração – CAG na fase de recomposição 
fluente, sendo religado conforme procedimento específico para cada área.
Nota: na fase fluente, o controle da frequência será executado pela UHE (nome da usina). 
3.2.5. Informar aos centros de operação dos agentes de operação envolvidos a ocorrência de perturbação e 
o andamento da fase de recomposição que possam influenciar as ações previstas nas instruções de 
operação determinadas para esses agentes.
3.2.6. Caracterizar para o agente de operação quando os procedimentos para recomposição fluente não 
possam ser realizados. Nesses casos, o processo de recomposição será coordenado pelos centros de 
operação do ONS. 
3.2.7. Supervisionar a retomada da carga, que deve ser efetuada gradativamente de maneira evitar eventuais 
oscilações de potência ou de tensão que possam acarretar perturbações durante o processo de 
recomposição.
3.2.8. Coordenar, caso seja necessário, o corte manual de carga durante o processo de recomposição para 
preservar a segurança da Rede de Operação.
Para a determinação do corte de carga, o COSR-X deve considerar a proporção de carga restabelecida 
pelos agentes no momento.
3.2.9. Nos casos em que as condições de tensões para energização de linhas de transmissão ou de 
equipamentos não estejam explicitadas nesta instrução de operação, devem ser observados como 
valores máximos, os valores apresentados no item 3.2.2.
Item 3.3. Para os Agentes de Operação 
3.3.1. Preparar as instalações para recebimento / envio de tensão, efetuando manobras de acordo com as 
instruções específicas.
Desligar ou manter desligados os bancos de capacitores, bem como o modo de comutação automática 
dos comutadores sob carga dos transformadores. 
Ajustar os comutadores sob carga dos transformadores em posições definidas nesta Instrução de 
Operação ou em posições que não impliquem sobretensões no sistema.
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 8/ 22
3.3.2. Supervisionar, comandar e executar as ações de recomposição fluente de suas instalações, bem como 
as ações de restabelecimento das cargas na área sob sua responsabilidade, conforme instruções de 
operação do ONS.
3.3.3. Supervisionar, comandar e executar, na fase coordenada, as ações de recomposição determinadas pelo 
ONS.
3.3.4. Utilizar apenas tensões que atendam às condições de energização determinadas nas instruções de 
operação de suas instalações, com exceção de casos acordados em tempo real pelos agentes de 
operação envolvidos e autorizados pelo centro de operação do ONS. Caso as tensões para as condições 
de energização não estejam explicitadas nas instruções de operação de suas instalações ou não sejam 
acordadas em tempo real, devem ser observados como valores máximos, os valores apresentados no 
item 3.2.2.
3.3.5. Restabelecer a carga prioritária, conforme definido pelo agente em cada fase de recomposição, até o 
limite preestabelecido nas instruções de operação de suas instalações ou pelo COSR-X. As tomadas de 
carga em blocos devem ser defasadas de 1 minuto entre elas.
Na rede de distribuição, os agentes devem ligar bancos de capacitores na medida do necessário, de 
modo a se evitar tensões abaixo dos limites mínimos especificados para os barramentos a partir dos 
quais as cargas são atendidas.
3.3.6. Fazer contato com o COSR-X, informando-o quando detectar alguma anormalidade no processo de 
recomposição fluente ou quando do término das ações sob sua responsabilidade na fase fluente, e 
aguardar autorização para adoção das ações de recomposição para a fase coordenada ou liberação de 
carga adicional.
3.3.7. Desligar ou manter desligado o Estabilizador de Sistema de Potência (PSS) na UHE (nome da usina) (na 
UHE (nome da usina), na UHE (nome da usina)) que fazem parte da fase de recomposição fluente, 
sendo religado conforme procedimento específico para cada área. Incluir apenas quando houver pelo 
menos uma UHE com PSS.
3.3.8. No fechamento de disjuntores, informar ao ONS quando identificar que o fechamento se trata de um 
fechamento de paralelo. Nessa situação, informar a diferença de frequência, tensão e ângulo 
verificado na sua instalação e somente efetuar o fechamento do disjuntor após autorização do ONS. 
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 9/ 22
Item 4. SUPERVISÃO DA RECOMPOSIÇÃO FLUENTE DA ÁREA (NOME DA USINA DE 
AUTORRESTABELECIMENTO DO CORREDOR DE RECOMPOSIÇÃO)
Item 4.1. Recomposição Fluente da Área (nome da usina de autorrestabelecimento do corredor de 
recomposição)
Diagrama unifilar simplificado representando a fase fluente da Área (nome da usina de 
autorrestabelecimento do corredor de recomposição):
Para a recomposição, devem ser seguidas todas as orientações e restrições constantes nos procedimentos 
abaixo. 
O CNOS e o COSR-X deverão supervisionar a recomposição fluente conforme a seguir:
Caso exista algum procedimento intermediário em instalações que necessite de coordenação do COSR-X, 
deve ser explicitada a coordenação referente a essas instalações no respectivo item de controle. 
Ao término da fase fluente e/ou coordenada, remeter aos procedimentos de fechamento de paralelos, anéis 
e tomada adicional de carga, se houver, conforme a instrução de operação de recomposição de regiões.
Passo Executor Procedimento Objetivo / Item de Controle
Partir e sincronizar 2 (duas) unidades 
geradoras.
Manter a tensão na barra de 
geração em 14,3 kV durante a 
fase fluente
Efetuar o controle de frequência 
da área1 UHE XXX
(Agente operador)
Energizar a LT 500 kV XXX / BBB, 
enviando tensão para a SE BBB.
Reatores de linha conectados à 
LT.
SE XXX ≤ 525 kV
2
SE BBB
(Agente operador 
Receber tensão da SE XXX pela LT 
500 kV XXX / BBB e normalizar a SE 
BBB.

Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 10/ 22
Passo Executor Procedimento Objetivo / Item de Controle
Energizar a LT 500 kV BBB / CCC, 
enviando tensão para a SE CCC.
Reatores de linha conectados à LT
SE BBB ≤ 535 kV
Energizar o primeiro 
autotransformador de 500/230 kV – 
300 MVA da SE CCC, 
preferencialmente o AT01. 
SE CCC ≤ 550 kV
LTC na posição 17 ou inferior
Energizar a LT 230 kV BBB / CCC, 
enviando tensão para a SE CCC.
SE BBB ≤ 240 kV
da SE BBB)
Energizar o primeiro 
autotransformador de 230/138 kV – 
150 MVA da SE CCC.
SE CCC ≤ 240 kV
LTC na posição 17 ou inferior
Receber tensão da SE EEE pela LT 138 
kV BBB / EEE, normalizar circuitos e 
energizar a barra de 138 kV.3
SE EEE
(Agente operador 
da SE EEE)
Restabelecer até 25 MW de carga. Tensão mínima de 124 kV.
4
SE BBB
(Agente operador 
da SE BBB)
Energizar a LT 230 kV BBB / CCC, 
enviando tensão para a SE CCC.
SE BBB ≤ 240 kV.
Nota: Na SE CCC, a LT 230 kV BBB / 
CCC é ponto de fechamento entre as 
áreas XXX e YYY.
5
Exemplo 1: Com um atendimento de cerca de xxx MW, correspondente a aproximadamente 80% 
do bloco de carga da fase fluente, os procedimentos de RECOMPOSIÇÃO DAS INTERLIGAÇÕES 
DAS ÁREAS DA REGIÃO NORDESTE constantes da IO-RR.NE, podem ser iniciados.
Exemplo 2: Após a conclusão da fase fluente, o fechamento de paralelos e aneis, o 
reestabelecimento dos demais equipamentos da área, bem como a liberação de cargas 
adicionais, serão efetuados com coordenação e controle do COSR-NCO. Esses procedimentos 
constam na IO-RR.CO.
Obs.: Se a tomada de carga for por blocos, a informação de que o restabelecimento entre esses blocos é 
defasado em 1 minuto deve ser colocada no item de controle do procedimento.
Incluir outros textos que sejam pertinentes à recomposição desta Área.
Item 4.2. Notas Importantes
Exemplo: 4.2.1. Os bancos de capacitores existentes nas subestações devem ser energizados juntamente com 
as cargas, de modo a evitar tensões abaixo da mínima especificada para esses barramentos, conforme valores 
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 11/ 22
apresentados nos passos do subitem 4.1. 4.2.2. O modo de comutação automática dos comutadores sob 
carga dos transformadores deve ser desligado ou mantido desligado, para que, durante o processo de 
recomposição fluente, estes permaneçam na posição especificada nesta Instrução de Operação.
Ou caso não haja, preencher com “não se aplica”
Item 5. RECOMPOSIÇÃO COORDENADA DA ÁREA (NOME DA USINA DE AUTORRESTABELECIMENTO DO 
CORREDOR DE RECOMPOSIÇÃO)
Item 5.1. Procedimentos para complementação da Área (nome da usina de autorrestabelecimento do 
corredor de recomposição) 
Não existem procedimentos para complementação da Área (nome da usina de autorrestabelecimento do 
corredor de recomposição).
OU
Os procedimentos descritos neste item são coordenados e efetuados antes do fechamento de paralelo com 
qualquer outra área. 
Exemplo de tabela:
Passo
Coordenação
Controle
Comando / 
Execução
Procedimento Objetivo / Item de Controle
1
COSR-X
COSR-X
Agente
Energizar o segundo circuito da LT 500 kV A / C Conforme IO-PM.AAA
2
COSR-X
COSR-X
Agente
Energizar segundo transformador 500/138 kV da 
SE C Conforme IO-PM.AAA
3
COSR-X
COSR-X
Agente
Restabelecer o restante das cargas da SE C 138 kV
Item 5.2. Procedimentos Alternativos da Fase Fluente da Área (nome da usina de autorrestabelecimento 
do corredor de recomposição)
Para a Área (nome da usina de autorrestabelecimento do corredor de recomposição) não existem 
procedimentos alternativos.
OU
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 12/ 22
Item 5.2.1. Impossibilidade de Autorrestabelecimento da UHE (nome da usina) 
Passo
Coordenação
Controle
Comando / 
Execução
Procedimento Objetivo / Item de Controle
- O Agente AAA informa ao COSR-X a indisponibilidade da UHE XXX, para que este coordene a realização 
dos seguintes procedimentos:
1
COSR-X
COSR-X
Agente
Sincronizar as demais unidades geradoras da 
UHE ZZZ
- Após a conclusão da 
Recomposição da Área ZZZ.
2
COSR-X
COSR-X
Agente
Energizar a LT 230 kV A / B pelo terminal da SE A Tensão igual ou inferior a 240 kV
3
COSR-X
COSR-X
Agente
Partir e sincronizar as duas unidades geradoras 
da UHE XXX
4
O COSR-X informará o Agente AAA que, após a adoção destes procedimentos, a operação deve voltar ao 
processo de recomposição fluente conforme item 4.1.
Item 5.2.2. Indisponibilidade da LT 500 kV (nome da subestação) / (nome da subestação)
Passo
Coordenação
Controle
Comando / 
Execução
Procedimento Objetivo / Item de Controle
- O Agente AAA informa ao COSR-X a indisponibilidade da LT 500 kV A / B, para que este coordene a 
realização dos seguintes procedimentos:
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 13/ 22
Passo
Coordenação
Controle
Comando / 
Execução
Procedimento Objetivo / Item de Controle
1
COSR-X
COSR-X
Agente
Sincronizar pelo menos 3 (três) unidades 
geradoras na UHE XXX.
A UHE XXX deverá manter a tensão 
de 12,4kV na barra de geração, até 
ordem em contrário do COSR-X
Esta usina somente efetuará o 
controle de frequência da área até 
o fechamento do paralelo com a 
área ZZZ, o qual será informado 
pelo COSR-X.
4
O COSR-X informará o Agente AAA que, após a adoção destes procedimentos, a operação deve voltar ao 
processo de recomposição fluente conforme item 4.1.
Item 6. ANEXOS
Incluir o item 6 caso se julgue necessário.
Exemplo:
Anexo 1. Fluxograma da sequência da recomposição fluente
4.2. MODELO DE INSTRUÇÃO DE RECOMPOSIÇÃO DE INTERLIGAÇÕES ENTRE ÁREAS DE UMA REGIÃO 
OU DE INTERLIGAÇÕES ENTRE REGIÕES 
Este modelo de Instrução de Operação deve ser utilizado para o restabelecimento de equipamentos e linhas 
de transmissão que interligam áreas de uma região ou interligam regiões, a partir de fechamento de paralelo. 
A denominação da Instrução de Operação deve fazer referência à interligação das Áreas da Região RRR. 
No caso de interligação entre Regiões deve considerar as siglas de identificação das regiões.
Exemplos: 
No caso de fechamento de paralelo entre áreas da região Sudeste utilizar IO-RR.SE - Recomposição da Região 
Sudeste.
No caso de interligação entre as regiões Norte e Nordeste utilizar IO-RR.NNE – Recomposição da Interligação 
Norte/Nordeste.
Item 1. OBJETIVO
Estabelecer os procedimentos operativos a serem seguidos pelos Operadores de Sistema do CNOS, 
Operadores de Sistema do COSR-X e pelos Centros de Operação dos Agentes envolvidos, para o 
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 14/ 22
processo de recomposição coordenada de interligação (das Áreas (nome da usina de 
autorrestabelecimento do corredor de recomposição) e (nome da usina de autorrestabelecimento do 
corredor de recomposição) da Região (nome da região geoelétrica)) ou (das Regiões (nome da região 
geoelétrica) e (nome da região geoelétrica)) após desligamento geral, de acordo com os 
Procedimentos de Rede.
Item 2. CONSIDERAÇÕES GERAIS
Para o caso de interligação de áreas utilizar o item 2.1 a seguir:
2.1. Os procedimentos contidos nesta instrução contemplam a recomposição da Região (nome da região 
geoelétrica), no processo coordenado de interligação das Áreas desta Região.
Áreas de Recomposição da Região (nome da região geoelétrica)
Área (nome da usina de 
autorrestabelecimento 
do corredor de 
recomposição)
Área (nome da usina de 
autorrestabelecimento 
do corredor de 
recomposição)
Área (nome da usina de 
autorrestabelecimento 
do corredor de 
recomposição)
Área (nome da usina 
de 
autorrestabelecimento 
do corredor de 
recomposição)
Caso existam corredores de recomposição fora da Rede de Operação que fechem paralelo com o SIN, utilizar 
a tabela a seguir:
Áreas de Recomposição fora da Rede de Operação sem Instruções de Operação do ONS no MPO.
Os procedimentos dessas áreas encontram-se descritos em instruções internas dos agentes.
Estão descritos nesta IO os procedimentos referentes ao fechamento dessas áreas com o SIN quando 
o fechamento for em equipamento da rede de operação. Quando o fechamento for realizado por meio 
de equipamento fora da rede de operação, o agente deve solicitar autorização do COSR-X para o 
fechamento desta área com a rede de operação.
Área (nome da área 
de recomposição do 
Agente)
Área (nome da área 
de recomposição 
do Agente)
Área (nome da 
área de 
recomposição do 
Agente)
Área (nome da 
área de 
recomposição 
do Agente)
Para o caso de interligação de Regiões utilizar o item 2.1 a seguir:
2.1. Os procedimentos contidos nesta Instrução de Operação contemplam a Recomposição da interligação 
entre as Regiões (nome da região geográfica) e (nome da região geográfica) e fechamentos de anéis.
2.2 Os textos em destaque nesta Instrução de Operação objetivam:
texto em negrito na cor vermelha: montante de carga, fechamento de paralelo e fechamento de 
anel;
texto em negrito na cor preta: condições do tipo tensão de energização, posição de comutadores 
sob carga e fluxo de potência ativa;
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 15/ 22
texto na cor azul: passos com procedimentos alternativos;
fundo na cor verde: na fase coordenada, quando houver delegação de autonomia para o Agente, 
nos passos em que o ONS assume a coordenação e o controle do procedimento, as colunas “Passo”, 
“Coordenação”, “Controle” e “Comando / Execução”, opcionalmente, podem constar com fundo 
nessa cor;
fundo colorido de tom claro: para dar destaque, caso seja conveniente, a algum procedimento ou 
conjunto de procedimentos.
Incluir apenas os itens acima aplicáveis.
Incluir demais considerações gerais específicas desta Instrução de Operação, caso sejam pertinentes.
Item 3. PROCEDIMENTOS GERAIS
Item 3.1. Para os Operadores de Sistema do CNOS
3.1.1. Coordenar, supervisionar e controlar as ações para recomposição das interligações entre áreas e 
interligações entre regiões na fase coordenada.
3.1.2. Supervisionar as ações de recomposição de áreas na fase coordenada e as ações para interligações 
entre áreas de autorrestabelecimento, intervindo no processo e alterando-o quando julgar necessário 
ou quando solicitado.
3.1.3 Coordenar a operação do Controle Automático de Geração – CAG durante toda a recomposição da Rede 
de Operação do Operador Nacional do Sistema Elétrico – ONS.
Item 3.2. Para os Operadores de Sistema dos COSR-X e COSR-Y
3.2.1. Coordenar, supervisionar e controlar as ações para interligação entre Áreas de uma Região ou 
interligação entre Regiões.
3.2.2. Controlar a faixa de frequência de 59 Hz a 61 Hz. 
Controlar as faixas de tensão constantes na tabela a seguir. 
Tensão Nominal do 
Barramento (kV) Faixa de Tensão (kV)
765 690 a 800
525 472 a 550 
500 450 a 550 
440 396 a 484 
345 311 a 380 
230 207 a 242
138 124 a 145
88 79 a 92
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 16/ 22
Tensão Nominal do 
Barramento (kV) Faixa de Tensão (kV)
69 62 a 72
Incluir na tabela apenas os níveis de tensão existentes na Área.
3.2.3. Supervisionar e controlar a operação do Controle Automático de Geração – CAG durante toda a 
recomposição da Rede de Operação.
3.2.4. Informar aos centros de operação dos agentes de operação envolvidos a ocorrência de perturbação e 
o andamento da fase de recomposição que possam influenciar as ações previstas nas instruções de 
operação determinadas para esses agentes.
3.2.5. Liberar tomada de carga adicional, em função da disponibilidade de geração e dos limites de 
carregamento em equipamentos e em linhas de transmissão. 
3.2.6. Coordenar, caso seja necessário, o corte manual de carga durante o processo de recomposição para 
preservar a segurança da Rede de Operação.
Para a determinação do corte de carga, o COSR-X deve considerar a proporção de carga restabelecida 
pelos agentes no momento.
3.2.7. Na recomposição coordenada, quando uma ou mais áreas ou região permanecer com frequência 
inferior a 60 Hz em função de desequilíbrio carga x geração que impeça fechamento de paralelo, o 
COSR-X deve determinar o corte de carga na área ou região com frequência mais baixa, para permitir 
o fechamento do paralelo com a área ou região com frequência normal.
3.2.8. Utilizar para as energizações as tensões e outras condições determinadas nesta instrução de operação. 
Caso as tensões para as condições de energização não estejam explicitadas nesta instrução de 
operação, devem ser observados como valores máximos, os valores apresentados no item 3.2.2.
3.2.9. Nos fechamentos de paralelos, confirmar o atendimento às condições de diferença de frequência, 
tensão e ângulo antes de autorizar o fechamento ao agente. 
Item 3.3 Para os Agentes de Operação
3.3.1. Preparar as instalações para o recebimento / ou envio de tensão, efetuando manobras de acordo com 
as instruções específicas.
3.3.2. Supervisionar, comandar e executar, na fase coordenada, as ações de recomposição determinadas pelo 
ONS.
3.3.3 Utilizar apenas tensões que atendam às condições de energização determinadas nas instruções de 
operação de suas instalações, com exceção de casos acordados em tempo real pelos agentes de 
operação envolvidos e autorizados pelo centro de operação do ONS. Caso as tensões para as condições 
de energização não estejam explicitadas nas instruções de operação de suas instalações ou não sejam 
acordadas em tempo real, devem ser observados como valores máximos, os valores apresentados no 
item 3.2.2.
3.3.4 Antes dos fechamentos de paralelos confirmar o atendimento às condições de diferença de frequência, 
tensão e ângulo verificado na sua instalação e somente efetuar o fechamento após autorização do 
ONS. 
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 17/ 22
Item 3.4. Procedimentos para fechamento de paralelo
Quando não estiverem especificadas as condições para fechamento de paralelo, devem ser adotados 
os valores limites a seguir:
Máxima diferença de frequência igual a 0,2 Hz;
Máxima diferença de tensão igual a 10% da tensão nominal;
Máxima defasagem angular igual a 10 graus. 
Item 3.5. Procedimentos para fechamento de anel 
3.5.1. Para as manobras de fechamento em anel entre Áreas a diferença de tensão entre os terminais não 
é restritiva, porém, é obrigatória a verificação das condições de diferença angular entre as tensões 
dos terminais, quando for especificado.
3.5.2. Quando de situações não previstas em Instruções de Operação, o fechamento de anel entre áreas, 
deve ser precedida da avaliação do valor do ângulo entre as tensões terminais, observando-se que:
a) O fechamento do anel poderá ser executado independentemente do ângulo verificado nos 
terminais do disjuntor nas seguintes situações:
Anel constituído por linhas de transmissão de mesmo nível de tensão.
Anel constituído por linhas de transmissão de níveis de tensão diferentes, quando a manobra a 
ser executada for no lado de tensão inferior do transformador.
b) Após tentativa sem sucesso de minimizar a defasagem angular para valor igual ou inferior a 30 
graus, por meio do redespacho de geração das usinas envolvidas, o fechamento só deverá ser 
efetuado, nas seguintes situações:
Anel constituído por linhas de transmissão de níveis de tensão diferentes, quando a manobra a 
ser executada for no lado da tensão mais elevada do transformador.
Manobras interligando duas usinas por meio de uma linha de transmissão.
c) Quando não for possível atender limitações de ângulo para o fechamento de anel constantes 
das Instruções de Operação, o COSR-X deve coordenar o fechamento de anel com qualquer 
ângulo, na ocorrência das seguintes situações:
Carga interrompida.
Equipamentos em sobrecarga inadmissível.
Níveis de confiabilidade críticos.
Níveis de tensão superior / inferior a 10 % da tensão nominal, exceto para o 765 KV, cujo limite 
superior é 4,5 % e para tensões nominais menores ou iguais à 138 KV, cujo limite superior é 5 %.
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 18/ 22
Item 4. PROCEDIMENTOS PARA RECOMPOSIÇÃO DE INTERLIGAÇÕES DAS ÁREAS DA REGIÃO (NOME DA 
REGIÃO GEOELÉTRICA)
Incluir o diagrama indicando o fechamento de paralelo, caso julgar pertinente.
Diagrama unifilar simplificado representando o fechamento do paralelo.
Exemplo:
Passo
Coordenação
Controle
Comando e 
Execução
Procedimento Objetivo / Item de Controle
1
COSR-X
COSR-X
Agente
Ações para o fechamento do paralelo.
Ligar terminal XX da LT fechando o 
paralelo entre as áreas XX e YY
Condições a serem verificadas para o 
fechamento do paralelo:
Incluir as condições de fechamento de 
paralelo mesmo que sejam iguais ao item 3.4 
da seguinte forma:
∆V ≤ 105 kV,
∆ ≤ 10°, e
∆f ≤ 0,2 Hz
2
COSR-X
COSR-X
Agente
Alternativas para o fechamento do 
paralelo.
Condições a serem verificadas para o 
fechamento do paralelo:
Incluir as condições de fechamento de 
paralelo mesmo que sejam iguais ao item 3.4 
da seguinte forma:
∆V ≤ 105 kV,
∆ ≤ 10°, e
∆f ≤ 0,2 Hz

Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 19/ 22
Passo
Coordenação
Controle
Comando e 
Execução
Procedimento Objetivo / Item de Controle
3
COSR-X
COSR-X
Agente
Ações para fechamento de anéis Condições a serem verificadas para o 
fechamento de anéis.
4
COSR-X
COSR-X
Agente
Ações para religar o CAG Condições a serem atendidas.
Item 5. RECOMPOSIÇÃO PARCIAL 
Utilizar este item quando for pertinente.
Exemplo:
Item 5.1. Recomposição do trecho 230 kV entre as SE AAA a SE BBB
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 20/ 22
Passo
Coordenação
Controle
Comando / 
Execução
Procedimento Objetivo / Item de Controle
- O restabelecimento do trecho 230 kV entre as SE AAA e SE CCC será coordenado pelo COSR-X conforme 
procedimentos a seguir:
1
COSR-X
COSR-X
Agente
Energizar o primeiro circuito da LT 230 kV 
AAA / BBB, pelo terminal da SE AAA
SE AAA ≤ 235 kV
Reatores de linha conectados
2
COSR-X
COSR-X
Agente
Energizar o primeiro autotransformador 
de 230/138/13,8 kV – 150 MVA da SE BBB
SE BBB ≤ 240 kV
LTC entre as posições 5 e 10.
3
COSR-X
COSR-X
Agente
Restabelecer as cargas atendidas pela SE 
BBB 138 kV
Tensão igual ou superior a 130 kV
Cargas limitadas a X MW
4
COSR-X
COSR-X
Agente
Energizar as LT 230 kV BBB / CCC, pelo 
terminal da SE BBB SE BBB ≤ 240 kV
5
COSR-X
COSR-X
Agente
Energizar o primeiro transformador de 
230/69/13,8 kV – 100 MVA da SE CCC
SE CCC ≤ 240 kV
LTC entre as posições 5 e 10
6
COSR-X
COSR-X
Agente
Restabelecer as cargas atendidas pela SE 
CCC 69 kV
Tensão igual ou superior a 62 kV
Cargas limitadas a X MW
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 21/ 22
5. ANEXOS DE DIAGRAMAS PARA UTILIZAÇÃO NOS MODELOS DE INSTRUÇÃO DE RECOMPOSIÇÃO 
ANEXO – MODELO DE DIAGRAMA UNIFILAR SIMPLIFICADO / FLUXOGRAMA 
Exemplo 1: 
Exemplo 2:
UHE 
Marimbondo
SE Araraquara
SE Poços de 
Caldas
Se IItajubá
SE Cachoeira 
Paulista
SE Adrianópolis
SE Grajaú
SE Jacarepaguá
SE Santa Cruz
SE Angra
UHE Fontes 
Novas
UHE Nilo 
Peçanha
UTE Barbosa 
Lima Sobrinho
SE Terminal Sul 
( Carga )
SE Frei Caneca
( Carga )
SE Alcantara 
( Carga )
SE São José
SE Cascadura
Manual de Procedimentos da Operação
 Referência Técnica Código Revisão Item Vigência
Elaboração de Instruções de Operação para a 
Recomposição do Sistema após Perturbação RT-RR.BR 28 7.5. 06/07/2023
Referência: 22/ 22
Exemplo 3:
UHE Nilo 
Peçanha
Fontes
Nova
H
H
Alcântara
Terminal Sul
Frei 
Caneca
UTE Barbosa 
Lima Sobrinho
Cascadura 1
30 + 30 MW
Poços de 
Caldas
H
UHE 
Marimbondo
Araraquara
Itajubá 3
Cachoeira 
Paulista
Grajaú
Adrianópolis
AT 54 ou
AT 52
AT 56 ou
AT 58
Jacarepaguá
São José
310 MW 
Santa Cruz
Angra
T
Carga total da 
área : 370 MW
