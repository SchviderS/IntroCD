 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Uberaba 
 
 
Código Revisão Item Vigência 
IO-OI.S.UBR 31 3.7.5.4. 25/09/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Alteração das condições de autonomia do Agente para energização, desvinculando os procedimentos do 
Subitem 6.2.2 dos procedimentos de recomposição fluente da Instalação constantes no Subitem 5.2.2, 
passando-se a adotar, no Subitem 6.2.2, condições de sistema completo e/ou sistema N -1 para energização 
das seguintes linhas de transmissão e equipamentos: 
• LT 230 kV Curitiba Centro / Uberaba C1 ou C2; 
• LT 230 kV Distrito Industrial São José dos Pinhais / Uberaba; 
• Transformador TF-A ou TF-B 230/69/13,8 kV; 
• Transformador TF-1 ou TF-2 230/13,8 kV. 
- Alteração das condições de autonomia do Agente para fechamento em anel, no Subitem 6.2.2, das seguintes 
linhas de transmissão e equipamentos: 
• LT 230 kV Uberaba / Umbará C1 ou C2; 
• Transformador TF-A ou TF-B 230/69/13,8 kV; 
• Transformador TF-1 ou TF-2 230/13,8 kV. 
- Inclusão dos Subitens 5.4.1.2. e 5.4.2.2. e complementação do Subitem 5.4.2.3. para casos de desligamentos 
parciais. 
- Adequação à RT-OI.BR revisão 37, com destaque à inclusão dos Subitens 6.1.8 e 6.1.9.  
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S Copel DIS Copel GeT 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Uberaba IO-OI.S.UBR 31 3.7.5.4. 25/09/2024 
 
Referência: 2 / 13 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  3 
3.1. Barramento de 230 kV ................................................................................................................... 3 
3.2. Alteração da configuração dos Barramentos ................................................................................ 4 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 4 
4.1. Procedimentos Gerais ................................................................................................................... 4 
4.2. Procedimentos Específicos ............................................................................................................ 4 
4.2.1. Operação dos Comutadores de Tape Sob Carga (LTC) ................................................ 4 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  4 
5.1. Procedimentos Gerais ................................................................................................................... 4 
5.2. Procedimentos para Recomposição Fluente ................................................................................. 5 
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................ 5 
5.2.2. Recomposição Fluente da Instalação .......................................................................... 6 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 8 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 8 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 8 
5.4. Procedimentos Após Desligamento Parcial da Instalação ............................................................ 9 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 9 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 9 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 9 
6.1. Procedimentos Gerais ................................................................................................................... 9 
6.2. Procedimentos Específicos .......................................................................................................... 10 
6.2.1. Desenergização de Equipamentos ............................................................................ 10 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ..................................... 10 
7. NOTAS IMPORTANTES ................................ ................................ ................................ ................ 13 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Uberaba IO-OI.S.UBR 31 3.7.5.4. 25/09/2024 
 
Referência: 3 / 13 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE Uberaba, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme  estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da Instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR-S e a Instalação, de propriedade da Copel GeT, é realizada 
pela Copel GeT, agente responsável pela operação da Instalação, por intermédio do COGT Copel. 
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte da Área 230 kV do Paraná. 
2.4. No que se refere ao religamento manual de equipamentos ou de linhas de transmissão: 
2.4.1. A definição d a quantidade de  tentativas de religamento manual de equipamento ou de linha de 
transmissão, bem como o intervalo entre elas, é de responsabilidade do Agente e devem ser descritos 
no Cadastro de Informações Operacionais da respectiva Área Elétrica.  
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR -S autorização 
para religamento. Nessa oportunidade, o Agente também pode solicitar alteração no sentido normal 
para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em consideração as condições operativas do 
sistema. 
2.5. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação quando o Agente tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 230 KV 
A configuração do barramento de 230 kV é do tipo Principal e Transferência (Barra BP-A e Barra BP-B). Na 
operação normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados, exceto as 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Uberaba IO-OI.S.UBR 31 3.7.5.4. 25/09/2024 
 
Referência: 4 / 13 
 
seccionadoras de transferência das linhas de transmissão ou equipamentos, bem como, a barra de 
transferência deve estar desenergizada, com o disjuntor de transferência aberto e as seccionadoras do 
módulo de transferência fechadas. 
3.2. ALTERAÇÃO DA CONFIGURAÇÃO DOS BARRAMENTOS 
A mudança de configuração do barramento de 230 kV desta Instalação é executada com controle do COSR -
S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade do Agente Operador 
da Instalação 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S.  
As faixas de controle de tensão desse barramento estão estabelecidas no Cadastro de Informações 
Operacionais das Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. Os barramentos de 69 kV e 13,8 kV, não pertencentes à Rede de Operação, onde se conectam as 
transformações 230/69/13,8 kV e 230/13,8 kV, têm a sua regulação de tensão executada com 
autonomia pelo Agente Operador da Instalação, por meio da utilização de recursos locais disponíveis 
de autonomia dessa. 
Esgotados esses recursos, o Agente deve acionar o COSR-S, que deve verificar a indisponibilidade dos 
recursos sistêmicos. 
As faixas de controle de tensão desses barramentos estão estabelecidas no Cadastro de Informações 
Operacionais das Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.3. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob responsabilidade da Instalação. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
4.2.1. OPERAÇÃO DOS COMUTADORES DE TAPE SOB CARGA (LTC) 
Os LTCs dos transformadores TF-A e TF-B 230/69/13,8 kV operam em modo automático. 
Alterações no modo de operação desses comutadores são executadas com autonomia pela operação d o 
Agente Copel GeT.  
Os LTCs dos transformadores TF-1 e TF-2 230/13,8 kV operam em modo manual. 
A movimentação dos comutadores é realizada com autonomia pela operação do Agente Copel GeT.  
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação:  caracterizado quando não há tensão em todos os terminais 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Uberaba IO-OI.S.UBR 31 3.7.5.4. 25/09/2024 
 
Referência: 5 / 13 
 
de suas linhas de transmissão. 
• Desligamento parcial da Instalação:  qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao COSR-S 
as seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2, sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificand o a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia do Agente Operador da Instalação na 
recomposição, deve ser utilizado o Subitem 5.3.  
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE 
No caso de desligamento total, o Agente Operador deve configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• das linhas de transmissão: 
LT 230 kV Uberaba / Umbará C1; 
LT 230 kV Uberaba / Umbará C2;  
LT 230 kV Curitiba Centro / Uberaba C1; 
LT 230 kV Curitiba Centro / Uberaba C2; 
LT 230 kV Curitiba Leste / Uberaba; 
LT 230 kV Distrito Industrial São José dos Pinhais / Uberaba. 
• de todas as linhas de transmissão de 69 kV. 
• dos transformadores: 
TF-A (lados de 230 kV e de 69 kV); 
TF-B (lados de 230 kV e de 69 kV); 
TF-1 (lados de 230 kV e de 13,8 kV); 
TF-2 (lados de 230 kV e de 13,8 kV). 
• de todos os bancos de capacitores de 69 kV. 
Desligar ou manter desligado o modo de comutação automática dos comutadores sob carga da s 
transformações 230/69/13,8 kV e 230/13,8 kV da SE Uberaba. 
Cabe ao Agente Copel GeT para o barramento de 230 kV e ao Agente Copel DIS para o barramento de 69 kV 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Uberaba IO-OI.S.UBR 31 3.7.5.4. 25/09/2024 
 
Referência: 6 / 13 
 
informarem ao COSR-S quando a configuração de preparação desses barramentos não estiver atendida para 
o início da recomposição, independentemente de o equipamento ser próprio ou de outros Agentes. Nesse 
caso, o COSR-S fará contato com os agentes envolvidos para identificar o motivo do não-atendimento e, após 
confirmação do agente afetado de que o barramento está com a configuração atendida, o COSR-S coordenará 
os procedimentos para recomposição, caso necessário, em função da configuração desta Instalação. 
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO 
A Instalação faz parte da recomposição da Área Gov. Ney Aminthas de Barros Braga. Os Agentes Operadores 
devem adotar os procedimentos a seguir para recomposição fluente: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 Copel GeT 
Receber tensão da SE Umbará, por um dos 
circuitos da LT 230 kV  Uberaba / Umbará , e 
fechar o disjuntor, energizando o barramento de 
230 kV. 
 
1.1 Copel GeT 
Energizar, pelo lado de 230 kV, um dos 
transformadores 230/69/13,8 kV da SE Uberaba 
e ligar, energizando o barramento de 69 kV, o 
lado de 69 kV. 
TAPUBR-230/69 = 9 
Possibilitar o restabelecimento de 
carga prioritária na SE Uberaba e 
nas subestações atendidas pela SE 
Uberaba. 
1.1.1 Copel DIS 
Restabelecer carga prioritária  na SE Uberaba e 
nas subestações atendidas pela SE Uberaba. 
No máximo 27,5 MW. 
Respeitar o limite mínimo de 
tensão, de 62  kV, no barramento 
de 69 kV. 
1.2 Copel GeT 
Energizar, pelo lado de 230 kV, o segundo 
transformador 230/69/13,8 kV da SE Uberaba e 
ligar, interligando esse transformador com o 
outro transformador 230/69/13,8 kV da SE 
Uberaba, o lado de 69 kV. 
TAPUBR-230/69 = 9 
Após carga ativa no transformador 
230/69/13,8 kV da SE Uberaba que 
foi energizado. 
1.2.1 Copel DIS 
Restabelecer carga prioritária  na SE Uberaba e 
nas subestações atendidas pela SE Uberaba. 
No máximo 25 MW. 
Respeitar o limite mínimo de 
tensão de 62 kV no barramento de 
69 kV. 
1.3 Copel GeT 
Ligar o banco de capacitores BC -8 69 kV da SE 
Uberaba. 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Uberaba IO-OI.S.UBR 31 3.7.5.4. 25/09/2024 
 
Referência: 7 / 13 
 
Passo Executor Procedimentos Condições ou Limites Associados 
2 Copel GeT 
Receber tensão da SE Umbará, pelo segundo 
circuito da LT 230 kV Uberaba / Umbará, e 
fechar, em anel, o disjuntor. 
ou 
Receber tensão da SE Curitiba Leste, pela LT 230 
kV Curitiba Leste / Uberaba, e fechar, em anel, o 
disjuntor. 
 
2.1 Copel DIS 
Restabelecer carga prioritária no 69 kV  na SE 
Uberaba e nas subestações atendidas pela SE 
Uberaba. 
No máximo mais 50 MW , 
totalizando, no máximo , 102,5 
MW de carga restabelecida no 69 
kV na SE Uberaba. 
Respeitar o limite mínimo de 
tensão, de 62  kV, no barramento 
de 69 kV. 
2.2 Copel GeT 
Energizar, pelo lado de 230 kV, um dos 
transformadores 230/13,8 kV, e ligar, 
energizando o barramento de 13,8 kV, o lado de 
13,8 kV. 
TAPUBR-230/69 = 9. 
Após carga ativa na LT 230 kV 
Curitiba Leste / Uberaba e num dos 
circuitos da LT 230 kV Uberaba / 
Umbará, ou nos dois circuitos LT 
230 kV Uberaba / Umbará. 
Possibilitar o restabelecimento da 
carga na subestação Uberaba e nas 
subestações atendidas pela SE 
Uberaba. 
2.2.1 Copel DIS 
Restabelecer carga prioritária  na subestação 
Uberaba e nas subestações atendidas pela SE 
Uberaba. 
No máximo 25 MW. 
Respeitar o limite mínimo de 
tensão, de 12,4 kV, no barramento 
de 13,8 kV. 
2.3 Copel GeT 
Energizar, pelo lado de 230 kV, o segundo 
transformador 230/13,8 kV, e ligar, interligando 
esse transformador com o outro transformador 
230/13,8 kV da SE Uberaba, o lado de 13,8 kV. 
Após carga ativa no transformador 
230/13,8 kV da SE Uberaba que foi 
energizado. 
TAPUBR-230/69 = 9 
Possibilitar o restabelecimento da 
carga na subestação Uberaba e nas 
subestações atendidas pela SE 
Uberaba. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Uberaba IO-OI.S.UBR 31 3.7.5.4. 25/09/2024 
 
Referência: 8 / 13 
 
Passo Executor Procedimentos Condições ou Limites Associados 
2.3.1. Copel DIS 
Restabelecer carga prioritária na subestação 
Uberaba e nas subestações atendidas pela SE 
Uberaba. 
No máximo 25 MW, totalizando , 
no máximo , 50 MW de carga 
restabelecida no 13,8 kV na SE 
Uberaba e 152,5 MW na SE 
Uberaba no total. 
Respeitar o limite mínimo de 
tensão, de 12,4 kV, no barramento 
de 13,8 kV. 
2.4 Copel GeT 
Energizar a LT 230 kV Distrito Industrial de São 
José dos Pinhais / Uberaba, enviando tensão 
para a SE Distrito Industrial de São José dos 
Pinhais. 
Após fluxo de potência  ativa no 
transformador 230/69/13,8  kV da 
SE Uberaba que foi energizado e na 
LT 230 kV Curitiba Leste / Uberaba,  
ou, 
após fluxo de potência  ativa no 
transformador 230/69/13,8  kV da 
SE Uberaba que foi energizado e 
nos dois circuitos da LT 230 kV 
Uberaba / Umbará. 
2.5 Copel GeT 
Receber tensão da SE Curitiba Leste, pela LT 230 
kV Curitiba Leste / Uberaba, e fechar, em anel, o 
disjuntor, 
ou, 
receber tensão da SE Umbará, pelo segundo 
circuito da LT 230 kV Uberaba / Umbará e 
fechar, em anel, o disjuntor. 
 
Os demais equipamentos desta Instalação são restabelecidos sob controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2, enquanto não houver 
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia da operação da instalação 
na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Uberaba IO-OI.S.UBR 31 3.7.5.4. 25/09/2024 
 
Referência: 9 / 13 
 
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, o 
Agente Operador deve preparar a Instalação conforme Subitem 5.2.1, sem necessidade de 
autorização do ONS. 
5.4.1.2. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão nas barras de 230 kV, os Agentes Operadores devem preparar o setor de 230 
kV d a Instalação (disjuntores das linhas de transmissão e equipamentos) conforme Subitem 
5.2.1., sem necessidade de autorização do ONS.  
5.4.1.3. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador 
deve recompor a Instalação conforme Subitem 5.2.2, sem necessidade de autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., os Agentes 
Operadores devem informar ao COSR-S, para que a recomposição da Instalação seja executada com 
controle do COSR-S. 
5.4.2.3. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.3., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 6.2.2., sem necessidade de 
autorização do ONS. 
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
os Agentes Operadores devem informar ao COSR -S, para que a recomposição da Instalação seja 
executada com controle do COSR-S. 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, após 
desligamentos programados, de urgência ou de emergência, só podem ser efetuados com controle 
do COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelo Agente Operador Instala ção quando 
estiverem explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução de 
Operação. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Uberaba IO-OI.S.UBR 31 3.7.5.4. 25/09/2024 
 
Referência: 10 / 13 
 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, o Agente Operador da Instalação deve verificar se existe 
tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pel o Agente Operador da Instalação 
quando estiver especificado nesta Instrução de Operação e estiverem atendidas as condições do 
Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou, quando permitida, em sentido inverso, pode ser feita com 
autonomia pela operação do Agente, conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica. 
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o operador da Instalação deve fechá-lo em anel desde que tenha autonomia para tal, 
adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S. 
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S. 
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE EQUIPAMENTOS 
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação é sempre 
controlada pelo COSR-S. 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Uberaba IO-OI.S.UBR 31 3.7.5.4. 25/09/2024 
 
Referência: 11 / 13 
 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de equipamentos ou de linhas de transmissão. 
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado nas 
condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
LT 230 kV Curitiba Centro / 
Uberaba C1 ou C2 
Sentido Normal: SE Uberaba envia tensão para a SE Curitiba Centro 
Energizar a LT 230  kV Curitiba Centro / 
Uberaba C1 (ou C2). 
Sistema completo (de LT e TR) ou N-
1 (de LT ou TR) na SE Uberaba 230 kV 
• VUBR-230 ≤ 240 kV. 
Antes de energizar cada LT, verificar 
fluxo de potência ativa no circuito da 
LT 230 kV Curitiba Centro / Uberaba  
que já foi energizado. 
Sentido Inverso: SE Uberaba recebe tensão da SE Curitiba Centro 
Ligar, em anel, a LT 230 kV Curitiba 
Centro / Uberaba C1 (ou C2) na SE 
Curitiba Centro. 
 
LT 230 kV Curitiba Leste / 
Uberaba 
Sentido Normal: SE Uberaba recebe tensão da SE Curitiba Leste 
Ligar, em anel, a LT 230  kV Curitiba 
Leste / Uberaba. 
• VUBR-230 ≤ 240 kV. 
Sentido Inverso: SE Uberaba envia tensão da SE Curitiba Leste. 
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2PR. 
LT 230 kV Distrito Industrial 
São José dos Pinhais / 
Uberaba 
Sentido Normal: SE Uberaba envia tensão para SE Distrito Industrial São 
José dos Pinhais 
Energizar a LT 230 kV Distrito Industrial 
São José dos Pinhais / Uberaba. 
Sistema completo (de LT e TR) ou N-
1 (de LT ou TR) na SE Uberaba 230 kV 
• VUBR-230 ≤ 242 kV. 
Sentido Inverso: SE Uberaba recebe tensão da SE Distrito Industrial São José 
dos Pinhais 
Ligar, em anel, a LT 230  kV Distrito 
Industrial São José dos Pinhais  / 
Uberaba. 
 
LT 230 kV Uberaba / 
Umbará C1 ou C2 
Sentido Normal: SE Uberaba recebe tensão da SE Umbará 
Ligar, em anel, a LT 230 kV Uberaba / 
Umbará C1 (ou C2). 
 
Sentido Inverso: SE Uberaba envia tensão da SE Umbará 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Uberaba IO-OI.S.UBR 31 3.7.5.4. 25/09/2024 
 
Referência: 12 / 13 
 
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2PR. 
Transformador TF-A ou TF-
B 230/69/13,8 kV 
Sentido Normal: A partir do lado de 230 kV.  
Energizar, pelo lado de  230 kV, o 
transformador TF-A 230/69/13,8 kV (ou 
TF-B). 
Sistema completo  (de LT  e TR 
230/13,8) ou N -1 ( de LT ou TR 
230/13,8) na SE Uberaba 230 kV 
Como primeiro ou segundo 
transformador: 
• VUBR-230 ≤ 242 kV; e 
• 1 ≤ TAPUBR-230/69 ≤ 11. 
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa nos transformadores 
230/69/13,8 kV que já foram 
energizados. 
Ligar, energizando o barramento de 69 
kV, ou interligando esse transformador 
com outro já em operação, o lado de 
69 kV do transformador TF -A 
230/69/13,8 kV (ou TF-B). 
Barramento de 69 kV da SE Uberaba 
desenergizado ou energizado por 
outro transformador 230/ 69/13,8 
kV. 
Sentido Inverso: A partir do lado de 69 kV 
Energizar, pelo lado de 69 kV, o 
transformador TF-A 230/69/13,8 kV (ou 
TF-B).  
Sistema completo (de LT) ou N-1 (de 
LT) na SE Uberaba 69 kV 
Como primeiro  ou segundo 
transformador: 
• VUBR-69 ≤ 72 kV. 
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa nos transformadores 
230/69/13,8 kV que já foram 
energizados. 
Ligar, interligando esse transformador 
com outro já em operação , o lado  de 
230 kV do transformador TF -A 
230/69/13,8 kV (ou TF-B). 
Outro transformador 230/69/13,8 kV 
da SE Uberaba com fluxo de potência 
ativa. 
 
Transformador TF-1 ou TF-2 Sentido Único: A partir do lado de 230 kV 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Uberaba IO-OI.S.UBR 31 3.7.5.4. 25/09/2024 
 
Referência: 13 / 13 
 
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
230/13,8 kV Energizar, pelo lado de 230 kV, o 
transformador TF-1 230/13,8 kV (ou TF-
2). 
Sistema completo  (de LT  e TR 
230/69) ou N-1 (de LT ou TR 230/69) 
na SE Uberaba 230 kV 
Como primeiro ou segundo 
transformador: 
• VUBR-230 ≤ 242 kV. 
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa nos transformadores 
230/13,8 kV que já foram 
energizados. 
Ligar, energizando a seção do 
barramento de 13,8 kV correspondente, 
o lado de 13,8 kV do transformador TF-
1 230/13,8 kV (ou TF-2). 
Barramento de 13,8 kV da SE 
Uberaba desenergizado. 
7. NOTAS IMPORTANTES 
Não se aplica. 
