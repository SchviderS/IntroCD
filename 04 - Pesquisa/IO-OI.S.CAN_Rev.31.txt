 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Canoinhas 
 
 
Código Revisão Item Vigência 
IO-OI.S.CAN 31 3.7.5.3. 02/08/2024 
 
. 
 
MOTIVO DA REVISÃO  
- Alteração das condições de autonomia do Agente para energização, desvinculando os procedimentos do 
Subitem 6.2.2 dos procedimentos de recomposição fluente da Instalação constantes no Subitem 5.2.2, 
passando-se a adotar, no Subitem 6.2.2, condições de sistema completo e/ou sistema N -1 para energização 
das seguintes linhas de transmissão e equipamentos: 
• Transformador TF 1 ou TF 2 230/138/13,8 kV; 
• Transformador TF 3 230/138/13,8 kV.  
- Alteração das condições de autonomia do Agente para fechamento em anel, no Subitem 6.2.2, das seguintes 
linhas de transmissão e equipamentos: 
• LT 230 kV Canoinhas / São Mateus do Sul C1 ou C2. 
- Inclusão dos Subitens 5.4.1.2. e 5.4.2.2. e complementação do Subitem 5.4.2.3. para casos de desligamentos 
parciais. 
- Adequação à RT-OI.BR revisão 36, com destaque à inclusão dos Subitens 6.1.8 e 6.1.9.  
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S Celesc  COPEL GeT CGT Eletrosul 
(COT Norte) 
Evoltz     
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Canoinhas IO-OI.S.CAN 31 3.7.5.3. 02/08/2024 
 
Referência:  2 / 12 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  4 
3.1. Barramento de 230 kV ................................................................................................................... 4 
3.2. Alteração da Configuração dos Barramentos ................................................................................ 4 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 5 
4.1. Procedimentos Gerais ................................................................................................................... 5 
4.2. Procedimentos Específicos ............................................................................................................ 5 
4.2.1. Operação do Banco de Capacitores............................................................................. 5 
4.2.2. Operação dos Comutadores de Tape sob Carga (LTC) ................................................ 5 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  5 
5.1. Procedimentos Gerais ................................................................................................................... 5 
5.2. Procedimentos para Recomposição Fluente ................................................................................. 6 
5.2.1. Preparação da Instalação para a Recomposição Fluente ............................................ 6 
5.2.2. Recomposição Fluente da Instalação .......................................................................... 6 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 8 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 8 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 8 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 8 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 8 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 9 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 9 
6.1. Procedimentos Gerais ................................................................................................................... 9 
6.2. Procedimentos Específicos .......................................................................................................... 10 
6.2.1. Desenergização de Linhas de Transmissão e de Equipamentos ............................... 10 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ..................................... 10 
7. NOTAS IMPORTANTES ................................ ................................ ................................ ................ 12 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Canoinhas IO-OI.S.CAN 31 3.7.5.3. 02/08/2024 
 
Referência:  3 / 12 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE Canoinhas, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido no s Procedimentos 
de Rede. 
Estabelecer também procedimentos para a operação dos módulos de 138 kV dos Transformadores TF 1, 
TF 2 e TF 3 230/138/13,8 kV, módulos esses localizados na SE Canoinhas (Celesc). 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pelos Agentes Operadores das Instalações, devendo fazer parte do manual 
de operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade 
das ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR -S e a Instalação, no que se refere aos equipamentos de 
manobra na Instalação, é realizada conforme segue. 
Linha de Transmissão ou 
Equipamento  
Agente de 
Operação 
Agente 
Operador 
Centro de Operação 
do Agente Operador  
Barramento de 230 kV CGT Eletrosul CGT Eletrosul COT Norte 
LT 230 kV Canoinhas / São Mateus 
do Sul C2 Evoltz IV Evoltz COS Evoltz 
LT 230 kV Canoinhas / São Mateus 
do Sul C1 CGT Eletrosul CGT Eletrosul COT Norte 
Transformador TF 1 230/138/13,8 kV 
CGT Eletrosul 
(transformador e 
módulo de 230 kV) 
CGT Eletrosul COT Norte 
Celesc (módulo de 
138 kV na SE 
Canoinhas (Celesc)) 
Celesc COS Celesc 
Transformador TF 2 230/138/13,8 kV 
CGT Eletrosul 
(transformador e 
módulo de 230 kV) 
CGT Eletrosul COT Norte 
Celesc (módulo de 
138 kV na SE 
Canoinhas (Celesc)) 
Celesc COS Celesc 
Transformador TF 3 230/138/13,8 kV 
CGT Eletrosul 
(transformador e 
módulo de 230 kV) 
CGT Eletrosul COT Norte 
Celesc (módulo de 
138 kV na SE 
Canoinhas (Celesc)) 
Celesc COS Celesc 
Banco de Capacitores BC 1 230 kV  CGT Eletrosul CGT Eletrosul COT Norte 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Canoinhas IO-OI.S.CAN 31 3.7.5.3. 02/08/2024 
 
Referência:  4 / 12 
 
2.3. Os equipamentos e linhas de transmissão desta Instalação  fazem parte da Área 230 kV de Santa 
Catarina. 
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.4.1. A definição da quantidade  de tentativas de religamento manual de linha de transmissão ou de 
equipamento, bem como o intervalo entre elas , é de responsabilidade do Agente e devem estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica. 
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR -S autorização 
para religamento. Nessa oportunidade, o Agente também pode solicitar alteração no sentido normal 
para envio de tensão, caso não tenha autonomia para tal.   
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em consideração as condições operativas do 
sistema. 
2.5. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica.  
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2. desta Instrução de Operação , quando o Agente  tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 230 KV 
A configuração do barramento de 230 kV é do tipo Principal e Transferência (Barra P e Barra T). Na 
operação normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados, exceto 
as seccionadoras de transferência das linhas de transmissão ou equipamentos. 
3.2. ALTERAÇÃO DA CONFIGURAÇÃO DOS BARRAMENTOS 
A mudança de configuração do barramento de  230 kV desta Instalação é executada com controle do 
COSR-S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade d os Agentes 
Operadores da Instalação. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Canoinhas IO-OI.S.CAN 31 3.7.5.3. 02/08/2024 
 
Referência:  5 / 12 
 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S. 
As faixas de controle de tensão para esse barramento estão estabelecidas no Cadastro de Informações 
Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. O barramento de 138 kV  da SE Canoinhas ( Celesc), não pertencente à Rede de Operação , onde se 
conecta a transformação 230/138/13,8 kV da SE Canoinhas, tem a sua regulação de tensão executada 
com autonomia pel os Agentes Operadores da s Instalações por meio da utilização de recursos locais 
disponíveis de autonomia dessas. 
Esgotados esses recursos, os Agentes devem acionar o COSR -S, que deve verificar a disponibilidade 
dos recursos sistêmicos. 
As faixas de controle de tensão para o barramento de 138 kV estão estabelecidas no Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.3. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob a responsabilidade da Instalação. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
4.2.1. OPERAÇÃO DO BANCO DE CAPACITORES 
A manobra do banco de capacitores BC 1 230 kV – 50 Mvar é executada sob controle do COSR-S. 
4.2.2. OPERAÇÃO DOS COMUTADORES DE TAPE SOB CARGA (LTC) 
Os LTCs dos transformadores TF 1, TF 2, e TF 3 230/138/13,8 kV operam em modo manual.  
A movimentação dos comutadores é realizada com autonomia pela operação do Agente CGT Eletrosul. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação  dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir, para atendimento às necessidades sistêmicas: 
• Desligamento total da Instalação : caracterizado quando não há tensão em todos os terminais de 
suas linhas de transmissão.  
• Desligamento parcial da Instalação : qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, os Agentes Operadores da Instalação  devem fornecer ao 
COSR-S as informações a seguir: 
• horário da ocorrência; 
• configuração da Instalação logo após a ocorrência. 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Canoinhas IO-OI.S.CAN 31 3.7.5.3. 02/08/2024 
 
Referência:  6 / 12 
 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2., sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não -aplicabilidade da 
recomposição fluente ou interrompendo a autonomia d os Agentes Operadores da Instalação  na 
recomposição, deve ser utilizado o Subitem 5.3.  
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO FLUENTE 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO FLUENTE 
No caso de desligamento total, os Agentes Operadores da Instalação devem configurar os disjuntores 
dos seguintes equipamentos, linhas de transmissão e unidades geradoras , conforme condição 
apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• das linhas de transmissão: 
LT 230 kV Canoinhas / São Mateus do Sul C1; 
LT 230 kV Canoinhas / São Mateus do Sul C2. 
• dos transformadores: 
TF 1 230/138/13,8 kV (lado de 230 kV na SE Canoinhas e lado de 138 kV na SE Canoinhas (Celesc)); 
TF 2 230/138/13,8 kV (lado de 230 kV na SE Canoinhas e lado de 138 kV na SE Canoinhas (Celesc)); 
TF 3 230/138/13,8 kV (lado de 230 kV na SE Canoinhas e lado de 138 kV na SE Canoinhas (Celesc)). 
• do banco de capacitores:  
BC 1 230 kV. 
Desligar ou manter desligado o modo de comutação automática dos comutadores sob carga da 
transformação 230/138/13,8 kV da SE Canoinhas. 
Cabe ao Agente à CGT Eletrosul, para a SE Canoinhas, informar ao COSR-S quando a configuração de 
preparação da Instalação não estiver atendida para o início da recomposição, independentemente 
de o equipamento ser próprio ou de outros agentes. Nesse caso, o COSR -S fará contato com os 
agentes envolvidos para identificar o motivo do não -atendimento e, após confirmação do Agente 
CGT Eletrosul de que os barramentos estão com a configuração atendida, o COSR -S coordenará os 
procedimentos para recomposição, caso necessário, em função da configuração desta Instalação.  
Cabe ao Agente Celesc, para a SE Canoinhas (Celesc), informar ao COSR-S quando a configuração de 
preparação da Instalação não estiver atendida para o início da recomposição, independentemente 
de o equipamento ser próprio ou de outros agentes. Nesse caso, o COSR -S fará contato com os 
agentes envolvidos, pa ra identificar o motivo do não -atendimento e, após confirmação do Agente 
Celesc de que os barramentos estão com a configuração atendida, o COSR -S coordenará os 
procedimentos para recomposição, caso necessário, em função da configuração desta Instalação. 
5.2.2. RECOMPOSIÇÃO FLUENTE DA INSTALAÇÃO 
A Instalação faz parte da recomposição da Área Gov. Ney Aminthas de Barros Braga . Os Agentes 
Operadores da Instalação devem adotar os procedimentos a seguir para recomposição fluente: 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Canoinhas IO-OI.S.CAN 31 3.7.5.3. 02/08/2024 
 
Referência:  7 / 12 
 
Passo Executor Procedimentos Condições ou Limites Associados 
1 CGT Eletrosul 
(COT Norte) 
Receber tensão da SE São Mateus do Sul, pela LT 
230 kV Canoinhas / São Mateus do Sul C1 e 
energizar o barramento de 230 kV. 
 
1.1 CGT Eletrosul 
(COT Norte) 
Energizar, pelo lado 230 kV, o Transformador 
TF 1 230/138/13,8 kV (ou TF 2) da SE Canoinhas, 
enviando tensão para a SE Canoinhas (Celesc). 
TAPCAN-230/138 = 10. 
Possibilitar o restabelecimento de 
carga prioritária nas subestações 
atendidas pela SE Canoinhas. 
1.1.1 
Celesc  
(na SE 
Canoinhas 
(Celesc)) 
Receber tensão da SE Canoinhas, pelo 
Transformador TF 1 230/138/13,8 kV (ou TF 2) 
da SE Canoinhas, e energizar o barramento de 
138 kV da SE Canoinhas (Celesc). 
 
1.1.1.1 Celesc 
(na SE 
Canoinhas 
(Celesc)) 
Restabelecer carga prioritária  nas subestações 
atendidas pela SE Canoinhas  
No máximo 40 MW. 
Respeitar o limite mínimo de 
tensão de 124 kV para a área de 
138 kV. 
1.2 CGT Eletrosul 
(COT Norte) 
Energizar, pelo lado 230 kV, o Transformador 
TF 2 230/138/13,8 kV (ou TF 1) da SE Canoinhas, 
enviando tensão para a SE Canoinhas (Celesc). 
TAPCAN-230/138 = 10.  
 
Após fluxo de potência ativa  no 
transformador 230/138/13,8  kV 
da SE Canoinhas que foi 
energizado. 
1.2.1 
Celesc 
(na SE 
Canoinhas 
(Celesc)) 
Receber tensão da SE Canoinhas, pelo segundo 
transformador 230/138/13,8 kV da SE 
Canoinhas:  
• se for o TF 1 ou o TF 2 230/138/13,8 kV da 
SE Canoinhas, ligá-lo em anel,  
• se for o TF 3 230/138/13,8 kV  da SE 
Canoinhas, ligá-lo interligando esse 
transformador com o outro já em operação. 
 
1.3 CGT Eletrosul 
(COT Norte) 
Energizar, pelo lado 230 kV, o Transformador 
TF 3 230/138/13,8 kV da SE Canoinhas , 
enviando tensão para a SE Canoinhas (Celesc). 
TAPCAN-230/138 = 10.  
 
Após fluxo de potência ativa  na 
transformação 230/138/13,8  kV 
da SE Canoinhas que foi 
energizada. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Canoinhas IO-OI.S.CAN 31 3.7.5.3. 02/08/2024 
 
Referência:  8 / 12 
 
Passo Executor Procedimentos Condições ou Limites Associados 
1.3.1 
Celesc  
(na SE 
Canoinhas 
(Celesc)) 
Receber tensão da SE Canoinhas, pelo terceiro 
transformador 230/138/13,8 kV da SE 
Canoinhas: 
• se for o TF 1 ou o TF 2 230/138/13,8 kV da 
SE Canoinhas ligá-lo em anel 
• se for o TF 3 230/138/13,8 kV da SE 
Canoinhas, ligá -lo interligando esse 
transformador com o outro já em operação. 
 
2 Evoltz 
Receber tensão da SE São Mateus do Sul, pela LT 
230 kV Canoinhas / São Mateus do Sul C2 e ligá-
la, em anel. 
 
Os demais equipamentos desta Instalação são restabelecidos sob controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
Os Agentes Operadores da Instalação devem realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
Os Agentes Operadores da Instalação devem realizar os procedimentos do Subitem 5.2.2 ., enquanto 
não houver intervenção do COSR -S no processo de recomposição ou interrupção da autonomia da 
operação da instalação na recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR-S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras.  
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL  
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, os 
Agentes Operadores da Instalação  devem preparar a Instalação conforme Subitem 5.2.1., sem 
necessidade de autorização do ONS. 
5.4.1.2. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão nas barras de 230 kV, os Agentes Operadores devem preparar o setor de 230 
kV d a Instalação (disjuntores das linhas de transmissão e equipamentos) conforme Subitem 
5.2.1., sem necessidade de autorização do ONS.  
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Canoinhas IO-OI.S.CAN 31 3.7.5.3. 02/08/2024 
 
Referência:  9 / 12 
 
5.4.1.3. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., os Agentes Operadores 
da Instalação  devem recompor a Instalação conforme Subitem 5.2 .2., sem necessidade de 
autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., os Agentes 
Operadores devem informar ao COSR-S, para que a recomposição da Instalação seja executada com 
controle do COSR-S. 
5.4.2.3. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.3., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 6.2.2., sem necessidade de 
autorização do ONS. 
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
os Agentes Operadores devem informar ao COSR -S, para que a recomposição da Instalação seja 
executada com controle do COSR-S. 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS  
6.1.1. Os procedimentos para desligamento programado ou de urgência de unidades geradoras, e 
desenergização programada ou de urgência de linhas de transmissão ou de equipamentos, só 
podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, ou sincronismo 
de unidades geradoras , após desligamento s programados, de urgência ou de emergência, só 
podem ser efetuados com controle do COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, ou para sincronismo de unidades geradoras,  após desligamento automático sem 
atuação de proteção que impeça o retorno do equipamento , só podem ser executados com 
autonomia pel os Agentes Operadores da Instalação  quando estiverem explicitados e estiverem 
atendidas as condições do Subitem 6.2.2. desta Instrução de Operação. 
Quando as condições ou limites associados não estiverem atendid os ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, os Agentes Operadores da Instalação devem verificar 
se existe tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pel os Agentes Operadores da 
Instalação quando estiver especificado nesta Instrução de Operação e estiverem atendidas as 
condições do Subitem 6.2.2. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Canoinhas IO-OI.S.CAN 31 3.7.5.3. 02/08/2024 
 
Referência:  10 / 12 
 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou , quando permitida, em sentido  inverso, pode ser feita com 
autonomia pelos Agentes Operadores da Instalação, conforme procedimentos para manobras que 
estão definidos nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização 
controlados pelo COSR-S estão definidos na Instrução de Operação de Preparação para Manobras 
da respectiva área elétrica.  
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados  na Instalação, durante execução de 
intervenções, são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador 
é energizado, o s Agentes O peradores da Instalação deve m fechá-lo em anel desde que tenha 
autonomia para tal, adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S.  
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão  ou equipamento, o fechamento desse disjuntor deve ser realizado com 
controle do COSR-S. 
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação, 
é sempre controlada pelo COSR-S. 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Canoinhas IO-OI.S.CAN 31 3.7.5.3. 02/08/2024 
 
Referência:  11 / 12 
 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pel os Agentes Operadores da Instalação , 
após desligamento automático de unidades geradoras, de equipamentos ou de linhas de 
transmissão. 
Os Agentes Operadores da Instalação devem identificar os desligamentos automáticos observando 
na Instalação as demais unidades geradoras, linhas de transmissão e equipamentos em operação, 
conforme explicitado nas condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
LT 230 kV Canoinhas / São 
Mateus do Sul C1 ou C2 
Sentido Normal: SE Canoinhas recebe tensão da SE São Mateus do Sul.  
Ligar, em anel, a LT 230 kV Canoinhas / 
São Mateus do Sul C1 (ou C2).  
 
Sentido Inverso: SE Canoinhas envia tensão para SE São Mateus do Sul.  
A energização em sentido inverso é controlada pelo COSR-S, conforme  
IO-PM.S.2SC. 
Transformador TF 1 ou TF 2  
230/138/13,8 kV 
Sentido Normal: A partir do lado de 230 kV 
Na SE Canoinhas:  
Energizar, pelo  lado de 230 kV , o 
Transformador TF 1 230/138/13,8 kV 
(ou TF 2). 
Sistema completo (de LT) ou N-1 (de 
LT) na SE Canoinhas 230 kV 
Como primeiro, segundo ou terceiro 
transformador: 
• VCAN-230 ≤ 242 kV. 
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa nos transformadores 
230/138/13,8 kV que já foram 
energizados. 
Na SE Canoinhas (Celesc): 
Ligar, em anel , ou energizando o 
barramento de 138 kV da SE Canoinhas 
(Celesc), o lado de 138 kV  do 
Transformador TF 1 230/138/13,8 kV 
(ou TF 2) da SE Canoinhas. 
 
Sentido Inverso: A partir do lado de 138 kV, na SE Canoinhas (Celesc) 
Na SE Canoinhas (Celesc): 
A energização em sentido inverso é controlada pelo COSR-S, conforme  
IO-PM.S.2SC. 
Na SE Canoinhas:  
Ligar, em anel, o lado de 230 kV  do 
Transformador TF 1 230/138/13,8 kV 
(ou TF 2). 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Canoinhas IO-OI.S.CAN 31 3.7.5.3. 02/08/2024 
 
Referência:  12 / 12 
 
Equipamento / Linha de 
Transmissão Procedimentos Condições ou Limites Associados 
Transformador TF 3 
230/138/13,8 kV 
Sentido Normal: A partir do lado de 230 kV 
Na SE Canoinhas:  
Energizar, pelo  lado da 230 kV , o 
Transformador TF 3 230/138/13,8 kV. 
Sistema completo (de LT) ou N-1 (de 
LT) na SE Canoinhas 230 kV 
Como primeiro, segundo ou terceiro 
transformador: 
• VCAN-230 ≤ 242 kV. 
 
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa nos transformadores 
230/138/13,8 kV que já foram 
energizados. 
Na SE Canoinhas (Celesc): 
Ligar, energizando o barramento de 138 
kV da SE Canoinhas ( Celesc), ou 
interligando esse transformador com 
outro já em operação, o lado de 138 kV 
do Transformador TF 3 230/138/13,8 kV 
da SE Canoinhas. 
Sem verificação angular: 
O barramento 138 kV da SE 
Canoinhas ( Celesc) deve estar 
desenergizado ou energizado por 
outro transformador 
230/138/13,8 kV da SE Canoinhas.  
Sentido Inverso: A partir do lado de 138 kV, na SE Canoinhas (Celesc) 
Na SE Canoinhas (Celesc): 
A energização em sentido inverso é controlada pelo COSR-S, conforme  
IO-PM.S.2SC. 
Na SE Canoinhas:  
Ligar, em anel, o lado de 230 kV  do 
Transformador TF 3 230/138/13,8 kV. 
 
Banco de Capacitores BC 1 
230 kV – 50 Mvar A manobra deste Banco de Capacitores é controlada pelo COSR-S. 
7. NOTAS IMPORTANTES 
7.1. Para substituir o disjuntor da LT 230 kV Canoinhas / São Mateus do Sul C2 pelo disjuntor de interligação 
das barras P e T  230 kV, o COSR -S coordenará essa ação com a Evoltz e com a CGT Eletrosul. Após tal 
substituição, os procedimentos para abertura do disjuntor ou recomposição da linha de transmissão 
deverão ser coordenados pelo COSR -S, que orientará a operação da CGT Eletrosul  na execução dos 
procedimentos descritos nesta Instrução de Operação e na IO-PM.S.2SC – Procedimentos de Preparação 
para Manobras na Área 230 kV de Santa Catarina. 
7.2. O Transformador TF 3 230/138/13,8 kV da SE Canoinhas não dispõe de recursos para sincronismo no 
lado de 138 kV. 
