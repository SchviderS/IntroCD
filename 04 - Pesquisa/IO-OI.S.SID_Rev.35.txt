 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Siderópolis 
 
 
Código Revisão Item Vigência 
IO-OI.S.SID 35 3.7.5.3. 02/08/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Alteração das condições de autonomia do Agente para energização, desvinculando os procedimentos do 
Subitem 6.2.2 dos procedimentos de recomposição fluente da Instalação constantes no Subitem 5.2.2, 
passando-se a adotar, no Subitem 6.2.2, condições de sistema completo e/ou sistema N -1 para energização 
das seguintes linhas de transmissão e equipamentos: 
• LT 230 kV Jorge Lacerda B / Siderópolis C1 ou C2; 
• LT 230 kV Siderópolis / Tubarão Sul (exclusão de texto sobre desligamento total). 
- Alteração das condições de autonomia do Agente para fechamento em anel, no Subitem 6.2.2, das seguintes 
linhas de transmissão e equipamentos: 
• LT 230 kV Forquilhinha / Siderópolis; 
- Inclusão dos Subitens 5.4.1.2. e 5.4.2.2. e complementação do Subitem 5.4.2.3. para casos de desligamentos 
parciais. 
- Adequação à RT-OI.BR revisão 37, com destaque à inclusão dos Subitens 6.1.8 e 6.1.9.  
 
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S Celesc CGT Eletrosul 
(COT Norte) Cteep 
EDP Brasil Coopercocal   
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Siderópolis IO-OI.S.SID 35 3.7.5.3. 02/08/2024 
 
Referência:  2 / 12 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  4 
3.1. Barramento de 230 kV ................................................................................................................... 4 
3.2. Alteração da Configuração dos Barramentos ................................................................................ 4 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 4 
4.1. Procedimentos Gerais ................................................................................................................... 4 
4.2. Procedimentos Específicos ............................................................................................................ 5 
4.2.1. Operação dos Comutadores de Tape sob Carga (LTC) ................................................ 5 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  5 
5.1. Procedimentos Gerais ................................................................................................................... 5 
5.2. Procedimentos para Recomposição com Autonomia ................................................................... 6 
5.2.1. Preparação da Instalação para a Recomposição com Autonomia .............................. 6 
5.2.2. Recomposição com Autonomia após Desligamento Total da Instalação.................... 6 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................... 7 
5.3.1. Preparação da Instalação após Desligamento Total ................................................... 7 
5.3.2. Recomposição após Desligamento Total da Instalação .............................................. 7 
5.4. Procedimentos após Desligamento Parcial da Instalação ............................................................. 7 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ............. 7 
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................ 8 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................... 8 
6.1. Procedimentos Gerais ................................................................................................................... 8 
6.2. Procedimentos Específicos ............................................................................................................ 9 
6.2.1. Desenergização de Linhas de Transmissão e de Equipamentos ................................. 9 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ..................................... 10 
7. NOTAS IMPORTANTES ................................ ................................ ................................ ................ 11 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Siderópolis IO-OI.S.SID 35 3.7.5.3. 02/08/2024 
 
Referência:  3 / 12 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE Siderópolis , definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido no s Procedimentos de 
Rede.  
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, 
realizados com autonomia pela operação da instalação, devendo fazer parte do manual de operação 
próprio elaborado pelo Agente quando existente, observando-se a complementaridade das ações 
que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2.  A comunicação operacional entre o COSR -S e a Instalação, no que se refere aos equipamentos de 
manobra na Instalação, é realizada conforme segue. 
Linha de Transmissão ou 
Equipamento Agente de Operação Agente Operador Centro de Operação 
do Agente Operador  
Barramento de 230 kV CGT Eletrosul CGT Eletrosul COT Norte 
LT 230 kV Forquilhinha / 
Siderópolis CGT Eletrosul CGT Eletrosul COT Norte 
LT 230 kV Jorge Lacerda B / 
Siderópolis C1 CGT Eletrosul CGT Eletrosul COT Norte 
LT 230 kV Jorge Lacerda B / 
Siderópolis C2 CGT Eletrosul CGT Eletrosul COT Norte 
LT 230 kV Siderópolis / 
Siderópolis 2 C1 EDP Aliança EDP Brasil COGT EDP 
LT 230 kV Siderópolis / 
Siderópolis 2 C2 EDP Aliança EDP Brasil COGT EDP 
LT 230 kV Siderópolis / 
Tubarão Sul Iesul Cteep COT Cteep 
Transformador TF 1 
230/69/13,8 kV CGT Eletrosul CGT Eletrosul COT Norte 
Transformador TF 3 
230/69/13,8 kV CGT Eletrosul CGT Eletrosul COT Norte 
Transformador TF 4 
230/69/13,8 kV CGT Eletrosul CGT Eletrosul COT Norte 
2.3. Os equipamentos e linhas de transmissão desta Instalação  fazem parte da Área  230 kV de Santa 
Catarina.  
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.4.1. A definição da quantidade de tentativas de religamento manual de linha de transmissão ou 
equipamento, bem como o intervalo entre elas, é de responsabilidade do Agente e devem estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Siderópolis IO-OI.S.SID 35 3.7.5.3. 02/08/2024 
 
Referência:  4 / 12 
 
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR -S autorização 
para religamento. Nessa oportunidade, o Agente também pode solicitar alteração no sentido normal 
para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em consideração as condições operativas do 
sistema. 
2.5. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2 desta Instrução de Operação quando o Agente tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 230 KV 
A configuração do barramento de 230 kV é do tipo Principal e Transferência (Barra P e Barra T). Na operação 
normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados, exceto as 
seccionadoras de transferência das linhas de transmissão ou equipamentos. 
3.2. ALTERAÇÃO DA CONFIGURAÇÃO DOS BARRAMENTOS 
A mudança de configuração do barramento de 230 kV desta Instalação é executada com controle do COSR -
S. 
A mudança de configuração dos demais barramentos é executada sob responsabilidade do s Agentes 
Operadores da Instalação 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. O barramento de 230 kV, pertencente à Rede de Operação, tem a sua regulação de tensão controlada 
pelo COSR-S.  
As faixas de controle de tensão desse barramento estão estabelecidas no Cadastro de Informações 
Operacionais das Faixas para Controle de Tensão da respectiva Área Elétrica. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Siderópolis IO-OI.S.SID 35 3.7.5.3. 02/08/2024 
 
Referência:  5 / 12 
 
4.1.1. O barramento de 69 kV, não pertencente à Rede de Operação, onde se conecta a transformação 
230/69/13,8 kV, têm a sua regulação de tensão executada com autonomia pelo Agente Operador da 
Instalação, por meio da utilização de recursos locais disponíveis de autonomia dessa. 
Esgotados esses recursos, o Agente deve acionar o COSR-S, que deve verificar a indisponibilidade dos 
recursos sistêmicos. 
As faixas de controle de tensão para esse barramento estão estabelecidas no Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob responsabilidade da Instalação. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
4.2.1. OPERAÇÃO DOS COMUTADORES DE TAPE SOB CARGA (LTC) 
Os LTCs dos transformadores TF1, TF3 e TF4 230/69/13,8 kV operam em modo manual.  
A movimentação dos comutadores é realizada com autonomia pela operação do Agente CGT Eletrosul. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação: caracterizado quando não há tensão em todos os terminais 
de suas linhas de transmissão.  
• Desligamento parcial da Instalação:  qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, o s Agentes Operadores da Instalação devem fornecer ao 
COSR-S as seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2, sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificand o a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia do s Agentes Operadores da Instalação na 
recomposição, deve ser utilizado o Subitem 5.3.  
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Siderópolis IO-OI.S.SID 35 3.7.5.3. 02/08/2024 
 
Referência:  6 / 12 
 
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO COM AUTONOMIA 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO COM AUTONOMIA 
No caso de desligamento total, os Agentes Operadores devem configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• das linhas de transmissão:  
LT 230 kV Forquilhinha / Siderópolis; 
LT 230 kV Jorge Lacerda B / Siderópolis C1; 
LT 230 kV Jorge Lacerda B / Siderópolis C2; 
LT 230 kV Siderópolis / Siderópolis 2 C1; 
LT 230 kV Siderópolis / Siderópolis 2 C2; 
LT 230 kV Siderópolis / Tubarão Sul. 
• de todas as linhas de transmissão de 69 kV. 
• dos transformadores:  
TF 1 230/69/13,8 kV (lados de 230 kV e de 69 kV); 
TF 3 230/69/13,8 kV (lados de 230 kV e de 69 kV); 
TF 4 230/69/13,8 kV (lados de 230 kV e de 69 kV). 
Desligar ou manter desligado o modo de comutação automática dos comutadores sob carga da 
transformação 230/69/13,8 kV da SE Siderópolis. 
Cabe ao Agente CGT Eletrosul para o barramento de 230 kV e ao Agente Celesc para o barramento de 69 kV 
informarem ao COSR-S quando a configuração de preparação desses barramentos não estiver atendida para 
o início da recomposição, independentemente de o equipamento ser próprio ou de outros Agentes. Nesse 
caso, o COSR-S fará contato com os agentes envolvidos para identificar o motivo do não-atendimento e, após 
confirmação do agente afetado de que o barramento está com a configuração atendida, o COSR-S coordenará 
os procedimentos para recomposição, caso necessário, em função da configuração desta Instalação. 
5.2.2. RECOMPOSIÇÃO COM AUTONOMIA APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
Os Agentes Operadores da Instalação devem realizar com autonomia  os seguintes procedimentos para a 
recomposição: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 CGT Eletrosul 
(COT Norte) 
Receber tensão da SE Forquilhinha, pela LT 
230 kV Forquilhinha / Siderópolis e energizar o 
barramento de 230 kV. 
 
1.1 EDP Brasil Energizar a LT 230 kV Siderópolis / Tubarão Sul, 
enviando tensão para a SE Tubarão Sul. 
 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Siderópolis IO-OI.S.SID 35 3.7.5.3. 02/08/2024 
 
Referência:  7 / 12 
 
Passo Executor Procedimentos Condições ou Limites Associados 
1.2 CGT Eletrosul 
(COT Norte) 
Energizar, pelo lado de 230 kV, o primeiro 
transformador 230/69/13,8 kV da SE Siderópolis 
e ligar, energizando o barramento de 69 kV , o 
lado de 69 kV. 
Restabelecer carga somente após 
autorização do COSR-S. 
1.3 CGT Eletrosul 
(COT Norte) 
Energizar, pelo lado de 230 kV, o segundo 
transformador 230/69/13,8 kV da SE Siderópolis 
e ligar, interligando esse transformador com o 
outro transformador 230/69/13,8 kV da SE 
Siderópolis, o lado de 69 kV. 
Após carga ativa na transformação 
230/69/13,8 kV da SE Siderópolis 
energizada. 
1.4 CGT Eletrosul 
(COT Norte) 
Energizar, pelo lado de 230 kV, o terceiro 
transformador 230/69/13,8 kV da SE Siderópolis 
e ligar, interligando esse transformador com o 
outro transformador 230/69/13,8 kV da SE 
Siderópolis, o lado de 69 kV. 
Após carga ativa na transformação 
230/69/13,8 kV da SE Siderópolis 
energizada. 
Os demais equipamentos desta Instalação são restabelecidos sob controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
Os Agentes Operadores da Instalação devem realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
Os Agentes Operadores da Instalação devem realizar os procedimentos do Subitem 5.2.2, enquanto não 
houver intervenção do COSR -S no processo de recomposição ou interrupção da autonomia destes na 
recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL  
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, os 
Agentes Operadores devem preparar a Instalação conforme Subitem 5.2.1, sem necessidade de 
autorização do ONS. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Siderópolis IO-OI.S.SID 35 3.7.5.3. 02/08/2024 
 
Referência:  8 / 12 
 
5.4.1.2. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão nas barras de 230 kV, os Agentes Operadores devem preparar o setor de 230 
kV d a Instalação (disjuntores das linhas de transmissão e equipamentos) conforme Subitem 
5.2.1., sem necessidade de autorização do ONS.  
5.4.1.3. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 5.2.2, sem necessidade de 
autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., os Agentes 
Operadores devem informar ao COSR-S, para que a recomposição da Instalação seja executada com 
controle do COSR-S. 
5.4.2.3. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.3., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 6.2.2., sem necessidade de 
autorização do ONS. 
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
os Agentes Operadores devem informar ao COSR -S, para que a recomposição da Instalação seja 
executada com controle do COSR-S. 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, após 
desligamentos programados, de urgência ou de emergência, só podem ser efetuados com controle 
do COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelos Agentes Operadores Ins talação 
quando estiverem explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução 
de Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Siderópolis IO-OI.S.SID 35 3.7.5.3. 02/08/2024 
 
Referência:  9 / 12 
 
6.1.4. Antes do fechamento de qualquer disjuntor, os Agentes Operadores da Instalação devem verificar se 
existe tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pelos Agentes Operadores da 
Instalação quando estiver especificado nesta Instrução de Operação e estiverem atendidas as 
condições do Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou, quando permitida, em sentido inverso, pode ser feita com 
autonomia pela operação do Agente, conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica. 
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o operador da Instalação deve fechá -lo em anel desde que tenha autonomia para tal, 
adotando as condições para o fechamento constantes no Subitem 6.2.2. 
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S.  
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S. 
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação é sempre 
controlada pelo COSR-S. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Siderópolis IO-OI.S.SID 35 3.7.5.3. 02/08/2024 
 
Referência:  10 / 12 
 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelos Agentes Operadores da Instalação, 
após desligamento automático de equipamentos ou de linhas de transmissão. 
Os Agentes Operadores da Instalação devem identificar os desligamentos automáticos observando 
na Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado 
nas condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Linha de Transmissão / 
Equipamento  Procedimentos Condições ou Limites Associados 
LT 230 kV Forquilhinha / 
Siderópolis 
Sentido Normal: SE Siderópolis recebe tensão da SE Forquilhinha 
Ligar, em anel, a LT 230 kV Forquilhinha 
/ Siderópolis. 
 
Sentido Inverso: SE Siderópolis envia tensão para SE Forquilhinha 
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2SC. 
LT 230 kV Jorge Lacerda B / 
Siderópolis C1 ou C2 
Sentido Normal: SE Siderópolis envia tensão para SE Jorge Lacerda B 
Energizar a LT 230 kV Jorge Lacerda B / 
Siderópolis C1 (ou C2). 
Sistema completo (de LT e TR) ou 
N-1 (de LT ou TR)  na SE 
Siderópolis 230 kV 
•VSID-230 ≤ 242 kV. 
Antes de energizar cada circuito 
da LT 230 kV Jorge Lacerda B / 
Siderópolis, verificar fluxo de 
potência ativa nos circuitos que já 
foram energizados.  
Sentido Inverso: SE Siderópolis recebe tensão da SE Jorge Lacerda B 
Ligar, em anel, a LT 230 kV Jorge Lacerda 
B / Siderópolis C1 (ou C2) 
 
LT 230 kV Siderópolis / 
Siderópolis 2 C1 ou C2 
Sentido Normal: SE Siderópolis recebe tensão da SE Siderópolis 2 
Ligar, em anel, a LT 230 kV Siderópolis / 
Siderópolis 2 C1 (ou C2). 
•∆δ ≤ 20°. 
Sentido Inverso: SE Siderópolis envia tensão para a SE Siderópolis 2 
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.2SC. 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Siderópolis IO-OI.S.SID 35 3.7.5.3. 02/08/2024 
 
Referência:  11 / 12 
 
Linha de Transmissão / 
Equipamento  Procedimentos Condições ou Limites Associados 
LT 230 kV Siderópolis / 
Tubarão Sul 
Sentido Normal: SE Siderópolis envia tensão para SE Tubarão Sul 
Energizar a LT 230 kV Siderópolis / 
Tubarão Sul. 
Sistema completo (de LT e TR) ou 
N-1 ( de LT ou TR) na SE 
Siderópolis 230 kV 
•VSID-230 ≤ 242 kV. 
Sentido Inverso: SE Siderópolis recebe tensão da SE Tubarão Sul 
Ligar, em anel, a LT 230 kV Siderópolis / 
Tubarão Sul. 
 
Transformador TF 1, TF 3 ou 
TF 4 230/69/13,8 kV 
 
Sentido Único: A partir do lado de 230 kV 
Energizar, pelo lado de 230 kV, o 
transformador TF 1 230/69/13,8 kV (TF 3 
ou TF 4). 
Sistema completo (de LT) ou N-1 
(de LT) na SE Siderópolis 230 kV 
Como primeiro, segundo ou 
terceiro transformador: 
•VSID-230 ≤ 242 kV; e 
• -8 ≤ TAPSID-230/69 ≤ 0. 
Antes de energizar cada 
transformador, verificar fluxo de 
potência ativa nos 
transformadores 230/138/13,8 
kV que já foram energizados. 
Ligar, em anel, ou energizando o 
barramento de 69 kV, o lado de 69 kV do 
transformador TF 1 230/69/13,8 kV (TF 3 
ou TF 4). 
 
Em caso de abertura apenas do lado de 230 kV, proceder conforme 
segue: 
Ligar, em anel, o lado de 230 kV  do 
transformador TF 1 230/69/13,8 kV da 
SE Siderópolis (TF 3 ou TF 4).  
 
7. NOTAS IMPORTANTES 
7.1. Para substituir o disjuntor da LT 230 kV Siderópolis / Tubarão Sul  pelo disjuntor de interligação das 
barras P e T  230 kV, o COSR -S coordenará essa ação com a CGT Eletrosul e com a Cteep. Após tal 
substituição, os procedimentos para abertura do disjuntor ou recomposição da linha de transmissão 
deverão ser coordenados pelo COSR -S, que orientará a operação da CGT Eletrosul na execução dos 
procedimentos descritos nesta Instrução de Operação e na IO-PM.S.2SC – Preparação para Manobras 
da Área 230 kV de Santa Catarina. 
7.2. Para substituir o disjuntor da LT 230 kV Siderópolis / Siderópolis 2 C1 ou C2  pelo disjuntor de 
interligação das barras P e T 230 kV, o COSR-S coordenará essa ação com a CGT Eletrosul e com a EDP 
Brasil. Após tal substituição, os procedimentos para abertura do disjuntor ou recomposição da linha 
Manual de Procedimentos da Operação - Módulo 5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Siderópolis IO-OI.S.SID 35 3.7.5.3. 02/08/2024 
 
Referência:  12 / 12 
 
de transmissão deverão ser coordenados pelo COSR-S, que orientará a operação da CGT Eletrosul na 
execução dos procedimentos descritos nesta Instrução de Operação e na IO -PM.S.2SC – Preparação 
para Manobras da Área 230 kV de Santa Catarina. 
7.3. As tratativas de operação entre o ONS e a Cooperativa Energética Cocal  – Coopercocal são feitas 
diretamente com a Coopercocal, por meio do COD Coopercocal. As cargas da Coopercocal estão 
conectadas na subestação de 69 kV Cocal do Sul. 
7.4. No caso de desligamento total da SE Siderópolis, o restabelecimento de carga pela Coopercocal 
somente pode ser realizado após autorização do COSR-S. 
