Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação
Procedimentos Sistêmicos para a Operação da UHE Foz do Chapecó
Código Revisão Item Vigência
IO-OI.S.UHFC 15 3.7.5.2. 06/11/2023
.
MOTIVO DA REVISÃO
- Melhoria textual na tabela do item 6.2.2.2.
LISTA DE DISTRIBUIÇÃO
CNOS COSR-S FCE
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE Foz 
do Chapecó IO-OI.S.UHFC 15 3.7.5.2. 06/11/2023
Referência: 2/7
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. CONSIDERAÇÕES GERAIS .................................................................................................................3
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO..............................................................3
3.1. Unidades Geradoras......................................................................................................................3
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL......................................................3
4.1. Procedimentos Gerais ...................................................................................................................3
4.2. Procedimentos Específicos............................................................................................................4
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................................................4
5.1. Procedimentos Gerais ...................................................................................................................4
5.2.Procedimentos para Recomposição Fluente...............................................................................5
5.3. Procedimentos após Desligamento Total da Instalação................................................................5
5.3.1. Preparação da Instalação após Desligamento Total....................................................5
5.3.2. Recomposição após Desligamento Total da Instalação...............................................6
5.4. Procedimentos após Desligamento Parcial da Instalação .............................................................6
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ..............6
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................6
6. MANOBRAS DE UNIDADES GERADORAS ..........................................................................................6
6.1. Procedimentos Gerais ...................................................................................................................6
6.2. Procedimentos Específicos............................................................................................................6
6.2.1. Desligamento de Unidades Geradoras ........................................................................6
6.2.2. Sincronismo de Unidades Geradoras ..........................................................................7
7. NOTAS IMPORTANTES .....................................................................................................................7
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE Foz 
do Chapecó IO-OI.S.UHFC 15 3.7.5.2. 06/11/2023
Referência: 3/7
1. OBJETIVO
Estabelecer os procedimentos para a operação da UHE Foz do Chapecó, definidos pelo ONS, responsável 
pela coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos 
Procedimentos de Rede.
2. CONSIDERAÇÕES GERAIS
2.1. Os procedimentos contidos nesta Instrução de Operação são aqueles de interesse sistêmico, realizados 
com autonomia pelo Agente Operador da Instalação, devendo fazer parte do manual de operação 
próprio elaborado pelo Agente quando existente, observando-se a complementaridade das ações que 
devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS.
2.2. A comunicação operacional entre o COSR-S e a Instalação, de propriedade da FCE, é realizada pela FCE, 
agente responsável pela operação da Instalação, por intermédio da UHE Foz do Chapecó.
2.3. As unidades geradoras e equipamentos desta Instalação fazem parte da Área 230 kV do Rio Grande do 
Sul.
2.4. O COSR-S controla e supervisiona o despacho de geração desta Usina. 
2.5. Esta Usina:
está conectada na Rede Básica;
participa do Controle Automático da Geração – CAG;
é de autorrestabelecimento;
é fonte para início do processo de recomposição fluente da Área Foz do Chapecó;
é responsável pelo controle de frequência na Área Foz do Chapecó da Região Sul, na fase de 
recomposição fluente.
2.6. Os dados operacionais desta Usina estão descritos no Cadastro de Informações Operacionais da 
respectiva Área Elétrica.
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO
3.1. Unidades Geradoras 
As unidades geradoras GH 1, GH 2, GH 3 e GH 4 13,8 kV estão conectados diretamente aos barramentos 
de 230 kV pelos transformadores TE 1, TE 2, TE 3 e TE 4 230/13,8 kV da SE Foz do Chapecó, , com todas 
as seccionadoras e disjuntores fechados, exceto a seccionadora de transferência e uma das seletoras de 
barras de 230 kV. 
4. CONTROLE DE TENSÃO E DE GERAÇÃO NA OPERAÇÃO NORMAL
4.1. Procedimentos Gerais
4.1.1. A Usina deve manter os valores de geração de acordo com os valores programados, constantes no 
Programa Diário de Operação – PDO. Para atendimento dos valores programados constantes no PDO, 
não é necessária a autorização prévia do COSR-S.
Qualquer alteração no valor de geração da Usina em relação ao valor de geração ao constante no PDO 
somente pode ser executada após autorização do COSR-S.
As reprogramações de geração quando de necessidades sistêmicas serão executadas pela Usina 
quando solicitadas pelo COSR-S.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE Foz 
do Chapecó IO-OI.S.UHFC 15 3.7.5.2. 06/11/2023
Referência: 4/7
Após reprogramação de geração solicitada pelo COSR-S, o Agente somente poderá alterar a geração 
da Usina com autorização do ONS, inclusive para adoção de valores contidos no PDO.
4.1.2. Os desvios de geração da Usina em relação aos valores previstos no PDO ou em relação às 
reprogramações devem ser controlados observando-se os valores máximos permitidos explicitados na 
Instrução de Operação IO-CG.BR.01 – Controle da Geração em Condição Normal.
4.1.3. Quando não existir ou não estiver disponível a supervisão da Usina para o ONS, essa deve seguir as 
orientações para envio de dados conforme Rotina Operacional RO-AO.BR.08 – Apuração de Dados de 
Despacho de Geração e de Intercâmbios.
4.1.4. A Usina deve registrar e informar imediatamente os seguintes dados ao COSR-S:
movimentação de unidades geradoras hidráulicas (mudança de estado operativo / disponibilidade); 
restrições e ocorrências na Usina ou na conexão elétrica que afetem a disponibilidade de geração, 
com o respectivo valor da restrição, contendo o horário de início e término e a descrição do evento;
demais informações sobre a operação da Instalação, solicitadas pelo ONS.
4.1.5. O controle de tensão, por meio da geração ou absorção de potência reativa das unidades geradoras 
da Usina, é controlado pelo COSR-S, com comando e execução pela operação do Agente.
4.2. Procedimentos Específicos
4.2.1. Operação das Unidades Geradoras como Compensadores Síncronos
A conversão da modalidade de operação de gerador para compensador síncrono, bem como a 
reversão de compensador síncrono para gerador, deve ser executada sob controle do COSR-S.
Devem ser observados os procedimentos relacionados a prestação de Serviços Ancilares de Suporte 
de Reativos, conforme Instrução de Operação IO-CG.BR.01 – Controle da Geração em Condição 
Normal.
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO
5.1. Procedimentos Gerais
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir:
Desligamento total da Instalação: caracterizado por meio da verificação de ausência de tensão em 
todos os terminais de suas conexões ou ausência de fluxo de potência ativa nessas conexões.
Desligamento parcial da Instalação: qualquer outra configuração que não se enquadre como 
desligamento total.
5.1.2. Quando de desligamento total ou parcial, a operação da Instalação deve fornecer ao COSR-S as 
seguintes informações:
horário da ocorrência;
configuração da Instalação após a ocorrência;
configuração da Instalação após ações realizadas com autonomia pela operação dessa.
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2, sem necessidade de autorização prévia por parte do ONS. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE Foz 
do Chapecó IO-OI.S.UHFC 15 3.7.5.2. 06/11/2023
Referência: 5/7
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia do Agente Operador da Instalação na 
recomposição, deve ser utilizado o Subitem 5.3.
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.
5.2.Procedimentos para Recomposição Fluente
5.2.1. Preparação da Instalação para a Recomposição Fluente
No caso de desligamento total, o Agente Operador deve configurar os disjuntores das seguintes 
unidades geradoras, conforme condição apresentada a seguir.
Abrir ou manter abertos os disjuntores:
das unidades geradoras:
GH 1 13,8 kV (de 230 kV na SE Foz do Chapecó);
GH 2 13,8 kV (de 230 kV na SE Foz do Chapecó);
GH 3 13,8 kV (de 230 kV na SE Foz do Chapecó);
GH 4 13,8 kV (de 230 kV na SE Foz do Chapecó).
Cabe ao Agente FCE, informar ao COSR-S quando a configuração de preparação da Instalação não 
estiver atendida para o início da recomposição, independentemente de o equipamento ser próprio ou 
de outros agentes. Nesse caso, o COSR-S fará contato com os agentes envolvidos para identificar o 
motivo do não-atendimento e, após confirmação do Agente FCE de que os barramentos estão com a 
configuração atendida, o COSR-S coordenará os procedimentos para recomposição em função da 
configuração desta Instalação.
5.2.2. Recomposição Fluente da Instalação
A Instalação faz parte da recomposição da Área Foz do Chapecó. O Agente Operador deve adotar os 
procedimentos a seguir para a recomposição fluente:
Passo Executor Procedimentos Condições ou Limites Associados
1
Partir uma das unidades geradoras e fechar, 
energizando o barramento de 230 kV da SE 
Foz do Chapecó, o disjuntor e controlar a 
frequência em 60 Hz.
Tensão terminal em 13,1 kV.
1.1
Partir a segunda unidade geradora, caso essa 
estivesse operando antes do desligamento, 
sincronizá-la e controlar a frequência em 
60 Hz.
1.2
Partir a terceira unidade geradora, caso essa 
estivesse operando antes do desligamento, 
sincronizá-la e controlar a frequência em 
60 Hz.
1.3
FCE
Partir a quarta unidade geradora, caso essa 
estivesse operando antes do desligamento, 
sincronizá-la e controlar a frequência em 
60 Hz.
Tensão terminal em 13,1 kV.
O sincronismo será efetuado por 
meio do disjuntor na SE Foz do 
Chapecó.
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE Foz 
do Chapecó IO-OI.S.UHFC 15 3.7.5.2. 06/11/2023
Referência: 6/7
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2, enquanto não 
houver intervenção do COSR-S no processo de recomposição ou interrupção da autonomia deste na 
recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR-S, 
conforme procedimentos contidos nas respectivas Instruções de Preparação para Manobras ou na 
IO-RR.S.FCO.  
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL
A configuração não deve ser alterada pelo Agente Operador da Instalação até o início da 
recomposição da Instalação.
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
Devem ser utilizados os procedimentos descritos no Subitem 5.3.2, observando a configuração 
resultante do desligamento.
6. MANOBRAS DE UNIDADES GERADORAS 
6.1. Procedimentos Gerais
6.1.1. Os procedimentos para desligamento programado ou de urgência de unidades geradoras só podem 
ser efetuados com controle do COSR-S.
6.1.2. Os procedimentos para sincronismo de unidades geradoras, após desligamentos programados, de 
urgência ou de emergência, só podem ser efetuados com controle do COSR-S.
6.1.3. Os procedimentos para sincronismo de unidades geradoras, após desligamento automático sem 
atuação de proteção que impeça o retorno do equipamento, só podem ser executados com autonomia 
pelo Agente Operador da Instalação quando estiverem explicitados e estiverem atendidas as condições 
do Subitem 6.2.2. desta Instrução de Operação.
6.1.4. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente. 
6.1.5. A partida de unidades geradoras deve seguir critérios próprios do Agente. O sincronismo à Rede de 
Operação só pode ser executado com autonomia pela operação da Usina quando estiver explicitado e 
estiverem atendidas as condições do Subitem 6.2.2. A tomada de carga deve ser realizada com controle 
do COSR-S.
6.2.PROCEDIMENTOS ESPECÍFICOS
6.2.1. DESLIGAMENTO DE UNIDADES GERADORAS 
O desligamento de unidades geradoras é sempre controlado pelo COSR-S.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da UHE Foz 
do Chapecó IO-OI.S.UHFC 15 3.7.5.2. 06/11/2023
Referência: 7/7
6.2.2. SINCRONISMO DE UNIDADES GERADORAS
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S.
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de unidades geradoras.
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais unidades geradoras em operação, conforme explicitado nas condições de 
energização para a manobra.
Para os demais desligamentos parciais, proceder conforme Subitem 5.4.
Unidade Geradora Procedimentos Condições ou Limites Associados
Unidade Geradora
GH 1, GH 2, GH 3 ou 
GH 4 13,8 kV
Partir e sincronizar a unidade geradora. 13,11 ≤ V UHFC-13,8 ≤ 14,49 kV; e
elevar a geração da unidade 
geradora, após autorização do 
COSR-S.
7. NOTAS IMPORTANTES
Não se aplica.
