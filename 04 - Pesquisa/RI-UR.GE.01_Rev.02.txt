Administración del Mercado Eléctrico
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / ADME
REGLAMENTO INTERNACIONAL DE OPERACIÓN ONS / ADME
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
2
Data de Vigência / 
Fecha de Vigencia
20/04/2021
Referência:
Página 1 de 12
ÍNDICE
1. INTRODUÇÃO
2. ESTRUTURA DO REGULAMENTO
3. ABRANGÊNCIA
4. PRINCÍPIOS OPERACIONAIS BÁSICOS
5. TERMINOLOGIA OPERACIONAL
6. RELACIONAMENTO, PROCEDIMENTOS DE 
COMUNICAÇÃO E TROCA DE INFORMAÇÕES 
OPERACIONAIS
7. RECURSOS DE SUPERVISÃO, CONTROLE E 
TELECOMUNICAÇÃO NA INTERLIGAÇÃO
8. INTERVENÇÕES, ENSAIOS E TESTES EM 
EQUIPAMENTOS DE INTERLIGAÇÃO
9. COORDENAÇÃO DE MANOBRAS NA 
INTERLIGAÇÃO
10. CONTROLE DOS FLUXOS DE INTERCÂMBIO 
DE ENERGIA
11. REGULAÇÃO DOS NÍVEIS DE TENSÃO
12. ANÁLISE DA OPERAÇÃO E DE 
PERTURBAÇÕES NA INTERLIGAÇÃO
13. SOLUÇÃO DE CONTROVÉRSIAS TÉCNICO-
OPERACIONAIS
14. DISPOSIÇÕES GERAIS
15. APROVAÇÃO
ÍNDICE
1. INTRODUCCIÓN
2. ESTRUCTURA DEL REGLAMENTO
3. ALCANCE
4. PRINCIPIOS OPERACIONALES BÁSICOS
5. TERMINOLOGÍA OPERATIVA
6. RELACIONAMIENTO, PROCEDIMIENTOS 
DE COMUNICACIÓN E INTERCAMBIO DE 
INFORMACIONES OPERATIVAS
7. RECURSOS DE SUPERVISIÓN, CONTROL Y 
TELECOMUNICACIÓN EN LA 
INTERCONEXIÓN
8. TAREAS DE MANTENIMIENTO, ENSAYOS Y 
PRUEBAS EN EQUIPAMIENTOS DE 
INTERCONEXIÓN
9. COORDINACIÓN DE MANIOBRAS EN LA 
INTERCONEXIÓN
10. CONTROL DE LOS FLUJOS DE 
INTERCAMBIO DE ENERGÍA
11. REGULACIÓN DE LOS NIVELES DE 
TENSIÓN
12. ANÁLISIS DE LA OPERACIÓN Y DE 
PERTURBACIONES EN LA INTERCONEXIÓN
13. SOLUCIÓN DE CONTROVERSIAS TÉCNICO-
OPERATIVAS
14. DISPOSICIONES GENERALES
15. APROBACIÓN
Administración del Mercado Eléctrico
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / ADME
REGLAMENTO INTERNACIONAL DE OPERACIÓN ONS / ADME
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
2
Data de Vigência / 
Fecha de Vigencia
20/04/2021
Referência:
Página 2 de 12
1. INTRODUÇÃO 1. INTRODUCCIÓN
Este Regulamento, aprovado pelo Operador 
Nacional do Sistema Elétrico – ONS (Brasil) e pela 
Administración del Mercado Eléctrico – ADME 
(Uruguai), visa definir as regras e procedimentos a 
serem adotados na coordenação da operação das 
Interligações Livramento – Rivera e Candiota – 
Melo.
Este Regulamento constitui-se na revisão do 
Regulamento Internacional, firmado no dia 
02/05/2001, para a operação da interligação 
elétrica Brasil – Uruguai.
Para efeitos deste Regulamento, são válidos os 
seguintes termos e suas correspondentes 
abrangências específicas:
Agência Nacional de Energia Elétrica – ANEEL: 
autarquia sob regime especial que tem a finalidade 
de regular e fiscalizar a produção, transmissão, 
distribuição e comercialização de energia elétrica 
no Brasil.
Operador Nacional do Sistema Elétrico – ONS: 
o responsável pela operação, definição do 
intercâmbio de energia e de disponibilidade dos 
equipamentos do Sistema Brasileiro.
Centros de Operação do ONS: corresponde aos 
Centros de Operação próprios do ONS, 
designados por CNOS ou COSR.
Centro Nacional de Operação do Sistema –
CNOS: Centro de Operação de maior nível 
hierárquico do Operador Nacional do Sistema 
Elétrico – ONS, responsável pela coordenação, 
supervisão e controle da operação da Rede de 
Operação.
Centro de Operação do Sistema Sul – COSR-S: 
é um dos Centros de Operação de propriedade do 
ONS, de nível hierárquico imediatamente abaixo 
do CNOS, e responsável pela coordenação, 
supervisão e controle da operação na Região Sul 
do Brasil e estado do Mato Grosso do Sul.
Este Reglamento, aprobado por el Operador 
Nacional do Sistema Elétrico – ONS (Brasil) y por 
la Administración del Mercado Eléctrico – ADME 
(Uruguay), tiene por objeto definir las reglas y 
procedimientos a ser adoptados en la 
coordinación de la operación de las 
Interconexiones Livramento – Rivera y Candiota 
– Melo.
Este Reglamento se constituye en la revisión del 
Reglamento Internacional, firmado en el día 
02/05/2001, para la operación de la interconexión 
eléctrica Brasil – Uruguay.
A los efectos del presente Reglamento, los 
términos que a continuación se detallan tienen el 
alcance que se especifica:
Agência Nacional de Energia Elétrica – 
ANEEL: ente autárquico bajo un régimen 
especial cuyo objeto es regular e inspeccionar la 
producción, transmisión, distribución y 
comercialización de energía eléctrica en Brasil.
Operador Nacional do Sistema Elétrico – 
ONS: el responsable por la operación, definición 
de intercambios de energía y de disponibilidad de 
equipos del Sistema Brasileño.
Centros de Operación del ONS: centros de 
Operación propios del ONS, designados CNOS o 
COSR.
Centro Nacional de Operação do Sistema – 
CNOS: Centro de Operación de mayor nivel 
jerárquico del Operador Nacional del Sistema 
Eléctrico – ONS, responsable por la 
coordinación, supervisión y control de la 
operación de la Red de Operación.
Centro de Operação Sul – COSR-S: es el 
Centro de Operación de propiedad del ONS, de 
nivel jerárquico inmediatamente abajo del CNOS, 
y responsable por la coordinación, supervisión y 
control de la operación en la Región Sur del Brasil 
y estado de Mato Grosso do Sul.
Administración del Mercado Eléctrico
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / ADME
REGLAMENTO INTERNACIONAL DE OPERACIÓN ONS / ADME
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
2
Data de Vigência / 
Fecha de Vigencia
20/04/2021
Referência:
Página 3 de 12
Câmara de Comercialização de Energia Elétrica 
– CCEE: organização regida por regras 
claramente estabelecidas, na qual se processam a 
compra e venda de energia entre seus 
participantes, tanto por meio de contratos bilaterais 
como em regime de curto prazo, tendo como 
limites o Sistema Elétrico Interligado do Brasil.
Unidad Reguladora de los Servicios de Energía 
y Agua – URSEA: instituição estatal responsável 
pela regulação, supervisão e assessoria dos 
serviços de energia, combustível e água, no 
Uruguai.
Administración del Mercado Eléctrico – ADME: 
entidade pública não-estatal, responsável pela 
administração do mercado atacadista de energia 
elétrica e operação do sistema.
Despacho Nacional de Cargas – DNC: unidade 
da ADME responsável pela Programação e 
Operação do Sistema Interligado Nacional.
Despacho de Cargas da UTE – DCU: unidade da 
UTE que, no âmbito de um contrato de 
arrendamento de serviços com a ADME e na 
qualidade de prestadora de serviços, executa 
serviços de Operação e Controle em tempo real 
para o DNC, assim como a coordenação entre 
unidades de outros países dos intercâmbios de 
energia, determinação da disponibilidade de 
equipamentos e operações da rede do Sistema 
Uruguaio. Atualmente, esses serviços são 
prestados pela empresa estatal UTE, no âmbito de 
um contrato de arrendamento de serviços.
Dirección Nacional de Energía – DNE: a 
Unidade do Poder Executivo, subordinada ao 
Ministério de Indústria, Energia e Minas, 
responsável por elaborar, propor e coordenar as 
políticas na área energética no Uruguai.
Centrais Elétricas Brasileiras – ELETROBRAS: 
a empresa proprietária, no território brasileiro, das 
linhas de interligação Conversora Rivera / 
Câmara de Comercialização de Energia 
Elétrica – CCEE: organización regida por reglas 
claramente establecidas, en el cual se procesan 
la compra y venta de energía entre sus 
participantes, ya sea por medio de contratos 
bilaterales o en régimen de corto plazo, teniendo 
como límites el Sistema Eléctrico Interconectado 
de Brasil.
Unidad Reguladora de los Servicios de 
Energía y Agua – URSEA: institución estatal que 
se encarga de la regulación, fiscalización y 
asesoramiento de los servicios de energía, 
combustible y agua, en Uruguay.
Administración del Mercado Eléctrico – 
ADME: entidad pública no estatal, responsable 
de la administración del mercado mayorista de 
energía eléctrica y de la operación del sistema.
Despacho Nacional del Cargas – DNC: unidad 
de ADME responsable de la Programación y 
Operación del Sistema Interconectado Nacional.
Despacho de Cargas de UTE – DCU: unidad de 
UTE que, en el marco de un contrato de 
arrendamiento de servicios con ADME y en su 
calidad de prestador de servicios, ejecuta para el 
DNC los servicios de Operación y Control en 
tiempo real, así como las coordinaciones entre 
los despachos de otros países de los 
intercambios de energía, la determinación de la 
disponibilidad de los equipamientos y las 
operaciones de red del Sistema Uruguayo. En la 
actualidad, estos servicios los presta la empresa 
estatal UTE, en el marco de un contrato de 
arrendamiento de servicios.
Dirección Nacional de Energía – DNE: la 
Unidad del Poder Ejecutivo, dependiente del 
Ministerio de Industria, Energía y Minería, 
responsable de elaborar, proponer y coordinar 
las políticas en materia energética en Uruguay.
Centrais Elétricas Brasileiras – 
ELETROBRAS: la empresa propietaria, en 
territorio brasileño, de las líneas de interconexión 
Administración del Mercado Eléctrico
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / ADME
REGLAMENTO INTERNACIONAL DE OPERACIÓN ONS / ADME
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
2
Data de Vigência / 
Fecha de Vigencia
20/04/2021
Referência:
Página 4 de 12
Livramento 2 e Candiota / Conversora Melo, assim 
como dos equipamentos da SE Candiota e do 
módulo na SE Livramento 2.
Companhia de Geração e Transmissão de 
Energia Elétrica do Sul do Brasil – CGT 
ELETROSUL: a empresa responsável pela 
operação e manutenção dos equipamentos de 
propriedade da ELETROBRAS, associados à 
Interligação Livramento – Rivera e à Interligação 
Candiota – Melo.
Centro de Operação do Sistema CGT 
ELETROSUL – COSE: centro de operação do 
Sistema da CGT ELETROSUL.
Administracion Nacional de Usinas y 
Transmisiones Eléctricas – UTE: a empresa 
proprietária, no território uruguaio, das linhas de 
interligação Conversora Rivera / Livramento 2 e 
Candiota / Conversora Melo, bem como das 
Conversoras Rivera e Melo.
Conversora Rivera / Livramento 2 y Candiota / 
Conversora Melo, así como de los equipamientos 
de la S.E. Candiota y de la respectiva sección en 
la S.E. Livramento 2.
Companhia de Geração e Transmissão de 
Energia Elétrica do Sul do Brasil – CGT 
ELETROSUL: la empresa responsable por la 
operación y mantenimiento de los equipamientos 
de propriedad de la ELETROBRAS, asociados a 
la Interconexión Livramento – Rivera y a la 
Interconexión Candiota – Melo.
Centro de Operação do Sistema CGT 
ELETROSUL – COSE: es el centro de operación 
del Sistema de CGT ELETROSUL.
Administración Nacional de Usinas y 
Transmisiones Eléctricas – UTE: la empresa 
propietaria, en el territorio uruguayo, de las líneas 
de interconexión Conversora Rivera / Livramento 
2 y Candiota / Conversora Melo, así como de las 
estaciones conversoras Rivera y Melo.
2. ESTRUTURA DO REGULAMENTO
2.1. O Regulamento é composto pelos seguintes 
módulos:
- Módulo 1 – Normativo Geral;
- Módulo 2 – Terminologia Operacional;
- Módulo 3 – Procedimentos Operativos para 
Relacionamento, Comunicação e Troca de 
Informações;
- Módulo 4 – Programação de Intercâmbio de 
Energia e Procedimentos para a Solicitação e 
Execução de Intervenções;
- Módulo 5 – Operação da Interligação 
Internacional Livramento – Rivera;
- Módulo 6 – Operação da Interligação 
Internacional Candiota – Melo.
2.2. O Módulo 1 é de caráter normativo e é aprovado 
e assinado pelo Gerente Executivo de 
Procedimentos e Desenvolvimento da 
2. ESTRUCTURA DEL REGLAMENTO
2.1. El Reglamento está compuesto por los 
siguientes módulos:
- Módulo 1 – Normativa General;
- Módulo 2 – Terminología Operativa;
- Módulo 3 – Procedimientos Operativos para 
Relacionamiento, Comunicación e 
Intercambio de Informaciones;
- Módulo 4 – Programación de Intercambio de 
Energía y Procedimientos para la Solicitud y 
Ejecución de Intervenciones;
- Módulo 5 – Operación de la Interconexión 
Internacional Livramento – Rivera;
- Módulo 6 – Operación de la Interconexión 
Internacional Candiota – Melo.
2.2. El Módulo 1 es de carácter normativo y es 
aprobado y suscrito por el Gerente Ejecutivo 
de Procedimientos y Desarrollo de la 
Administración del Mercado Eléctrico
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / ADME
REGLAMENTO INTERNACIONAL DE OPERACIÓN ONS / ADME
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
2
Data de Vigência / 
Fecha de Vigencia
20/04/2021
Referência:
Página 5 de 12
Operação do ONS, do Brasil, e pelo Gerente de 
Técnica e Despacho Nacional de Cargas da 
ADME, do Uruguai.
2.3. Os demais módulos são de aplicação 
operacional, podendo ser revisados e 
atualizados em qualquer instância e de comum 
acordo pelas gerências dessas organizações.
2.4. Em caso de necessidade, outros módulos 
poderão ser elaborados para contemplar 
procedimentos específicos na operação das 
interligações.
Operación del ONS, de Brasil, y por el Gerente 
de Técnica y Despacho Nacional de Cargas de 
ADME, de Uruguay.
2.3. Los demás módulos son de aplicación 
operativa, pudiendo ser revisados y 
actualizados en cualquier instancia de común 
acuerdo por las gerencias de esas 
organizaciones.
2.4. En caso de necesidad, otros módulos podrán 
ser elaborados para contemplar 
procedimientos específicos en la operación de 
las interconexiones.
3. ABRANGÊNCIA
3.1. Equipamento: todos os equipamentos que 
compõem as interligações Livramento – Rivera 
e Candiota – Melo.
3.1.1. Equipamentos que compõem as Interligações 
Internacionais:
Interligação Internacional Candiota – Melo:
 Conversora Melo;
 LT 525 kV Candiota / Conversora Melo.
Interligação Internacional Livramento – 
Rivera:
 Conversora Rivera;
 LT 230 kV Conversora Rivera / Livramento 
2.
3.2. Intercâmbio: a coordenação operativa dos 
intercâmbios energéticos entre Brasil e Uruguai 
referentes à Interligação Livramento – Rivera e 
à Interligação Candiota – Melo.
3.3. Coordenação Internacional Operativa: a 
coordenação internacional é realizada pelo 
ONS, no Brasil, e pela ADME, no Uruguai.
3. ALCANCE
3.1. Equipamiento: todos los equipamientos que 
afecten las interconexiones Livramento – 
Rivera y Candiota – Melo.
3.1.1. Equipamientos que componen las 
Interconexiones Internacionales:
Interconexión Internacional Candiota – 
Melo:
 Conversora Melo;
 LT 525 kV Candiota / Conversora Melo.
Interconexión Internacional Livramento – 
Rivera:
 Conversora Rivera;
 LT 230 kV Conversora Rivera / 
Livramento 2.
3.2. Intercambio: la coordinación operativa de los 
intercambios energéticos entre Brasil y 
Uruguay referentes a la Interconexión 
Livramento – Rivera y a la Interconexión 
Candiota – Melo.
3.3. Coordinación Internacional Operativa: la 
coordinación internacional es realizada por el 
ONS, de Brasil, y por ADME, de Uruguay.
Administración del Mercado Eléctrico
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / ADME
REGLAMENTO INTERNACIONAL DE OPERACIÓN ONS / ADME
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
2
Data de Vigência / 
Fecha de Vigencia
20/04/2021
Referência:
Página 6 de 12
4. PRINCÍPIOS OPERACIONAIS BÁSICOS
4.1.O ONS e a ADME deverão cumprir as normas, 
procedimentos, metodologias, regras e critérios 
aplicáveis dos Módulos deste Regulamento, no 
que se refere ao controle de intervenções, 
operação, relacionamento, troca de informações 
e comunicação entre Centros de Operação, 
considerando as regras existentes em cada país.
4.2.O ONS e a ADME trocarão entre si diagramas 
unifilares das instalações de Livramento 2 e de 
Rivera, bem como de Candiota e de Melo, onde 
constará a identificação operacional de 
equipamentos a ser utilizada no relacionamento 
operacional entre ambos, comprometendo-se a 
fornecer cópia atualizada destes diagramas 
sempre que ocorrerem atualizações.
4.3.O ONS e a ADME deverão informar-se 
mutuamente sobre todas as modificações nas 
instalações que possam interferir na operação 
das interligações, tais como alterações em 
esquemas de proteção ou nos limites dos 
equipamentos.
4.4.O ONS e a ADME deverão trocar informações 
sobre as características dos equipamentos que 
permitam modelá-los para a realização de 
estudos.
4.5.Caso se verifiquem diferenças de interpretação ou 
de aplicação deste Regulamento, o ONS e a 
ADME procurarão chegar a um acordo o mais 
rápido possível, seguindo as prerrogativas do item 
13 deste Módulo.
4.6.O ONS e a ADME atuarão de maneira que a 
operação das interligações seja realizada de 
forma que não haja prejuízo para qualquer dos 
Sistemas.
4.7.O ONS e a ADME atuarão, durante todo processo 
de intervenção dos equipamentos das 
4. PRINCIPIOS OPERACIONALES BÁSICOS 
4.1.El ONS y la ADME deberán cumplir las normas, 
procedimientos, metodologías, reglas y 
criterios aplicables de los Módulos de este 
Reglamento, en lo referente al control de 
maniobras, la operación, el relacionamiento, el 
intercambio de informaciones y la 
comunicación entre sus Centros de 
Operaciones, considerando las reglas 
existentes en cada país.
4.2.El ONS y la ADME intercambiarán diagramas 
unifilares de las instalaciones de Livramento 2 
y de Rivera, así como de Candiota y de Melo, 
donde constará la identificación operativa de 
los equipamientos a ser utilizada en el 
relacionamiento operativo entre ambos, 
comprometiéndose a suministrar copia 
actualizada de estos diagramas siempre que 
hubiere actualizaciones.
4.3.El ONS y la ADME deberán informarse 
mutuamente todas las modificaciones en las 
instalaciones que puedan interferir en la 
operación de las Interconexiones, tales como 
alteraciones en esquemas de protección o en 
los límites de los equipos.
4.4.El ONS y la ADME deberán intercambiar 
información sobre las características de los 
equipos que permitan modelarlos para la 
realización de estudios.
4.5.Cuando se verifiquen diferencias de 
interpretación o de aplicación de este 
Reglamento, el ONS y la ADME procurarán 
llegar a un acuerdo lo más rápido posible, 
siguiendo las etapas del ítem 13 de este 
Módulo. 
4.6.El ONS y la ADME actuarán de manera tal que 
la operación de las interconexiones sea 
realizada de forma que no haya perjuicio para 
cualquiera de los Sistemas.
Administración del Mercado Eléctrico
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / ADME
REGLAMENTO INTERNACIONAL DE OPERACIÓN ONS / ADME
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
2
Data de Vigência / 
Fecha de Vigencia
20/04/2021
Referência:
Página 7 de 12
interligações, no sentido de manter a segurança e 
confiabilidade dos Sistemas Brasileiro e Uruguaio.
4.8.A segurança de pessoas e equipamentos 
envolvidos na execução das intervenções acima 
referidas é de responsabilidade das empresas 
proprietárias das instalações ou dos seus agentes 
operadores designados.
4.7.El ONS y la ADME actuarán, durante toda 
maniobra o ensayo de los equipamientos de 
interconexión, en el sentido de mantener la 
seguridad y confiabilidad de los Sistemas 
Brasileño y Uruguayo.
4.8.La seguridad de las personas y de los equipos 
involucrados en la ejecución de las maniobras 
y ensayos arriba referidos es responsabilidad 
de las empresas propietarias de las 
instalaciones o de sus agentes operativos 
designados.
5. TERMINOLOGIA OPERACIONAL
5.1.Os Operadores de Sistema do ONS e da ADME 
devem trocar informações relativas a seus 
Sistemas utilizando os termos e definições do 
Módulo 2 deste Regulamento.
5.2.Os Operadores do ONS se expressarão em 
Português e os Operadores da ADME, em 
Espanhol.
5. TERMINOLOGÍA OPERATIVA
5.1. Los Operadores del ONS y de la ADME 
deberán intercambiar informaciones relativas a 
sus Sistemas utilizando los términos y 
definiciones del Módulo 2 de este Reglamento.
5.2. Los Operadores del ONS se expresarán en 
idioma Portugués y los Operadores de la 
ADME, en idioma Español.
6. RELACIONAMENTO, PROCEDIMENTOS DE 
COMUNICAÇÃO E TROCA DE INFORMAÇÕES 
OPERACIONAIS
6.1.O relacionamento operativo e a troca de 
informações entre o ONS e a ADME deverão ser 
efetuados conforme procedimentos específicos 
do Módulo 3 deste Regulamento.
6. RELACIONAMIENTO, PROCEDIMIENTOS DE 
COMUNICACIÓN E INTERCAMBIO DE 
INFORMACIONES OPERATIVAS
6.1.El relacionamiento operativo y el intercambio de 
informaciones entre el ONS y la ADME deberán 
ser efectuados conforme a los procedimientos 
específicos del Módulo 3 de este Reglamento.
Administración del Mercado Eléctrico
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / ADME
REGLAMENTO INTERNACIONAL DE OPERACIÓN ONS / ADME
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
2
Data de Vigência / 
Fecha de Vigencia
20/04/2021
Referência:
Página 8 de 12
7. RECURSOS DE SUPERVISÃO, CONTROLE E 
TELECOMUNICAÇÃO NA INTERLIGAÇÕES
7.1.Os recursos de supervisão, controle e 
telecomunicação para a aquisição e troca de 
informações entre o ONS e a ADME estão 
descritos no Módulo 3 deste Regulamento.
7. RECURSOS DE SUPERVISIÓN, CONTROL Y 
TELECOMUNICACIÓN EN LA 
INTERCONEXIÓN
7.1.Los recursos de supervisión, control y 
telecomunicación para la adquisición e 
intercambio de información entre el ONS y la 
ADME se describen en el Módulo 3 de este 
Reglamento.
8. INTERVENÇÕES, ENSAIOS E TESTES EM 
EQUIPAMENTOS DA INTERLIGAÇÃO
8.1.As intervenções, testes e ensaios em qualquer 
equipamento componente das interligações 
devem ser solicitadas e realizadas conforme 
procedimentos específicos do Módulo 4 deste 
Regulamento.
8. TAREAS DE MANTENIMIENTO, ENSAYOS Y 
PRUEBAS EN EQUIPAMIENTOS DE 
INTERCONEXIÓN
8.1.Los mantenimientos, pruebas y ensayos en 
cualquier equipamiento componente de las 
Interconexiones deben ser solicitadas y 
realizadas conforme a los procedimientos 
específicos del Módulo 4 de este Reglamento.
9. COORDENAÇÃO DE MANOBRAS NA 
INTERLIGAÇÃO
9.1.As manobras em equipamentos das interligações, 
para serviços de manutenção ou durante a 
recomposição após perturbações, deverão ser 
coordenadas com as empresas envolvidas, 
conforme estabelecido no Módulo 3 deste 
Regulamento sobre relacionamento operativo.
9.2.Sempre que necessitem ser executadas 
manobras que afetem direta ou indiretamente as 
Interligações, os Centros de Operação do ONS e 
da ADME devem realizar contato entre si, de 
modo que cada Centro de Operação esteja ciente 
das manobras que o outro coordenará.
9.3.As manobras que, por motivos de segurança de 
pessoas ou integridade de equipamentos, 
precisem ser realizadas o mais rápido possível, 
não necessitam de acordo prévio entre os Centros 
de Operação. O Centro de Operação que efetuou 
9. COORDINACIÓN DE MANIOBRAS EN LA 
INTERCONEXIÓN
9.1.Las maniobras de los equipos de las 
interconexiones, para trabajos de 
mantenimiento o durante el restablecimiento 
luego de perturbaciones, deberán ser 
coordinadas con las empresas involucradas, 
conforme lo estabelecido en el Módulo 3 de 
este Reglamento sobre relacionamiento 
operativo.
9.2.Siempre que se necesite ejecutar maniobras 
que afecten directa o indirectamente a las 
Interconexiones, los Centros de Operaciones 
del ONS y del ADME deberán contactarse, de 
modo que cada Centro de Operaciones esté en 
conocimiento de las maniobras que el otro 
coordinará.
9.3.Las maniobras que, por motivos de seguridad 
de las personas o integridad de los 
equipamientos, deban ser realizadas lo más 
rápido posible, no necesitan de acuerdo previo 
Administración del Mercado Eléctrico
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / ADME
REGLAMENTO INTERNACIONAL DE OPERACIÓN ONS / ADME
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
2
Data de Vigência / 
Fecha de Vigencia
20/04/2021
Referência:
Página 9 de 12
a manobra deverá imediatamente informar o fato 
ao outro Centro de Operação.
9.4.A coordenação das manobras deve ser realizada 
de forma a que sejam atendidos os requisitos de 
ordem técnica e de segurança, objetivando obter 
o maior grau de confiabilidade na operação.
9.5.Os procedimentos para o restabelecimento das 
Interligações, após desligamentos voluntários ou 
involuntários, estão definidos em procedimentos 
específicos descritos nos módulos 5 e 6 deste 
Regulamento.
entre los Centros de Operaciones. El Centro de 
Operaciones que efectuó la maniobra deberá 
inmediatamente informar el hecho al otro 
Centro de Operaciones.
9.4.La coordinación de las maniobras debe ser 
realizada de forma tal que sean satisfechos los 
requisitos de orden técnico y de seguridad, 
teniendo como objetivo obtener el mayor grado 
de confiabilidad en la operación.
9.5.Los procedimientos para el restablecimiento de 
las Interconexiones, después de 
desconexiones voluntarias o involuntarias, 
están definidos en los procedimientos 
específicos descriptos en los módulos 5 y 6 de 
este Reglamento.
10. CONTROLE DOS FLUXOS DE INTERCÂMBIO 
DE ENERGIA
10.1. Para acompanhamento, em tempo real, dos 
fluxos de importação ou exportação de energia, 
devem ser consideradas as seguintes 
referências.
10.1.1. No lado do Brasil:
 na saída da LT 230 kV Conversora Rivera / 
Livramento 2, na SE Livramento 2; e 
 na saída da LT 525 kV Candiota / 
Conversora Melo, na SE Candiota.
10.1.2. No lado do Uruguai: 
 na saída da LT 230 kV Conversora Rivera / 
Livramento 2, na Conversora Rivera; e 
 na saída da LT 525 kV Candiota / 
Conversora Melo, na Conversora Melo.
10.2. Para efeito de acompanhamento de saldo de 
energia (em MWh) que não é comercializada, 
as perdas entre os pontos de entrega de 
energia em um país e recebimento no outro são 
assumidas pelo país importador. O ONS e a 
10. CONTROL DE LOS FLUJOS DE 
INTERCAMBIO DE ENERGÍA
10.1.Para monitorear, en tiempo real, los flujos de 
importación o exportación de energía, las 
siguientes referencias deben ser 
considerados.
10.1.1. En el lado de Brasil:
 en la salida de la LT 230 kV Conversora 
Rivera / Livramento 2, en Livramento 2; y
 en la salida de la LT 525 kV Candiota / 
Conversora Melo, en Candiota.
10.1.2. En el lado de Uruguay:
 en la salida de la LT 230 kV Conversora 
Rivera / Livramento 2, en Conversora 
Rivera; y
 en la salida de la LT 525 kV Candiota / 
Conversora Melo, en Conversora Melo.
10.2.Para efecto de monitoreo de balance de 
energía (en MWh) que no es comercializada, 
las pérdidas entre los puntos de entrega de 
energía en un país y que reciben en el otro son 
asumidos por el país importador. El ONS y la 
Administración del Mercado Eléctrico
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / ADME
REGLAMENTO INTERNACIONAL DE OPERACIÓN ONS / ADME
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
2
Data de Vigência / 
Fecha de Vigencia
20/04/2021
Referência:
Página 10 de 12
ADME devem fornecer os dados verificados à 
contraparte.
10.3. Qualquer modificação de ponto de referência 
deverá ser acordada entre ambas as partes.
10.4. O fluxo de energia nas interligações Livramento 
– Rivera e Candiota – Melo deve ser controlado 
pelos Centros de Operação do ONS e da 
ADME, de forma a respeitar os procedimentos 
e limites estabelecidos nos módulos 3, 5 e 6 
deste Regulamento.
ADME deben proporcionar los datos 
verificados a la contraparte.
10.3.Cualquier modificación de punto de referencia 
deberá ser acordada entre ambas partes.
10.4.El flujo de energía en las interconexiones 
Livramento – Rivera y Candiota – Melo debe 
ser controlado por los Centros de Operaciones 
del ONS y de la ADME, respetando los 
procedimientos y límites establecidos en los 
módulos 3, 5 y 6 de este Reglamento.
11. REGULAÇÃO DOS NÍVEIS DE TENSÃO
11.1. O controle de tensão de cada interligação 
deverá ser efetuado de maneira a manter os 
níveis de tensão dos Sistemas Brasileiro e 
Uruguaio dentro de suas respectivas faixas 
permitidas, conforme procedimentos e valores 
estabelecidos nos módulos 5 e 6 deste 
Regulamento.
11. REGULACIÓN DE LOS NIVELES DE 
TENSIÓN
11.1.El control de tensión en cada Interconexión 
deberá ser efectuado de manera de mantener 
los niveles de tensión de los Sistemas 
Brasileño y Uruguayo dentro de sus 
respectivas bandas permitidas, conforme 
procedimientos y valores establecidos en los 
módulos 5 y 6 de este Reglamento.
12. ANÁLISE DA OPERAÇÃO E DE 
PERTURBAÇÕES NA INTERLIGAÇÃO
12.1. A operação de cada interligação deverá ser 
analisada diariamente pela ADME e pelo ONS. 
As tratativas para análise diária, de 
perturbações e/ou troca de informações, 
deverão ser realizadas conforme 
procedimentos especificados no Módulo 3 
deste Regulamento.
12. ANÁLISIS DE LA OPERACIÓN Y DE 
PERTURBACIONES EN LA 
INTERCONEXIÓN
12.1. La operación de cada Interconexión deberá 
ser analizada diariamente por ADME y por el 
ONS. Las tratativas para el análisis diario, de 
las perturbaciones y/o el intercambio de 
informaciones, deberán ser realizadas 
conforme a los procedimientos especificados 
en el Módulo 3 de este Reglamento.
Administración del Mercado Eléctrico
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / ADME
REGLAMENTO INTERNACIONAL DE OPERACIÓN ONS / ADME
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
2
Data de Vigência / 
Fecha de Vigencia
20/04/2021
Referência:
Página 11 de 12
13. SOLUÇÃO DE CONTROVÉRSIAS TÉCNICO-
OPERACIONAIS
13.1. As partes farão seus melhores esforços para 
resolver todas as divergências de caráter 
técnico-operacional.
13.2. Uma divergência técnico-operacional que não 
puder ser resolvida pelos representados 
designados por cada órgão será considerada 
uma Controvérsia Operacional.
13.3. Constatada uma Controvérsia Operacional, as 
Gerências das áreas envolvidas do ONS e da 
ADME terão um prazo de até 10 dias úteis da 
constatação da Controvérsia para solucioná-la. 
Caso essa não seja solucionada pelas 
Gerências respectivas, a controvérsia deverá 
ser encaminhada aos representantes legais do 
ONS e da ADME, que poderão se reunir em 
Brasília, em Montevideo ou em qualquer outro 
lugar acordado, para solucionar a Controvérsia.
13. SOLUCIÓN DE CONTROVERSIAS 
TÉCNICO-OPERATIVAS
13.1. Las partes haran sus mejores esfuerzos para 
resolver todas las divergencias de carácter 
técnico-operativas.
13.2. Una divergencia técnico-operativa que no 
pueda ser resuelta por los representantes 
designados por cada organismo será 
considerada una Controversia Operativa.
13.3. Constatada una Controversia Operativa, las 
Gerencias de las áreas involucradas del ONS 
y de ADME tendrán un plazo de hasta 10 días 
hábiles de la constatación de una 
Controversia para solucionarla. En caso de 
que no sea solucionada por las Gerencias 
respectivas, la Controversia deberá ser 
elevada a los representantes legales de 
ADME y de ONS, que podrán reunirse en 
Brasilia, en Montevideo o en cualquier otro 
lugar acordado, para arribar a una solución.
14. DISPOSIÇÕES GERAIS
14.1. Toda vez que uma das partes julgar necessário 
proceder a revisão do presente Regulamento, 
poderá convocar uma reunião.
14.2. Toda modificação do Regulamento será feita 
por meio de substituição desse, que se 
denominará “Revisão Nº ____ do Módulo 
específico do Regulamento Internacional de 
Operação ONS / ADME", aprovado pelas 
partes. A revisão do Regulamento poderá ser 
realizada por cada Módulo individualmente.
14. DISPOSICIONES GENERALES
14.1. Toda vez que una de las partes juzgue 
necesario proceder a la revisión del presente 
Reglamento, podrá convocar a una reunión 
conjunta.
14.2. Toda modificación del Reglamento será 
realizada por medio de una sustitución de ese, 
la que se denominará “Revisión Nº ____ del 
Módulo específico del Reglamento 
Internacional de Operación ONS / ADME", 
aprobado por las partes. La revisión del 
Reglamento podrá ser realizada por cada 
Módulo individualmente.
Administración del Mercado Eléctrico
REGULAMENTO INTERNACIONAL DE OPERAÇÃO ONS / ADME
REGLAMENTO INTERNACIONAL DE OPERACIÓN ONS / ADME
Assunto / Asunto
NORMATIVO GERAL / NORMATIVA GENERAL
Módulo / 
Módulo
1
Revisão / 
Revisión
2
Data de Vigência / 
Fecha de Vigencia
20/04/2021
Referência:
Página 12 de 12
15. APROVAÇÃO
O Operador Nacional do Sistema Elétrico – ONS e 
a Administração do Mercado Elétrico – ADME, por 
intermédio de seus representantes autorizados, 
firmam o Normativo Geral, que compreende o 
Módulo 1 do presente Regulamento, redigido em 
Português e em Espanhol, ambos os textos de 
mesmo teor e validade, firmados no dia 16 de abril 
de 2021.
Este documento foi assinado digitalmente por Arthur da 
Silva Santa Rosa.
Para verificar as assinaturas vá ao site 
https://portalassinaturas.ons.org.br e utilize o código 
1F1F-9B46-E6C0-CED1.
_______________________________________
Eng. Arthur da Silva Santa Rosa
Gerente Executivo de Procedimentos e 
Desenvolvimento da Operação do ONS
15. APROBACIÓN
El Operador Nacional do Sistema Elétrico – ONS 
y la Administración del Mercado Eléctrico – 
ADME, por medio de sus representantes 
autorizados, firman la Normativa General, que 
comprende el Módulo 1 del presente 
Reglamento, redactado en idioma Portugués y en 
idioma Español, ambos textos del mismo tenor y 
validez, firmados en el día 16 de abril de 2021.
____________________________________
Ing. Ruben Chaer
Gerente de Técnica y Despacho Nacional de 
Cargas de ADME
