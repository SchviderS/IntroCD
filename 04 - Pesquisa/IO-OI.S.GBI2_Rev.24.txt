Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.12
Instrução de Operação para Procedimentos Sistêmicos da Instalação
Procedimentos Sistêmicos para a Operação da SE Conversora Garabi II
Código Revisão Item Vigência
IO-OI.S.GBI2 24 3.7.5.1. 03/06/2024
.
MOTIVO DA REVISÃO
- Alteração das condições de autonomia do Agente para fechamento em anel, no Subitem 6.2.2, 
desvinculando os procedimentos do Subitem 6.2.2 dos procedimentos de recomposição com autonomia 
após desligamento total da Instalação constantes no Subitem 5.2.2, da seguinte linha de transmissão:
•LT 525 kV Conversora Garabi II / Santo Ângelo.
- Complementação do Subitem 5.4.2.2.
- Adequação à RT-OI.BR revisão 36, com destaque à inclusão dos Subitens 6.1.8 e 6.1.9. 
- Exclusão dos subitens 7.1 e 7.2, devido ao Subitem 6.1.10.
LISTA DE DISTRIBUIÇÃO
CNOS COSR-S Taesa
(COS Saira)
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Conversora Garabi II IO-OI.S.GBI2 24 3.7.5.1. 03/06/2024
Referência: 2 / 11
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. CONSIDERAÇÕES GERAIS .................................................................................................................3
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO..............................................................5
3.1. Barramento de 525 kV / 60 Hz ......................................................................................................5
3.2. Barramento de 500 kV / 50 Hz ......................................................................................................5
3.3. Alteração da Configuração dos Barramentos................................................................................5
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL .............................................................................5
4.1. Procedimentos Gerais ...................................................................................................................5
4.2. Procedimentos Específicos............................................................................................................6
4.2.1. Operação dos Reatores ...............................................................................................6
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................................................6
5.1. Procedimentos Gerais ...................................................................................................................6
5.2. Procedimentos para Recomposição com Autonomia ...................................................................6
5.2.1. Preparação da Instalação para a Recomposição com Autonomia...............................6
5.2.2. Recomposição com Autonomia após Desligamento Total da Instalação ....................7
5.3. Procedimentos após Desligamento Total da Instalação................................................................7
5.3.1. Preparação da Instalação após Desligamento Total....................................................7
5.3.2. Recomposição após Desligamento Total da Instalação...............................................7
5.4. Procedimentos após Desligamento Parcial da Instalação .............................................................8
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ..............8
5.4.2. Recomposição após Desligamento Parcial da Instalação ............................................8
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS.....................................................8
6.1. Procedimentos Gerais ...................................................................................................................8
6.2. Procedimentos Específicos............................................................................................................9
6.2.1. Desenergização de Equipamentos ..............................................................................9
6.2.2. Energização de Linhas de Transmissão e de Equipamentos........................................9
7. NOTAS IMPORTANTES ...................................................................................................................11
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Conversora Garabi II IO-OI.S.GBI2 24 3.7.5.1. 03/06/2024
Referência: 3 / 11
1. OBJETIVO
Estabelecer os procedimentos para a operação da SE Conversora Garabi II, definidos pelo ONS, 
responsável pela coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos 
Procedimentos de Rede.
2. CONSIDERAÇÕES GERAIS
2.1. Os procedimentos contidos nesta Instrução de Operação são somente aqueles de interesse sistêmico, 
realizados com autonomia pelo Agente Operador da Instalação, devendo fazer parte do manual de 
operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade das 
ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS.
2.2. A comunicação operacional entre o COSR-S e a Instalação, no que se refere aos equipamentos de 
manobra da Instalação, é realizada conforme segue:
Linha de Transmissão ou 
Equipamento Agente de Operação Agente Operador Centro de Operação do 
Agente Operador
Barramento de 525 kV e de 
500 kV, lados de 60 Hz e de 
50 Hz, respectivamente
Taesa Taesa COS Saira
LT 525 kV Conversora Garabi II 
/ Santo Ângelo e Reatores RL 5 
e RL 6 (manobráveis de linha)
Taesa Taesa COS Saira
No território 
brasileiro: Taesa Taesa COS Saira
LT 500 kV Conversora Garabi II 
/ Rincón de Santa Maria (*)
No território 
argentino: TESA – 
Transportadora de 
Energia S.A.
-
A comunicação 
operacional para os 
equipamentos no território 
argentino é realizada entre 
o COSR-S e o COC da 
Cammesa, conforme 
Regulamento Internacional 
de Operação ONS / 
Cammesa
Conversor de Frequência P3, 
lados de 60 Hz e de 50 Hz Taesa Taesa COS Saira
Filtros de Harmônicos 60 Hz: 
FH 45, FH 46, FH 47 e FH 48 Taesa Taesa COS Saira
Filtros de Harmônicos 50 Hz: 
FH 33, FH 34, FH 35 e FH 36 Taesa Taesa COS Saira
Conversor de Frequência P4, 
lados de 60 Hz e de 50 Hz Taesa Taesa COS Saira
Filtros de Harmônicos 60 Hz: 
FH 37, FH 38, FH 39 e FH 40 Taesa Taesa COS Saira
Filtros de Harmônicos 50 Hz: 
FH 25, FH 26, FH 27 e FH 28 Taesa Taesa COS Saira
(*) Somente os equipamentos em território brasileiro fazem parte da Rede de Operação.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Conversora Garabi II IO-OI.S.GBI2 24 3.7.5.1. 03/06/2024
Referência: 4 / 11
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte das seguintes áreas elétricas:
Linha de Transmissão ou Equipamento Área Elétrica
Barramento de 525 kV e de 500 kV, lados de 60 
Hz e de 50 Hz, respectivamente 525 kV da Região Sul
LT 525 kV Conversora Garabi II / Santo Ângelo 525 kV da Região Sul 
No território brasileiro: 525 kV da Região SulLT 500 kV Conversora Garabi II / Rincón de Santa 
Maria No território argentino: Sistema Elétrico da 
Cammesa
Conversor de Frequência P3, lados de 60 Hz e de 
50 Hz
Filtros de Harmônicos 60 Hz: FH 45, FH 46, FH 47 
e FH 48
Filtros de Harmônicos 50 Hz: FH 33, FH 34, FH 35 
e FH 36
Conversor de Frequência P4, lados de 60 Hz e de 
50 Hz
Filtros de Harmônicos 60 Hz: FH 37, FH 38, FH 39 
e FH 40
Filtros de Harmônicos 50 Hz: FH 25, FH 26, FH 27 
e FH 28
525 kV da Região Sul
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos:
2.4.1. A definição da quantidade de tentativas de religamento manual de linha de transmissão ou de 
equipamento, bem como o intervalo entre elas, é de responsabilidade do Agente e devem estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica ou no Regulamento 
Internacional de Operação ONS – CAMMESA.
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar ao COSR-S autorização 
para religamento. Nessa oportunidade, o Agente pode solicitar também a alteração no sentido 
normal para envio de tensão, caso não tenha autonomia para tal.
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR-S levará em consideração as condições operativas do 
sistema.
2.5. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica ou no Regulamento Internacional de Operação ONS – CAMMESA.
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica ou no Regulamento Internacional de Operação ONS – CAMMESA.
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica caso a manobra seja realizada sob coordenação do COSR-S, ou os procedimentos descritos 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Conversora Garabi II IO-OI.S.GBI2 24 3.7.5.1. 03/06/2024
Referência: 5 / 11
no Subitem 6.2.2. desta Instrução de Operação quando o Agente tiver autonomia para energizar a 
linha de transmissão ou o equipamento.
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO
3.1. BARRAMENTO DE 525 KV / 60 HZ 
A configuração do barramento de 525 kV / 60 Hz (Barra 21A e Barra 21B) é do tipo Duplo.
•Com intercâmbio de energia entre a Argentina e o Brasil, na operação normal, todas as seccionadoras 
e disjuntores operam fechados.
•Quando não houver intercâmbio de energia entre a Argentina e o Brasil , na operação normal, todas 
as seccionadoras e disjuntores operam fechados, exceto os disjuntores GBI2 DJ 2130 e GBI2 DJ 2132 
do Conversor de Frequências P3, bem como, os disjuntores GBI2 DJ 2140 e GBI2 DJ 2142 do Conversor 
de Frequências P4.
3.2. BARRAMENTO DE 500 KV / 50 HZ 
A configuração do barramento de 500 kV / 50 Hz (Barra 11A e Barra 11B) é do tipo Duplo.
•Com intercâmbio de energia entre a Argentina e o Brasil, na operação normal, todas as seccionadoras 
e disjuntores operam fechados, exceto o disjuntor GBI2 DJ 112 da interligação da Barra 10A da SE 
Conversora Garabi I com a Barra 11A da SE Conversora Garabi II e o disjuntor GBI2 DJ 114 da 
interligação da Barra 10B da SE Conversora Garabi I com a Barra 11B da SE Conversora Garabi II.
•Quando não houver intercâmbio de energia entre a Argentina e o Brasil, na operação normal, todas 
as seccionadoras e disjuntores operam fechados, exceto os disjuntores GBI2 DJ 1130 e GBI2 DJ 1132 
do Conversor de Frequências P3, os disjuntores GBI2 DJ 1140 e GBI2 DJ 1142 do Conversor de 
Frequências P4, o disjuntor GBI2 DJ 112 da interligação da Barra 10A da SE Conversora Garabi I com 
a Barra 11A da SE Conversora Garabi II e o disjuntor GBI2 DJ 114 da interligação da Barra 10B da SE 
Conversora Garabi I com a Barra 11B da SE Conversora Garabi II.
3.3. ALTERAÇÃO DA CONFIGURAÇÃO DOS BARRAMENTOS
A mudança de configuração dos barramentos de 525 kV / 60 Hz e de 500 kV / 50 Hz desta Instalação é 
executada com controle do COSR-S.
A mudança de configuração dos demais barramentos é executada sob responsabilidade do Agente 
Operador da Instalação.
4.CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL
4.1. PROCEDIMENTOS GERAIS
4.1.1. Os barramentos de 500 kV / 50 Hz e de 525 kV / 60 Hz, pertencentes à Rede de Operação, têm sua 
regulação de tensão controlada pelo COSR-S.
As faixas de controle de tensão desses barramentos estão estabelecidas no Cadastro de Informações 
Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica e no Regulamento 
Internacional de Operação ONS – CAMMESA.
4.1.2. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob a responsabilidade da Instalação.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Conversora Garabi II IO-OI.S.GBI2 24 3.7.5.1. 03/06/2024
Referência: 6 / 11
4.2. PROCEDIMENTOS ESPECÍFICOS
4.2.1. OPERAÇÃO DOS REATORES
A operação dos reatores RL-5 e RL-6 kV 525 kV / 60 Hz – 85 Mvar é executada sob controle do COSR-S.
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO
5.1. PROCEDIMENTOS GERAIS
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir:
•Desligamento total da Instalação: caracterizado quando não há tensão em todos os terminais de 
suas linhas de transmissão.
•Desligamento parcial da Instalação: qualquer outra configuração que não se enquadre como 
desligamento total.
5.1.2. Quando de desligamento total ou parcial, o Agente Operador da Instalação deve fornecer ao COSR-S 
as seguintes informações:
•horário da ocorrência;
•configuração da Instalação após a ocorrência;
•configuração da Instalação após ações realizadas com autonomia pela operação dessa.
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2., sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia da operação da instalação na recomposição, 
deve ser utilizado o Subitem 5.3. 
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4. 
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO COM AUTONOMIA
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO COM AUTONOMIA
No caso de desligamento total, o Agente Operador deve configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir:
Abrir ou manter abertos os disjuntores:
•das linhas de transmissão: 
LT 525 kV Conversora Garabi II / Santo Ângelo;
LT 500 kV Conversora Garabi II / Rincón Santa Maria.
•do Conversor de Frequência P3, lados de 60 Hz e de 50 Hz:
Filtros de Harmônicos 60 Hz: FH 45, FH 46, FH 47 e FH 48;
Filtros de Harmônicos 50 Hz: FH 33, FH 34, FH 35 e FH 36.
•do Conversor de Frequência P4, lados de 60 Hz e de 50 Hz:
Filtros de Harmônicos 60 Hz: FH 37, FH 38, FH 39 e FH 40;
Filtros de Harmônicos 50 Hz: FH 25, FH 26, FH 27 e FH 28.
•das interligações:
GBI2 DJ 112 – da Barra 10 A da SE Conversora Garabi I com a Barra 11 A da SE Conversora Garabi II;
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Conversora Garabi II IO-OI.S.GBI2 24 3.7.5.1. 03/06/2024
Referência: 7 / 11
GBI2 DJ 114 – da Barra 10 B da SE Conversora Garabi I com a Barra 11 B da SE Conversora Garabi II;
GBI2 DJ 212 – da Barra 20 A da SE Conversora Garabi I com a Barra 21 A da SE Conversora Garabi II;
GBI2 DJ 214 – da Barra 20 B da SE Conversora Garabi I com a Barra 21 B da SE Conversora Garabi II.
5.2.2. RECOMPOSIÇÃO COM AUTONOMIA APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
O Agente Operador da Instalação deve realizar com autonomia os seguintes procedimentos para a 
recomposição:
Passo Executor Procedimentos Condições ou Limites Associados
1
Taesa
(COS Saira)
Conectar ou manter conectados os reatores RL 
5 e RL 6 525 kV / 60 Hz da SE Conversora Garabi 
II na LT 525 kV Conversora Garabi II / Santo 
Ângelo.
1.1
Taesa
(COS Saira)
Solicitar, à SE Santo Ângelo, o envio de tensão.
2
Taesa
(COS Saira)
Verificar o recebimento da tensão da SE 
Conversora Garabi I.
2.1
Taesa
(COS Saira)
Energizar as barras 21A e 21B de 525 kV / 60 Hz 
da SE Conversora Garabi II.
3
Taesa
(COS Saira)
Receber tensão da SE Santo Ângelo pela LT 
525 kV Conversora Garabi II / Santo Ângelo e 
ligar, em anel.
4
Taesa
(COS Saira)
Receber tensão da SE Rincón Santa Maria pela 
LT 500 kV Conversora Garabi II / Rincón Santa 
Maria e energizar o barramento de 500 kV / 
50 Hz.
Permitido somente com 
autorização do COSR-S.
4.1
Taesa
(COS Saira)
A recomposição dos conversores de frequência 
P3 e P4 da SE Conversora Garabi II é permitida 
somente com autorização do COSR-S.
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL
O Agente Operador da Instalação deve realizar a preparação conforme Subitem 5.2.1.
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO
O Agente Operador da Instalação deve realizar os procedimentos do Subitem 5.2.2., enquanto não houver 
intervenção do COSR-S no processo de recomposição ou interrupção da autonomia da operação da instalação 
na recomposição. 
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR-S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Conversora Garabi II IO-OI.S.GBI2 24 3.7.5.1. 03/06/2024
Referência: 8 / 11
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja: 
•ausência de tensão em todos os barramentos,
•ausência de fluxo de potência ativa nas linhas de transmissão, e
•existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, 
o Agente Operadors deve preparar a Instalação conforme Subitem 5.2.1., sem necessidade de 
autorização do ONS.
5.4.1.2. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação.
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., o Agente Operador 
deve recompor a Instalação conforme Subitem 5.2.2., sem necessidade de autorização do ONS.
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.2., o Agente Operador 
deve recompor a Instalação conforme Subitem 6.2.2., sem necessidade de autorização do ONS.   
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
o Agente Operador deve informar ao COSR-S, para que a recomposição da Instalação seja 
executada com controle do COSR-S.
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS
6.1. PROCEDIMENTOS GERAIS
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S.
6.1.2. Os procedimentos para energização de linhas de transmissão ou de equipamentos, após desligamento 
programado, de urgência ou de emergência, só podem ser efetuados com controle do COSR-S.
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelo Agente Operador da Instalação quando 
explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução de Operação.
Quando as condições ou limites associados não estiverem atendidos ou quando não existir autonomia, 
a energização deve ser executada com controle do COSR-S, conforme Instrução de Operação de 
Preparação para Manobras da respectiva área elétrica ou o Regulamento Internacional de Operação 
ONS – CAMMESA.
6.1.4. Antes do fechamento de qualquer disjuntor, o Agente Operador da Instalação deve verificar se existe 
tensão de retorno e se a condição de fechamento será em anel.
O fechamento em anel só pode ser executado com autonomia pelo Agente Operador da Instalação 
quando estiver especificado nesta Instrução de Operação e estiverem atendidas as condições do 
Subitem 6.2.2.
O fechamento de paralelo só pode ser efetuado com controle do COSR-S.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Conversora Garabi II IO-OI.S.GBI2 24 3.7.5.1. 03/06/2024
Referência: 9 / 11
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento:
6.1.5.1. A energização em sentido normal ou, quando permitida, em sentido inverso, pode ser feita com 
autonomia pelo Agente Operador, conforme procedimentos para manobras que estão definidos 
nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização controlados pelo 
COSR-S estão definidos na Instrução de Operação de Preparação para Manobras da respectiva área 
elétrica.
A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente. 
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o Agente Operador da Instalação deve fechá-lo em anel desde que tenha autonomia para 
tal, adotando as condições para o fechamento constantes no Subitem 6.2.2.
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S. 
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S.
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra.
6.2. PROCEDIMENTOS ESPECÍFICOS
6.2.1. DESENERGIZAÇÃO DE EQUIPAMENTOS
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação, é 
sempre controlada pelo COSR-S.
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S.
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelo Agente Operador da Instalação, após 
desligamento automático de equipamentos ou de linhas de transmissão.
O Agente Operador da Instalação deve identificar os desligamentos automáticos observando na 
Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado nas 
condições de energização para a manobra.
Para os demais desligamentos parciais, proceder conforme Subitem 5.4.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Conversora Garabi II IO-OI.S.GBI2 24 3.7.5.1. 03/06/2024
Referência: 10 / 11
Linha de Transmissão / 
Equipamento Procedimentos Condições ou Limites Associados
Interligação da Barra 20 A 
(ou 20 B) 525 kV 60 Hz da 
SE Conversora Garabi I com 
a Barra 21 A (ou 21 B) 
525 kV 60 Hz da SE 
Conversora Garabi II 
Interligar, na SE Conversora Garabi II, 
em anel ou energizando o barramento:  
•a Barra 20 A 525 kV GBI1 com a 
Barra 21 A 525 kV GBI2, ou
•a Barra 20 B 525 kV GBI1 com a 
Barra 21 B 525 kV GBI2.
•V GBI1 e 2-525 ≤ 550 kV.
ou
•V GBI1 e 2-525 ≤ 525 kV, se apenas 
uma das conversoras, Garabi I 
ou II, estiver transferindo 
potência (importando ou 
exportanto) entre a Argentina e 
o Brasil.
Sentido Normal: SE Conversora Garabi II recebe tensão da SE Santo 
Ângelo
Conectar ou manter conectados os 
reatores RL 5 e RL 6 525 kV / 60 Hz da 
SE Conversora Garabi II na LT 525 kV 
Conversora Garabi II / Santo Ângelo. 
A conexão dos reatores RL 5 e RL 6 
525 kV/60 Hz na LT, com autonomia 
pela Taesa, somente é realizada 
caso os dois reatores estivessem em 
operação antes do desligamento da 
LT.
Nas demais situações, solicitar 
coordenação do COSR-S. 
Solicitar à SE Santo Ângelo o envio de 
tensão.
Ligar, em anel, a LT 525 kV Conversora 
Garabi II / Santo Ângelo.
Sem transferência de energia: 
•V GBI2-525 ≤ 550 kV.
Com transferência de energia:
•BRA → ARG: V GBI2-525 ≤ 543 kV;
•ARG → BRA: V GBI2-525 ≤ 530 kV.
Sentido Inverso: SE Conversora Garabi II envia tensão para SE Santo 
Ângelo
LT 525 kV Conversora 
Garabi II / Santo Ângelo
Permitida somente com coordenação do COSR-S, conforme IO-PM.S.5SU.
Sentido Único: SE Conversora Garabi II recebe tensão da SE Rincón de 
Santa Maria
Permitido somente com autorização do COSR-S.
LT 500 kV Conversora 
Garabi II / Rincón de Santa 
Maria
Ligar, energizando o barramento de 
500 kV / 50 Hz, a LT 500 kV Conversora 
Garabi II / Rincón de Santa Maria.
Ligar o Conversor de Frequência P3 (ou P4)
Somente pode ser ligado com autorização do COSR-S.
Conversor de Frequência
P3 ou P4
Recompor o conversor de frequência 
P3 (ou P4), conforme instruções 
próprias, havendo tensão da SE Santo 
Ângelo e da SE Rincón de Santa Maria.
VGBI2-525 ≤ 550 kV.
VGBI2-500 ≤ 515 kV.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12
 Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência
Procedimentos Sistêmicos para a Operação da SE 
Conversora Garabi II IO-OI.S.GBI2 24 3.7.5.1. 03/06/2024
Referência: 11 / 11
Linha de Transmissão / 
Equipamento Procedimentos Condições ou Limites Associados
Desligar o Conversor de Frequência P3 (ou P4)
Somente pode ser desligado com autorização do COSR-S.
Providenciar o desligamento do 
Conversor de Frequência P3 (ou P4), 
conforme instruções próprias.
7. NOTAS IMPORTANTES
7.1. Não se aplica.
