 
Manual de Procedimentos da Operação 
Módulo 5 - Submódulo 5.12 
 
Instrução de Operação para Procedimentos Sistêmicos da Instalação 
Procedimentos Sistêmicos para a Operação da SE Guaíba 3 
 
 
Código Revisão Item Vigência 
IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
. 
 
MOTIVO DA REVISÃO 
- Início da Operação do Compensador Estático CE1 da SE Guaíba 3 , com alterações nos subitens 2.2., 2.3., 
5.2.1., 5.2.2. e 6.2.2., e com inclusão dos subitens 4.2.3., 7.2. e 7.3. 
- Incorporação da parte correspondente do conteúdo da Mensagem Operativa MOP/ONS 372 -R/2024 - 
"Mudança da denominação da CEEE-T e do seu centro de operação". 
 
 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-S CPFL-T Cymi Neoenergia 
(COT) 
CGT Eletrosul 
(COT Sul) MEZ    
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Guaíba 3 IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
Referência:  2 / 16 
 
ÍNDICE 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONSIDERAÇÕES GERAIS ................................ ................................ ................................ .............. 3 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO ................................ ............................  5 
3.1. Barramento de 525 kV ................................................................................................................... 5 
3.2. Barramento de 230 kV ................................................................................................................... 5 
3.3. Alteração da Configuração dos Barramentos ................................................................................ 6 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL ................................ ................................ ........... 6 
4.1. Procedimentos Gerais ................................................................................................................... 6 
4.2. Procedimentos Específicos ............................................................................................................ 6 
4.2.1. Operação dos Comutadores de Tapes Sob Carga (LTC) .............................................. 6 
4.2.2. Operação dos Reatores ............................................................................................... 6 
4.2.3. Operação do Compensador Estático ........................................................................... 6 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO ................................ ..............................  6 
5.1. Procedimentos Gerais ................................................................................................................... 6 
5.2. Procedimentos para Recomposição com Autonomia ................................................................... 7 
5.2.1. Preparação da Instalação para a Recomposição com Autonomia .............................. 7 
5.2.2. Recomposição com Autonomia após Desligamento Total da Instalação.................... 8 
5.3. Procedimentos após Desligamento Total da Instalação ............................................................. 10 
5.3.1. Preparação da Instalação após Desligamento Total ................................................. 10 
5.3.2. Recomposição após Desligamento Total da Instalação ............................................ 10 
5.4. Procedimentos após Desligamento Parcial da Instalação ........................................................... 11 
5.4.1. Preparação da Instalação para a Recomposição após Desligamento Parcial ........... 11 
5.4.2. Recomposição após Desligamento Parcial da Instalação .......................................... 11 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS ................................ ................. 12 
6.1. Procedimentos Gerais ................................................................................................................. 12 
6.2. Procedimentos Específicos .......................................................................................................... 13 
6.2.1. Desenergização de Equipamentos ............................................................................ 13 
6.2.2. Energização de Linhas de Transmissão e de Equipamentos ..................................... 13 
7. NOTAS IMPORTANTES ................................ ................................ ................................ ................ 16 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Guaíba 3 IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
Referência:  3 / 16 
 
1. OBJETIVO 
Estabelecer os procedimentos para a operação da SE Guaíba 3, definidos pelo ONS, responsável pela 
coordenação, supervisão e controle da Rede de Operação, conforme estabelecido nos Procedimentos de 
Rede. 
2. CONSIDERAÇÕES GERAIS 
2.1. Os procedimentos contidos nesta Instrução de Operação são somente aqueles de interesse sistêmico, 
realizados com autonomia pelos Agentes Operadores da Instalação, devendo fazer parte do manual 
de operação próprio elaborado pelo Agente quando existente, observando-se a complementaridade 
das ações que devem ser realizadas com coordenação e controle pelos Centros de Operação do ONS. 
2.2. A comunicação operacional entre o COSR -S e a Instalação, no que se refere aos equipamentos de 
manobra da Instalação, é realizada conforme segue: 
Linha de Transmissão ou 
Equipamento  Agente de Operação Agente Operador Centro de Operação 
do Agente Operador 
Barramento de 525 kV Chimarrão Cymi COT Cymi 
Barramento de 230 kV  Chimarrão Cymi COT Cymi 
LT 525 kV Capivari do Sul / 
Guaíba 3 e Reator RE7 525 kV 
(manobrável de linha) 
Pampa Transmissão de 
Energia (linha de 
transmissão e módulo) Cymi COT Cymi Chimarrão 
(interligação de 
módulos) 
LT 525 kV Gravataí / Guaíba 3 Chimarrão Cymi COT Cymi 
LT 525 kV Guaíba 3 / Nova 
Santa Rita C1 
CGT Eletrosul (linha de 
transmissão e módulo) 
CGT Eletrosul 
(*) COT Sul 
Chimarrão 
(interligação de 
módulos) 
Cymi COT Cymi 
LT 525 kV Guaíba 3 / Nova 
Santa Rita C2 Chimarrão Cymi COT Cymi 
LT 525 kV Guaíba 3 / Povo 
Novo C1 
CGT Eletrosul (linha de 
transmissão e módulo) 
CGT Eletrosul 
(*) COT Sul 
Chimarrão 
(interligação de 
módulos) 
Cymi COT Cymi 
LT 525 kV Guaíba 3 / Povo 
Novo C2 Chimarrão Cymi COT Cymi 
LT 525 kV Guaíba 3 / Povo 
Novo C3 EKTT5 Neoenergia COT Neoenergia 
LT 525 kV Candiota 2 / Guaíba 
3 C1 Chimarrão Cymi COT Cymi 
LT 525 kV Candiota 2 / Guaíba 
3 C2 Chimarrão Cymi COT Cymi 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Guaíba 3 IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
Referência:  4 / 16 
 
Linha de Transmissão ou 
Equipamento  Agente de Operação Agente Operador Centro de Operação 
do Agente Operador 
LT 230 kV Guaíba 2 / Guaíba 3 
C1 Chimarrão Cymi COT Cymi 
LT 230 kV Guaíba 2 / Guaíba 3 
C2 Chimarrão Cymi COT Cymi 
LT 230 kV Charqueadas 3 / 
Guaíba 3 MEZ 5 MEZ COZ MEZ Energia 
Transformador TF1 
 525/230/13,8 kV Chimarrão Cymi COT Cymi 
Transformador TF2 
 525/230/13,8 kV Chimarrão Cymi COT Cymi 
Reator RE1 525 kV – 100 Mvar 
(manobrável de barra) Chimarrão Cymi COT Cymi 
Reator RE2 525 kV – 100 Mvar 
(manobrável de barra) Chimarrão Cymi COT Cymi 
Compensador Estático CE1 MEZ 5 MEZ COZ MEZ Energia 
(*) Nos casos em que a linha de transmissão ou equipamento compartilhem equipamentos de manobra com 
mais de um agente, o agente operador responsável pela LT / equipamento é apresentado em negrito. 
2.3. Os equipamentos e linhas de transmissão desta Instalação fazem parte das seguintes áreas elétricas: 
Linha de Transmissão ou Equipamento Área Elétrica 
Barramento 525 kV Área 525 kV da Região Sul 
Barramento 230 kV Área 230 kV do Rio Grande do Sul 
LT 525 kV Capivari do Sul / Guaíba 3 e Reator 
RE7 525 kV  525 kV da Região Sul 
LT 525 kV Gravataí / Guaíba 3 Área 525 kV da Região Sul 
LT 525 kV Guaíba 3 / Nova Santa Rita C1 Área 525 kV da Região Sul 
LT 525 kV Guaíba 3 / Nova Santa Rita C2 Área 525 kV da Região Sul 
LT 525 kV Guaíba 3 / Povo Novo C1 Área 525 kV da Região Sul 
LT 525 kV Guaíba 3 / Povo Novo C2 Área 525 kV da Região Sul 
LT 525 kV Guaíba 3 / Povo Novo C3 Área 525 kV da Região Sul 
LT 525 kV Candiota 2 / Guaíba 3 C1 Área 525 kV da Região Sul 
LT 525 kV Candiota 2 / Guaíba 3 C2 Área 525 kV da Região Sul 
LT 230 kV Guaíba 2 / Guaíba 3 C1 Área 230 kV do Rio Grande do Sul 
LT 230 kV Guaíba 2 / Guaíba 3 C2 Área 230 kV do Rio Grande do Sul 
LT 230 kV Charqueadas 3 / Guaíba 3 Área 230 kV do Rio Grande do Sul 
Transformador TF1 525/230/13,8 kV Área 525 kV da Região Sul 
Transformador TF2 525/230/13,8 kV Área 525 kV da Região Sul 
Reator RE1 525 kV Área 525 kV da Região Sul 
Reator RE2 525 kV Área 525 kV da Região Sul 
Compensador Estático CE1 Área 525 kV da Região Sul 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Guaíba 3 IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
Referência:  5 / 16 
 
2.4. No que se refere ao religamento manual de linhas de transmissão ou de equipamentos: 
2.4.1. A definição da quantidade  de tentativas de religamento manual de linha de transmissão ou de 
equipamento, bem como o intervalo entre elas, é de responsabilidade do Agente e deve m estar 
descritos no Cadastro de Informações Operacionais da respectiva Área Elétrica. 
2.4.2. Depois de efetuadas as tentativas de religamento manual previstas pelo Agente, e não havendo 
sucesso, esse deve definir a necessidade de tentativas adicionais e solicitar autorização ao COSR -S 
para religamento. Nessa oportunidade, o Agente pode solicitar t ambém a alteração no sentido 
normal para envio de tensão, caso não tenha autonomia para tal. 
Para a referida autorização, além de buscar obter informações com o Agente, para diagnóstico das 
possíveis causas do desligamento, o COSR -S levará em consideração as condições operativas do 
sistema. 
2.5. Quando caracterizado impedimento de linha de transmissão ou de equipamento, devem ser 
adotados os procedimentos descritos na instrução de operação em contingência da respectiva área 
elétrica. 
2.6. Para manobra de desenergização de linha de transmissão ou de equipamento, devem ser adotados 
os procedimentos descritos na instrução de operação de preparação para manobras da respectiva 
área elétrica. 
2.7. Para manobra de energização de linha de transmissão ou de equipamento, devem ser adotados os 
procedimentos descritos na instrução de operação de preparação para manobras da respectiva área 
elétrica, caso a manobra seja realizada sob coordenação do COSR -S, ou os procedimentos descritos 
no Subitem 6.2.2. desta Instrução de Operação , quando o Agente  tiver autonomia para energizar a 
linha de transmissão ou o equipamento. 
3. CONFIGURAÇÃO NORMAL DE OPERAÇÃO DA INSTALAÇÃO 
3.1. BARRAMENTO DE 525 KV 
A configuração do barramento de 525 kV (Barra A e Barra B) é do tipo Disjuntor e Meio. Na operação normal 
desse barramento, todos os disjuntores e seccionadoras devem estar fechados. 
3.2. BARRAMENTO DE 230 KV 
A configuração do barramento de 230 kV é do tipo Barra Dupla (Barra A e Barra B) a Quatro Chaves. Na 
operação normal desse barramento, todos os disjuntores e seccionadoras devem estar fechados, exceto as 
seccionadoras de transferência e uma das seletoras de barra das linhas de transmissão ou equipamentos.  
A Instalação deve operar em condição normal com a seguinte configuração: 
Em uma das Barras – A (ou B) Na outra Barra – B (ou A) 
LT 230 kV Guaíba 2 / Guaíba 3 C1 (ou C2) LT 230 kV Guaíba 2 / Guaíba 3 C2 (ou C1) 
LT 230 kV Charqueadas 3 / Guaíba 3 - 
Transformador TF1 525/230/13,8 kV (ou TF2) Transformador TF2 525/230/13,8 kV (ou TF1) 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Guaíba 3 IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
Referência:  6 / 16 
 
3.3. ALTERAÇÃO DA CONFIGURAÇÃO DOS BARRAMENTOS 
A mudança de configuração dos barramentos de 525 kV e 230 kV desta Instalação é executada com controle 
do COSR-S. 
A mudança de configuração dos demais barramentos é executada com autonomia pelos Agentes Operadores 
da Instalação. 
4. CONTROLE DE TENSÃO NA OPERAÇÃO NORMAL 
4.1. PROCEDIMENTOS GERAIS 
4.1.1. Os barramentos de 525 kV e de 230 kV , pertencentes à Rede de Operação, t êm a sua regulação de 
tensão controlada pelo COSR-S.  
As faixas para controle de tensão para os barramentos de 525 kV e de 230 kV estão estabelecidas no 
Cadastro de Informações Operacionais de Faixas para Controle de Tensão da respectiva Área Elétrica. 
4.1.2. Os demais barramentos, não pertencentes à Rede de Operação, têm a sua regulação de tensão 
executada sob a responsabilidade da Instalação. 
4.2. PROCEDIMENTOS ESPECÍFICOS 
4.2.1. OPERAÇÃO DOS COMUTADORES DE TAPES SOB CARGA (LTC) 
Os LTCs dos transformadores TF1 e TF2 525/230/13,8 kV operam em modo manual.  
A movimentação dos comutadores é realizada com controle do COSR-S. 
4.2.2. OPERAÇÃO DOS REATORES 
A manobra dos reatores RE1 e RE2 525 kV – 100 Mvar e o RE7 525 kV – 50 Mvar é executada sob controle do 
COSR-S. 
4.2.3. OPERAÇÃO DO COMPENSADOR ESTÁTICO 
Em condição de operação normal, o compensador estático CE1 17,5 kV – 180 / + 300 Mvar opera regulando 
a tensão do barramento de 525 kV desta Instalação. 
As ações de alteração no modo de operação, mudança da referência de tensão, bem como qualquer variação 
de geração de potência reativa, deve ser executada com controle do COSR-S. 
5. RECOMPOSIÇÃO APÓS DESLIGAMENTO DA INSTALAÇÃO 
5.1. PROCEDIMENTOS GERAIS 
5.1.1. Quando de desligamento da Instalação, a operação dessa deve identificar o desligamento e a 
configuração da Instalação, conforme critério a seguir: 
• Desligamento total da Instalação : caracterizado quando não há tensão em todos os terminais de 
suas linhas de transmissão. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Guaíba 3 IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
Referência:  7 / 16 
 
• Desligamento parcial da Instalação : qualquer outra configuração  que não se enquadre como 
desligamento total. 
5.1.2. Quando de desligamento total ou parcial, os Agentes Operadores da Instalação devem fornecer ao 
COSR-S as seguintes informações: 
• horário da ocorrência; 
• configuração da Instalação após a ocorrência; 
• configuração da Instalação após ações realizadas com autonomia pela operação dessa. 
5.1.3. Caracterizado desligamento total da Instalação, a operação dessa deve adotar os procedimentos de 
recomposição constantes no Subitem 5.2., sem necessidade de autorização prévia por parte do ONS. 
Caso o ONS intervenha no processo de recomposição, identificando a não aplicabilidade da 
recomposição fluente ou interrompendo a autonomia dos Agentes Operadores  da Instalação na 
recomposição, deve ser utilizado o Subitem 5.3.  
5.1.4. Caracterizado desligamento parcial da Instalação, deve ser utilizado o Subitem 5.4.  
5.2. PROCEDIMENTOS PARA RECOMPOSIÇÃO COM AUTONOMIA 
5.2.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO COM AUTONOMIA  
No caso de desligamento total, os Agentes Operadores devem configurar os disjuntores dos seguintes 
equipamentos e linhas de transmissão, conforme condição apresentada a seguir: 
Abrir ou manter abertos os disjuntores: 
• das linhas de transmissão: 
LT 525 kV Capivari do Sul / Guaíba 3; 
LT 525 kV Gravataí / Guaíba 3; 
LT 525 kV Guaíba 3 / Nova Santa Rita C1; 
LT 525 kV Guaíba 3 / Nova Santa Rita C2; 
LT 525 kV Guaíba 3 / Povo Novo C1; 
LT 525 kV Guaíba 3 / Povo Novo C2; 
LT 525 kV Guaíba 3 / Povo Novo C3; 
LT 525 kV Candiota 2 / Guaíba 3 C1; 
LT 525 kV Candiota 2 / Guaíba 3 C2; 
LT 230 kV Guaíba 2 / Guaíba 3 C1; 
LT 230 kV Guaíba 2 / Guaíba 3 C2; 
LT 230 kV Charqueadas 3 / Guaíba 3. 
• dos transformadores:  
TF1 525/230/13,8 kV (lados de 525 kV e de 230 kV); 
TF2 525/230/13,8 kV (lados de 525 kV e de 230 kV). 
• do Compensador Estático: 
CE1. 
• das interligações de módulos: 
LT 525 kV Gravataí / Guaíba 3 – Barra A; 
LT 525 kV Guaíba 3 / Nova Santa Rita C1 – Barra A; 
LT 525 kV Guaíba 3 / Nova Santa Rita C2 – Barra A; 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Guaíba 3 IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
Referência:  8 / 16 
 
LT 525 kV Guaíba 3 / Povo Novo C1 – Barra A; 
LT 525 kV Guaíba 3 / Povo Novo C2 – LT 525 kV Capivari do Sul / Guaíba 3; 
LT 525 kV Guaíba 3 / Povo Novo C3 – Barra B; 
LT 525 kV Candiota 2 / Guaíba 3 C1 – Compensador Estático CE1; 
LT 525 kV Candiota 2 / Guaíba 3 C2 – Barra B; 
RE 1 525 kV – Transformador TF1 525/230/13,8 kV; 
RE 2 525 kV – Transformador TF2 525/230/13,8 kV. 
• dos reatores: 
RE1 525 kV; 
RE2 525 kV; 
RE7 525 kV. 
Fechar ou manter fechado o disjuntor: 
• do módulo de interligação barras de 230 kV, exceto quando esse estiver substituindo o disjuntor de um 
equipamento ou de uma linha de transmissão. 
Desligar ou manter desligado o modo de comutação automática dos comutadores sob carga da 
transformação 525/230/13,8 kV da SE Guaíba 3. 
Cabe ao Agente Cymi informar ao COSR-S quando a configuração de preparação da Instalação não estiver 
atendida para o início da recomposição, independentemente de o equipamento ser próprio ou de outros 
agentes. Nesse caso, o COSR -S fará contato com os agentes envolvidos para identificar o motivo do não -
atendimento e, após confirmação do Agente Cymi de que o barramento está com a configuração atendida, 
o COSR -S coordenará os procedimentos para recomposição, caso necessário, em função da configuração 
desta Instalação. 
5.2.2. RECOMPOSIÇÃO COM AUTONOMIA APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
Os Agentes Operadores da Instalação deve m realizar com autonomia os seguintes procedimentos para a 
recomposição: 
Passo Executor Procedimentos Condições ou Limites Associados 
1 Cymi 
Receber tensão da SE Gravataí, pela LT 525 kV 
Gravataí / Guaíba 3 , e energizar as Barras A e B 
525 kV. 
 
2 Cymi 
Receber tensão da SE Nova Santa Rita , pela LT 
525 kV Guaíba 3 / Nova Santa Rita C2, e ligar em 
anel. 
VGUA3-525 ≤ 547 kV. 
2.1 Cymi Energizar a LT 525 kV Guaíba 3 / Povo Novo C2, 
enviando tensão para a SE Povo Novo. 
VGUA3-525 ≤ 541 kV. 
2.1.1 
Cymi 
Energizar, pelo lado de 525 kV, o Transformador 
TF1 525/230/13,8 kV (ou TF2) da SE Guaíba 3 e 
ligar o lado de 230 kV,  energizando o 
barramento de 230 kV. 
Após fluxo de potência ativa nas:  
- LT 525 kV Gravataí / Guaíba 3, 
- LT 525 kV Guaíba 3 / Nova Santa 
Rita C2, e 
- LT 525 kV Guaíba 3 / Povo Novo 
C2. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Guaíba 3 IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
Referência:  9 / 16 
 
Passo Executor Procedimentos Condições ou Limites Associados 
2.1.1.1 
Cymi 
Energizar a LT 230 kV Guaíba 2 / Guaíba 3 C1 (ou 
C2), enviando tensão para a SE Guaíba 2. 
 
2.1.2 Cymi 
Energizar, pelo lado de 525 kV, o Transformador 
TF2 525/230/13,8 kV (ou TF1) da SE Guaíba 3 e 
ligar, interligando esse transformador com o 
outro transformador 525/230/13,8 kV da SE 
Guaíba 3, o lado de 230 kV. 
Após fluxo de potência ativa  no 
transformador TF1 
525/230/13,8 kV (ou TF2) da SE 
Guaíba 3. 
2.1.2.1 
Cymi 
Energizar a LT 230 kV Guaíba 2 / Guaíba 3 C2 (ou 
C1), enviando tensão para a SE Guaíba 2. 
Após fluxo de potência ativa  no 
primeiro circuito da LT 230 kV 
Guaíba 2 / Guaíba 3. 
2.1.2.2 
MEZ 
Energizar a LT 230 kV Charqueadas 3 / Guaíba 3, 
enviando tensão para a SE Charqueadas 3. 
Após fluxo de potência ativa: 
- na LT 230 kV Guaíba 2 / Guaíba 3 
C1 e C2, e 
- nos dois transformadores 
525/230/13,8 kV da SE Guaíba 3. 
2.2 Cymi 
Energizar a LT 525 kV Candiota 2 / Guaíba 3 C1 
(ou C2), enviando tensão para a SE Candiota 2. 
VGUA3-525 ≤ 534 kV. 
VGUA3-230 ≤ 237 kV. 
Após fluxo de potência ativa nas:  
- LT 525 kV Gravataí / Guaíba 3, 
- LT 525 kV Guaíba 3 / Nova Santa 
Rita C2, 
- LT 525 kV Guaíba 3 / Povo Novo 
C2, e 
- nos dois transformadores 
525/230/13,8 kV. 
2.3 Cymi 
Energizar a LT 525 kV Candiota 2 / Guaíba 3 C2 
(ou C1), enviando tensão para a SE Candiota 2. 
VGUA3-525 ≤ 534 kV. 
Após fluxo de potência ativa  no 
primeiro circuito da LT 525 kV 
Candiota 2 / Guaíba 3. 
3 CGT Eletrosul 
(COT Sul) 
Receber tensão da SE Nova Santa Rita, pela LT 
525 kV Guaíba 3 / Nova Santa Rita C1, e ligar em 
anel. 
VGUA3-525 ≤ 547 kV. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Guaíba 3 IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
Referência:  10 / 16 
 
Passo Executor Procedimentos Condições ou Limites Associados 
3.1 CGT Eletrosul 
(COT Sul) 
Energizar a LT 525 kV Guaíba 3 / Povo Novo C1 , 
enviando tensão para a SE Povo Novo. 
Após fluxo de potência ativa nas:  
- LT 525 kV Gravataí / Guaíba 3,  
- LT 525 kV Guaíba 3 / Povo Novo , 
C2, 
- LT 525 kV Candiota 2 / Guaíba 3  
C1 e C2, e  
- nos dois transformadores 
525/230/13,8 kV. 
3.2 Neoenergia 
Energizar a LT 525 kV Guaíba 3 / Povo Novo C 3, 
enviando tensão para a SE Povo Novo. 
Após fluxo de potência ativa nas:  
- LT 525 kV Gravataí / Guaíba 3,  
- LT 525 kV Guaíba 3 / Povo Novo , 
C1 e C2, 
- LT 525 kV Candiota 2 / Guaíba 3 
C1 e C2, e 
- nos dois transformadores 
525/230/13,8 kV. 
3.3 Cymi 
Energizar a LT 525 kV Capivari do Sul / Guaíba 3, 
enviando tensão para a SE Capivari do Sul. 
VGUA3-525 ≤ 538 kV. (*) 
Após fluxo de potência ativa nas:  
- LT 525 kV Gravataí / Guaíba 3,  
- LT 525 kV Guaíba 3 / Povo Novo , 
C1, C2 e C3, 
- LT 525 kV Candiota 2 / Guaíba 3 
C1 e C2, e 
- nos dois transformadores 
525/230/13,8 kV. 
(*) Com ou sem RE7 525 kV da SE 
Guaíba 3 e RE10  525 kV da SE 
Capivari do Sul conectados. 
5.3. PROCEDIMENTOS APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
5.3.1. PREPARAÇÃO DA INSTALAÇÃO APÓS DESLIGAMENTO TOTAL 
Os Agentes Operadores da Instalação devem realizar a preparação conforme Subitem 5.2.1. 
5.3.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO TOTAL DA INSTALAÇÃO 
Os Agentes Operadores da Instalação devem  realizar os procedimentos do Subitem 5.2.2 ., enquanto não 
houver intervenção do COSR -S no processo de recomposição ou interrupção da autonomia desses na 
recomposição.  
Havendo intervenção, a recomposição da Instalação é executada com o controle do COSR -S, conforme 
procedimentos contidos nas respectivas Instruções de Preparação para Manobras.  
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Guaíba 3 IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
Referência:  11 / 16 
 
5.4. PROCEDIMENTOS APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO 
5.4.1. PREPARAÇÃO DA INSTALAÇÃO PARA A RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL 
5.4.1.1. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão em todos os barramentos, 
• ausência de fluxo de potência ativa nas linhas de transmissão, e 
• existência de tensão de retorno em pelo menos uma das linhas de transmissão da Instalação, os 
Agentes Operadores devem  preparar a Instalação conforme Subitem 5.2 .1., sem necessidade de 
autorização do ONS. 
5.4.1.2. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão nas barras de 525 kV, e 
• ausência de fluxo de potência ativa nas linhas de transmissão de 525 kV, os Agentes Operadores 
devem preparar o setor de 525 kV d a Instalação (disjuntores das linhas de transmissão e 
equipamentos) conforme Subitem 5.2.1., sem necessidade de autorização do ONS.  
5.4.1.3. Caracterizado desligamento parcial da Instalação que seja:  
• ausência de tensão nas barras de 230 kV , os Agentes Operadores  devem preparar o setor de 
230 kV da Instalação (disjuntores das linhas de transmissão e equipamentos) conforme Subitem 
5.2.1., sem necessidade de autorização do ONS.  
5.4.1.4. Para demais desligamentos parciais da Instalação, não há necessidade de preparação da Instalação. 
5.4.2. RECOMPOSIÇÃO APÓS DESLIGAMENTO PARCIAL DA INSTALAÇÃO  
5.4.2.1. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.1., os Agentes 
Operadores devem  recompor a Instalação conforme Subitem 5.2 .2., sem necessidade de 
autorização do ONS. 
5.4.2.2. Caracterizado desligamento parcial da Instalação, conforme Subitens 5.4.1.2. e 5.4.1.3., os Agentes 
Operadores devem informar ao COSR-S, para que a recomposição da Instalação seja executada com 
controle do COSR-S. 
5.4.2.3. Caracterizado desligamento parcial da Instalação, conforme Subitem 5.4.1.4., os Agentes 
Operadores devem recompor a Instalação conforme Subitem 6.2.2., sem necessidade de 
autorização do ONS. 
Quando as condições ou limites associados, constantes no Subitem 6.2.2., não estiverem atendidos, 
os Agentes Operadores devem informar ao COSR -S, para que a recomposição da Instalação seja 
executada com controle do COSR-S. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Guaíba 3 IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
Referência:  12 / 16 
 
6. MANOBRAS DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.1. PROCEDIMENTOS GERAIS 
6.1.1. Os procedimentos para desenergização programada ou de urgência de linhas de transmissão ou de 
equipamentos, só podem ser efetuados com controle do COSR-S. 
6.1.2. Os procedimentos para energização de linhas de transmi ssão ou de equipamentos, após  
desligamento programado, de urgência ou de emergência, só podem ser efetuados com controle do 
COSR-S. 
6.1.3. Os procedimentos para energização e fechamento em anel de linhas de transmissão ou de 
equipamentos, após desligamento automático sem atuação de proteção que impeça o retorno do 
equipamento, só podem ser executados com autonomia pelos Agentes Operadores da Instalação 
quando explicitados e estiverem atendidas as condições do Subitem 6.2.2. desta Instrução de 
Operação. 
Quando as condições ou limites associados não estiverem atendidos ou quando não existir 
autonomia, a energização deve ser executada com controle do COSR -S, conforme Instrução de 
Operação de Preparação para Manobras da respectiva área elétrica. 
6.1.4. Antes do fechamento de qualquer disjuntor, os Agentes Operadores da Instalação devem verificar se 
existe tensão de retorno e se a condição de fechamento será em anel. 
O fechamento em anel só pode ser executado com autonomia pelos Agentes Operadores da 
Instalação quando estiver especificado nesta Instrução de O peração e estiverem atendidas as 
condições do Subitem 6.2.2. 
O fechamento de paralelo só pode ser efetuado com controle do COSR-S. 
6.1.5. No que se refere ao sentido de energização de linha de transmissão ou de equipamento: 
6.1.5.1. A energização em sentido normal ou , quando permitida, em sentido  inverso, pode ser feita com 
autonomia pelos Agentes Operadores da Instalação, conforme procedimentos para manobras que 
estão definidos nesta Instrução de Operação, Subitem 6.2.2. Os procedimentos para energização 
controlados pelo COSR-S estão definidos na Instrução de Operação de Preparação para Manobras 
da respectiva área elétrica.  
6.1.5.2. A energização em sentido inverso deve ser efetuada quando, na energização em sentido normal, 
as condições não estiverem atendidas ou não houver sucesso na energização.  
6.1.6. Os procedimentos de segurança a serem adotados na Instalação, durante execução de intervenções, 
são de responsabilidade do Agente.  
6.1.7. Em caso de abertura apenas do terminal / lado pelo qual a linha de transmissão ou transformador é 
energizado, o Agente Operador da Instalação deve fechá-lo em anel desde que tenha autonomia para 
tal, adotando as condições para o fechamento constantes no Subitem 6.2.2. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Guaíba 3 IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
Referência:  13 / 16 
 
6.1.8. Em caso de abertura manual indevida de qualquer disjuntor da instalação, implicando ou não em 
desligamento de linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser 
realizado com controle do COSR-S.  
6.1.9. Em caso de abertura automática de disjuntor da instalação que não implique em desligamento de 
linha de transmissão ou equipamento, o fechamento desse disjuntor deve ser realizado com controle 
do COSR-S. 
6.1.10. Os procedimentos para manobras de linhas de transmissão dotadas de reatores de linha fixo 
consideram esse equipamento conectado; caso contrário, essa informação constará no respectivo 
procedimento para manobra. 
6.2. PROCEDIMENTOS ESPECÍFICOS 
6.2.1. DESENERGIZAÇÃO DE EQUIPAMENTOS 
A desenergização de linhas de transmissão ou de equipamentos, pertencentes à Rede de Operação, é 
sempre controlada pelo COSR-S. 
6.2.2. ENERGIZAÇÃO DE LINHAS DE TRANSMISSÃO E DE EQUIPAMENTOS 
6.2.2.1. Quando da atuação de esquema especial de proteção, as ações de restabelecimento dos 
equipamentos e linhas de transmissão, desligados pela atuação do esquema, devem ser adotadas 
após autorização do COSR-S. 
6.2.2.2. Os procedimentos listados a seguir devem ser adotados pelos Agentes Operadores da Instalação , 
após desligamento automático de equipamentos ou de linhas de transmissão. 
Os Agentes Operadores da Instalação devem identificar os desligamentos automáticos observando 
na Instalação as demais linhas de transmissão e equipamentos em operação, conforme explicitado 
nas condições de energização para a manobra. 
Para os demais desligamentos parciais, proceder conforme Subitem 5.4. 
Linha de Transmissão / 
Equipamento  Procedimentos Condições ou Limites Associados 
LT 525 kV Capivari do 
Sul / Guaíba 3 
Sentido Normal: SE Guaíba 3 envia tensão para a SE Capivari do Sul 
Energizar a LT 525 kV Capivari do Sul / 
Guaíba 3. 
Sistema completo (de LT, TR e CER) ou 
N-1 (de LT, TR ou CER) na SE Guaíba 3 
525 kV 
• VGUA3-525 ≤ 538 kV. (*) 
(*) Com ou sem RE7 525 kV da SE 
Guaíba 3 e RE10  525 kV da SE Capivari 
do Sul conectados. 
Sentido Inverso: SE Guaíba 3 recebe tensão da SE Capivari do Sul 
Ligar, em anel, a LT 525 kV Capivari do 
Sul / Guaíba 3. 
 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Guaíba 3 IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
Referência:  14 / 16 
 
Linha de Transmissão / 
Equipamento  Procedimentos Condições ou Limites Associados 
LT 525 kV Gravataí / 
Guaíba 3 
Sentido Normal: SE Guaíba 3 recebe tensão da SE Gravataí 
Ligar, em anel, a LT 525 kV Gravataí / 
Guaíba 3.  
• VGUA3-525 ≤ 545 kV. 
Sentido Inverso: SE Guaíba 3 envia tensão para a SE Gravataí 
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.5SU. 
LT 525 kV Guaíba 3 / 
Nova Santa Rita C1 ou 
C2 
Sentido Normal: SE Guaíba 3 recebe tensão da SE Nova Santa Rita  
Ligar, em anel, a LT 525 kV Guaíba 3 / 
Nova Santa Rita C1 (ou C2).  
• VGUA3-525 ≤ 547 kV. 
Sentido Inverso: SE Guaíba 3 envia tensão para a SE Nova Santa Rita  
A energização em sentido inverso é controlada pelo COSR-S, conforme IO-
PM.S.5SU. 
LT 525 kV Guaíba 3 / 
Povo Novo C1, C2 ou C3  
Sentido Normal: SE Guaíba 3 envia tensão para a SE Povo Novo 
Energizar a LT 525 kV Guaíba 3 / Povo 
Novo C1 (C2 ou C3). 
Sistema completo (de LT e TR) ou N-1 
(de LT ou TR) na SE Guaíba 3 525 kV 
• VGUA3-525 ≤ 550 kV.    
Antes de energizar cada LT, verificar 
fluxo de potência ativa na LT 525 kV 
Guaíba 3 / Povo Novo que já foi 
energizada. 
Sentido Inverso: SE Guaíba 3 recebe tensão da SE Povo Novo  
Ligar, em anel, a LT 525 kV Guaíba 3 / 
Povo Novo C1 (C2 ou C3).  
 
LT 525 kV Candiota 2 / 
Guaíba 3 C1 ou C2 
Sentido Normal: SE Guaíba 3 envia tensão para a SE Candiota 2  
Energizar a  LT 525 kV Candiota 2 / 
Guaíba 3 C1 (ou C2). 
Sistema completo (de LT, TR e CER) ou 
N-1 (de LT, TR ou CER) na SE Guaíba 3 
525 kV 
• VGUA3-525 ≤ 534 kV, e 
• VGUA3-230 ≤ 237 kV. 
Antes de energizar cada LT, verificar 
fluxo de potência ativa na LT 525 kV 
Candiota 2 / Guaíba 3 que já foi 
energizada. 
Sentido Inverso: SE Guaíba 3 recebe tensão da SE Candiota 2  
Ligar, em anel, a LT 525 kV Candiota 2 
/ Guaíba 3 C1 (ou C2). 
• ∆δ ≤ 16°. 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Guaíba 3 IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
Referência:  15 / 16 
 
Linha de Transmissão / 
Equipamento  Procedimentos Condições ou Limites Associados 
LT 230 kV Guaíba 2 / 
Guaíba 3 C1 ou C2 
Sentido Normal: SE Guaíba 3 envia tensão para a SE Guaíba 2  
Energizar a  LT 230 kV Guaíba 2 / 
Guaíba 3 C1 (ou C2). 
Sistema completo (de LT e TR) ou N-1 
(de LT ou TR) na SE Guaíba 3 230 kV 
• VGUA3-230 ≤ 242 kV. 
Antes de energizar cada LT, verificar 
fluxo de potência ativa na LT 230 kV 
Guaíba 2 / Guaíba 3 que já foi 
energizada. 
Sentido Inverso: SE Guaíba 3 recebe tensão da SE Guaíba 2  
Ligar, em anel, a LT 230 kV Guaíba 2 / 
Guaíba 3 C1 (ou C2). 
 
LT 230 kV Charqueadas 
3 / Guaíba 3 
Sentido Único: SE Guaíba 3 envia tensão para a SE Charqueadas 3  
Energizar a LT 230 kV Charqueadas 3 
/ Guaíba 3. 
Sistema completo (de LT e TR) ou N-1 
(de LT ou TR) na SE Guaíba 3 230 kV 
• VGUA3-230 ≤ 242 kV. 
Transformador 
TF1 ou TF2 
525/230/13,8 kV 
Sentido Normal: A partir do lado de 525 kV 
Energizar, pelo lado de 525 kV, o 
transformador TF1 525/230 /13,8 kV 
(ou TF2). 
Sistema completo (de LT) ou N-1 (de LT) 
na SE Guaíba 3 525 kV 
Como primeiro ou segundo 
transformador: 
• VGUA3-525 ≤ 550 kV. 
Antes de energizar cada transformador, 
verificar fluxo de potência ativa nos 
transformadores 525/230/13,8 kV que 
já foram energizados. 
Ligar, interligando esse 
transformador com outro 
transformador 525/230/13,8 kV da SE 
Guaíba 3, o lado de 230 kV do 
transformador T F1 525/230/13,8 kV 
(ou TF2). 
O outro transformador 
525/230/13,8 kV da SE Guaíba 3 deve 
estar com fluxo de potência ativa.  
Sentido Inverso: A partir do lado de 230 kV  
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.12 
Instrução de Operação para Procedimentos Sistêmicos da Instalação Código Revisão Item Vigência 
Procedimentos Sistêmicos para a Operação da SE 
Guaíba 3 IO-OI.S.GUA3 12 3.7.5.1. 10/10/2024 
 
Referência:  16 / 16 
 
Linha de Transmissão / 
Equipamento  Procedimentos Condições ou Limites Associados 
Energizar, o lado de 230 kV do 
transformador TF1 525/230 /13,8 kV 
(ou TF2). 
Sistema completo (de LT) ou N-1 (de LT) 
na SE Guaíba 3 230 kV 
Como primeiro ou segundo 
transformador: 
• VGUA3-230 ≤ 242 kV; e 
• 1 ≤ TAPGUA3-525/230 ≤ 10. 
Antes de energizar cada transformador, 
verificar fluxo de potência ativa nos 
transformadores 525/230/13,8 kV que 
já foram energizados. 
Ligar, em anel , o lado de 525 kV do 
transformador T F1 525/230/13,8 kV 
(ou TF2). 
 
Reator RE1, RE2 ou RE7 
525 kV A manobra destes Reatores é controlada pelo COSR-S. 
Compensador Estático 
CE1 17,5 kV 
-180 / +300 Mvar 
A manobra deste Compensador Estático  é controlada pelo COSR-S. 
7. NOTAS IMPORTANTES 
7.1. Os disjuntores de interligação de módulos deve m ser fechados, em anel, toda vez que forem 
recompostos os dois circuitos do mesmo vão ou quando, num dos lados do vão, for uma barra. 
7.2. A MEZ deve informar ao COSR-S qualquer mudança que ocorra ou que venha a ser necessária na faixa 
de operação do Compensador Estático CE1, conforme consta no Cadastro de Informações 
Operacionais CD-CT.S.5SU.01 – Cadastro de Dados Operacionais de Equipamentos da Área 525 kV da 
Região Sul, em função da condição de disponibilidade dos elementos TCR1, TCR2, TSC1, TSC2 ou 
filtros de harmônicos (STF1 ou STF2) desse Compensador Estático de Reativos. 
7.3. Para ligar o Compensador Estático CE1, a MEZ deve solicitar à Cymi fechar o disjuntor DJ 1150 (central 
do vão de 525 kV associado), que possui resistor de pré-inserção. 
Caso a MEZ opte por ligar o Compensador Estático iniciando pelo disjuntor da Barra B de 525 kV (DJ 
1152), essa deve verificar se o dispositivo sincronizador desse disjuntor está habilitado , devido a 
restrição por transitório eletromagnético na energização do Transformador 525/17,5  kV do 
Compensador; caso o dispositivo sincronizador do Disjuntor DJ 1 152 não esteja habilitado, esse 
disjuntor somente poderá ser fechado após o fechamento do Disjuntor 1150. 
7.4. Para substituir o disjuntor da LT 230 kV Charqueadas 3 / Guaíba 3  pelo disjuntor de interligação das 
barras A e B, o COSR -S coordenará com a Cymi e com a MEZ  esses procedimentos. Após tal 
substituição, os procedimentos para abertura do disjuntor ou recomposição da linha de transmissão 
deverão ser coordenados pelo COSR -S, que orientará a operação da Cymi na execução dos 
procedimentos descritos nesta Instrução de Operação e na IO -PM.S.2RS – Procedimentos de 
Preparação para Manobras na Área 230 kV do Rio Grande do Sul. 
