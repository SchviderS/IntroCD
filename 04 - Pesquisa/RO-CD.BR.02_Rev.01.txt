Manual de Procedimentos da Operação
Módulo 5 - Submódulo 5.13
Rotina Operacional
Controle de Informações Operacionais de faixas para controle de tensão
Código Revisão Item Vigência
RO-CD.BR.02 01 4.1.1. 16/09/2019
.
MOTIVO DA REVISÃO:
 Adequação à nova configuração funcional da Diretoria de Operação do ONS.
LISTA DE DISTRIBUIÇÃO
CNOS COSR-SE COSR-NE COSR-NCO COSR-S Agentes de Operação
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Controle de Informações Operacionais de faixas para 
controle de tensão RO-CD.BR.02 01 4.1.1. 16/09/2019
Referência:  2 / 8
ÍNDICE
1. OBJETIVO.........................................................................................................................................3
2. REFERÊNCIAS ...................................................................................................................................3
3. CONCEITOS ......................................................................................................................................3
4. CONSIDERAÇÕES GERAIS .................................................................................................................3
5. DESCRIÇÃO DO PROCESSO ...............................................................................................................5
5.1. Fase precedente à entrada em operação da instalação................................................................5
5.2. Fase posterior à entrada em operação da instalação ...................................................................5
6. ATRIBUIÇÕES DO ONS E AGENTES....................................................................................................6
6.1. Atribuições da Diretoria de Operação do ONS (DOP) ...................................................................6
6.2. Atribuições da Diretoria de Planejamento do ONS (DPL)..............................................................6
6.3. Atribuições dos Agentes................................................................................................................6
7. ANEXOS ...........................................................................................................................................7
ANEXO 1 – Exemplo de Tabela de definição de períodos de carga por faixa horária..............................7
ANEXO 2 – Exemplo de tabela de definição de faixas de tensão por períodos de carga ........................7
ANEXO 3 – Exemplo de tabela de definição de faixas de tensão por faixa de demanda ........................8
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Controle de Informações Operacionais de faixas para 
controle de tensão RO-CD.BR.02 01 4.1.1. 16/09/2019
Referência:  3 / 8
1. OBJETIVO
Estabelecer o processo de controle e atualização dos valores das faixas para controle de tensão dos 
barramentos da Rede de Operação e dos barramentos não pertencentes à Rede de Operação, mas que estão 
conectados aos secundários das transformações dessa Rede, assim como as atividades e responsabilidades 
do ONS e dos Agentes envolvidos.
2. REFERÊNCIAS
Submódulo 10.7 - Controle da Transmissão em Operação Normal dos Procedimentos de Rede.
3. CONCEITOS
3.1. Faixa para controle de tensão - são os limites máximo e mínimo entre os quais deve ser controlada a 
tensão  dos barramentos.
3.2. Estas faixas para controle de tensão são caracterizadas por períodos de carga: Pesada, Média, Leve 
e Mínima.
3.3. Os períodos de carga podem ser definidos em função de Faixa Horária ou Faixa de Demanda, 
conforme definido a seguir:
3.3.1. A Faixa Horária é estabelecida em função dos dias da semana, feriados e horários nos quais se 
verificam os períodos de carga Pesada, Média, Leve e Mínima.
3.3.2. A  Faixa de Demanda  é estabelecida em função de uma grandeza específica que pode ser demanda, 
fluxo, etc.
3.4. BDT – Base de Dados Técnica que contém os dados cadastrais dos Agentes, de suas instalações e de 
seus equipamentos.
3.5. GERLIM - Aplicativo desenvolvido para cadastro, manutenção e controle dos dados de faixas de 
tensão dos barramentos e de limites operativos de equipamentos na Base de Dados Técnica (BDT) e 
Base de Dados de Tempo Real.
4. CONSIDERAÇÕES GERAIS
4.1. Os Cadastros de Informações Operacionais de Faixas para Controle de Tensão são os documentos de 
referência utilizados pelo ONS e pelos Agentes para definição dos valores das faixas de tensão dos 
barramentos da Rede de Operação e dos barramentos não pertencentes à Rede de Operação, mas 
que estão conectados aos secundários das transformações dessa Rede.
4.2. As faixas para controle de tensão dos barramentos devem ser implantadas por meio dos Cadastros 
de Informações Operacionais de Faixas para Controle de Tensão, devendo existir um cadastro para 
cada área elétrica do Sistema Interligado Nacional, SIN. 
4.3. O padrão de elaboração dos cadastros de faixas de controle de tensão pela Gerência de 
Procedimentos Operativos está estabelecido na RT-CD.BR.03 - Elaboração do Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão.
4.4. As alterações dos valores das faixas para controle de tensão dos barramentos podem ser originadas 
em função de:
  Informações de Relatórios de estudos elaborados pelas gerências da Diretoria de Planejamento - 
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Controle de Informações Operacionais de faixas para 
controle de tensão RO-CD.BR.02 01 4.1.1. 16/09/2019
Referência:  4 / 8
DPL.
 Situações operacionais vivenciadas em tempo real, após validação da DPL dos novos valores.
 Restrições em equipamentos informadas pelos agentes.
4.5. O Agente proprietário de instalação que faz fronteira com a Rede Básica pode solicitar as Gerências 
responsáveis pelos estudos do ONS um ajuste nos valores dos seus limites de tensão para atender às 
suas necessidades. O ajuste solicitado só poderá ser adotado pela operação caso o desempenho do 
SIN não seja afetado, sejam respeitadas as limitações específicas dos equipamentos e haja anuência 
do agente de transmissão envolvido.
4.6. Os valores limites das faixas para controle de tensão são atualizados pela Gerência de Procedimentos 
Operativos. Os valores das faixas para controle de tensão a serem alterados são modificados no 
GERLIM, que atualiza esses valores na BDT e gera automaticamente uma revisão do Cadastro de 
Informações Operacionais de Faixas para Controle de Tensão da respectiva área elétrica ou região 
correspondente.
4.7. Quando as Gerências responsáveis pelos estudos do ONS indicarem a necessidade de atualização da 
definição dos períodos de carga mínima, leve, média e pesada, a Gerência de Procedimentos 
Operativos deve atualizar os Cadastros de Informações Operacionais de Faixas para Controle de 
Tensão e repassar a informação para a Gerência de Tempo Real do Centro envolvido, que procederá 
a atualização na BDT. Essa atualização somente vai ser feita na Base de Dados de Tempo Real quando 
for feita atualização da BDT pela Gerência de Supervisão e Controle do Centro Regional. A atualização 
da BDT ocorre, geralmente, semanalmente. 
Cabe à Gerência de Procedimentos Operativos a sincronização dessas ações para que a revisão do 
documento só entre em vigência com a atualização da Base de Dados do Tempo Real.
4.8. Quando as Gerências responsáveis pelos estudos do ONS indicarem necessidade de atualização da 
definição das Faixas de Demanda ou dos seus valores, a Gerência de Procedimentos Operativos deve 
atualizar o Cadastro de Informações Operacionais de Faixas para Controle de Tensão e repassar a 
informação para:
 Gerências de Tempo Real do ONS, que, caso se alterem os valores das Faixas de Demanda, devem 
cadastrar esses novos valores na Base de Dados do Tempo Real.
 Gerência de Supervisão e Controle do ONS, que, caso se altere a definição das Faixas de Demanda 
(por exemplo, inserção/retirada de uma parcela), deve realizar essa alteração na BDT. Essa 
atualização somente vai para a Base de Dados do Tempo Real quando for feita atualização da BDT, 
a qual ocorre, geralmente, semanalmente.
Cabe à Gerência de Procedimentos Operativos a sincronização dessas ações para que a revisão do 
documento só entre em vigência com a atualização da Base de Dados do Tempo Real.
4.9. A alteração dos valores das faixas para controle de tensão da Base de Dados Técnica (BDT) do ONS e 
a elaboração do respectivo Cadastros de Informações Operacionais de Faixas para Controle de Tensão 
Faixas de 
Tensão
Estudos 
diversos
Gerência de 
Procedimentos 
Operativos
Atualizar 
GERLIM
Atualiza 
BDT
Atualizar Base 
de Dados do 
Tempo Real
Gera revisão 
do cadastro
Entrada em 
operação
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Controle de Informações Operacionais de faixas para 
controle de tensão RO-CD.BR.02 01 4.1.1. 16/09/2019
Referência:  5 / 8
são realizadas por meio do GERLIM. Toda alteração feita no GERLIM será recebida e atualizada na 
Base de Dados do Tempo Real (BTR).
5. DESCRIÇÃO DO PROCESSO
5.1. FASE PRECEDENTE À ENTRADA EM OPERAÇÃO DA INSTALAÇÃO
5.1.1. Nesta fase, as informações referentes aos valores das faixas para controle de tensão dos 
barramentos são obtidas por meio de relatório Pré-Operacional, Quadrimestral, Mensal ou estudo 
específico. O cadastro destas faixas no GERLIM é realizado pela Gerência de Procedimentos 
Operativos.
5.1.2. O Agente proprietário de instalação que faz fronteira com a Rede Básica pode solicitar às Gerências 
responsáveis pelos estudos do ONS um ajuste nos valores do seus limites de tensão para atender 
às suas necessidades. Esse ajuste poderá ser feito caso o desempenho do SIN não seja afetado, 
sejam respeitadas as limitações específicas dos equipamentos e haja anuência do agente de 
transmissão envolvido.
5.1.3. Quando uma nova instalação entrar em operação, os valores correspondentes às faixas para 
controle de tensão serão adicionados pela Gerência de Procedimentos Operativos no GERLIM, que 
atualizará esses valores na BDT e gerará automaticamente uma revisão do Cadastro de Informações 
Operacionais de Faixas para Controle de Tensão. A Base de Dados do Tempo Real é atualizada logo 
após a alteração ser realizada na BDT.
5.2. FASE POSTERIOR À ENTRADA EM OPERAÇÃO DA INSTALAÇÃO
5.2.1. Nesta fase, os valores das faixas para controle de tensão a serem cadastrados na BDT devem ser 
disponibilizados pela DPL. Eventuais alterações nos valores devem ser introduzidos pela Gerência 
de Procedimentos Operativos no GERLIM, que atualiza esses valores na BDT e gera 
automaticamente uma revisão do Cadastro de Informações Operacionais de Faixas para Controle 
de Tensão. A Base de Dados do Tempo Real é atualizada quase que simultaneamente à BDT.
5.2.2. O Agente proprietário de instalação que faz fronteira com a Rede Básica pode solicitar às Gerências 
responsáveis pelos estudos do ONS um ajuste nos valores dos seus limites de tensão para atender 
às suas necessidades. Esse ajuste poderá ser feito caso o desempenho do SIN não seja afetado, 
sejam respeitadas as limitações específicas dos equipamentos e haja anuência do agente de 
transmissão envolvido.
BTR REGERGERLIM BDT
CADASTRO DE 
INFORMAÇÕES 
OPERACIONAIS
Barramento de Integração
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Controle de Informações Operacionais de faixas para 
controle de tensão RO-CD.BR.02 01 4.1.1. 16/09/2019
Referência:  6 / 8
6. ATRIBUIÇÕES DO ONS E AGENTES
6.1. ATRIBUIÇÕES DA DIRETORIA DE OPERAÇÃO DO ONS (DOP)
6.1.1. Gerência de Procedimentos Operativos
 Cadastrar o novo valor das faixas para controle de tensão na BDT, utilizando o GERLIM, para que 
seja implementada a alteração na Base de Dados do Tempo Real.
 Informar às demais gerências do ONS e aos agentes envolvidos sobre os novos valores de faixas 
operacionalizadas, por meio da vigência dos Cadastros de Informações Operacionais de Faixas para 
Controle de Tensão.
 Repassar à Gerência de Supervisão e Controle as alterações que houver nas definições das Faixas 
de Demanda das áreas elétricas.
 Repassar às Gerências de Tempo Real as alterações nos valores das demandas e nas Faixas Horárias 
das áreas elétricas.
6.1.2. Gerência de Supervisão e Controle ou Tempo Real do ONS
 Fazer a alteração da definição das Faixas de Demanda e das Faixas Horárias na Base de Dados de 
Tempo Real, quando necessário, após solicitação da Gerência de Procedimentos Operativos.
6.1.3. Gerências de Tempo Real do ONS
 Fazer a alteração das Faixas de Demanda e das Faixas Horárias na Base de Dados de Tempo Real, 
quando necessário, após informação repassada pela Gerência de Procedimentos Operativos.
 Alterar faixas para controle de tensão quando houver restrições ou flexibilizações informadas pelos 
agentes em tempo real.
6.2. ATRIBUIÇÕES DA DIRETORIA DE PLANEJAMENTO DO ONS (DPL)
6.2.1. Informar as faixas para controle de tensão, bem como qualquer alteração ocorrida e sua motivação, 
à Gerência de Procedimentos Operativos por meio de relatório Pré-operacional, Quadrimestral, 
Mensal ou estudo específico.
6.2.2. Receber as informações de faixas para controle de tensão do Agente proprietário de instalação que 
faz fronteira com a Rede Básica, estudá-las e emitir seus valores em relatórios enviados à DOP, caso 
o desempenho do SIN não seja afetado, sejam respeitadas as limitações específicas dos 
equipamentos e haja anuência do agente de transmissão envolvido, esse ajuste poderá ser feito.
6.3. ATRIBUIÇÕES DOS AGENTES
6.3.1. Solicitar ao ONS alteração nas faixas para controle de tensão dos seus barramentos. As Gerências 
responsáveis pelos estudos do ONS devem estudar e validar as alterações.
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Controle de Informações Operacionais de faixas para 
controle de tensão RO-CD.BR.02 01 4.1.1. 16/09/2019
Referência:  7 / 8
7. ANEXOS
ANEXO 1 – EXEMPLO DE TABELA DE DEFINIÇÃO DE PERÍODOS DE CARGA POR FAIXA HORÁRIA
Períodos de carga diários
FAIXA HORÁRIA (h) Dias Úteis Sábados Domingos e Feriados 
00:00 à 01:00 Média Média Leve
01:00 às 06:00 Leve Leve Leve
06:00 às 08:00 Leve Leve Mínima
08:00 às 18:00 Média Leve Mínima
18:00 às 22:00 Pesada Pesada Pesada
22:00 às 23:00 Pesada Pesada Média
23:00 às 24:00 Média Média Média
Obs.: Os períodos indicados referem-se ao horário de Brasília-DF.
ANEXO 2 – EXEMPLO DE TABELA DE DEFINIÇÃO DE FAIXAS DE TENSÃO POR PERÍODOS DE CARGA 
Subestação / Usina Faixas de Tensão em função da faixa horária (kV)
Nome Tensão (kV) PESADA MÉDIA LEVE / MÍNIMA 
DDD 500 kV 530 - 550 530 - 550 520 - 550
EEE 230 kV
FFF 138 kV
Manual de Procedimentos da Operação - Módulo5 - Submódulo 5.13
 Rotina Operacional Código Revisão Item Vigência
Controle de Informações Operacionais de faixas para 
controle de tensão RO-CD.BR.02 01 4.1.1. 16/09/2019
Referência:  8 / 8
ANEXO 3 – EXEMPLO DE TABELA DE DEFINIÇÃO DE FAIXAS DE TENSÃO POR FAIXA DE DEMANDA
Subestação / Usina Faixas de Tensão em função da Demanda D (kV)
Nome Tensão (kV) D > 4400 MW
(Pesada) 
3700 < D < 4400 MW
(Média) 
D < 3700 MW
(Leve / Mínima)
SE AAA xxx 530 - 550 530 - 550 520 - 550
SE BBB xxx 530 - 550 530 - 550 520 - 550
SE CCC (4) xxx 510 - 550 510 - 550 510 - 550
