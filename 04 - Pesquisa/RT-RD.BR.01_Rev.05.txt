 
 
Manual de Procedimentos da Operação 
 
 
Referência Técnica 
Critérios para Definição das Instalações Integrantes das Redes de Operação 
Sistêmica e Regional 
 
 
Código Revisão Item Vigência 
RT-RD.BR.01 05 7.13. 31/07/2024 
 
. 
 
MOTIVO DA REVISÃO 
• Alteração do item 4.1, adequando, principalmente, as orientações para classificação de barramentos, 
disjuntores e chaves seccionadoras classificadas como sistêmico, em função da classificação de 
equipamentos e linhas de transmissão. 
LISTA DE DISTRIBUIÇÃO 
CNOS COSR-NCO COSR-NE COSR-S COSR-SE 
 
 
Manual de Procedimentos da Operação 
Referência Técnica Código Revisão Item Vigência 
Critérios para Definição das Instalações Integrantes das 
Redes de Operação Sistêmica e Regional RT-RD.BR.01 05 7.13. 31/07/2024 
 
Referência:  2 / 6 
 
ÍNDICE 
 
1. OBJETIVO ................................ ................................ ................................ ................................ ..... 3 
2. CONCEITOS ................................ ................................ ................................ ................................ ... 3 
3. REDES FUNCIONAIS DOS CENTROS DE OPERAÇÃO DO ONS ................................ ............................  3 
4. CLASSIFICAÇÃO – REDE DE OPERAÇÃO SISTÊMICA OU REGIONAL ................................ ................... 4 
5. PROCEDIMENTO PARA CLASSIFICAÇÃO DAS REDES SISTÊMICA E REGIONAL ................................ ... 6 
 
 
Manual de Procedimentos da Operação 
Referência Técnica Código Revisão Item Vigência 
Critérios para Definição das Instalações Integrantes das 
Redes de Operação Sistêmica e Regional RT-RD.BR.01 05 7.13. 31/07/2024 
 
Referência:  3 / 6 
 
1. OBJETIVO 
Estabelecer critérios para definir as instalações do Sistema Interligado Nacional – SIN que fazem parte da 
Rede de Operação Sistêmica e da Rede de Operação Regional. 
2. CONCEITOS 
2.1. As definições das redes básica, complementar, de operação, de supervisão e de simulação  estão 
descritas na Rotina Operacional RO-RD.BR.01 – “Definição da Rede a que Pertencem os Equipamentos 
de uma Instalação do SIN”. 
2.2. A definição das atividades de operação em tempo real que descrevem a coordenação, a supervisão, o 
controle, o comando e a execução da operação, exercidas pelos Centros de Operação do ONS e pelos 
agentes, constam no Submódulo 1.2 – “Glossário dos Procedimentos de Rede”.  
2.3. As responsabilidades dos Centros de Operação do ONS estão descritas no  Submódulo 5.1 – “Operação 
do sistema e das instalações da Rede de Operação”. 
3. REDES FUNCIONAIS DOS CENTROS DE OPERAÇÃO DO ONS 
3.1. Para permitir a definição das responsabilidades nas ações operativas dos Centros de Operação do ONS, 
a Rede de Operação está organizada funcionalmente em: 
3.1.1. Rede de Operação Sistêmica: é a parte da Rede de Operação constituída pelas usinas térmicas e 
demais usinas com potência superior a 250 MW, pelas interligações internacionais, e equipamentos 
e linhas  de transmissão, com tensão igual ou superior a 345 kV, utilizad os para a integração 
eletroenergética, cujos fenômenos repercutem predominantemente de forma sistêmica. 
3.1.2. Rede de Operação Regional: é a parte da Rede de Operação constituída pelas usinas não-térmicas 
com potência igual ou inferior a 250 MW,  pelos equipamentos e linhas de transmissão não utilizadas 
para integração eletroenergética, que interligam os sistemas tronco de transmissão às instalações de 
fronteira da Rede de Operação, cujos fenômenos repercutem predominantemente de forma 
regional. 
3.2. As definições de Rede de Operação Sistêmica e de Rede de Operação Regional não afetam as atividades 
de operação dos Agentes. 
Manual de Procedimentos da Operação 
Referência Técnica Código Revisão Item Vigência 
Critérios para Definição das Instalações Integrantes das 
Redes de Operação Sistêmica e Regional RT-RD.BR.01 05 7.13. 31/07/2024 
 
Referência:  4 / 6 
 
4. CLASSIFICAÇÃO – REDE DE OPERAÇÃO SISTÊMICA OU REGIONAL 
A seguir estão descritos os critérios que devem ser observados para classificação da Rede de Operação. 
4.1. Pertencem à Rede de Operação Sistêmica: 
4.1.1. unidades geradoras e seus respectivos transformadores elevadores e seus  módulos gerais, de usinas 
térmicas da Rede de Operação  e das demais usinas da Rede de Operação com potência superior a 
250 MW; 
4.1.2. equipamentos (incluindo todos aqueles que compõe: compensação reativa, chaves e disjuntores dos 
pátios AC e DC do sistema HVDC), e linhas de transmissão pertencentes a interligações internacionais; 
4.1.3. equipamentos (incluindo todos aqueles que compõe : compensação reativa , eletrodos, chaves e 
disjuntores dos pátios AC e DC do sistema HVDC), linhas de transmissão pertencentes a interligações 
em corrente contínua (HVDC); 
4.1.4. equipamentos e linhas de transmissão  (linhas de transmissão e transformadores com tensão no 
enrolamento primário maior ou igual a 345kV)  incluindo equipamentos de controle de tensão 
(reatores de barra, bancos de capacitores shunt, compensadores síncronos e estáticos com o seus 
respectivos transformadores elevadores)  pertencentes às áreas elétricas definidas como 
interligação; 
4.1.5. equipamentos e linhas de transmissão (linhas de transmissão e transformadores com tensão no 
enrolamento primário maior ou igual a 345kV) , pertencentes as demais áreas elétricas, que 
contemplem pelo menos uma das seguintes características: 
a) estejam submetidas a variações significativas de carregamento em decorrência de alteração de 
intercâmbio entre regiões e que acarretem influência sobre esse, cuja indisponibilidade altere 
o limite de intercâmbio; 
b) transmitam potência cujo fluxo não esteja associado ao atendimento às cargas da área elétrica 
à qual pertence;  
c) sua indisponibilidade restrinja montante de geração que impacte nos limites de intercâmbio 
entre regiões;  
d) possuam esquema especial com repercussão em duas ou mais áreas elétricas, cujo objetivo 
seja assegurar a transferência de energia entre regiões; 
e) tenham impacto no controle de tensão dos barramentos e que estejam relacionados aos 
limites de transferência de energia entre regiões. 
4.1.6. instalações com pelo menos um equipamento ou uma linha de transmissão sistêmica; 
4.1.7. barramentos da instalação  (incluindo todas as seções de barra, caso haja) , do nível de tensão 
correspondente, onde se conecta  pelo menos um equipamento ou  uma linha de transmissão 
sistêmica;  
4.1.8. equipamentos conectados diretamente a linhas de transmissão, tais como  reatores fixos ou 
manobráveis de linha e bancos de capacitores série cuja linha de transmissão é classificada como  
sistêmica;  
Manual de Procedimentos da Operação 
Referência Técnica Código Revisão Item Vigência 
Critérios para Definição das Instalações Integrantes das 
Redes de Operação Sistêmica e Regional RT-RD.BR.01 05 7.13. 31/07/2024 
 
Referência:  5 / 6 
 
4.1.9. disjuntores e seccionadoras associad as aos disjuntores que podem ser utilizados na manobra de  
equipamentos e linhas de transmissão sistêmicas;  
Nota: tanto no barramento em anel como no barramento em disjuntor e meio, os disjuntores 
utilizados na manobra são todos aqueles conectados aos equipamentos e linhas de transmissão a 
serem manobrados. 
4.1.10. disjuntores de interligação de barras e chaves associadas (configuração barra dupla ou barra principal 
e transferência ), se pelo menos um equipamento ou uma linha de transmissão da instalação for 
sistêmica;  
4.2. Pertencem à Rede de Operação Regional: 
4.2.1. todos os equipamentos e linhas de transmissão que não se enquadrem nos critérios apresentados 
no subitem 4.1; 
4.2.2. linhas de transmissão que conectem usinas sistêmicas a instalação da Rede de Operação, desde que 
a indisponibilidade de uma dessas linhas de conexão não cause limitação da geração da usina. 
 
4.3. Links de geração ou transmissão e transformadores elevadores: 
Os links de geração ou transmissão e transformadores elevadores que conectam uma unidade 
geradora, compensador estático ou compensador síncrono à Rede de Operação possuem a mesma 
classificação de tipo de malha que está definid a para o respectivo equipamento que está sendo 
interligado. 

Manual de Procedimentos da Operação 
Referência Técnica Código Revisão Item Vigência 
Critérios para Definição das Instalações Integrantes das 
Redes de Operação Sistêmica e Regional RT-RD.BR.01 05 7.13. 31/07/2024 
 
Referência:  6 / 6 
 
 
5. PROCEDIMENTO PARA CLASSIFICAÇÃO DAS REDES SISTÊMICA E REGIONAL 
5.1. A Gerência de Procedimentos Operativos define a classificação dos novos equipamentos  e linhas de 
transmissão seguindo os critérios constantes no item 4. 
5.2. Em função da expansão do SIN, a Gerência de Procedimentos Operativos pode avaliar reclassificações 
de equipamentos e linhas de tra nsmsissão, de modo a sempre manter a coerência dos critérios 
estabelecidos no Item 4. 
5.3. A Gerência de Procedimentos Operativos  deve manter as instruções de operação atualizadas e em 
conformidade com os critérios para definição das redes funcionais estabelecidos no Item 4, bem como, 
informar todas as modificações , ou classificação de novos equipamentos,  nessas redes funcionais  às 
demais gerências responsáveis pela atualização da Base de Dados Técnica e dos diagramas unifilares  
de regiões do SIN. 

