# Projeto:
Busca em manuais do ONS por informações de manobras e/ou agente/instalação

## Equipe:
Data Pówa

## Descrição:
A intenção é usar os documentos normativos do ONS para encontrar quais deles possuem alguma sequência de manobra de alguma localidade. O objetivo é identificar se o documento possui em algum trecho textos que possam ser considerados uma sequência de ações usadas para realização de manobras em equipamentos no sistema elétrico, informações que auxiliam em um momento de ocorrência no sistema para pessoas que precisem consultar como deve ser preparada a configuração de recomposição do sistema ou qual a sequência de passos a ser tomada. Se o objetivo for muito ousado, a tentativa será encontrar pelo menos os documentos de certo agente ou instalação (subestação, usina, conjunto eólico, etc…) entre todos os documentos.

## Membros:
Alexandre Schviderski, 2638908, SchviderS, erdnaxelaschvi@gmail.com, PPGCA, UTFPR
